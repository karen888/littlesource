<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Payments extends Seeder {

	public function run()
	{
        DB::table('payments')->delete();

        DB::table('payments')->insert(Array(
            Array(
                'from_user_id' => '1',
                'job_id' => '0',
                'transaction_id' => '88',
                'payment_amount' => '10.00',
                'payment_refund' => '1',
                'payment_description' => 'Temporary charge to verify your card.',
                'created_at' => '2015-04-13 03:40:40',
                'updated_at' => '2015-04-13 03:40:40',
            )
        ));
    }
}
