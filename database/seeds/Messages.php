<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Messages extends Seeder {

    protected $faker;

    public function __construct()
    {
        $this->faker = Faker\Factory::create();
    }

	public function run()
	{
        $this->add_messages_threads();
        $this->add_messages();
    }


    public function add_messages_threads()
    {
        DB::table('messages_threads')->delete();
        DB::table('messages_threads')->insert([
            [
                'id' => 1,
                'job_id' => 1,
                'sender_id' => 1,
                'receiver_id' => 2,
                'is_archived' => 0,
                'created_at' => Carbon::now()->subDays(4),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'job_id' => 2,
                'sender_id' => 1,
                'receiver_id' => 2,
                'is_archived' => 0,
                'created_at' => Carbon::now()->subDays(7),
                'updated_at' => Carbon::now()->subDays(6),
            ],
            [
                'id' => 3,
                'job_id' => null,
                'sender_id' => 1,
                'receiver_id' => 2,
                'is_archived' => 1,
                'created_at' => Carbon::now()->subDays(15),
                'updated_at' => Carbon::now()->subDays(14),
            ],
        ]);

    }

    public function add_messages()
    {
        DB::table('messages')->delete();
        $this->add_message(1, 1, 1);
        $this->add_message(1, 2, 0);

        $this->add_message(2, 1, 1);
        $this->add_message(2, 2, 1);
        $this->add_message(2, 1, 0);

        $this->add_message(3, 1, 1);
        $this->add_message(3, 2, 1);
        $this->add_message(3, 1, 1);
        $this->add_message(3, 2, 0);
    }

    protected function add_message($thread_id, $sender_id, $is_read, $message = null)
    {
        $msg = $message ?: $this->faker->sentence(30);
        DB::table('messages')->insert([
            [
                'message_thread_id' => $thread_id,
                'user_id' => $sender_id,
                'message' => $msg,
                'is_read' => $is_read,
                'created_at' => Carbon::now()->subDays(4),
                'updated_at' => Carbon::now()->subDays(4),
            ]
        ]);
    }

}
