<?php

use Illuminate\Database\Seeder;
use App\Http\Models\ExperienceLevel;

class ExperienceLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('experience_levels')->truncate();

        ExperienceLevel::create([
            "title" => 'Entry Level',
            "value" => '$',
            "description" => 'I\'m looking for caregivers with the lowest rates',
        ]);

        ExperienceLevel::create([
            "title" => 'Intermediate',
            "value" => '$$',
            "description" => 'I\'m looking for a mix of experience and value',
        ]);

        ExperienceLevel::create([
            "title" => 'Expert',
            "value" => '$$$',
            "description" => 'I\'m willing to pay higher rates for the most experienced caregivers',
        ]);
    }
}
