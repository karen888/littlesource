<?php

use Illuminate\Database\Seeder;
use App\Http\Models\UserInfo;
use App\Entities\Employee\EmployeeInfo;
use App\Entities\Criteria\Base;


class UsersForTest extends Seeder {
    use App\Http\Traits\GeolocationTrait;

    public function run()
    {
        $users = factory(App\User::class, 50)->create();

        $allCriteria = Base::allCriteria();

        foreach ($users as $item) {
            $postcode = random_int(2000, 2600);
            $location = $this->getGeoDataByPostcode($postcode);

            $data_criteria = [];
            $userInfo = new UserInfo;
            $userInfo->user_id = $item->id;
            $userInfo->country_id = 36;
            $userInfo->zipcode = $postcode;
            $userInfo->lat = $location->results[0]->geometry->location->lat;
            $userInfo->lng = $location->results[0]->geometry->location->lng;
            $userInfo->save();

            $employeeInfo = new EmployeeInfo;
            $employeeInfo->user_id = $item->id;
            $employeeInfo->max_rate = random_int(1, 99);
            $employeeInfo->available = true;
            $employeeInfo->gender = (random_int(0, 1)) ? 'male' : 'female';
            $employeeInfo->save();

            foreach ($allCriteria as $key=>$itm) {
                if ($key == 'qualification') {
                    $k = random_int(0, (count($itm['items'])-1));
                    for ($i = 0; $i <= $k; $i++) {
                        array_push($data_criteria, $itm['items'][$k]['value']);
                    }
                } else {
                    $rand = random_int(0, (count($itm['items'])-1));
                    array_push($data_criteria, $itm['items'][$rand]['value']);
                }

            }

            $item->user_criteria()->sync($data_criteria);
        }
    }
}
