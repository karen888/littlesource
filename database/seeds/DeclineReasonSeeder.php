<?php

use Illuminate\Database\Seeder;

class DeclineReasonSeeder extends Seeder {

    public function run()
    {
        DB::table('decline_reason')->delete();

        DB::table('decline_reason')->insert([
            ['reason' => 'Job is not a fit to my skills.'],
            ['reason' => 'Not interested in work described.'],
            ['reason' => 'Too busy on other jobs.'],
            ['reason' => 'Client has to little Little Ones experience.'],
            ['reason' => 'Proposed rate is to low.'],
            ['reason' => 'Spam.'],
            ['reason' => 'Client asked for free work.'],
            ['reason' => 'Client asked to work outside Little Ones.'],
            ['reason' => 'Other.'],
        ]);
    }
}
