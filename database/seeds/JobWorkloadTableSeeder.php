<?php

use Illuminate\Database\Seeder;

class JobWorkloadTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('job_workloads')->delete();

        DB::table('job_workloads')->insert([
            ['name' => 'as required', 'active' => true],
            ['name' => '10 hours/week or less', 'active' => true],
            ['name' => '20 hours/week or less', 'active' => true],
            ['name' => '30 hours/week or less', 'active' => true],
            ['name' => '40 hours/week or less', 'active' => true],
            ['name' => '40+ hours/week', 'active' => true],
        ]);
    }
}
