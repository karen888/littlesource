<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SkillsUsers extends Seeder {

	public function run()
	{
        DB::table('skills_users')->delete();

        DB::table('skills_users')->insert(Array(
            Array(
                'user_id' => 1,
                'skill_id' => 1,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 1,
                'skill_id' => 2,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 1,
                'skill_id' => 4,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 1,
                'skill_id' => 5,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 1,
                'skill_id' => 6,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 4,
                'skill_id' => 9,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 4,
                'skill_id' => 11,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 4,
                'skill_id' => 15,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 4,
                'skill_id' => 5,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 2,
                'skill_id' => 10,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 3,
                'skill_id' => 12,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 3,
                'skill_id' => 20,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
            Array(
                'user_id' => 3,
                'skill_id' => 22,
                'created_at' => '2015-03-04 10:31:15',
                'updated_at' => '2015-03-04 10:31:15',
            ),
        ));
    }
}
