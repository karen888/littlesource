<?php

use Illuminate\Database\Seeder;

class Qualifications extends Seeder {

    public function run()
    {
        DB::table('qualifications')->delete();

        DB::table('qualifications')->insert([
            ['name' => 'Certificate III in Early Childhood Education and Care'],
            ['name' => 'Diploma in Early Childhood Education and Care'],
            ['name' => 'Advanced Diploma in Early Childhood Education and Care'],
            ['name' => 'Bachelor of Education (Early Childhood)']
        ]);
    }
}
