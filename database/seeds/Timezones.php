<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Timezones extends Seeder {

	public function run()
	{
        DB::table('timezones')->delete();

        DB::table('timezones')->insert(Array(
            Array (
                'timezone_name' => 'Etc/UTC',
                'timezone_description' => 'UTC (Coordinated Universal Time)',                
            ),
            Array (
                'timezone_name' => 'Europe/London',
                'timezone_description' => 'UTC (Coordinated Universal Time) Dublin, Edinburgh, London',                
            ),
            Array (
                'timezone_name' => 'Africa/Casablanca',
                'timezone_description' => 'UTC (no DST) Tangiers, Casablanca',                
            ),
            Array (
                'timezone_name' => 'Europe/Lisbon',
                'timezone_description' => 'UTC+00:00 Lisbon',                
            ),
            Array (
                'timezone_name' => 'Africa/Algiers',
                'timezone_description' => 'UTC+01:00 Algeria',                
            ),
            Array (
                'timezone_name' => 'Europe/Berlin',
                'timezone_description' => 'UTC+01:00 Berlin, Stockholm, Rome, Bern, Brussels',                
            ),
            Array (
                'timezone_name' => 'Europe/Paris',
                'timezone_description' => 'UTC+01:00 Paris, Madrid',                
            ),
            Array (
                'timezone_name' => 'Europe/Prague',
                'timezone_description' => 'UTC+01:00 Prague, Warsaw',                
            ),
            Array (
                'timezone_name' => 'Europe/Athens',
                'timezone_description' => 'UTC+02:00 Athens, Helsinki, Istanbul',                
            ),
            Array (
                'timezone_name' => 'Africa/Cairo',
                'timezone_description' => 'UTC+02:00 Cairo',                
            ),
            Array (
                'timezone_name' => 'EET',
                'timezone_description' => 'UTC+02:00 Eastern Europe',                
            ),
            Array (
                'timezone_name' => 'Africa/Harare',
                'timezone_description' => 'UTC+02:00 Harare, Pretoria',                
            ),
            Array (
                'timezone_name' => 'Asia/Jerusalem',
                'timezone_description' => 'UTC+02:00 Israel',                
            ),
            Array (
                'timezone_name' => 'Asia/Baghdad',
                'timezone_description' => 'UTC+03:00 Baghdad, Kuwait, Nairobi, Riyadh',                
            ),
            Array (
                'timezone_name' => 'Europe/Minsk',
                'timezone_description' => 'UTC+03:00 Minsk',                
            ),
            Array (
                'timezone_name' => 'Europe/Moscow',
                'timezone_description' => 'UTC+03:00 Moscow, St. Petersburg, Volgograd',                
            ),
            Array (
                'timezone_name' => 'Asia/Tehran',
                'timezone_description' => 'UTC+03:30 Tehran',                
            ),
            Array (
                'timezone_name' => 'Asia/Tbilisi',
                'timezone_description' => 'UTC+04:00 Abu Dhabi, Muscat, Tbilisi, Kazan',                
            ),
            Array (
                'timezone_name' => 'Asia/Yerevan',
                'timezone_description' => 'UTC+04:00 Armenia',                
            ),
            Array (
                'timezone_name' => 'Asia/Kabul',
                'timezone_description' => 'UTC+04:30 Kabul',                
            ),
            Array (
                'timezone_name' => 'Asia/Karachi',
                'timezone_description' => 'UTC+05:00 Islamabad, Karachi',                
            ),
            Array (
                'timezone_name' => 'Asia/Yekaterinburg',
                'timezone_description' => 'UTC+05:00 Sverdlovsk',                
            ),
            Array (
                'timezone_name' => 'Asia/Tashkent',
                'timezone_description' => 'UTC+05:00 Tashkent',                
            ),
            Array (
                'timezone_name' => 'Asia/Calcutta',
                'timezone_description' => 'UTC+05:30 Mumbai, Kolkata, Chennai, New Delhi',                
            ),
            Array (
                'timezone_name' => 'Asia/Katmandu',
                'timezone_description' => 'UTC+05:45 Kathmandu, Nepal',                
            ),
            Array (
                'timezone_name' => 'Asia/Almaty',
                'timezone_description' => 'UTC+06:00 Almaty, Dhaka',                
            ),
            Array (
                'timezone_name' => 'Asia/Omsk',
                'timezone_description' => 'UTC+06:00 Omsk, Novosibirsk',                
            ),
            Array (
                'timezone_name' => 'Asia/Omsk',
                'timezone_description' => 'UTC+06:00 Tomsk',
            ),
            Array (
                'timezone_name' => 'Asia/Bangkok',
                'timezone_description' => 'UTC+07:00 Bangkok, Jakarta, Hanoi',                
            ),
            Array (
                'timezone_name' => 'Asia/Krasnoyarsk',
                'timezone_description' => 'UTC+07:00 Talnah, Krasnoyarsk',
            ),
            Array (
                'timezone_name' => 'Asia/Shanghai',
                'timezone_description' => 'UTC+08:00 Beijing, Chongqing, Urumqi',                
            ),
            Array (
                'timezone_name' => 'Australia/Perth',
                'timezone_description' => 'UTC+08:00 Hong Kong SAR, Perth, Singapore, Taipei',                
            ),
            Array (
                'timezone_name' => 'Asia/Irkutsk',
                'timezone_description' => 'UTC+08:00 Irkutsk (Lake Baikal)',                
            ),
            Array (
                'timezone_name' => 'Asia/Tokyo',
                'timezone_description' => 'UTC+09:00 Tokyo, Osaka, Sapporo, Seoul',                
            ),
            Array (
                'timezone_name' => 'Australia/Adelaide',
                'timezone_description' => 'UTC+09:30 Adelaide',                
            ),
            Array (
                'timezone_name' => 'Australia/Darwin',
                'timezone_description' => 'UTC+09:30 Darwin',                
            ),
            Array (
                'timezone_name' => 'Australia/Brisbane',
                'timezone_description' => 'UTC+10:00 Brisbane',                
            ),
            Array (
                'timezone_name' => 'Pacific/Guam',
                'timezone_description' => 'UTC+10:00 Guam, Port Moresby',                
            ),
            Array (
                'timezone_name' => 'Asia/Vladivostok',
                'timezone_description' => 'UTC+10:00 Magadan, Vladivostok',                
            ),
            Array (
                'timezone_name' => 'Australia/Sydney',
                'timezone_description' => 'UTC+10:00 Sydney, Melbourne',                
            ),
            Array (
                'timezone_name' => 'Asia/Yakutsk',
                'timezone_description' => 'UTC+10:00 Yakutsk (Lena River)',                
            ),
            Array (
                'timezone_name' => 'Australia/Hobart',
                'timezone_description' => 'UTC+11:00 Hobart',                
            ),
            Array (
                'timezone_name' => 'Pacific/Kwajalein',
                'timezone_description' => 'UTC+12:00 Eniwetok, Kwajalein',                
            ),
            Array (
                'timezone_name' => 'Pacific/Fiji',
                'timezone_description' => 'UTC+12:00 Fiji Islands, Marshall Islands',                
            ),
            Array (
                'timezone_name' => 'Asia/Kamchatka',
                'timezone_description' => 'UTC+12:00 Kamchatka',                
            ),
            Array (
                'timezone_name' => 'Asia/Magadan',
                'timezone_description' => 'UTC+12:00 Solomon Islands, New Caledonia',                
            ),
            Array (
                'timezone_name' => 'Pacific/Auckland',
                'timezone_description' => 'UTC+12:00 Wellington, Auckland',                
            ),
            Array (
                'timezone_name' => 'Pacific/Apia',
                'timezone_description' => 'UTC+13:00 Apia (Samoa)',                
            ),
            Array (
                'timezone_name' => 'Atlantic/Azores',
                'timezone_description' => 'UTC-01:00 Azores, Cape Verde Island',                
            ),
            Array (
                'timezone_name' => 'Atlantic/South_Georgia',
                'timezone_description' => 'UTC-02:00 Mid-Atlantic',                
            ),
            Array (
                'timezone_name' => 'America/Buenos_Aires',
                'timezone_description' => 'UTC-03:00 E Argentina (BA, DF, SC, TF)',                
            ),
            Array (
                'timezone_name' => 'America/Fortaleza',
                'timezone_description' => 'UTC-03:00 NE Brazil (MA, PI, CE, RN, PB)',                
            ),
            Array (
                'timezone_name' => 'America/Recife',
                'timezone_description' => 'UTC-03:00 Pernambuco',                
            ),
            Array (
                'timezone_name' => 'America/Sao_Paulo',
                'timezone_description' => 'UTC-03:00 S &amp; SE Brazil (GO, DF, MG, ES, RJ, SP, PR, SC, RS)',                
            ),
            Array (
                'timezone_name' => 'America/St_Johns',
                'timezone_description' => 'UTC-03:30 Newfoundland',                
            ),
            Array (
                'timezone_name' => 'America/Halifax',
                'timezone_description' => 'UTC-04:00 Atlantic Time (Canada)',                
            ),
            Array (
                'timezone_name' => 'America/La_Paz',
                'timezone_description' => 'UTC-04:00 La Paz',                
            ),
            Array (
                'timezone_name' => 'America/Caracas',
                'timezone_description' => 'UTC-04:30 Caracas',                
            ),
            Array (
                'timezone_name' => 'America/Bogota',
                'timezone_description' => 'UTC-05:00 Bogota, Lima',                
            ),
            Array (
                'timezone_name' => 'America/New_York',
                'timezone_description' => 'UTC-05:00 Eastern Time (US &amp; Canada)',                
            ),
            Array (
                'timezone_name' => 'America/Indiana/Indianapolis',
                'timezone_description' => 'UTC-05:00 Eastern Time - Indiana - most locations',                
            ),
            Array (
                'timezone_name' => 'America/Chicago',
                'timezone_description' => 'UTC-06:00 Central Time (US &amp; Canada)',                
            ),
            Array (
                'timezone_name' => 'America/Indiana/Knox',
                'timezone_description' => 'UTC-06:00 Eastern Time - Indiana - Starke County',                
            ),
            Array (
                'timezone_name' => 'America/Mexico_City',
                'timezone_description' => 'UTC-06:00 Mexico City, Tegucigalpa',                
            ),
            Array (
                'timezone_name' => 'America/Managua',
                'timezone_description' => 'UTC-06:00 Nicaragua',                
            ),
            Array (
                'timezone_name' => 'America/Regina',
                'timezone_description' => 'UTC-06:00 Saskatchewan',                
            ),
            Array (
                'timezone_name' => 'America/Phoe>nix',
                'timezone_description' => 'UTC-07:00 Arizona',                
            ),
            Array (
                'timezone_name' => 'America/Denver',
                'timezone_description' => 'UTC-07:00 Mountain Time (US &amp; Canada)',                
            ),
            Array (
                'timezone_name' => 'America/Los_Angeles',
                'timezone_description' => 'UTC-08:00 Pacific Time (US &amp; Canada); Los Angeles',                
            ),
            Array (
                'timezone_name' => 'America/Tijuana',
                'timezone_description' => 'UTC-08:00 Pacific Time (US &amp; Canada); Tijuana',                
            ),
            Array (
                'timezone_name' => 'America/Nome',
                'timezone_description' => 'UTC-09:00 Alaska',                
            ),
            Array (
                'timezone_name' => 'Pacific/Honolulu',
                'timezone_description' => 'UTC-10:00 Hawaii',                
            ),
            Array (
                'timezone_name' => 'Pacific/Midway',
                'timezone_description' => 'UTC-11:00 Midway Island, Samoa',                
            ),            
        ));
    }
}
