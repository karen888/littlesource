<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class PaymentPeriods extends Seeder {

	public function run()
	{
        //@TODO Make a command to generate this periods automatically.

        DB::table('payment_period')->delete();

        Carbon::setWeekStartsAt(Carbon::MONDAY);

        $monday = (Carbon::now()->isMonday()) ? Carbon::now() : Carbon::now()->startOfWeek();
        $periods = [];

        //8 weeks
        for($i = 1; $i<=8; $i++)
        {
            $period = [
                'id'         => $i,
                'start_date' => ($i == 1) ? $monday->toDateString() : $monday->addDay()->toDateString(),
                'end_date'   => $monday->addDays(6)->toDateString()
            ];
            array_push($periods, $period);
        }

        DB::table('payment_period')->insert($periods);
    }
}
