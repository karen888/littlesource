<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Timezone;

class TimezonesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('timezones')->truncate();
        Timezone::create(['name' => '(UTC+08:00) Perth', 'offset' => '(UTC+08:00)', 'php_timezone' => 'Australia/Perth']);
        Timezone::create(['name' => '(UTC+09:30) Adelaide', 'offset' => '(UTC+09:30)', 'php_timezone' => 'Australia/Adelaide']);
        Timezone::create(['name' => '(UTC+09:30) Darwin', 'offset' => '(UTC+09:30)', 'php_timezone' => 'Australia/Darwin']);
        Timezone::create(['name' => '(UTC+10:00) Brisbane', 'offset' => '(UTC+10:00) ', 'php_timezone' => 'Australia/Brisbane']);
        Timezone::create(['name' => '(UTC+10:00) Canberra', 'offset' => '(UTC+10:00)', 'php_timezone' => 'Australia/Canberra']);
        Timezone::create(['name' => '(UTC+10:00) Hobart', 'offset' => '(UTC+10:00)', 'php_timezone' => 'Australia/Hobart']);
        Timezone::create(['name' => '(UTC+10:00) Melbourne', 'offset' => '(UTC+10:00)', 'php_timezone' => 'Australia/Melbourne']);
        Timezone::create(['name' => '(UTC+10:00) Sydney', 'offset' => '(UTC+10:00)', 'php_timezone' => 'Australia/Sydney']);
    }
}
