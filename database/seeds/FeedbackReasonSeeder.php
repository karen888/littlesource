<?php

use Illuminate\Database\Seeder;

class FeedbackReasonSeeder extends Seeder {

    public function run()
    {
        DB::table('feedback_reason')->delete();
        DB::table('feedback_reason')->insert([
            ['reason' => 'Job Finished successfully'],
            ['reason' => 'Job Finished unsuccessfully'],
            ['reason' => 'Client had change of plans'],
            ['reason' => 'Unresponsive client/caregiver'],
            ['reason' => 'Other']
        ]);
    }
}
