<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Cards extends Seeder {

	public function run()
	{
        DB::table('cards')->delete();

        DB::table('cards')->insert(Array(
            Array(
                'user_id' => '1',
                'card_description' => 'my first card',
                'card_type' => 'Visa',
                'card_number' => '4220476512678309',
                'card_expdate' => '032017',
                'card_cvv2' => '111',
                'card_firstname' => 'Alex',
                'card_lastname' => 'AlexL',
                'card_country_code' => 'AI',
                'card_address1' => 'street X',
                'card_address2' => 'xxx',
                'card_city' => 'Talnah',
                'card_zip_code' => '123321',
                'card_status' => '1',
                'created_at' => '2015-04-13 03:07:45',
                'updated_at' => '2015-04-13 03:07:45',
            )
        ));
    }
}
