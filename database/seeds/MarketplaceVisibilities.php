<?php

use Illuminate\Database\Seeder;
use App\Http\Models\MarketplaceVisibility;

class MarketplaceVisibilities extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('marketplace_visibilities')->truncate();

        MarketplaceVisibility::create(['value' => 'Give my job maximum exposure (people can find it on Google)']);
        MarketplaceVisibility::create(['value' => 'Give my job minimum exposure (people won\'t find it on Google)']);
    }

    /**
     * Show only active Marketplace Visibilities on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('active', 1);
    }

    /**
     * Returns a customselect object of Active MarketplaceVisibilities;
     * @return [type] [description]
     */
    public static function customSelectListForActiveMarketplaceVisibilities() {
        $marketplaceVisibilities = static::active()->get();

        $customSelectPlaceholder = ['value' => '', 'text' => 'Please select...'];

        $customSelect = $marketplaceVisibilities->map(function ($item, $key) {
            return ['value' => $item->id, 'text' => $item->value];
        });

        $customSelect->prepend($customSelectPlaceholder);

        return $customSelect;
    }
}
