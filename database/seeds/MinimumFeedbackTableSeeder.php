<?php

use Illuminate\Database\Seeder;
use App\Http\Models\MinimumFeedback;

class MinimumFeedbackTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('minimum_feedbacks')->delete();

        MinimumFeedback::create(['value' => 'No Minimum Feedback Required']);
        MinimumFeedback::create(['value' => 'At least 5']);
        MinimumFeedback::create(['value' => 'At least 4.5']);
        MinimumFeedback::create(['value' => 'At Least 3']);
    }
}
