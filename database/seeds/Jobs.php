<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Http\Models\Category;
use App\Entities\Qualification\Qualification;


class Jobs extends Seeder {

    protected $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

	public function run()
	{
        $jobs = factory(App\Entities\Job\Job::class, 25)->create();

        foreach ($jobs as $item) {
            $qualifications = [];
            $qualification = Qualification::select('id')->get()->toArray();
            $key = array_rand($qualification);
            for ($i = 0; $i < $key; $i++) {
                $qualifications[] = $qualification[$i]['id'];
            }

            $item->qualification()->sync($qualifications);

            $categories = [];
            $category = Category::select('id')->get()->toArray();
            $key = array_rand($category);

            for ($j = 0; $j < $key; $j++) {
                $categories[] = $category[$j]['id'];
            }
            $item->category()->sync($categories);            

            $this->add_occupation($item->id);
        }
	}

    protected function add_occupation($job_id)
    {
        foreach($this->days as $idx=>$day) {
            DB::table('job_selected_days')->insert([
                [
                    'day' => $day,
                    'from' => 6,
                    'to' => 18,
                    'job_id' => $job_id,
                    'selected' => ($idx % 2 == 0),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ],
            ]);
        }
    }
}
