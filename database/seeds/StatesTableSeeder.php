<?php

use Illuminate\Database\Seeder;
use App\Http\Models\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states = [
            'ACT',
            'NT',
            'SA',
            'WA',
            'NSW',
            'QLD',
            'VIC',
            'TAS',
        ];

        foreach ($states as $state){
            State::create(['name' => $state]);
        }
    }
}
