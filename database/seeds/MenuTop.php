<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MenuTop extends Seeder {

	public function run()
	{
        DB::table('menu_top')->delete();

        DB::table('menu_top')->insert(Array(
            Array(
                'id' => 1,
                'name' => 'Home',
                'url' => '/home',
                'main_menu' => 0,
            ),
            Array(
                'id' => 2,
                'name' => 'My Jobs',
                'url' => '/home/jobs',
                'main_menu' => 0,
            ),
            Array(
                'id' => 3,
                'name' => 'Recruit',
                'url' => '/home/recruit',
                'main_menu' => 0,
            ),
            Array(
                'id' => 7,
                'name' => 'Manage My Team',
                'url' => '/home/contracts',
                'main_menu' => 0,
            ),
            Array(
                'id' => 8,
                'name' => 'Reports',
                'url' => '/home/reports',
                'main_menu' => 0,
            ),
            Array(
                'id' => 9,
                'name' => 'Messages',
                'url' => '/home/messages',
                'main_menu' => 0,
            ),
            Array(
                'id' => 11,
                'name' => 'Post Job',
                'url' => '/home/recruit/post-job',
                'main_menu' => 3,
            ),
            Array(
                'id' => 12,
                'name' => 'Jobs List',
                'url' => '/home/recruit/jobs-list',
                'main_menu' => 3,
            ),
            Array(
                'id' => 13,
                'name' => 'Offers',
                'url' => '/home/recruit/offers',
                'main_menu' => 3,
            ),
            Array(
                'id' => 14,
                'name' => 'Find Freelancer',
                'url' => '/home/recruit/find-freelancer',
                'main_menu' => 3,
            ),
            Array(
                'id' => 20,
                'name' => 'Find Job',
                'url' => '/home/jobs/find-job',
                'main_menu' => 2,
            ),
            Array(
                'id' => 21,
                'name' => 'Job Applications',
                'url' => '/home/jobs/applications',
                'main_menu' => 2,
            ),
            Array(
                'id' => 22,
                'name' => 'My Jobs',
                'url' => '/home/jobs/myjobs',
                'main_menu' => 2,
            ),
            Array(
                'id' => 30,
                'name' => 'My Freelancers',
                'url' => '/home/contracts/freelancers',
                'main_menu' => 7,
            ),
            Array(
                'id' => 31,
                'name' => 'Contracts',
                'url' => '/home/contracts/my-contracts',
                'main_menu' => 7,
            ),
            Array(
                'id' => 40,
                'name' => 'Transaction History',
                'url' => '/home/reports/transaction-history',
                'main_menu' => 8,
            ),
            Array(
                'id' => 41,
                'name' => 'Timesheet',
                'url' => '/home/reports/timesheet',
                'main_menu' => 8,
            ),
            Array(
                'id' => 50,
                'name' => 'Freelancers',
                'url' => '/home/reports/timesheet/freelancers',
                'main_menu' => 41,
            ),
            Array(
                'id' => 51,
                'name' => 'Jobs',
                'url' => '/home/reports/timesheet/jobs',
                'main_menu' => 41,
            ),
        ));

	}

}

