<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class JobApplies extends Seeder {

	public function run()
	{
        DB::table('job_applies')->delete();

        DB::table('job_applies')->insert(Array(
            Array(
                'id' => 1,
                'from_user_id' => 1,
                'job_id' => 2,
                'user_id' => 4,
                'initiated_by_fr' => 1,
                'cover_letter_freelancer' => 'hhhhhhhhhhhhhhhhhhhhhhhhhh',
                'job_apply_fixed_price' => 0,
                'job_apply_price' => '16.00',
                'active' => 1,
                'created_at' => '2015-03-04 10:44:47',
                'updated_at' => '2015-03-04 10:44:47',
            ),
            Array(
                'id' => 2,
                'from_user_id' => 1,
                'job_id' => 2,
                'user_id' => 2,
                'initiated_by_fr' => 1,
                'cover_letter_freelancer' => 'gggggggggggggggggggggggggggggggggg',
                'job_apply_fixed_price' => 0,
                'job_apply_price' => '14.00',
                'active' => 1,
                'created_at' => '2015-03-04 10:45:17',
                'updated_at' => '2015-03-04 10:45:17',
            ),
            Array(
                'id' => 6,
                'from_user_id' => 6,
                'job_id' => 6,
                'user_id' => 7,
                'initiated_by_fr' => 1,
                'cover_letter_freelancer' => 'im great',
                'job_apply_fixed_price' => 1,
                'job_apply_price' => '15.00',
                'active' => 1,
                'created_at' => '2015-03-26 04:40:14',
                'updated_at' => '2015-03-26 04:40:14',
            ),
        ));

	}

}
