<?php

use Illuminate\Database\Seeder;

class JobDurationTableSeeder extends Seeder
{
    public function run()
    {

        DB::table('job_durations')->delete();

        DB::table('job_durations')->insert([
            ['name' => 'as required', 'active' => true],
            ['name' => 'less than a week', 'active' => true],
            ['name' => 'less than 2 weeks', 'active' => true],
            ['name' => 'less than a month', 'active' => true],
            ['name' => 'less than 3 months', 'active' => true],
            ['name' => 'less than 6 months', 'active' => true],
            ['name' => 'over 6 months', 'active' => true],
        ]);
    }
}
