<?php

use Illuminate\Database\Seeder;
use App\Entities\SecurityQuestion\SecurityQuestionTypes;

class SecurityQuestionTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('security_question_types')->delete();
        SecurityQuestionTypes::create([
            'type' => 'What was your childhood nickname?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What is the name of your favorite childhood friend?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'In what city or town did your mother and father meet?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What is the middle name of your oldest child?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What was your favorite sport in high school?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What was your favorite food as a child?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What is the first name of the boy or girl that you first kissed?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What was the make and model of your first car?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What was the name of the hospital where you were born?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'Who is your childhood sports hero?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What school did you attend for sixth grade?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What was the last name of your third grade teacher?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'In what town was your first job?'
        ]);
        SecurityQuestionTypes::create([
            'type' => 'What was the name of the company where you had your first job?'
        ]);
    }
}
