<?php

use Illuminate\Database\Seeder;

class RatingOptionsSeeder extends Seeder {

    public function run()
    {
        DB::table('rating_options')->delete();
        DB::table('rating_options')->insert([
            [
                'text' => 'Working schedule as described?',
                'type' => 'client'
            ],
            [
                'text' => 'Punctuality of Payments',
                'type' => 'client'
            ],
            [
                'text' => 'House and working conditions',
                'type' => 'client'
            ],
            [
                'text' => 'Client communication',
                'type' => 'client'
            ],
            [
                'text' => 'Client treated you professionally',
                'type' => 'client'
            ],
            [
                'text' => 'Quality of Work',
                'type' => 'employee'
            ],
            [
                'text' => 'Punctuality and Reliability',
                'type' => 'employee'
            ],
            [
                'text' => 'Adherence to work schedule',
                'type' => 'employee'
            ],
            [
                'text' => 'Communication',
                'type' => 'employee'
            ],
            [
                'text' => 'Cooperation',
                'type' => 'employee'
            ],
        ]);
    }
}
