<?php

use Illuminate\Database\Seeder;
use App\Http\Models\MinimumExperience;

class MinimumExperienceTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('minimum_experiences')->delete();

        MinimumExperience::create(['value' => 'No Minimum Experience Required']);
        MinimumExperience::create(['value' => 'At least 1 year']);
        MinimumExperience::create(['value' => 'At least 2 years']);
        MinimumExperience::create(['value' => 'Over 3 Years']);
    }
}
