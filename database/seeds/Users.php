<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Users extends Seeder {
    use App\Http\Traits\GeolocationTrait;

	public function run()
	{
        $location = $this->getGeoDataByPostcode(2000);
        
        DB::table('users')->delete();

        DB::table('users')->insert(Array(
            Array(
                'id' => 1,
                'first_name' => 'Parent',
                'email' => 'parent@example.com',
                'password' => '$2y$10$uvAOhMFenM5OBbKx19Me1OnfLXvLCbQ3SmzGu8IDqH6eg/x/1UNA2', // parent
                'remember_token' => '',
                'user_path' => '86d17ea5e0ec751136dcdbd45df518ef',
                'user_photo_100' => '',
                'user_photo_64' => '',
                'timezone_id' => '0',
                'country_id' => '0',
                'user_city' => '',
                'created_at' => '2015-03-26 04:39:23',
                'updated_at' => '2015-03-26 04:41:45',
                'is_caregiver' => false,
                'user_available' => true
            ),
            Array(
                'id' => 2,
                'first_name' => 'Caregiver',
                'email' => 'caregiver@example.com',
                'password' => '$2y$10$PMnPyGzf07TB6Fk31z/36OsRdSDkM46A4S8rvm7td5SsNNVAf/lKm', // caregiver
                'remember_token' => '',
                'user_path' => 'd594b34695c0696e1407fa14c45f4462',
                'user_photo_100' => '',
                'user_photo_64' => '',
                'timezone_id' => '0',
                'country_id' => '0',
                'user_city' => '',
                'created_at' => '2015-03-26 04:39:23',
                'updated_at' => '2015-03-26 04:41:45',
                'is_caregiver' => true,
                'user_available' => true
            ),
        ));

        DB::table('employee_info')->insert([
            'user_id' => 2,
            'max_rate' => 20
        ]);

        DB::table('user_infos')->delete();
        DB::table('user_infos')->insert([
            [
                'user_id' => 1,
                'lat' => $location->results[0]->geometry->location->lat,
                'lng' => $location->results[0]->geometry->location->lng,
                'zipcode' => 2000
            ],
            [
                'user_id' => 2,
                'lat' => $location->results[0]->geometry->location->lat,
                'lng' => $location->results[0]->geometry->location->lng,
                'zipcode' => 2000
            ]
        ]);
        DB::table('notifications')->delete();
        DB::table('notifications')->insert([['user_id' => 1], ['user_id' => 2]]);
        DB::table('security_questions')->delete();
        DB::table('security_questions')->insert([
            ['user_id' => 1, 'type_id' => 1, 'answer' => 'answer'],
            ['user_id' => 2, 'type_id' => 1, 'answer' => 'answer']
        ]);
	}
}
