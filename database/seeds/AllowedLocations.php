<?php

use Illuminate\Database\Seeder;
use App\Http\Models\AllowedLocations as AllowedLocation;

class AllowedLocations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('allowed_locations')->delete();
        AllowedLocation::create(['value' => 'Any Location']);
        AllowedLocation::create(['value' => 'Up to 5 Km']);
        AllowedLocation::create(['value' => 'Up to 10 Km']);
        AllowedLocation::create(['value' => 'Up to 15 Km']);
        AllowedLocation::create(['value' => 'Up to 30 Km']);
        AllowedLocation::create(['value' => 'Up to 50 Km']);
    }
}
