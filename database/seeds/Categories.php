<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Http\Models\Category;

class Categories extends Seeder {

	public function run()
	{
        DB::table('categories')->delete();
        Category::create(['name' => 'Nannies & Babysitters', 'main_category' => '0' ]);
        Category::create(['name' => 'Tutoring & Lessons', 'main_category' => '0']);
        Category::create(['name' => 'Special Needs', 'main_category' => '0']);
    }
}
