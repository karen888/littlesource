<?php

use Illuminate\Database\Seeder;
use App\Http\Models\NumberOfHires;

class NumberOfHiresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('number_of_hires')->truncate();

        NumberOfHires::create(['value' => 'I want to hire one caregiver']);
        NumberOfHires::create(['value' => 'I want to hire two caregivers']);
        NumberOfHires::create(['value' => 'I want to hire three or more caregivers']);
    }
}
