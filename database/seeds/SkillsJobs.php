<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SkillsJobs extends Seeder {

	public function run()
	{
        DB::table('skills_jobs')->delete();

        DB::table('skills_jobs')->insert(Array(
            Array(
                'job_id' => 2,
                'skill_id' => 1,
                'created_at' => '2015-03-04 12:01:50',
                'updated_at' => '2015-03-04 12:01:50',
            ),
            Array(
                'job_id' => 2,
                'skill_id' => 2,
                'created_at' => '2015-03-04 12:01:50',
                'updated_at' => '2015-03-04 12:01:50',
            ),
            Array(
                'job_id' => 1,
                'skill_id' => 9,
                'created_at' => '2015-03-04 12:02:53',
                'updated_at' => '2015-03-04 12:02:53',
            ),
            Array(
                'job_id' => 1,
                'skill_id' => 11,
                'created_at' => '2015-03-04 12:02:53',
                'updated_at' => '2015-03-04 12:02:53',
            ),
        ));

    }
}
