<?php

use Illuminate\Database\Seeder;
use App\Entities\Criteria\ChildAge;
use App\Entities\Criteria\Service;
use App\Entities\Criteria\YearsExperience;
use App\Entities\Criteria\Skill;
use App\Entities\Criteria\OwnChildren;
use App\Entities\Criteria\HasOwnCar;
use App\Entities\Criteria\Pets;
use App\Entities\Criteria\Smoker;

class UpdateCriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('criterias')->where('type', 'App\Entities\Criteria\ChildAge')->delete();
        DB::table('criterias')->where('type', 'App\Entities\Criteria\Service')->delete();
        DB::table('criterias')->where('type', 'App\Entities\Criteria\YearsExperience')->delete();
        DB::table('criterias')->where('type', 'App\Entities\Criteria\Skill')->delete();
	    foreach ($this->getChildAges() as $item) {
	        ChildAge::create($item);
	    }

	    foreach ($this->getServices() as $item) {
	        Service::create($item);
	    }

        foreach ($this->getYearsExperience() as $item) {
            YearsExperience::create($item);
        }

        foreach ($this->getSkills() as $item) {
            Skill::create($item);
        }
    }



    public function getChildAges()
    {
        return [
            ['name' => '0 to 6 Months', 'description' => null, 'data' => null, 'default' => 1, 'order' => 1],
            ['name' => '7 Months to 2 Years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => '3 to 5 Years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => '5 to 8 Years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => '9 to 12 Years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => '13 Years +', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
        ];
    }

    public function getServices()
    {
        return [
            ['name' => 'Pickups/Drop Offs', 'description' => null, 'data' => ['image' => 'pickups'], 'default' => 0, 'order' => 1],
            ['name' => 'Meal Preparation', 'description' => null, 'data' => ['image' => 'preparation'], 'default' => 0, 'order' => 2],
            ['name' => 'Housekeeping', 'description' => null, 'data' => ['image' => 'housekeeping'], 'default' => 0, 'order' => 3],
            ['name' => 'Errands', 'description' => null, 'data' => ['image' => 'errands'], 'default' => 0, 'order' => 4],
            ['name' => 'Travel', 'description' => null, 'data' => ['image' => 'travel'], 'default' => 0, 'order' => 5],
            ['name' => 'Overnight Care', 'description' => null, 'data' => ['image' => 'overnight'], 'default' => 0, 'order' => 6],
            ['name' => 'Will Care for Sick Children', 'description' => null, 'data' => ['image' => 'sick'], 'default' => 0, 'order' => 7],
        ];
    }

    public function getYearsExperience()
    {
        return [
            ['name' => 'Less Than 1 Year', 'description' => null, 'data' =>  null, 'default' => 0, 'order' => 1],
            ['name' => '1 - 3 Years', 'description' => null, 'data' =>  null, 'default' => 0, 'order' => 2],
            ['name' => '3 -5 Years', 'description' => null, 'data' =>  null, 'default' => 0, 'order' => 3],
            ['name' => '5 Years +', 'description' => null, 'data' =>  null, 'default' => 0, 'order' => 4],
        ];
    }

   public function getSkills()
    {
        return [
            ['name' => 'First Aid Training', 'description' => null, 'data' => ['image' => 'aid'], 'default' => 0, 'order' => 1],
            ['name' => 'Water Safety', 'description' => null, 'data' => ['image' => 'safety'], 'default' => 0, 'order' => 2],
            ['name' => 'Tutoring', 'description' => null, 'data' => ['image' => 'tutoring'], 'default' => 0, 'order' => 3],
            ['name' => 'Special Needs Care', 'description' => null, 'data' => ['image' => 'needscare'], 'default' => 0, 'order' => 4],
            ['name' => 'Languages', 'description' => null, 'data' => ['image' => 'languages'], 'default' => 0, 'order' => 6],
        ];
    }
}