<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Settings extends Seeder {

	public function run()
	{
        DB::table('settings')->delete();

        DB::table('settings')->insert(Array(
            Array(
                'id' => 1,
                'setting_name' => 'jscss',
                'setting_values' => '{"status":0,"cache":"123","js_files":["app.js"],"css_files":["app.css"]}', // 0 - no cache, 1 - cache, 2 - start new cache
                'status' => '1',
            ),
        ));

    }
}
