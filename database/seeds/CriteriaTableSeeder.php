<?php

use Illuminate\Database\Seeder;
use App\Entities\Criteria\LocationRange;
use App\Entities\Criteria\CareType;
use App\Entities\Criteria\ChildAge;
use App\Entities\Criteria\Availability;
use App\Entities\Criteria\AgeRange;
use App\Entities\Criteria\EmploymentType;
use App\Entities\Criteria\Qualification;
use App\Entities\Criteria\Gender;
use App\Entities\Criteria\YearsExperience;
use App\Entities\Criteria\DriversLicence;
use App\Entities\Criteria\OwnChildren;
use App\Entities\Criteria\HasOwnCar;
use App\Entities\Criteria\Pets;
use App\Entities\Criteria\Smoker;
use App\Entities\Criteria\Skill;
use App\Entities\Criteria\Service;
use App\Entities\Criteria\Interest;
use App\Entities\Criteria\MaxPerBooking;

class CriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('criterias_users')->truncate();
        DB::table('criterias')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach ($this->getLocationRanges() as $item) {
            LocationRange::create($item);
        }

        foreach ($this->getCareTypes() as $item) {
            CareType::create($item);
        }

        foreach ($this->getChildAges() as $item) {
            ChildAge::create($item);
        }

        foreach ($this->getAvailabilities() as $item) {
            Availability::create($item);
        }

        foreach ($this->getAgeRanges() as $item) {
            AgeRange::create($item);
        }

        foreach ($this->getEmploymentTypes() as $item) {
            EmploymentType::create($item);
        }

        foreach ($this->getQualifications() as $item) {
            Qualification::create($item);
        }

        foreach ($this->getGenders() as $item) {
            Gender::create($item);
        }

        foreach ($this->getYearsExperience() as $item) {
            YearsExperience::create($item);
        }

        foreach ($this->getDriversLicences() as $item) {
            DriversLicence::create($item);
        }

        foreach ($this->getBinaries() as $item) {
            OwnChildren::create($item);
            HasOwnCar::create($item);
            Pets::create($item);
            Smoker::create($item);
        }

        foreach ($this->getSkills() as $item) {
            Skill::create($item);
        }

        foreach ($this->getServices() as $item) {
            Service::create($item);
        }

        foreach ($this->getInterests() as $item) {
            Interest::create($item);
        }

        MaxPerBooking::create([
            'name' => 'No Maximum',
            'data' => -1,
            'default' => 1,
        ]);

        for ($i = 1; $i <= 10; ++$i) {
            MaxPerBooking::create([
                'name' => $i == 1 ? $i . ' child' : $i . ' children',
                'data' => $i
            ]);
        }
    }

    public function getLocationRanges()
    {
        return [
            ['name' => 'No Maximum', 'description' => null, 'data' => -1, 'default' => 1, 'order' => 1],
            ['name' => '5km', 'description' => null, 'data' => 5, 'default' => 0, 'order' => 2],
            ['name' => '10km', 'description' => null, 'data' => 10, 'default' => 0, 'order' => 3],
            ['name' => '15km', 'description' => null, 'data' => 15, 'default' => 0, 'order' => 4],
            ['name' => '20km', 'description' => null, 'data' => 20, 'default' => 0, 'order' => 5],
            ['name' => '30km', 'description' => null, 'data' => 30, 'default' => 0, 'order' => 6],
            ['name' => '50km', 'description' => null, 'data' => 50, 'default' => 0, 'order' => 7],
            ['name' => '100km', 'description' => null, 'data' => 100, 'default' => 0, 'order' => 8],
        ];
    }

    public function getCareTypes()
    {
        return [
            ['name' => 'Need-it-now care', 'description' => null, 'data' => null, 'default' => 1, 'order' => 1],
            ['name' => 'Regular Babysitting', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => 'Part-time Nanny', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => 'Full-time Nanny', 'description' => null, 'data' => null, 'default' => 0, 'order' => 4],
        ];
    }

    public function getChildAges()
    {
        return [
            ['name' => 'Zero to 6 months', 'description' => null, 'data' => null, 'default' => 1, 'order' => 1],
            ['name' => '7 months to 2 years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => '3 years to 5 years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => '6 years to 8 years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => '9 years to 12 years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => '13 years and above', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
        ];
    }

    public function getAvailabilities()
    {
        return [
            ['name' => 'Monday', 'description' => null, 'data' => ['day' => 1, 'time' => []], 'default' => 0, 'order' => 1],
            ['name' => 'Tuesday', 'description' => null, 'data' => ['day' => 2, 'time' => []], 'default' => 0, 'order' => 2],
            ['name' => 'Wednesday', 'description' => null, 'data' => ['day' => 3, 'time' => []], 'default' => 0, 'order' => 3],
            ['name' => 'Thursday', 'description' => null, 'data' => ['day' => 4, 'time' => []], 'default' => 0, 'order' => 4],
            ['name' => 'Friday', 'description' => null, 'data' => ['day' => 5, 'time' => []], 'default' => 0, 'order' => 5],
            ['name' => 'Saturday', 'description' => null, 'data' => ['day' => 6, 'time' => []], 'default' => 0, 'order' => 6],
            ['name' => 'Sunday', 'description' => null, 'data' => ['day' => 7, 'time' => []], 'default' => 0, 'order' => 7],
        ];
    }

    public function getAgeRanges()
    {
        return [
            ['name' => '18—25', 'description' => null, 'data' => ['from' => 18, 'to' => 25], 'default' => 0, 'order' => 1],
            ['name' => '26—30', 'description' => null, 'data' => ['from' => 26, 'to' => 30], 'default' => 0, 'order' => 2],
            ['name' => '31—40', 'description' => null, 'data' => ['from' => 31, 'to' => 40], 'default' => 0, 'order' => 3],
            ['name' => '40+', 'description' => null, 'data' => ['from' => 41, 'to' => 99], 'default' => 0, 'order' => 4],
        ];
    }

    public function getEmploymentTypes()
    {
        return [
            ['name' => 'Emergency', 'description' => 'less than 3 hours notice', 'data' => null, 'default' => 0, 'order' => 1],
            ['name' => 'Casual', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => 'Part-time', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => 'Full-time', 'description' => null, 'data' => null, 'default' => 0, 'order' => 4],
        ];
    }

    public function getQualifications()
    {
        return [
            ['name' => 'Certificate III in Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 107],
            ['name' => 'Diploma in Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 122],
            ['name' => 'Advanced Diploma in Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 6],
            ['name' => 'Bachelor of Education (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 41],
            ['name' => 'Advanced Diploma of Children\'s Services', 'description' => null, 'data' => null, 'default' => 0, 'order' => 5],
            ['name' => 'AMI Montessori Diploma Early Childhood (3-6)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 6],
            ['name' => 'Associate Degree in Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 7],
            ['name' => 'Associate Degree in Early Years Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 8],
            ['name' => 'Associate Degree of Early Years Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 9],
            ['name' => 'Associate Degree of Education (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 10],
            ['name' => 'Associate Diploma of Social Science (Child Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 11],
            ['name' => 'Associate Diploma of Social Science (Child Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 12],
            ['name' => 'Bachelor of Arts (Early Childhood Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 13],
            ['name' => 'Bachelor of Arts (Psychology, Children and Family Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 14],
            ['name' => 'Bachelor of Arts/Bachelor of Education (Primary/Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 15],
            ['name' => 'Bachelor of Arts/Bachelor of Teaching (Birth - 12 years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 16],
            ['name' => 'Bachelor of Child and Family Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 17],
            ['name' => 'Bachelor of Child and Family Studies / Bachelor of Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 18],
            ['name' => 'Bachelor of Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 19],
            ['name' => 'Bachelor of Early Childhood (Education and Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 20],
            ['name' => 'Bachelor of Early Childhood and Primary Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 21],
            ['name' => 'Bachelor of Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 22],
            ['name' => 'Bachelor of Early Childhood Education (Birth to Five Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 23],
            ['name' => 'Bachelor of Early Childhood Education (Honours)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 24],
            ['name' => 'Bachelor of Early Childhood Education (In-service)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 25],
            ['name' => 'Bachelor of Early Childhood Education and Care (Birth-5)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 26],
            ['name' => 'Bachelor of Early Childhood Learning', 'description' => null, 'data' => null, 'default' => 0, 'order' => 27],
            ['name' => 'Bachelor of Early Childhood Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 28],
            ['name' => 'Bachelor of Early Childhood Teaching', 'description' => null, 'data' => null, 'default' => 0, 'order' => 29],
            ['name' => 'Bachelor of Early Childhood Teaching (Birth to Five Years) / Bachelor of Nursing', 'description' => null, 'data' => null, 'default' => 0, 'order' => 30],
            ['name' => 'Bachelor of Early Childhood Teaching (Birth to Five Years) / Bachelor of Speech and Hearing Services', 'description' => null, 'data' => null, 'default' => 0, 'order' => 31],
            ['name' => 'Bachelor of Early Learning', 'description' => null, 'data' => null, 'default' => 0, 'order' => 32],
            ['name' => 'Bachelor of Early Years Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 33],
            ['name' => 'Bachelor of Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 34],
            ['name' => 'Bachelor of Education (Birth - 5 Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 35],
            ['name' => 'Bachelor of Education (Birth - Year 6)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 36],
            ['name' => 'Bachelor of Education (Birth to Five)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 37],
            ['name' => 'Bachelor of Education (Birth to Twelve Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 38],
            ['name' => 'Bachelor of Education (Conversion)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 39],
            ['name' => 'Bachelor of Education (Conversion) Early Childhood Specialisation', 'description' => null, 'data' => null, 'default' => 0, 'order' => 40],
            ['name' => 'Bachelor of Education (Early Childhood / Primary)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 41],
            ['name' => 'Bachelor of Education (Early Childhood 0-8)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 42],
            ['name' => 'Bachelor of Education (Early Childhood and Care: 0-8 Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 43],
            ['name' => 'Bachelor of Education (Early Childhood and Primary)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 44],
            ['name' => 'Bachelor of Education (Early Childhood and Primary) (Honours)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 45],
            ['name' => 'Bachelor of Education (Early Childhood and Special Education)/Bachelor of Disability Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 46],
            ['name' => 'Bachelor of Education (Early Childhood Birth - 5 years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 47],
            ['name' => 'Bachelor of Education (Early Childhood Education and Care: 0-8 years) / Bachelor of Arts', 'description' => null, 'data' => null, 'default' => 0, 'order' => 48],
            ['name' => 'Bachelor of Education (Early Childhood Education and Care: 0-8 years) / Bachelor of Arts (Honours)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 49],
            ['name' => 'Bachelor of Education (Early Childhood Education and Care: 0-8 years) / Bachelor of Behavioural Science', 'description' => null, 'data' => null, 'default' => 0, 'order' => 50],
            ['name' => 'Bachelor of Education (Early Childhood Education and Care: 0-8 years) / Bachelor of Science', 'description' => null, 'data' => null, 'default' => 0, 'order' => 51],
            ['name' => 'Bachelor of Education (Early Childhood Education and Care)(Inservice)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 52],
            ['name' => 'Bachelor of Education (Early Childhood Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 53],
            ['name' => 'Bachelor of Education (Early Childhood Education) (Birth to 12)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 54],
            ['name' => 'Bachelor of Education (Early Childhood Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 55],
            ['name' => 'Bachelor of Education (Early Childhood Teaching)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 56],
            ['name' => 'Bachelor of Education (Early Childhood) - Graduate', 'description' => null, 'data' => null, 'default' => 0, 'order' => 58],
            ['name' => 'Bachelor of Education (Early Childhood) (Honours)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 59],
            ['name' => 'Bachelor of Education (Early Childhood) / Bachelor of Arts', 'description' => null, 'data' => null, 'default' => 0, 'order' => 60],
            ['name' => 'Bachelor of Education (Early Childhood/Primary)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 61],
            ['name' => 'Bachelor of Education (Early Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 62],
            ['name' => 'Bachelor of Education (Honours) in Early Years and Primary Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 63],
            ['name' => 'Bachelor of Education (Honours) in Early Years Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 64],
            ['name' => 'Bachelor of Education (Inservice)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 65],
            ['name' => 'Bachelor of Education (K-7)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 66],
            ['name' => 'Bachelor of Education (Kindergarten through Primary)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 67],
            ['name' => 'Bachelor of Education (Pre-service Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 68],
            ['name' => 'Bachelor of Education (Pre-service) - Graduate - Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 69],
            ['name' => 'Bachelor of Education (Primary and Early Childhood Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 70],
            ['name' => 'Bachelor of Education (Primary Teaching)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 71],
            ['name' => 'Bachelor of Education (Primary) + Graduate Certificate in Education (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 72],
            ['name' => 'Bachelor of Education (Primary) with specialisation in Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 73],
            ['name' => 'Bachelor of Education (Special Needs) pathway Bachelor of Education (Early Childhood Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 74],
            ['name' => 'Bachelor of Education (Teaching) - Early Childhood Education specialisation', 'description' => null, 'data' => null, 'default' => 0, 'order' => 75],
            ['name' => 'Bachelor of Education Early Childhood Teaching', 'description' => null, 'data' => null, 'default' => 0, 'order' => 76],
            ['name' => 'Bachelor of Education Early Childhood/Primary (Birth - 12 years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 77],
            ['name' => 'Bachelor of Education Early Childhood/Primary (Honours) (Birth - 12 years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 78],
            ['name' => 'Bachelor of Education in Early Childhood and Primary Teaching', 'description' => null, 'data' => null, 'default' => 0, 'order' => 79],
            ['name' => 'Bachelor of Education in Early Childhood Teaching (Birth-8)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 80],
            ['name' => 'Bachelor of Education in Early Childhood Teaching (Conversion)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 81],
            ['name' => 'Bachelor of Education Professional Development (Early Childhood Education Major)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 82],
            ['name' => 'Bachelor of Education The Early Years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 83],
            ['name' => 'Bachelor of Education The Early Years (Deanâ€™s Scholar)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 84],
            ['name' => 'Bachelor of Education The Early Years (Honours)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 85],
            ['name' => 'Bachelor of General Education Studies exit from Bachelor of Education (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 86],
            ['name' => 'Bachelor of Human Services (Child and Family Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 87],
            ['name' => 'Bachelor of Learning Management (Early Childhood Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 88],
            ['name' => 'Bachelor of Primary Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 89],
            ['name' => 'Bachelor of Social Science', 'description' => null, 'data' => null, 'default' => 0, 'order' => 90],
            ['name' => 'Bachelor of Social Science (Children and Family Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 91],
            ['name' => 'Bachelor of Social Science (Major or Minor in Children and Family Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 92],
            ['name' => 'Bachelor of Teaching (Birth to Five Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 93],
            ['name' => 'Bachelor of Teaching (Birth to five years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 94],
            ['name' => 'Bachelor of Teaching (Early Childhood and Primary)(Honours)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 95],
            ['name' => 'Bachelor of Teaching (Early Childhood Communities)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 96],
            ['name' => 'Bachelor of Teaching (Early Childhood Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 97],
            ['name' => 'Bachelor of Teaching (Early Childhood Services)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 98],
            ['name' => 'Bachelor of Teaching (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 99],
            ['name' => 'Bachelor of Teaching (Primary) / Bachelor of Early Childhood Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 100],
            ['name' => 'Bachelor of Teaching (Primary) with specialisation in Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 101],
            ['name' => 'Bachelor of Teaching and Learning (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 102],
            ['name' => 'Bachelor of Teaching and Learning (Pre-service)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 103],
            ['name' => 'Bachelor of Teaching and Learning In-service (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 104],
            ['name' => 'Certificate III in Children\'s Services', 'description' => null, 'data' => null, 'default' => 0, 'order' => 105],
            ['name' => 'Certificate III in Education (Teacher Aide/Assistant)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 107],
            ['name' => 'Certificate III in Education Support', 'description' => null, 'data' => null, 'default' => 0, 'order' => 108],
            ['name' => 'Certificate in Applied Social Science (Child Care Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 109],
            ['name' => 'Certificate in Applied Social Science (Child Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 110],
            ['name' => 'Certificate in Child Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 111],
            ['name' => 'Certificate in Child Care Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 112],
            ['name' => 'Certificate in Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 113],
            ['name' => 'Certificate in Early Childhood Teaching', 'description' => null, 'data' => null, 'default' => 0, 'order' => 114],
            ['name' => 'Certificate IV in Education (Aboriginal and Torres Strait Islander)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 115],
            ['name' => 'Certificate IV in Education (Teacher Aide/Assistant)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 116],
            ['name' => 'Certificate IV in Education Support', 'description' => null, 'data' => null, 'default' => 0, 'order' => 117],
            ['name' => 'Child Care Certificate', 'description' => null, 'data' => null, 'default' => 0, 'order' => 118],
            ['name' => 'Child Care Practices Course', 'description' => null, 'data' => null, 'default' => 0, 'order' => 119],
            ['name' => 'Course of Study in The Sydney Kindergarten Training College', 'description' => null, 'data' => null, 'default' => 0, 'order' => 120],
            ['name' => 'Diploma (Child Care and Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 121],
            ['name' => 'Diploma of Arts (Child Care Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 122],
            ['name' => 'Diploma of Arts (Child Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 123],
            ['name' => 'Diploma of Child Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 124],
            ['name' => 'Diploma of Child Care (or Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 125],
            ['name' => 'Diploma of Child Care and Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 126],
            ['name' => 'Diploma of Child Care Services', 'description' => null, 'data' => null, 'default' => 0, 'order' => 127],
            ['name' => 'Diploma of Childcare and Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 128],
            ['name' => 'Diploma of Children\'s Services', 'description' => null, 'data' => null, 'default' => 0, 'order' => 129],
            ['name' => 'Diploma of Children\'s Services (0 - 5 Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 130],
            ['name' => 'Diploma of Children\'s Services (0 - 5 years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 131],
            ['name' => 'Diploma of Children\'s Services (Centre-based Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 132],
            ['name' => 'Diploma of Children\'s Services (Centre-based/Home-based/Family Day Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 133],
            ['name' => 'Diploma of Children\'s Services (Child Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 134],
            ['name' => 'Diploma of Children\'s Services (Early Childhood Education and Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 135],
            ['name' => 'Diploma of Children\'s Services (Home-based)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 136],
            ['name' => 'Diploma of Children\'s Services (Home-based/Family Day Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 137],
            ['name' => 'Diploma of Community Services (Child Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 138],
            ['name' => 'Diploma of Community Services (Children\'s Services)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 139],
            ['name' => 'Diploma of Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 140],
            ['name' => 'Diploma of Early Childhood and Primary Education (Montessori)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 141],
            ['name' => 'Diploma of Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 142],
            ['name' => 'Diploma of Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 143],
            ['name' => 'Diploma of Early Childhood Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 144],
            ['name' => 'Diploma of Education (Aboriginal and Torres Strait Islander)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 145],
            ['name' => 'Diploma of Education (Child Care)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 146],
            ['name' => 'Diploma of Education (Montessori Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 147],
            ['name' => 'Diploma of Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 148],
            ['name' => 'Diploma of Education Support', 'description' => null, 'data' => null, 'default' => 0, 'order' => 149],
            ['name' => 'Diploma of Montessori Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 150],
            ['name' => 'Diploma of Social Science (Child Care Services)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 151],
            ['name' => 'Diploma of Teacher Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 152],
            ['name' => 'Diploma of Teaching (Early Childhood Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 153],
            ['name' => 'Diploma of Teaching (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 154],
            ['name' => 'Diploma of the Kindergarten Teacher\'s College', 'description' => null, 'data' => null, 'default' => 0, 'order' => 155],
            ['name' => 'Enrolled Nurse (Mothercraft)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 156],
            ['name' => 'Graduate Certificate in Early Childhood Care and Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 157],
            ['name' => 'Graduate Certificate in Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 158],
            ['name' => 'Graduate Certificate in Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 159],
            ['name' => 'Graduate Certificate in Education (early childhood leadership)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 160],
            ['name' => 'Graduate Certificate in Education (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 161],
            ['name' => 'Graduate Certificate in Education Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 162],
            ['name' => 'Graduate Certificate of Education (Early Childhood Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 163],
            ['name' => 'Graduate Diploma Child and Family Studies', 'description' => null, 'data' => null, 'default' => 0, 'order' => 164],
            ['name' => 'Graduate Diploma in Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 165],
            ['name' => 'Graduate Diploma in Early Childhood Care and Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 166],
            ['name' => 'Graduate Diploma in Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 167],
            ['name' => 'Graduate Diploma in Early Childhood Teaching', 'description' => null, 'data' => null, 'default' => 0, 'order' => 168],
            ['name' => 'Graduate Diploma in Education (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 169],
            ['name' => 'Graduate Diploma in Education (Early Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 170],
            ['name' => 'Graduate Diploma in Education Preparatory to year three', 'description' => null, 'data' => null, 'default' => 0, 'order' => 171],
            ['name' => 'Graduate Diploma in Education Studies (ECE)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 172],
            ['name' => 'Graduate Diploma in Education: Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 173],
            ['name' => 'Graduate Diploma in Social Science (Children and Family Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 174],
            ['name' => 'Graduate Diploma in Teaching (Early Childhood Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 175],
            ['name' => 'Graduate Diploma in Teaching (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 176],
            ['name' => 'Graduate Diploma of Advanced Studies in Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 177],
            ['name' => 'Graduate Diploma of Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 178],
            ['name' => 'Graduate Diploma of Early Childhood Teaching', 'description' => null, 'data' => null, 'default' => 0, 'order' => 179],
            ['name' => 'Graduate Diploma of Education - Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 180],
            ['name' => 'Graduate Diploma of Education (Early Childhood Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 181],
            ['name' => 'Graduate Diploma of Education (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 182],
            ['name' => 'Graduate Diploma of Education (Montessori) with a focus on the 3-6 years age group', 'description' => null, 'data' => null, 'default' => 0, 'order' => 183],
            ['name' => 'Graduate Diploma of Education Studies (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 184],
            ['name' => 'Graduate Diploma of Teaching', 'description' => null, 'data' => null, 'default' => 0, 'order' => 185],
            ['name' => 'Graduate Diploma of Teaching (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 186],
            ['name' => 'Level 3 Diploma in Child Care and Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 187],
            ['name' => 'Level 3 Diploma in Child Care and Education (Northern Ireland) (VRQ)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 188],
            ['name' => 'Level 3 Diploma in Child Care and Education (VRQ)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 189],
            ['name' => 'Level 3 National Certificate in Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 190],
            ['name' => 'Level 5 Certificate in Childcare', 'description' => null, 'data' => null, 'default' => 0, 'order' => 191],
            ['name' => 'Level 5 Certificate in Early Childhood Care and Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 192],
            ['name' => 'Level 5 National Certificate in Early Childhood Education and Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 193],
            ['name' => 'Level 6 Advanced Certificate in Early Childhood Care and Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 194],
            ['name' => 'Level 6 Advanced Certificate in Supervision in Childcare', 'description' => null, 'data' => null, 'default' => 0, 'order' => 195],
            ['name' => 'Master of Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 196],
            ['name' => 'Master of Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 197],
            ['name' => 'Master of Early Childhood Education (Pathway 2)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 198],
            ['name' => 'Master of Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 199],
            ['name' => 'Master of Education (early childhood leadership)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 200],
            ['name' => 'Master of Education (Early Childhood Teaching)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 201],
            ['name' => 'Master of Education (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 202],
            ['name' => 'Master of Education with a major in Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 203],
            ['name' => 'Master of Human Services (Childhood Studies)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 204],
            ['name' => 'Master of Learning and Development (with a specialisation in Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 205],
            ['name' => 'Master of Teaching', 'description' => null, 'data' => null, 'default' => 0, 'order' => 206],
            ['name' => 'Master of Teaching - Early Childhood', 'description' => null, 'data' => null, 'default' => 0, 'order' => 207],
            ['name' => 'Master of Teaching - Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 208],
            ['name' => 'Master of Teaching - Primary /Early Childhood Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 209],
            ['name' => 'Master of Teaching (Birth - 12 Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 210],
            ['name' => 'Master of Teaching (Birth - 5 Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 211],
            ['name' => 'Master of Teaching (Birth to Five Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 212],
            ['name' => 'Master of Teaching (Early Childhood and Primary)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 213],
            ['name' => 'Master of Teaching (Early Childhood Education)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 214],
            ['name' => 'Master of Teaching (Early Childhood)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 215],
            ['name' => 'Master of Teaching (Early Years)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 216],
            ['name' => 'Master of Teaching (K-7)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 217],
            ['name' => 'Master of Teaching Conversion (K-7)', 'description' => null, 'data' => null, 'default' => 0, 'order' => 218],
            ['name' => 'Master of Teaching in Early Years and Primary Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 219],
            ['name' => 'Master of Teaching in Early Years Education', 'description' => null, 'data' => null, 'default' => 0, 'order' => 220],
            ['name' => 'Mother Craft Nurse', 'description' => null, 'data' => null, 'default' => 0, 'order' => 221],
            ['name' => 'Teacher accreditation with the NSW Institute of Teachers AND a primary teaching qualification AND an approved education and care diploma or higher qualification (e.g. approved graduate diploma) published on ACECQA\'s qualification lists', 'description' => null, 'data' => null, 'default' => 0, 'order' => 222],
            ['name' => 'Teacher registration with Teacher Registration Board of Northern Territory AND a primary teaching qualification AND an approved education and care diploma or higher qualification (e.g. approved graduate diploma) published on ACECQA\'s qualification list', 'description' => null, 'data' => null, 'default' => 0, 'order' => 223],
            ['name' => 'Teacher registration with Teacher Registration Board of South Australia AND a primary teaching qualification AND an approved education and care diploma or higher qualification (e.g. approved graduate diploma) published on ACECQA\'s qualification list', 'description' => null, 'data' => null, 'default' => 0, 'order' => 224],
            ['name' => 'Teacher registration with Teacher Registration Board of Western Australia AND a primary teaching qualification AND an approved education and care diploma or higher qualification (e.g. approved graduate diploma) published on ACECQA\'s qualification list', 'description' => null, 'data' => null, 'default' => 0, 'order' => 225],
            ['name' => 'Teacher registration with the ACT Teacher Quality Institute AND a primary teaching qualification AND an approved education and care diploma or higher qualification (e.g. approved graduate diploma) published on ACECQA\'s qualification lists', 'description' => null, 'data' => null, 'default' => 0, 'order' => 226],
            ['name' => 'Teacher registration with the Queensland College of Teachers AND a primary teaching qualification AND an approved education and care diploma or higher qualification (e.g. approved graduate diploma) published on ACECQA\'s qualification lists', 'description' => null, 'data' => null, 'default' => 0, 'order' => 227],
            ['name' => 'Teacher registration with the Teachers Registration Board of Tasmania AND a primary teaching qualification AND an approved education and care diploma or higher qualification (e.g. approved graduate diploma) published on ACECQA\'s qualification lists', 'description' => null, 'data' => null, 'default' => 0, 'order' => 228],
            ['name' => 'Teacher registration with the Victorian Institute of Teaching AND a primary teaching qualification AND an approved education and care diploma or higher qualification (e.g. approved graduate diploma) published on ACECQA\'s qualification lists', 'description' => null, 'data' => null, 'default' => 0, 'order' => 229],
        ];
    }

    public function getGenders()
    {
        return [
            ['name' => 'Male', 'description' => null, 'data' => null, 'default' => 0, 'order' => 1],
            ['name' => 'Female', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => 'Transgender', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => 'Other', 'description' => null, 'data' => null, 'default' => 0, 'order' => 4],
            ['name' => 'Prefer Not to Say', 'description' => null, 'data' => null, 'default' => 0, 'order' => 5],
        ];
    }

    public function getYearsExperience()
    {
        return [
            ['name' => 'Less than 1 Year', 'description' => null, 'data' => null, 'default' => 0, 'order' => 1],
            ['name' => 'Less than 2 Years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => 'Less than 5 Years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => '5+ Years', 'description' => null, 'data' => null, 'default' => 0, 'order' => 4],
        ];
    }

    public function getDriversLicences()
    {
        return [
            ['name' => 'No', 'description' => null, 'data' => null, 'default' => 0, 'order' => 1],
            ['name' => 'Yes, but no car', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => 'Yes, with car', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
        ];
    }

    public function getBinaries()
    {
        return [
            ['name' => 'Yes', 'description' => null, 'data' => null, 'default' => 0, 'order' => 1],
            ['name' => 'No', 'description' => null, 'data' => null, 'default' => 1, 'order' => 2],
        ];
    }

    public function getSkills()
    {
        return [
            ['name' => 'First Aid Training', 'description' => null, 'data' => null, 'default' => 0, 'order' => 1],
            ['name' => 'Water Safety', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => 'Tutoring', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => 'Special Needs Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 4],
            ['name' => 'Foreign Languages', 'description' => null, 'data' => null, 'default' => 0, 'order' => 6],
        ];
    }

    public function getServices()
    {
        return [
            ['name' => 'Pickups/Drop Offs', 'description' => null, 'data' => null, 'default' => 0, 'order' => 1],
            ['name' => 'Meal Preparation', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => 'Housekeeping', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => 'Errands', 'description' => null, 'data' => null, 'default' => 0, 'order' => 4],
            ['name' => 'Travel', 'description' => null, 'data' => null, 'default' => 0, 'order' => 5],
            ['name' => 'Overnight Care', 'description' => null, 'data' => null, 'default' => 0, 'order' => 6],
            ['name' => 'Will Care for Sick Children', 'description' => null, 'data' => null, 'default' => 0, 'order' => 7],
        ];
    }

    public function getInterests()
    {
        return [
            ['name' => 'Arts & Music', 'description' => null, 'data' => null, 'default' => 0, 'order' => 1],
            ['name' => 'Home & Gardening', 'description' => null, 'data' => null, 'default' => 0, 'order' => 2],
            ['name' => 'Travel', 'description' => null, 'data' => null, 'default' => 0, 'order' => 3],
            ['name' => 'Cars', 'description' => null, 'data' => null, 'default' => 0, 'order' => 4],
            ['name' => 'Health & Fitness', 'description' => null, 'data' => null, 'default' => 0, 'order' => 5],
            ['name' => 'Outdoors', 'description' => null, 'data' => null, 'default' => 0, 'order' => 6],
            ['name' => 'Entertainment', 'description' => null, 'data' => null, 'default' => 0, 'order' => 7],
            ['name' => 'Technology', 'description' => null, 'data' => null, 'default' => 0, 'order' => 8],
            ['name' => 'Family', 'description' => null, 'data' => null, 'default' => 0, 'order' => 9],
            ['name' => 'Food & Drink', 'description' => null, 'data' => null, 'default' => 0, 'order' => 10],
        ];
    }
}
