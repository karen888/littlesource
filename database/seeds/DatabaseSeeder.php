<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Model::unguard();

        $this->call('SecurityQuestionTypesTableSeeder');

        $this->call('MenuTop');
        $this->call('Settings');

        $this->call('DeclineReasonSeeder');
        $this->call('PaymentPeriods');
        $this->call('FeedbackReasonSeeder');
        $this->call('RatingOptionsSeeder');
        $this->call('CriteriaTableSeeder');
        $this->call('CountriesSeeder');
        $this->call('StatesTableSeeder');
        $this->call('UpdateCriteriaTableSeeder');
    }

}
