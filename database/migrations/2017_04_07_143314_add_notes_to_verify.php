<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotesToVerify extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('user_infos', function (Blueprint $table) {
                $table->text('address_notes')->nullable();
                $table->text('wwcc_notes')->nullable();
                $table->text('bd_id_notes')->nullable();


            });

            Schema::table('certificates', function (Blueprint $table) {
                $table->text('notes')->nullable();
            });
        } catch (Exception $e) {}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
