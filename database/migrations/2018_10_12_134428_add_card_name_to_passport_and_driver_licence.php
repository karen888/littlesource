<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCardNameToPassportAndDriverLicence extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('passport_ids', function (Blueprint $table) {
            $table->string('card_name')->after('photo');
        });
        Schema::table('driver_licence_ids', function (Blueprint $table) {
            $table->string('card_name')->after('photo');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_licence_ids', function (Blueprint $table) {
            $table->dropColumn('card_name');
        });
        Schema::table('passport_ids', function (Blueprint $table) {
            $table->dropColumn('card_name');
        });
    }
}
