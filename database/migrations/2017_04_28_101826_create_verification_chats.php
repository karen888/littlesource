<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVerificationChats extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('verification_chats', function(Blueprint $table) {
           $table->increments('id');
           $table->unsignedInteger('user_id')->length(10);
           $table->string('verification_type');
           $table->string('verification_id')->nullable();
           $table->text('message');
           $table->string('message_sender');
           $table->boolean('is_answered')->default(false);
           $table->timestamps();

           $table->index(['user_id', 'verification_type', 'verification_id'],'ver_chats_type_indexes');
           $table->index('message_sender', 'ver_chats_sender_idx');
           $table->index('is_answered', 'ver_chats_is_answered_index');
           $table->foreign('user_id')
               ->references('id')->on('users');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verification_chats', function(Blueprint $table){
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('verification_chats');
    }
}
