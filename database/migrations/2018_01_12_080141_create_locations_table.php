<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->string('user_photo_100')->change();
        });

        Schema::table('user_infos', function(Blueprint $table) {
            $table->dropForeign('user_infos_country_id_foreign');
        });

        Schema::table('user_infos', function(Blueprint $table) {
            $table->unsignedInteger('country_id')->change();
        });

        Schema::table('countries', function(Blueprint $table) {
            $table->unsignedInteger('id')->change();
        });

        Schema::table('user_infos', function(Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });

        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('country_id')->nullable()->default(null);
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->double('latitude', 10,6);
            $table->double('longitude', 10,6);
            $table->string('street')->nullable()->default(null);
            $table->string('suburb')->nullable()->default(null);
            $table->string('state')->nullable()->default(null);
            $table->integer('post_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');

        Schema::table('user_infos', function(Blueprint $table) {
            $table->dropForeign('user_infos_country_id_foreign');
        });

        Schema::table('user_infos', function(Blueprint $table) {
            $table->integer('country_id')->change();
        });

        Schema::table('countries', function(Blueprint $table) {
            $table->integer('id')->change();
        });

        Schema::table('user_infos', function(Blueprint $table) {
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
        });

        DB::statement('ALTER TABLE `do5_users` CHANGE `user_photo_100` `user_photo_100` MEDIUMBLOB NULL DEFAULT NULL;');
    }
}
