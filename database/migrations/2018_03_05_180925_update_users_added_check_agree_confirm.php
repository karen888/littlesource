<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersAddedCheckAgreeConfirm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('check_agree')->nullable()->default(false);
            $table->boolean('check_confirm')->nullable()->default(false);
        });

        Schema::table('government_ids', function (Blueprint $table) {
            $table->dropColumn('check_agree');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('government_ids', function (Blueprint $table) {
            $table->boolean('check_agree')->nullable()->default(false);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['check_agree','check_confirm']);
        });
    }
}
