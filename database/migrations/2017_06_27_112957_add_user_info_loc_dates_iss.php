<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserInfoLocDatesIss extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_infos', function(Blueprint $table) {
            $table->string('loc_issue_date')->nullable();
            $table->string('loc_expiry_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_infos', function(Blueprint $table) {
            $table->dropColumn('loc_issue_date');
            $table->dropColumn('loc_expiry_date');
        });
    }
}
