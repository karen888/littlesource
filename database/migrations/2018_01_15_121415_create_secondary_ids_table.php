<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('secondary_ids', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedTinyInteger('type');
            $table->string('card_names')->nullable()->default(null);
            $table->date('acquisition_date')->nullable()->default(null);
            $table->date('expiry_date')->nullable()->default(null);
            $table->string('id_number');
            $table->unsignedInteger('card_type')->nullable()->default(null);
            $table->unsignedInteger('individual_ref_number')->nullable()->default(null);

            $table->boolean('verified')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('secondary_ids');
    }
}
