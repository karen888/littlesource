<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_config', function(Blueprint $table) {
            $table->string('key', 255);
            $table->text('value');

            $table->index('key', 'app_config_key_idx')->unique();
            $table->primary('key', 'pk_app_config_key_idxeddd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_config');
    }
}
