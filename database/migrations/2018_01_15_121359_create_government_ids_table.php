<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGovernmentIdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('government_ids', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedTinyInteger('type');
            $table->string('photo')->nullable()->default(null);
            $table->string('user_holding_document_photo');
            $table->string('id_number');

            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
            $table->boolean('year_only')->nullable()->default(false);

            $table->boolean('check_agree')->nullable()->default(false);
            $table->boolean('verified')->nullable()->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('government_ids');
    }
}
