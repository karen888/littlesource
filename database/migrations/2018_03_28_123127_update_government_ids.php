<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGovernmentIds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('government_ids', function (Blueprint $table) {
            $table->dropForeign('government_ids_state_id_foreign');
            $table->dropColumn(['photo', 'id_number', 'year_only', 'type', 'verified', 'state_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('government_ids', function (Blueprint $table) {
            $table->unsignedTinyInteger('type');
            $table->string('photo')->nullable()->default(null);
            $table->string('id_number');
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
            $table->boolean('year_only')->nullable()->default(false);
            $table->boolean('verified')->nullable()->default(false);
        });
    }
}
