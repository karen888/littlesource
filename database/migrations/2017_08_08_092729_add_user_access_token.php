<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserAccessToken extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('access_tokens', function(Blueprint $table) {
           $table->increments('id');
           $table->unsignedInteger('user_id');
           $table->string('token');
           $table->boolean('revoked')->default(false);
           $table->timestamps();

           $table->unique('token', 'idx_access_tokens_token');
           $table->foreign('user_id')
               ->references('id')
               ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('access_tokens');
    }
}
