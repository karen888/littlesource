<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_infos', function (Blueprint $table) {
            $table->string('wwcc_user_img')->after('wwcc_image')->nullable()->default(null);
            $table->boolean('wwcc_check_agree')->after('wwcc_user_img')->nullable()->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_infos', function (Blueprint $table) {
            $table->dropColumn('wwcc_user_img');
            $table->dropColumn('wwcc_check_agree');
        });
    }
}
