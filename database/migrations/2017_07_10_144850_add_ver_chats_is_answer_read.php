<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVerChatsIsAnswerRead extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('verification_chats', function(Blueprint $table) {
           $table->boolean('is_answer_read')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('verification_chats', function(Blueprint $table) {
            $table->dropColumn('is_answer_read');
        });
    }
}
