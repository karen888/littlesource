<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEmployeeInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_info', function (Blueprint $table) {
            $table->dropColumn(['max_rate', 'min_rate']);
            $table->double('rate', 8,2)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_info', function (Blueprint $table) {
            $table->dropColumn('rate');
            $table->double('max_rate', 8,2)->nullable()->default(null);
            $table->double('min_rate', 8,2)->nullable()->default(null);
        });
    }
}
