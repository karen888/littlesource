<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDescriptionEducation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::table('educations', function (Blueprint $table) {
                $table->text('notes')->nullable();
            });
        }catch (Exception $e){}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            Schema::table('educations', function (Blueprint $table) {
                $table->dropColumn('notes');
            });
        } catch (Exception $e){}
    }
}
