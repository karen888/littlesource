<?php
$factory->define(App\User::class, function (Faker\Generator $faker) {
    $uniq_id = uniqid();

    return [
        'first_name' => $faker->word(),
        'last_name' => $faker->word(),
        'email' => $uniq_id . '@example.com',
        'password' => $uniq_id,
        'user_path' => $uniq_id,
        'is_caregiver' => true,
        'is_admin' => false,
        'user_available' => true
    ];
});
