<?php
$factory->define(App\Entities\Job\Job::class, function (Faker\Generator $faker) {

    $job_duration = App\Entities\Job\JobDuration::select('id')->get()->toArray();
    $job_workload = App\Entities\Job\JobWorkload::select('id')->get()->toArray();
    $marketplace_visibility = App\Http\Models\MarketplaceVisibility::select('id')->get()->toArray();
    $minimum_feedback = App\Http\Models\MinimumFeedback::select('id')->get()->toArray();
    $allowed_locations = App\Http\Models\AllowedLocations::select('id')->get()->toArray();
    $minimum_experience = App\Http\Models\MinimumExperience::select('id')->get()->toArray();
    $have_transportation = DB::table('criterias')->where('type', 'App\Entities\Criteria\DriversLicence')->select('id')->get();
    $gender = DB::table('criterias')->where('type', 'App\Entities\Criteria\Gender')->select('id')->get();
    $smoker = DB::table('criterias')->where('type', 'App\Entities\Criteria\Smoker')->select('id')->get();
    $comfortable_with_pets = DB::table('criterias')->where('type', 'App\Entities\Criteria\Pets')->select('id')->get();
    $own_children = DB::table('criterias')->where('type', 'App\Entities\Criteria\OwnChildren')->select('id')->get();
    $sick_children = DB::table('criterias')->where('type', 'App\Entities\Criteria\HasOwnCar')->select('id')->get();
    $child_age_range = DB::table('criterias')->where('type', 'App\Entities\Criteria\ChildAge')->select('id')->get();
    $age = DB::table('criterias')->where('type', 'App\Entities\Criteria\AgeRange')->select('id')->get();


    return [
        'title' => $faker->sentence(5),
        'description' => $faker->text(),
        'ocupation' => true,
        'user_id' => 1,
        'job_duration_id' => $job_duration[array_rand($job_duration)]['id'],
        'job_workload_id' => $job_workload[array_rand($job_workload)]['id'],
        'marketplace_visibility_id' => $marketplace_visibility[array_rand($marketplace_visibility)]['id'],
        'minimum_feedback_id' => $minimum_feedback[array_rand($minimum_feedback)]['id'],
        'allowed_locations_id' => $allowed_locations[array_rand($allowed_locations)]['id'],
        'minimum_experience_id' => $minimum_experience[array_rand($minimum_experience)]['id'],
        'have_transportation' => $have_transportation[array_rand($have_transportation)]->id,
        'gender' => $gender[array_rand($gender)]->id,
        'smoker' => $smoker[array_rand($smoker)]->id,
        'comfortable_with_pets' => $comfortable_with_pets[array_rand($comfortable_with_pets)]->id,
        'own_children' => $own_children[array_rand($own_children)]->id,
        'sick_children' => $sick_children[array_rand($sick_children)]->id,
        'child_age_range' => $child_age_range[array_rand($child_age_range)]->id,
        'age' => json_encode([$age[array_rand($age)]->id])
    ];
});
