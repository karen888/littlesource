#!/usr/bin/env bash
# Watch for the line endings (and encoding, has to be UTF8)

timedatectl set-timezone UTC

sudo apt-get update
sudo apt-get -y install php-mcrypt

# SSL
sudo openssl req -x509 -nodes -sha256 -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt -subj "/C=AU/ST=VIC/L=Melbourne/O=LittleOnes/OU=IT Department/CN=little-ones.test"
sudo openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048

SSLCONTENTS=$(cat <<EOF
ssl_certificate /etc/ssl/certs/nginx-selfsigned.crt;
ssl_certificate_key /etc/ssl/private/nginx-selfsigned.key;
EOF
)
echo "${SSLCONTENTS}" > /etc/nginx/snippets/self-signed.conf

SSLCONTENTS=$(cat <<EOF
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
ssl_ecdh_curve secp384r1;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off;
ssl_stapling on;
ssl_stapling_verify on;
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
# Disable preloading HSTS for now.  You can use the commented out header line that includes
# the "preload" directive if you understand the implications.
#add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;

ssl_dhparam /etc/ssl/certs/dhparam.pem;
EOF
)
echo "${SSLCONTENTS}" > /etc/nginx/snippets/ssl-params.conf

# Create nginx config file:
NGINX_CONFIG_FILE_CONTENTS="server {
    listen $3;
    listen 443 ssl http2;
    include snippets/self-signed.conf;
    include snippets/ssl-params.conf;
    server_name $1;
    root \"$2\";

    index index.html index.htm index.php;

    charset utf-8;

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    access_log off;
    error_log  /var/log/nginx/$1-error.log error;

    sendfile off;

    client_max_body_size 100m;

    location ~ \.php$ {
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_intercept_errors off;
        fastcgi_buffer_size 16k;
        fastcgi_buffers 4 16k;
    }

    location ~ /\.ht {
        deny all;
    }
}"

echo "$NGINX_CONFIG_FILE_CONTENTS" > "/etc/nginx/sites-available/$1"
ln -fs "/etc/nginx/sites-available/$1" "/etc/nginx/sites-enabled/$1"

service nginx restart
service php7-fpm restart

# Setting up mysql
mysql -uhomestead -psecret -e "SET PASSWORD = PASSWORD('secret');" --connect-expired-password;
mysql -uhomestead -psecret -e "DROP DATABASE IF EXISTS \`$4\`";
mysql -uhomestead -psecret -e "CREATE DATABASE \`$4\` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_unicode_ci";
apt-get install notify-osd -y
apt-get install libnotify-bin -y

# Upgrade nodejs
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get install -y nodejs

# Install gulp
npm install gulp-cli -g
npm install gulp

# Installs for PhantomJS
apt-get install build-essential chrpath libssl-dev libxft-dev -y
apt-get install libfreetype6 libfreetype6-dev -y
apt-get install libfontconfig1 libfontconfig1-dev -y
cd /tmp
wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
tar xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2
cp phantomjs-2.1.1-linux-x86_64/bin/phantomjs /usr/local/bin
rm -r phantomjs-2.1.1-linux-x86_64
rm phantomjs-2.1.1-linux-x86_64.tar.bz2

# Aliases
alias ll="ls -alF --group-directories-first"
alias codecept="./vendor/bin/codecept"
alias art="php artisan"

# application
cd /vagrant
composer install
mysql -uhomestead -psecret little-ones < setup/littleones.sql
# add an admin user (password: 123456)
mysql -uhomestead -psecret little-ones -e "INSERT INTO do5_users SET is_caregiver = 0, is_admin = 1, email = 'dev@littleones.com.au', password = '\$2a\$04\$LGL.rCSmt7a19IwYon1RZ.hdvzxumnaU6Ah0SxyFtaz64L2ulf/rq', first_name = 'admin', last_name = 'admin', user_path = '', user_available = 1"
npm install

cp setup/.env.vagrant /vagrant/.env
php artisan key:generate
php artisan migrate
# DatabaseSeeder class user $this->call() to select what seeders will run
php artisan db:seed --class=DatabaseSeeder

gulp