<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    @inject('carbon', 'Carbon\Carbon')

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>LittleOnes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta id="_token" value="{{ csrf_token() }}"> <!-- Laravel Token -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/styles.css?v={{ time() }}">
    {{--<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">--}}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/js/vendor/modernizr.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    {{--<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>--}}

</head>

<body>
    <div id="pre-launch">
        @include('flash::message')
        <div class="selection-of-roles-box" v-show="!viewIsSelected" style="display: none;">
            <div class="parents-role" >
                <h4 class="SelectionRole__Header">I want to hire a caregiver</h4>
                <h6 class="SelectionRole__Description">Find, interview, hire and <br> pay a caregiver.</h6>
            <a class="btn btn-primary btn-text SelectionRole__Action" href="/pre-launch/clients">Enter Site</a>
            </div>
            <span class="h4 help-text">or</span>
            <div class="caregiver-role" >
                <h4 class="SelectionRole__Header"> I'm a caregiver looking for work</h4>
                <h6 class="SelectionRole__Description">Create a profile, apply to positions, <br> and earn money.</h6>
        <a class="btn btn-default btn-text SelectionRole__Action" href="/pre-launch/caregivers">Enter Site</a>
            </div>
        </div><!-- selection-of-roles-box -->
        <beta :show.sync="showModal"></beta>
        <vc-adina-info :show.sync="showAdinaModal"></vc-adina-info>
        @yield('content')
    </div>

    <!-- main-footer -->
        @yield('page-required-scripts')
    <!-- Scripts -->
    <script src="/js/all.js?v={{ time() }}"></script>
</body>
</html>
