<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    @inject('carbon', 'Carbon\Carbon')

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>LittleOnes @yield('page-title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta id="_token" value="{{ csrf_token() }}"> <!-- Laravel Token -->

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- Place favicon.ico in the root directory -->

	<!-- Fonts -->
    <!--
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    -->

    <link rel="stylesheet" href="/bootstrap/font-awesome/css/font-awesome.min.css" type="text/css" />

    {!! $csss or '' !!}
    <link href="/js/select2/css/select2.min.css" rel="stylesheet" />
    <link href="/bootstrap/bootstraphelp/css/bootstrap-formhelpers.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.ui/1.11.4/jquery-ui.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/locale/en-au.js"></script>
    <!-- whizzweb editing -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css').'?nc='.rand() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/redefine_styles.css').'?nc='.rand() }}">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script src="/js/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.ui/1.11.4/jquery-ui.min.js"></script>
	<script src="/bootstrap/js/bootstrap.min.js"></script>

    <script src="/js/select2/js/select2.min.js"></script>
    <script src="/bootstrap/bootstraphelp/js/bootstrap-formhelpers-datepicker.js"></script>
    <script src="/bootstrap/bootstraphelp/js/lang/en_US/bootstrap-formhelpers-datepicker.en_US.js"></script>

    <script src="/js/vendor/modernizr.js"></script>
    <script src="/js/inputmask/jquery.inputmask.bundle.js"></script>
    <script src="/js/jq-fileupload/jquery.iframe-transport.js"></script>
    <script src="/js/jq-fileupload/jquery.fileupload.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.loadingoverlay/latest/loadingoverlay_progress.min.js"></script>

    <!--<script src="/js/app.js"></script>-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.13/css/intlTelInput.css">
    <link rel="stylesheet" href="https://rawgit.com/fkranenburg/bootstrap-pincode-input/master/css/bootstrap-pincode-input.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/sweetalert2/6.6.2/sweetalert2.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.13/js/intlTelInput.min.js"></script>

    <script src="https://cdn.jsdelivr.net/sweetalert2/6.6.2/sweetalert2.min.js"></script>

    <script src="https://rawgit.com/fkranenburg/bootstrap-pincode-input/master/js/bootstrap-pincode-input.js"></script>

    {!! $javascripts or '' !!}

    @yield('js')

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
        { (i[r].q=i[r].q||[]).push(arguments)}

        ,i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-90275104-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script>
        window._csrf = {};

        window._csrf._token = '{{csrf_token()}}';
    </script>

    <script>
        /*window.ImageErrorHandler = window.setInterval(function(){
            var imgs = document.getElementsByTagName("img");

            $.each(imgs, function(k, img){
                img.onerror = function () {

                    if($(this).attr('no-hide') == 'true') {
                        return;
                    }

                    this.style.display = "none";
                    var $parent = $(this).parent();

                    var isA = false;

                    if($parent.is('a')) {
                        $parent = $parent.parent();
                        isA = true;
                    }

                    $parent.append('-');

                    if(isA) {
                        $(this).remove();
                    } else {
                        $(this).parent().remove();
                    }
                }
            });
        },500);*/

        //window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
                document.getElementById("scrolltop").style.visibility = "visible";
                document.getElementById("scrolltop").style.opacity = "1";
            } else {
                document.getElementById("scrolltop").style.visibility = "hidden";
                document.getElementById("scrolltop").style.opacity = "0";
            }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Chrome, Safari and Opera
            document.documentElement.scrollTop = 0; // For IE and Firefox
        }
    </script>

    <style>
        #scrolltop {
            transition: visibility .2s ease-in-out, opacity .2s ease-in-out;
            visibility: hidden; /* Hidden by default */
            position: fixed; /* Fixed/sticky position */
            opacity: 0;
            top: 0px; /* Place the button at the bottom of the page */
            left: 0px; /* Place the button 30px from the right */
            z-index: 99; /* Make sure it does not overlap */
            border: none; /* Remove borders */
            outline: none; /* Remove outline */
            background-color: rgba(0, 0, 0, 0.02); /* Set a background color */
            color: #9c9998; /* Text color */
            cursor: pointer; /* Add a mouse pointer on hover */
            padding: 15px; /* Some padding */
            width: 160px;
            height: 100%;
        }

        #scrolltop > i.fa {
            font-size: 26pt;

        }

        #scrolltop:hover {
            background-color: rgba(0, 0, 0, 0.03); /* Add a dark-grey background on hover */
        }

        @media(max-width: 1430px) {
            #scrolltop {
                display: none !important;
                visibility: hidden;
                z-index: -9999999;
                position: absolute;
                top: -9999999px;
                left: -999999px;
            }
        }


        .terms-left {
            margin-top: 100px;

            padding: 10px 5px 10px 5px;
            background: #f9f9f9;

        }
        .terms-left ul {
            list-style-type: none;


        }
        .terms-left ul li {
            margin: 8px 0px 8px 0px;

        }
        .terms-left ul li a {
            text-decoration: none;
            font-size: 1.09345em;

        }
        .terms-left ul li a:hover, .terms-left ul li a:active , .terms-left ul li a.active {

            text-decoration: underline;
            color: #24992b;
        }
        .page {
            margin-top: 10px;
            height: 100%;
            padding-right: 2%;
            font-family: 'Open Sans';
        }

        .page .col-lg-9 {
            outline: none;
            border-left: 1px #d9d9d9 solid;
            position: relative;
            margin-top: 100px;
            max-width: 1200px;
            padding-left: 25px;
        }
        .page p {
            font-size: 16px;
            text-align: justify;
            text-indent: 15px;
            line-height: 26px;
            color: #686868;
            font-family: 'Open Sans';
            text-shadow: 1px 0px 0px #E0E0E0;
            -moz-text-shadow: 1px 0px 0px #E0E0E0;
            -webkit-text-shadow: 1px 0px 0px #E0E0E0;
            -o-text-shadow: 1px 0px 0px #E0E0E0;
        }

        .page h1 {
            font-size: 30pt;
            font-family: 'Open Sans';
            color: #444444;
        }
        .page h2 {
            margin-left: 27px;
            font-size: 16pt;
            font-family: 'Open Sans';
            color: #444;
        }
        @media(max-width: 991px) {
            .page p {
                font-size: 13px !important;
                line-height: 20px !important;
            }

            .page h1 {
                font-size: 27pt !important;
            }
            .page h2 {
                margin-left: 0px;
                font-size:18pt !important;
            }
        }

        @media(max-width: 500px) {
            .page table {
                margin-left: 0px !important;
            }
            .page p {
                font-size: 14px !important;
                line-height: 22px !important;
            }

            .page h1 {
                font-size: 16.5pt !important;
            }
            .page h2 {
                margin-left: 0px;
                font-size:12.5pt !important;
            }
        }

    </style>
</head>
<body class="page-<?php print str_replace(".", "-", Route::getCurrentRoute()->getName()); if(Auth::guest()){ print ' is_guest'; } ?>">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
    {{--<button onclick="topFunction()" id="scrolltop" title="Go to top">--}}
        {{--<i class="fa fa-chevron-up "></i>--}}
    {{--</button>--}}
    {!! $main_menu or 'none' !!}

    <div id="content-wrap" class="content-wrap">
        @yield('content')
    </div>

    @include('blocks.footer')
    @yield('page-required-scripts')


    <!-- Scripts -->
    <script src="/js/tag-it.min.js"></script>
    <script src="/js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>

    <script src="/js/all.js?ts=<?=time()?>"></script>


@yield('js-bottom')
</body>
</html>
