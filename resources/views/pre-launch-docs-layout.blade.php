<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    @inject('carbon', 'Carbon\Carbon')

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>LittleOnes</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta id="_token" value="{{ csrf_token() }}"> <!-- Laravel Token -->


    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">
    {{--<link rel="stylesheet" type="text/css" href="/css/styles.css?v={{ time() }}">--}}


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/js/vendor/modernizr.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>

</head>

<body style="background-color: #f8f8f8;">
    <div id="pre-launch-docs-layout">
        @include('flash::message')
        @yield('content')
    </div>

    <!-- main-footer -->
        @yield('page-required-scripts')
    <!-- Scripts -->
    <script src="/js/all.js?v={{ time() }}"></script>
</body>
</html>
