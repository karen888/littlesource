@extends('layouts.master')

@section('content')
<div class="container-fluid" id="messages">
    <div class="content">
        <div class="row threads-box">
            <composer></composer>

            <div id="sidebar-menu" class="threads-sidebar-open hidden-lg hidden-md hidden-sm visible-xs" @click="toggleSidebar()">
                <i class="fa fa-3x" :class="{'fa-chevron-circle-right': !showSidebar, 'fa-chevron-circle-left': showSidebar}"></i>
            </div>
            <div class="bordered-left threads-sidebar col-lg-4 col-md-4 col-sm-5 col-xs-11 hidden-xs" :class="{'shown': showSidebar}" >
                <div class="sidebar">
                    <div class="sidebar__header row">
                        <div class="sidebar__header__buttons text-center">
                            {{--<div class="btn-group btn-group-lg btn-left btn-group-outer" role="group">
                                <button type="button" class="btn btn-default"><i class="icon icon-settings"></i></button>
                            </div>--}}
                            <div class="btn-group btn-group-lg btn-group-middle" role="group">
                                <button type="button" class="btn btn-default" :class="{'selected': currentView=='threads'}" @click='setPath("threads")'><i class="icon icon-inbox"></i></button>
                                <button type="button" class="btn btn-default" :class="{'selected': currentView=='corespondents'}" @click='setPath("corespondents")'><i class="icon icon-view-profile"></i></button>
                            </div>
                            {{--<div class="btn-group btn-group-lg btn-right btn-group-outer" role="group">
                                <button type="button" class="btn btn-default" @click="showComposer = true"><i class="icon fa fa-plus"></i></button>
                            </div>--}}
                        </div>
                    </div>

                    <div class="sidebar__search row">
                        <div class="inner-addon filter left-addon">
                            <div class="icon icon-search"></div>
                            <input type="text" placeholder="Search...">
                        </div>
                    </div>

                    <component :is="currentView" :select_thread="{{ $thread_id }}"></component>

                    <div class="sidebar__footer row">
                        <div class="well well-sm text-center" v-if="viewOptions == 'inbox'">
                            <p class="text-muted"><strong>Looking for older messages?</strong></p>
                            <a @click='setPath("threads", true)'>View archived messages</a>
                        </div>
                        <div class="well well-sm text-center" v-else>
                            <p class="text-muted"><strong>Looking for recent messages?</strong></p>
                            <a @click='setPath("threads")'>View current messages</a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="bordered thread-view col-lg-8 col-md-8 col-sm-7 col-xs-12" :class="{'muted': showSidebar}">
                <conversation></conversation>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection