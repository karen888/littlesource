@extends('app')

@section('page-title') - Contact Us @endsection
@section('content')
    <style>
        .container > .row > p {
            text-align: justify;
            font-family: 'Open Sans';
        }

        .container > .row > h4 {
            text-transform: uppercase;
            font-family: 'Open Sans';
        }

        .contact-panel p {
            color: #555;

        }
        .contact-panel p.contact-section {
            color: green;
            font-weight: bold;
            font-family: "Open Sans"
        }

        .form-group:not(.error) .error-message {
            display: none;
        }

    </style>
    <div class="container" style="position: relative;margin-top: 70px; margin-bottom: 15px;padding: 30px 40px">
        <div class="row">
            <a class="" href="javascript:void(0);" onclick="window.history.go(-1);"><i style="font-size: 14pt" class="fa fa-chevron-circle-left"
                                                                             aria-hidden="true"></i> Back to
                Previous Page</a>
        </div>
        <div class="row" style="position: relative;left:-4px;">
            <h3>Contact Us</h3>

        </div>
        <div class="row">
            <div class="col-lg-8 col-md-6 col-xs-12">
                <div class="alert alert-success" id="show" style="display:none">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> Email Successfully Sent!.
                </div>
                <div class="alert alert-danger" role="alert" style="display:none" id="validate">
                    <strong>Warning!</strong>
                    <p></p>
                </div>
                <div class="form-horizontal" style="font-size: 18px;">
                    {!!Form::open() !!}

                    <div class="form-group">
                        {!!Form::label('Name:')!!}
                        {!!Form::text('name',null,['id' => 'name' ,'class'=>'form-control','placeholder'=>'Name','maxlength' => '40'])!!}
                        <div class="error-message"></div>
                    </div>
                    <div class="form-group">
                        {!!Form::label('Email:')!!}
                        {!!Form::text('email',null,['id' => 'email', 'class'=>'form-control','placeholder'=>'Email','maxlength' => '50'])!!}
                        <div class="error-message"></div>
                    </div>

                    <div class="form-group">
                        {!!Form::label('Message:')!!}
                        {!!Form::textarea('message',null,['id' => 'message', 'class'=>'form-control','placeholder'=>'Message','maxlength' => '300'])!!}
                        <div class="error-message"></div>
                    </div>
                    <div class="form-group pull-right">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                        {!!link_to('#', $title='SEND', $attributes=['id' => 'send','class'=>'btn btn-default','style' => 'color:white'] ,$secure=null)!!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="panel contact-panel" style="margin-top:28px;">
                    <div class="panel-head">

                    </div>
                    <div class="panel-body">
                        <p class="contact-section">Telephone</p>
                        <p>1300 312 026</p>
                        <hr/>
                        <p class="contact-section">Address</p>
                        <p>3/3 Levida Drive, Carrum Downs VIC 3201
                        <hr/>

                        <p>littleones.com.au Pty Ltd
                            <br/>
                            ABN: 47 169 565 468</p>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script>
        function goback() {
            if (window.history.length > 0) {
                window.history.go(-1);
            } else {
                open(location, '_self').close();
            }
        }

        $(document).ready(function(){
            var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

            $("#name").keyup(function () {
                $("#validate").fadeOut('slow');
                $("#show").fadeOut('slow');
                $(this).parent().removeClass('error');
            }).keyup();
            $("#email").keyup(function () {
                $("#validate").fadeOut('slow');
                $("#show").fadeOut('slow');
                $(this).parent().removeClass('error');
            }).keyup();
            $("#message").keyup(function () {
                $("#validate").fadeOut('slow');
                $("#show").fadeOut('slow');
                $(this).parent().removeClass('error');
            }).keyup();

            $('#send').click(function()
            {
                var name=$("#name").val();
                var email=$("#email").val();
                var subject=$("#subject").val();
                var message=$("#message").val();

                var error = false;

                if(name == "")
                {
                    $("#name").parent().addClass('error').find('.error-message').text("Name field is Required");
                    $("#name").focus();
                    error = true;
                }

                if(email == "")
                {
                    $("#email").focus();
                    $("#email").parent().addClass('error').find('.error-message').text("Email field is Required");
                    error = true;
                }
                if(!expr.test(email))
                {
                    $("#email").focus();
                    $("#email").parent().addClass('error').find('.error-message').text("The email must be a valid email address");
                    error = true;
                }
                if(message==""){
                    $("#message").focus();
                    $("#message").parent().addClass('error').find('.error-message').text("Message field is Required");
                    error = true;
                }

                if(error) return;

                var route = "/contact";
                var token = $('#token').val();

                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN':token},
                    type: 'POST',
                    dataType : 'json',
                    data: {
                        name:name,
                        email:email,
                        subject:subject,
                        message:message
                    },
                    success: function() {
                        $("#show").show();
                        $("#name").val("");
                        $("#email").val("");
                        $("#message").val("");
                    }
                });
            })
        });
    </script>
@endsection