<!DOCTYPE html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>LittleOnes @yield('page-title')</title>

    <!-- Styles -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link href="{{ elixir('css/new-app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('/bootstrap/font-awesome/css/font-awesome.min.css')}}" type="text/css"/>
    @stack('styles')
</head>
<body>
@yield('content')
<!-- Scripts -->
<script src="{{ elixir('js/new-app.js') }}"></script>
@stack('scripts')
</body>
</html>
