@extends('app')

@section('page-title') - Privacy Policy @endsection
@section('content')
    <style>
        .container > .row > p {
            text-align: justify;
            font-family:'Open Sans';
        }
        .container>.row>h4{
            text-transform: uppercase;
            font-family:'Open Sans';
        }
    </style>
    <div class="container" style="position: relative;margin-top: 70px; margin-bottom: 15px;padding: 15px 30px">
        <div class="row">

            <p><a class="beta-0-back" href="javascript:void(0);" onclick="goback()"><i class="fa fa-chevron-circle-left"></i> Back to Previous Page</a>
            </p>
            <p>
                <time style="color: #333; font-size: 13pt">
                Updated August, 2017
                </time>
            </p>
            <p>This following document sets forth the Privacy Policy for the Little Ones website, <a
                        href="https://www.littleones.com.au" target="_blank">https://www.littleones.com.au</a></p>

            <p>Little Ones is committed to providing you with the best possible customer service experience. Little Ones
                is bound by the Privacy Act 1988 (Cth), which sets out a number of principles concerning the privacy of
                individuals.</p>

            <h4>Collection of your personal information</h4>
            <p>
                There are many aspects of the site which can be viewed without providing personal information, however,
                for access to create a store or make purchases you are required to submit personally identifiable
                information. This may include but not limited to a unique username and password, or provide sensitive
                information in the recovery of your lost password. It may include information about your children at the
                school, and credit card information.
            </p>

            <h4>Sharing of your personal information</h4>
            <p>
                We may occasionally hire other companies to provide services on our behalf, including but not limited to
                handling customer support enquiries, processing transactions or customer freight shipping. Those
                companies will be permitted to obtain only the personal information they need to deliver the service.
                Little Ones takes reasonable steps to ensure that these organisations are bound by confidentiality and
                privacy obligations in relation to the protection of your personal information.
            </p>
            <h4>Use of your personal information</h4>
            <p>
                We may use the information we collect from you when you register, make a purchase, sign up for our
                newsletter, respond to a survey or marketing communication, surf the website, or use certain other site
                features in the following ways:
            </p>
            <ul>
                <li>To personalise your experience and to allow us to deliver the type of content and product offerings
                    in which you are most interested.
                </li>
                <li>To improve our website in order to better serve you.</li>
                <li>To allow us to better service you in responding to your customer service requests.</li>
                <li>To administer a contest, promotion, survey or other site feature.</li>
                <li>To quickly process your transactions.</li>
                <li>To send periodic emails regarding your order or other products and services.</li>
                <li>To follow up with them after correspondence (live chat, email or phone inquiries)</li>
            </ul>
            <p>
                For each visitor to reach the site, we expressively collect the following non-personally identifiable
                information, including but not limited to browser type, version and language, operating system, pages
                viewed
                while browsing the Site, page access times and referring website address. This collected information is
                used
                solely internally for the purpose of gauging visitor traffic, trends and delivering personalised content
                to
                you while you are at this Site.
                From time to time, we may use customer information for new, unanticipated uses not previously disclosed
                in
                our privacy notice. If our information practices change at some time in the future we will use for these
                new
                purposes only, data collected from the time of the policy change forward will adhere to our updated
                practices.
            </p>
            <h4>Protection of your personal information</h4>
            <p>Our website is scanned on a regular basis for security holes and known vulnerabilities in order to make
                your visit to our site as safe as possible.
                Your personal information is contained behind secured networks and is only accessible by a limited
                number of persons who have special access rights to such systems, and are required to keep the
                information confidential. In addition, all sensitive/credit information you supply is encrypted via
                Secure Socket Layer (SSL) technology.
                We implement a variety of security measures when a user places an order enters, submits, or accesses
                their information to maintain the safety of your personal information.
                All credit card transactions are processed through a gateway provider and are not stored or processed on
                our servers.
            </p>
            <h4>Do we use 'cookies'?</h4>
            <p>
                Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive
                through your Web browser (if you allow) that enables the site's or service provider's systems to
                recognise your browser and capture and remember certain information. For instance, we use cookies to
                help us remember and process the items in your shopping cart. They are also used to help us understand
                your preferences based on previous or current site activity, which enables us to provide you with
                improved services. We also use cookies to help us compile aggregate data about site traffic and site
                interaction so that we can offer better site experiences and tools in the future.

                We use cookies to:
            </p>
            <ul>
                <li>Help remember and process the items in the shopping cart.</li>
                <li>Understand and save user&#39;s preferences for future visits.</li>
                <li>Compile aggregate data about site traffic and site interactions in order to offer better site
                    experiences and tools in the future. We may also use trusted third-party services that track this
                    information on our behalf.
                </li>
            </ul>
            <p>
                You can choose to have your computer warn you each time a cookie is being sent, or you can choose to
                turn
                off all cookies. You do this through your browser settings. Since browser is a little different, look at
                your browser's Help Menu to learn the correct way to modify your cookies.
            </p>
            <h4>If users disable cookies in their browser:</h4>

            <p>
                If you turn cookies off, Some of the features that make your site experience more efficient may not
                function
                properly.Some of the features that make your site experience more efficient and may not function
                properly.
            </p>

            <h4>Third-party disclosure</h4>

            <p>
                We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information
                unless we provide users with advance notice. This does not include website hosting partners and other
                parties who assist us in operating our website, conducting our business, or serving our users, so long
                as
                those parties agree to keep this information confidential. We may also release information when it's
                release
                is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights,
                property or safety.
            </p>
            <p>
                However, non-personally identifiable visitor information may be provided to other parties for marketing,
                advertising, or other uses.
            </p>

            <h4>
                Third-party links
            </h4>
            <p>
                Occasionally, at our discretion, we may include or offer third-party products or services on our
                website.
                These third-party sites have separate and independent privacy policies. We therefore have no
                responsibility
                or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the
                integrity of our site and welcome any feedback about these sites.
            </p>

            <h5>Google</h5>

            <p>
                Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in
                place
                to provide a positive experience for users.
                <a
                        href="https://support.google.com/adwordspolicy/answer/1316548?hl=en"
                        target="_blank">
                    https://support.google.com/adwordspolicy/answer/1316548?hl=en
                </a>
            </p>
            <p>
                We have not enabled Google AdSense on our site but we may do so in the future.
            </p>

            <h5>How does our site handle Do Not Track signals?</h5>
            <p>
                We honour Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track
                (DNT)
                browser mechanism is in place.
            </p>

            <h5>Does our site allow third-party behavioural tracking?</h5>
            <p>It's also important to note that we do not allow third-party behavioural tracking</p>


            <h5>CAN SPAM Act</h5>

            <p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for
                commercial
                messages, gives recipients the right to have emails stopped from being sent to them, and spells out
                tough
                penalties for violations.
            </p>

            <h5>
                We collect your email address in order to:
            </h5>

            <ul>
                <li> Send information, respond to inquiries, and/or other requests or questions</li>
                <li>Process orders and to send information and updates pertaining to orders.</li>
                <li>Send you additional information related to your product and/or service</li>
                <li>Market to our mailing list or continue to send emails to our clients after the original transaction
                    has
                </li>
                <li>occurred.</li>
            </ul>
            <h5>To be in accordance with CANSPAM, we agree to the following:</h5>

            <ul>
                <li>Not use false or misleading subjects or email addresses.</li>
                <li>Identify the message as an advertisement in some reasonable way.</li>
                <li>Include the physical address of our business or site headquarters.</li>
                <li>Monitor third-party email marketing services for compliance, if one is used.</li>
                <li>Honor opt-out/unsubscribe requests quickly.</li>
                <li>Allow users to unsubscribe by using the link at the bottom of each email.</li>
            </ul>
            <p>
                If at any time you would like to unsubscribe from receiving future emails, you can follow the
                instructions
                at the bottom of each email and we will promptly remove you from ALL correspondence.
            </p>

            <h4>Changes to this Privacy Policy</h4>

            <p>
                Little Ones reserves the right to make amendments to this Privacy Policy at any time. If you have
                objections
                to the Privacy Policy, you should not access or use the Site.
            </p>

            <h4>Accessing Your Personal Information</h4>

            <p>
                You have a right to access your personal information, subject to exceptions allowed by law. If you would
                like to do so, please let us know. You may be required to put your request in writing for security
                reasons.
                Little Ones reserves the right to charge a fee for searching for, and providing access to, your
                information
                on a per request basis.
            </p>
            <h4>Contacting us</h4>

            <p>
                Little Ones welcomes your comments regarding this Privacy Policy. If you have any questions about this
                Privacy Policy and would like further information, please contact us by any of the following means
                during
                business hours Monday to Friday.
            </p>

            <p>
            <blockquote style="font-size: inherit">
                <strong>Call:</strong> <a href="tel:1300 312 026">1300 312 026</a>


            <address><strong>Post:</strong> Attn: Privacy Policy,
            Littleones.com.au Pty Ltd,
                3/3 Levida drive. Carrum downs. 3201</address>
            <strong>E-mail:</strong><a href="mailto:info@littleones.com.au">info@littleones.com.au</a>
            </blockquote>
            </p>
        </div>
    </div>


    <script>
        function goback() {
            if (window.history.length > 0) {
                window.history.go(-1);
            } else {
                open(location, '_self').close();
            }
        }
    </script>
@endsection