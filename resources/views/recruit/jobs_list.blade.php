@extends('app')

@section('content')

<div class="page-header">
<h3>Jobs List</h3>
</div>

<div class="row">
    <table class="table table-striped">
        <thead>
            <tr>
                <th class="col-md-5 text-left">Job</th>
                <th>Applicants</th>
                <th>Date Posted</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($jobs_list as $val)
            <tr>
                <td class="col-md-5 text-left"><a href="{{ $val->link_edit }}">{{ $val->title }}</a></td>
                <td><a href="{{ $val->link_applicants }}">View ({{ $val->applies_count }})</a></td>
                <td>{{ $val->created_human }}</td>
                <td>
	                <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="proc" value="del">
                        <input type="hidden" name="id" value="{{ $val->id }}">

                        <a href="" class="send-req-form text-danger" data-loading-text=".." data-confirm="Are you sure?"><i class="fa fa-remove text-danger"></i></a>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>

<div class="row">
{!! $jobs_list->render() !!}
</div>

<script src="/js/recruit.js?v={{ time() }}"></script>
@endsection
