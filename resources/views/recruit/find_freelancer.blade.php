@extends('app')

@section('content')

<div class="page-header">
<h3>Find Freelancer</h3>
</div>


<div class="row">

    <div class="col-md-8 col-md-offset-3">
    	<form class="form-horizontal" role="form" method="GET" action="" enctype="multipart/form-data" >
    	    <div class="form-group">
                <div class="col-md-5">
                    <input type="text" class="form-control" name="search" value="{{ $search or '' }}">
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    @foreach($find_result as $val)
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="media">
                    <div class="media-left">
                        <img src="{{ $val->user_photo_64 }}">
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading"><a href="{{ $val->user_path }}">{{ $val->name }}</a></h4>
                        {{ $val->user_overview }}
                    </div>
                    <div class="media-right">
                        <a href="{{ routeClear('home') }}/recruit/invite/{{ $val->id }}" class="btn btn-info">Contact</a>
                    </div>
                </div>
            </div>
            <div class="panel-footer text-muted">
                Skills: 
                @foreach($val->skills as $val)
                    <a href="javascript:void(0);" class="btn btn-default btn-xs">{{ $val->skill_name }}</a>
                @endforeach
            </div>
        </div>
    @endforeach
</div>
<div class="row">
    @if (!empty($find_result)) 
        {!! $find_result->appends(\Input::all())->render() !!} 
    @endif
</div>

@endsection
