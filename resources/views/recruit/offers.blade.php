@extends('app')

@section('content')

<div class="page-header">
<h3>Offers</h3>
</div>
<div class="row">

    <table class="table table-striped">
        <thead>
            <tr>
                <th></th>
                <th>Freelancer</th>
                <th>Job</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($offers_list as $val)
            <tr>
                <td>
                    <a href="{{ routeClear('home_offer') }}/{{ $val->id }}">See offer</a>
                </td>
                <td>
                    <a href="{{ routeClear('user_link') }}/{{ $val->user_path }}">{{ $val->contractor_name }}</a>
                </td>
                <td>
                    <a href="{{ routeClear('job_link') }}/{{ $val->path }}">{{ $val->title }}</a>
                </td>
                <td>
	                <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" value="{{ $val->id }}">

                        <a href="" class="send-req-form text-danger" data-loading-text="" data-confirm="Are you sure?"><i class="fa fa-remove text-danger"></i></a>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>

@endsection
