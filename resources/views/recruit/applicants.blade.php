@extends('app')

@section('content')

<div class="row padd-top">
    @foreach($appls_list as $val)
        <div class="thumbnail appl-items-bl">
            <div class="row">
                <div class="col-md-2 appl-items-photo" style="width: auto">
                    <div class="thumbnail">
                        <img src="{{ userPhoto($val)->photo_100 }}">
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row">
                        <a href="{{ routeClear('user_link') }}/{{ $val->user_path }}" class="appl-items-name">{{ $val->name }}</a>
                        @if ($val->initiated_by_fr == 0)
                            <span class="text-muted text-small">
                                - invited
                                @if($val->active == 0)
                                     - no response yet
                                @endif
                            </span>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-md-1" style="padding-left: 0px;">
                            @if($val->active == 1)
                               <b>${{ $val->job_apply_price }}</b><span class="text-muted">{{ (!$val->job_apply_fixed_price) ? '/hr' : '' }}</span>
                            @endif
                        </div>
                        <div class="col-md-2 text-muted feedback-bl-md">
                            <div class="feedback-rate-numb pull-left" style="margin-left: 0px; margin-right: 3px;">{{ $val->rate->numeric }}</div>
                            <div class="feedback-rate-bl pull-left">
                                <div class="feedback-rate-val" style="width: {{ $val->rate->percent }}%">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-1 text-muted">
                            {{ $val->country_name }}
                        </div>
                    </div>

                    <div class="row" style="margin-top: 10px;">
                        <div class=""><strong>Cover Letter</strong></div>
                        <div class="">
                            {{ $val->cover_letter_freelancer }}
                        </div>
                    </div>

                    @if (!empty($val->file_file_name))
                    <div class="row" style="margin-top: 10px;">
                        <a href="{{ routeClear('home') }}/file/{{ $val->file_id }}">{{ $val->file_file_name }}</a>
                    </div>
                    @endif

                    <div class="row" style="margin-top: 10px;">
                        <a href="" class="appl-modal-message" data-user-id="{{ $val->user_id }}">Send Message</a>
                        <!--
                        | <a href="{{ routeClear('home') }}/recruit/offer/{{ $val->user_id }}/{{ $job_id }}">Hire</a>
                        -->
                        | <a href="{{ routeClear('home') }}/recruit/offer/{{ $val->id }}">Hire</a>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>

<div class="modal fade" id="appl-send-message" tabindex="-1" role="dialog" aria-labelledby="appl-send-message-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Send Message</h4>
            </div>
    	    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="job_id" value="{{ $job_id }}">
                <input type="hidden" name="user_id" value="">

                <div class="modal-body">
                    <div class="form-group" data-name="title">
            			<label class="col-md-3 control-label">To</label>
            			<div class="col-md-8">
                            <div class="row form-control-static">
                                <div class="col-md-2 appl-modal-photo"></div>
                                <div class="col-md-8 appl-modal-username">
                                
                                </div>
                            </div>
            			</div>
                    </div>
                    
                    <div class="form-group" data-name="message">
            			<label class="col-md-3 control-label">Message</label>
            			<div class="col-md-8">
                            <textarea class="form-control" name="message" rows="10"></textarea>
            			</div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary send-req-form">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{ JsCss::addJs('recruit.js') }}
@endsection
