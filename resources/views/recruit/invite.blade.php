@extends('app')

@section('content')

<div class="row">
@if (!empty($data_info))
	<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="user" value="{{ $data_info->user->id }}">

	    <div class="form-group" data-name="">
			<label class="col-md-4 control-label">Freelancer</label>
            <div class="col-md-6 form-control-static">
                <strong>{{ $data_info->user->name }}</strong>
                <br>
                {{ $data_info->user->user_title  }}
            </div>
        </div>

        <div class="form-group" data-name="message">
            <label class="col-md-4 control-label">Message</label>
            <div class="col-md-6">
                <textarea class="form-control" rows="10" name="message"></textarea>
            </div>
        </div>

        <div class="form-group" data-name="job_id">
            <label class="col-md-4 control-label">Choose a Job</label>
            <div class="col-md-6">
                @if (empty($data_info->jobs_list))
                    <div class="form-control-static">Create a job!</div>
                @else
                    <select name="job_id" class="form-control">
                        @foreach ($data_info->jobs_list as $val)
                            <option value="{{ $val->id }}">{{ $val->title }}</option>
                        @endforeach
                    </select>
                @endif
            </div>
        </div>
        
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary send-req-form">Send</button>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <div class="alert alert-danger alert-hidden"></div>
            </div>
        </div>
    </form>
    
@endif
</div>

@endsection
