@extends('app')

@section('content')

<div class="page-header">
<h3>Post Job</h3>
</div>

<div class="row">

	<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="edit" value="{{ $info_job->id or '0' }}">

	    <div class="form-group" data-name="category_sub">
			<label class="col-md-4 control-label">Choose a category</label>
			<div class="col-md-3">
                <select class="form-control" name="category_main" id="post-job-category-main">
                    <option value="0">Please select...</option>
                    @foreach ($categories->main as $val)
                        <option value="{{ $val->id }}" {{ ($info_job->category_main_id == $val->id) ? 'selected' : '' }}>{{ $val->name }}</option>
                    @endforeach
                </select>
			</div>
			<div class="col-md-3" data-name="category_sub">
                <select class="form-control" name="category_sub" id="post-job-category-sub">
                    <option value="0" data-par="0">Please select...</option>
                    @foreach($list_cat_sub as $val)
                        <option value="{{ $val->id }}" {!! ($info_job->category_main_id != $val->main_category) ? 'style="display: none;"' : '' !!} {{ ($info_job->category_sub_id == $val->id) ? 'selected' : '' }} data-par="{{ $val->main_category }}">{{ $val->name }}</option>
                    @endforeach
                </select>
			</div>
		</div>

        <div class="form-group" data-name="title">
			<label class="col-md-4 control-label">Title</label>
			<div class="col-md-6">
                <input type="text" class="form-control" name="title" value="{{ $info_job->title or '' }}">
			</div>
        </div>

        <div class="form-group" data-name="description">
			<label class="col-md-4 control-label">Describe the work</label>
			<div class="col-md-6">
                <textarea class="form-control" name="description" rows="10">{{ $info_job->description or '' }}</textarea>
			</div>
        </div>

        <div class="form-group" data-name="skills">
			<label class="col-md-4 control-label">Skills</label>
			<div class="col-md-6">
                <select id="my-skills" class="form-control" name="skills[]" multiple="multiple">
                @foreach ($info_job->skills as $val)
                    <option value="{{ $val->skill_id }}" selected="selected">{{ $val->skill_name }}</option>
                @endforeach
                </select>
			</div>
        </div>

	    <div class="form-group" data-name="type_price">
			<label class="col-md-4 control-label">How would you like to pay?</label>
			<div class="col-md-3">
                <select class="form-control" name="type_price" id="post-job-type-price">
                    <option value="hourly" {{ $info_job->type_price_hourly }}>Hourly - Pay by the hour</option>
                    <option value="fixed" {{ $info_job->type_price_fixed }}>Fixed Price - Pay by the project</option>
                </select>
            </div>
        </div>

        <div class="form-group post-job-fixed-price" data-name="fixed_price" style=" {{ $info_job->type_price_fixed_display }}">
			<label class="col-md-4 control-label">Budget</label>
            <div class="col-md-3">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" name="fixed_price" value="{{ $info_job->job_fixed_price or '' }}">
                    <span class="input-group-addon">.00</span>
                </div>
			</div>
        </div>

        <div class="form-group post-job-fixed-price" data-name="end_date" style=" {{ $info_job->type_price_fixed_display }}">
			<label class="col-md-4 control-label">Estimated End Date<br><span class="text-muted">(optional)</span></label>
            <div class="col-md-3 form-control-static">
                <div class="bfh-datepicker" data-format="y-m-d" data-name="end_date" data-date="{{ $info_job->job_end_date }}"></div>
			</div>
        </div>

	    <div class="form-group post-job-hourly-price" data-name="duration" style="{{ $info_job->type_price_hourly_display }}">
			<label class="col-md-4 control-label">Estimated Duration</label>
			<div class="col-md-3">
                <select class="form-control" name="duration">
                    <option value="">Please select...</option>
                    @foreach($durations as $key => $val)
                        <option value="{{ $key }}" {{ ($info_job->job_duration == $key) ? 'selected' : '' }}>{{ $val }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group" data-name="file">
			<label class="col-md-4 control-label">Attach a document</label>
            <div class="col-md-6">
                    <input type="file" name="file" class="">
                @if (isset($info_job->file_name) && !empty($info_job->file_name))
                        <a href="/{{ $info_job->download_link }}">{{ $info_job->file_name }}</a>&nbsp;&nbsp; <label class="control-label"><input type="checkbox" name="file_delete"> Delete File</label>
                @endif
			</div>
        </div>

        <div class="form-group" data-name="file">
			<div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary send-req-form">Post Job</button>
			</div>
        </div>


    </form>
    

</div>

<div>



</div>

{{ JsCss::addJs('post-job.js') }}
@endsection
