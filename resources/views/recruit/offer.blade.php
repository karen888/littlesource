@extends('app')

@section('content')

<div class="row padd-top">
@if (!empty($offer_info->user))
	<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="job_apply_id" value="{{ $offer_info->job_apply->id }}">

	    <div class="form-group" data-name="">
			<label class="col-md-4 control-label">Applicant</label>
            <div class="col-md-6 form-control-static">
                {{ $offer_info->user->name }}
            </div>
        </div>

	    <div class="form-group" data-name="">
			<label class="col-md-4 control-label">Job</label>
            <div class="col-md-6 form-control-static">
                {{ $offer_info->job_apply->title }}
            </div>
        </div>

        <div class="form-group" data-name="offer_text">
			<label class="col-md-4 control-label">Describe the offer</label>
			<div class="col-md-6">
                <textarea class="form-control" name="offer_text" rows="10"></textarea>
			</div>
        </div>

        @if ($offer_info->job_apply->job_apply_fixed_price == 0)
        <div class="form-group" data-name="card">
    		<label class="col-md-4 control-label">Billing method</label>
            <div class="col-md-3">
                <select name="card" class="form-control">
                    <option value="0">Please select...</option>
                    @foreach($offer_info->cards_list as $val)
                        <option value="{{ $val->id }}">{{ $val->card_description }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @endif

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary send-req-form">Send</button>
            </div>
        </div>

    </form>
@endif

@if ($offer_info->sent_offer)
    This applicant has already your offer!
@endif

@if ($offer_info->job_apply_active != 1)
    This applicant didn't approve your invite yet! 
@endif
</div>
@endsection
