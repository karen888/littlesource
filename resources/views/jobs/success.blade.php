@extends('app')

@section('content')

<div class="container padd-top-30 job-success" id="view_job_success">
    <input type="hidden" v-model="job_id" value="{{ $job->id }}">
    @if(!$view)
    <div class="row">
        <div class="col-md-12">
            <div class="main-heading">
                <h4>Success! Your job was submitted.</h4>
                @unless($job->marketplace_visibility_id == 1)
                <h5>Your job has been posted privately. Invite some caregivers to apply!</h5>
                @endunless
            </div>
        </div>
    </div>
    @endif
    <hr>
    <notifications
        :value="notificationValues"
        v-if="displayNotification"
        @notification="handleNotification">
    </notifications>
    <div class="row">
        <div class="col-md-12">
            <div class="job-title">
                <h5><a href="{{route('caregiver.jobs.details', [$job->id])}}">{{ $job->title }}</a></h5>
            </div>
            <!-- how do i get the hourly rate? -->
            Posted {{ $job->created_at->diffForHumans() }} — <a href="{{ route('client.jobs.my-jobs.edit', $job) }}">Edit job</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="description">
                {{ $job->description }}
            </div>
        </div>
    </div>
    <hr>
    <?php
        $_employee = [];

        foreach($employee as $k=>$e) {
            if($e !== null) {
                $_employee[] = $e;
            }
        }


        $employee = $_employee;


    ?>
    @if(!empty($employee[0]))
        <div class="row">
            <div class="col-md-9">
                <div class="recommendation">
                    <h5>The top {{ count($employee) }} recommended caregivers for your job:</h5>
                </div>
            </div>
            <div class="col-md-3">
                <a href="javascript:;" class="btn btn-themed btn-invite pull-right" @click.stop="setInviteAll()">Invite All to Apply</a>
            </div>
        </div>

        <div class="caregivers">
            @foreach($employee as $item)
                <?php

                if(!App\User::find($item['id']) || !App\User::find($item['id'])->is_caregiver || !\App\User::find($item['id'])->canApplyToJob()) {
                    continue;
                }
                ?>
                <div class="caregiver employee_list" data-id="{{ $item['id'] }}" >
                    <img width="36" height="36" src="{{ $item['photo_url'] }}" alt="">
                    <div class="name">
                        {{ $item['name'] }}
                    </div>
                </div>
            @endforeach
        </div>

        <div class="row">
            @include('employee.profile.view', ['includeHr' => true, 'employee' => $employee[0]])
        </div>
    @else
        <div class="panel">
            <div class="panel-row empty">
                <h5>Your recommended caregivers list is empty.</h5>
            </div>
        </div>
    @endif
</div>


@endsection
