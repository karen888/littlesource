<div class="modal" id="advanced" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Advanced search</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="cc-number" class="control-label">All of these words </label>
                            <input class="form-control" v-model="advanced_options.all_words">

                        </div>
                        <div class="form-group">
                            <label for="cc-number" class="control-label">Any of these words </label>
                            <input class="form-control" v-model="advanced_options.any_words">

                        </div>
                        <div class="form-group">
                            <label for="cc-number" class="control-label">Title Search</label>
                            <input class="form-control" v-model="advanced_options.title">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <input type="checkbox" id="option3" name="locked-status" v-model="advanced_options.select">
                                <label for="option3">
                                    Select this option if you want to search job that only fits your declared qualification.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" data-dismiss="modal" @click="setAdvanced(true)">Search</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal">Close</a>
                </div>
            </div>
        </div>
    </div>
</div>