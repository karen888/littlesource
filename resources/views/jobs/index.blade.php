@extends('app')

@section('content')

<div class="container padd-top-30">
    @include('flash::message')
    <div class="row">
        <div class="col-md-10">
            <div class="main-heading">
                <h4>Jobs</h4>
            </div>
        </div>
        <div class="col-md-2">
            <a href="{{ route('client.jobs.create') }}" class="btn btn-themed btn-primary btn-main-action pull-right">Post Job</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5>Open Jobs</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel job-postings job_list">
            @if($jobs->isEmpty())
                <div class="panel-row empty">
                    <h5>You currently have no open job postings</h5>
                </div>
            @else
            @foreach ($jobs as $job)
                <div class="panel-row">
                    <div class="values">
                        <div class="job-details">
                            <div class="name">
                                <a href="{{ route('client.jobs.my-jobs.details', ['job' => $job->id]) }}"><strong> {{ str_limit( $job->title , 100) }}</strong></a>
                            </div>
                            <div class="date-posted">
                                Posted {{ $job->created_at->diffForHumans() }}
                            </div>
                        </div>
                        <div class="applicants">
                            <strong>{{ $job->jobProposalCount() }}</strong>
                            Proposals
                        </div>
                        <div class="messaged">
                            <strong>{{ $job->jobInterviewCount() }}</strong>
                            Interviewing
                        </div>
                        <div class="offers">
                            <strong>{{ $job->jobOffersCount() }}</strong>
                            <span>Offers</span>
                        </div>
                        <div class="invite">
                            @unless($job->marketplace_visibility_id == 1)
                                <div class="skill">Invite-Only</div>
                            @endunless
                        </div>
                        <div class="actions">
                            <div class="actions-group">
                                <div class="trigger">
                                    <div class="text_outer"><div class="text trn"> Actions </div></div>
                                </div>
                                <div class="list">
                                    <div class="option"
                                    data-action="edit-job"
                                    class="trn">
                                        <a href="{{ route('client.jobs.my-jobs.edit', ['job' => $job->id]) }}">Edit Job</a>
                                    </div>
                                    <!--<div class="option"
                                    data-action="view-profile"
                                    class="trn">
                                        <a href="{{ route('client.jobs.my-jobs.details', ['job' => $job->id]) }}">View Details</a>
                                    </div>-->
                                    <div class="option"
                                    data-action="send-message"
                                    class="trn">
                                        <a href="{{ route('client.jobs.my-jobs.view-applicants', ['job' => $job->id]) }}">View Applicants</a>
                                    </div>
                                    <div class="option"
                                    data-action="send-message"
                                    class="trn">
                                        <a href="{{ route('client.employee.find') }}">Invite Caregivers</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a class="job-posting-action" href="{{ route('client.jobs.my-jobs.all') }}">View all job postings</a>
        </div>
    </div>
    {{--<div class="row">
        <div class="col-md-12">
            <h5>Contracts</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel job-postings">
            @if($offers->isEmpty())
                <div class="panel-row empty">
                    <h5>You have no active contracts. Active contracts will show up here as soon you hire someone!</h5>
                    <p></p>
                </div>
            @else
                @foreach($offers as $item)
                    <div class="panel-row">
                        <div class="values">
                            <div class="job-details">
                                <div class="name">
                                    <strong> {{ str_limit($item->job->title, 100) }}</strong>
                                </div>
                                <div class="date-posted">
                                    Posted {{ $item->job->created_at->diffForHumans() }}
                                </div>
                            </div>
                            <div class="caregiver">
                                <img class="img_50" src="{{ $item->employee->PhotoUrl }}" alt="">
                                <strong>{{ $item->employee->name }}</strong>
                            </div>
                            <div class="rates">
                                ${{$item->offer_paid}}/hour
                            </div>
                            <div class="actions">
                                <div class="actions-group">
                                    <div class="trigger">
                                        <div class="text_outer"><div class="text trn"> Actions </div></div>
                                    </div>
                                    <div class="list">
                                        <div class="option"
                                             data-action="view-profile"
                                             class="trn">
                                            <a href="{{ route('employee.profile', [$item->employee->id]) }}">View Profile</a>
                                        </div>
                                        <div class="option"
                                        data-action="view-profile"
                                        class="trn">
                                            <a href="{{ route('jobs.contracts.details', $item->id) }}">View Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <a class="job-posting-action" href="{{ route('client.jobs.contracts') }}">View all contracts</a>
        </div>
    </div>--}}
</div>

@endsection
