@extends('app')

@section('content')
    <div class="container padd-top-40">
        @if($jobInterview)
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                You have already been apply on this job
            </div>
        @endif
        <div class="row back-action">
            <div class="col-md-12">
                <a href="{{ URL::previous() }}" class="back-button"><span class="back-icon"></span>Back to Previous Page</a>
            </div>
        </div>        
        <h4>
            {{ $job->title }}
        </h4>
        <div class="date-posted">Posted {{ $job->created_at->diffForHumans() }}</div>
        @if($proposal && $is_caregiver)
        <div class="about-client already-submited">
            <div><h6>You have already submited for this proposal</h6></div>
        </div>
        @endif
        <div class="caregiver-details">
            <div class="details-and-proposals">
                <div class="panel">
                    <div class="panel-row">
                        <div class="details">
                            <h5>Details</h5>
                            <p>{{ $job->description }}</p>
                        </div>
                    </div>
                    <div class="panel-row">
                        <div class="qualifications-and-activity">
                            <div class="qualifications">
                                <h6>Preferred Qualifications</h6>
                                <ul>
                                    <li><strong>Rate:</strong> {{ $job->minimumFeedback->value }}</li>
                                    <li><strong>Experience:</strong> {{ $job->minimumExperience->value }}</li>
                                    <li><strong>Duration:</strong> {{ $job->jobDuration->name }}</li>
                                    <li><strong>Workload:</strong> {{ $job->jobWorkload->name }}</li>
                                    <li><strong>Minimum Feedback:</strong> {{ $job->minimumFeedback->value }}</li>
                                    <li><strong>Allowed Locations:</strong> {{ $job->allowedLocations->value }}</li>
                                    @if ($job->get_have_transportation)
                                        <li><strong>Drivers licence:</strong> {{ $job->get_have_transportation->name }}</li>
                                    @endif
                                    @if ($job->get_gender)
                                        <li><strong>Gender:</strong> {{ $job->get_gender->name }}</li>
                                    @endif
                                    @if ($job->get_smoker)
                                        <li><strong>Smoker:</strong> {{ $job->get_smoker->name }}</li>
                                    @endif
                                    @if ($job->get_comfortable_with_pets)
                                        <li><strong>Comfortable with pets:</strong> {{ $job->get_comfortable_with_pets->name }}</li>
                                    @endif
                                    @if ($job->get_own_children)
                                        <li><strong>Has own children:</strong> {{ $job->get_own_children->name }}</li>
                                    @endif
                                    @if ($job->get_sick_children)
                                        <li><strong>Willing to care for sick children:</strong> {{ $job->get_sick_children->name }}</li>
                                    @endif
                                    @if ($child_age_range)
                                        <li><strong>Child Age Rang:</strong> {{ $child_age_range }}</li>
                                    @endif
                                    @if ($age)
                                        <li><strong>Employee Age:</strong> {{ $age }}</li>
                                    @endif
                                    @if ($qualifications)
                                        <li><strong>Qualifications:</strong> {{ $qualifications }}</li>
                                    @endif
                                    @if ($questions)
                                        <li><strong>Questions:</strong></li>
                                        @foreach($questions as $q)
                                            <li><strong>{{ $q }}</strong></li>
                                        @endforeach
                                    @endif

                                </ul>
                            </div>
                            <div class="client-activity">
                                <h6>Client Activity on this Job</h6>
                                <ul>
                                    <li><strong>Last Viewed:</strong> 2 hours ago</li>
                                    <li><strong>Proposals:</strong> {{ $job->jobProposalCount() }}</li>
                                    <li><strong>Interviewing:</strong> {{ $job->jobInterviewCount() }}</li>
                                    <li><strong>Offers:</strong> {{ $job->jobOffersCount() }}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @if($jobInvite)
                    <div class="panel proposals">
                        <div class="panel-row">
                            <h5>Message from Client</h5>
                        </div>
                        <div class="panel-row">
                            <div>{!! $jobInvite->message !!}</div>
                            <br>
                            <div>{{ $job->user->name }}</div>
                        </div>
                    </div>
                @endif
            </div>
            @if($jobInvite && $is_caregiver)
                <div class="about-client">
                    <div><h5>Invitation to Interview</h5></div>
                    <div>Client: {{ $job->user->name }}</div>
                    <div>Are you interested in discussing this job?</div>
                    <div class="actions">
                        <a class="btn btn-themed btn-primary btn-main-action full-width" onclick="$('#accept-invite').modal('show')">YES, ACCEPT INTERVIEW</a>
                        <a type="button" class="btn btn-themed btn-preview full-width" onclick="$('#decline-invite').modal('show')">DECLINE</a>
                    </div>
                </div>
            @elseif(!$proposal && !$offer && $is_caregiver)
                <div class="about-client">
                    <div class="actions">
                        <a class="btn btn-themed btn-primary btn-main-action full-width" onclick="$('#submit-proposal').modal('show')">Submit A Proposal</a>
                    </div>
                </div>
            @endif
            <div class="about-client">
                <div class="panel">
                    <div class="panel-row">
                        <div class="details">
                            <h5>
                                About the client
                            </h5>
                        </div>
                    </div>
                    <div class="panel-row">
                        <div class="caregiver-info">
                            <div class="caregiver">
                                <img class="img_50" style="width: 50px; height:50px; border-radius: 100%; -webkit-border-radius:100%;" src="{{ $job->user->photo_url }}" alt="">
                                <div class="name"><strong>{{ $job->user->name }}</strong></div>
                            </div>
                            <div class="payment-method">Payment Method Not Verified</div>

                            <div class="country"><strong>{{ $job->user->info->country->name or '' }}</strong></div>
                            <div class="city">{{ $job->user->info->city or ''}} {{ $job->user->info->state or '' }} {{ isset($job->user->info->timezone_id) ? '(' . \Carbon\Carbon::now()->setTimezone($job->user->info->timezone->php_timezone)->format('h:iA') . ')' : '' }}</div>

                            <div class="hire-rate">
                                <strong> {{ $job->user->jobsCount }} {{ $job->user->jobsCount > 1 ?  'Jobs' : 'Job' }}  Posted</strong> <br>
                                50% Hire Rate, {{ $job->user->jobsOpenCount }} Open {{ $job->user->jobsOpenCount > 1 ?  'Jobs' : 'Job' }}
                            </div>
                            @if($proposal && $is_caregiver)
                             <div class="actionss" style="padding: 15px; margin-bottom: -30px;">
                                <a class="btn btn-themed btn-primary btn-main-action" href="{{ route('messages.index', ['direct' => $job->user->id]) }}">Send Message</a>
                              </div>
                            @endif
                            <div class="member-since">Member since: {{ $job->user->created_at->diffForHumans() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    @if($jobInvite)
        <div id="accept-invite-job">
            @include('jobs.modals.accept-invite')
            @include('jobs.modals.decline-invite')
            <input type="hidden" name="job_invite" v-model="job_invite" value="{{ $jobInvite->id }}" />
            <input type="hidden" name="job_id" v-model="job_id" value="{{ $job->id }}" />
        </div>
    @endif
    @include('jobs.modals.submit-proposal', ['job_id' => $job->id])
@endsection
