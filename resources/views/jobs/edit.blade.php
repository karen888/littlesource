@extends('app')

@section('content')

<div class="container padd-top-30" id="create-job">
    <div class="row back-action">
        <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="back-button"><span class="back-icon"></span>Back to Previous Page</a>
        </div>
    </div>    
    <div class="row">
        <div class="col-md-12">
            <div class="main-heading">
                <h4>Edit Job <span v-show="previewMode">— Preview</span></h4>
            </div>
        </div>
    </div>  
    <div :class="['job-form', previewMode ? 'job-form--preview' : '']" >
        {!! Form::open(['method'=> 'PATCH', 'url' => ['api/v1/clients/job', $job->id], 'id' => 'job-form']) !!}
            @include('jobs.form_edit', ['edit' => true, 'ocupation' => $job->ocupation])
        {!! Form::close() !!}
    </div>
</div>

@endsection
