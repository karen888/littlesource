@extends('app')

<?php $applicants = collect(['']); ?>

@section('content')

<script>
    var jobId = {{ $job->id }};
</script>

<div id="applicants" class="container padd-top-30">
    <notifications
        :value="notificationValues"
        v-if="displayNotification"
        @notification="handleNotification">
    </notifications>
    <div class="row back-action">
        <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="back-button"><span class="back-icon"></span>Back to Previous Page</a>
        </div>
    </div>        
    <div class="row">
        <div class="col-md-12">
            <h4>{{ $job->title }}</h4>
            <div class="job-status">{{ $job->closed }} — Posted {{ $job->created_at->diffForHumans() }} — <a href="{{ route('client.jobs.my-jobs.details', $job->id)}}">View</a> or <a href="{{ route('client.jobs.my-jobs.edit', $job->id) }}">Edit</a> this job post</div>
        </div>
    </div>
    <div class="applicants-filter">
        <div class="filter">
            <strong>Sort by:</strong>
            <custom-select
                :options.sync="filters"
                :selected.sync="filterSelected"
                :prevent="true"
                :name="filter"
                @changed-option="handleFilter">
            </custom-select>
        </div>
    </div>
    <div class="applicants">
        <applicant-list
            @shortlist-applicant="handleShortlist"
            @hide-applicant="handleHidden"
            :current-list.sync="currentList"
            :list-name="currentRoute"
            view-recommended="{{ route('client.jobs.create.success', ['job' => $job->id, 'view' => true]) }}">
        </applicant-list>
        <div class="menu">
            <div class="panel">
                <nav>
                    <ul>
                        <li :class="[applicantsActive ? 'active' : '']">
                            <a href="#" @click="setActive('applicants')" title="">
                                <span class="icon icon-applicants"></span> <span class="text">Applicants</span> <span class="quantity" v-html="applicants"></span>
                            </a>
                        </li>
                        <li :class="[shortlistedActive ? 'active' : '']">
                            <a href="#" @click="setActive('shortlisted')" title="">
                                <span class="icon icon-shortlisted"></span> <span class="text">Shortlisted</span> <span class="quantity" v-html="shortlisted"></span>
                            </a>
                        </li>
                        <li :class="[messagedActive ? 'active' : '']">
                            <a href="#" @click="setActive('messaged')" title="">
                                <span class="icon icon-messaged"></span> <span class="text">Messaged</span> <span class="quantity" v-html="messaged"></span>
                            </a>
                        </li>
                        <li :class="[hiddenActive ? 'active' : '']">
                            <a href="#" @click="setActive('hidden')" title="">
                                <span class="icon icon-hidden"></span> <span class="text">Hidden</span> <span class="quantity" v-html="hidden"></span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>

@endsection
