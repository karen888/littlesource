@extends('app')

@section('content')

<div class="container padd-top-30" id="All-jobs">
    <div class="row back-action">
        <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="back-button"><span class="back-icon"></span>Back to Previous Page</a>
        </div>
    </div>
    <vc-job jobs="{{$jobs}}"></vc-job>
 </div>
    
@endsection
