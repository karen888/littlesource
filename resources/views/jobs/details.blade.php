@extends('app')

@section('content')

<div class="container padd-top-40">
    <div class="row back-action">  
        <div class="col-md-12">
            <a class="back-button" onclick="window.history.back();" href="javascript:void(0);"><span class="back-icon"></span>Back to Previous Page</a>
        </div>
    </div>
    @include('flash::message')
    <div class="panel" >
        <div class="panel-row">
            <nav class="job-details-nav">
                <ul class="list-inline" >
                    <li><a href="{{ route('client.jobs.my-jobs.edit', $job) }}" title="Edit Job"><span class="icon icon-edit-job"></span> <span class="menu-item">Edit Job</span></a></li>
                        <input type="hidden" id="conditional" value="{{$job->closed}}">
                        <li id="close_job"  style="display:none" data-url="{{ route('api.v1.clients.job.close', [$job->id]) }}">
                            <a href="#" title="Close Job" >
                                <span class="icon icon-close-job"></span>  <span class="menu-item">Close Job</span>
                            </a>      
                        </li>
                        <li id="repost_job" style="display:none"  data-url="{{ route('api.v1.clients.job.repost_job', [$job->id]) }}">  
                            <a href="#" title="Repost Job" >
                                <span class="icon icon-repost-job"></span>  <span class="menu-item">Repost Job</span>
                            </a>      
                        </li>
                        @unless($job->marketplace_visibility_id == 1)
                         <li id="make_job_public" data-url="{{ route('api.v1.clients.job.make_public', [$job->id]) }}">
                             <a href="#" title="Make Public">
                                <span class="icon icon-make-public"></span>  <span class="menu-item">Make Public</span>
                             </a>
                         </li>
                        @endunless
                <div class="all-job-applicants">
                    <a href="{{ route('client.jobs.my-jobs.view-applicants',$job->id) }}">View Job Applicants</a>
                </div>
                </ul>
            </nav>
        </div>
    </div>
    <h4>
        {{ $job->title }}
    </h4>
    <div class="date-posted">Posted {{ $job->created_at->diffForHumans() }}</div>
    <div class="caregiver-details">
        <div class="details-and-proposals">
            <div class="panel">
                <div class="panel-row">
                    <div class="details">
                        <h5>Details</h5>
                        <p>{{ $job->description }}</p>
                    </div>
                </div>
                <div class="panel-row">
                    <div class="qualifications-and-activity">
                        <div class="qualifications">
                            <h6>Preferred Qualifications</h6>
                            <ul>
                                <li><strong>Rate:</strong> {{ $job->minimumFeedback->value }}</li>
                                <li><strong>Experience:</strong> {{ $job->minimumExperience->value }}</li>
                            </ul>
                        </div>
                        <div class="client-activity">
                            <h6>Client Activity on this Job</h6>
                            <ul>
                                <li><strong>Last Viewed:</strong> 2 hours ago</li>
                                <li><strong>Proposals:</strong> {{ $job->jobProposalCount() }}</li>
                                <li><strong>Interviewing:</strong> {{ $job->jobInterviewCount() }}</li>
                                <li><strong>Offers:</strong> {{ $job->jobOffersCount() }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            {{--<div class="panel proposals">--}}
                {{--<div class="panel-row">--}}
                    {{--<h6><a href="#"> <span class="icon icon-proposals-active"></span> Proposals (3)</a></h6>--}}
                    {{--<div class="headings">--}}
                        {{--<div class="caregiver-name"><strong>Caregiver</strong></div>--}}
                        {{--<div class="proposal-date"><strong>Proposal date</strong></div>--}}
                        {{--<div class="initiated-by"><strong>Initiated by</strong></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--@for($i = 0; $i < 3; $i++)--}}
                {{--<div class="panel-row">--}}
                    {{--<div class="caregiver-name"><a href="#">Gabrielle Gribble</a></div>--}}
                    {{--<div class="proposal-date">21 hours ago</div>--}}
                    {{--<div class="initiated-by">Client</div>--}}
                {{--</div>--}}
                {{--@endfor--}}
            {{--</div>--}}
        </div>
        <div class="about-client">
            <div class="panel">
                <div class="panel-row">
                    <div class="details">
                        <h5>
                            About the client
                        </h5>
                    </div>
                </div>
                <div class="panel-row">
                    <div class="caregiver-info">
                        <div class="caregiver">
                            <img class="img_50" width="50px" height="50px" src="{{ $job->user->photo_url }}" alt="">
                            <div class="name"><a href="/employee/{{ $job->user->id }}"><strong>{{ $job->user->name }}</strong></a></div>
                        </div>
                        <div class="payment-method">Payment Method Not Verified</div>

                        <div class="country"><strong>{{ $job->user->info->country->name or '' }}</strong></div>
                        <div class="city">{{ $job->user->info->city or ''}} {{ $job->user->info->state or '' }} {{ isset($job->user->info->timezone_id) ? '(' . \Carbon\Carbon::now()->setTimezone($job->user->info->timezone->php_timezone)->format('h:iA') . ')' : '' }}</div>

                        <div class="hire-rate">
                            <strong> {{ $job->user->jobsCount }} {{ $job->user->jobsCount > 1 ?  'Jobs' : 'Job' }}  Posted</strong> <br>
                            50% Hire Rate, {{ $job->user->jobsOpenCount }} Open {{ $job->user->jobsOpenCount > 1 ?  'Jobs' : 'Job' }}
                        </div>

                        <div class="member-since">Member since: {{ $job->user->created_at->diffForHumans() }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection
