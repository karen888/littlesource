@extends('app')

@section('content')

<div class="container padd-top-30" id="create-job">
    <div class="row">
        <div class="col-md-12">
            <div class="main-heading">
                <h4>Post a Job <span v-show="previewMode">— Preview</span></h4>
            </div>
        </div>
    </div>

    <div :class="['job-form', previewMode ? 'job-form--preview' : '']">
        <!--{!! Form::open(['class' => 'create-job']) !!}-->
            @include('jobs.form')
        <!--{!! Form::close() !!}-->
    </div>
</div>

@endsection
