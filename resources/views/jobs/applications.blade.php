@extends('app')

@section('content')

<div class="page-header">
<h3>Job Applications</h3>
</div>

<div class="row">

    <div class="panel panel-default">
        <div class="panel-heading">Offers</div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Sent</th>
                        <th>Offer</th>
                        <th>Client</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($offers_appl as $val)
                    <tr>
                        <td class="col-md-3">{{ $val->created_at }}</td>
                        <td><a href="{{ routeClear('home_offer') }}/{{ $val->id }}">{{ $val->title }}</a></td>
                        <td>{{ $val->employer_name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Invitations to Interview</div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Sent</th>
                        <th>Invite</th>
                        <th>Client</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invites_appl as $val)
                    <tr>
                        <td class="col-md-3">{{ $val->created_at }}</td>
                        <td><a href="{{ $val->link_invite }}">{{ $val->title }}</a></td>
                        <td>{{ $val->employer_name }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading">Sent Job Applications</div>
        <div class="panel-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>Sent</th>
                        <th>Job</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($sent_appl as $val)
                    <tr>
                        <td class="col-md-3">{{ $val->created_at }}</td>
                        <td><a href="{{ $val->link_job }}">{{ $val->title }}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


</div>
@endsection

