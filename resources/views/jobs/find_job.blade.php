@extends('app')

@section('content')

<div class="page-header">
<h3>Find Job</h3>
</div>

<div class="row">

    <div class="col-md-8 col-md-offset-3">
	<form class="form-horizontal" role="form" method="GET" action="" enctype="multipart/form-data" >
	    <div class="form-group">
            <div class="col-md-5">
                <input type="text" class="form-control" name="search" value="{{ $search or '' }}">
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-primary">Search</button>
            </div>
        </div>
    </form>
    </div>
</div>

<div class="row">
    @foreach($find_result as $val)
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="pull-left">
                    <a href="{{ $val->path }}"><b>{{ $val->title }}</b></a>
                </div>
                <div class="pull-right text-muted text-small">
                    @if ($val->job_fixed_price != '0.00')
                        <b>Fixed</b> Price - Est.Budget: ${{ $val->job_fixed_price }} - Posted {{ $val->created_at }}
                    @else
                        <b>Hourly</b> - Est.Time: {{ $val->job_duration }} - Posted {{ $val->created_at }}
                    @endif
                </div>

                <div class="clearfix"></div>
            </div>
            <div class="panel-body">
                {{ $val->description }}
            </div>
            <div class="panel-footer text-muted">
                Skills: 
                @foreach($val->skills as $val)
                    <a href="javascript:void(0);" class="btn btn-default btn-xs">{{ $val->skill_name }}</a>
                @endforeach
            </div>
        </div>
    @endforeach
</div>
<div class="row">
    @if (!empty($find_result)) 
        {!! $find_result->appends(\Input::all())->render() !!} 
    @endif
</div>
@endsection
