@extends('app')

@section('content')

<div class="container padd-top-30">
    <div class="row">
        <div class="col-md-12">
            <div class="main-heading">
                <h4>Edit Job — Preview</h4>
            </div>
        </div>
    </div>

    <div class="job-form job-form--preview">
        <form>
            @include('jobs.form', ['preview' => true])
        </form>
    </div>
</div>

@endsection
