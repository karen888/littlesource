@extends('app')

@section('content')

<div id="content-wrap" class="content-wrap">
    <div class="container">
        <div class="content">
            <div class="caregiver-profile" id="employee-profile">
                <div class="col-md-12 search_blok">
                    <div class="col-md-3">
                        <div class="details">
                            <h4>Job Search</h4>


                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="col-md-9">
                            <input type="text" class="form-control" aria-label="..." placeholder="">

                            <a class="" data-toggle="modal" href="#myModal" >Advanced search</a>
                        </div>
                        <div class="col-md-3">
                             <button class="btn btn-default left" id="become_caregiver">Search</button>
                        </div>
                    </div>

                </div>

                <div class="col-md-3 left_drop">

                    <div class="dropdown border_b">
                        <p> Category </p>
                        <button class="all_cat btn-primary dropdown-toggle" type="button" data-toggle="dropdown">lorem ipsum
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">lorem ipsum</a></li>
                            <li><a href="#">lorem ipsum</a></li>
                            <li><a href="#">lorem ipsum</a></li>
                        </ul>
                    </div>
                    <div class="border_b">


                        <div class="col-md-12">
                            <p>Checkbox Buttons</p>

                            <div class="funkyradio">

                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox1" />
                                    <label for="checkbox1">Third Option success</label>
                                </div>
                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox2" checked/>
                                    <label for="checkbox2">Third Option success</label>
                                </div>
                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox3" checked/>
                                    <label for="checkbox3">Third Option success</label>
                                </div>

                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox4" checked/>
                                    <label for="checkbox4">Third Option success</label>
                                </div>
                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox6" checked/>
                                    <label for="checkbox5">Third Option success</label>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="dropdown border_b">
                        <p> Location </p>
                        <button class="all_cat btn-primary dropdown-toggle" type="button" data-toggle="dropdown">lorem ipsum
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">lorem ipsum</a></li>
                            <li><a href="#">lorem ipsum</a></li>
                            <li><a href="#">lorem ipsum</a></li>
                        </ul>
                    </div>
                    <div class="border_b">


                        <div class="col-md-12">
                            <p>Checkbox Buttons</p>

                            <div class="funkyradio">


                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox1" />
                                    <label for="checkbox1">Third Option success</label>
                                </div>
                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox2" checked/>
                                    <label for="checkbox2">Third Option success</label>
                                </div>
                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox3" checked/>
                                    <label for="checkbox3">Third Option success</label>
                                </div>

                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox4" checked/>
                                    <label for="checkbox4">Third Option success</label>
                                </div>
                                <div class="funkyradio-success">
                                    <input type="checkbox" name="checkbox" id="checkbox6" checked/>
                                    <label for="checkbox5">Third Option success</label>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="col-md-9">

                        <div class="panel job-postings">
                            <div class="panel-row empty">
                                <h5> Your Search was successfully saved and added to your job Feed </h5>

                            </div>
                        </div>
                    <br>

                    {{--<div class="info_success">--}}
                        {{--<button class="close_info_success" data-dismiss="">×</button>--}}
                        {{--<p>Your Search was successfully saved and added to your job Feed <span class="glyphicon glyphicon-info-sign"></span></p>--}}
                    {{--</div>--}}
                    <div class="save_banner">

                            <div class="col-md-9">
                                <div class="col-md-9">

                                    <a href="#">If you like this search ,you can save it to your Job Feed</a>
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-default" data-toggle="modal" href="#mySave" >Save this Search</button>
                                </div>
                            </div>
                    </div>

                </div>
                <div class="col-md-9 ">


                    <div class="work-history">
                        <div class="heading">
                            <h5>Search results (25)</h5>
                        </div>
                        <div class="history-filter">
                            <div class="pull-right">
                                <div class="customselect v2">
                                    <select name="previous-jobs" id="previous-jobs">
                                        <option value="1">Newest first</option>
                                        <option value="2">Oldest First</option>
                                    </select>
                                    <div class="trigger">
                                        <div class="text_outer"><div class="text trn">
                                                Newest first
                                            </div></div>
                                    </div>
                                    <div class="list">
                                        <div class="option active" data-value="1" data-selected="1">
                                            <span class="option-name">Newest first</span>
                                        </div>
                                        <div class="option " data-value="2" data-selected="0">
                                            <span class="option-name">Oldest First</span>
                                        </div>
                                    </div>
                                </div>                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="feedback-row">
                            <div class="job">
                                <div class="job-name"><a href="#">Caregiver for two girls</a></div>
                                <div class="job-rating">
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    5.0
                                </div>
                            </div>
                            <div class="feedback">
                                <div class="description">
                                    “Stella was amazing! We had her watch our 7 month old daughter while we were on vacation in Laguna Beach. Stella was prompt to arrive at our hotel and happy to sit in the small hotel room (no TV) and read a book while the baby slept. She was warm, friendly and caring. I would recommend Joanne very much.”

                                </div>
                                <div class="rates">
                                    <div class="rate">
                                        About the client
                                    </div>
                                    <div class="timespan">
                                        Brisbane QLD
                                    </div>

                                </div>
                            </div>
                            <div class="skills">Skills : <span class="skill">housekeeping</span><span class="skill" >part-time</span>

                                <a class="btn btn-themed btn-preview" id="become_caregiver" data-toggle="modal" href="#mySave" >Save</a>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="feedback-row">
                            <div class="job">
                                <div class="job-name"><a href="#">Caregiver for two girls</a></div>
                                <div class="job-rating">
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    5.0
                                </div>
                            </div>
                            <div class="feedback">
                                <div class="description">
                                    “Stella was amazing! We had her watch our 7 month old daughter while we were on vacation in Laguna Beach. Stella was prompt to arrive at our hotel and happy to sit in the small hotel room (no TV) and read a book while the baby slept. She was warm, friendly and caring. I would recommend Joanne very much.”

                                </div>
                                <div class="rates">
                                    <div class="rate">
                                        About the client
                                    </div>
                                    <div class="timespan">
                                        Brisbane QLD
                                    </div>

                                </div>
                            </div>
                            <div class="skills">Skills : <span class="skill">housekeeping</span><span class="skill" >part-time</span>

                                <a class="btn btn-themed btn-preview" id="become_caregiver" data-toggle="modal" href="#mySave" >Save</a>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="feedback-row">
                            <div class="job">
                                <div class="job-name"><a href="#">Caregiver for two girls</a></div>
                                <div class="job-rating">
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    5.0
                                </div>
                            </div>
                            <div class="feedback">
                                <div class="description">
                                    “Stella was amazing! We had her watch our 7 month old daughter while we were on vacation in Laguna Beach. Stella was prompt to arrive at our hotel and happy to sit in the small hotel room (no TV) and read a book while the baby slept. She was warm, friendly and caring. I would recommend Joanne very much.”

                                </div>
                                <div class="rates">
                                    <div class="rate">
                                        About the client
                                    </div>
                                    <div class="timespan">
                                        Brisbane QLD
                                    </div>

                                </div>
                            </div>
                            <div class="skills">Skills : <span class="skill">housekeeping</span><span class="skill" >part-time</span>

                                <a class="btn btn-themed btn-preview" id="become_caregiver" data-toggle="modal" href="#mySave" >Save</a>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="feedback-row">
                            <div class="job">
                                <div class="job-name"><a href="#">Caregiver for two girls</a></div>
                                <div class="job-rating">
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    5.0
                                </div>
                            </div>
                            <div class="feedback">
                                <div class="description">
                                    “Stella was amazing! We had her watch our 7 month old daughter while we were on vacation in Laguna Beach. Stella was prompt to arrive at our hotel and happy to sit in the small hotel room (no TV) and read a book while the baby slept. She was warm, friendly and caring. I would recommend Joanne very much.”

                                </div>
                                <div class="rates">
                                    <div class="rate">
                                        About the client
                                    </div>
                                    <div class="timespan">
                                        Brisbane QLD
                                    </div>

                                </div>
                            </div>
                            <div class="skills">Skills : <span class="skill">housekeeping</span><span class="skill" >part-time</span>

                                <a class="btn btn-themed btn-preview" id="become_caregiver" data-toggle="modal" href="#mySave" >Save</a>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="feedback-row">
                            <div class="job">
                                <div class="job-name"><a href="#">Caregiver for two girls</a></div>
                                <div class="job-rating">
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    5.0
                                </div>
                            </div>
                            <div class="feedback">
                                <div class="description">
                                    “Stella was amazing! We had her watch our 7 month old daughter while we were on vacation in Laguna Beach. Stella was prompt to arrive at our hotel and happy to sit in the small hotel room (no TV) and read a book while the baby slept. She was warm, friendly and caring. I would recommend Joanne very much.”

                                </div>
                                <div class="rates">
                                    <div class="rate">
                                        About the client
                                    </div>
                                    <div class="timespan">
                                        Brisbane QLD
                                    </div>

                                </div>
                            </div>
                            <div class="skills">Skills : <span class="skill">housekeeping</span><span class="skill" >part-time</span>

                                <a class="btn btn-themed btn-preview" id="become_caregiver" data-toggle="modal" href="#mySave" >Save</a>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="feedback-row">
                            <div class="job">
                                <div class="job-name"><a href="#">Caregiver for two girls</a></div>
                                <div class="job-rating">
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    <div class="icon-star"></div>
                                    5.0
                                </div>
                            </div>
                            <div class="feedback">
                                <div class="description">
                                    “Stella was amazing! We had her watch our 7 month old daughter while we were on vacation in Laguna Beach. Stella was prompt to arrive at our hotel and happy to sit in the small hotel room (no TV) and read a book while the baby slept. She was warm, friendly and caring. I would recommend Joanne very much.”

                                </div>
                                <div class="rates">
                                    <div class="rate">
                                        About the client
                                    </div>
                                    <div class="timespan">
                                        Brisbane QLD
                                    </div>

                                </div>
                            </div>
                            <div class="skills">Skills : <span class="skill">housekeeping</span><span class="skill" >part-time</span>

                                <a class="btn btn-themed btn-preview" id="become_caregiver" data-toggle="modal" href="#mySave" >Save</a>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                        </div>


                    </div>

                    <ul class="pagination pagination-sm">
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                    </ul>

                </div>
                <!-- Caregiver Options -->


            </div>
        </div>
    </div>
</div>

<div class="modal" id="mySave">
    <div class="modal-header">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <form action="">
                        <div class="Message__composer modal-transition">
                            <div class="Message__composer__content">
                                <h3 class="Message__composer--heading">Save this Search</h3>
                                <div class="Message__composer--body">

                                    <div class="Message__composer__form">
                                        <div class="form-group">
                                            <label for="cc-number" class="control-label">Search Query </label>
                                            <input  class="form-control ">

                                        </div>
                                        <div class="form-group">
                                            <label for="cc-number" class="control-label">Category </label>
                                            <input  class="form-control ">

                                        </div>
                                        <div class="form-group">
                                            <label for="cc-number" class="control-label">Name this Search</label>
                                            <input  class="form-control ">
                                        </div>
                                        <div class="form-group">
                                            <label for="cc-number" class="control-label">Message</label>
                                            <textarea name="content" class="form-control" id="content" cols="30" rows="5" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                </div>

                                <div class="Message__composer__action">
                                    <a class="Message__composer__action--send btn btn-themed btn-primary btn-main-action">Save</a>
                                    <a class="Message__composer__action--cancel btn btn-themed btn-main-action btn-back" data-dismiss="modal">Close</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="modal" id="myModal">
    <div class="modal-header">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">

                    <form action="">
                        <div class="Message__composer modal-transition">
                            <div class="Message__composer__content">
                                <h3 class="Message__composer--heading">Advanced search</h3>
                                <div class="Message__composer--body">

                                    <div class="Message__composer__form">

                                        <div class="form-group">
                                            <label for="cc-number" class="control-label">All of these words </label>
                                            <input  class="form-control ">

                                        </div>
                                        <div class="form-group">
                                            <label for="cc-number" class="control-label">Any of these words </label>
                                            <input  class="form-control ">

                                        </div>
                                        <div class="form-group">
                                            <label for="cc-number" class="control-label">Skill Search </label>
                                            <input  class="form-control ">

                                        </div>
                                        <div class="form-group">
                                            <label for="cc-number" class="control-label">Title Search</label>
                                            <input  class="form-control ">
                                        </div>

                                    </div>
                                </div>

                                <div class="Message__composer__action">
                                    <a class="Message__composer__action--send btn btn-themed btn-primary btn-main-action">Search</a>
                                    <a class="Message__composer__action--cancel btn btn-themed btn-main-action btn-back" data-dismiss="modal">Close</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript">






    $( document ).ready(function() {

        $(".close_info_success").on("click", function () {

            $(".close_info_success").parent().remove();


    }); });
</script>
@endsection
