 @extends('app')

@section('content')

    <div id="content-wrap" class="content-wrap">
        <div class="container" id="employee-find-jobs" v-cloak>
            <div class="content">
                <div id="jobs-list" class="caregiver-profile">
                    <div class="col-md-9 search_blok">
                        <div class="">
                            <div class="details">
                                <h4> Saved jobs </h4>
                            </div>
                        </div>
                        <div class="margin">
                            Keep track of jobs you're interested in.Once a job is closed, it will automatically be removed from list.
                        </div>
                    </div>
                    @if(! count($jobs))
                        <div class="col-md-9 rem_display">
                            <hr class="no_margin">
                            <div class="col-md-12 rem_dir">
                                <div class="panel job-postings">
                                    <div class="panel-row empty">
                                        <h5> You have no saved jobs</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        @foreach($jobs as $job)
                        <div class="col-md-9 work_list">
                            <div class="work-history no_margin">
                                <hr class="no_margin">
                                <div class="col-md-9 "></div>
                                <div class="feedback-row">
                                    <div class="job">
                                        <div class="job-name">
                                            <a href="/jobs/{{ $job->id }}/details">{{ $job->title }}</a>
                                            <div class="date-posted">Posted {{ $job->created_at->diffForHumans() }}</div>
                                        </div>
                                        
                                        <div class="job-rating">
                                            <div class="icon-star"></div>
                                            <div class="icon-star"></div>
                                            <div class="icon-star"></div>
                                            <div class="icon-star"></div>
                                            <div class="icon-star"></div>
                                            5.0
                                        </div>
                                    </div>
                                    <div class="feedback">
                                        <div class="description">
                                            {{ $job->description }}
                                        </div>
                                        <div class="rates">
                                            <div class="rate">
                                                About the client
                                            </div>
                                            <div class="timespan">
                                                {{ $job->user->first_name . ' ' . $job->user->last_name }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="skills">Category:
                                        @foreach($job->categories as $cat)
                                        <span class="skill">{{ $cat }}</span>
                                        @endforeach
                                         <a class="btn btn-themed btn-preview" id="remove_job{{$job->id}}"  data-url="{{ route('api.v1.employee.job.remove_job', [$job->id]) }}">  Remove job</a>
                                    </div>
                                </div>
                                {{--<div class="col-md-12">--}}
                                    {{--<hr>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        @endforeach
                    @endif
                        {{--@if(count($jobs))--}}
                        {{--<div class="col-md-9 rem_display">--}}
                            {{--<hr class="no_margin">--}}
                            {{--<div class="col-md-12 rem_dir">--}}
                                {{--<div class="panel-row empty">--}}
                                    {{--<a href="#">Load More Jobs</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@endif--}}
                        {{--<div class="col-md-3 right_drop">--}}
                            {{--<div class="dropdown border_b">--}}
                                {{--<p> Category </p>--}}
                                {{--<ul class="">--}}
                                    {{--<li><a href="#">lorem ipsum</a></li>--}}
                                    {{--<li><a href="#">lorem ipsum</a></li>--}}
                                    {{--<li><a href="#">lorem ipsum</a></li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <!-- Caregiver Options -->
                </div>
                <hr class='no_margin'>
            </div>
        </div>
    </div>

<script type="text/javascript">
   $( document ).ready(function() {
        $(".btn-preview").on('click', function() {
            var removeButtonId = $(this).attr('id');
            var removeButton = $('#' + removeButtonId);
            jQuery.ajax({
                type: 'get',
                url: removeButton.data('url'),
                success: function(resp) {
                    removeButton.parent().parent().parent().parent().remove();
                    var savedJobsCount = $('#jobs-list').find('.work_list').size();

                    if(! savedJobsCount)
                    {
                        var html = "<div class='col-md-9 rem_display'>";
                        html += "<hr class='no_margin'>";
                        html += "<div class='col-md-12 rem_dir'>";
                        html += "<div class='panel job-postings'>";
                        html += "<div class='panel-row empty'>";
                        html += "<h5> You have no saved jobs</h5>";
                        html += "</div>";
                        html += "</div>";
                        html += "</div>";
                        html += "</div>";
                        $('#jobs-list').append(html);
                    }
                }
            });
        });
   });
</script>
@endsection
