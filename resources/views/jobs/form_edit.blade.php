<div v-cloak>
    <script>
        var editMode = true;
    </script>


    <div class="row form-row">
        <div class="col-md-3">
            <strong>Choose a category</strong>
        </div>
        <div class="col-md-4" v-show="!previewMode">
            <div class="FindCaregivers__filter FindCaregivers__filter--qualifications" :class="['full-width', category.errors.message ? 'error' : '' ]" v-else>
                <custom-select-ext class="choose_category"
                                   :as-list="true"
                                   :options="category.items"
                                   @click="category.errors.message = ''"
                                   :selected.sync="category.selected"
                                   :multiple="category.multiple"
                                   :required="false"
                                   name="category">
                </custom-select-ext>
                <div class="error-message" v-if="category.errors.message"> @{{ category.errors.message }} </div>
            </div>
        </div>
        <div class="col-md-6" v-show="previewMode">
            @{{ category.text}}
        </div>
    </div>

    <div class="row form-row">
        <div class="col-md-3">
            <strong>Child Age Range</strong>
        </div>
        <div class="col-md-9" v-show="!previewMode">
            <div class="FindCaregivers__filter FindCaregivers__filter--care-type">
                <custom-select-ext class="child-age" name="child-age" id="child-age" v-if="selects"
                                   :options="selects.child_age.items" :multiple="selects.child_age.multiple" :status-icon="true" :selected.sync="child_age_range" :msg-all="All">
                </custom-select-ext>
                <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
            </div>
        </div>
        <div class="col-md-6" v-show="previewMode">
            @{{ prevtext.child_age_range }}
        </div>
    </div>

    <div class="row form-row">
        <div class="col-md-3">
            <strong>Give your job a title</strong>
        </div>
        <div class="col-md-4">
            <div :class="['full-width', title.errors.message ? 'error' : '' ]">
                <div v-if="previewMode">
                    @{{ title.text }}
                    <div v-if="!title.text">
                        Please enter a Title for this job
                    </div>
                </div>

                <input type="text" @keyup="title.errors.message = ''" v-model="title.text" name="title" v-else/>

                <div class="error-message" v-if="title.errors.message"> @{{ title.errors.message }} </div>
            </div>
        </div>
    </div>
    <div class="row form-row">
        <div class="col-md-3">
            <strong>Describe the services you need</strong>
        </div>
        <div class="col-md-6">
            <div :class="['full-width', description.errors.message ? 'error' : '' ]">
                <textarea name="description" rows="5" v-model="description.text" v-show="!previewMode" @keyup="description.errors.message = ''"></textarea>
                <div v-else>
                    @{{ description.text }}
                    <div v-if="!description.text">
                        Please enter a description for this job
                    </div>
                </div>
                <div class="error-message" v-if="description.errors.message"> @{{ description.errors.message }} </div>
            </div>
        </div>
    </div>
    <div class="row form-row">
        <div class="col-md-3">
            <strong>Qualifications</strong>
        </div>
        <div class="col-md-9" v-show="!previewMode">
            <div class="full-width">

                <div class="FindCaregivers__filter FindCaregivers__filter--qualifications" v-else>
                    <qualification class="qualification" name="qualification" id="qualification" :as-list="true" v-if="qualifications.items" :multiple-list="true"
                                   :options="qualifications.items" :multiple="qualifications.multiple" :selected.sync="qualifications.selected" :required="false">
                    </qualification>
                </div>
            </div>
        </div>
        <div class="col-md-6" v-show="previewMode">
            @{{ prevtext.qualifications_items_text }}
        </div>
    </div>
    <div class="row form-row">
        <div class="col-md-3">
            <strong>Estimated Duration</strong>
        </div>
        <div class="col-md-9" v-show="!previewMode">
            <div :class="['full-width', job_duration.errors.message ? 'error' : '' ]">

                <div class="FindCaregivers__filter FindCaregivers__filter--w200" v-else>
                    <custom-select-ext class="job_duration" name="job_duration_id" id="job_duration_id" :as-list="true" v-if="true"
                                       :options="{{ json_encode($jobDurations) }}" :selected.sync="job_duration.selected" :required="false">
                    </custom-select-ext>
                </div>
                <div class="error-message" v-if="job_duration.errors.message"> @{{ job_duration.errors.message }} </div>
            </div>
        </div>
        <div class="col-md-6" v-show="previewMode">
            @{{ prevtext.job_duration }}
        </div>
    </div>

    <div class="row form-row">
        <div class="col-md-3">
            <strong>Estimated Workload</strong>
        </div>
        <div class="col-md-9" v-show="!previewMode">
            <div :class="['full-width', job_workload.errors.message ? 'error' : '' ]">

                <div class="FindCaregivers__filter FindCaregivers__filter--w200" v-else>
                    <custom-select-ext class="estimated_workload" name="job_workload_id" id="job_workload_id" :as-list="true" v-if="true"
                                       :options="{{ json_encode($jobWorkloads) }}" :selected.sync="job_workload.selected" :required="false">

                    </custom-select-ext>
                </div>
                <div class="error-message" v-if="job_workload.errors.message"> @{{ job_workload.errors.message }} </div>
            </div>
        </div>
        <div class="col-md-6" v-show="previewMode">
            @{{ prevtext.estimated_workload }}
        </div>
    </div>
    @if (isset($ocupation))
        @include('jobs.partials.ocupation', ['ocupation' => $ocupation])
    @else
        @include('jobs.partials.ocupation')
    @endif


    <div class="row form-row">
        <div class="col-md-3">
            <strong>Marketplace Visibility</strong>
        </div>
        <div class="col-md-6">
            <div :class="['full-width', marketplace_visibility.errors.message ? 'error' : '' ]">
                <div class="FindCaregivers__filter FindCaregivers__filter--w300" v-show="!previewMode">
                    <custom-select-ext class="marketplace_visibility_id" name="marketplace_visibility_id" id="marketplace_visibility_id" :as-list="true" v-if="true"
                                       :options="{{ json_encode($marketplaceVisibilities) }}" :selected.sync="marketplace_visibility.selected" :required="false">
                    </custom-select-ext>
                </div>
                <div class="error-message" v-if="marketplace_visibility.errors.message"> @{{ marketplace_visibility.errors.message }} </div>
            </div>
        </div>
        <div class="col-md-6" v-show="previewMode">
            @{{ prevtext.marketplace_visibility }}
        </div>
    </div>
    <div v-if="!previewMode">
        <h5>Customize Your Application Requirements</h5>
        <hr>
    </div>
    <div class="section-2">
        <div class="row form-row" v-show="!previewMode">
            <div class="col-md-3">
                <strong>Cover Letter</strong>
            </div>
            <div class="col-md-6">
                <div class="full-width">
                    <p>Ask applicants to write a cover letter introducing themselves.</p>
                    <div class="checkbox">
                        <input type="checkbox" v-model="cover_letter" name="cover-letter" value="1" checked id="cover_letter"> <label for="cover_letter" class="fx2">Yes, require a cover letter </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Minimum Feedback Score</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', minimum_feedback.errors.message ? 'error' : '' ]">
                    <div v-if="previewMode">
                        @{{ minimum_feedback.text }}
                    </div>
                    <div class="FindCaregivers__filter FindCaregivers__filter--w300" v-else>
                        <custom-select-ext class="minimum_feedback_id" name="minimum_feedback_id" id="minimum_feedback_id" :as-list="true" v-if="true"
                                           :options="{{ json_encode($minimumFeedbacks) }}" :selected.sync="minimum_feedback.selected" :required="false">
                        </custom-select-ext>
                    </div>
                    <div class="error-message" v-if="minimum_feedback.errors.message"> @{{ minimum_feedback.errors.message }} </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.minimum_feedback }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Location</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', allowed_locations.errors.message ? 'error' : '' ]">
                    <div v-if="previewMode">
                        @{{ allowed_locations.text }}
                    </div>
                    <div class="FindCaregivers__filter FindCaregivers__filter--w300" v-else>
                        <custom-select-ext class="allowed_locations_id" name="allowed_locations_id" id="allowed_locations_id" :as-list="true" v-if="true"
                                           :options="{{ json_encode($allowedLocations) }}" :selected.sync="allowed_locations.selected" :required="false">
                        </custom-select-ext>
                    </div>
                    <div class="error-message" v-if="allowed_locations.errors.message"> @{{ allowed_locations.errors.message }} </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.allowed_locations }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Experience</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', experience.errors.message ? 'error' : '' ]">
                    <div v-if="previewMode">
                        @{{ experience.text }}
                    </div>
                    <div class="FindCaregivers__filter FindCaregivers__filter--w300" v-else>
                        <custom-select-ext class="allowed_locations_id" name="allowed_locations_id" id="allowed_locations_id" :as-list="true" v-if="true"
                                           :options="{{ json_encode($minimumExperiences) }}" :selected.sync="experience.selected" :required="false">
                        </custom-select-ext>
                    </div>
                    <div class="error-message" v-if="experience.errors.message"> @{{ experience.errors.message }} </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.experience }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Willing to care for sick children</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', experience.errors.message ? 'error' : '' ]">
                    <div class="FindCaregivers__filter FindCaregivers__filter--range">
                        <custom-select-ext class="sick_children" style="width: 100%" name="care-sick" id="care-sick" v-if="selects" :as-list="true" :list-preference="true"
                                           :options="selects.care_sick.items" :multiple="selects.care_sick.multiple" :selected.sync="sick_children">
                        </custom-select-ext>
                        {{--<div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-6" v-if="previewMode">
                @{{ prevtext.care_sick }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Comfortable with pets</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', experience.errors.message ? 'error' : '' ]">

                    <div class="FindCaregivers__filter FindCaregivers__filter--range" >
                        <custom-select-ext class="pets comfortable_with_pets" name="pets" id="pets" v-if="selects" :as-list="true" :list-preference="true"
                                           :options="selects.pets.items" :multiple="selects.pets.multiple" :selected.sync="comfortable_with_pets">
                        </custom-select-ext>
                        {{--<div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.pets }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Preferred age range</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', age_range.errors.message ? 'error' : '' ]">
                    <div class="FindCaregivers__filter FindCaregivers__filter--range">
                        <custom-select-ext class="age-range age" name="age-range" id="age-range" :as-list="true" v-if="selects" :list-preference="true"
                                           :options="selects.age_range.items" :multiple="selects.age_range.multiple" :selected.sync="age" :required="true">
                        </custom-select-ext>
                    </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.age_range }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Preferred gender</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', gender.errors.message ? 'error' : '' ]">
                    <div class="FindCaregivers__filter FindCaregivers__filter--range">
                        <custom-select-ext class="gender" name="gender" id="gender" :as-list="true" v-if="selects" :list-preference="true"
                                           :options="selects.gender.items" :multiple="false" :status-icon="false" :selected.sync="gender" :required="true">
                        </custom-select-ext>
                    </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.gender }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Have own children</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', experience.own_children.message ? 'error' : '' ]">
                    <div class="FindCaregivers__filter FindCaregivers__filter--range">
                        <custom-select-ext class="own-children own_children" name="own-children" id="own-children" :as-list="true" v-if="selects" :list-preference="true"
                                           :options="selects.own_children.items" :multiple="selects.own_children.multiple" :selected.sync="own_children" :required="false">
                        </custom-select-ext>
                    </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.own_children }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Drivers licence</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', experience.errors.message ? 'error' : '' ]">
                    <div class="FindCaregivers__filter FindCaregivers__filter--range">
                        <custom-select-ext class="drivers-licence have_transportation" name="drivers-licence" id="drivers-licence" v-if="selects" :as-list="true" :list-preference="true"
                                           :options="selects.drivers_licence.items" :multiple="selects.drivers_licence.multiple" :selected.sync="have_transportation">
                        </custom-select-ext>
                        <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.drivers_licence }}
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Smoker</strong>
            </div>
            <div class="col-md-9" v-show="!previewMode">
                <div :class="['full-width', experience.errors.message ? 'error' : '' ]">
                    <div class="FindCaregivers__filter FindCaregivers__filter--range">
                        <custom-select-ext class="smoker" name="smoker" id="smoker" v-if="selects" :as-list="true" :list-preference="true"
                                           :options="selects.smoker.items" :multiple="selects.smoker.multiple" :selected.sync="smoker">
                        </custom-select-ext>
                        <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" v-show="previewMode">
                @{{ prevtext.smoker }}
            </div>
        </div>
        <hr>
        <div class="row form-row" >
            <div class="col-md-3">
                <strong>Screening Questions</strong>
            </div>
            <div class="col-md-6">
                <div class="full-width" v-if="previewMode">
                    <div v-for="question in questions.list" v-if="question.text != ''" class="row form-row">
                        @{{ question.text }}
                    </div>
                </div>
                <div class="full-width" v-if="!previewMode">
                    <p>Add a few questions you'd like your candidates to answer when applying to you job.</p>
                    <questions
                            :form-data.sync="questions">
                    </questions>
                </div>

            </div>
        </div>
        <hr>
    </div>
    <div class="row">
        <div class="col-md-6 col-md-push-3">
            <div class="full-width">
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click.prevent="mainAction">@{{ mainActionText  }}</a>
                    <a type="button" class="btn btn-themed btn-preview" @click.prevent="handlePreviewMode()">@{{ secondaryActionText }}</a>
                </div>
            </div>
        </div>
    </div>
</div>