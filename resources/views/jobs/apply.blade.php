@extends('app')

@section('content')
<h3>Apply to Job</h3>
<br>
@if (!empty($job_info))
<div class="row">
    
    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="path" value="{{ Route::input('one') }}">

        <div class="form-group" data-name="price">
			<label class="col-md-4 control-label">Propose terms</label>
            <div class="col-md-4">
                <div class="well row" style="margin-left: 0px; margin-right: 0px;">
                    @if($job_info->job_fixed_price != '0.00')
                        <div class="col-md-4 valign">
                            Your bid
                        </div>
                        <div class="col-md-7 valign">
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" name="price" value="{{ $job_info->job_fixed_price }}">
                            </div>
                        </div>
                    @else
                        <div class="col-md-4 valign">
                            Your rate
                        </div>
                        <div class="col-md-7 valign">
                            <div class="input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" name="price" value="{{ Auth::user()->user_rate }}">
                                <span class="input-group-addon">/hr</span>
                            </div>
                        </div>
                    @endif

                </div>
			</div>
        </div>
        
        <div class="form-group" data-name="cover_letter">
			<label class="col-md-4 control-label">Cover Letter</label>
			<div class="col-md-6">
                <textarea class="form-control" name="cover_letter" rows="10"></textarea>
			</div>
        </div>

        <div class="form-group" data-name="file">
			<label class="col-md-4 control-label">Attach a file</label>
            <div class="col-md-6 form-control-static upload-file-block">
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-default btn-file btn-sm"> Browse&hellip; <input type="file" name="file" class="input-sm"></span>
                    </span>
                    <input type="text" class="form-control input-sm" readonly>
                    <span class="input-group-btn">
                        <a href="" class="btn btn-default btn-sm upload-file-clear">Clear</a>
                    </span>
                    <span class="input-group-btn" style="display:none;">
                        <a href="" class="btn btn-default btn-sm upload-file-remove">Remove</a>
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group">
			<div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary send-req-form">Apply Job</button>
                <a href="/home" class="btn btn-default">Cancel</a>
			</div>
        </div>

    </form>
</div>
@endif
@endsection
