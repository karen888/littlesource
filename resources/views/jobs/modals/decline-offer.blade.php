<div class="modal fade" id="decline-offer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Decline Offer</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-3 no_padding">
                        <strong>Reason</strong>
                    </div>
                    <div class="col-md-9">
                        <div :class="['full-width', reason.errors.message ? 'error' : '' ]">
                            <custom-select
                                :options="{{ json_encode($declineReason) }}"
                                @click="reason.errors.message = ''"
                                :selected.sync="reason.selected"
                                name="reason">
                            </custom-select>
                            <div class="error-message-text" v-if="reason.errors.message"> @{{ reason.errors.message }} </div>
                        </div>

                    </div>
                </div>
                <br>
                <div class="row form-row">
                    <div class="col-md-3 no_padding">
                        <strong>Message to Client</strong>
                        <small>(optional)</small>
                    </div>
                    <div class="col-md-9">
                        <div class="full-width">
                            <textarea name="accept_message" rows="5" ></textarea>
                        </div>
                    </div>
                </div>
                <br>
                <div class="actions col-md-10 col-md-offset-2">
                    <a class="btn btn-themed btn-primary btn-main-action" @click.prevent="declineOffer">DECLINE OFFER</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
