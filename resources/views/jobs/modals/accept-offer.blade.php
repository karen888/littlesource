
<div class="modal fade" id="accept-offer" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button

                        type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Accept Offer</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-3 no_padding">
                        <strong>Message to Client</strong>
                    </div>
                    <div class="col-md-9">
                        <div :class="['full-width', message_accept.errors.message ? 'error' : '' ]">
                            <textarea name="message_accept" rows="5" v-model="message_accept.text" @keyup="message_accept.errors.message = ''"></textarea>
                            <div class="error-message" v-if="message_accept.errors.message"> @{{ message_accept.errors.message }} </div>
                        </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-3 no_padding">
                        <strong>Agree to terms</strong>
                    </div>
                    <div class="col-md-9">
                        <div class="full-width">
                            <div class="check">
                                <input type="checkbox" id="option3" name="locked-status" value="accept" checked>
                                <label for="option3">
                                    Yes, I understand and agree the LittleOnes <a href="#">Terms of Service</a>, including the <a href="#">User Agreement</a> and <a href="#">Privacy Policy</a>.
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="actions col-md-10 col-md-offset-2">
                    <a class="btn btn-themed btn-primary btn-main-action" @click.prevent="acceptOffer">ACCEPT OFFER</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
