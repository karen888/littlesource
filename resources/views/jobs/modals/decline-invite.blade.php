<div class="modal fade" id="decline-invite" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Decline</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    You will politely notify the client you are not interested in this job.
                </div>
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <strong>Reason</strong>
                    </div>
                    <div :class="['full-width', decline_reason.errors.message ? 'error' : '' ]">
                            <custom-select
                                :options="{{ json_encode($declineReason) }}"
                                @click="decline_reason.errors.message = ''"
                                :selected.sync="decline_reason.selected"
                                name="reason">
                            </custom-select>
                        <div class="error-message-text" v-if="decline_reason.errors.message"> @{{ decline_reason.errors.message }} </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <strong>Include a message (optional)</strong>
                    </div>
                    <div class="col-md-12">
                        <div class="full-width">
                            <textarea name="decline_text" rows="5" v-model="decline_text.text"></textarea>
                        </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-12 full-width">
                        <div class="checkbox">
                            <label><input type="checkbox" name="block_client_invitation" v-model="block_client_invitation" value="1"> Block this client from sending me any future invitations. </label>
                        </div>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click.prevent="declineJobInvite">WITHDRAW PROPOSAL</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
