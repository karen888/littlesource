<div class="modal fade" id="submit-proposal" tabindex="-1" role="dialog" aria-hidden="true">
    <input type="hidden" name="job_id" v-model="job_id" value="{{ $job_id }}" />
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Submit A Proposal</h4>
            </div>
            <div class="modal-body">
                @if(false === Auth::user()->canApplyToJob())

                <?php
                $user = Auth::user();
                ?>

                    <div class="alert alert-danger">
                        To apply to a job you must submit list what we require:
                        <ul>
                            @if($user->info->phone == '')
                                <li><a style=" text-decoration: underline;" href="/user/settings/get-verified/mobile-number">Phone</a></li>
                            @endif
                            @if($user->info->wwcc_image == '')
                                <li><a style=" text-decoration: underline;" href="/user/settings/get-verified/wwcc">WWCC</a></li>
                            @endif
                            @if($user->info->prof_scan_id == '')
                                    <li><a style=" text-decoration: underline;" href="/user/settings/get-verified/id">ID</a></li>
                            @endif
                            @if($user->info->loc_city == '')
                                    <li><a style=" text-decoration: underline;" href="/user/settings/get-verified/address">Address</a></li>
                            @endif
                            @if($user->employee_info->overview == '')
                                    <li><a style=" text-decoration: underline;" href="/employee/profile?edit=overview">Overview</a></li>
                            @endif
                        </ul>
                    </div>

                @else
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <strong>Message to Client</strong>
                    </div>
                    <div class="col-md-12">
                        <div :class="['full-width', accept_message.errors.message ? 'error' : '' ]">
                            <textarea name="accept_message" rows="5" v-model="accept_message.text" @keyup="accept_message.errors.message = ''"></textarea>
                            <div class="error-message" v-if="accept_message.errors.message"> @{{ accept_message.errors.message }} </div>
                        </div>
                    </div>
                </div>
                <div class="row form-row" v-show="questions.items.length">
                    <div class="col-md-12 no_padding">
                        <h6><strong>Job Questions:</strong></h6>
                    </div>
                    <div class="row form-row" v-for="item in questions.items">
                        <div class="col-md-12 no_padding">
                            <strong>@{{ item.question }}</strong>
                        </div>
                        <div class="col-md-12">
                            <div :class="['full-width', questions.errors ? 'error' : '' ]">
                                <textarea name="accept_message" rows="2" v-model="item.answer" @keyup="questions.errors = ''"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 error">
                        <div class="error-message" v-if="questions.errors"> @{{ questions.errors }} </div>
                    </div>
                </div>


                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <strong>Propose Terms</strong>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-4 no_padding">
                        <div class="col-md-12 no_padding">
                            <strong>Billing Rate:</strong>
                        </div>
                        <div class="col-md-12 no_padding">
                            <small>This is what the client sees</small>
                        </div>
                    </div>
                    <div class="col-md-4 no_padding">
                        <div :class="[accept_rate.errors.message ? 'error' : '' ]">
                            <label>$ <input type="text" name="accept_rate" size="5" v-model="accept_rate.float" @keyup="setRate('accept')" /> /hr</label>
                            <div class="error-message" v-if="accept_rate.errors.message"> @{{ accept_rate.errors.message }} </div>
                        </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-4 no_padding">
                        <div class="col-md-12 no_padding">
                            <strong>You will earn:</strong>
                        </div>
                        <div class="col-md-12 no_padding">
                            <small>Estimated</small>
                        </div>
                    </div>
                    <div class="col-md-4 no_padding">
                        <div>
                            <label>$ <input type="text" size="5" name="current_rate" v-model="current_rate" @keyup="setRate('current')" /> /hr</label>
                        </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-4 col-md-offset-4">
                        <small>Your current profile rate is $@{{ user_rate }}/hr</small>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click.prevent="acceptJobInvite">ACCEPT</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal">CANCEL</a>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
