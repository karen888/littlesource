@extends('app')

@section('content')
    <div class="container padd-top-40" id="view_offer">
        <h4>
            View Offer
        </h4>
        @if($offer->status !== 'sended')
            <div class="date-posted">{{ ucfirst($offer->status) }} by {{ $offer->employee->name }} on {{ $offer->updated_at->format('j M Y , g:ia') }} <a href="{{ route('jobs.contracts.details', ['contract' => $offer->id]) }}">View Contract</a></div>
        @else
            <div class="date-posted">Pending -- Expires on {{ date('F j, Y', strtotime("{$offer->start_date} +2 week")) }}</div>
        @endif
        <div class="caregiver-details">
            <div class="details-and-proposals">
                <div class="panel">
                    <div class="panel-row">
                        <div class="details">
                            <h5>Offer details</h5>
                        </div>
                    </div>
                    <div class="panel-row">
                        <div class="qualifications-and-activity">
                            <div class="row">
                                <div class="col-md-3"><strong>Contract Title</strong></div>
                                <div class="col-md-9">{{ $offer->title }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>Job Category</strong></div>
                                <div class="col-md-9">{{ $offer_category }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>Hourly Rate</strong></div>
                                <div class="col-md-9">${{ $offer->offer_paid }}/hr</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>Offer Date</strong></div>
                                <div class="col-md-9">{{ $offer->created_at->format('j M Y , g:ia') }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>Start Date</strong></div>
                                <div class="col-md-9">{{ date('j M Y', strtotime($offer->start_date)) }}</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3"><strong>Client Message</strong></div>
                                <div class="col-md-9">{{ $offer->offer_text }}</div>
                            </div>
                        </div>
                        @if ($accept_offer)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="actions">
                                        <a class="btn btn-themed btn-primary btn-main-action"
                                            @if($offer->employee->isUserFullyVerified()) data-toggle="modal" data-target="#accept-offer"
                                            @else data-toggle="popover-hover" data-title="You are not verified yet"
                                           data-content="Once you will be verified you'll be able to accept offers"
                                           data-placement="top"
                                            @endif
                                        >ACCEPT OFFER</a>
                                        <a type="button" class="btn btn-themed btn-preview" data-toggle="modal" data-target="#decline-offer">DECLINE OFFER</a>
                                    </div>
                                </div>
                            </div>
                            @if($offer->employee->isUserFullyVerified())
                                @include('jobs.modals.accept-offer')
                            @endif
                            @include('jobs.modals.decline-offer')
                            <input type="hidden" v-model="offer" value="{{ $offer->id }}">
                        @endif
                    </div>
                </div>
            </div>
            <div class="about-client">
                <div class="panel">
                    <div class="panel-row">
                        <div class="details">
                            <h5>
                                Parties
                            </h5>
                        </div>
                    </div>
                    <div class="panel-row">
                        <div class="caregiver-info">
                            <div class="">
                                <strong>Offer made by:</strong>
                            </div>
                            <div class="caregiver">
                                <img class="img_50" src="{{ $offer->client->photo_url }}" alt="">
                                <div class="name"><strong>{{ $offer->client->name }}</strong></div>
                            </div>
                            <div class="hire-rate">
                                <strong>Offer made to:</strong>
                            </div>
                            <div class="caregiver">
                                <img class="img_50" src="{{ $offer->employee->photo_url }}" alt="">
                                <div class="name"><strong>{{ $offer->employee->name }}</strong></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-row">
                        <div class="caregiver-info">
                            <p><a href="{{ route('caregiver.jobs.details', ['job' => $offer->job_id]) }}">View original proposal</ahref>
                            {{--<p><a>View messages associated with this offer</a>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

