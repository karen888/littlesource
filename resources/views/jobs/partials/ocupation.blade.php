<template id="job-ocupation-template" >
@if (isset($ocupation))
    <input type="hidden" v-model="ocupation" value="{{ $ocupation }}">
@endif
<div class="row form-row">
    <div class="col-md-3" @changed-option="handleDaysDisplay()">
        <strong @click="handleDaysDisplay()">Specific hours</strong>
    </div>
    <div class="col-md-4">
        <div :class="['full-width', ocupationFormData.errors.message ? 'error' : '' ]">
            <div v-show="previewMode">
                <div class="day-text-preview" v-show="ocupationFormData.text.length">Specific days and time</div>
                <div class="day-text-preview" v-for="text in ocupationFormData.text">
                    @{{ text }}
                </div>
                <div v-if="!ocupationFormData.text.length">
                    Please select specific days and time.
                </div>
            </div>
            <custom-select class="occupation_select"
                :options="[{ text: 'Specific days and time', value: '1' }, { text: 'No specific hours', value: '0' }]"
                :selected.sync="ocupationFormData.selected"
                name="specific hours"
                @click="ocupationFormData.errors.message = ''"
                v-else>
            </custom-select>
            <div class="error-message" v-if="ocupationFormData.errors.message"> @{{ ocupationFormData.errors.message }} </div>
        </div>
    </div>
</div>
<div class="row form-row occupation_day" v-show="displayDays && !previewMode">
    <div class="col-md-9 col-md-push-3">
        <div class="full-width">
            <days
                :ocupation-form-data.sync="ocupationFormData"
                @if(isset($edit) && $edit)
                :edit-mode="editMode"
                @endif
            >
            </days>
        </div>
    </div>
</div>
</template>

<ocupation
    :ocupation-form-data.sync="ocupationFormData"
    :preview-mode="previewMode"
    @if(isset($edit) && $edit)
    :edit-mode="1"
    @endif
    >
</ocupation>