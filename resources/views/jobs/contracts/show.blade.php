@extends('app')

@section('content')
<div class="container padd-top-30" id="view-contract">
    <div class="row back-action">
        <div class="col-md-12">
            <a href="{{ URL::previous() }}" class="back-button"><span class="back-icon"></span>Back to Previous Page</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h4>{{ $by_employee ? $contract->client->name : $contract->employee->name }} — {{ $contract->job->title }}</h4>
        </div>
    </div>
    <div class="contract-details padd-top-30" v-cloak>
        <div class="details">
            <div class="panel">
                <div class="panel-row">
                    <h5>Contract Details</h5>
                </div>
                <div class="panel-row">
                    <div class="contract-status"><strong>@if($contract->is_active == 0){{ $contract->job->closed }}@else Active @endif</strong></div>
                    <div class="contract-date">Since {{ date('M j, Y', strtotime($contract->start_date)) }} (Contract ID {{ $contract->id }}). {{ $contract->job->getCloseDate() ? "Closed {$contract->job->getCloseDate()}" : "" }}</div>
                    <a href="{{ route('caregiver.jobs.details', ['job' => $contract->job_id]) }}" target="_blank">View original job post</a>
                    <div class="contract-rate">
                        <strong>Rate:</strong> ${{ $contract->offer_paid }}  <!--a href="#">Decrease rate</a-->
                    </div>
                    @foreach ($feedback as $feed)
                        <div class="row">
                            <div class="col-md-4">
                                <b>Feedback to {{ $feed['by_employee'] ? 'Client' : 'Employee' }}</b>
                            </div>
                            <div class="col-md-8">
                                <star-rating :view_type="true" :value="{{ $feed['rating'] }}"></star-rating>
                                <p style="white-space: pre-line; ">{{ $feed['comment'] }}</p>
                            </div>
                        </div>
                    @endforeach
                    @if(!$edit)
                        <div class="detail-actions">
                            <a href="{{ route('jobs.contracts.end-contract', ['contract' => $contract->id]) }}" class="btn btn-themed btn-contract-actions">
                            @if($contract->is_active == 1)
                            End Contract
                            @else
                            Leave Feedback
                            @endif
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="caregiver">
            <div class="panel">
                <div class="panel-row">
                    @if(!$by_employee)
                        <h5>Caregiver</h5>
                    @else
                        <h5>Client</h5>
                    @endif
                </div>
                <div class="panel-row">
                    @if(!$by_employee)
                        <div class="caregiver-name">
                            <div class="image"><img class="img_50" src="{{ $contract->employee->photoUrl }}" alt=""></div>
                            <div class="name"><a href="{{ route('employee.profile', ['employee' => $contract->employee->id]) }}"><strong>{{ $contract->employee->name }}</strong></a></div>
                        </div>
                    @else
                        <div class="caregiver-name">
                            <div class="image"><img class="img_50" src="{{ $contract->client->photoUrl }}" alt=""></div>
                            <div class="name"><strong>{{ $contract->client->name }}</strong></div>
                        </div>
                    @endif
                    <div class="caregiver-actions">
                        <a href="{{ route('messages.index') }}" class="btn btn-themed btn-contract-actions">Send a Message</a>
                        @if($edit)
                            <a href="{{ route('jobs.contracts.end-contract', ['contract' => $contract->id]) }}" class="btn btn-themed btn-contract-actions">Change Feedback</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
