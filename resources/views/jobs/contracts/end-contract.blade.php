@extends('app')

@section('content')

    <div class="container padd-top-30" id="end-contract">
        <input type="hidden" v-model="feedback_type" value="{{ $feedback_type }}">
        <input type="hidden" v-model="contract" value="{{ $contract->id }}">


        <div class="row">
            <div class="col-md-12" v-cloak>
                <div class="main-heading">
                    <h4>@{{ contractTitle }}</h4>
                    <h5> {{ $contract->job->title }} </h5>
                </div>
            </div>
        </div>
        <div class="panel job-postings" v-cloak>
            <div class="panel-row">
                <div class="row">
                    <div class="col-md-12">
                        <div class="main-heading">
                            <h5>Leave Feedback for Client for Other Caregivers to View</h5>
                            <span>This feedback is public and will be shown in client's profile. Please leave honest and accurate feedback</span>
                        </div>
                    </div>
                </div>
                <div class="job-form">
                    <div class="row form-row prev_feedback" v-if="!set_type">
                        <div class="col-md-4">
                            <b>Your Feedback to {{ ucfirst($feedback_type) }}</b>
                        </div>
                        <div class="col-md-8">
                            <star-rating :view_type="true" :value="prev_rating"></star-rating>
                            <p style="white-space: pre-line;">@{{ prev_comment }}</p>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-3">
                            <strong>{{ ucfirst($feedback_type) }}</strong>
                        </div>
                        @if ($feedback_type == 'employee')
                            <div class="col-md-4 full-width caregiver full-width">
                                <img class="img_50" src="{{ $contract->employee->PhotoUrl }}" alt="" >
                                <strong>{{ $contract->employee->name }}</strong>
                            </div>
                        @else
                            <div class="col-md-4 full-width caregiver full-width">
                                <img class="img_50" src="{{ $contract->client->PhotoUrl }}" alt="" >
                                <strong>{{ $contract->client->name }}</strong>
                            </div>
                        @endif

                    </div>

                    <div class="row form-row" v-if="reason.items.length">
                        <div class="col-md-3">
                            <strong>Reason for Ending Contract</strong>
                        </div>
                        <div class="col-md-4">
                            <div :class="['full-width', errors.reason ? 'error' : '' ]" v-cloak>
                                <custom-select class="choose_job"
                                    :options="reason.items"
                                    @click="errors.reason = ''"
                                    :selected.sync="reason.selected"
                                    name="Reason"
                                >
                                </custom-select>
                                <div class="error-message" v-if="errors.reason"> @{{ errors.reason }} </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row" v-if="ifReasonOther">
                        <div class="col-md-3">
                            <strong>Other Reason</strong>
                        </div>
                        <div class="col-md-4">
                            <div :class="['full-width', errors.reason_text ? 'error' : '' ]" v-cloak>
                                <input type="text" name="message" v-model="reason_text" @keyup="errors.reason_text = ''" />
                                <div class="error-message" v-if="errors.reason_text"> @{{ errors.reason_text }} </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-3">
                            <strong>Rate this {{ ucfirst($feedback_type) }}</strong>
                        </div>
                        <div class="col-md-6" v-if="!isEmptyRatingTypes">
                            <star-rating v-for="types in rating_types" :rating_type="types.text" :value.sync="types.value"></star-rating>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-3">
                            <strong>Your comments</strong>
                        </div>
                        <div class="col-md-4">
                            <div :class="['full-width', errors.comment ? 'error' : '' ]" v-cloak>
                                <textarea name="message" rows="5" v-model="comment" @keyup="errors.comment = ''"></textarea>
                                <div class="error-message" v-if="errors.comment"> @{{ errors.comment }} </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-3">
                            <strong>Note to {{ ucfirst($feedback_type) }}</strong>
                        </div>
                        <div class="col-md-4">
                            <div :class="['full-width', errors.note ? 'error' : '' ]" v-cloak>
                                <textarea name="message" rows="5" v-model="note" @keyup="errors.note = ''"></textarea>
                                <div class="error-message" v-if="errors.note"> @{{ errors.note }} </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-md-push-3 buttons">

                            <div class="col-md-5">
                                <!--<a class="btn btn-themed btn-primary btn-main-action" @click="endContract()">@{{ contractTitle }}</a>-->
                                <a class="btn btn-themed btn-primary btn-main-action" @click="endContract()">Save</a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ route('jobs.contracts.details', ['contract' => $contract->id]) }}" type="button" class="btn btn-themed btn-preview">Cancel</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@endsection