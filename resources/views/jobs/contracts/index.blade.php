@extends('app')

@section('content')
    <?php

    $contracts = $contracts->toJson();

    $contracts = json_decode($contracts, true);

    foreach($contracts as $key=>$contract) {
        $contracts[$key]['employee']['user_photo_100'] = '/photo_core/user/100_' . $contract['employee']['id'];
        $contracts[$key]['employee']['photo_url'] = '/photo_core/user/100_' . $contract['employee']['id'];
        $contracts[$key]['employee']['user_photo_64'] = '/photo_core/user/64_' . $contract['employee']['id'];
    }

    $contracts = json_encode($contracts, JSON_HEX_QUOT+JSON_UNESCAPED_SLASHES+JSON_UNESCAPED_UNICODE+JSON_PRETTY_PRINT);

    ?>
<div class="container padd-top-30" id="get-contract">
<vc-contract contracts="{{$contracts}}"></vc-contract>
    </div>
    
@endsection
