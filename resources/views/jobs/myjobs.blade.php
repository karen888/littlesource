@extends('app')

@section('content')

<div class="page-header">
<h3>My Jobs</h3>
</div>

<div class="row">


    <table class="table table-striped">
        <thead>
            <tr>
                <th>Sent</th>
                <th>Offer</th>
                <th>Job</th>
                <th>Client</th>
            </tr>
        </thead>
        <tbody>
            @foreach($jobs_list as $val)
            <tr>
                <td class="col-md-3">{{ $val->created_at }}</td>
                <td><a href="{{ routeClear('home_offer') }}/{{ $val->id }}">{{ $val->title }}</a></td>
                <td><a href="{{ routeClear('job_link') }}/{{ $val->path }}">{{ $val->title }}</a></td>
                <td>{{ $val->employer_name }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@endsection
