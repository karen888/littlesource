<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        @inject('carbon', 'Carbon\Carbon')
        @include('blocks.head')
        @include('blocks.styles')
    </head>
    <body class="page-<?php print str_replace(".", "-", Route::getCurrentRoute()->getName()); if(Auth::guest()){ print ' is_guest'; } ?>">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" id="_token">
        {!! $main_menu or 'none' !!}{{--<!--TODO: move it to a partial view, not necessary to be a ViewComposer-->--}}

        <div id="content-wrap" class="content-wrap">
            @yield('content')
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js"></script>
        @include('blocks.footer')
        @include('blocks.scripts')
        @yield('page-required-scripts'){{--<!--TODO: check if some views are using this-->--}}
    </body>
</html>
