@extends('contracts.contracts')

@section('content_contracts')

<div class="page-header">
<h3>My Freelancers</h3>
</div>

<div class="row">
    @foreach($data_list as $val)
        <div class="thumbnail">
        <div class="media" style="overflow: visible;">
            <div class="media-left">
                <img src="{{ userPhoto($val)->photo_64 }}">
            </div>
            <div class="media-body" style="overflow: visible;">
                <div class="row">
                    <div class="col-md-2">
                        <h4 class="media-heading">{{ $val->contractor_name }}</h4>
                        {{ $val->user_title }}
                    </div>
                    <div class="col-md-8">
                        @foreach($val->jobs as $key_job => $job)
                            <a href="{{ routeClear('job_link') }}/{{ $job->path }}">{{ $job->title }}</a>{{ ($val->jobs_count == $key_job) ? '' :',' }}
                        @endforeach
                    </div>
                    <div class="col-md-2"> 
                    </div>
                </div>
            </div>
            <div class="media-right media-middle" style="padding-right: 30px;">
                <div class="btn-group">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Message</a></li>
                        <li><a href="#">Another action</a></li>
                    </ul>
                </div>
            </div>
        </div>
        </div>
    @endforeach
</div>

<div class="row">
{!! $data_list->render() !!}
</div>

@endsection

