@extends('contracts.contracts')

@section('content_contracts')

<div class="page-header">
@if ($data_info->employer)
    <h3>Close contract</h3>
@else
    <h3>Feedback</h3>
@endif
</div>

<div class="row">
@if ($data_info->feedback_message)
    You wrote already your feedback.
@endif

@if ($data_info->can_proc)
	<form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id" value="{{ $data_info->offer->id }}">

        <div class="form-group">
			<div class="col-md-6 col-md-offset-4">
                <a href="{{ routeClear('job_link') }}/{{ $data_info->offer->path }}">{{ $data_info->offer->title }}</a>
			</div>
        </div>

        <div class="form-group">
			<div class="col-md-6 col-md-offset-4">
                <div class="media">
                    <div class="media-left">
                        <div class="thumbnail thumbnail-64"><img src="{{ $data_info->user_info->user_photo_64 }}"></div>
                    </div>
                    <div class="media-body">
                        <a href="{{ routeClear('user_link') }}/{{ $data_info->user_info->user_path }}">{{ $data_info->user_info->name }}</a>
                    </div>
                </div>
			</div>
        </div>

        <div class="form-group" data-name="rate_group">
			<label class="col-md-4 control-label">Rate</label>
			<div class="col-md-6">
                <!--
                <input type="text" name="rate" id="feedback-rate" class="rating" min=1 max=10 step=2 data-size="sm">
                -->

                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="1" autocomplete="off"> 1
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="2" autocomplete="off"> 2
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="3" autocomplete="off"> 3
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="4" autocomplete="off"> 4
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="5" autocomplete="off"> 5
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="6" autocomplete="off"> 6
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="7" autocomplete="off"> 7
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="8" autocomplete="off"> 8
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="9" autocomplete="off"> 9
                    </label>                    
                    <label class="btn btn-info">
                        <input type="radio" name="rate" value="10" autocomplete="off"> 10
                    </label>                    
                </div>
                <input type="text" name="rate_group" style="width: 0px; border: none;">
			</div>
        </div>

        <div class="form-group" data-name="feedback">
			<label class="col-md-4 control-label">Feedback</label>
			<div class="col-md-6">
                <textarea class="form-control" name="feedback" rows="10"></textarea>
			</div>
        </div>

        @if ($data_info->employer)
            @if ($data_info->offer->job_apply_fixed_price == 1)
            <div class="form-group post-job-fixed-price" data-name="pay" style="">
    			<label class="col-md-4 control-label">Pay</label>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">$</span>
                        <input class="form-control" name="pay" value="{{ $data_info->offer->job_apply_price }}" type="text">
                    </div>
    			</div>
            </div>

            <div class="form-group post-job-fixed-price" data-name="card" style="">
    			<label class="col-md-4 control-label">Billing method</label>
                <div class="col-md-3">
                    <select class="form-control" name="card">
                        <option value="0">Please select...</option>
                        @foreach ($data_info->cards as $val)
                            <option value="{{ $val->id }}">{{ $val->card_description }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif
        @endif

        <div class="form-group">
			<div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary send-req-form">Send</button>

                <a href="{{ routeClear('home') }}/contracts/my-contracts" class="btn btn-default">Cancel</a>
			</div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4 alert alert-warning alert-hidden">aaaa
            </div>
        </div>
    </form>
@endif
</div>

@endsection
