@extends('contracts.contracts')

@section('content_contracts')

<div class="page-header">
<h3>My Contracts</h3>
</div>

<div class="row">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>
                Freelancer
                </th>
                <th>
                Contract
                </th>
                <th>
                Job
                </th>
                <th>
                Action
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($contracts_list as $val)
            <tr>
                <td>
                    <a href="{{ routeClear('user_link') }}/{{ $val->user_path }}">{{ $val->contractor_name }}</a>
                </td>
                <td>
                    <a href="{{ routeClear('home_offer') }}/{{ $val->id }}">View</a>
                </td>
                <td>
                    <a href="{{ routeClear('job_link') }}/{{ $val->path }}">{{ $val->title }}</a>
                </td>
                <td class="col-md-2">
                    <a href="{{ routeClear('home') }}/contracts/my-contracts/close/{{ $val->id }}" class="btn btn-default">Close contract</a>
                </td>
            </tr>
            @endforeach
        </tbody>

    </table> 
</div>

<div class="row">
{!! $contracts_list->render() !!}
</div>

@endsection
