<header id="auth-menu" class="main-header auth-menu">
    <div class="main-header-inner">
        <h1 class="main-logo">
            <a class="navbar-brand" href="/"><img src="/img/logo.png" alt="Little ones" style="height: 60px; width: auto !important;"></a>
        </h1>
        <div class="header-auth-row">
            {{--<nav class="main-nav-auth">--}}
                {{--@if ($isCaregiver)--}}
                    {{--<ul class="list-inline">--}}
                        {{--<li>--}}
                            {{--@if(App::environment() == "staging")--}}
                            {{--<a href="{{ route('caregiver.dashboard') }}">Find Work</a>--}}
                            {{--@else--}}
                            {{--<a href="#"  data-placement="bottom" data-toggle="popover-hover" data-content="This feature will be available soon. Until then, get your profile looking sharp, get yourself verified, and when we launch your sparkling new profile will be ready to go!"--}}
                            {{-->Find Work</a>--}}
                            {{--@endif--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--@if(App::environment() == "staging")--}}
                            {{--<a href="{{ route('employee.my-jobs') }}">My Jobs</a>--}}
                            {{--@else--}}
                            {{--<a href="#" data-placement="bottom" data-toggle="popover-hover" data-content="This feature will be available soon. Until then, get your profile looking sharp, get yourself verified, and when we launch your sparkling new profile will be ready to go!">My Jobs</a>--}}
                            {{--@endif--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--@if(App::environment() == "staging")--}}
                            {{--<a class="@if ($count_new_message>0) new-message @endif" href="{{ route('messages.index') }}">Messages--}}
                                {{--@if ($count_new_message>0)--}}
                                    {{--<div class="messages-count"><label>{{ $count_new_message }}</label></div>--}}
                                {{--@endif--}}
                            {{--</a>--}}
                            {{--@else--}}
                            {{--<a href="#" data-placement="bottom" data-toggle="popover-hover" data-content="This feature will be available soon. Until then, get your profile looking sharp, get yourself verified, and when we launch your sparkling new profile will be ready to go!">--}}
                                {{--Messages--}}
                            {{--</a>--}}
                            {{--@endif--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--@else--}}

                    {{--<ul class="list-inline">--}}
                        {{--<li>--}}
                            {{--<a href="{{ route('client.jobs.my-jobs') }}">Jobs</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<a href="{{ route('client.employee.hired') }}">Caregivers</a>--}}
                        {{--</li>--}}

                        {{--<li>--}}
                            {{--<a class="@if ($count_new_message>0) new-message @endif" href="{{ route('messages.index') }}">Messages--}}
                                {{--@if ($count_new_message>0)--}}
                                    {{--<div class="messages-count"><label>{{ $count_new_message }}</label></div>--}}
                                {{--@endif--}}
                            {{--</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--@endif--}}
            {{--</nav>--}}

            <nav class="profile-nav">
                    <profile-box
                        {{--unread="{{\App\Http\Models\VerificationChats::getCountUserUnread()}}"--}}
                        settings="{{ url('/') }}"
                        logout="{{ url('auth/logout') }}"
                        {{-- profile="{{ route('employee.profile.edit') }}" --}}
                        :user="{{ json_encode($currentUser) }}"
                        >
                    </profile-box>
                {{--<a class="settings" href="{{ route('client.settings.my-info') }}" ><span class="icon icon-settings"></span>--}}
                    {{--@if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread()) > 0)--}}
                        {{--<span  style="display: inline-block;position: relative;float:left;left: 37px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:8px;" class="unread-settings-badge">--}}
                            {{--{{$unread}}--}}
                        {{--</span>--}}
                    {{--@endif--}}
                {{--</a>--}}
                <a class="logout" href="{{ url('auth/logout') }}" ><span class="icon icon-logout"></span></a>
            </nav>
        </div>

    </div>
</header>
@if(if_route_pattern(['client.jobs','client.jobs.*', 'client.employee', 'client.employee.*', 'caregiver.dashboard','employee.saved-jobs','employee.my-jobs','employee.contracts','employee.work-diary','employee.profile.edit','caregivers.employee.employee-proposals','client.settings.my-info', 'caregiver*', 'jobs*', 'employee*', 'client.settings*' ]))
<div class="sub-nav-wrapper">
    <nav class="sub-nav">
        <div class="col-md-10 col-md-push-2">
            <ul class="list-inline">

                @if(App::environment() == "staging")
                <?php /* TODO: Uncomment the below after parents are allowed */ ?>
                @if ($isCaregiver)
                @if(if_route_pattern(['caregiver.dashboard','caregivers.employee.employee-proposals','employee.saved-jobs']))
                <li class="{{ Route::is('caregiver.dashboard') ? ' active ' : '' }}"><a href="{{ route('caregiver.dashboard') }}" title="">Find Jobs</a></li>
                {{--<li class="{{ Route::is('caregiver.dashboard') ? ' active ' : '' }}"><a href="#" >Find Jobs</a></li>--}}
                <li class="{{ Route::is('employee.saved-jobs') ? ' active ' : '' }}"><a href="{{ route('employee.saved-jobs') }}" title="">Saved Jobs</a></li>
                <li class="{{ Route::is('caregivers.employee.employee-proposals') ? ' active ' : '' }}"><a href="{{ route('caregivers.employee.employee-proposals') }}" title="">Proposals</a></li>
				 <li class="{{ Route::is('employee.profile.edit') ? ' active ' : '' }}"><a href="{{ route('employee.profile.edit') }}" title="">My Profile</a></li>
                @endif
                @if(if_route_pattern(['employee.my-jobs','employee.contracts','employee.work-diary']))
                <li class="{{ Route::is('employee.my-jobs') ? ' active ' : '' }}"><a href="{{ route('employee.my-jobs') }}" title="">My Jobs</a></li>
                <li  class="{{ Route::is('employee.contracts') ? ' active ' : '' }}"><a href="{{ route('employee.contracts') }}" title="">Contracts</a></li>
                <li  class="{{ Route::is('employee.work-diary') ? ' active ' : '' }}"><a href="{{ route('employee.work-diary') }}" title="">Work Diary</a></li>
                @endif
                @else
                @if (if_route_pattern(['client.jobs.*']))
                <li class="{{ active_class(if_route_pattern(['client.jobs.my-jobs', 'client.jobs.my-jobs.*'])) }}"><a href="{{ route('client.jobs.my-jobs') }}" title="">My Jobs</a></li>
                <li class="{{ active_class(if_route(['client.jobs.contracts'])) }}"><a href="{{ route('client.jobs.contracts') }}" title="">Contracts</a></li>
                <li class="{{ active_class(if_route(['client.jobs.create'])) }}"><a href="{{ route('client.jobs.create') }}" title="">Post a Job</a></li>
                @endif
                @if(if_route_pattern(['client.employee.*', 'client.work-diary.*','employee.work-diary']))
                <li class="{{ active_class(if_route_pattern(['client.employee.*'])) }}"><a href="{{ route('client.employee.hired') }}" title="">My Caregivers</a></li>
                <li class="{{ active_class(if_route(['client.employee.find'])) }}"><a href="{{ route('client.employee.find') }}" title="">Find Caregivers</a></li>
                <li class="{{ active_class(if_route(['client.work-diary.*'])) }}"><a href="{{ route('employee.work-diary') }}" title="">Work Diary</a></li>
                @endif
                @endif
                @endif
            <div class="hidden-sm hidemenu">
                <a class="settings" href="{{ route('client.settings.my-info') }}" ><span class="icon icon-settings"></span></a>
                <a class="logout" href="{{ url('auth/logout') }}" ><span class="iicon icon-logout"></span></a>
            </div>   				
            </ul>
        </div>	
    </nav>
</div>
@endif
