<header class="main-header">
    <div class="main-header-inner">
        @include('flash::message')
        <h1 class="main-logo">
            <a class="navbar-brand" href="/"><img src="/img/logo.png" alt="Little ones" style="height: 60px; width: auto !important;"></a>
        </h1>

        <nav class="top-nav-box">
            <ul class="nav nav-pills">

                <li class="reg" style="font-weight: bold; ">
                    <a href="/pre-register" style="color :#73c5bc">Create Account</a>
                </li>

                <li class="log">
                    <a href="/auth/login">Log In</a>
                </li>
                {{--<li>--}}
                    {{--<div class="search-dropdown input-group">--}}
                        {{--<div class="input-group-btn">--}}
                            {{--<button type="button" class="btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                            {{--<i class="fa fa-search"></i> <span class="fa fa-angle-down"></span></button>--}}
                            {{--<div class="dropdown-menu">--}}
                                {{--<form action="">--}}
                                    {{--<div class="dropdown-menu-search-row">--}}
                                        {{--<input id="search-caregivers" name="for" type="radio" checked />--}}
                                        {{--<label for="search-caregivers">Find Caregiver</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="dropdown-menu-search-row">--}}
                                        {{--<input id="search-jobs" name="for" type="radio" />--}}
                                        {{--<label for="search-jobs">Find Jobs</label>--}}
                                    {{--</div>--}}
                                {{--</form>--}}
                            {{--</div>--}}

                        {{--</div><!-- /btn-group -->--}}
                        {{--<input type="text" class="form-control" aria-label="..." placeholder="">--}}
                    {{--</div><!-- /input-group -->                    --}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<button class="btn btn-default" id="become_caregiver">Become a Caregiver</button>--}}
                {{--</li>--}}
            </ul>
        </nav>

    </div>
</header>