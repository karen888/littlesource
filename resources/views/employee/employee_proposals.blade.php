ыш@extends('app')

@section('content')

    <div id="content-wrap" class="content-wrap">
        <div class="container">
            <div class="content">
                <div class="caregiver-profile" id="employee-profile">
                    <div class="col-md-12 search_blok">
                        <div class="col-md-3">
                            <div class="details">
                                <h4>My Proposals</h4>
                                <a href="#">Active</a> | <a href="#">Archived</a>
                            </div>
                        </div>

                    </div>
                   <div class="col-md-12">
                        <div class="heading">
                            <h5>Active Candidacies(<?php print count($inter); ?>) </h5>
                            <hr>
                        </div>
                        <div class="panel job-postings">
                           <div class="panel-row empty"  id="ActiveCandida">
                                    <h5>No active proposals </h5> 
                            </div>
                              @foreach($inter as $prop)
                               @if($prop->status == "accept")
                                <input type="hidden" value="{{$prop->status}}" id="candidacies">
                                 <div class="panel-row">
                                    <div class="values">
                                        <div class="job-details">
                                            <div class="name">
                                                <a href="/jobs/{{$prop->id}}/details"><strong>{{$prop->title}}</strong></a>
												<div class="date-posted">Posted {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $prop->created_at)->diffForHumans() }}</div>
                                            </div>
                                        </div>
                                        <div class="rates">
                                            ${{$prop->hourly_rate}}/hour
                                        </div>                                        
                                        <div class="caregiver">
                                            <img class="img_50" src="/img/no_photo_100.jpg" alt="">
                                            <strong>{{ $prop->first_name}} {{$prop->last_name}}</strong>
                                        </div>
                                    <div class="actions">
                                    <a class="btn btn-themed btn-primary btn-main-action send-message-button" href="{{ route('messages.index', ['direct' => $prop->user_id]) }}">Send Message</a>
                                     </div>
                                    </div>
                                    </div>    
                                   @endif
                              @endforeach
                                </div>
                            </div>
                
                    <div class="col-md-12">
                        <div class="heading">
                            <h5>Invitations to Interview(<?php print count($invit); ?>)</h5>
                            <hr>
                        </div>
                            <div class="panel job-postings">
                                <div class="panel-row empty"  id="ActivetInt">
                                    <h5 id="texto">No active proposals </h5>
                                </div>
                               @foreach($invit as $prop2)
                              @if($prop2->status =="sended")
                                <input type="hidden" value="{{$prop2->status}}" id="interview">
                                <div class="panel-row">
                                    <div class="values">
                                        <div class="job-details">
                                            <div class="name">
                                                <a href="/jobs/{{$prop2->id}}/details"><strong>{{$prop2->title}}</strong></a>
                                                <div class="date-posted">Posted {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $prop2->created_at)->diffForHumans() }}</div>
                                            </div>
                                        </div>
                                        <div class="caregiver">
                                            <img class="img_50" src="/img/no_photo_100.jpg" alt="">
                                            <strong>{{ $prop2->first_name}} {{$prop2->last_name}}</strong>
                                        </div>
                                        <div class="actions">
                                        <a class="btn btn-themed btn-primary btn-main-action send-message-button" href="{{ route('messages.index', ['direct' => $prop2->user_id]) }}">Send Message</a>
                                    <!--<div class="actions-group">
                                        <div class="trigger">
                                             <div class="text_outer"><div class="text trn"> Actions </div></div>
                                        </div>
                                     <div class="list">
                                         <div class="option"
                                              data-action="edit-job"
                                              class="trn">
                                              <a href="{{ route('messages.index', ['direct' => $prop2->user_id]) }}">Send Message</a>
                                         </div>
                                        <div class="option"
                                             data-action="view-profile"
                                             class="trn">
                                        <a href="/jobs/{{$prop2->id}}/details">View Details</a>
                                        </div>
                                       </div>
                                    </div>-->
                                  </div>
                                  </div>
                                </div>    
                                 @endif
                              @endforeach
                                </div>
                    </div>

                    <div class="col-md-12">
                        <div class="heading">
                            <h5>Submitted Proposals(<?php print count($app); ?>)</h5>
                            <hr>
                        </div>
                           <div class="panel job-postings">
                                <div class="panel-row empty"  id="activeProp">
                                    <h5>No active proposals </h5>
                                </div>
                                @foreach($app as $prop3)
                                  @if($prop3->by_invite == "0")
                                  <input type="hidden" value="{{$prop3->by_invite}}" id="proposal">
                                <div class="panel-row">
                                    <div class="values">
                                        <div class="job-details">
                                            <div class="name">
                                                <a href="/jobs/{{$prop3->id}}/details"><strong>{{$prop3->title}}</strong></a>
                                                <div class="date-posted">Posted {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $prop3->created_at)->diffForHumans() }}</div>
                                            </div>
                                        </div>
                                        <div class="rates">
                                            ${{$prop3->hourly_rate}}/hour
                                         </div>                                        
                                        <div class="caregiver">
                                            <img class="img_50" src="/img/no_photo_100.jpg" alt="">
                                            <strong>{{ $prop3->first_name}} {{$prop3->last_name}}</strong>
                                        </div>
                                      <div class="actions">
                                      <a class="btn btn-themed btn-primary btn-main-action send-message-button" href="{{ route('messages.index', ['direct' => $prop3->user_id]) }}">Send Message</a>
                                        </div>
                                    </div>
                                </div>    
                               @endif
                              @endforeach
                                </div>
                                </div>
                            </div>
                    <div class="col-md-12" style="margin-bottom:50px;">
                        <div class="margin"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $( document ).ready(function() {

            $(".close_info_success").on("click", function () {
                $(".close_info_success").parent().remove();
            });

             var candidacies  = $("#candidacies").val();
             if(candidacies == "accept" || candidacies == "undefined")
               {
                $("#ActiveCandida").hide();
               }
              
             var interview  = $("#interview").val();
          
             if(interview == "sended" || interview == "undefined" )
               {
                     $("#ActivetInt").hide();
               }
               
             var proposal = $("#proposal").val();
             if(proposal == "0" || proposal == "undefined")
               {
                     $("#activeProp").hide();
               }

             });
    </script>
@endsection
