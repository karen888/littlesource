@extends('app')

@section('content')

<div class="container padd-top-30">
    @include('flash::message')
    <div class="row">
        <div class="col-md-10">
            <div class="main-heading">
                <h4>My Jobs</h4>
            </div>
        </div>
        <div class="col-md-2">
            <a href="contracts" class="btn btn-themed btn-primary btn-main-action pull-right">View all Contracts</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <h5>Hourly</h5>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel job-postings job_list">
            @if($jobs->isEmpty())
                <div class="panel-row empty">
                    <h5>You currently have no open job postings</h5>
                </div>
            @else
            @foreach ($jobs as $job)
                <div class="panel-row">
                    <div class="values">
                        <div class="job-details">
                            <div class="name">
                               <a href="{{ route('caregiver.jobs.details', ['job' => $job->id]) }}"> <strong> {{ str_limit( $job->title , 100) }} </strong></a>
                            </div>
                            <div class="date-posted">
                                Hired by {{ $job->user->first_name . ' ' . $job->user->last_name }}
                            </div>
                        </div>
                        <div class="applicants">
                            <strong> 24h</strong> {{--hours worked this week--}}
                        </div>
                        <div class="applicants"></div>
                        <div class="messaged">
                            <strong> ${{ round($job->acceptedOffer()->offer_paid) }} /hr </strong> {{--= $500--}}
                        </div>
                        <div class="actions">
                            <div class="actions-group">
                                <div class="trigger">
                                    <div class="text_outer"><div class="text trn"> Actions </div></div>
                                </div>
                                <div class="list">
                                    <div class="option"
                                    class="trn">
                                        <a href="{{ route('messages.index', ['direct' => $job->user_id]) }}">Send Message</a>
                                    </div>
                                    <div class="option"
                                    data-action="send-message"
                                    class="trn">
                                        <a href="{{ route('employee.work-diary') }}">View Work Diary</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            @endif
            </div>
        </div>
        &nbsp;
    </div>
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<a class="job-posting-action" href="{{ route('employee.contracts') }}">View all job postings</a>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>


@endsection



