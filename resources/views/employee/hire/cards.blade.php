<div class="billing-method-form">
    <div class="form-row select-bil">
        <div class="field">
            <strong>Billing Method</strong>
        </div>
        <div class="value">
            <div :class="[cards.errors.message ? 'error' : '' ]">
                <custom-select class="choose_job"
                    :options="cards.options"
                    @click="cards.errors.message = ''"
                    :selected.sync="cards.selected"
                    @selected-value="cardsSelectedValue"
                    name="billing method"
                >
                </custom-select>
                <div class="error-message" v-if="cards.errors.message"><span v-html="cards.errors.message"></span></div>
            </div>
        </div>
        <input type="hidden" v-model="cards.options" value="{{ json_encode($cards) }}" />
    </div>
    <div class="form-row">
        <div class="field">
        </div>
        <div class="value">
            <a>+ Add a Billing Method</a>
        </div>
    </div>
</div>
