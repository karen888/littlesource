@extends('app')

@section('content')
<div class="container" id="hire-employee">
    <div class="content">
        <div class="row padd-top-50">
            <div class="col-md-11 col-md-push-1">
                @if(false === $employee->isUserFullyVerified())

                        <div class="alert alert-warning">
                            <table>
                                <tr>
                                    <td>
                                        <i style="font-size: 24pt;" class="fa fa-exclamation-triangle"></i>
                                    </td>
                                    <td style="padding-left: 12px;">
                                        This caregiver has submitted all their verification documents (identity, qualifications, etc) but we have not verified those documents yet. Verification usually takes a couple of days. As soon as the documents are verified the contract will automatically become active.
                                    </td>
                                </tr>
                            </table>

                        </div>


                @endif
                <div class="HireCaregiver">
                    <img class="HireCaregiver__image" src="{{ $employee->PhotoUrl }}" alt="">
                    <h4 class="HireCaregiver__header">Hire {{ $employee->name }}</h4>
                    <input type="hidden" name="caregiver" v-model="caregiver.text" value="{{ $employee->id }}">
                    <input type="hidden" name="job" v-model="job.selected" value="{{ $job }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-11 col-md-push-1">
                <div class="panel Panel--HireCaregivers HireCaregiversForm">
                    <form action="" method="get" accept-charset="utf-8">
                        <div class="panel-row">
                            <div class="HireCaregiversForm__row">
                                <h5 class="HireCaregiversForm__heading">Contract Details</h5>
                            </div>
                            <div class="HireCaregiversForm__row">
                                <div class="HireCaregiversForm__field">
                                    <label for="#">Contract Title</label>
                                </div>
                                <div class="HireCaregiversForm__value" :class="[title.errors.message ? 'error' : '' ]">
                                    <input type="text" v-model="title.text" @keyup="title.errors.message = ''" />
                                    <div class="error-message" v-if="title.errors.message"><span v-html="title.errors.message"></span></div>
                                </div>
                            </div>
                            <div class="HireCaregiversForm__row">
                                <div class="HireCaregiversForm__field">
                                    <label for="#">Job</label>
                                </div>
                                <div class="HireCaregiversForm__value">
                                    <div :class="[job.errors.message ? 'error' : '' ]">
                                        <custom-select class="estimated_duration"
                                            :options="{{ json_encode($jobs) }}"
                                            @click="job.errors.message = ''"
                                            :selected.sync="{{$job}} != null ? {{$job}} : job.selected"
                                            name="job"

                                            @selected-value="jobSelectedValue"
                                        >
                                        </custom-select>
                                        <div class="error-message" v-if="job.errors.message"><span v-html="job.errors.message"></span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="HireCaregiversForm__row">
                                <div class="HireCaregiversForm__field">
                                    <label for="#">Hourly Rate</label>
                                </div>
                                <div class="HireCaregiversForm__value ">
                                    <div class="HireCaregiversForm__hourly-rate" :class="[rate.errors.message ? 'error' : '' ]">
                                        <div class="HireCaregiversForm__currency">$</div>
                                        <input type="text" name="rate" v-model="rate.text" @blur="setRate()" @keyup="setRate()" />
                                        <div class="HireCaregiversForm__rate">/hr</div>
                                    </div>
                                    <div class="error-message" v-if="rate.errors.message"><span v-html="rate.errors.message"></span></div>
                                    <div class="HireCaregiversForm__message">{{ $employee->name }} profile rate is ${{ $employee->employee_info->view_rate }}/hr</div>
                                </div>
                            </div>
                            <div class="HireCaregiversForm__row">
                                <div class="HireCaregiversForm__field">
                                    <label for="#">Start Date</label>
                                </div>
                                <div class="HireCaregiversForm__value HireCaregiversForm__value--start-date" :class="[start_date.errors.message ? 'error' : '' ]">
                                    <div class="icon icon-calendar"></div>
                                    <input data-inputmask="'alias': 'date'" id="start-date" v-model="start_date.text" @keyup="start_date.errors.message = ''" /> &nbsp;&nbsp;&nbsp;&nbsp; <a href="javascript:void(0);" @click="setToday()">Set today</a>
                                    <div class="error-message" v-if="start_date.errors.message"><span v-html="start_date.errors.message"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-row add-billing-method" v-show="!cards.options">
                            <h5>Add Billing Method</h5>
                            @include('user.billing-methods.form')
                        </div>
                        <div class="panel-row add-billing-method" v-show="cards.options">
                            <h5></h5>
                            @include('employee.hire.cards')
                        </div>
                        <div class="panel-row">
                            <div class="HireCaregiversForm__row HireCaregiversForm__row--additional-info">
                                <div class="HireCaregiversForm__field HireCaregiversForm__field--additional-info">
                                    <label for="#">Additional Information</label>
                                </div>
                                <div class="HireCaregiversForm__value HireCaregiversForm__value--additional-info" :class="[additional_info.errors.message ? 'error' : '' ]">
                                    <textarea name="additional-info" rows="6" v-model="additional_info.text" @keyup="additional_info.errors.message = ''"></textarea>
                                    <div class="error-message" v-if="additional_info.errors.message"><span v-html="additional_info.errors.message"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-row">
                            <div class="HireCaregiversForm__actions">
                                <div class="check">
                                    <input type="checkbox" id="option3" name="locked-status" value="accept" checked>
                                    <label for="option3">
                                        Yes, I understand and agree the LittleOnes <a href="#">Terms of Service</a>, including the <a href="#">User Agreement</a> and <a href="#">Privacy Policy</a>.
                                    </label>
                                </div>

                                <a href="#" class="btn btn-themed btn-primary btn-main-action" @click.prevent="createCard">Hire{{-- {{ $employee->name }} --}}</a>
                                <a href="{{ URL::previous() }}" class="btn btn-themed btn-preview">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection