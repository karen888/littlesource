@extends('employee.caregivers-template')

@section('caregivers_content')
    <div class="panel margin-top-60">
        <input type="hidden" v-model="type" value="hired">
            <div class="panel-row caregivers-row" v-for="user in users" v-if="!emptyUsers">
                <img class="img_50" :src="user.photo_url" alt="">
                <div class="name"><a :href="user.profile"><strong>@{{ user.name }}</strong></a></div>
                <div class="actions">
                    <div class="actions-group">
                        <div class="trigger">
                            <div class="text_outer"><div class="text trn"> Actions </div></div>
                        </div>
                        <div class="list">
                            <div class="option"
                            data-action="send-message"
                            class="trn">
                                <a :href="user.invite">Invite to job</a>
                            </div>
                            <div class="option"
                            data-action="send-message"
                            class="trn">
                                <a :href="user.messages">Send Message</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-row empty" v-if="emptyUsers">
                <h5>You have not hired any caregivers yet.</h5>
            </div> 
    </div>
<script type="text/javascript">
    $(document).ready(function(){
        $(".actions-group").on('click', '.actions', function(){
           // alert('pisa');
        });
    });
</script>
@endsection
