@extends('app')

@section('content')
    <div  class="content-wrap" id="emplo-contract">
        @if(request('newbie') === "true")
        <div style="max-width:1200px; margin: 0 auto; position: relative; top: 30px;" class="alert alert-success">You will be able to receive references after successfully ending a contract</div>
        @endif

        <?php

            /** @var \Illuminate\Support\Collection $empcontract */
        $empcontract = $empcontract->toArray();


        foreach($empcontract as $k=>$item) {
            $empcontract[$k]['created_at'] = date('M j, Y', strtotime($item['created_at']));

            if($item['end_date']) {
                $empcontract[$k]['end_date'] = date('M j, Y', strtotime($item['end_date']));
            }
        }

        $empcontract = json_encode($empcontract);

        ?>
        <vc-contracts empcontract="{{$empcontract}}"></vc-econtracts>
	</div>
@endsection
