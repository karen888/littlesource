@extends('app')

@section('content')
<div class="container">
    <div class="content">
        <div class="caregiver-profile">
            @include('employee.profile.view')
        </div>
    </div>
</div>
@endsection