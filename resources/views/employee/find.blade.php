@extends('app')

@section('content')
<div class="container" id="find-caregivers">
    <div class="content">    
        <div class="row padd-top-50">
        <div class="row back-action">
            <div class="col-md-12">
                <a href="{{ URL::previous() }}" class="back-button"><span class="back-icon"></span>Back to Previous Page</a>
            </div>
        </div>            
            <div class="col-md-12">
                <h4>Find Caregivers</h4>
            </div>
        </div>
    </div>
    <div class="FindCaregivers content" v-cloak>
        <div class="FindCaregivers__address-range row">
            <div class="FindCaregivers__address col-md-6" :class="[postcode.error ? 'error' : '' ]">
                <input id="autocomplete" type="text" placeholder="Enter Post Code" v-model="postcode.value">
                <input type="hidden" v-model="loc.lat" id="lat"/>
                <input type="hidden" v-model="loc.lng" id="lng"/>
                <div class="error-message" v-if="postcode.error"> @{{ postcode.error }} </div>
            </div>
            <div class="FindCaregivers__range col-md-6">
                <strong>Search Within</strong>
                <div id="slider"></div>
            </div>
        </div>
        <div class="FindCaregivers__filters content">
            <div class="FindCaregivers__filter FindCaregivers__filter--care-type">
                <label for="#">Care Type</label>
                <care-type class="care-type" name="care-type" id="care-type" v-if="selects"
                    :options="selects.care_type.items" :multiple="selects.care_type.multiple" :status-icon="true" :selected.sync="selects.care_type.selected">
                </care-type>
                <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
            </div>

            <div class="FindCaregivers__filter FindCaregivers__filter--child-age">
                <label for="#">Child Age Range</label>
                <child-age class="child-age" name="child-age" id="child-age" v-if="selects"
                    :options="selects.child_age.items" :multiple="selects.child_age.multiple" :status-icon="true" :selected.sync="selects.child_age.selected" :msg-all="All">
                </child-age>
                <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
            </div>

            <div class="FindCaregivers__filter FindCaregivers__filter--availability">
                <label for="#">Availability Days</label>
                <availability class="availability" name="availability" id="availability" v-if="selects"
                    :options="selects.availability.items" :multiple="selects.availability.multiple" :status-icon="true" :selected.sync="selects.availability.selected" msg-all="All days">
                </availability>
                <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
            </div>

            <div class="FindCaregivers__filter FindCaregivers__filter--employment-type">
                <label for="#">Employment Type</label>
                <employment-type class="employment-type" name="employment-type" id="employment-type" v-if="selects"
                    :options="selects.employment_type.items" :multiple="selects.employment_type.multiple" :status-icon="true" :selected.sync="selects.employment_type.selected">
                </employment-type>
                <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
            </div>

            <div class="FindCaregivers__action find">
                <a href="#map" class="btn btn-themed btn-primary" @click.stop="postSearch(false, true)">Search</a>
            </div>
        </div>
    </div>
    <div class="FindCaregivers FindCaregivers--advanced-filters row">
        <div class="FindCaregivers__toggle-filters col-md-12">
            <a class="btn btn-themed btn-hire-me" v-show="!showAdvanced" @click.stop="setShowAdvanced(true)">Show Advanced Filters</a>
            <a class="btn btn-themed btn-hire-me Hide" v-show="showAdvanced" style="display:none;" @click.stop="setShowAdvanced(false)">Hide Advanced Filters</a>
        </div>
        <div class="FindCaregivers__filters FindCaregivers__filters  row" v-show="showAdvanced">
            <div class="FindCaregivers__half-column col-md-6">
                {{--<div class="FindCaregivers__filter FindCaregivers__filter--qualifications">--}}
                    {{--<label for="#">Qualifications:</label>--}}
                    {{--<qualification class="qualification" name="qualification" id="qualification" :as-list="true" v-if="selects" :multiple-list="true"--}}
                        {{--:options="selects.qualification.items" :multiple="selects.qualification.multiple" :selected.sync="selects.qualification.selected" :required="false">--}}
                    {{--</qualification>--}}
                {{--</div>--}}

                <div class="FindCaregivers__filter FindCaregivers__filter--w200">
                    <label for="#">Years of experience:</label>
                    <years-experience class="years-experience" name="years-experience" id="years-experience" v-if="selects" :as-list="true" :list-preference="true"
                        :options="selects.years_experience.items" :multiple="true" :status-icon="true" :selected.sync="selects.years_experience.selected" :required="false">
                    </years-experience>
                    <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
                </div>

                <div class="FindCaregivers__filter FindCaregivers__filter--range">
                    <label for="#">Willing to care for sick children:</label>
                    <care-sick class="care-sick" name="care-sick" id="care-sick" v-if="selects" :as-list="true"
                        :options="selects.care_sick.items" :multiple="false" :status-icon="true" :selected.sync="selects.care_sick.selected">
                    </care-sick>
                    <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
                </div>

                <div class="FindCaregivers__filter FindCaregivers__filter--range">
                    <label for="#">Comfortable with pets:</label>
                    <pets class="pets" name="pets" id="pets" v-if="selects" :as-list="true"
                        :options="selects.pets.items" :multiple="false" :status-icon="true" :selected.sync="selects.pets.selected">
                    </pets>
                    <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
                </div>
                <div class="FindCaregivers__filter FindCaregivers__filter--range">
                    <label for="#">Smoker:</label>
                    <smoker class="smoker" name="smoker" id="smoker" v-if="selects" :as-list="true"
                            :options="selects.smoker.items" :multiple="false" :status-icon="false" :selected.sync="selects.smoker.selected">
                    </smoker>
                    <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
                </div>
            </div>
            <div class="FindCaregivers__half-column col-md-6">
                <div class="FindCaregivers__filter FindCaregivers__filter--range">
                    <label for="#">Age range:</label>
                    <age-range class="age-range" name="age-range" id="age-range" :as-list="true" v-if="selects" :list-preference="true"
                        :options="selects.age_range.items" :multiple="selects.age_range.multiple" :status-icon="false" :selected.sync="selects.age_range.selected" :required="true">
                    </age-range>
                </div>

                <div class="FindCaregivers__filter FindCaregivers__filter--range">
                    <label for="#">Gender:</label>
                    <gender class="gender" name="gender" id="gender" :as-list="true" v-if="selects"
                        :options="selects.gender.items" :multiple="selects.gender.multiple" :status-icon="false" :selected.sync="selects.gender.selected" :required="true">
                    </gender>
                </div>

                <div class="FindCaregivers__filter FindCaregivers__filter--range">
                    <label for="#">Have own children:</label>
                    <own-children class="own-children" name="own-children" id="own-children" :as-list="true" v-if="selects"
                        :options="selects.own_children.items" :multiple="false" :status-icon="false" :selected.sync="selects.own_children.selected" :required="false">
                    </own-children>
                </div>

                <div class="FindCaregivers__filter FindCaregivers__filter--w200">
                    <label for="#">Drivers licence:</label>
                    <drivers-licence class="drivers-licence" name="drivers-licence" id="drivers-licence" v-if="selects" :as-list="true" :list-preference="true"
                        :options="selects.drivers_licence.items" :multiple="false" :status-icon="true" :selected.sync="selects.drivers_licence.selected">
                    </drivers-licence>
                    <div class="customselect" v-else><div class="trigger"><div class="text trn text-muted"><i class="fa fa-spinner fa-spin fa-fw"></i> Loading ...</div></div></div>
                </div>


            </div>
        </div>
    </div>

    {{--<div id="map"></div>--}}
    <div class="clearfix"></div>
    <hr>
    <ul class="pagination">
        <li :class="current_page == page ? 'active' : ''" v-for="page in pages">
            <a href="javascript:;" @click="setUsersPage(page)" >@{{ page }}</a>
        </li>
    </ul>
    <div id="map"></div>
    @include('employee.find_result')
    <div class="clearfix"></div>
    <hr>
</div>
@endsection

@section('page-required-scripts')
<script type="text/javascript">
    var map;
    var userLocation = {
        lat: {{Auth::user()->info->lat}},
        lng: {{Auth::user()->info->lng}}
    };
    window.radiusCircle = null;
    var mapOptions = {
        center: {lat: {{ $lat }}, lng: {{ $lng }}},
        zoom: 12,
        minZoom: 1
    };
    function initMap() {
      map = new google.maps.Map(document.getElementById('map'), mapOptions);
     window.radiusCircle = new google.maps.Circle({
        strokeColor: '#5796df',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#40aaff',
        fillOpacity: 0.1,
        map: map,
        center: userLocation,
        radius: 5000
    });

    }
</script>
{{--TODO: async defer, now it gives an error, probably need to shim "google" --}}
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{ config('google.map_key') }}&libraries=places&signed_in=true&callback=initMap"></script>

@endsection