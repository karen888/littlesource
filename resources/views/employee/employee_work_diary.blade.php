@extends('app')

@section('content')

	<div  class="content-wrap" id="emplo-contract" style="margin-bottom:50px;">
	<div class="container">
            <div class="content">
                <div class="caregiver-profile" id="employee-profile">
                    <div class="col-md-9 ">
           
                            		<div class="details">
                               		<h4>Work Diary</h4>
                                	
                                    <div class="filter">
                                        <div class="inner-addon left-addon">
                                         <select id="work" class="form-control">
											  <option value="0" selected>We need a caregiver to care for my children 5 years old </option>
											  <option value="1">I need a caregiver to care for my children 7 years for 3 hours</option>
											  <option value="2">We need a caregiver who do not smoke and you can take them to the park for 2 hours</option>
									     </select>
                                        </div>
                                    	</div>
                            		</div>
                      </div>    
                </div>
                <div class="row" id="sche1">
                        <div id="scheduler"></div>
                </div>

                <div class="row"  >
                        <div id="scheduler2"></div>
                </div>

                <div class="row"  >
                        <div id="scheduler3"></div>
                </div>

                </div>
            </div>
        </div>

        
<link href="http://cdn.alloyui.com/3.0.1/aui-css/css/bootstrap.min.css" rel="stylesheet"></link>
<script src="http://cdn.alloyui.com/3.0.1/aui/aui-min.js"></script>
   
 <script>
    YUI({ filter: 'raw' }).use('aui-scheduler', function(Y) {

        var items = [
            {
                content: 'I care for the child and gave her food'
            },
           
            {
                content: 'Review or (even better) Rewrite Your Goals',
                startDate: new Date(2013, 1, 4, 12),
                endDate: new Date(2016, 1, 4, 16)
            },
            {
                content: 'Event 1',
                startDate: new Date(2014, 6, 4),
                endDate: new Date(2014, 6, 20),
                allDay: true
            },
            {
                content: 'Event 2',
                startDate: new Date(2015, 6, 4),
                endDate: new Date(2015, 6, 20),
                allDay: true
            },
            {
                content: 'Event 3',
                startDate: new Date(2014, 6, 4),
                endDate: new Date(2014, 6, 20),
                allDay: true
            },
            {
                content: 'Other event',
                startDate: new Date(2014, 6, 20),
                endDate: new Date(2014, 6, 22),
                allDay: true
            },
            {
                content: 'Other event 2',
                startDate: new Date(2014, 6, 23),
                endDate: new Date(2014, 6, 28),
                allDay: true
            }
        ];

        var schedulerViews = [
        
            new Y.SchedulerDayView(),
      
            
        ];

        new Y.Scheduler({
            boundingBox: '#scheduler',
            items: items,
            views: schedulerViews,
            activeView: schedulerViews[2],
            eventRecorder: new Y.SchedulerEventRecorder()
            // firstDayOfWeek: 1,
            // activeView: weekView,
            // views: [dayView, weekView, monthView, agendaView]
        }).render();

    });
    </script>

 <script>
    YUI({ filter: 'raw' }).use('aui-scheduler', function(Y) {

        var items = [
            {
                content: 'We Read a book and I played with your childrens'
            },
           
            {
                content: 'Review or (even better) Rewrite Your Goals',
                startDate: new Date(2013, 1, 4, 12),
                endDate: new Date(2016, 1, 4, 16)
            },
            {
                content: 'Event 1',
                startDate: new Date(2014, 6, 4),
                endDate: new Date(2014, 6, 20),
                allDay: true
            },
            {
                content: 'Event 2',
                startDate: new Date(2015, 6, 4),
                endDate: new Date(2015, 6, 20),
                allDay: true
            },
            {
                content: 'Event 3',
                startDate: new Date(2014, 6, 4),
                endDate: new Date(2014, 6, 20),
                allDay: true
            },
            {
                content: 'Other event',
                startDate: new Date(2014, 6, 20),
                endDate: new Date(2014, 6, 22),
                allDay: true
            },
            {
                content: 'Other event 2',
                startDate: new Date(2014, 6, 23),
                endDate: new Date(2014, 6, 28),
                allDay: true
            }
        ];

        var schedulerViews = [
        
            new Y.SchedulerDayView(),
      
            
        ];

        new Y.Scheduler({
            boundingBox: '#scheduler2',
            items: items,
            views: schedulerViews,
            activeView: schedulerViews[2],
            eventRecorder: new Y.SchedulerEventRecorder()
            // firstDayOfWeek: 1,
            // activeView: weekView,
            // views: [dayView, weekView, monthView, agendaView]
        }).render();

    });
    </script>


 <script>
    YUI({ filter: 'raw' }).use('aui-scheduler', function(Y) {

        var items = [
            {
                content: 'I take them to the park '
            },
           
            {
                content: 'Review or (even better) Rewrite Your Goals',
                startDate: new Date(2013, 1, 4, 12),
                endDate: new Date(2016, 1, 4, 16)
            },
            {
                content: 'Event 1',
                startDate: new Date(2014, 6, 4),
                endDate: new Date(2014, 6, 20),
                allDay: true
            },
            {
                content: 'Event 2',
                startDate: new Date(2015, 6, 4),
                endDate: new Date(2015, 6, 20),
                allDay: true
            },
            {
                content: 'Event 3',
                startDate: new Date(2014, 6, 4),
                endDate: new Date(2014, 6, 20),
                allDay: true
            },
            {
                content: 'Other event',
                startDate: new Date(2014, 6, 20),
                endDate: new Date(2014, 6, 22),
                allDay: true
            },
            {
                content: 'Other event 2',
                startDate: new Date(2014, 6, 23),
                endDate: new Date(2014, 6, 28),
                allDay: true
            }
        ];

        var schedulerViews = [
        
            new Y.SchedulerDayView(),
      
            
        ];

        new Y.Scheduler({
            boundingBox: '#scheduler3',
            items: items,
            views: schedulerViews,
            activeView: schedulerViews[2],
            eventRecorder: new Y.SchedulerEventRecorder()
            // firstDayOfWeek: 1,
            // activeView: weekView,
            // views: [dayView, weekView, monthView, agendaView]
        }).render();

    });
    </script>

    <script type="text/javascript">
        $(document).ready(function(){

            $("#scheduler2").hide();
            $("#scheduler3").hide();
            $('#work').on('change', function() {
                     
                     var work= $(this).val();
                         if(work=="0")
                     {
                        $("#sche1").show();
                        $("#scheduler2").hide();
                        $("#scheduler3").hide();
                     }  

                          if(work=="1")
                     {
                        $("#sche1").hide();
                        $("#scheduler2").show();
                        $("#scheduler3").hide();

                     }  
                          if(work=="2")
                     {
                        $("#sche1").hide();
                        $("#scheduler2").hide();
                        $("#scheduler3").show();
                     }  
  

            });


        });
    </script>

@endsection