@extends('app')

@section('content')

    <div id="content-wrap" class="content-wrap">
        <div class="container">
            <div class="content">
                <div class="caregiver-profile" id="employee-profile">
                    <div class="col-md-12 search_blok">
                        <div class="col-md-3">
                            <div class="details">
                                <h4>My Proposals</h4>
                                <a href="#">Active</a> | <a href="#">Archived</a>


                            </div>
                        </div>


                    </div>


                    <div class="col-md-12">
                        <div class="heading">
                            <h5>Active Candidacies(1)</h5>
                            <hr>
                        </div>
                        <table class="table table-hover contracts">
                            <thead>
                              <tr>
                                <th>Job</th>
                                <th>Client</th>
                                <th>Received</th>
                              </tr>
                            </thead>
                            
                            <tbody>
                              
                              <tr class="">
                                <td>One week job </td>
                                <td>Ann Rogers</td>
                                <td>7:47 AM <span> 13 minutes ago</span> </td>
                                
                                
                              </tr>
                              
                             

                              
                            </tbody>
                        </table>

                       

                    </div>

                    <div class="col-md-12">
                        <div class="heading">
                            <h5>Invitations to Interview(1)</h5>
                            <hr>
                        </div>

                        <table class="table table-hover contracts">
                            <thead>
                              <tr>
                                <th>Job</th>
                                <th>Client</th>
                                <th>Received</th>
                              </tr>
                            </thead>
                            
                            <tbody>
                              
                              <tr class="">
                                <td>One week job </td>
                                <td>Ann Rogers </td>
                                <td>7:47 AM <span> 13 minutes ago</span> </td>
                                
                                
                              </tr>
                              
                             

                              
                            </tbody>
                        </table>

                    </div>
                    <div class="col-md-12">
                        <div class="heading">
                            <h5>Submitted Proposals(1)</h5>
                            <hr>
                        </div>

                        <table class="table table-hover contracts">
                            <thead>
                              <tr>
                                <th>Job</th>
                                <th>Client</th>
                                <th>Received</th>
                              </tr>
                            </thead>
                            
                            <tbody>
                              
                              <tr class="">
                                <td>One week job </td>
                                <td>Ann Rogers</td>
                                <td>7:47 AM <span> 13 minutes ago</span> </td>
                                
                                
                              </tr>
                              
                             

                              
                            </tbody>
                        </table>


                    </div>



                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">






        $( document ).ready(function() {

            $(".close_info_success").on("click", function () {

                $(".close_info_success").parent().remove();


            }); });
    </script>
@endsection
