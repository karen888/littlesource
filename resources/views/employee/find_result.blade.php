<div v-if="usersIsEmpty" class="col-md-12">
    <div class="panel job-postings">
        <div class="panel-row empty">
            <h5> There is no results matching your search criteria. </h5>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <ul class="pagination">
            <li :class="current_page == page ? 'active' : ''" v-for="page in pages">
                <a href="javascript:;" @click="setUsersPage(page)" >@{{ page }}</a>
            </li>
        </ul>
        <div class="panel job-postings">

            <div class="panel-row" v-for="user in users">
                <div class="values">
                    <div class="caregiver">
                        <img class="img_50" :src="user.photo" alt="">
                        <strong>@{{ user.name }}</strong>
                    </div>
                    <div class="rates">
                        $@{{ user.rate }}/hour
                    </div>
                    <div class="actions">
                        <div class="actions-group">
                            <div class="trigger">
                                <div class="text_outer"><div class="text trn"> Actions </div></div>
                            </div>
                            <div class="list">
                                <div class="option"
                                     data-action="view-profile"
                                     class="trn">
                                    <a :href="user.view">View Profile</a>
                                </div>
                                <div class="option"
                                     data-action="send-message"
                                     class="trn">
                                    <a :href="user.invite">Invite to job</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
