@extends('employee.caregivers-template')

@section('caregivers_content')

@if($recently->isEmpty())
    <div class="panel margin-top-60">
        <div class="panel-row empty">
            <h5>You have no Recently Viewed caregivers yet.</h5>
        </div>
    </div>
@else
    <div class="panel margin-top-60">
        @for($i = 0; $i < 3; $i++)
        <div class="panel-row caregivers-row">
            <img src="http://lorempixel.com/50/50" alt="">
            <div class="name"><strong>Stella Lawrence</strong></div>
            <div class="actions">
                <div class="actions-group">
                    <div class="trigger">
                        <div class="text_outer"><div class="text trn"> Actions </div></div>
                    </div>
                    <div class="list">
                        <div class="option"
                        data-action="send-message"
                        class="trn">
                            Send Message
                        </div>
                        <div class="option"
                        data-action="view-profile"
                        class="trn">
                            View Profile
                        </div>
                        <!-- <div class="option"
                        data-action="send-message"
                        class="trn">
                            View Contract
                        </div>
                        <div class="option"
                        data-action="send-message"
                        class="trn">
                            End Contract
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        @endfor
    </div>
@endif

@endsection
