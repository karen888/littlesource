@extends('app')

@section('content')
    <div class="container">
        <div class="content">
            <div class="caregiver-profile" id="employee-profile">
                <div class="col-md-9" v-cloak>
                    <div class="caregiver-info">
                        <img src="{{ $user->photo_url }}" alt="">
                        <div class="details">
                            <h4><span v-html="origin.first_name"></span>
                                <span v-html="origin.last_name_view"></span>{{--<small>(28 years old, Adelaide)</small>--}}
                                {{--<a href="#" class="set-edit" v-show="editMode" data-toggle="modal" data-target="#name-title-modal">Edit</a>--}}
                                <a href="/user/settings/get-verified/id" class="set-edit" v-show="editMode">Edit</a>
                            </h4>
                            <div class="description">
                                <span v-html="origin.title"></span>
                            </div>
                            <div class="rates pull-right profilerates">
                               <span v-if="parseInt(origin.min_rate) > 0">
                                   <h4>$</h4>
                                   <h4 v-html="origin.min_rate">$</h4>
                                    <h5>/hr</h5>
                                   <h4> - </h4>
                                </span>
                                <h4>$</h4>
                                <h4 v-html="origin.max_rate">$</h4>
                                <h5>/hr</h5>
                                <a href="#" style="position: relative; top:5px;" id="hrateEditBtn" class="set-edit" v-show="editMode" data-toggle="modal" data-target="#hourly-rate-modal">Edit</a>
                            </div>

                            <div class="skills">
                                <div class="skill" v-if="origin.skills.length" v-for="(key, skill) in origin.skills" v-html="skill"></div>
                                <a href="#" class="set-edit" v-show="editMode" data-toggle="modal" data-target="#skills-modal">Edit Skills</a>
                            </div>
                        </div>


                    </div>
                    <div class="overview">
                        <h5>Overview <a href="#" class="set-edit" v-show="editMode" id="overviewEditBtn" data-toggle="modal" data-target="#overview-modal">Edit</a></h5>
                        <hr>
                        <p v-html="origin.overview" style="white-space: pre-line;">
                        </p>
                    </div>
                    <div class="work-history">

                        <div class="heading">
                            <h5>Work History and Feedback (@{{ job_history.length }})</h5>
                        </div>
                        <div class="history-filter">
                            {{--<div class="pull-right">--}}
                            {{--@include('helpers.customselect', [--}}
                            {{--'select' => [--}}
                            {{--'name' => 'previous-jobs',--}}
                            {{--'options' => collect([['value' => '1', 'name' => 'Newest first', 'is_selected' => true], ['value' => '2', 'name' => 'Oldest First', 'is_selected' => false] ])--}}
                            {{--]--}}
                            {{--])--}}
                            {{--</div>--}}
                        </div>
                        <div v-if="job_history.length" v-show="feedback.show" v-for="(key, feedback) in job_history">
                            <div class="col-md-12">
                                <hr>
                            </div>
                            <div class="feedback-row">
                                <div class="job">
                                    <div class="job-name"><a href="javascript:;" @click="openFeedback(key)">@{{ feedback.title }}</a></div>
                                    <div class="job-rating" v-if="feedback.feedback.rating">
                                        <star-rating :view_type="true" :value="feedback.feedback.rating"></star-rating>
                                    </div>
                                </div>
                                <div class="feedback">
                                    <div class="description" v-if="feedback.feedback.text.length">
                                        “@{{ feedback.feedback.text }}”
                                    </div>
                                    <div class="description inactive" v-else>
                                        No feedback given
                                    </div>
                                    <div class="rates">
                                        <div class="rate">
                                            $@{{ feedback.offer_paid }}/hr
                                        </div>
                                        <div class="timespan">
                                            @{{ feedback.start_date }} @{{ feedback.end_date ? '- ' + feedback.end_date : '' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <a href="javascript:;" class="view-more" v-if="load_more" @click="showFeedbacks()">View More (5)</a>
                        </div>
                    </div>

<!-- myform-->
                    <div class="certificates">
                        <h5>Certificates
                            {{--<a href="#" class="set-edit" v-show="editMode" data-toggle="modal" data-target="#certificates-modal">Edit</a>--}}
                            <a href="/user/settings/get-verified/certificates" class="set-edit" v-show="editMode">Edit</a>
                        </h5>
                        <hr>
                        @foreach(Auth::user()->certificates as $certificate)
                        <div class="institution">
                            <div class="institution-name"><strong>{{$certificate->name}}</strong>

                            @if($certificate->status == 1) <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span> @endif
                            @if($certificate->status == 0) <span style="color: #97979d;"> <i class="fa fa-refresh"></i>&nbsp;verification pending </span> @endif

                            </div>
                            <div class="institution-provider">{{$certificate->provider}}</div>
                            <div class="institution-description">{{$certificate->description}}</div>

                        </div>
                        @endforeach
                    </div>
<!-- endmyform-->

                    <div class="education">
                        <h5>Education
                            {{--<a href="#" class="set-edit" v-show="editMode" data-toggle="modal" data-target="#education-modal" @click="setAutocompl()">Edit</a>--}}
                            <a href="/user/settings/get-verified/qualifications" class="set-edit" v-show="editMode" @click="setAutocompl()">Edit</a>
                        </h5>
                        <hr>
                        <div v-if="origin.educations.length" v-for="(key, education) in origin.educations" class="institution">
                            <div class="institution-name"><strong v-html="education.school.text"></strong>
                                <span style="color: #22b92c;" v-if="education.status == 1"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span>
                                <span style="color: #97979d;" v-if="education.status == 0"> <i class="fa fa-refresh"></i>&nbsp;verification pending </span>
                            </div>
                            <div class="institution-degree" v-html="education.degree.text"></div>
                            <div v-if="education.area_study.text" class="institution-degree" v-html="education.area_study.text"></div>
                            <div class="institution-graduate-year" >
                                <span v-html="education.dates_attended_from.selected"></span>-
                                <span v-html="education.dates_attended_to.selected"></span></div>
                            <div v-text="education.desc.text"></div>
                        </div>
                        <div class="languages" v-if="origin.languages.length">
                            <strong>Languages</strong>
                            <div v-for="(key, language) in origin.languages" class="language" v-html="language.name"></div>
                        </div>
                    </div>
                    <div class="qualifications">
                        <h5>Qualifications <a href="#" id="qualificationsEditBtn" class="set-edit" v-show="editMode" data-toggle="modal" data-target="#qualifications-modal">Edit</a></h5>
                        <hr>
                        <ul>
                            <li v-if="origin.qualifications.smoker">
                                Non-smoker
                                <div class="value" v-if="selects">
                                    <div v-for="smoker in selects.smoker.items" v-if="smoker.value == origin.qualifications.smoker">
                                        <div :class="[smoker.text == 'No' ? 'icon-checkmark' : 'icon-delete']"></div>
                                    </div>
                                </div>
                            </li>
                            <li v-if="origin.qualifications.sick_children">
                                Willing to care for sick children
                                <div class="value" v-if="selects">
                                    <div v-for="care_sick in selects.care_sick.items" v-if="care_sick.value == origin.qualifications.sick_children">
                                        <div :class="[care_sick.text == 'Yes' ? 'icon-checkmark' : 'icon-delete']"></div>
                                    </div>
                                </div>
                            </li>
                            <li v-if="origin.qualifications.sick_children">
                                Comfortable with pets
                                <div class="value" v-if="selects">
                                    <div v-for="pets in selects.pets.items" v-if="pets.value == origin.qualifications.comfortable_with_pets">
                                        <div :class="[pets.text == 'Yes' ? 'icon-checkmark' : 'icon-delete']"></div>
                                    </div>
                                </div>
                            </li>
                            <li v-if="origin.qualifications.have_transportation">
                                Drivers licence
                                <div class="value" v-for="item in selects.drivers_licence.items"
                                     v-if="origin.qualifications.have_transportation == item.value">
                                    @{{ item.text }}
                                </div>
                            </li>
                            <li v-if="origin.qualifications.willing_travel">
                                Willing to travel
                                <div class="value">
                                    <div v-for="location_range in selects.location_range.items" v-if="location_range.value == origin.qualifications.willing_travel">
                                        Up to @{{ location_range.text }}
                                    </div>
                                </div>
                            </li>
                            <li v-for="(index, care_type) in origin.qualifications.care_type">
                                <span v-if="index == 0">Care Type</span>
                                <div class="value" v-for="item in selects.care_type.items" v-if="care_type == item.value">
                                    @{{ item.text }}
                                </div>
                            </li>
                            <li v-if="origin.qualifications.own_children">
                                Own children
                                <div class="value" v-if="selects">
                                    <div v-for="own_children in selects.own_children.items" v-if="own_children.value == origin.qualifications.own_children">
                                        <div :class="[own_children.text == 'Yes' ? 'icon-checkmark' : 'icon-delete']"></div>
                                    </div>
                                </div>
                            </li>

                            <li v-for="(index, child_age) in origin.qualifications.child_age">
                                <span v-if="index == 0">Child Age Range</span>
                                <div class="value" v-for="item in selects.child_age.items" v-if="child_age == item.value">
                                    @{{ item.text }}
                                </div>
                            </li>
                            {{--<li v-for="(index, qualification) in origin.qualifications.qualification" v-if="selects">--}}
                                {{--<span v-if="index == 0">Qualifications</span>--}}
                                {{--<div class="value" v-for="item in selects.qualification.items" v-if="qualification == item.value">--}}
                                    {{--@{{ item.text }}--}}
                                {{--</div>--}}
                            {{--</li>--}}
                        </ul>
                    </div>
                </div>
                <!-- Caregiver Options -->
                <div class="col-md-3" v-cloak>
                    <div class="caregiver-edit">
                        <a href="#" id="caregiverEditButton" class="btn  btn-themed btn-caregiver-option" @click.prevent="setEdit()">
                            <span v-html="editButton"></span>
                        </a>
                    </div>

                    <div class="caregiver-work-history">
                        <div class="userRate">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{ isset($user_progress['rate']) ? $user_progress['rate'] : "-" }}"
                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ isset($user_progress['rate']) ? $user_progress['rate'] : 0 }}%">
                                        {{ isset($user_progress['rate']) ? $user_progress['rate'] : 0 }}% Complete
                                </div>
                            </div>
                            {{--<div class="list-group">--}}
                            {{--@foreach($user_progress['links'] as $link)--}}
                                {{--<a href="{{ $link['link'] }}" class="progress-addbtn list-group-item list-group-item-action profile-add-thing-btn">+ Add {{ $link['name'] }} (+{{ $link['rate'] }}%)</a>--}}
                            {{--@endforeach--}}
                            {{--</div>--}}
                        </div>
                        <strong>Work history</strong>
                        <div class="success" v-if="employee_rating">
                            <div class="job-rating" >
                                <star-rating :view_type="true" :value="employee_rating"></star-rating>
                            </div>
                        </div>
                        <div class="success" v-else>
                            <div class="job-rating">
                                <div class="icon-star-inactive"></div>
                                <div class="icon-star-inactive"></div>
                                <div class="icon-star-inactive"></div>
                                <div class="icon-star-inactive"></div>
                                <div class="icon-star-inactive"></div>
                            </div>
                        </div>
                        <div class="experience">
                            <strong>Experience <a href="#" class="set-edit" v-show="editMode" data-toggle="modal" data-target="#experience-modal">Edit</a></strong>

                            <div class="value" v-for="item in selects.years_experience.items" v-if="selects">
                                <span v-if="item.value == origin.experience" v-html="item.text"></span>
                            </div>
                        </div>
                        <div class="availability" v-if="selects">
                            <strong>Availability <a href="#" class="set-edit" v-show="editMode" id="availableEditBtn" data-toggle="modal" data-target="#availability-modal">Edit</a></strong>

                            <div class="value" v-if="!origin.available.value">Not Available</div>
                            <div v-else>
                                <div class="value" v-for="employment_type in selects.employment_type.items" v-if="origin.available.selected == employment_type.value">
                                    @{{ employment_type.text }}
                                    <span v-if="getAvailabilityDays && selects">
                                        (@{{ getAvailabilityDays }})
                                    </span>
                                </div>
                            </div>
                        </div>
                        {{--<div class="payment-options">
                            <strong>Payment Options</strong>
                            <div class="value">Cash</div>
                        </div>--}}
                    </div>
                    @if(!$user->wwcc_status&&
                          !$user->phone_verifed &&
                          !$user->verifed_id&&
                          !$user->qua_status&&
                          !$user->address_status&&
                          !$user->cert_status&&
                          !$user->abn_verifed&&
                          !$user->phonsoc_statuse_verifed
                          ) @else
                    <div class="caregiver-work-history title" style="margin-top: 20px;">
                        <h6><strong>Verified info</strong></h6>
                    </div>
                    <div class="caregiver-work-history after-title">
                            @if($user->wwcc_status == 1) <p>WWCC <i class="fa fa-check-circle-o " aria-hidden="true" style="font-size: 18px; float: right;"></i></p> @endif
                            @if($user->phone_verifed == 1) <p>Phone <i class="fa fa-check-circle-o " aria-hidden="true" style="font-size: 18px; float: right;"></i></p> @endif
                            @if($user->verifed_id == 1) <p>ID <i class="fa fa-check-circle-o " aria-hidden="true" style="font-size: 18px; float: right;"></i></p> @endif
                            @if($user->qua_status == 1) <p>Qualifications <i class="fa fa-check-circle-o " aria-hidden="true" style="font-size: 18px; float: right;"></i></p> @endif
                            @if($user->address_status == 1) <p>Address <i class="fa fa-check-circle-o " aria-hidden="true" style="font-size: 18px; float: right;"></i></p> @endif
                            @if($user->cert_status == 1) <p>Certifications <i class="fa fa-check-circle-o " aria-hidden="true" style="font-size: 18px; float: right;"></i></p> @endif
                            @if($user->abn_verifed == 1) <p>ABN <i class="fa fa-check-circle-o " aria-hidden="true" style="font-size: 18px; float: right;"></i></p> @endif
                            @if($user->soc_status == 1) <p>Social network profile <i class="fa fa-check-circle-o " aria-hidden="true" style="font-size: 18px; float: right;"></i></p> @endif
                    </div>
                        @endif
                </div>
                @include('employee.profile.modals.availability')
                {{--@include('employee.profile.modals.certificates')--}}
                {{--@include('employee.profile.modals.education')--}}
                {{-- @include('employee.profile.modals.name-title') --}}
                @include('employee.profile.modals.overview')
                @include('employee.profile.modals.qualifications')
                @include('employee.profile.modals.hourly-rate')
                @include('employee.profile.modals.skills')
                @include('employee.profile.modals.experience')
                @include('employee.profile.modals.view-feedback')
            </div>
        </div>
    </div>

@if(request('edit'))
    <script>
        /**
         * Fire an event handler to the specified node. Event handlers can detect that the event was fired programatically
         * by testing for a 'synthetic=true' property on the event object
         * @param {HTMLNode} node The node to fire the event handler on.
         * @param {String} eventName The name of the event without the "on" (e.g., "focus")
         */
        function fireEvent(node, eventName) {
            // Make sure we use the ownerDocument from the provided node to avoid cross-window problems
            var doc;
            if (node.ownerDocument) {
                doc = node.ownerDocument;
            } else if (node.nodeType == 9){
                // the node may be the document itself, nodeType 9 = DOCUMENT_NODE
                doc = node;
            } else {
                throw new Error("Invalid node passed to fireEvent: " + node.id);
            }

            if (node.dispatchEvent) {
                // Gecko-style approach (now the standard) takes more work
                var eventClass = "";

                // Different events have different event classes.
                // If this switch statement can't map an eventName to an eventClass,
                // the event firing is going to fail.
                switch (eventName) {
                    case "click": // Dispatching of 'click' appears to not work correctly in Safari. Use 'mousedown' or 'mouseup' instead.
                    case "mousedown":
                    case "mouseup":
                        eventClass = "MouseEvents";
                        break;

                    case "focus":
                    case "change":
                    case "blur":
                    case "select":
                        eventClass = "HTMLEvents";
                        break;

                    default:
                        throw "fireEvent: Couldn't find an event class for event '" + eventName + "'.";
                        break;
                }
                var event = doc.createEvent(eventClass);
                event.initEvent(eventName, true, true); // All events created as bubbling and cancelable.

                event.synthetic = true; // allow detection of synthetic events
                // The second parameter says go ahead with the default action
                node.dispatchEvent(event, true);
            } else  if (node.fireEvent) {
                // IE-old school style
                var event = doc.createEventObject();
                event.synthetic = true; // allow detection of synthetic events
                node.fireEvent("on" + eventName, event);
            }
        };
        $(document).ready(function(){
            $('body').LoadingOverlay("show", {});

            setTimeout(function(){
                fireEvent(document.getElementById('caregiverEditButton'), 'click');
                setTimeout(function(){
                    fireEvent(document.getElementById('{{request('edit')}}EditBtn'), 'click');
                    $('.modal').on('shown.bs.modal', function() {
                        $('body').LoadingOverlay("hide", {});
                        $("#overview_text").focus();

                    });

                }, 300)

            },300)
        });
    </script>
@endif
@endsection