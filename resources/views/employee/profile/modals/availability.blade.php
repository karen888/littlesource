<div class="modal fade" id="availability-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Change Availability</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <strong>I am currently</strong>
                    </div>
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range">
                        <ul class="list-inline">
                            <li :class="[available.value ? 'active' : '']" @click="setAvailable(true)">AVAILABLE</li>
                            <li :class="[!available.value ? 'active' : '']" @click="setAvailable(false)">NOT&nbsp;AVAILABLE</li>
                        </ul>
                    </div>
                </div>
                <div class="row form-row" v-show="available.value" v-if="selects" :class="[available.errors.message ? 'error' : '' ]">
                    <div class="col-md-12" v-for="employment in selects.employment_type.items">
                        <div class="full-width">
                            <div class="radio">

                                    <input type="radio" name="available" id="available_radio_@{{ employment.value }}" v-model="available.selected" value="@{{ employment.value }}">
                                    <label for="available_radio_@{{ employment.value }}"><span></span> @{{ employment.text }}</label>

                            </div>
                        </div>
                    </div>
                    <div class="error-message" v-if="available.errors.message"> @{{ available.errors.message }} </div>
                </div>
                <div class="row form-row" :class="[availability_days.error ? 'error' : '' ]">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--availability" v-show="available.value" v-if="selects">
                        <label for="#">Availability Days:</label>
                        <custom-select-ext class="availability" name="availability_days" id="availability_days" v-if="selects" msg-empty="Select Availability Days"
                                           :options="selects.availability.items" :multiple="true" :status-icon="true" :selected.sync="availability_days.selected" msg-all="All days">
                        </custom-select-ext>
                        <div class="error-message" v-if="availability_days.error"> @{{ availability_days.error }} </div>
                    </div>
                </div>

                <div class="row form-row" v-show="!available.value">
                    <div class="col-md-12 no_padding">
                        <strong>When do you expect to be ready for new work? (optional)</strong>
                    </div>
                    <div class="col-md-12" :class="[expect_ready.errors.message ? 'error' : '' ]">
                        <input type="text" id="start-date" size="10" v-model="expect_ready.text" @keyup="expect_ready.errors.message = ''" />
                        <div class="error-message" v-if="expect_ready.errors.message"> @{{ expect_ready.errors.message }} </div>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('availability')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal" @click="cancelEdit('availability')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
