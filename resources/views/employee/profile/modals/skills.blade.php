<div class="modal fade" id="skills-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Edit My Skills</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <strong>Enter skills</strong>
                    </div>
                    <div class="col-md-12 full-width" :class="[skills.errors.message ? 'error' : '' ]">
                        <input type="text" id="skills" @keyup="skills.errors.message = ''" />
                        <input type="hidden" id="tags_input" v-model="skills.text" />
                        <div class="error-message" v-if="skills.errors.message"> @{{ skills.errors.message }} </div>
                    </div>
                    <div class="col-md-12 no_padding">
                        <small>Add up to 10 skills. Reorder your skills by dragging tags to a new position.</small>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('skills')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal" @click="cancelEdit('skills')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
