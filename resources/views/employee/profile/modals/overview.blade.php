<div class="modal fade" id="overview-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Overview</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <p>
                            Use this space to show clients you have the skills and experience they're looking for.
                        </p>
                        <ul>
                            <li>
                                Describe your strengths and skills
                            </li>
                            <li>
                                Highlight previous experience and education
                            </li>
                            <li>
                                Keep it short and check for typos!
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-12">
                        <div class="full-width" :class="[overview.errors.message ? 'error' : '' ]">
                            <textarea id="overview_text" name="overview" rows="5" v-model="overview.text" @keyup="overview.errors.message = ''"></textarea>
                            <div class="error-message" v-if="overview.errors.message"> @{{ overview.errors.message }} </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('overview')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal" @click="cancelEdit('overview')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
