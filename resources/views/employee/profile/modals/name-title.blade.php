<div class="modal fade" id="name-title-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Edit Name & Title</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-3 file no_padding caregiver-info">
                        <div class="profile-picture">
                            <img :src="picture.src" alt="">
                        </div>
                    </div>
                    <div class="col-md-9 file">
                        <div class="file-select" transition="ease">
                            {!! Form::open([ 'method' => 'POST', 'files' => true]) !!}
                                <div class="file-select__choose-file">
                                    <input type="file" name="profile_picture" id="file" v-model="profile_picture" class="inputfile" accept="image/*"  @change="bindFile" v-el:profilepic />
                                    <label for="file">Choose a file</label>
                                    <div class="filename">
                                        @{{ filePath }}
                                    </div>
                                    <!--<div class="progress">
                                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>-->
                                </div>
                                <div v-show="picture != '/img/no_photo_100.jpg' && picture.length" class="file-select__delete">
                                    <input v-model="delete" type="checkbox" name="delete" id="delete">
                                    <label for="delete">Delete photo</label>
                                </div>
                            {!! Form::close() !!}
                        </div>  
                    </div>                  
                </div>            
                <div class="row form-row">
                    <div class="col-md-3 no_padding">
                        <strong>First Name</strong>
                    </div>
                    <div class="col-md-9" :class="[first_name.errors.message ? 'error' : '' ]">
                        <input type="text" v-model="first_name.text" @keyup="first_name.errors.message = ''" size="32" />
                        <div class="error-message" v-if="first_name.errors.message"> @{{ first_name.errors.message }} </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-3 no_padding">
                        <strong>Last Name</strong>
                    </div>
                    <div class="col-md-9" :class="[last_name.errors.message ? 'error' : '' ]">
                        <input type="text" v-model="last_name.text" @keyup="last_name.errors.message = ''" size="32" />
                        <div class="error-message" v-if="last_name.errors.message"> @{{ last_name.errors.message }} </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <strong>Display Name</strong>
                    </div>
                    <div class="col-md-12">
                        <div class="radio">
                            <label>
                                <input type="radio" name="display_name" v-model="display_name" value="1">
                                <span>Show my full name (e.g. Amy White)</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="radio">
                            <label>
                                <input type="radio" name="display_name" v-model="display_name" value="0">
                                <span>Show only my first name and last initial (e.g. Amy W.)</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[gender.errors.message ? 'error' : '' ]">
                        <label for="#">Gender</label>
                        <span>
                            <custom-select-ext class="location_range" name="location_range" id="child-age" v-if="selects" :as-list="true"
                                               :options="selects.gender.items" :selected.sync="gender.text" :msg-all="All">
                            </custom-select-ext>
                            <div class="error-message" v-if="gender.errors.message"> @{{ gender.errors.message }} </div>
                        </span>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-3 no_padding">
                        <strong>Birthday</strong>
                    </div>
                    <div class="col-md-9" :class="[birthday.errors.message ? 'error' : '' ]">
                        <input type="text" id="birthday" v-model="birthday.text" @keyup="birthday.errors.message = ''" size="9" />
                        <div class="error-message" v-if="birthday.errors.message"> @{{ birthday.errors.message }} </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-3 no_padding">
                        <strong>Title (optional)</strong>
                    </div>
                    <div class="col-md-9" :class="[title.errors.message ? 'error' : '' ]">
                        <input type="text" v-model="title.text" @keyup="title.errors.message = ''" size="32" />
                        <div class="error-message" v-if="title.errors.message"> @{{ title.errors.message }} </div>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('name-title')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal" @click="cancelEdit('name-title')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
