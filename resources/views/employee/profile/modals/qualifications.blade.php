<div class="modal fade" id="qualifications-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Edit Qualifications</h4>
            </div>
            <div class="modal-body" style="padding-top: 0;">
                {{--<div class="FindCaregivers__filter FindCaregivers__filter--qualifications" :class="[qualifications_errors.qualifications ? 'error' : '' ]">--}}
                    {{--<div class="label-wraper">--}}
                        {{--<label for="#">Qualifications:</label>--}}
                    {{--</div>--}}
                    {{--<span id="selector_qualification">--}}
                        {{--<custom-select-ext-qualifications class="qualification" name="qualification" id="qualification" :as-list="true" :multiple-list="true"--}}
                                       {{--:options="selects.qualification.items" :multiple="qualifications.qualification.multiple" :selected.sync="qualifications.qualification.selected" :required="false"--}}
                        {{--@changed-option="qualifications_errors.qualifications = ''">--}}

                        {{--</custom-select-ext-qualifications>--}}
                        {{--<div class="error-message" v-if="qualifications_errors.qualifications"> @{{ qualifications_errors.qualifications }} </div>--}}
                    {{--</span>--}}
                {{--</div>--}}
                <hr>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[qualifications_errors.have_transportation ? 'error' : '' ]">
                        <div class="label-wraper">
                            <label for="#">Drivers licence:</label>
                        </div>
                        <span>
                            <custom-select-ext class="drivers-licence" name="drivers-licence" id="selector_car_type" v-if="selects"
                                               :options="selects.drivers_licence.items" :multiple="false" :status-icon="true" :selected.sync="qualifications.have_transportation"
                            @changed-option="qualifications_errors.have_transportation = ''">

                            </custom-select-ext>
                            <div class="error-message" v-if="qualifications_errors.have_transportation"> @{{ qualifications_errors.have_transportation }} </div>
                        </span>
                    </div>
                </div>
                <hr>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[qualifications_errors.care_type ? 'error' : '' ]">
                        <div class="label-wraper">
                            <label for="#">Care Type:</label>
                        </div>
                        <span>
                            <custom-select-ext class="care-type" name="care-type" id="selector_care_type" v-if="selects" msg-empty="Select Care Type"
                                               :options="selects.care_type.items" :multiple="qualifications.care_type.multiple" :status-icon="true" :selected.sync="qualifications.care_type.selected"
                            @changed-option="qualifications_errors.care_type = ''">

                            </custom-select-ext>
                            <div class="error-message" v-if="qualifications_errors.care_type"> @{{ qualifications_errors.care_type }} </div>
                        </span>
                    </div>
                </div>
                <hr>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[qualifications_errors.child_age ? 'error' : '' ]">
                        <div class="label-wraper">
                            <label for="#">Child Age Range</label>
                        </div>
                        <span>
                            <custom-select-ext class="child-age" name="child-age" id="selector_child_age_range" v-if="selects" msg-empty="Select Child Age Range"
                                               :options="selects.child_age.items" :multiple="true" :status-icon="true" :selected.sync="qualifications.child_age.selected" :msg-all="All"
                            @changed-option="qualifications_errors.child_age = ''">

                            </custom-select-ext>
                            <div class="error-message" v-if="qualifications_errors.child_age"> @{{ qualifications_errors.child_age }} </div>
                        </span>
                    </div>
                </div>
                <hr>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[qualifications_errors.willing_travel ? 'error' : '' ]">
                        <div class="label-wraper">
                            <label for="#">Willing to travel:</label>
                        </div>
                        <span>
                            <custom-select-ext class="location_range" name="location_range" id="selector_willing_to_travel" v-if="selects"
                                               :options="selects.location_range.items" :status-icon="true" :selected.sync="qualifications.willing_travel" :msg-all="All"
                            @changed-option="qualifications_errors.willing_travel = ''">

                            </custom-select-ext>
                            <div class="error-message" v-if="qualifications_errors.willing_travel"> @{{ qualifications_errors.willing_travel }} </div>
                        </span>
                    </div>
                </div>
                <hr>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[qualifications_errors.smoker ? 'error' : '' ]">
                        <div class="label-wraper">
                            <label for="#">Are you smoker?</label>
                        </div>
                        <span id="selector_smoker">
                            <custom-select-ext class="allowed_locations_id" name="care-sick" id="smoker" v-if="selects" :as-list="true"
                                               :options="selects.smoker.items" :multiple="selects.smoker.multiple" :selected.sync="qualifications.smoker"
                            @changed-option="qualifications_errors.smoker = ''">

                            </custom-select-ext>
                            <div class="error-message" v-if="qualifications_errors.smoker"> @{{ qualifications_errors.smoker }} </div>
                        </span>
                    </div>
                </div>
                <hr>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[qualifications_errors.sick_children ? 'error' : '' ]">
                        <div class="label-wraper">
                            <label for="#">Willing to care for sick children?</label>
                        </div>
                        <span id="selector_sick_children">
                            <custom-select-ext class="care_sick" name="care-sick" v-if="selects" :as-list="true"
                                               :options="selects.care_sick.items" :multiple="selects.care_sick.multiple" :selected.sync="qualifications.sick_children"
                            @changed-option="qualifications_errors.sick_children = ''">

                            </custom-select-ext>
                            <div class="error-message" v-if="qualifications_errors.sick_children"> @{{ qualifications_errors.sick_children }} </div>
                        </span>
                    </div>
                </div>
                <hr>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[qualifications_errors.comfortable_with_pets ? 'error' : '' ]">
                        <div class="label-wraper">
                            <label for="#">Are you comfortable with pets?</label>
                        </div>
                        <span id="selector_pets">
                            <custom-select-ext class="pets" name="pets" id="pets" v-if="selects" :as-list="true"
                                               :options="selects.pets.items" :multiple="selects.pets.multiple" :selected.sync="qualifications.comfortable_with_pets"
                            @changed-option="qualifications_errors.comfortable_with_pets = ''">

                            </custom-select-ext>
                            <div class="error-message" v-if="qualifications_errors.comfortable_with_pets"> @{{ qualifications_errors.comfortable_with_pets }} </div>
                        </span>
                    </div>
                </div>
                <hr>
                <div class="row form-row">
                    <div class="col-md-12 FindCaregivers__filter FindCaregivers__filter--range" :class="[qualifications_errors.own_children ? 'error' : '' ]">
                        <div class="label-wraper">
                            <label for="#">Do you have own children?</label>
                        </div>
                        <span id="selector_own_children">
                            <custom-select-ext class="allowed_locations_id" name="own_children"  v-if="selects" :as-list="true"
                                               :options="selects.own_children.items" :multiple="selects.own_children.multiple" :selected.sync="qualifications.own_children"
                            @changed-option="qualifications_errors.own_children = ''"
                            >

                            </custom-select-ext>
                            <div class="error-message" v-if="qualifications_errors.own_children"> @{{ qualifications_errors.own_children }} </div>
                        </span>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('qualifications')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal" @click="cancelEdit('qualifications')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>

@if(request('directEdit'))


    <script>
        /**
         * Fire an event handler to the specified node. Event handlers can detect that the event was fired programatically
         * by testing for a 'synthetic=true' property on the event object
         * @param {HTMLNode} node The node to fire the event handler on.
         * @param {String} eventName The name of the event without the "on" (e.g., "focus")
         */
        function fireEvent(node, eventName) {
            // Make sure we use the ownerDocument from the provided node to avoid cross-window problems
            var doc;
            if (node.ownerDocument) {
                doc = node.ownerDocument;
            } else if (node.nodeType == 9){
                // the node may be the document itself, nodeType 9 = DOCUMENT_NODE
                doc = node;
            } else {
                throw new Error("Invalid node passed to fireEvent: " + node.id);
            }

            if (node.dispatchEvent) {
                // Gecko-style approach (now the standard) takes more work
                var eventClass = "";

                // Different events have different event classes.
                // If this switch statement can't map an eventName to an eventClass,
                // the event firing is going to fail.
                switch (eventName) {
                    case "click": // Dispatching of 'click' appears to not work correctly in Safari. Use 'mousedown' or 'mouseup' instead.
                    case "mousedown":
                    case "mouseup":
                        eventClass = "MouseEvents";
                        break;

                    case "focus":
                    case "change":
                    case "blur":
                    case "select":
                        eventClass = "HTMLEvents";
                        break;

                    default:
                        throw "fireEvent: Couldn't find an event class for event '" + eventName + "'.";
                        break;
                }
                var event = doc.createEvent(eventClass);
                event.initEvent(eventName, true, true); // All events created as bubbling and cancelable.

                event.synthetic = true; // allow detection of synthetic events
                // The second parameter says go ahead with the default action
                node.dispatchEvent(event, true);
            } else  if (node.fireEvent) {
                // IE-old school style
                var event = doc.createEventObject();
                event.synthetic = true; // allow detection of synthetic events
                node.fireEvent("on" + eventName, event);
            }
        };
        $(document).ready(function(){
            $('body').LoadingOverlay("show", {});

           setTimeout(function(){
               fireEvent(document.getElementById('caregiverEditButton'), 'click');
               setTimeout(function(){
                   fireEvent(document.getElementById('qualificationsEditBtn'), 'click');
                   $('#qualifications-modal').on('shown.bs.modal', function() {
                       $('body').LoadingOverlay("hide", {});
                        $('#selector_{{request('directEdit')}}').focus();
                        fireEvent(document.getElementById('selector_{{request('directEdit')}}'), 'click');

                       $('#qualifications-modal').animate({
                           scrollTop: $("#selector_{{request('directEdit')}}").parent().offset().top
                       }, 500);

                       $("#selector_{{request('directEdit')}}").parent().effect("highlight", {}, 5000);

                   });

               }, 300)

           },300)
        });
    </script>
@endif