<div class="modal fade" id="education-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Edit Education</h4>
            </div>
            <div class="modal-body">
                <div v-for="(key, education) in educations.list">
                    <input type="hidden" v-model="education.id" />
                    <div class="row form-row">
                        <div class="col-md-12 no_padding">
                            <strong>School</strong>
                        </div>
                        <div class="col-md-12 full-width" :class="[education.school.errors.message ? 'error' : '' ]">
                            <input type="text" v-model="education.school.text" @keyup="education.school.errors.message = ''" />
                            <div class="error-message" v-if="education.school.errors.message"> @{{ education.school.errors.message }} </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-6 no_padding">
                            <div class="col-md-12 no_padding">
                                <strong>Degree</strong>
                            </div>
                            <div class="col-md-12 full-width" :class="[education.degree.errors.message ? 'error' : '' ]">
                                <input type="text" v-model="education.degree.text" @keyup="education.degree.errors.message = ''" />
                                <div class="error-message" v-if="education.degree.errors.message"> @{{ education.degree.errors.message }} </div>
                            </div>
                        </div>
                        <div class="col-md-6 no_padding">
                            <div class="col-md-12 no_padding">
                                <strong>Area of Study (optional)</strong>
                            </div>
                            <div class="col-md-12 full-width" :class="[education.area_study.errors.message ? 'error' : '' ]">
                                <input type="text" v-model="education.area_study.text" @keyup="education.area_study.errors.message = ''" />
                                <div class="error-message" v-if="education.area_study.errors.message"> @{{ education.area_study.errors.message }} </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-6 no_padding">
                            <div class="col-md-12 no_padding">
                                <strong>Dates Attended</strong>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6 no_padding">
                                    <div class="col-md-11 no_padding full-width" :class="[education.dates_attended_from.errors.message ? 'error' : '' ]">
                                        <custom-select class="choose_job"
                                           :options="{{ json_encode([
                                                ['value' => '1990', 'text' => '1990'],
                                                ['value' => '1991', 'text' => '1991'],
                                                ['value' => '1992', 'text' => '1992'],
                                                ['value' => '1993', 'text' => '1993'],
                                                ['value' => '1994', 'text' => '1994'],
                                                ['value' => '1995', 'text' => '1995'],
                                                ['value' => '1996', 'text' => '1996'],
                                                ['value' => '1997', 'text' => '1997'],
                                                ['value' => '1998', 'text' => '1998'],
                                                ['value' => '1999', 'text' => '1999'],
                                                ['value' => '2000', 'text' => '2000'],
                                                ['value' => '2001', 'text' => '2001'],
                                                ['value' => '2002', 'text' => '2002'],
                                                ['value' => '2003', 'text' => '2003'],
                                                ['value' => '2004', 'text' => '2004'],
                                                ['value' => '2005', 'text' => '2005'],
                                                ['value' => '2006', 'text' => '2006'],
                                                ['value' => '2007', 'text' => '2007'],
                                                ['value' => '2008', 'text' => '2008'],
                                                ['value' => '2009', 'text' => '2009'],
                                                ['value' => '2010', 'text' => '2010'],
                                                ['value' => '2011', 'text' => '2011'],
                                                ['value' => '2012', 'text' => '2012'],
                                                ['value' => '2013', 'text' => '2013'],
                                                ['value' => '2014', 'text' => '2014'],
                                                ['value' => '2015', 'text' => '2015'],
                                                ['value' => '2016', 'text' => '2016'],
                                                ['value' => '2017', 'text' => '2017'],
                                                ['value' => '2018', 'text' => '2018'],
                                                ['value' => '2019', 'text' => '2019'],
                                            ]) }}"
                                            :selected.sync="education.dates_attended_from.selected"
                                            name="dates attended from"
                                            @keyup="education.dates_attended_from.errors.message = ''"
                                        >
                                        </custom-select>
                                        <div class="error-message" v-if="education.dates_attended_from.errors.message"> @{{ education.dates_attended_from.errors.message }} </div>
                                    </div>
                                </div>
                                <div class="col-md-6 no_padding">
                                    <div class="col-md-12 no_padding full-width" :class="[education.dates_attended_to.errors.message ? 'error' : '' ]">
                                        <custom-select class="choose_job"
                                            :options="{{ json_encode([
                                                ['value' => '1990', 'text' => '1990'],
                                                ['value' => '1991', 'text' => '1991'],
                                                ['value' => '1992', 'text' => '1992'],
                                                ['value' => '1993', 'text' => '1993'],
                                                ['value' => '1994', 'text' => '1994'],
                                                ['value' => '1995', 'text' => '1995'],
                                                ['value' => '1996', 'text' => '1996'],
                                                ['value' => '1997', 'text' => '1997'],
                                                ['value' => '1998', 'text' => '1998'],
                                                ['value' => '1999', 'text' => '1999'],
                                                ['value' => '2000', 'text' => '2000'],
                                                ['value' => '2001', 'text' => '2001'],
                                                ['value' => '2002', 'text' => '2002'],
                                                ['value' => '2003', 'text' => '2003'],
                                                ['value' => '2004', 'text' => '2004'],
                                                ['value' => '2005', 'text' => '2005'],
                                                ['value' => '2006', 'text' => '2006'],
                                                ['value' => '2007', 'text' => '2007'],
                                                ['value' => '2008', 'text' => '2008'],
                                                ['value' => '2009', 'text' => '2009'],
                                                ['value' => '2010', 'text' => '2010'],
                                                ['value' => '2011', 'text' => '2011'],
                                                ['value' => '2012', 'text' => '2012'],
                                                ['value' => '2013', 'text' => '2013'],
                                                ['value' => '2014', 'text' => '2014'],
                                                ['value' => '2015', 'text' => '2015'],
                                                ['value' => '2016', 'text' => '2016'],
                                                ['value' => '2017', 'text' => '2017'],
                                                ['value' => '2018', 'text' => '2018'],
                                                ['value' => '2019', 'text' => '2019'],
                                            ]) }}"
                                            :selected.sync="education.dates_attended_to.selected"
                                            name="dates attended to"
                                            @keyup="education.dates_attended_to.errors.message = ''"
                                        >
                                        </custom-select>
                                        <div class="error-message" v-if="education.dates_attended_to.errors.message"> @{{ education.dates_attended_to.errors.message }} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 no_padding">
                            <div class="col-md-12 no_padding">
                                <strong>&nbsp;</strong>
                            </div>
                            <div class="col-md-12 remove_data">
                                <a href="javascript:;" @click="removeEducation(key)">Remove this school</a>
                            </div>
                        </div>
                    </div>
                    <hr>
                </div>
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <div class="col-md-12">
                            <a href="javascript:;" @click="addEducation()">+ Add School</a>
                        </div>
                    </div>
                </div>
                <hr>

                <div v-for="(key, language) in languages.list">
                    <div class="row form-row">
                        <div class="col-md-12">
                            <strong>Language</strong>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-5 no_padding" :class="[language.errors.message ? 'error' : '' ]">
                                <input class="language_text" type="text" v-model="language.name" @keyup="language.errors.message = ''" />
                                <div class="error-message" v-if="language.errors.message"> @{{ language.errors.message }} </div>
                            </div>
                            <div class="col-md-6 remove_data">
                                <a href="javascript:;" @click="removeLanguage(key)">Remove this language</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <div class="col-md-12">
                            <a href="javascript:;" @click="addLanguage()">+ Add Language</a>
                        </div>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('educations')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal"  @click="cancelEdit('educations')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
