<div class="modal fade" id="hourly-rate-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Change Hourly Rate</h4>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-4 no_padding">
                        <div class="col-md-12 no_padding">
                            <strong>Minimum Hourly Rate:</strong>
                        </div>
                        <div class="col-md-12 no_padding comision">
                            <small>You will earn $<label class="com-rate-min">@{{ min_rate.commisons }}</label>/hour</small>
                        </div>                        
                    </div>
                    <div class="col-md-4 no_padding">
                        <div :class="[min_rate.errors.message ? 'error' : '' ]">
                            <label>$ <input type="text" name="min_rate" v-model="min_rate.text" size="7" @keyup="setRateMin()" /> /hr</label>
                            <div class="error-message" v-if="min_rate.errors.message"> @{{ min_rate.errors.message }} </div>

                        </div>
                    </div>

                </div>
                <div class="row form-row">
                    <div class="col-md-4 no_padding">
                        <div class="col-md-12 no_padding">
                            <strong>Maximum Hourly Rate:</strong>
                        </div>
                        <div class="col-md-12 no_padding comision">
                            <small>You will earn $<label class="com-rate-max"> @{{ max_rate.commisons }}</label>/hour</small>
                        </div>                          
                    </div>
                    <div class="col-md-4 no_padding">
                        <div :class="[max_rate.errors.message ? 'error' : '' ]">
                            <label>$ <input type="text" name="max_rate" v-model="max_rate.text" size="7" @keyup="setRateMax()" /> /hr</label>
                            <div class="error-message" v-if="max_rate.errors.message"> @{{ max_rate.errors.message }} </div>
                        </div>
                    </div>                  
                </div>
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <small>Enter maximum hourly rate if your rate is flexible.</small>
                    </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('hourly-rate')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal" @click="cancelEdit('hourly-rate')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>

