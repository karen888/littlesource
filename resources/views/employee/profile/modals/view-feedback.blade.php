<div class="modal fade" id="view-feedback-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="appl-send-message-label">Job Feedback</h5>
            </div>
            <div class="modal-body">
                <div class="row form-row">
                    <div class="col-md-12 no_padding">
                        <h4>@{{ view_feedback.title }}</h4>
                    </div>
                    <div class="col-md-12 no_padding">
                        <strong>$@{{ view_feedback.offer_paid }}/hr</strong>, @{{ view_feedback.start_date }} - @{{ view_feedback.end_date }}
                    </div>
                </div>
            </div>
            <div class="modal-body" v-for="rating in view_feedback.feedback.feedbacks">
                <div class="row form-row" >
                    <div class="col-md-12 no_padding" v-if="rating.by_employee == false">
                        <h6><strong>Client's Feedback to Caregiver</strong></h6>
                    </div>
                    <div class="col-md-12 no_padding" v-else>
                        <h6><strong>Caregiver's Feedback to Client</strong></h6>
                    </div>
                    <div class="col-md-12 no_padding">
                        <star-rating :view_type="true" :value="rating.rating"></star-rating>
                    </div>
                    <div class="col-md-12 no_padding" style="white-space: pre-line; margin-bottom: 10px;">
                        “@{{ rating.comment }}”
                    </div>
                    <div s class="col-md-12 no_padding" v-for="feedback in rating.ratings" v-if="rating.by_employee == false">
                        <star-rating :rating_type="feedback.text" :value="feedback.pivot.data" :only_view="true"></star-rating>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
