<div class="modal fade" id="experience-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Experience</h4>
            </div>
            <div class="modal-body">
                <div class="FindCaregivers__filter FindCaregivers__filter--w200">
                    <label for="#">Years of experience:</label>
                    <custom-select-ext class="years-experience" name="years-experience" id="years-experience" :as-list="true" v-if="selects"
                                      :options="selects.years_experience.items" :multiple="false" :selected.sync="experience.selected" :required="false">
                    </custom-select-ext>
                    <div class="error-message" v-if="experience.errors.message"> @{{ experience.errors.message }} </div>
                </div>
                <br>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('experience')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal" @click="cancelEdit('experience')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
