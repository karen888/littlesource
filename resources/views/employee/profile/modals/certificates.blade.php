<div class="modal fade" id="certificates-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="appl-send-message-label">Edit Certificates</h4>
            </div>
            <div class="modal-body">
                    <input type="hidden" v-model="certificates.id" />
                    <div class="row form-row">
                        <div class="col-md-12 no_padding">
                            <strong>Certification name * </strong>
                        </div>
                        <div class="col-md-12 full-width" :class="[certificates.name.errors.message ? 'error' : '' ]">
                            <input type="text" v-model="certificates.name.text" @keyup="certificates.name.errors.message = ''" />
                            <div class="error-message" v-if="certificates.name.errors.message"> @{{ certificates.name.errors.message }} </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12 no_padding">
                            <strong>Provider * </strong>
                        </div>
                        <div class="col-md-12 full-width" :class="[certificates.provider.errors.message ? 'error' : '' ]">
                            <input type="text" v-model="certificates.provider.text" @keyup="certificates.provider.errors.message = ''" />
                            <div class="error-message" v-if="certificates.provider.errors.message"> @{{ certificates.provider.errors.message }} </div>
                        </div>
                    </div>
                    <div class="row form-row">
                        <div class="col-md-12 no_padding">
                            <strong>Description</strong>
                        </div>
                        <div class="col-md-12">
                            <div class="full-width" :class="[certificates.description.errors.message ? 'error' : '' ]">
                                <textarea name="description" rows="5" v-model="certificates.description.text" @keyup="certificates.description.errors.message = ''"></textarea>
                                <div class="error-message" v-if="certificates.description.errors.message"> @{{ certificates.description.errors.message }} </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-row"> 
                        <div class="col-md-12 no_padding">
                            <strong>Date earned * </strong>
                        </div>
                        <div class="col-md-12 full-width" :class="[certificates.date_earned.errors.message ? 'error' : '' ]">
                            <input id="date-earned" type="text" v-model="certificates.date_earned.text" @keyup="certificates.date_earned.errors.message = ''" />
                            <div class="error-message" v-if="certificates.date_earned.errors.message"> @{{ certificates.date_earned.errors.message }} </div>
                        </div>
                    </div>    
                    <div class="row form-row">
                        <div class="col-md-12 no_padding">
                            <strong>Submission code or link * </strong>
                        </div>
                        <div class="col-md-12 full-width" :class="[certificates.submission.errors.message ? 'error' : '' ]">
                            <input type="text" v-model="certificates.submission.text" @keyup="certificates.submission.errors.message = ''" />
                            <div class="error-message" v-if="certificates.submission.errors.message"> @{{ certificates.submission.errors.message }} </div>
                        </div>
                    </div>                                                                          
                    <hr>
                <div class="actions">
                    <a class="btn btn-themed btn-primary btn-main-action" @click="setRequest('certificates')">SAVE</a>
                    <a class="btn btn-themed btn-preview" data-dismiss="modal"  @click="cancelEdit('certificates')">CANCEL</a>
                </div>
            </div>
        </div>
    </div>
</div>
