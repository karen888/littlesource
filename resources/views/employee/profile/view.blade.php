<div id="view-employee-profile">
    @include('employee.profile.modals.view-feedback')
    <div class="col-md-9" v-cloak>

        <input type="hidden" id="employee_id" value="{{ $employee['id'] }}" v-model="employee_id" />

        @if(isset($includeHr) && $includeHr)<hr>@endif

        <div class="caregiver-info">
            <img :src="photo" alt="">
            <div class="details">
                <h4>@{{ name }} {{--<small>(Australia)</small>--}}</h4>
                <div class="description">
                    <span>@{{ title }}</span>
                </div>
                <div class="rates pull-right profilerates" v-if="max_rate">
                    <span v-if="parseInt(min_rate) > 0">
                        <h4>@{{ min_rate }}</h4>
                        <h5>$/hr</h5>
                        <h4> - </h4>
                    </span>
                    <h4>@{{ max_rate }}</h4>
                    <h5>$/hr</h5>
                </div>                
                <div class="skills">
                    <div class="skill" v-if="skills.length" v-for="(key, skill) in skills">@{{ skill }}</div>
                </div>
            </div>


        </div>

        <div class="overview">
            <h5>Overview</h5>
            <hr>
            <p> @{{ overview }} </p>
        </div>
        <div class="work-history">
            <div class="heading">
                <h5>Work History and Feedback (@{{ job_history.length }})</h5>
            </div>
            <div class="history-filter">
                {{--<div class="pull-right">--}}
                    {{--@include('helpers.customselect', [--}}
                        {{--'select' => [--}}
                            {{--'name' => 'previous-jobs',--}}
                            {{--'options' => collect([['value' => '1', 'name' => 'Newest first', 'is_selected' => true], ['value' => '2', 'name' => 'Oldest First', 'is_selected' => false] ])--}}
                        {{--]--}}
                    {{--])--}}
                {{--</div>--}}
            </div>
            <div v-if="job_history.length" v-show="feedback.show" v-for="(key, feedback) in job_history">
                <div class="col-md-12">
                    <hr>
                </div>
                <div class="feedback-row">
                    <div class="job">
                        <div class="job-name"><a href="javascript:;" @click="openFeedback(key)">@{{ feedback.title }}</a></div>
                        <div class="job-rating" v-if="feedback.feedback.rating">
                            <star-rating :view_type="true" :value="feedback.feedback.rating"></star-rating>
                        </div>
                    </div>
                    <div class="feedback">
                        <div class="description" v-if="feedback.feedback.text.length">
                            “@{{ feedback.feedback.text }}”
                        </div>
                        <div class="description inactive" v-else>
                            No feedback given
                        </div>
                        <div class="rates">
                            <div class="rate">
                                $@{{ feedback.offer_paid }}/hr
                            </div>
                            <div class="timespan">
                                @{{ feedback.start_date }} @{{ feedback.end_date ? '- ' + feedback.end_date : '' }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr>
                <a href="javascript:;" class="view-more" v-if="load_more" @click="showFeedbacks()">View More (5)</a>
            </div>
        </div>
        <div class="education">
            <h5>Education</h5>
            <hr>
            <div v-if="educations.length" v-for="(key, education) in educations" class="institution">
                <div class="institution-name"><strong>@{{ education.school.text }}</strong>
                    <span style="color: #22b92c;" v-if="education.status == 1"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span>
                </div>
                <div class="institution-degree">@{{ education.degree.text }}</div>
                <div v-if="education.area_study.text" class="institution-degree">@{{ education.area_study.text }}</div>
                <div class="institution-graduate-year">@{{ education.dates_attended_from.selected }} - @{{ education.dates_attended_to.selected }}</div>
                <div>@{{ education.desc.text }}</div>
            </div>
            <div class="languages" v-if="languages.length">
                <strong>Languages</strong>
                <div v-for="(key, language) in languages" class="language">@{{ language.name }}</div>
            </div>
        </div>
        <div class="qualifications">
            <h5>Qualifications</h5>
            <hr>
            <ul>
                <li v-if="qualifications.smoker">
                    Non-smoker
                    <div class="value" v-if="selects">
                        <div v-for="smoker in selects.smoker.items" v-if="smoker.value == qualifications.smoker">
                            <div :class="[smoker.text == 'No' ? 'icon-checkmark' : 'icon-delete']"></div>
                        </div>
                    </div>
                </li>
                <li v-if="qualifications.sick_children">
                    Willing to care for sick children
                    <div class="value" v-if="selects">
                        <div v-for="care_sick in selects.care_sick.items" v-if="care_sick.value == qualifications.sick_children">
                            <div :class="[care_sick.text == 'Yes' ? 'icon-checkmark' : 'icon-delete']"></div>
                        </div>
                    </div>
                </li>
                <li v-if="qualifications.sick_children">
                    Comfortable with pets
                    <div class="value" v-if="selects">
                        <div v-for="pets in selects.pets.items" v-if="pets.value == qualifications.comfortable_with_pets">
                            <div :class="[pets.text == 'Yes' ? 'icon-checkmark' : 'icon-delete']"></div>
                        </div>
                    </div>
                </li>
                <li v-if="qualifications.have_transportation && selects">
                    Drivers licence
                    <div class="value" v-for="item in selects.drivers_licence.items"
                         v-if="qualifications.have_transportation == item.value">
                        @{{ item.text }}
                    </div>
                </li>
                <li v-if="qualifications.willing_travel && selects">
                    Willing to travel
                    <div class="value">
                        <div v-for="location_range in selects.location_range.items" v-if="location_range.value == qualifications.willing_travel">
                            Up to @{{ location_range.text }}
                        </div>
                    </div>
                </li>
                <li v-for="(index, care_type) in qualifications.care_type" v-if="selects">
                    <span v-if="index == 0">Care Type</span>
                    <div class="value" v-for="item in selects.care_type.items" v-if="care_type == item.value">
                        @{{ item.text }}
                    </div>
                </li>
                <li v-if="origin.qualifications.own_children">
                    Own children
                    <div class="value" v-if="selects">
                        <div v-for="own_children in selects.own_children.items" v-if="own_children.value == origin.qualifications.own_children">
                            <div :class="[own_children.text == 'Yes' ? 'icon-checkmark' : 'icon-delete']"></div>
                        </div>
                    </div>
                </li>
                <li v-for="(index, child_age) in qualifications.child_age" v-if="selects">
                    <span v-if="index == 0">Child Age Range</span>
                    <div class="value" v-for="item in selects.child_age.items" v-if="child_age == item.value">
                        @{{ item.text }}
                    </div>
                </li>
                {{--<li v-for="(index, qualification) in qualifications.qualification.selected" v-if="selects">--}}
                    {{--<span v-if="index == 0">Qualifications</span>--}}
                    {{--<div class="value" v-for="item in selects.qualification.items" v-if="qualification == item.value">--}}
                        {{--@{{ item.text }}--}}
                    {{--</div>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>
    <!-- Caregiver Options -->

    <div class="col-md-3" v-cloak>
        <div class="caregiver-options" v-if="!hidden_buttons">
            <a :href="contact_now" class="btn  btn-themed btn-caregiver-option btn-primary btn-contact">Contact Now</a>
            <a {!! !is_object($employee) || $employee->canApplyToJob() ? ':href="route_hire"' : ' style="cursor: not-allowed !important"'!!}  v-if="show_hire == true" class="btn btn-themed btn-caregiver-option btn-hire-now"
               @if(is_object($employee) === true && !$employee->canApplyToJob())
                data-toggle="popover-hover"
               data-title="User is not verified!"
                data-content="This caregiver has submitted all their verification documents (identity, qualifications, etc) but we have not verified those documents yet. Verification usually takes a couple of days. As soon as the documents are verified the contract will automatically become active."
                data-placement="top"
               @endif
            >Hire Now</a>
            <a href="javascript:;" class="btn btn-themed btn-caregiver-option btn-add-to-favorites" @click="toogleFavorite()">@{{ favoriteText }} Favorites</a>
        </div>

        <div class="caregiver-work-history">
            <strong>Work history</strong>
            <div class="success" v-if="employee_rating">
                <div class="job-rating" >
                    <star-rating :view_type="true" :value="employee_rating"></star-rating>
                </div>
            </div>
            <div class="success" v-else>
                <div class="job-rating">
                    <div class="icon-star-inactive"></div>
                    <div class="icon-star-inactive"></div>
                    <div class="icon-star-inactive"></div>
                    <div class="icon-star-inactive"></div>
                    <div class="icon-star-inactive"></div>
                </div>
            </div>

            <div v-for="item in selects.years_experience.items" v-if="item.value == experience.selected && selects">
                <div class="experience">
                    <strong>Experience</strong>
                    <div class="value">
                        <span>@{{ item.text }}</span>
                    </div>
                </div>
            </div>

            <div class="availability" v-if="selects">
                <strong>Availability</strong>

                <div class="value" v-if="!available.value">Not Available</div>
                <div v-else>
                    <div class="value" v-for="employment_type in selects.employment_type.items" v-if="available.selected == employment_type.value">
                        @{{ employment_type.text }}
                        <span v-if="getAvailabilityDays && selects">
                            (@{{ getAvailabilityDays }})
                        </span>
                    </div>
                </div>
            </div>
            {{--<div class="payment-options">
                <strong>Payment Options</strong>
                <div class="value">Cash</div>
            </div>--}}
        </div>
    </div>
</div>