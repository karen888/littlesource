@extends('app')

@section('content')
<div class="container" id="my-caregivers">
    <div class="content">
        <div class="row padd-top-50">
            <div class="col-md-3">
                <h4>My caregivers</h4>
                <div id="settings-nav">
                    <div class="filter">
                        <div class="inner-addon left-addon">
                            <div class="icon icon-search"></div>
                            <input type="text" placeholder="Search for Caregivers" v-model="search">
                        </div>
                    </div>
                    <ul class="nav nav-stacked">
                        <li class={{ active_class(if_route(['client.employee.hired'])) }}><a href="{{ route('client.employee.hired') }}"> <span class="icon icon-hired"></span>  Hired</a></li>
                        <li class={{ active_class(if_route(['client.employee.messaged'])) }}><a href="{{ route('client.employee.messaged') }}"> <span class="icon icon-messaged"></span>   Messaged</a></li>
                        <li class={{ active_class(if_route(['client.employee.favorites'])) }}><a href="{{ route('client.employee.favorites') }}"> <span class="icon icon-favorites"></span>  Favourites</a></li>
                        {{--<li class={{ active_class(if_route(['client.employee.recently-viewed'])) }}><a href="{{ route('client.employee.recently-viewed') }}"><span class="icon icon-recently-viewed"></span> Recently Viewed</a></li>--}}
                    </ul>
                </div>
            </div>
            <div class="col-md-9" v-cloak>
                @yield('caregivers_content')
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection