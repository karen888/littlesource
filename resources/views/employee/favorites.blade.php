@extends('employee.caregivers-template')

@section('caregivers_content')
    <div class="panel margin-top-60" id="client-favorites">
        <input type="hidden" v-model="type" value="favorites">
        <div class="panel-row favorite-caregivers-row" v-for="(key, note) in users" v-if="!emptyUsers">
            <img class="img_50" :src="note.photo" alt="">
            <div class="name">
                <a :href="note.employee_profile"><strong><span v-html="note.name"></span></strong></a>
                <div class="saved-date"><span v-html="note.created_at"></span></div>
            </div>
            <div class="notes">
                <p v-text="note.notes_origin" v-if="!note.edit"></p>
                <div class="full-width" :class="[note.error ? 'error' : '' ]" v-if="note.edit">
                    <textarea v-model="note.users" @keyup="note.error = ''"></textarea>
                    <div class="error-message" v-if="note.error"> <span v-html="note.error"></span> </div>
                </div>
                <div v-if="!note.edit">
                    <a @click="setEdit(key)" href="javascript:void(0);">Edit</a>
                </div>
                <div v-if="note.edit">
                    <a @click="saveNotes(key)" href="javascript:void(0);">Save</a>&nbsp;
                    <a @click="cancelNotes(key)" href="javascript:void(0);">Cancel</a>
                </div>
            </div>
            <div class="actions">
                <div class="actions-group">
                    <div class="trigger">
                        <div class="text_outer"><div class="text trn"> Actions </div></div>
                    </div>
                    <div class="list">
                        <!--<div class="option"
                             data-action="view-profile"
                             class="trn">
                            <a :href="note.employee_profile">View Profile</a>
                        </div>-->
                        <div class="option"
                             data-action="send-message"
                             class="trn">
                            <a :href="note.employee_invite">Invite to job</a>
                        </div>
                        <div class="option"
                        data-action="send-message"
                        class="trn">
                            <a :href="note.employee_send_message">Send Message</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-row empty" v-if="emptyUsers">
            <h5>You have no favourite caregivers yet.</h5>
        </div>
    </div>
@endsection
