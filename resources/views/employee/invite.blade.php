@extends('app')

@section('content')

    <div class="container padd-top-30" id="invite-job">
        <div class="row">
            <div class="col-md-12">
                <div class="main-heading">
                    <h4>Invite</h4>
                </div>
            </div>
        </div>
        <div class="job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Caregiver</strong>
                </div>

                <div class="col-md-4 full-width caregiver" :class="['full-width', caregiver.errors.message ? 'error' : '' ]" v-cloak>
                    <img class="img_50" src="{{ $caregiver->PhotoUrl }}" alt="" >
                    <strong>{{ $caregiver->name }}</strong>
                    <input type="hidden" name="caregiver" rows="5" v-model="caregiver.text" @keyup="caregiver.errors.message = ''" value="{{ $caregiver->id }}">
                    <div class="error-message" v-if="caregiver.errors.message"> @{{ caregiver.errors.message }} </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Message</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', message.errors.message ? 'error' : '' ]" v-cloak>
                        <textarea name="message" rows="5" v-model="message.text" @keyup="message.errors.message = ''"></textarea>
                        <div class="error-message" v-if="message.errors.message"> @{{ message.errors.message }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Job</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', job.errors.message ? 'error' : '' ]" v-cloak>
                        <custom-select class="choose_job"
                            :options="{{ json_encode($parentJob) }}"
                            @click="job.errors.message = ''"
                            :selected.sync="job.selected"
                            name="job"
                            @selected-text="jobSelectedText"
                        >
                        </custom-select>
                        <div class="error-message" v-if="job.errors.message"> @{{ job.errors.message }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Estimated Duration</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', duration.errors.message ? 'error' : '' ]" v-cloak>
                        <custom-select class="choose_duration"
                            :options="{{ json_encode($jobDurations) }}"
                            @click="duration.errors.message = ''"
                            :selected.sync="duration.selected"
                            name="duration"
                            @selected-text="durationSelectedText"
                        >
                        </custom-select>
                        <div class="error-message" v-if="duration.errors.message"> @{{ duration.errors.message }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Estimated Workload</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', workload.errors.message ? 'error' : '' ]" v-cloak>
                        <custom-select class="choose_workload"
                            :options="{{ json_encode($jobWorkloads) }}"
                            @click="workload.errors.message = ''"
                            :selected.sync="workload.selected"
                            name="workload"
                            @selected-text="workloadSelectedText"
                        >
                        </custom-select>
                        <div class="error-message" v-if="workload.errors.message"> @{{ workload.errors.message }} </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            <a class="btn btn-themed btn-primary btn-main-action" @click.prevent="createJobInvite">Send Invitation</a>
                            <a type="button" class="btn btn-themed btn-preview" href="/employee/my-caregivers/hired">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

@endsection
