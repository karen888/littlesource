@extends('app')

@section('page-title')Hourly, Bonus, and Expense Payment Agreement with Escrow Instructions @endsection
@section('content')

    <div class="page">
        @include('terms.partial.left')

        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="position: relative; margin-top: 100px;">
            <h1>Hourly, Bonus, and Expense Payment Agreement with Escrow Instructions</h1>
            <p>Effective date: {{date('d.m.Y')}}</p>
            <p>If Client and Caregiver enter into an Hourly Contract, or if Client makes a bonus or expense payment to Caregiver, this Hourly, Bonus, and Expense Payment Agreement with Escrow Instructions (<b>Agreement</b>) applies.</p>
            <p>To the extent permitted by applicable law, we may modify this Agreement, and the Escrow Instructions it contains, without prior notice to you, and any revisions to the Agreement will take effect when posted on the Site unless otherwise stated. Please check the Site often for updates.</p>
            <p>This Agreement hereby incorporates by reference the Terms of Service. Capitalised terms not defined in this Agreement are defined in the User Agreement, elsewhere in the Terms of Service, or have the meanings given such terms on the Site. The Escrow Instructions in this Agreement do not apply to Fixed-Price Escrow Accounts, though they do govern the making and receiving of bonus, expense and other miscellaneous payments for Fixed-Price Contracts.</p>
            <h1>1. DIGITAL SIGNATURE</h1>
            <p>By clicking to accept an Hourly Contract or make a bonus payment, Client and Caregiver are deemed to have executed this Agreement electronically, effective on the date Caregiver clicks to accept the Engagement, pursuant to the Australian Electronic Transactions Act. Doing so constitutes an acknowledgement that you are able to electronically receive, download, and print this Agreement and the Escrow Instructions it contains.</p>
            <h1>2. MAKING OR RECEIVING AN HOURLY PAYMENT</h1>
            <h2>2.1 TIME LOGS</h2>
            <p>For Hourly Contracts, reports of hours billed (<b>Time Logs</b>) are generated for hours worked until 11:59 PM GMT+10, each Monday following the week in which the hours were worked (the <b>Time Log Deadline</b>). Caregiver irrevocably authorises and instructs LittleOnes, as its agent, to (i) create an invoice on behalf of Caregiver for payment due based upon the hours recorded on the weekly Time Log before the Time Log Deadline; and (ii) submit the invoice on behalf of Caregiver to Caregiver's Client for payment.</p>
            <p>By recording time on a Time Log and allowing an invoice to be created based on that Time Log, Caregiver represents and warrants that (y) Caregiver has completed the applicable Caregiver Services fully and satisfactorily; and (z) the hours Caregiver reports are true, accurate, and complete.</p>
            <h2>2.2 TIME LOG REVIEW</h2>
            <p>Client must review and approve or dispute the weekly Time Log by 11:59 PM GMT+10 of the Friday following submission of the Time Log. Payments will be held in escrow during the Dispute Period (defined below), providing 4 additional days to review and dispute work before funds are released. During the Dispute Period, Client may initiate a Dispute as to some or all of the time recorded on the Time Log.</p>
            <p>On the Friday of the week following submission of the Time Log, Client will be deemed to have approved all undisputed time, and LittleOnes' Affiliate, LittleOnes Escrow Pty Ltd. (“LittleOnes Escrow“), will release escrow funds as described in this Agreement.</p>\
            <h1>3. MAKING OR RECEIVING A BONUS OR EXPENSE PAYMENT</h1>
            <p>Client may also pay Caregiver a bonus, tip, expense, or other miscellaneous payment, at Client’s discretion, using the Site. To pay a bonus to a Caregiver, Client must follow the instructions and links on the Site and provide the information requested. If Client clicks to pay a bonus to Caregiver, LittleOnes Escrow will release escrow funds as described in this Agreement.</p>
            <h1>4. INSTRUCTIONS TO PAY IRREVOCABLE</h1>
            <p>Client’s instruction to LittleOnes Escrow and its wholly owned subsidiaries to pay a Caregiver is irrevocable. Such instruction is Client’s authorisation to transfer funds to Caregiver from the Client Escrow Account or authorisation to charge Client’s Payment Method. Such instruction is also Client’s representation that Client has received, inspected and accepted the subject work or expense. Client acknowledges and agrees that upon receipt of Client’s instruction to pay Caregiver, LittleOnes Escrow will transfer funds to the Caregiver and that LittleOnes, LittleOnes Escrow, and other Affiliates have no responsibility to and may not be able to recover such funds. Therefore, and in consideration of services described in this Agreement, Client agrees that once LittleOnes Escrow or its subsidiary has charged Client’s Payment Method, the charge is non-refundable.</p>
            <h1>5. RELEASE AND DELIVERY OF AMOUNTS IN ESCROW</h1>
            <p>In addition, LittleOnes Escrow is authorised to and will release applicable portions of the Client Escrow Account (each portion, a <b>Release</b>) to the Caregiver Escrow Account, upon the occurrence of and in accordance with one or more Release Conditions provided below or as otherwise permitted by applicable law. The amount of the Release will be delivered to the Caregiver Escrow Account, in accordance with Caregiver's and Client’s instructions, as applicable, these Escrow Instructions, and the other Terms of Service.</p>
            <h2>5.1 RELEASE CONDITIONS</h2>
            <p>As used in these Escrow Instructions, <b>Release Condition</b> means any of the following:<br/>
                1	Client and Caregiver have submitted joint written instructions for a Release.<br/>
                2	Client has approved all or a portion of the Caregiver's weekly Time Log. This Release Condition will apply to and only for time recorded by the Caregiver that Client has approved.<br/>
                3	Client has not disputed time recorded on Caregiver's weekly Time Log during the Dispute Period pursuant to this Agreement. This Release Condition will apply to and only for time recorded by the Caregiver that was not disputed by the Client.<br/>
                4	LittleOnes reviews Client’s dispute of time recorded on Caregiver's weekly Time Log for an Engagement with Work Diaries pursuant to this Agreement and determines that the time is related to the Engagement requirements or Client’s instructions documented in the Work Diaries.<br/>
                5	Client initiates a Dispute with respect to Caregiver's weekly Time Log for an Engagement without Work Diaries pursuant to this Agreement and Client and Caregiver resolve the dispute without the assistance of LittleOnes.<br/>
                6	Issuance of the final order of a court of competent jurisdiction from which appeal is not taken.<br/>
                7	We believe, in our discretion, that Client or Caregiver has committed or is attempting to commit fraud, illicit acts, or has violated LittleOnes' Terms of Service, in which case LittleOnes may instruct LittleOnes Escrow to take such actions as we deem appropriate in our sole discretion and in accordance with applicable law.<br/>
            </p>
            <h1>6. HOURLY PAYMENT PROTECTION FOR CAREGIVERS</h1>
            <p>In the rare event that a Caregiver's Client does not make payment for legitimate services performed by a Caregiver, LittleOnes will provide limited payment protection to the Caregiver as detailed below and in the User Agreement (<b>Hourly Payment Protection</b>) as a membership benefit to foster fairness, reward loyalty, and encourage the Caregiver to continue to use the Site Services for their business needs. Hourly Payment Protection will be offered only if all of the following criteria are met:</p>
            <p>	1	Both Client and Caregiver must have agreed to use Work Diaries upon acceptance of the Hourly Contract, as part of the terms.<br/>
                2	Client must have an Account in good standing, a valid and authenticated default Payment Method, and Client must agree to automatically pay for hours billed by Caregiver through Work Diaries.<br/>
                3	Caregiver's Account must be in good standing.<br/>
                4	Caregiver must have used Work Diaries enabled to document any and all hours covered by the Hourly Payment Protection for Caregivers.<br/>
                5	The number of hours billed must be within the hours authorised by the Client for the week in the Work Diaries.<br/>
                6	We believe, in our sole discretion, that fraud, illicit acts, or a violation of LittleOnes' Terms of Service has been committed or is being committed or attempted, in which case Client and Caregiver irrevocably authorise and instruct LittleOnes Escrow to take such actions as we deem appropriate in our sole discretion and in accordance with applicable law, in order to prevent or remedy such acts, including to return the proceeds of such acts to their source of payment.<br/>
            </p>
            <p>LittleOnes will investigate and determine in its sole discretion whether the above terms and conditions are met.</p>
            <p>Hourly Payment Protection does not apply to: (1) hours not authorised by Client in the Work Diaries; (2) bonus payments; (3) refunds; (4) manual time; (5) time added after Client has Disputed a billing and before the resolution of that incident; (6) Fixed-Price Contracts; and (7) Engagements prohibited by the Terms of Service. The maximum rate per hour protected by LittleOnes to Caregiver under the Hourly Payment Protection for Caregivers is the lesser of: (i) the rate provided in the Hourly Contract terms; (ii) the usual hourly rate billed by Caregiver on the Site across all Clients; and (iii) the going rate for the same skills on the Site in Caregiver's area (such determination to be made in LittleOnes' sole discretion). The maximum amount of coverage under the Hourly Payment Protection for Caregivers for the life of a relationship between the same Client and Caregiver is $2,500 or 50 hours logged in Work Diary, whichever is less.</p>
            <h1>7. HOURLY PROTECTION FOR CLIENTS</h1>
            <p>Hourly Protection for Clients does not apply to: (1) Fixed-Price Contracts; and (2) Engagements prohibited by the Terms of Service.</p>
            <h1>8. DISPUTES BETWEEN CLIENT AND CAREGIVER</h1>
            <h2>8.1 DISPUTES INITIATED VIA THE PLATFORM</h2>
            <p>For Hourly-Rate Contracts, Client may dispute Caregiver's hours reported in the Time Log for the prior week (Sunday 12:00 a.m. midnight GMT+10 to Sunday 11:59 p.m. GMT+10) during the five days following the close of the weekly invoice period (Monday 12:00 a.m. midnight GMT+10 to Friday 11:59 p.m. GMT+10) (the “Dispute Period“). It is Client’s responsibility to review the Time Log of every Hourly-Rate Contract on a weekly basis and to file any disputes during the Dispute Period. Once the Dispute Period expires, Client will be deemed to have accepted the Caregiver Services and Caregiver Fees and can no longer dispute them. Disputes can only address the hours billed, not the quality of the Caregiver Services or the Work Product provided under Hourly-Rate Contracts. If Client disputes Caregiver's hours reported in the Time Log under an Hourly-Rate Contract during the Dispute Period, Client and Caregiver are encouraged to resolve the dispute between themselves. If Client and Caregiver fail to come to a resolution, LittleOnes will promptly investigate the Time Log and determine, in our sole discretion, whether an adjustment is appropriate. LittleOnes' determination of such dispute shall be final.</p>
            <p>If Client’s payment is unsuccessful, LittleOnes will review the work to determine if it qualifies for Hourly Payment Protection. If LittleOnes, in its sole discretion, determines that the work qualifies for Hourly Payment Protection, it will make payment to the Caregiver.</p>

<p>Client may choose to approve Caregiver's work prior to the end of the Dispute Period. If Client releases payment to Caregiver prior to the end of the Dispute Period, Client certifies that it accepts the work and waives any further right to dispute.</p>
            <p>You further acknowledge and agree that LittleOnes and Affiliates are not and will not be a party to any such dispute. LittleOnes Escrow may, at its sole discretion, withhold or delay payment in the event of dispute between a Client and a Caregiver.</p>
            <h2>8.2 LITTLEONES DISPUTE ASSISTANCE</h2>
            <p>Non-binding dispute assistance (<b>Dispute Assistance</b>) is available within 30 days of the date of the last release of funds from Client to Caregiver. If Client or Caregiver contacts LittleOnes via support ticket within 30 days of the date of the last payment from Client to Caregiver and requests non-binding dispute assistance for any dispute among between (a <b>Dispute</b>), LittleOnes will attempt to assist Client and Caregiver by reviewing the Dispute and proposing a mutual, non-binding resolution. LittleOnes will only review the 30 days of work performed prior to the date a User requests Dispute Assistance.</p>
            <p>	•	The LittleOnes Disputes team will notify Client and Caregiver via ticket by providing a notice of dispute along with a request for information and supporting documentation (if any).<br/>
                •	If both Client and Caregiver respond to the notice and request for information, then the Disputes team will review the documentation submitted and any information available on the Site that pertains to the Dispute. After review, the Disputes team will propose a mutual, non-binding resolution based on the results of the review.
                •	The proposed resolution is non-binding; Client and Caregiver can choose whether or not to agree to it. If Client and Caregiver agree in writing to the proposed resolution, Client and Caregiver agree that LittleOnes Escrow is authorised and irrevocably instructed to immediately release Escrow funds in accordance with the proposed resolution.<br/>
                •	If Client and/or Caregiver disagree with LittleOnes' proposed, non-binding resolution then Client and/or Caregiver must pursue the Dispute independently.<br/>
                •	LittleOnes reserves the right to review the Caregiver's work for 30 days prior to the date of the request for Dispute Assistance for compliance with Hourly Payment Protection requirements, and in its sole discretion, to make adjustments to invoices, and to direct LittleOnes Escrow to make appropriate releases to Client if it finds work that clearly does not relate Hourly Contract requirements or Client instructions in the Work Diaries or violations of the Terms of Service during its review of the work.
                <br/>
            </p>

            <h1>9. NO RESPONSIBILITY FOR CAREGIVER SERVICES OR CLIENT PAYMENTS</h1>
            <p>LittleOnes and Affiliates merely provide a platform for Internet payment services. LittleOnes and Affiliates do not have any responsibility or control over the Caregiver Services that Client purchases. Nothing in this Agreement deems or will be interpreted to deem LittleOnes or any Affiliate as Client’s or Caregiver's agent with respect to any Caregiver Services, or expand or modify any warranty, liability or indemnity stated in the Terms of Service. For example, LittleOnes does not guarantee the performance, functionality, quality, or timeliness of Caregiver Services.</p>


        </div>
    </div>

@endsection
@section('js-bottom')
    <script>
        function urlify(text) {
            var urlRegex = /(((https?:\/\/)|(www))[^\s\,]+)/g;
            return text.replace(urlRegex, function(url) {
                var abs_url;
                url = ""+url;

                var reg = new RegExp(/https?:\/\//g);

                if(!reg.test(url)) {
                    abs_url = "http://" + url;
                } else {
                    abs_url = url;
                }

                return '<a rel="noopener" target="_blank" href="' + abs_url + '">' + url + '</a>';
            })
            // or alternatively
            // return text.replace(urlRegex, '<a href="$1">$1</a>')
        }
        $('.col-lg-9 p').each(function(el) {
            $(this).html(urlify($(this).html()));
        })
    </script>
@endsection