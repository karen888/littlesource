
<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 terms-left">
<ul class="">
    <li>
        &raquo; <a href="/legal/user-agreement" @if($section == "user-agreement")class="active" @endif>User Agreement</a>
    </li>
    <li>
        &raquo; <a href="/legal/payment-agreement" @if($section == "payment-agreement")class="active" @endif>Hourly, Bonus, and Expense Payment Agreement with Escrow Instructions</a>
    </li>
    <li>
        &raquo; <a href="/legal/fixed-price" @if($section == "fixed-price")class="active" @endif>Fixed-Price Escrow Instructions</a>
    </li>
    <li>
        &raquo; <a href="/legal/lo-escrow" @if($section == "lo-escrow")class="active" @endif>LittleOnes Escrow Pty Ltd.</a>
    </li>
    <li>
        &raquo; <a href="/legal/fee-agreement" @if($section == "fee-agreement")class="active" @endif>Fee Agreement</a>
    </li>
    <li>
        &raquo; <a href="/legal/proprietary-rights" @if($section == "proprietary-rights")class="active" @endif>Proprietary Rights Infringement Reporting Procedures</a>
    </li>

</ul>
</div>