@extends('app')

@section('page-title') Fee Agreement @endsection
@section('content')

    <div class="page">
        @include('terms.partial.left')

        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="position: relative; margin-top: 100px;">
            <h1>Fee Agreement</h1>
            <p>Effective date: {{date('d.m.Y')}}</p>
            <p>This Fee Agreement (this <b>Agreement</b>) provides information on the fees LittleOnes and its Affiliate LittleOnes Escrow Pty Ltd. (<b>LittleOnes Escrow</b>) charge for use of the Site’s communication, invoicing, dispute resolution and payment services, including Payment Protection. This Agreement is part of the Terms of Service. Capitalised terms not defined in this Agreement are defined in the User Agreement, elsewhere in the Terms of Service, or have the meanings given such terms on the Site.</p>
            <p>By continuing to use the Site or the Site Services on or after the Effective Date, you accept and agree to this Agreement. To the extent permitted by applicable law and except as otherwise provided in the Terms of Service, we may modify this Agreement without prior notice to you, and any revisions to this Agreement will take effect when posted on the Site unless otherwise stated. Please check the Site often for updates.</p>


            <h1>1. SERVICE FEES CHARGED TO CAREGIVERS</h1>
            <p>Pursuant to the User Agreement, LittleOnes Escrow charges Caregivers a Service Fee for each Engagement, as discussed in further detail in this Section 1. Where applicable, LittleOnes or LittleOnes Escrow may also collect taxes (such as GST) on Service Fees.</p>
            <h2>1.1  STRAIGHT PRICING</h2>
            <p>We will charge you a Service Fee of $service_fee% of the Caregibers Fees (<b>Straight Pricing</b>), as shown in the examples below.</p>
            <p>TABLE:

                <table class="table table-bordered table-condensed" style="max-width: 500px;margin-left: 27px;">
                <thead>
                <tr>
                    <th>CONTRACT TYPE</th>
                    <th>CAREGIVER FEES</th>
                    <th>SERVICE FEES</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Fixed-Price</td>
                    <td>$500</td>
                    <td>$55</td>
                </tr>
                <tr>
                    <td>Hourly</td>
                    <td>$50 for one hour</td>
                    <td>$5.50 for one hour</td>
                </tr>


                <tr>
                    <td colspan="3">Note: Examples in this Agreement are provided for illustrative purposes only and are not binding. Because of rounding, the Service Fees charged may differ slightly.</td>
                </tr>
                </tbody>
            </table>
            </p>
            <h1>2. PAYMENT PROCESSING FEES CHARGED TO CLIENT</h1>
            <h2>2.1  $payment% PAYMENT PROCESSING FEE</h2>
            <p>Starting on or after the Effective Date, we will charge Clients a payment processing and administration fee of $payment_fee% of the total amount of each payment made for the Site Services (the <b>Payment Processing Fee</b>), except as otherwise provided in this Agreement. We will notify you of the date when we will begin charging the Payment Processing Fee by email and by posting a notice on the Site.</p>
            <p>If payments made by a Client are released to the Client Escrow Account for any reason or refunded by a Caregiver, the Payment Processing Fee, if applicable, will not be refunded.</p>
            <p>Enterprise Clients are charged the rate(s) provided in the applicable Enterprise Client contract and are not charged the Payment Processing Fee, unless otherwise provided in the applicable Enterprise Client contract.</p>

        </div>
    </div>

@endsection
@section('js-bottom')
    <script>
        function urlify(text) {
            var urlRegex = /(((https?:\/\/)|(www))[^\s\,]+)/g;
            return text.replace(urlRegex, function(url) {
                var abs_url;
                url = ""+url;

                var reg = new RegExp(/https?:\/\//g);

                if(!reg.test(url)) {
                    abs_url = "http://" + url;
                } else {
                    abs_url = url;
                }

                return '<a rel="noopener" target="_blank" href="' + abs_url + '">' + url + '</a>';
            })
            // or alternatively
            // return text.replace(urlRegex, '<a href="$1">$1</a>')
        }
        $('.col-lg-9 p').each(function(el) {
            $(this).html(urlify($(this).html()));
        })
    </script>
@endsection