@extends('app')

@section('page-title') User Agreement @endsection
@section('content')

    <div class="page">
        @include('terms.partial.left')

        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="position: relative; margin-top: 100px;">
            <h1>Proprietary Rights Infringement Reporting Procedures</h1>
            <p>Effective date: {{date('d.m.Y')}}</p>
            <p>LittleOnes Pty Ltd. (<b>LittleOnes</b>, <b>our</b>, <b>us</b> or <b>we</b>) provides this Proprietary Rights Infringement Reporting Procedures (these <b>Procedures</b>) to inform you of our policies and procedures regarding claims of infringement of proprietary rights by third parties on our website located at www.littleones.com.au (the <b>Site</b>). These Procedures may be updated from time to time. We will notify you of any material changes by posting the new Procedures on the Site. You are advised to consult these Procedures regularly for any changes.</p>
            <p>If you are a proprietary rights owner and you believe someone is using LittleOnes to infringe your proprietary rights, you may provide LittleOnes with the notice described below (the <b>Notice</b>) by email to $email_legal by postal mail to Attn: Legal, $address. The Notice fulfills the requirements of the Australian Copyright Amendment Act 2006.</p>
            <p>In response to your Notice, LittleOnes may remove or disable access to the allegedly infringing material, and take such other actions we deem appropriate in our sole discretion. Please bear in mind that you may be liable for damages (including costs and legal fees) if you materially misrepresent that material is infringing. If we remove or disable access, we will attempt to contact the poster of the allegedly infringing material so that the poster may provide a counter notification (the <b>Counter Notice</b>) as described below.</p>
            <p>
                Please include the following items in your Notice, and number them as follows:
                <Br/>1	Identify the proprietary rights that you claim are infringed. For example, identify your trademark or quote your copyrighted material. Provide a URL or link to where the material is located, if possible.
                <Br/>2	Identify the website, webpage, posting, profile, feedback, or other material that you claim infringes your proprietary rights. Provide information reasonably sufficient to enable us to locate it.
                <Br/>3	Identify yourself by name. Provide your address, telephone number, and email address.
                <Br/>4	Provide contact information for the owner or administrator of the allegedly infringing material, if possible.
                <Br/>5	Include the following statement: <b>I have a good faith belief that use of the material in the manner described is not authorised by the owner of the exclusive proprietary right, its agent, or the law.</b>
                <Br/>6	Include the following statement: <b>The information in this notice is accurate.</b>
                <Br/>7	Include the following statement: <b>I swear, under penalty of perjury, that I am authorised to act on behalf of the owner of the exclusive proprietary right that is allegedly infringed.</b>
                <Br/>8	Sign and date the Notice.
            </p>
            <p>The owner or administrator of the allegedly infringing material may provide LittleOnes with a Counter Notice by email to $email_legal or by postal mail to Attn: Legal, $address. The Counter Notice fulfills the requirements of the Australian Copyright Amendment Act 2006.</p>
            <p>In response to your Counter Notice, LittleOnes may reinstate the allegedly infringing material. Please bear in mind that you may be liable for damages (including costs and legal fees) if you materially misrepresent that material is not infringing the proprietary rights of others.</p>
            <p>
                Please include the following items in your Counter Notice, and number them as follows:
                <br/>1	Identify the website, webpage, posting, profile, feedback, or other material that LittleOnes has removed or to which LittleOnes has disabled access. Identify the location at which the material appeared before it was removed or access to it was disabled.
                <br/>2	Identify yourself by name. Provide your address, telephone number, and an email address where we can contact you.
                <br/>3	Include a statement that you consent to the jurisdiction of the Melbourne Supreme Court.
                <br/>4	Include the following statement: <b>I swear, under penalty of perjury, that I have a good faith belief that each website, webpage, posting, profile, feedback or other material identified above was removed or disabled as a result of a mistake or misidentification of the material to be removed or disabled.</b>
                <br/>5	Sign and date the Counter Notice.
            </p>
            <p>Please bear in mind that LittleOnes cannot give you legal advice. If you have questions about whether certain proprietary rights are valid or whether certain material is infringing, you should contact a lawyer.</p>

        </div>
    </div>

@endsection
@section('js-bottom')
    <script>
        function urlify(text) {
            var urlRegex = /(((https?:\/\/)|(www))[^\s\,]+)/g;
            return text.replace(urlRegex, function(url) {
                var abs_url;
                url = ""+url;

                var reg = new RegExp(/https?:\/\//g);

                if(!reg.test(url)) {
                    abs_url = "http://" + url;
                } else {
                    abs_url = url;
                }

                return '<a rel="noopener" target="_blank" href="' + abs_url + '">' + url + '</a>';
            })
            // or alternatively
            // return text.replace(urlRegex, '<a href="$1">$1</a>')
        }
        $('.col-lg-9 p').each(function(el) {
            $(this).html(urlify($(this).html()));
        })
    </script>
@endsection