@extends('app')

@section('page-title') LittleOnes Escrow Pty Ltd. @endsection
@section('content')

    <div class="page">
        @include('terms.partial.left')

        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="position: relative; margin-top: 100px;">
            <h1>LittleOnes Escrow Pty Ltd.</h1>
            <p>Effective date: {{date('d.m.Y')}}</p>
            <p>LittleOnes Escrow Inc. provides escrow services solely to Users of LittleOnes. We provide these escrow services solely to deliver, hold, or receive payment for jobs for services engaged through, and to pay fees including service, membership and payment processing and administration fees to, LittleOnes. These escrow services are intended for business use by Caregivers, and Caregivers agree to use these escrow services only for business purposes and not for consumer, personal, family, or household purposes.</p>
            <p>LITTLEONES ESCROW PTY LTD. DOES NOT PROVIDE ESCROW SERVICES TO THE GENERAL PUBLIC.</p>

        </div>
    </div>

@endsection
@section('js-bottom')
    <script>
        function urlify(text) {
            var urlRegex = /(((https?:\/\/)|(www))[^\s\,]+)/g;
            return text.replace(urlRegex, function(url) {
                var abs_url;
                url = ""+url;

                var reg = new RegExp(/https?:\/\//g);

                if(!reg.test(url)) {
                    abs_url = "http://" + url;
                } else {
                    abs_url = url;
                }

                return '<a rel="noopener" target="_blank" href="' + abs_url + '">' + url + '</a>';
            })
            // or alternatively
            // return text.replace(urlRegex, '<a href="$1">$1</a>')
        }
        $('.col-lg-9 p').each(function(el) {
            $(this).html(urlify($(this).html()));
        })
    </script>
@endsection