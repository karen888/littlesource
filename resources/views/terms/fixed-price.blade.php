@extends('app')

@section('page-title') Fixed-Price Escrow Instructions @endsection
@section('content')

    <div class="page">
        @include('terms.partial.left')

        <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="position: relative; margin-top: 100px;">
            <h1>Fixed-Price Escrow Instructions</h1>
            <p>Effective date: {{date('d.m.Y')}}</p>
            <p>If a Client and a Caregiver enter into a Fixed-Price Contract, these Fixed-Price Escrow Instructions (<b>Escrow Instructions</b>) apply. These Escrow Instructions govern Fixed Price Escrow Accounts. If you have a Fixed-Price Contract and wish to make or receive a miscellaneous or bonus payment, those activities are governed by the Hourly, Bonus, and Expense Payment Agreement with Escrow Instructions.</p>
            <p>To the extent permitted by applicable law, we may modify these Escrow Instructions without prior notice to you, and any revisions to these Escrow Instructions will take effect when posted on the Site unless otherwise stated. Please check the Site often for updates.</p>
            <p>These Escrow Instructions hereby incorporate the Terms of Service (<b>Terms of Service</b>). Capitalised terms not defined in these Escrow Instructions are defined in the User Agreement, elsewhere in the Terms of Service, or have the meanings given such terms on the Site. These Escrow Instructions do not apply to Hourly Contracts.</p>
            <h1>1. DIGITAL SIGNATURE</h1>
            <p>By clicking to fund Escrow (a <b>Funding Approval</b>) or to accept a Fixed-Price Contract, Client and Caregiver are deemed to have executed these Escrow Instructions electronically, effective on the date Caregiver clicks to accept the engagement, pursuant to the Australian Electronic Transactions Act. Doing so constitutes an acknowledgement that you are able to electronically receive, download, and print these Escrow Instructions. All references to the Escrow in these Escrow Instructions will include the initial Funding Approval and any additional Funding Approval for the Engagement.</p>
            <h1>2. RELEASE AND DELIVERY OF AMOUNTS IN ESCROW</h1>
            <p>Client and Caregiver irrevocably authorise and instruct LittleOnes Escrow Inc. (“LittleOnes Escrow“) to release applicable portions of the Fixed Price Escrow Account (each portion, a <b>Release</b>) to their Caregiver Escrow Account or Client Escrow Account, as applicable, via the Site, upon the occurrence of and in accordance with one or more Release Conditions provided below or as otherwise permitted by applicable law. The amount of the Release will be delivered to the applicable Escrow Account in accordance with Caregiver's or Client’s instructions, as applicable, these Escrow Instructions, and the other Terms of Service.</p>
            <h2>2.1 RELEASE CONDITIONS</h2>
            <p>
                As used in these Escrow Instructions, <b>Release Condition</b> means any of the following:<br/>
                1	Client clicks to release funds to Caregiver.<br/>
                2	Caregiver cancels the contract before a milestone payment has been released.<br/>
                3	Client and Caregiver have submitted joint written instructions for a Release.<br/>
                4	Client and Caregiver agree to close the contract.<br/>
                5	Client or Caregiver has failed to make its Arbitration payment initiated by the other Party pursuant to the Dispute Assistance Policy in Section 5 of these instructions.<br/>
                6	Client and Caregiver have failed to timely submit to Arbitration for an unresolved Dispute as such term is defined in the Dispute Assistance Policy in Section 5 of these instructions.<br/>
                7	Client or Caregiver has failed timely to respond to an LittleOnes Dispute Assistance notification as required by the Dispute Assistance Policy in Section 5 of these instructions.<br/>
                8	Client or Caregiver has failed timely to pay for its share of Arbitration costs as required by the Dispute Assistance Policy in Section 5 of these instructions.<br/>
                9	Client or Caregiver otherwise has failed to comply with the Dispute Assistance Policy in Section 5 of these instructions.<br/>
                10	Submittal of the final binding award of an arbitrator appointed pursuant to the Dispute Assistance Policy in Section 5 of these instructions.<br/>
                11	Issuance of the final order of a court of competent jurisdiction from which appeal is not taken.<br/>
                12	We believe, in our sole discretion, that fraud, illicit acts, or a violation of LittleOnes' Terms of Service has been committed or is being committed or attempted, in which case Client and Caregiver irrevocably authorise and instruct LittleOnes Escrow to take such actions as we deem appropriate in our sole discretion and in accordance with applicable law, in order to prevent or remedy such acts, including to return the proceeds of such acts to their source of payment.<br/>
            </p>
            <h1>3. DORMANT ENGAGEMENTS</h1>
            <p>To be fair to Clients and Caregivers, LittleOnes has a procedure for Fixed-Price Contracts that appear Dormant. For purposes of determining Dormant status, <b>activity</b> means business term or milestone updates or requests, Fixed-Price Escrow Funding, Fixed-Price Escrow Release, Fixed-Price Escrow Refunds, Funding requests, Release requests, Engagement Cancellation requests, Status Report submittals, or actions under the Fixed Price Dispute Assistance Policy.</p>
            <p>
                If a Fixed-Price Contract has a Fixed-Price Escrow Account with a balance but has had no activity for 90 consecutive days after the last milestone date contained in the business terms, then the Engagement will be <b>Dormant.</b> Dormant Engagements are subject to the following rules:<br/>
                1	LittleOnes will notify Client when the Engagement becomes Dormant (<b>Dormant Date</b>).<br/>
                2	If no activity other than Release requests has occurred within 7 days after the Dormant Date, LittleOnes will notify the Caregiver that the Engagement is Dormant.<br/>
                3	If Caregiver and Client take no action for 7 days after the Dormant Date, Caregiver and Client agree that LittleOnes Escrow is authorised and irrevocably instructed to immediately release escrow funds to Client.<br/>
                4	If Caregiver submits a Release request and client does not take any action for 14 days from the date of the Release request, Caregiver and Client agree that LittleOnes Escrow is authorised and irrevocably instructed to immediately release to Caregiver the amount related to the milestone with Release request.<br/>
                5	All funds released to Caregiver under this Section, Dormant Engagements, will be subject to the applicable Service Fees for Caregivers.<br/>
            </p>
            <h1>4. REFUNDS AND CANCELLATIONS</h1>
            <p>Client and Caregiver are encouraged to come to a mutual agreement if refunds or cancellations are necessary. If there are no funds in escrow, Client or Caregiver can cancel the contract at any time by clicking to close the contract. If funds are held in escrow, refunds and cancellations must be initiated by Client or Caregiver by following the steps below.</p>
            <h2>4.1 CANCELLATION BY CAREGIVER</h2>
            <p>If Caregiver wants to cancel a contract with funds held in escrow, Caregiver must click to close the contract. When Caregiver clicks to close the contract, Caregiver and Client agree that LittleOnes Escrow is authorised and irrevocably instructed to immediately release to Client all Escrow funds associated with the contract.</p>
            <h2>4.2 CANCELLATION BY CLIENT</h2>
            <p>If Client wants to cancel a contract with funds held in escrow, Client must click to close the contract. Caregiver must either click to approve or dispute the Client’s cancellation within 7 days. If Caregiver approves the cancellation, Caregiver and Client agree that LittleOnes Escrow is authorised and irrevocably instructed to immediately release to Client all escrow funds associated with the contract. If you are using the Site on a mobile device and do not have the ability to approve or dispute the cancellation with a click on the mobile website, Caregiver must dispute the Client’s cancellation via support ticket within 7 days. If Caregiver takes no action within 7 days from the date Caregiver was notified of the cancellation, Caregiver and Client agree that LittleOnes Escrow is authorised and irrevocably instructed to immediately release to Client all Escrow funds associated with the contract. If Caregiver disputes the cancellation, Caregiver and Client will be offered LittleOnes Dispute Assistance.</p>
            <h1>5. DISPUTE ASSISTANCE PROGRAM</h1>
            <p>If Client and Caregiver fail to come to a mutual resolution by way of the Refund and Cancellation process as stated in Section 4, LittleOnes provides this Dispute Assistance Program as a mechanism to resolve the Dispute.</p>
            <h2>5.1 DEFINITIONS AND KEY DATES</h2>
            <p>
                1	<b>Arbitration</b> means binding non-appearance based Arbitration by a neutral third party as described in Section 5.<br/>
                2	<b>Arbitration Payment</b> means Client’s or Caregiver's applicable portion of the costs of Arbitration as more particularly described in Section 5 below.<br/>
                3	<b>Arbitration Limitations Date</b> means the date 30 days after the date a Dispute is filed via the Site or via support ticket with LittleOnes.<br/>
                4	<b>Dispute</b> means a dispute between a Client and Caregiver concerning a Fixed-Price Engagement and covered by this Dispute Assistance Program.<br/>
                5	<b>Dispute Assistance Deadline</b> means the date 30 days after the Client was billed for the last milestone.<br/>
                6	<b>LittleOnes Dispute Assistance</b> means the Dispute assistance provided by LittleOnes as set forth in this Section 5.<br/>
            </p>
            <h2>5.2 AVAILABILITY OF LITTLEONES DISPUTE ASSISTANCE</h2>
            <p>Dispute Assistance is only available (i) after initial funding of the Fixed Price Escrow Account associated with the Engagement, and (ii) prior to the Dispute Assistance Deadline. Dispute Assistance is not available to either the Caregiver or the Client via the Site after the Dispute Assistance Deadline.</p>
            <h2>5.3 NON-BINDING ASSISTANCE</h2>


            <p>LittleOnes will first attempt to assist Client and Caregiver by reviewing the Dispute and proposing a mutual, non-binding resolution.
                <Br/>▪	The LittleOnes Disputes team will notify Client and Caregiver via ticket by providing a notice of dispute along with a request for information and supporting documentation (if any).
                <Br/>                ▪	If both Client and Caregiver respond to the notice and request for information, then the Disputes team will review the documentation submitted and any information available on the Site that pertains to the Dispute. After review, the Disputes team will propose a mutual, non-binding resolution based on the results of the review.
                <Br/>                ▪	The proposed resolution is non-binding; Client and Caregiver can choose whether or not to agree to it. If Client and Caregiver agree in writing to the proposed resolution, Client and Caregiver agree that LittleOnes Escrow is authorised and irrevocably instructed to immediately release Escrow funds in accordance with the final resolution.
                <Br/>▪	If Client or Caregiver disagrees with LittleOnes' proposed, non-binding resolution, they may proceed to Arbitration. If Client and Caregiver do not choose to arbitrate, Caregiver and Client agree that LittleOnes Escrow is authorised and irrevocably instructed to immediately release to Client all funds held in Escrow.
                <Br/>▪	If Client or Caregiver chooses to arbitrate by notifying LittleOnes via support ticket of their intent to arbitrate, LittleOnes will notify both Client and Caregiver via ticket that they must make the Arbitration Payment within 5 business days of the notice (the “First Arbitration Notice“). If both parties make the Arbitration Payment (including after initial notice of non-payment), then LittleOnes will deliver instructions for initiating Arbitration.
                <Br/>▪	If Client or Caregiver does not make the Arbitration Payment within 5 business days of the notice, Caregiver and Client will be deemed to have irrevocably authorised and instructed LittleOnes Escrow to, and LittleOnes Escrow will, release the disputed funds to the party that paid the Arbitration Payment to the extent that any disputed funds remain in the Escrow Account. If no funds remain in the Escrow, LittleOnes will close the Dispute.
                <Br/>▪	If Client and Caregiver both do not make the Arbitration Payment by the Arbitration Limitations Date, then Client and Caregiver will be deemed to have authorised and instructed LittleOnes Escrow to, and LittleOnes Escrow will, release the disputed funds to Client, to the extent that any disputed funds remain in the Escrow Account, if no funds remain LittleOnes will close the Dispute.
            </p>
            <h2>5.8 ARBITRATION AWARD</h2>
            <p>You agree that the arbitrator is authorised to decide the Dispute within its discretion. You agree that the arbitrator’s award is final, that it may be entered in and enforced by any court of competent jurisdiction, and that if the arbitrator delivers notice of any award to LittleOnes, then LittleOnes and LittleOnes Escrow have the right to treat such notice as conclusive and act in reliance thereon.</p>
            <h2>5.9 SERVICE FEES FOR ESCROW FUNDS RELEASED FOLLOWING DISPUTE ASSISTANCE</h2>
            <p>All Escrow Funds released under this policy are subject to the normal Service Fees associated with Escrow Accounts, as detailed in the User Agreement and applicable Escrow Instructions.</p>
            <h1>6. NOTICES</h1>
            <p>All notices to a User required by these Escrow Instructions will be made via email sent by LittleOnes to the User’s registered email address. Users are solely responsible for maintaining a current, active email address registered with LittleOnes, for checking their email and for responding to notices sent by LittleOnes to the User’s registered email address.</p>
            <h1>7. COOPERATION WITH THE DISPUTE ASSISTANCE PROGRAM</h1>
            <p>All claims, disputes or other disagreements between you and another User that are covered by the Dispute Assistance Program must be resolved in accordance with the terms in the Dispute Assistance Program. All claims filed or brought contrary to the Dispute Assistance Program will be considered improperly filed, and LittleOnes will have the right to take any other action, including suspension or termination of your Account, and any other legal action as LittleOnes deems appropriate in its sole discretion.</p>

            <h1>8. ABUSE</h1>
            <p>LittleOnes, in its sole discretion, reserves the right to suspend or terminate your Account immediately upon giving notice to you if LittleOnes believes you may be abusing this Dispute Assistance Program or as otherwise permitted by the other Terms of Service. However, any Disputes for any Engagements that existed prior to termination will be subject to the Terms of Service.</p>


        </div>
    </div>

@endsection
@section('js-bottom')
    <script>
        function urlify(text) {
            var urlRegex = /(((https?:\/\/)|(www))[^\s\,]+)/g;
            return text.replace(urlRegex, function(url) {
                var abs_url;
                url = ""+url;

                var reg = new RegExp(/https?:\/\//g);

                if(!reg.test(url)) {
                    abs_url = "http://" + url;
                } else {
                    abs_url = url;
                }

                return '<a rel="noopener" target="_blank" href="' + abs_url + '">' + url + '</a>';
            })
            // or alternatively
            // return text.replace(urlRegex, '<a href="$1">$1</a>')
        }
        $('.col-lg-9 p').each(function(el) {
            $(this).html(urlify($(this).html()));
        })
    </script>
@endsection