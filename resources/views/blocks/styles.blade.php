<!-- Fonts -->
<!--<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>-->

<link rel="stylesheet" href="/bootstrap/font-awesome/css/font-awesome.min.css" type="text/css" />

{{--{!! $csss or '' !!} removed because it's a duplicated bootstrap--}}
<link href="/js/select2/css/select2.min.css" rel="stylesheet" />
<link href="/bootstrap/bootstraphelp/css/bootstrap-formhelpers.min.css" rel="stylesheet">

<!-- whizzweb editing -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css') }}">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
