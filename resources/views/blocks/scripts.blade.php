<script src="/js/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script src="/bootstrap/js/bootstrap.min.js"></script>

<script src="/js/select2/js/select2.min.js"></script>
<script src="/bootstrap/bootstraphelp/js/bootstrap-formhelpers-datepicker.js"></script>
<script src="/bootstrap/bootstraphelp/js/lang/en_US/bootstrap-formhelpers-datepicker.en_US.js"></script>

<script src="/js/vendor/modernizr.js"></script>
<script src="/js/inputmask/jquery.inputmask.bundle.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/hammer.js/2.0.8/hammer.min.js"></script>


{{--<!--<script src="/js/app.js"></script>-->--}}
{!! $javascripts or '' !!}{{--<!--TODO: check if there are some scripts used, otherwise remove it-->--}}

<script src="/js/tag-it.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/all.js"></script>
