<footer class="main-footer">
    <div class="main-footer-inner">
        <div class="nav-soc">
            <nav class="nav-footer">
                <ul class="nav nav-pills">
                    <li class="about">
                        <a href="/about">About us</a>
                    </li>
                    <li class="terms">
                        <a href="/legal/user-agreement">Terms of Service</a>
                    </li>
                    <li class="piracy">
                        <a href="{{route('privacy')}}">Privacy Policy</a>
                    </li>
                    <li class="cookie faqs">
                        <a href="{{route('faqs')}}">FAQs</a>
                    </li>
                    <li class="cookie">
                        <a href="/contact">Contact Us</a>
                    </li>
                </ul>
            </nav>

            <div class="social-networks">
                <ul class="list-inline">

                    <li><a href="https://www.facebook.com/LittleOnes.AU1" target="_blank"><i class="fa fa-facebook-square fa-2x" aria-hidden="true" style="color:#fff; opacity: 0.5;"></i></a></li>
                    <li><a target="_blank" href="https://www.instagram.com/littleones.com.au/"><i class="fa fa-instagram fa-2x" aria-hidden="true" style="color:#fff; opacity: 0.5;"></i></a></li>
                </ul>
            </div>
        </div>

        <div class="foo-text" style="width: 180px !important;">
           <p class="inside-text" style="font-size: 10pt;">
               littleones.com.au Pty Ltd<br/>
               ABN: 47 169 565 468<br/>
               3/3 Levida Drive, Carrum Downs. VIC
            </p>
            <p class="copy-right-text">
                &copy; {{ $carbon->now()->year }} Little Ones Australia.
            </p>
        </div>
        <div class="foo-text-r">
            <p><!--put text here if u want--></p>
        </div>

    </div>
</footer><!-- main-footer -->