@extends('reports.transaction_history')

@section('transaction_history_content')

<table class="table table-striped">
    <thead>
        <th>Date</th>
        <th>Description</th>
        <th>Client</th>
        <th>Amount</th>
    </thead>
    <tbody>
    @foreach($data_history as $val)
        <tr>
            <td>{{ $val->created_at }}</td>
            <td>{{ $val->payment_description }}</td>
            <td>{{ $val->name }}</td>
            <td>{{ $val->payment_amount }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@endsection

