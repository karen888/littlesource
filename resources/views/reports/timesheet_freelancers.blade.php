@extends('app')

@section('content')

    <div class="page-header">
        <h3>Timesheet - freelancers</h3>
    </div>

    <div class="row">
        <div class="pull-left">
        <h4>
            <a href="{{ routeClear('reports') }}/timesheet/freelancers?week={{ $week->last_week }}&year={{ $week->last_year }}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
            {{ $week->week_date }}
            <a href="{{ routeClear('reports') }}/timesheet/freelancers?week={{ $week->next_week }}&year={{ $week->next_year }}"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
            </h4>
        </div>

        <div class="pull-right">
        </div>

        <div class="clearfix"></div>
    </div>

    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Freelancer</th>
                    @foreach($week->days_of_week as $val)
                        <th class="text-center">{{ $val->view_d }} <br> {{ $val->view_md }}</th>
                    @endforeach
                    <th>Hours</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>

@endsection
