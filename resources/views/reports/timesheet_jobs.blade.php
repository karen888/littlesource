@extends('app')

@section('content')

    <div class="page-header">
        <h3>Timesheet - jobs</h3>
    </div>

    <div class="row">
        <div class="pull-left">
        <h4>
            <a href="{{ routeClear('reports') }}/timesheet/jobs?week={{ $week->last_week }}&year={{ $week->last_year }}"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span></a>
            {{ $week->week_date }}
            <a href="{{ routeClear('reports') }}/timesheet/jobs?week={{ $week->next_week }}&year={{ $week->next_year }}"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></a>
            </h4>
        </div>

        <div class="pull-right">
            @if ($week->curr_week)
                <a href="javascript:void(0);" class="btn btn-success" id="timesheet-add-time">Add Time</a>
            @endif
        </div>

        <div class="clearfix"></div>
    </div>

    <div class="row">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Freelancer</th>
                    @foreach($week->days_of_week as $val)
                        <th class="text-center">{{ $val->view_d }} <br> {{ $val->view_md }}</th>
                    @endforeach
                    <th>Hours</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>


@if ($week->curr_week)
<div class="modal fade" id="timesheet-add-time-send" tabindex="-1" role="dialog" aria-labelledby="timesheet-add-time-send-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="timesheet-add-time-send-label">Add Time</h4>
            </div>
    	    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="user_id" value="">

                <div class="modal-body">
                    <div class="form-group" data-name="contract">
            			<label class="col-md-3 control-label">Contracts</label>
            			<div class="col-md-8">
                            <select class="form-control" name="contract">
                                <option value="0">choose contract...</option>
                                @foreach($contracts as $val)
                                    <option value="{{ $val->id }}">{{ $val->employer_name }} - {{ $val->title }}</option>
                                @endforeach
                            </select>
            			</div>
                    </div>

                    <div class="form-group" data-name="day">
            			<label class="col-md-3 control-label">Day of week</label>
            			<div class="col-md-8">
                            <select class="form-control" name="day">
                                <option value="0">choose please...</option>
                                @foreach($week->days_of_week as $val)
                                    <option value="{{ $val->sql }}">{{ $val->view_d }} - {{ $val->view_md }}</option>
                                @endforeach
                            </select>
            			</div>
                    </div>

                    <div class="form-group" data-name="hours">
            			<label class="col-md-3 control-label">&nbsp;</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <select class="form-control" name="hours">
                                    @foreach($week->hours as $val)
                                        <option value="{{ $val }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-addon">hr</span>
                            </div>
            			</div>
            			<div class="col-md-4">
                            <div class="input-group">
                                <select class="form-control" name="minutes">
                                    @foreach($week->minutes as $val)
                                        <option value="{{ $val }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                                <span class="input-group-addon">min</span>
                            </div>
            			</div>
                    </div>
                    
                    <div class="form-group" data-name="description">
            			<label class="col-md-3 control-label">Description</label>
            			<div class="col-md-8">
                            <textarea class="form-control" name="description" rows="10"></textarea>
            			</div>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary send-req-form">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endif


{{ JsCss::addJs('reports.js') }}
@endsection

