@extends('app')

@section('content')
    <div class="page-header">
        <h3>Transaction History</h3>
    </div>

    <div class="row">
        <div  class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <li class="{{ $billing_active or '' }}"><a href="{{ routeClear('reports') }}/transaction-history/billing">Billing</a></li>
                <li class="{{ $earning_active or '' }}"><a href="{{ routeClear('reports') }}/transaction-history/earning">Earning</a></li>
            </ul>
        </div>

        <div class="col-md-10">
            <div class="row well well-sm">
	            <form class="form-inline" method="GET" action="" enctype="multipart/form-data" >

                    <div class="form-group" data-name="date_from" style="">
            			<label>From</label>
                        <div class="bfh-datepicker" data-input="form-control" data-format="y-m-d" data-name="date_from" data-date="today"></div>
                    </div>

                    <div class="form-group" data-name="date_to" style="padding-left: 5px;">
            			<label>To</label>
                        <div class="bfh-datepicker" data-input="form-control" data-format="y-m-d" data-name="date_to" data-date="today"></div>
                    </div>
 
                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Go</button>
                    </div>

                </form>
                
            </div>

            @yield('transaction_history_content')
        </div>


    </div>



@endsection
