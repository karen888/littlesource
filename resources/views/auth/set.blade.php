@extends('new-app')

@section('content')
    <div id="password-set"></div>
@endsection

@push('scripts')
    <script src="{{elixir('js/set.js')}}"></script>
@endpush
