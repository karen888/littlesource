@extends('app')

@section('content')

    <div class="container" id="registr-form" v-cloak>
        <input type="hidden" v-model="role" value="{{ $role }}">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="row form-select">
                    <div class="col-sm-2 select-text">Create Account</div>
                    <div class="col-xs-12 col-sm-8 buttons-row">
                        @if(App::environment() == "staging")
                        <button class="btn btn-parent" :class="role != 'caregiver' ? 'btn-primary present' : ''" onclick="location.href = '/auth/register?role=parent';">For Parents</button>
                        @else
                        <button class="btn btn-parent" :class="role != 'caregiver' ? 'btn-primary present' : ''" onclick="location.href = 'http://prelaunch.littleones.com.au/clients/';">For Parents</button>
                        @endif
                        <button class="btn btn-care" :class="role == 'caregiver' ? 'btn-default present' : ''" @click="setRole('caregiver')">For Caregivers</button>
                    </div>
                </div>

                <div class="create-account-form-box tab-parent">
                    @if(App::environment() == "staging")
                    <h4 :class="role == 'caregiver' ? 'hidden' : ''">Create a free parents account</h4>
                    @endif
                    <h4 :class="role != 'caregiver' ? 'hidden' : ''">Create a free caregiver account</h4>

                    <form autocomplete="on" id="registerForm" style="max-width: 400px;
margin: 31px auto 23px;
width: 100%;
padding: 10px;
" class="form-horizontal" method="POST" action="/auth/register" onsubmit="return false;">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="alert alert-success" v-show="handle"> @{{ handle }}</div>

                        <div class="first_step" v-show="step == 'first'">
                            <div class="form-group" :class="[first_name.errors.message ? 'error' : '' ]">
                                <div class="field-fname input-placeholder field-fname-show" style=""><span>First name</span><div class="back"></div></div>
                                <input type="text" class="form-control" name="fname" v-model="first_name.value" @keyup="first_name.errors.message = ''" placeholder="First name" >
                                <div class="error-message" v-if="first_name.errors.message"> @{{ first_name.errors.message }} </div>
                            </div>

                            <div class="form-group" :class="[last_name.errors.message ? 'error' : '' ]">
                                <div class="field-lname input-placeholder field-lname-show" style=""><span>Last name</span><div class="back"></div></div>
                                <input type="text" class="form-control" name="lname" v-model="last_name.value" @keyup="last_name.errors.message = ''" placeholder="Last name" >
                                <div class="error-message" v-if="last_name.errors.message"> @{{ last_name.errors.message }} </div>
                            </div>

                            <div class="form-group" :class="[email.errors.message ? 'error' : '' ]">
                                <div class="field-email input-placeholder field-email-show" style=""><span>Email</span><div class="back"></div></div>
                                <input type="email" class="form-control" name="email" v-model="email.value" @keyup="email.errors.message = ''" placeholder="Email">
                                <div class="error-message" v-if="email.errors.message"> @{{ email.errors.message }} </div>
                            </div>

                            <div class="form-group" :class="[password.errors.message ? 'error' : '' ]">
                                <div class="field-password input-placeholder field-password-show" style=""><span>Password</span><div class="back"></div></div>
                                <input type="password" class="form-control" name="password" v-model="password.value" @keyup="password.errors.message = ''" placeholder="Password">
                                <div class="error-message" v-if="password.errors.message"> @{{ password.errors.message }} </div>
                            </div>

                            <div class="form-group" :class="[password_confirmation.errors.message ? 'error' : '' ]">
                                <div class="field-password_confirmation input-placeholder field-password_confirmation-show" style=""><span>Confirm Password</span><div class="back"></div></div>
                                <input type="password" class="form-control" name="password_confirmation" v-model="password_confirmation.value" @keyup="password_confirmation.errors.message = ''" placeholder="Confirm Password">
                                <div class="error-message" v-if="password_confirmation.errors.message"> @{{ password_confirmation.errors.message }} </div>
                            </div>
                        </div>

                        <div class="second_step" v-show="step == 'second'">
                            <div v-if="role != 'caregiver'">
                                <div class="form-group" :class="[security_questions.errors.message ? 'error' : '' ]">
                                    <div v-if="security_questions.value" class="field-security_questions_answer input-placeholder field-security_questions-show" style="display: block !important;"><span>Security Question Answer</span><div class="back"></div></div>

                                    <div class="full-width">
                                        <custom-select
                                                :options="{{ json_encode($security_questions) }}"
                                                @click="security_questions.errors.message = ''"
                                                :selected.sync="security_questions.value"
                                                name="security question">
                                        </custom-select>
                                        <div class="error-message" v-if="security_questions.errors.message"> @{{ security_questions.errors.message }} </div>
                                    </div>
                                </div>
                                <div class="form-group" v-if="security_questions.value" :class="[security_questions_answer.errors.message ? 'error' : '' ]">
                                    <div class="field-security_questions_answer input-placeholder field-security_questions_answer-show" style=""><span>Security Question Answer</span><div class="back"></div></div>
                                    <input type="text" class="form-control" v-model="security_questions_answer.value" name="security_questions_answer" @keyup="security_questions_answer.errors.message = ''" placeholder="Security Question Answer">
                                    <div class="error-message" v-if="security_questions_answer.errors.message"> @{{ security_questions_answer.errors.message }} </div>
                                </div>

                                <div class="form-group" :class="[post_code.errors.message ? 'error' : '' ]">
                                    <div class="field-post_code input-placeholder field-post_code-show" style=""><span>Post code</span><div class="back"></div></div>
                                    <input type="text" class="form-control" name="post_code" v-model="post_code.value" @keyup="post_code.errors.message = ''" placeholder="Post code">
                                    <div class="error-message" v-if="post_code.errors.message"> @{{ post_code.errors.message }} </div>
                                </div>

                                <div class="form-group" :class="[hourly_rate.errors.message ? 'error' : '' ]" v-show="role == 'caregiver'">
                                    <div class="field-hourly_rate input-placeholder field-hourly_rate-show" style=""><span>Hourly Rate</span><div class="back"></div></div>
                                    <input type="text" class="form-control" name="hourly_rate" v-model="hourly_rate.value" @keyup="setRate()" placeholder="Hourly Rate">
                                    <div class="error-message" v-if="hourly_rate.errors.message"> @{{ hourly_rate.errors.message }} </div>
                                </div>
                            </div>
                            <div v-if="role == 'caregiver'">
                                <div class="form-group" :class="[post_code.errors.message ? 'error' : '' ]">
                                    <div class="field-post_code input-placeholder field-post_code-show" style=""><span>Post code</span><div class="back"></div></div>
                                    <input type="text" class="form-control" name="post_code" v-model="post_code.value" @keyup="post_code.errors.message = ''" placeholder="Post code">
                                    <div class="error-message" v-if="post_code.errors.message"> @{{ post_code.errors.message }} </div>
                                </div>

                                <div class="form-group" :class="[gender.errors.message ? 'error' : '' ]">
                                    <div v-if="gender.value" class="field-post_code input-placeholder field-gender-show" style="display: block !important;"><span>Select gender</span><div class="back"></div></div>
                                    <div class="full-width">
                                        <custom-select
                                                :options="genderValue"
                                                @click="this.gender.errors.message = ''"
                                                :selected.sync="gender.value"
                                                name="gender">
                                        </custom-select>
                                        <div class="error-message" v-if="gender.errors.message"> @{{ gender.errors.message }} </div>
                                    </div>
                                </div>

                                <div class="form-group" :class="[describe.errors.message ? 'error' : '' ]" v-if="gender.value == 3">
                                    <div class="field-post_code input-placeholder field-describe-show" style=""><span>Describe your gender</span><div class="back"></div></div>
                                    <input type="text" class="form-control" name="describe" v-model="describe.value" @keyup="describe.errors.message = ''" placeholder="Describe your gender">
                                    <div class="error-message" v-if="describe.errors.message"> @{{ describe.errors.message }} </div>
                                </div>


                                <div class="form-group" :class="[care_type.errors.message ? 'error' : '' ]">
                                    <div v-if="care_type.value" class="field-post_code input-placeholder field-loking-show" style="display: block !important;"><span>I'm looking for</span><div class="back"></div></div>
                                    <div class="full-width">
                                        <custom-select
                                                :options="{{ json_encode($care_type) }}"
                                                @click="care_type.errors.message = ''"
                                                :selected.sync="care_type.value"
                                                name="I'm looking for">
                                        </custom-select>
                                        <div class="error-message" v-if="care_type.errors.message"> @{{ care_type.errors.message }} </div>
                                    </div>
                                </div>
                            </div>                                
                        </div>

                        <button type="button" class="btn btn-default register" @click="registrAction()">@{{ getStepText }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection