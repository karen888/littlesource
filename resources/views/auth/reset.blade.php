@extends('new-app')

@section('content')
	<div id="password-reset"></div>
@endsection

@push('scripts')
@if (isset($token))
	<script>
        let token = '{{ $token }}';
	</script>
@endif
<script src="{{elixir('js/password-reset.js')}}"></script>
@endpush
