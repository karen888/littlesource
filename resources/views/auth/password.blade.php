@extends('new-app')

@section('content')
    <div id="password-email"></div>
@endsection

@push('scripts')
@if (session('handle'))
    <script>
        let handle = '{{ session('handle') }}';
    </script>
@endif
<script src="{{elixir('js/password-email.js')}}"></script>
@endpush
