@extends('new-app')

@section('content')
    <div id="login"></div>

@endsection

@push('scripts')
@if (session('handle'))
    <script>
        let handle = '{{ session('handle') }}';
    </script>
@endif
    <script src="{{elixir('js/login.js')}}"></script>
@endpush
