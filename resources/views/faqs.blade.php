@extends('app')

@section('page-title') - FAQs @endsection

@section('content')
    <style>
        div.faqs {
            margin: 30px;
        }
        div.faqs h2 {
            color: #3d3d44;
            font-size: 26pt;
            text-align: center;
        }
        div.faqs h3 {
            color: #6b6b6b;
            font-size: 22pt;
        }
        div.faqs h4 {

            font-size: 18pt;
        }
        div.faqs p , div.faqs ul{

            text-align: justify;
            color: #333;
        }
    </style>
    <div class="container">
        <div class="row faqs" style="margin-top: 60px">
            <h2>Little Ones FAQS</h2>
            <h3>General Information</h3>
            <h4>Is our website secure?</h4>
            <p>Our site is hosted on a secure server and uses industry standard SSL (encryption) for all communication between you and our server. This means your personal information, uploaded identification, and financial information are secure.</p>
            <p>For caregivers, when you verify your account you are required to upload identification documents. We store those files for as long as required to verify your identity and then those files are securely destroyed.</p>
            <p>For parents, we have a direct merchant facility with the bank to process credit cards. This means we've passed the bank's stringent security checks on both our company, the company director, and our website. As part of this, we do not store your credit card information at any time, even for recurring payments. The bank stores and handles all transactions on our behalf.</p>
            <p>We also use one of the industry's best security platforms called “CloudFlare". CloudFlare provides a whole range of anti-hacking security measures as well as ensuring the speed and up-time of the server is as good as possible.</p>


            <h3 id="profile-verification-caregivers">Information for Caregivers</h3>

            <h4>1. How is my profile verified?</h4>
            <p>Once you submit your profile to us for review, it will go through a verification process which includes, but is not limited to, an ID check of your government issued documents, the validity of your right to work with children and your qualifications. It’s crucial that you keep your profile up to date to avoid your profile losing its verified status.</p>

            <h4>2. Can I still use the site if my profile is not verified?</h4>
            <p>Yes. You can search for jobs and update your profile. However, you will not be able to apply for, or accept any jobs until your profile is verified.</p>

            <h4>3. Is there a fee to be listed as a caregiver?</h4>
            <p>Please note all bookings including any bonuses or reimbursements paid to you will incur a service fee of 11% inclusive of GST. This fee is deducted from the final payment due to you post completion of a job.</p>
            <p>All payments are exchanged via Little Ones to ensure a smooth and secure process for you and the parent. Little Ones site is hosted on a secure server and uses industry standard SSL (encryption) for all communication between you and our server. This means your information and payments are safe and secure. Disputes rarely happen, but in the event that there is a payment dispute the Little Ones support team is here to help.</p>

            <h4>4. Do I get to set my own rates or are they pre-determined?</h4>
            <p>You are completely free to set your own rate of pay. We encourage you to base this on your qualifications, years of experience and the amount of ‘trusted’ ratings you receive on Little Ones. We know these things are all important to parents and many of them will be willing to pay more for a caregiver they can see easily from the outset that they can trust.</p>

            <h4>5. What are the qualifications I need to be listed as a caregiver?</h4>
            <p>We only list certified caregivers on Little Ones. Please click here to view our requirements for <a href="{{ route('pom-parents') }}">caregiver qualifications</a>.</p>

            <h4>6. How will I be paid?</h4>
            <p>All payments will be made via the Little Ones secure payment portal from users to carers, you can then request to have your available funds transferred to your nominated bank account.</p>

            <h4>7. How do I get notified of a potential job?</h4>
            <p>We will notify you by email when new jobs become available in your area so you can apply. It's up to the parent to choose the caregiver they feel most suits their needs. </p>

            <h4>8. What If I accepted a job but can no longer attend a job?</h4>
            <p>Please notify us ASAP that you can no longer attend a job and we will re-open that job as available for other carers. All you have to do is select “cancel job” and select the reason why you are canceling.</p>


            <h3>Parents / Childcare centres</h3>

            <h4>1. How can I trust that Little Ones is safe to use?</h4>
            <p>The safety of your little ones is our number one priority. We inforce a strict verification process for all of our caregivers. All of our caregivers have the same legal requirements and qualifications that are required for caregivers in childcare centres. You can also read reviews and feedback on all of our caregivers, as well as check out their past work experience and references. Please click here for detailed information about our <a href="{{ route('pom-parents') }}">caregiver verification</a> process.</p>

            <h4>2. Is there a fee to request a carer?</h4>
            <p>There is no fee to request a caregiver via Little Ones. This service is completely free for parents to use.</p>
            <p>A service fee of 11% including GST for all bookings including any bonuses or reimbursements paid is taken out of the payment you submit to the caregiver.</p>
            <p>All payments are exchanged via Little Ones to ensure a smooth and secure process for you and the caregiver. Little Ones site is hosted on a secure server and uses industry standard SSL (encryption) for all communication between you and our server. This means your information and payments are safe and secure. Disputes rarely happen, but in the event that there is a payment dispute the Little Ones support team is here to help.</p>

            <h4>3. Do I pay the carer directly or through Little Ones?</h4>
            <p>All payments are exchanged via Little Ones to ensure a smooth and secure process for you and the caregiver. Little Ones uses a safe and secure portal that always keeps your details private.</p>

            <h4>4. What if I want to extend my hours that I’ve requested for a carer after the job has been accepted/started?</h4>
            <p>If you have requested your carer to stay on past the agreed hours originally arranged, simply log back into your profile, extend the hours that the carer stayed/has been requested to stay and your payment will be adjusted accordingly. The caregiver will be sent an SMS notification to confirm whether or not they are able to extend the hours, as per your request.</p>

            <h4>5. What if the carer doesn’t show up?</h4>
            <p>Please contact us immediately and we will organise alternate carer ASAP and/or a refund.</p>
        </div>
    </div>
@endsection
