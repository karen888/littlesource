<html>
<head>

    <title>LittleOnes - Upload a file</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.css?time={{time()}}">--}}
    {{--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/basic.min.css">--}}

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.1.1/min/dropzone.min.js?time={{time()}}"></script>--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        html,body {
            font-family:Arial, Tahoma, Verdana, sans-serif;
            background: #f2f2f2;
        }
        body {
            padding: 30px;
            width: 100%;
            background: #f2f2f2;
        }
        .container {
            background: #f2f2f2;
        }

    </style>

</head>
<body>

<div class="container" style="padding-top: 15px; border: 1px solid #d0cdd0;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">
<form action="/compatibility/upload" method="post" enctype="multipart/form-data" onsubmit="formSubmit()">

    @if(Session::has('message'))
        <div class="alert alert-danger">
            <small>{{Session::get('message')}}</small>
        </div>
    @endif
    <br/>

    <div class="col-md-3">
        <div class="form-group">
            <input name="file" id="file" type="file" accept="" style="display: none" onchange="fileChange()"/>

            <button id="selectBtn" type="button" onclick="$('#file').trigger('click')" class="btn btn-sm btn-info">Select document</button>
        </div>
        @if(request('avatar', '0') == '1')
        <input type="hidden" name="avatar" value="1">
        @endif
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <p id="fileName" style=" position:relative;top:4px;font-style: italic;color:#444;"></p>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <input type="submit" id="submitBtn" value="Upload" class="btn btn-sm btn-success">
        </div>
    </div>
</form>

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script>

    function fileChange(){
        var fullPath = document.getElementById('file').value;
        if (fullPath) {
            var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
            var filename = fullPath.substring(startIndex);
            if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                filename = filename.substring(1);
            }
            $('#fileName').html(filename);
        }
    }

    function formSubmit() {
        $('#submitBtn').attr('disabled', true);
        $('#selectBtn').attr('disabled', true);
    }

//        (function(w){
//            var loUploader = $('#loupload').dropzone({
//                url: '/compatibility/upload',
//                paramName: "file",
//                debug: true,
////                forceFallback: true,
//                maxFiles: 1,
//                success: function(d, response) {
//
//                    setTimeout(function() {
//                        if(typeof w.uploadCallback !== 'undefined') {
//                            w.uploadCallback(response);
//                        }
//                    }, 500);
//                },
//                error: function(err, message) {
//                    alert(message);
//                },
//
//            });
//        })(window);




</script>
</body>
</html>