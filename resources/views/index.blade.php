
@extends('app')

@section('content')

    <div class="home-content">
        <div class="home-lil-bit-about">
            <div class="home-lil-bit-about-inner">
                <p><img  src="/img/logo.png" width="180px" alt="Little ones"></p>
                <p class="titles">Ever wished finding a babysitter was as easy as ordering an Uber?</p>
                <p class="moretext">Well, now it is! Certified caregivers are just a click away on Little Ones!</p>
                <a href="/pre-register" class="btn btn-started">Get Started</a>
            </div>
        </div><!-- home-lil-bit-about -->

        <div class="how-does-it-work">
            <div class=" container">
                <div class="row">
                    <div class="how-does-it-work-inner ">

                    <div class="text-box-net pull-left col-sm-4">
                        <span class="h2">
                            How Does it Work
                        </span>
                                <p>We’re definitely not your average babysitting directory! Little Ones doesn’t just connect you with long-term care options, our certified caregivers can pop over for even just an hour or two — whenever you need a little ‘me time’! It’s as easy as ordering an Uber!</p>
                    </div>


                    </div>
                </div>
            </div>
        </div><!-- how-does-it-work -->

        <div class="verf-network ">
                <div class=" container">

            <div class="verf-network-inner row">
                <div class="text-box-net pull-left col-sm-4">
                    <span class="h2">
                        Verified Network
                    </span>
                            <p>We only list the highest standard of caregivers with a very <a style="color: #73c5bc; font-weight: bold" href="{{url('/caregivers-verification')}}">strict verification process</a>, so parents can have peace of mind that their children are being left in qualified, professional hands.</p>
                </div>
                <div class="col-sm-8">
                    <div class="">
                                <div class="row">
                        <ul class="net-items ">

                            <li class="col-sm-4">
                                <span class="ava-box">
                                    <img src="img/temp/home/ava3.png" alt="">
                                </span>
                                <b>Amy Leigh</b>
                                <i>5 years of experience</i>

                                <em>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </em>
                            </li>
                            <li class="col-sm-4">
                                <span class="ava-box">
                                    <img src="img/temp/home/ava2.png" alt="">
                                </span>
                                <b>Lucinda Lihou</b>
                                <i>2 years of experience</i>

                                <em>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </em>
                            </li>
                            <li class="col-sm-4">
                                <span class="ava-box">
                                    <img src="img/temp/home/ava.png" alt="">
                                </span>
                                <b>Audrey De Mestre</b>
                                <i>7 years of experience</i>

                                <em>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </em>
                            </li>
                        </ul>
                    </div>
                    </div>
                </div>
            </div>

        </div><!-- verf-network -->
        </div>

        <div class="woman-children-box">
            <div class=" container">
                <div class="row">
                    <div class="woman-children-box-inner ">

                                <div class="text-box  col-sm-4">
                                    <h3>Easy. Fast. Safe.</h3>
                                    <p>Whether you're looking for a full-time nanny, an occasional babysitter or just some last-minute help, our qualified caregivers are ready to provide the assistance your family needs!</p>
                                </div>


                    </div>
                </div>
            </div>
        </div><!-- woman-children-box -->

        <div class="get-started-box">
            <div class="container">
                <div class="row">
                    <div>

                        <span class="h2">Getting Started</span>
                        <p>Whether you just need an hour of ‘me time’, a regular babysitter, <br>
                            or a long-term nanny - we’ll help to identify the best options for you!<br>
                            Everything you need is just a click away on your computer or mobile device.</p>

                        <a href="/pre-register" class="btn btn-started btn-lg">Get Started</a>
                    </div>
                </div>
            </div>

        </div><!-- get-started-box -->

        <div class="user-comment-bubble">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                            <!--<div class="user-comment-bubble-inner ">

                                        <div class="user-name">
                                            <span class="h3">
                                                Meet Viviana
                                            </span>
                                        </div>

                                        <div class="user-ava">
                                            <img src="img/temp/ava-2.png" alt="ava">
                                        </div>

                                        <div class="bq-text">
                                            <p>Little Ones was founded by Viviana Rossi, a mother of three children, who knows first hand the daily struggle of juggling family, work and social life balance. Viv developed Little Ones to help alleviate some stress from the lives of parents.</p>
                                            {{--<p>I’m passionate about <br> children and improving <br> care for kids and teens <br> in Australia.</p>--}}
                                            <p class="author-signature">Viviana, <br> Founder of Little Ones</p>
                                        </div>

                            </div>-->
                        <div style='margin: 43px 0px 43px 0px;text-align: center;'><iframe
                                    style="max-width: 560px;
max-height: 320px;
width: 100%;
height: 100%;
display: block;
position: relative;
margin: 0 auto;
min-height: 316px;
"
                                    src="https://www.youtube.com/embed/_XgQ_iIIp8Q" frameborder="0" allowfullscreen></iframe></div>
                    </div>

                </div>
            </div>
        </div><!-- user-comment-bubble -->
    </div><!--home-content -->

@endsection

