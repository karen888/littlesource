<div class="customselect v2">
    <select name="{{ $select['name'] }}" id="{{ $select['name'] }}" {{ array_key_exists('multiple', $select) && $select['multiple'] ? 'multiple' : '' }}>
        @if(array_key_exists('has_defaults', $select) && !$select['has_defaults'])
            <option value="-1">No Preference</option>
        @endif
        @foreach($select['options'] as $option)
            <option value="{{ $option['value'] }}">{{ $option['name'] }}</option>
        @endforeach
    </select>
    <div class="trigger">
        <div class="text_outer"><div class="text trn">
            {{-- */$i=0;/* --}} {{-- for multiple counter --}}
            {{-- */$count = count(array_filter($select['options']->toArray(), function($option) { return $option['is_selected']; }))/* --}}
            @foreach($select['options'] as $option)
                @if($option['is_selected'])
                    @if(array_key_exists('multiple', $select) && $select['multiple'] && ++$i != $count )
                         {{ str_limit($option['name'], 3, '')  }},
                    @elseif(array_key_exists('multiple', $select) && $select['multiple'] && $count > 1)
                        {{ str_limit($option['name'], 3, '')  }}
                    @else
                        {{ $option['name'] }}
                    @endif
                @endif
            @endforeach
            @if(array_key_exists('has_defaults', $select) && !$select['has_defaults'])
                No Preference
            @endif
        </div></div>
    </div>
    <div class="list">
        @if(array_key_exists('has_defaults', $select) && !$select['has_defaults'])
            <div class="option active" data-value="-1" data-selected="1"><span class="icon icon-status"></span><span class="option-name">No Preference</span></div>
        @endif
        @foreach($select['options'] as $option)
            <div class="option {{ $option['is_selected'] ? 'active' : '' }}"
            data-value="{{ $option['value'] }}"
            data-selected="{{ $option['is_selected'] ? 1 : 0 }}">
                @if(array_key_exists('multiple', $select) && $select['multiple'])
                    <span class="icon icon-status"></span>
                @endif
                <span class="option-name">{{ $option['name'] }}</span>
            </div>
        @endforeach
    </div>
</div>