{{--
OPTIONS (*required):
    $select: *name, id(name), has_defaults(false), type(dropdown|list|inline), multiple(false)
    $options[]: *value, is_selected(false), *name
--}}
{{--<select class="custom-select" name="{{ $select['name'] }}" id="{{ $select['id'] or $select['name'] }}"--}}
    {{--data-has-defaults="{{ $select['has_defaults'] or false }}" data-select-type="{{ $select['type'] or 'dropdown' }}" {{ isset($select['multiple']) && $select['multiple'] ? 'multiple' : '' }}>--}}
    {{--@foreach($options as $option)--}}
        {{--<option value="{{ $option['value'] }}" {{ isset($option['is_selected']) && $option['is_selected'] ? 'selected' : '' }}>{{ $option['name'] }}</option>--}}
    {{--@endforeach--}}
{{--</select>--}}

<custom-select class=""
    :options="{{ json_encode($options) }}"
    {{--@click="category.errors.message = ''"--}}
    {{--:selected.sync="category.selected"--}}
    name="category"
    {{--@selected-text="categorySelectedText">--}}
</custom-select>


{{--<div class="trigger">xxx</div>--}}

{{--<div class="list">--}}
    {{--<div class="option active" data-value="-1" data-selected="1"><span class="icon icon-status"></span><span class="option-name">No Preference</span></div>--}}
    {{--<div class="option active" data-value="" data-selected="">--}}
        {{--<span class="icon icon-status"></span>--}}{{--if multiple--}}
        {{--<span class="option-name">option.name</span>--}}
    {{--</div>--}}
{{--</div>--}}
