@extends('app')

@section('content')

    <section class="make-your-choice-box">
        <header>
            <h1>How it works</h1>
    
            <div class="buttons-row">
                <button data-id="parents" class="btn btn-primary btn-md present">for parents</button>
                <button data-id="caregiver" class="btn btn-green-border btn-md">for caregivers</button>
            </div>
        </header>

        <div class="tab-registration" id="parents">
            <div class="make-your-choice-box__item">
                <div class="make-your-choice-box__item-in">
                    <hgroup>
                        <h3>Create an account</h3>
                        <h6>Take a minute to set up your account and get started!</h6>
                    </hgroup>
        
                    <i class="art-icon map-case-blue"></i>
                </div>
            </div><!-- make-your-choice-box__item -->
            <div class="make-your-choice-box__item">
                <div class="make-your-choice-box__item-in">
                    <hgroup>
                        <h3>Search for sitters <br> OR post a job</h3>
                        <h6>You can browse our qualified caregivers, search by dates <br> or time available, or post your own job.</h6>
                    </hgroup>
        
                    <i class="art-icon people-blue"></i>
                </div>
            </div><!-- make-your-choice-box__item -->
            <div class="make-your-choice-box__item">
                <div class="make-your-choice-box__item-in">
                    <hgroup>
                        <h3>Book your sitter</h3>
                        <h6>Once you’ve found a match for your family, go ahead and <br> set a date and enjoy your time away.</h6>
                    </hgroup>
        
                    <i class="art-icon smile-check-blue"></i>
                </div>
            </div><!-- make-your-choice-box__item -->
            <div class="make-your-choice-box__item">
                <div class="make-your-choice-box__item-in">
                    <hgroup>
                        <h3>Pay online or cash</h3>
                        <h6>We make it easy to pay online or of course you can pay <br> your caregiver in person.</h6>
                    </hgroup>
        
                    <i class="art-icon wallet-blue"></i>
                </div>
            </div><!-- make-your-choice-box__item -->
        </div>
        
        <div class="tab-registration" id="caregiver" style="display:none;">
            <div class="make-your-choice-box__item">
                <div class="make-your-choice-box__item-in">
                    <hgroup>
                        <h3>Create an account</h3>
                        <h6>Take a minute to set up your account and get started!</h6>
                    </hgroup>
        
                    <i class="art-icon map-case-green"></i>
                </div>
            </div><!-- make-your-choice-box__item -->
            <div class="make-your-choice-box__item">
                <div class="make-your-choice-box__item-in">
                    <hgroup>
                        <h3>Set up a profile & <br> apply to jobs</h3>
                        <h6>Share a bit about yourself and your experience, post <br> your availability, and start applying to positions.</h6>
                    </hgroup>
        
                    <i class="art-icon portfolio-green"></i>
                </div>
            </div><!-- make-your-choice-box__item -->
            <div class="make-your-choice-box__item">
                <div class="make-your-choice-box__item-in">
                    <hgroup>
                        <h3>Interview / Accept Job</h3>
                        <h6>Chat with potential families to see if you're the right fit for <br> each other.</h6>
                    </hgroup>
        
                    <i class="art-icon chats-green"></i>
                </div>
            </div><!-- make-your-choice-box__item -->
            <div class="make-your-choice-box__item">
                <div class="make-your-choice-box__item-in">
                    <hgroup>
                        <h3>Get paid</h3>
                        <h6>We make it easy to get paid online or of course you can <br> be paid in person. </h6>
                    </hgroup>
        
                    <i class="art-icon dollar-green"></i>
                </div>
            </div><!-- make-your-choice-box__item -->
        </div>

        <footer>
            <strong>
                It's easy! Join Little Ones and find a job now!
            </strong>
    
            <button class="btn btn-primary btn-lg">Sign Up as Parent</button>
    
        </footer>
    </section> 
<script>
$(document).ready(function(){
	
	$('.buttons-row button').click(function(){
		
		$(".tab-registration").css("display", "none");
		
		var tab_id = $(this).attr('data-id');
		$('#'+tab_id).fadeIn();
		
		$('.buttons-row button').removeClass('btn-primary');
		$('.buttons-row button').removeClass('btn-default');
		
		if(!$(this).hasClass('present')){
			$('.buttons-row button').removeClass('present');
			$(this).addClass('present');
			
			if(tab_id == 'parents'){
				$(this).addClass('btn-primary');
				$('.make-your-choice-box footer').find('button').attr('class','btn btn-primary btn-lg');
				$('.make-your-choice-box footer').find('button').text('Sign Up as Parent');
			}
			else{
				$(this).addClass('btn-default');
				$('.make-your-choice-box footer').find('button').attr('class','btn btn-default btn-lg');
				$('.make-your-choice-box footer').find('button').text('Become a Caregiver');
			}
		}
	});
});
</script>


@endsection
