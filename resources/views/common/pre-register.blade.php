@extends('app')

@section('content')
<div class="container">
<div class="row">
    <div class="col-md-12">

        <div class="get-started-main-box">
    
        <h1>Get started</h1>
    
        <h4>Tell us what you are looking for.</h4>
        
        <div class="selection-of-roles-box">

            <?php // TODO: uncomment when parent is allowed ?>

            <div class="parents-role">
                <div>
                    <div>
                        <h4>I want to hire a caregiver</h4>
                        <h6>Find, interview, hire and <br> pay a caregiver.</h6>

                        @if(App::environment() == "staging")
                        <a href="/auth/register?role=parent" class="btn btn-primary btn-text">Hire</a>
                        @else
                        <a href="http://prelaunch.littleones.com.au/clients/" class="btn btn-primary btn-text">Hire</a>
                        @endif
                    </div>
                </div>
            </div>
    
    
            <div class="caregiver-role" style="/*margin: 0 auto; float: none;*/">
                <div>
                    <div>
                        <h4>I'm a caregiver looking for work</h4>
                        <h6>Create a profile, apply to positions, <br> and earn money.</h6>
    
                        <a href="/auth/register?role=caregiver" class="btn btn-default btn-text">Work</a>
                    </div>
                </div>
            </div>
    
            {{--<span class="h4 help-text">or</span>--}}
    
        </div><!-- selection-of-roles-box -->
    
    </div><!-- get-started-main-box -->
    
    <span class="artFont artFont-tab"></span>
                
    </div>
</div>
</div>
@endsection
