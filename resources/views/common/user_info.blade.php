@extends('app')
 
@section('content')
<div class="row padd-top">
    @if (!empty($user_info))
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-2">
                <div class="thumbnail">
                    <img src="{{ $user_info->user_photo_100 }}">
                </div>
            </div>
            <div class="col-md-10">
                <div class="row">
                    <h4 class="pull-left text-primary" style="margin-top:0px;">{{ $user_info->name }}</h4>
                    @if ($user_info->user_rate != '0.00')
                        <h4 class="pull-right" style="margin-top:0px;">${{ $user_info->user_rate }} <span class="text-muted">/ hr</span></h4>
                    @endif
                    <div class="clearfix"></div>
                </div>
                <div class="row text-muted"><strong>{{ $user_info->user_title }}</strong></div>
                <div class="row" style="margin-top: 15px;">
                    @foreach($user_info->skills as $val)
                        <a href="javascript:void(0);" class="btn  btn-default btn-xs">{{ $val->skill_name }}</a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <h4>Overview</h4>
            <hr>
            <p>{{ $user_info->user_overview }}</p>
        </div>
        <div class="row">
            <h4>Recent Work History & Feedback</h4>
            @if (!empty($user_info->history->progress))
            <hr>
            <a class="history-progress collapsed" data-toggle="collapse" href="#jobsProgress" aria-expanded="false" aria-controls="jobsProgress">Jobs in progress</a>
            <div class="collapse" id="jobsProgress">
            <table class="table">
                <tbody>
                    @foreach($user_info->history->progress as $val)
                    <tr>
                        <td>
                            <div>
                                <a href="{{ routeClear('job_link') }}/{{ $val->path }}">{{ $val->title }}</a>
                            </div>
                            <div class="text-muted">Job in progress</div>
                        </td>
                        <td class="col-md-3 text-right">
                            {{ $val->created_at }} - Present
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            @endif
            <table class="table">
                <tbody>
                    @foreach($user_info->history->finished as $val)
                    <tr>
                        <td>
                            <div>
                                <a href="{{ routeClear('job_link') }}/{{ $val->path }}">{{ $val->title }}</a>
                            </div>
                            <div class="">
                                @if (!empty($val->feedback))
                                    {{ $val->feedback->feedback_text }}
                                @else
                                    <span class="text-muted">No feedback given</span>
                                @endif
                            </div>
                        </td>
                        <td class="col-md-3 text-right">
                            @if (!empty($val->feedback))
                            <div style="padding-bottom: 5px;">
                                <div class="feedback-rate-numb pull-right">{{ $val->feedback->feedback_rate }}</div>
                                <div class="feedback-rate-bl pull-right">
                                    <div class="feedback-rate-val" style="width: {{ $val->feedback->feedback_rate_procent }}%">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            @endif
                            <div>{{ $val->created_at }} - {{ $val->finished_at }}</div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4">
        <div class="well">
            <div>
                <strong class="text-muted">Work history</strong>
                <hr class="hr-style">
                <div>
                    <div class="feedback-rate-numb pull-left" style="margin-left: 0px; margin-right: 3px;">{{ $user_info->history->rate }}</div>
                    <div class="feedback-rate-bl pull-left">
                        <div class="feedback-rate-val" style="width: {{ $user_info->history->rate_percent }}%">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div style="margin-top: 5px;">
                    <strong>{{ $user_info->history->jobs_count }}</strong> jobs
                </div>
                <div style="margin-top: 10px;">
                    <strong>{{ $user_info->country_name }}</strong>
                    <br>

                    <span class="text-muted">{{ $user_info->user_city.' '  }}{{ $user_info->timezone_date }}</span>
                </div>


                <br>
                <strong class="text-muted">Availability</strong>
                <hr class="hr-style">
                <strong>
                @if ($user_info->user_available)
                    Available
                @else
                    Not Available
                @endif
                </strong>
            </div>
        </div>
    </div>
    @endif
</div>

@endsection
