@extends('app')

 

@section('content')
    <div class="about-content-page">
        <div class="text-box">
            <div class="text-box-inner">
                <h3>About us</h3>
                <p>Little Ones provides care at a click! We connect parents / guardians with qualified caregivers. Whether you need a quick break just for an hour or longer term care, Little Ones can help you. It's very simple to use, just jump onto Little Ones and you can see which certified caregivers are available in your area, it's just like ordering an Uber! The best part about Little Ones is it's available to you around the clock, not just in business hours, like childcare centres.</p>

                <p>The safety and care of your Little Ones is our top priority, we ensure all of our caregivers are qualified, you can read more about our verification process here! You have access to reviews of each caregiver from other parents and can see each caregivers experience in their Little Ones profile. Rest easy knowing that your Little Ones are in good hands!</p>
            </div>
        </div><!-- text-box -->

        <!--<div class="text-box">
            <div class="text-box-inner">
                <h3>The vision</h3>
                <p>Little Ones is a website (made by a mum!) where you could be assured that the person sent in your home is reliable, honest, and also great with children. I want to find caregivers who want more than just a way to earn a few extra dollars – I want them to see themselves as important contributors to shaping my children’s lives. And as my children grown up, I want caregivers with experience dealing with the challenges of older children and teens.</p>
                <p>Whether it’s relief in a pinch to run errands or a permanent and consistent resource for your family needs, let Little One help you. After all, a happy mother ensures a happy household with happier children.</p>
            </div>
        </div> -->

        <div class="user-reviews-box">
            <div class="user-reviews-box-inner">
                <header>
                    <div class="user-name">
                        <span class="h3">Meet Viviana</span>
                    </div>

                    <div class="user-avatar">
                        <img src="img/temp/ava.png" alt="">
                    </div>
                </header>

                <div class="user-reviews-text-box">
                    <blockquote>
                        <p>I have three little girls who are within three years of each other. Even though I am a stay-at-home-mum, the truth is that focusing on my own needs always comes last. Sound about right? I was burning out and I needed help.</p>
                        <p>I have my children’s grandparents and family around, but they have their own lives and can't always drop everything to care for my children. I needed a different option. This is how Little Ones began. Little Ones aims to alleviate some stress from parents and guardians, by providing an alternate solution to childcare needs.</p>
                    </blockquote>
                    <i class="author-signature">Viviana,<br> Founder of Little Ones</i>
                </div>
            </div>
        </div><!-- user-reviews-box -->
        
    </div><!-- about-content-page -->

@endsection

