@extends('app')

@section('content')
<div class="row">
    @if (!empty($job_info)) 
    <div class="col-md-8">
        <h3>{{ $job_info->title }}</h3>
            
        <div class="row">
            @if ($job_info->fixed)
                <div class="col-md-4">
                    <div class="pull-left">
                        <i class="fa fa-database fa-lg text-muted"></i>
                    </div>
                    <div class="pull-left" style="padding-left: 5px;">
                        <strong>Fixed Price</strong>
                        @if (!empty($job_info->job_end_date))
                            <br>
                            <span class="text-muted text-small">
                                Deliver by {{ $job_info->job_end_date }}
                            </span>
                        @endif
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-offset-4">
                    <i class="fa fa-dollar fa-lg text-muted"></i>&nbsp;<strong>{{ $job_info->job_fixed_price }}</strong>
                </div>
            @else 
                <div class="col-md-4">
                    <div class="pull-left">
                        <i class="fa fa-clock-o fa-lg text-muted"></i>
                    </div>
                    <div class="pull-left" style="padding-left: 5px;">
                        <strong>Hourly Job</strong>
                        <br>
                        <span class="text-muted text-small">
                            As Needed<br>
                            {{ $job_info->job_duration }}
                        </span>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endif
        </div>
        <br>

        <div class="row panel-group">
            <div class="panel panel-default  border-hide">
                <div class="panel-heading">
                <b>Job Description</b>
                </div>
            </div>
        </div>
        <div class="row">
            {{ $job_info->description }}
        </div>
        @if ($job_info->file_name)
        <div class="row" style="margin-top: 12px; font-">
            <small>
                <strong>Attached File</strong>
                <br>
                <a href="{{ routeClear('job_link') }}/{{ $job_info->path }}?file={{ time() }}">{{ $job_info->file_name }}</a>
            </small>
        </div>
        @endif
        <div class="row" style="margin-top: 12px;">
            <small>
                <strong>Skills Required</strong>
                <br>
                @foreach ($job_info->skills as $val)
                    <a href="javascript:void(0);" class="btn btn-default btn-xs">{{ $val->skill_name }}</a>
                @endforeach
            </small>
        </div>
        <div class="row" style="margin-top: 12px;">
            <div class="panel-group" id="applications" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default border-hide">
                    <div class="panel-heading">
                        <a data-toggle="collapse" data-parent="#applications" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><strong>Applications ({{ $applies_list->count }})</strong></a>
                    </div>

                    <div id="collapseOne" class="panel-collapse collapse out" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body border-hide">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Freelancer</th>
                                    <th>Data Applied</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($applies_list->list as $val)
                                    <tr>
                                        <td><a href="{{ routeClear('user_link') }}/{{ $val->user_path }}">{{ $val->name }}</a></td>
                                        <td>{{ $val->created_human }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        @if (!Auth::guest())
        <br>
        <div class="well">

            @if (!empty($ch_apply))
            <div class="text-bold">
                <span>Your proposed terms:</span>
                <div class="text-right">
                    ${{ $ch_apply->job_apply_price }} {{ ($ch_apply->job_apply_fixed_price) ? '' : '/hr' }}
                </div>
            </div>
            <br>
            @endif

            <div>
                <a href="/home/jobs/apply/{{ $job_info->path }}" class="btn btn-primary btn-lg btn-block" {{ ((Auth::user()->id == $job_info->user_id) || (!empty($ch_apply))) ? 'disabled' : '' }}>Apply to this job</a>
            <div>

            @if (!empty($ch_apply) && (empty($ch_apply->offer_status)))
            <hr class="hr-style">
            <div>
	            <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="proc" value="">
                    <input type="hidden" name="job" value="{{ $job_info->path }}">

                    <button type="submit" class="btn btn-success btn-lg btn-block send-req-form" data-confirm="Are you sure?">Withdraw Application</button>
                </form>                
            </div>
            @endif
        </div>
        @endif
    </div>
    @else
        
    @endif
</div>
@endsection
