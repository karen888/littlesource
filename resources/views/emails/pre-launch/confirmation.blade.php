@extends('emails.layouts.email-confirmation', [
    'confirmation_url' => route('pre-launch.confirm', isset($prelaunchUser) ? $prelaunchUser->confirmation_code : 'placeholder'),
    'unsubscribe_link' => route('pre-launch.unsubscribe', isset($prelaunchUser) ? $prelaunchUser->email : 'user@email.com')
])
