======= :: Little Ones :: =======
   Caring for yours since 2005

---------------------------------
Click here to confirm your email:
{{ route('pre-launch.confirm', isset($prelaunchUser) ? $prelaunchUser->confirmation_code : 'placeholder') }}
---------------------------------
   Thank you for subscribing
---------------------------------

Little Ones is helping Australian families find the
best resources to take care of their children.
With verified profiles and testimonials of previous work,
help yourself out by finding your next Caregiver with Little Ones.

---------------------------------
You received this email because you subscribed
for the pre-launch of LittleOnes website.
---------------------------------
Unsubscribe link:
{{ route('pre-launch.unsubscribe', isset($prelaunchUser) ? $prelaunchUser->email : 'user@email.com') }}