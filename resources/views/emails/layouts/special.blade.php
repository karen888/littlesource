<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge" /><!--<![endif]-->
    <meta name="viewport" content="width=device-width" />
    <title> </title>
    <style type="text/css">
        .btn a:hover,
        .footer__links a:hover {
            opacity: 0.8;
        }
        .wrapper .footer__share-button:hover {
            color: #ffffff !important;
            opacity: 0.8;
        }
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }
        .column {
            padding: 0;
            text-align: left;
            vertical-align: top;
        }
        .mso .font-avenir,
        .mso .font-cabin,
        .mso .font-open-sans,
        .mso .font-ubuntu {
            font-family: sans-serif !important;
        }
        .mso .font-bitter,
        .mso .font-merriweather,
        .mso .font-pt-serif {
            font-family: Georgia, serif !important;
        }
        .mso .font-lato,
        .mso .font-roboto {
            font-family: Tahoma, sans-serif !important;
        }
        .mso .font-pt-sans {
            font-family: "Trebuchet MS", sans-serif !important;
        }
        .mso .footer p {
            margin: 0;
        }
        .btn {
            width: 100%;
        }
        @media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
            .fblike {
                background-image: url(https://i7.createsend1.com/static/eb/customise/13-the-blueprint-3/images/fblike@2x.png) !important;
            }
            .tweet {
                background-image: url(https://i8.createsend1.com/static/eb/customise/13-the-blueprint-3/images/tweet@2x.png) !important;
            }
            .linkedinshare {
                background-image: url(https://i9.createsend1.com/static/eb/customise/13-the-blueprint-3/images/lishare@2x.png) !important;
            }
            .forwardtoafriend {
                background-image: url(https://i3.createsend1.com/static/eb/customise/13-the-blueprint-3/images/forward@2x.png) !important;
            }
        }
        @media only screen and (max-width: 620px) {
            .wrapper .size-20 {
                font-size: 17px !important;
                line-height: 26px !important;
            }
            .wrapper .size-24 {
                font-size: 20px !important;
                line-height: 28px !important;
            }
            .wrapper .size-40 {
                font-size: 32px !important;
                line-height: 40px !important;
            }
            .btn a {
                display: block !important;
                font-size: 14px !important;
                line-height: 24px !important;
                padding: 12px 10px !important;
                width: auto !important;
            }
            .btn--shadow a {
                padding: 12px 10px 13px 10px !important;
            }
            .image img {
                height: auto;
                width: 100%;
            }
            .layout,
            .column,
            .header td,
            .footer,
            .footer__left,
            .footer__right,
            .footer__inner {
                width: 320px !important;
            }
            .preheader__snippet,
            .layout__edges {
                display: none !important;
            }
            .layout--full-width {
                width: 100% !important;
            }
            .layout--full-width tbody,
            .layout--full-width tr {
                display: table;
                Margin-left: auto;
                Margin-right: auto;
                width: 320px;
            }
            .column,
            .footer__left,
            .footer__right {
                display: block;
                Float: left;
            }
            .footer__inner {
                text-align: center;
            }
            .footer__right p,
            .footer__share-button {
                display: inline-block;
            }
        }
    </style>

    <!--[if !mso]><!--><style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Cabin:400,700,400italic,700italic|Open+Sans:400italic,700italic,700,400);
    </style><link href="https://fonts.googleapis.com/css?family=Cabin:400,700,400italic,700italic|Open+Sans:400italic,700italic,700,400" rel="stylesheet" type="text/css" /><!--<![endif]--><style type="text/css">
        body,.wrapper{background-color:#f5f7fa}.wrapper h1{color:#44a8c7;font-size:26px;line-height:34px}.wrapper h1{}.wrapper h1{font-family:Cabin,Avenir,sans-serif}.mso .wrapper h1{font-family:sans-serif !important}.wrapper h2{color:#44a8c7;font-size:20px;line-height:28px}.wrapper h3{color:#434547;font-size:16px;line-height:24px}.wrapper a{color:#0da417}@media only screen and (max-width: 620px){.wrapper h1{}.wrapper h1{font-size:22px;line-height:31px}.wrapper h2{}.wrapper h2{font-size:17px;line-height:26px}.wrapper h3{}.wrapper p{}}.column,.column__background td{color:#60666d;font-size:14px;line-height:21px}.column,.column__background td{font-family:"Open Sans",sans-serif}.mso .column,.mso .column__background td{font-family:sans-serif
        !important}.border{background-color:#b1c1d8}.layout--no-gutter.layout--has-border:not(.layout--full-width),.layout--has-gutter.layout--has-border .column__background,.layout--full-width.layout--has-border{border-top:1px solid #b1c1d8;border-bottom:1px solid #b1c1d8}.wrapper blockquote{border-left:4px solid #b1c1d8}.divider{background-color:#b1c1d8}.wrapper .btn a{color:#fff}.wrapper .btn a{font-family:"Open Sans",sans-serif}.mso .wrapper .btn a{font-family:sans-serif !important}.btn--flat a,.btn--shadow a,.btn--depth a{background-color:#0da417}.btn--ghost a{border:1px solid #0da417}.preheader--inline,.footer__left{color:#b9b9b9}.preheader--inline,.footer__left{font-family:"Open Sans",sans-serif}.mso .preheader--inline,.mso .footer__left{font-family:sans-serif !important}.wrapper .preheader--inline a,.wrapper .footer__left
        a{color:#b9b9b9}.wrapper .preheader--inline a:hover,.wrapper .footer__left a:hover{color:#b9b9b9 !important}.header__logo{color:#c3ced9}.header__logo{font-family:Roboto,Tahoma,sans-serif}.mso .header__logo{font-family:Tahoma,sans-serif !important}.wrapper .header__logo a{color:#c3ced9}.wrapper .header__logo a:hover{color:#859bb1 !important}.footer__share-button{background-color:#7b7c7d}.footer__share-button{font-family:"Open Sans",sans-serif}.mso .footer__share-button{font-family:sans-serif !important}.layout__separator--inline{font-size:20px;line-height:20px;mso-line-height-rule:exactly}
    </style><meta name="robots" content="noindex,nofollow" />
    <meta property="og:title" content="LittleOnes Confirmation Email" />
</head>
<!--[if mso]>
<body class="mso">
<![endif]-->
<!--[if !mso]><!-->
<body class="no-padding" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #f5f7fa;">
<!--<![endif]-->
<div class="wrapper" style="background-color: #f5f7fa;">
    <table style='border-collapse: collapse;table-layout: fixed;color: #b9b9b9;font-family: "Open Sans",sans-serif;' align="center">
        <tbody><tr>
            <td class="preheader__snippet" style="padding: 10px 0 5px 0;vertical-align: top;width: 280px;">
            </td>
        </tr>
        </tbody></table>

    <table class="layout layout--no-gutter layout--full-width" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;width: 100%;background-color: #d5d5d5;background-position: 0px 0px;background-repeat: no-repeat;" align="center">
        <tbody><tr>
            <td class="layout__edges" style="font-size: 0;">&nbsp;</td>
            <td class="column" style='padding: 0;text-align: left;vertical-align: top;color: #60666d;font-size: 14px;line-height: 21px;font-family: "Open Sans",sans-serif;width: 600px;'>

                <div class="image" style="font-size: 12px;font-style: normal;font-weight: 400;" align="center">
                    <img style="display: block;border: 0;max-width: 180px; padding-top: 10px;" alt="" width="180" height="178" src="https://www.littleones.com.au/img/logo.png" />
                </div>

            </td>
            <td class="layout__edges" style="font-size: 0;">&nbsp;</td>
        </tr>
        </tbody></table>

    <table class="layout layout--no-gutter" style="border-collapse: collapse;table-layout: fixed;Margin-left: auto;Margin-right: auto;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #ffffff;" align="center" emb-background-style>
        <tbody><tr>
            <td class="column" style='padding: 0;text-align: left;vertical-align: top;color: #60666d;font-size: 14px;line-height: 21px;font-family: "Open Sans",sans-serif;width: 600px;'>

                <div class="size-16" style="Margin-left: 20px;Margin-right: 20px;padding: 20px;">
                    @yield('content')
                </div>

            </td>
        </tr>
        </tbody></table>

    <table class="footer" style="border-collapse: collapse;table-layout: fixed;Margin-right: auto;Margin-left: auto; margin-top: 20px; border-spacing: 0;width: 560px;" align="center">
        <tbody><tr>
            <td style="padding: 0 0 40px 0;">
                <table class="footer__left" style='border-collapse: collapse;table-layout: fixed;border-spacing: 0;color: #b9b9b9;font-family: "Open Sans",sans-serif;width: 380px;'>
                    <tbody><tr>
                        <td class="footer__inner" style="padding: 0;font-size: 12px;line-height: 19px;">

                            <div>
                                <div><a href="http://www.littleones.com.au">LittleOnes.com.au</a></div>
                            </div>
                        </td>
                    </tr>
                    </tbody></table>
            </td>
        </tr>
        </tbody></table>

</div>

</body></html>
