@extends('emails.layouts.special')

@section('content')
	<h3>New message from contact form:</h3>
	<hr/>
	<p><strong>Name: </strong>{{$data['name']}}</p>
	<p><strong>Email: </strong>{{$data['email']}}</p>
	<p><strong>Message: </strong>{{$data['message']}}</p>
@endsection
