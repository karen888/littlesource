@extends('emails.layouts.email-layout')

@section('content')
    <h2 class="size-24" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #44a8c7;font-size: 24px;line-height: 32px;text-align: center;">
        <strong>You have changed your email address.</strong>
    </h2>

    <p class="size-16" style="Margin-top: 16px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
        Old email address:
    </p>
    <p style="text-align: center;">
        <a class="link-button"
           href="mailto:{{ $data['old_email'] }}">
            {{ $data['old_email'] }}
        </a>
    </p>
    <p class="size-16" style="Margin-top: 16px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
        New email address:
    </p>
    <p style="text-align: center;">
        <a class="link-button"
           href="mailto:{{ $data['new_email'] }}">
            {{ $data['new_email'] }}
        </a>
    </p>
    <p class="size-16" style="Margin-top: 16px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
        If you DID NOT request this change, please contact our support team immediately.
    </p>
@endsection
