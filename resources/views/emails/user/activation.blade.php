@extends('emails.layouts.base')

@section('content')
    <h2>Account was created.</h2>
    <p>Thanks for joining. Please activate your account by clicking on the link below.</p>
    <p>{{ url('auth/activate-account/' . $user->activation_code) }}</p>
@endsection
