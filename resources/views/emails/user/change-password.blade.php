@extends('emails.layouts.base')

@section('content')
    <h2>You have changed your password.</h2>
    <p>This email is to notify you that your password has changed.</p>
    <p>If you didn't change your password, contact support immediately.</p>
@endsection
