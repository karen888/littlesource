@extends('emails.layouts.email-layout')

@section('content')
    <h2 class="size-24" style="Margin-top: 0;Margin-bottom: 0;font-style: normal;font-weight: normal;color: #44a8c7;font-size: 24px;line-height: 32px;text-align: center;">
        <strong>Hi, {{ $user->first_name }} {{ $user->last_name }}</strong>
    </h2>

    <p class="size-16" style="Margin-top: 16px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
        You have changed your password.
    </p>
    <p class="size-16" style="Margin-top: 16px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
        This email is to notify you that your password has changed.
    </p>
    <p class="size-16" style="Margin-top: 16px;Margin-bottom: 0;font-size: 16px;line-height: 24px;text-align: center;">
        If you didn't change your password, contact support immediately.
    </p>
@endsection