@extends('emails.layouts.email-layout')

@section('content')
    You have changed your password.
    This email is to notify you that your password has changed.
    If you didn't change your password, contact support immediately.
@endsection