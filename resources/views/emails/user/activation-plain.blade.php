@extends('emails.layouts.base-plain')

@section('content')
Account was created
Thanks for joining. Please activate your account by clicking on the link below.
{{ url('auth/activate-account/' . $user->activation_code) }}
@endsection
