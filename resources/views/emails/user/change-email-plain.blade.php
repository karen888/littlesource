@extends('emails.layouts.email-layout')

@section('content')
You have changed your email address.
Old email address: {{ $data['old_email'] }}
New email address: {{ $data['new_email'] }}

If you DID NOT request this change, please contact our support team immediately.
@endsection
