@extends('emails.layouts.email-layout')

@section('content')

    <p style="font-size: 14pt;color:#383838; font-family: Arial, sans-serif;">Administrator answered in chat of {{$subj}} verification:</p>
<p>

<blockquote>{{$text}}</blockquote>
</p>

<p>
    You can reply to this email or <a style="text-weight: bold; color: #31912d" href="{{config('app.url')}}{{$link}}" target="_blank">respond on LittleOnes site.</a>
</p>
@endsection