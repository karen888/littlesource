@extends('emails.layouts.base')

@section('content')
    <h2>Notifications:</h2>
    @foreach ($data as $item)
        <p>{{ $item }}</p>
    @endforeach
@endsection