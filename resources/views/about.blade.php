@extends('app')

 

@section('content')

<div class="row">
	<div class="col-md-12">
    	<div class="about-content-page">
    
            <div class="text-box">
                <div class="text-box-inner">
                    <h3>About us</h3>
                    <p>Little Ones is a premier destination for helping families find the care they need. Specializing in providing you with local, qualified caregivers, we are here to help your family! Our hope is to help you be able to find a nanny or babysitter simple and easy.</p>
                </div>
            </div><!-- text-box -->
    
            <div class="text-box">
                <div class="text-box-inner">
                    <h3>The vision</h3>
                    <p>Little Ones is a website (made by a mum!) where you could be assured that the person sent in your home is reliable, honest, and also great with children. I want to find caregivers who want more than just a way to earn a few extra dollars – I want them to see themselves as important contributors to shaping my children’s lives. And as my children grown up, I want caregivers with experience dealing with the challenges of older children and teens.</p>
                    <p>Whether it’s relief in a pinch to run errands or a permanent and consistent resource for your family needs, let Little One help you. After all, a happy mother ensures a happy household with happier children.</p>
                </div>
            </div><!-- text-box -->
    
            <div class="user-reviews-box">
                <div class="user-reviews-box-inner">
                    <header>
                        <div class="user-name">
                            <span class="h3">Meet Viviana</span>
                        </div>
    
                        <div class="user-avatar">
                            <img src="img/temp/ava.png" alt="">
                        </div>
                    </header>
    
                    <div class="user-reviews-text-box">
                        <blockquote>
                            <p>I have three little girls who are within three years of each other. Even though I am a stay-at-home-mum, the truth is that focusing on my own needs always comes last. Sound about right? I was burning out and I needed help.</p>
                            <p>I have my children’s grandparents and family around, but they have their own lives. They have their own lives to lead, are not consistently available to meet m needs and often are not always reliable. I needed a different option. This is how Little Ones began.</p>
                        </blockquote>
                        <i class="author-signature">Viviana,<br> Founder of Little Ones</i>
                    </div>
                </div>
            </div><!-- user-reviews-box -->
            
    	</div><!-- about-content-page -->
    </div><!-- .col-md-8 -->
</div><!-- .row -->

@endsection

