<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        @inject('carbon', 'Carbon\Carbon')
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>LittleOnes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="../css/styles.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="/js/vendor/modernizr.js"></script>
        <script src="/js/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
    </head>
    <body>
        <!-- Page Header -->
        <!-- Set your background image for this header on the line below. -->
        <div class="Prelaunch--caregivers Prelaunch__wrapper-background1" style="background-color: aliceblue;">
            <div class="Prelaunch__background-transparency1">
                <div class="container" id="heading-section">
                    <div class="content" style="padding-bottom: 50px;">
                        <div class="logo">
                        </div>
                        <a href="/pre-launch" title="Little Ones">
                        <img src="../img/logo.png" class="img-responsive" ></a>
                        <h4 class="Prelaunch__Principal-caption" style="color: #6d6f72;text-align: center;margin: 10px;">Little Ones is about to change everything we know about accessing
                            caregivers online. Our most exciting feature — Uber-style babysitting!<br><br>
                            <span>Access awesome rates for short-term care opportunities your local area — register now! </span>
                        </h4>
                    </div>
                </div>
                <div class="Prelaunch__separator" v-show="!showTour"></div>
            </div>
        </div>
        @yield('content')
        <style>
            body{
            background-image: url('../img/texture_1_olive1.jpg');
            }
            .footer a {
            color: #FFFFFF;
            font-size: 20px;
            padding: 10px;
            -webkit-transition: all .5s ease;
            transition: all .5s ease;
            }
        </style>
      
    </body>
</html>