@extends('app')

@section('content')

<h3>View Offer</h3>
@if (!empty($offer_info->data))
<div class="row">
    <strong>
    @if ($offer_info->data->status == 1)
        <span class="text-success">ACCEPTED</span>
    @elseif ($offer_info->data->status == 2)
        <span class="text-danger">REJECTED</span>
    @elseif ($offer_info->data->status == 3)
        <span class="text-danger">CLOSED</span>
    @endif
    </strong>
</div>
<div class="row">
    <div class="col-md-8 thumbnail home-offer-info">
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Related Job Opening</strong></div>
            <div class="col-md-6"><a href="{{ routeClear('job_link') }}/{{ $offer_info->data->path }}">{{ $offer_info->data->title }}</a></div> 
        </div>
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Paid</strong></div>
            <div class="col-md-6 text-bold">${{ $offer_info->data->offer_paid }}</div> 
        </div>
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Rate</strong></div>
            <div class="col-md-6 text-bold">${{ $offer_info->data->job_apply_price }} {{ ($offer_info->data->job_apply_fixed_price) ? '' : ' / hr' }}</div> 
        </div>
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Offer Date</strong></div>
            <div class="col-md-6">{{ $offer_info->data->created_at }}</div> 
        </div>
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Client Message</strong></div>
            <div class="col-md-6">{{ $offer_info->data->offer_text }}</div> 
        </div>

        @if ($offer_info->can_proc)
        <div class="row">
            <div class="col-md-6 col-md-offset-4">
                <form role="form" method="POST" action="" enctype="multipart/form-data" class="pull-left">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="offer" value="{{ $offer_info->data->id }}">
                    <input type="hidden" name="proc" value="1">
                    
                    <button type="submit" class="btn btn-primary send-req-form">Accept</button>
                </form>

                <form role="form" method="POST" action="" enctype="multipart/form-data" class="pull-left" style="margin-left: 15px;">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="offer" value="{{ $offer_info->data->id }}">
                    <input type="hidden" name="proc" value="0">
                    
                    <button type="submit" class="btn btn-default send-req-form">Reject</button>
                </form>

                <div class="clearfix"></div>
            </div> 
        </div>
        @endif
    </div>
    
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Parties</div>
            <div class="panel-body">
                <div>
                    <div class="text-muted"><strong>Offer made by:</strong></div>
                    <div class="media media-no-mp-top">
                        <div class="media-left">
                            <img src="{{ $offer_info->employer_info->user_photo_64 }}">
                        </div>
                        <div class="media-body">
                            <a href="{{ routeClear('user_link') }}/{{ $offer_info->employer_info->user_path }}">{{ $offer_info->employer_info->name }}</a>
                        </div>
                    </div>
                </div>
                <br>
                <div>
                    <div class="text-muted"><strong>Offer made to</strong></div>
                    <div class="media media-no-mp-top">
                        <div class="media-left">
                            <img src="{{ $offer_info->contractor_info->user_photo_64 }}">
                        </div>
                        <div class="media-body">
                            <a href="{{ routeClear('user_link') }}/{{ $offer_info->contractor_info->user_path }}">{{ $offer_info->contractor_info->name }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
@endif

@endsection
