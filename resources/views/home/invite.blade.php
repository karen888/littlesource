@extends('app')

@section('content')

<h3>View Invite</h3>
@if (!empty($data_info->invite))
<div class="row">
    <strong>
    @if ($data_info->invite->active == 1)
        <span class="text-success">ACCEPTED</span>
    @endif
    </strong>
</div>

<div class="row">
    <div class="col-md-8 thumbnail home-offer-info">
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Related Job Opening</strong></div>
            <div class="col-md-6"><a href="{{ routeClear('job_link') }}/{{ $data_info->invite->path }}">{{ $data_info->invite->title }}</a></div> 
        </div>
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Invite Date</strong></div>
            <div class="col-md-6">{{ $data_info->invite->created_at }}</div> 
        </div>
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Employer Message</strong></div>
            <div class="col-md-6">{{ $data_info->invite->cover_letter_employer }}</div> 
        </div>
        @if (!empty($data_info->invite->cover_letter_freelancer))
        <div class="row">
            <div class="col-md-4 text-muted"><strong>Freelancer Message</strong></div>
            <div class="col-md-6">{{ $data_info->invite->cover_letter_freelancer }}</div> 
        </div>
        @endif

        @if ($data_info->can_proc)
        <div class="row">
            <div class="col-md-6 col-md-offset-4">
                <!--
                <form role="form" method="POST" action="" enctype="multipart/form-data" class="pull-left">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="invite" value="{{ $data_info->invite->id }}">
                    <input type="hidden" name="proc" value="1">
                    
                    <button type="submit" class="btn btn-primary send-req-form">Accept</button>
                </form>
                -->
                <a href="{{ routeClear('home') }}/jobs/apply/{{ $data_info->invite->path }}" class="btn btn-primary pull-left">Accept</a>

                <form role="form" method="POST" action="" enctype="multipart/form-data" class="pull-left" style="margin-left: 15px;">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="invite" value="{{ $data_info->invite->id }}">
                    <input type="hidden" name="proc" value="0">
                    
                    <button type="submit" class="btn btn-default send-req-form">Reject</button>
                </form>

                <div class="clearfix"></div>
            </div> 
        </div>
        @endif
    </div>
    
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">Parties</div>
            <div class="panel-body">
                <div>
                    <div class="text-muted"><strong>Invite made by:</strong></div>
                    <div class="media media-no-mp-top">
                        <div class="media-left">
                            <img src="{{ $data_info->employer_info->user_photo_64 }}">
                        </div>
                        <div class="media-body">
                            <a href="{{ routeClear('user_link') }}/{{ $data_info->employer_info->user_path }}">{{ $data_info->employer_info->name }}</a>
                        </div>
                    </div>
                </div>
                <br>
                <div>
                    <div class="text-muted"><strong>Invite made to</strong></div>
                    <div class="media media-no-mp-top">
                        <div class="media-left">
                            <img src="{{ $data_info->contractor_info->user_photo_64 }}">
                        </div>
                        <div class="media-body">
                            <a href="{{ routeClear('user_link') }}/{{ $data_info->contractor_info->user_path }}">{{ $data_info->contractor_info->name }}</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endif

@endsection
