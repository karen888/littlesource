@extends('app')

@section('content')

<div class="page-header">
    <h3>Notifications</h3>
</div>

<div class="row">
    <table class="table table-striped">
        <tbody>
            @foreach ($notifications_list as $val)
            <tr>
                <td class="col-md-2">{{ convToTimezone($val->created_at, true) }}</td>
                <td>{!! $val->notification_text !!}</td>
                <td>
	                <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data" >
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="proc" value="del">
                        <input type="hidden" name="id" value="{{ $val->id }}">

                        <a href="" class="send-req-form text-danger" data-loading-text=".." data-confirm="Are you sure?"><i class="fa fa-remove text-danger"></i></a>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="row">
{!! $notifications_list->render() !!}
</div>

@endsection
