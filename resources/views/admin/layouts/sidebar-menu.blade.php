<ul class="sidebar-menu">
    <li class="header">Little Ones Admin Menu</li>
    <li class="{{ active_class(if_route(['admin.dashboard'])) }}">
        <a href="{{route('admin.dashboard')}}">
            <i class="fa fa-dashboard"></i>
            <span>Dashboard</span>
        </a>
    </li>
    <li class="{{ active_class(if_route(['admin.messages'])) }}">
        <a href="{{route('admin.messages')}}">
            <i class="fa fa-envelope"></i>
            <i class="badge" id="unreadMessages" style="
                    position: absolute;
                    left:24px;top:1px;
                    background: #ff4238;
            @if(($unread = \App\Http\Models\VerificationChats::getCountUnread())==0)display:none @endif
                    ">{{$unread}}</i>
            <span @if($unread)style="margin-left: 9px;"@endif>Verification messages</span>
        </a>
    </li>

    <!-- <li><a href="#"><i class="fa fa-link"></i> <span>Another Link</span></a></li>

    <li class="treeview">
      <a href="#"><i class="fa fa-link"></i> <span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
      <ul class="treeview-menu">
        <li><a href="#">Link in level 2</a></li>
        <li><a href="#">Link in level 2</a></li>
      </ul>
    </li> -->
</ul><!-- /.sidebar-menu -->