<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Little Ones Admin -- @yield('title') </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/admin-assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/admin-assets/dist/css/AdminLTE.min.css">
    {{--<link rel="stylesheet" href="/admin-assets/src/css/lightbox.css">--}}

    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>

    {{--<script type="text/javascript" src="/admin-assets/src/js/lightbox.js"></script>--}}
    <!-- AdminLTE Skins. We have chosen the skin-green-light for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="/admin-assets/dist/css/skins/skin-green-light.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
      <script type="text/javascript" language="javascript" >
              $(document).ready(function() {
                  $('#example').DataTable({
                      "scrollX": true
                  });
              } );



              var fbi = window.setInterval(function(){
                  //console.log('Iteration');
                  jQuery(".fancybox").fancybox();


                  // if (jQuery('.fancybox').length) {
                  //     window.clearInterval(fbi);
                  //     console.log('Done')
                  // }

                  $('[data-toggle="popover-hover"]').popover({
                      trigger: 'hover click focus'

                  });

                  $('[data-toggle="tooltip"]').tooltip();
              }, 200);
              window.ImageErrorHandler = window.setInterval(function(){
                  var imgs = document.getElementsByTagName("img");

                  $.each(imgs, function(k, img){
                      img.onerror = function () {
                          this.style.display = "none";
                          var $parent = $(this).parent();

                          var isA = false;

                          if($parent.is('a')) {
                              $parent = $parent.parent();
                              isA = true;
                          }

                          $parent.append('-');

                          if(isA) {
                              $(this).remove();
                          } else {
                              $(this).parent().remove();
                          }
                      }
                  });
              },500);


    </script>
      <style>
          /** CHAT STYLES **/

          /** Chat **/
          .chat
          {
              list-style: none;
              margin: 0;
              padding: 0;

          }

          .chat li
          {
              margin-bottom: 10px;
              padding-bottom: 5px;
              border-bottom: 1px dotted #B3A9A9;
          }

          .chat li.left .chat-body
          {
              margin-left: 60px;
          }

          .chat li.right .chat-body
          {
              margin-right: 60px;
          }


          .chat li .chat-body p
          {
              margin: 0;
              color: #777777;
          }

          .panel .slidedown .glyphicon, .chat .glyphicon
          {
              margin-right: 5px;
          }

          .panel-body
          {
              overflow-y: scroll;
              height: 250px;
          }

          ::-webkit-scrollbar-track
          {
              -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
              background-color: #F5F5F5;
          }

          ::-webkit-scrollbar
          {
              width: 12px;
              background-color: #F5F5F5;
          }

          ::-webkit-scrollbar-thumb
          {
              -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
              background-color: #555;
          }

          #example td{
            text-align: center;
          }
      </style>
      <script>
          window._csrf = {};

          window._csrf._token = '{{csrf_token()}}';


      </script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.3/socket.io.min.js"></script>

      <style type="text/css">
          img  {
              border: none;
              outline: none;
          }
      </style>
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-green-light                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-green-light sidebar-mini sidebar-collapse">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="{{ route('admin.dashboard') }}" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>LO</b>A</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Little Ones</b> Admin</span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <ul class="nav navbar-nav">
            <li @if(Request::is('*admin/etc')) class="active" @endif>
              <a href="{{ route('admin.dashboard') }}">Home</a>
            </li>
            {{--<li>--}}
              {{--<a href="{{ url('/') }}" target="_blank">Open frontpage</a>--}}
            {{--</li>--}}
            {{--<li @if(Request::is('*admin/etc/config*')) class="active" @endif>--}}
              {{--<a href="{{ url('/admin/etc/config') }}">Config</a>--}}
            {{--</li>--}}
          </ul> <!-- / .navbar-nav -->

          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="/admin-assets/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- The user image in the menu -->
                  <li class="user-header">
                    <img src="/admin-assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <p>
                      {{ Auth::user()->name }} - Admin
                      <small>Member since {{ Auth::user()->created_at }}</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="{{ url('auth/logout') }}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="/admin-assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->name }}</p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          {{-- <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form --> 
          --}}

          <!-- Sidebar Menu -->
          @include('admin.layouts.sidebar-menu')
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            @section('page-header')
                Page Header
            @show
            <small>@section('optional-description') Optional description @show</small>
          </h1>


        </section>

        <!-- Main content -->
        <section class="content">

          @yield('content')
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          <!-- Text in the right goes here -->
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a href="{{ url('/') }}">Little Ones</a>.</strong> All rights reserved.
      </footer>

    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <!-- Bootstrap 3.3.5 -->
    <script src="/admin-assets/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="/admin-assets/dist/js/app.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script>
        // !function(){
        //     var socket = io(':3000');
        //     socket.on('admin unread', function(message) {
        //         console.log(message);
        //
        //         if(message.hasOwnProperty('sender') && message.sender === 'user') {
        //             var unreadDisplay = document.getElementById('unreadMessages');
        //             unreadDisplay.style.display = 'block';
        //
        //             var currentUnread = parseInt(unreadDisplay.innerHTML);
        //             currentUnread++;
        //             unreadDisplay.innerHTML = currentUnread;
        //
        //             var tableBadgeId = 'unreadBadge_' + message.user_id + '_' + message.type + '_' + message.id;
        //             console.log('Searching for "' + tableBadgeId + '"');
        //
        //             var tableBadge = document.getElementById(tableBadgeId);
        //
        //             if(tableBadge) {
        //                 var prevBadgeValue = parseInt(tableBadge.innerHTML);
        //
        //                 tableBadge.innerHTML = '' + (++prevBadgeValue);
        //             } else {
        //                 console.log('No table badge detected, skipping...');
        //             }
        //         }
        //     });
        // }.call();


        Inputmask().mask(document.querySelectorAll("input"));

        document.addEventListener("DOMContentLoaded", function(event) {
            document.querySelectorAll('img').forEach(function(img){

                if(img.src == '') {
                    img.style.display = 'none';
                    console.log('Hidden image 1');
                }
                img.onerror = function(){this.style.display='none';console.log('Hidden image 2')};
            })
        });
    </script>

    <!-- Optionally, you can add Slimscroll and FastClick plugins.
         Both of these plugins are recommended to enhance the
         user experience. Slimscroll is required when using the
         fixed layout. -->
  </body>
</html>
