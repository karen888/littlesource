@extends('admin.app')

@section('page-header') Variables @endsection
@section('optional-description') Config in DB @endsection
@section('content')

    <button onclick="add()" class="btn btn-success">Add new</button>
    <br/><br/>
    <div class="alert alert-success col-md-4" id="alert" style="display: none;">
        Saved!
    </div>
    <br/><br/>
    <form action="" id="configForm" method="POST">
    <table class="table">
        @foreach($config as $key=>$item)
            <tr>
                <td>${{$key}}</td>
                <td><input type="text" name="{{$key}}" value="{{$item}}" required></td>
                <td><a href="javascript:void(0);" onclick="remove('{{$key}}')">Remove</a></td>
            </tr>
        @endforeach
    </table>

    {{csrf_field()}}
    <button onclick="save(this)" type="button" class="btn btn-primary">Save</button>

    </form>
    <script>
        function save(btn) {
            $(btn).attr('disabled', true);

            $.ajax({
                url: '/admin/etc/config',
                type: 'POST',
                data: $('#configForm').serialize(),
                success: function(r) {
                    if(!r.success) {
                        alert('error');
                        return;
                    }

                    $(btn).attr('disabled', false);
                    $('#alert').fadeIn(200);
                    window.setTimeout(function() {
                        $('#alert').fadeOut(2000);
                    }, 2000);
                }
            });
        }
        function add() {
            var key = prompt('Enter key:', '');

            if(key) {
                var value = prompt('Enter value:', '');

                if(value) {
                    $.ajax({
                        url: '/admin/etc/config/add',
                        type: "POST",
                        data: {
                            key: key,
                            value: value,
                            _token: '{{csrf_token()}}'
                        },
                        success:function(){
                            location.reload(true);
                        }
                    });
                }
            }


        }
        function remove(key) {
            if(confirm('Are you sure to remove config item with key \"'+key+'\"?')) {
                $.ajax({
                    url: '/admin/etc/config/remove',
                    type: "DELETE",
                    data: {
                        key: key,
                        _token: '{{csrf_token()}}'
                    },
                    success:function(){
                        alert('Success');
                        location.reload(true);
                    }
                });
            }
        }
    </script>
@endsection