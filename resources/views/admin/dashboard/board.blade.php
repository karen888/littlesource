@extends('admin.app')

@section('title', 'Board user')

@section('page-header', 'Board user '. $user->user->first_name." ".$user->user->last_name . " (" . $user->user->email . ")")
@section('optional-description', ' ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')

    <div id="get-verified">
        <div class="" style="margin-top: 20px;">
            <div class="container">
                <h4>Status Documents Verification</h4>
                <p>On this page, you can check the status of documents and change documents in case of failure.</p>
                <div class="table-hover">
                    <table class="table">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>File Type</th>
                            <th>Submission date</th>
                            <th>Status</th>
                            <th>Comments</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                            </td>
                            <td><a href="/admin/etc/board/{{ $user->user->id }}/phone">Phone</a> *</td>
                            <td>@if(!$user->phone || $user->phone->phone == '') - @else {{ $user->phone->phone }} @endif</td>
                            <td>@if(!$user->phone || $user->phone->phone == '') - @else {{ date("m/d/Y",strtotime($user->phone->updated_at)) }} @endif</td>
                            <td>
                                @if(!$user->phone || $user->phone->phone == '')
                                    not submitted
                                @else
                                    @if($user->phone->verified == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                    @elseif($user->phone->verified == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @else
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                    @endif
                                @endif
                            </td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a href="/admin/etc/board/{{ $user->user->id }}/wwcc">WWCC</a> *</td>
                            <td>@if($user->user->government_id && $user->user->government_id->user_holding_document_photo)
                                    @include('admin.dashboard.partial.scans_preview', ['file' => $user->user->government_id->user_holding_document_photo])
                                @else
                                    -
                                @endif
                            </td>
                            <td>@if($user->user->wwcc && $user->user->government_id && $user->user->government_id->user_holding_document_photo)
                                    {{ $user->user->wwcc->updated_at }}
                                @else
                                    -
                                @endif
                            </td>
                            <td>
                                @if(!$user->user->wwcc)
                                    not submitted
                                @else
                                    @if($user->user->wwcc->verified == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                    @elseif($user->user->wwcc->verified == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @elseif($user->user->wwcc->verified == 2)
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                    @endif
                                @endif
                            </td>
                            <td>
                                <?php
                                $wwcc_chat = $user->user->getVerificationChat('wwcc');
                                ?>


                                @if(count($wwcc_chat))
                                <span style="color:green">{{$wwcc_chat[count($wwcc_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'User'}}: </span>
                                <a href="{{ route('admin.board.wwcc', $user->user->id) }}#chat_wwcc">{{$wwcc_chat[count($wwcc_chat) - 1]->message}}</a>
                                @else
                                    <a href="{{ route('admin.board.wwcc', $user->user->id) }}#chat_wwcc">-</a>
                                @endif
                            </td>
                        </tr>
                        <tr >
                            <td></td>
                            <th>Qualifications</th>
                            @foreach($user->qualifications as $qual)
                                <tr class="no-border">
                                    <td></td>
                                    <td><a href="/admin/etc/board/{{ $user->user->id }}/qualification?id={{ $qual['id'] }}">{{ $qual['name'] }} </a></td>
                                    <td>
                                        @if($qual['path'] == '') - @else
                                            @include('admin.dashboard.partial.scans_preview', ['file' => $qual['path']])
                                        @endif
                                    </td>
                                    <td>@if(@$qual['id'] == '') - @else {{ date("d/m/Y",strtotime($qual['created_at'])) }} @endif</td>
                                    <td>@if($qual['id'] == '')
                                            not submitted
                                        @else
                                            @if($qual['verified'] == 0)
                                                <i class="fa fa-refresh"></i>&nbspsubmitted
                                            @elseif($qual['verified'] == 1)
                                                <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                            @else
                                                <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                            @endif
                                        @endif</td>
                                    <td>
                                    <td>
                                        <?php
                                        $qual_chat = $user->user->getVerificationChat('qualification', $qual['id']);
                                        ?>
                                        @if(count($qual_chat))
                                            <span style="color:green">{{$qual_chat[count($qual_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'User'}}: </span>
                                            <a href="/admin/etc/board/{{ $user->user->id }}/qualification?id={{ $qual['id'] }}#chat_qualification">{{$qual_chat[count($qual_chat) - 1]->message}}</a>
                                        @else
                                            <a href="/admin/etc/board/{{ $user->user->id }}/qualification?id={{ $qual['id'] }}#chat_qualification">-</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tr>
                        @foreach($user->educations as $edu)
                            <tr class="no-border">
                                <td></td>
                                <td><a href="/admin/etc/board/{{ $user->user->id }}/qualification?id={{ $edu->id }}">{{ $edu->school }} </a></td>
                                <td>@if($edu->scan == '') - @else

                                        @include('admin.dashboard.partial.scans_preview', ['file' => $edu->scan])

                                    @endif</td>
                                <td>@if(@$edu->id == '') - @else {{ date("d/m/Y",strtotime($edu->created_at)) }} @endif</td>
                                <td>@if($edu->id == '')
                                        not submitted
                                    @else
                                        @if($edu->status == 0)
                                            <i class="fa fa-refresh"></i>&nbspsubmitted
                                        @elseif($edu->status == 1)
                                            <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                        @else
                                            <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                        @endif
                                    @endif</td>
                                <td>
                                    <?php
                                    $edu_chat = $user->user->getVerificationChat('education', $edu->id);
                                    ?>

                                    @if(count($edu_chat))
                                    <span style="color:green">{{$edu_chat[count($edu_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'User'}}: </span>
                                    <a href="/admin/etc/board/{{ $user->user->id }}/qualification?id={{ $edu->id }}#chat_education">{{$edu_chat[count($edu_chat) - 1]->message}}</a>
                                    @else
                                        <a href="/admin/etc/board/{{ $user->user->id }}/qualification?id={{ $edu->id }}#chat_education">-</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td><a onclick="alert('coming soon'); return false" style="color:#999!important" href="/admin/etc/board/{{ $user->user->id }}/id">ID (photo ID) *</a></td>
                            <td>
                                @if($user->prof_scan_id == '')
                                    -
                                @else
                                    @include('admin.dashboard.partial.scans_preview', ['file' => $user->prof_scan_id])
                                @endif
                            </td>
                            <td>@if($user->prof_scan_id == '') - @else {{ date("d/m/Y",strtotime($user->updated_at)) }} @endif</td>
                            <td>@if($user->prof_scan_id == '')
                                    not submitted
                                @else
                                    @if($user->user->verifed_id == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                        @elseif($user->user->verifed_id == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @else
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                        @endif
                                @endif</td>
                            <td>
                                <?php
                                $id_chat = $user->user->getVerificationChat('prof_id');
                                ?>
                                @if(count($id_chat))
                                    <span style="color:green">{{$id_chat[count($id_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'User'}}: </span>
                                    <a href="{{ route('admin.board.id', $user->id) }}#chat_prof_id">{{$id_chat[count($id_chat) - 1]->message}}</a>
                                @else
                                    <a href="{{ route('admin.board.id', $user->id) }}#chat_prof_id">-</a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a onclick="alert('coming soon'); return false" style="color:#999!important" href="/admin/etc/board/{{ $user->user->id }}/address">Address verification *</a></td>
                            <td>@if($user->loc_scan == '') - @else
                                    @include('admin.dashboard.partial.scans_preview', ['file' => $user->loc_scan])
                                @endif</td>
                            <td>@if($user->loc_city == '') - @else {{ date("d/m/Y", $user->loc_time ) }} @endif</td>
                            <td>
                                @if($user->loc_city == '')
                                    not submitted
                                @else
                                    @if($user->user->address_status == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                    @elseif($user->user->address_status == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @else
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                    @endif
                                @endif
                            </td>
                            <td>
                                <?php
                                $address_chat = $user->user->getVerificationChat('address');
                                ?>
                                @if(count($address_chat))
                                    <span style="color:green">{{$address_chat[count($address_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'User'}}: </span>
                                    <a href="{{ route('admin.board.address', $user->user->id) }}#chat_address">{{$address_chat[count($address_chat) - 1]->message}}</a>
                                @else
                                    <a href="{{ route('admin.board.address', $user->user->id) }}#chat_address">-</a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Certifications</th>
                            <th></th>
                            <td></td>
                            <td></td>
                            <td>-</td>
                        </tr>
                        @foreach($user->certificates as $cert)
                            <tr class="no-border">
                                <td></td>
                                <td><a href="/admin/etc/board/{{ $user->user->id }}/certificates?id={{ $cert->id }}">{{ $cert->name }} </a></td>
                                <td>@if($cert->id == '') - @else {{ $cert->provider }} @endif</td>
                                <td>@if(@$cert->id == '') - @else {{ date("d/m/Y",strtotime($cert->updated_at)) }} @endif</td>
                                <td>@if($cert->id == '')
                                        not submitted
                                    @else
                                        @if($cert->status == 0)
                                            <i class="fa fa-refresh"></i>&nbspsubmitted
                                        @elseif($cert->status == 1)
                                            <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                        @else
                                            <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                        @endif
                                    @endif</td>
                                <td>
                                    <?php
                                    $cert_chat = $user->user->getVerificationChat('certifications', $cert->id);
                                    ?>
                                    @if(count($cert_chat))

                                    <span style="color:green">{{$cert_chat[count($cert_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'User'}}: </span>
                                    <a href="/admin/etc/board/{{ $user->user->id }}/certificates?id={{ $cert->id }}#chat_certifications">{{$cert_chat[count($cert_chat) - 1]->message}}</a>
                                    @else
                                        <a href="/admin/etc/board/{{ $user->user->id }}/certificates?id={{ $cert->id }}#chat_certifications">-</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td><a onclick="alert('coming soon'); return false" style="color:#999!important" href="/admin/etc/board/{{ $user->user->id }}/abn">ABN</a></td>
                            <td>@if($user->abn_number == '') - @else {{ $user->abn_number }} @endif</td>
                            <td>@if($user->abn_number == '') - @else {{ date("d/m/Y", $user->abn_number_submit ) }} @endif</td>
                            <td>
                                @if($user->abn_number == '')
                                    not submitted
                                @else
                                    @if($user->user->abn_verifed == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                    @elseif($user->user->abn_verifed == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @else
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                    @endif
                                @endif
                            </td>
                            <td>
                                <?php
                                $abn_chat = $user->user->getVerificationChat('abn');
                                ?>
                                @if(count($abn_chat))
                                    <span style="color:green">{{$abn_chat[count($abn_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'User'}}: </span>
                                    <a href="/admin/etc/board/{{ $user->user->id }}/abn#chat_abn">{{$abn_chat[count($abn_chat) - 1]->message}}</a>
                                @else
                                    <a href="/admin/etc/board/{{ $user->user->id }}/abn#chat_abn">-</a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a onclick="alert('coming soon'); return false" style="color:#999!important" href="/admin/etc/board/{{ $user->user->id }}/social-links">Social Links</a></td>
                            <td>@if($user->soc_link == '') - @else {{ $user->soc_link }} @endif</td>
                            <td>@if($user->soc_link == '') - @else {{ date("d/m/Y", $user->soc_link_submit ) }} @endif</td>
                            <td>@if($user->soc_link == '')
                                    not submitted
                                @else
                                    @if($user->user->social_verifed == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                    @elseif($user->user->social_verifed == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @else
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                    @endif
                                @endif</td>
                            <td>
                                <?php
                                $soc_chat = $user->user->getVerificationChat('social');
                                ?>
                                @if(count($soc_chat))

                                <span style="color:green">{{$soc_chat[count($soc_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'User'}}: </span>
                                <a href="/admin/etc/board/{{ $user->user->id }}/social-links#chat_social">{{$soc_chat[count($soc_chat) - 1]->message}}</a>
                                @else
                                    <a href="/admin/etc/board/{{ $user->user->id }}/social-links#chat_social">-</a>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>ID</th>
                            <th></th>
                            <td></td>
                            <td></td>
                            <td>-</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a href="/admin/etc/board/{{ $user->user->id }}/medicare-card">Medicare Card</a></td>
                            <td></td>
                            <td>@if(!$user->medicare_card) - @else {{ $user->medicare_card['updated_at']->format('m/d/Y') }} @endif</td>
                            <td>@if(!$user->medicare_card)
                                    not submitted
                                @else
                                    @if($user->medicare_card['verified'] == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                    @elseif($user->medicare_card['verified'] == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @elseif($user->medicare_card['verified'] == -1)
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                    @endif
                                @endif
                            </td>
                            <td>-</td>
                        </tr>
                         <tr>
                            <td></td>
                            <td><a href="/admin/etc/board/{{ $user->user->id }}/passport">Passport</a></td>
                            <td></td>
                            <td>@if(!$user->passport) - @else {{ $user->passport['updated_at']->format('m/d/Y') }} @endif</td>
                            <td>@if(!$user->passport)
                                    not submitted
                                @else
                                    @if($user->passport['verified'] == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                    @elseif($user->passport['verified'] == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @elseif($user->passport['verified'] == -1)
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                    @endif
                                @endif
                            </td>
                            <td>-</td>
                        </tr>
                         <tr>
                            <td></td>
                            <td><a href="/admin/etc/board/{{ $user->user->id }}/driver-licence">Driver Licence</a></td>
                            <td></td>
                            <td>@if(!$user->driver_licence) - @else {{ $user->driver_licence['updated_at']->format('m/d/Y') }} @endif</td>
                            <td>@if(!$user->driver_licence)
                                    not submitted
                                @else
                                    @if($user->driver_licence['verified'] == 0)
                                        <i class="fa fa-refresh"></i>&nbspsubmitted
                                    @elseif($user->driver_licence['verified'] == 1)
                                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                    @elseif($user->driver_licence['verified'] == -1)
                                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                    @endif
                                @endif
                            </td>
                            <td>-</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pull-right">
                    <a href="/admin/etc" type="button" class="btn btn-themed btn-preview back" >Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
