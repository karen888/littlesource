@extends('admin.app')

@section('title', 'Medicare Card verification')

@section('page-header', 'Medicare Card verification (' . $user->getNameAttribute() . ')')
@section('optional-description', ' ')

@section('breadcrumbs')
{!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
<div class="container padd-top-30">
    <div class="verify-form job-form"  >
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Card name</strong>
            </div>
            <div class="col-md-4">
                <span>@if(!$user->medicare_card) - @else {{ $user->medicare_card['card_name'] }} @endif</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Expiry Date</strong>
            </div>
            <div class="col-md-4">
                <span>@if(!$user->medicare_card) - @else {{ $user->medicare_card['expiry_date'] }} @endif</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Card Number</strong>
            </div>
            <div class="col-md-4">
                <span>@if(!$user->medicare_card) - @else {{ $user->medicare_card['id_number'] }} @endif</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Card Type</strong>
            </div>
            <div class="col-md-4">
                <span>@if(!$user->medicare_card) - @else {{ $user->medicare_card['card_type_name'] }} @endif</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Individual Ref. Number</strong>
            </div>
            <div class="col-md-4">
                <span>@if(!$user->medicare_card) - @else {{ $user->medicare_card['individual_ref_number'] }} @endif</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Status</strong>
            </div>
            <div class="col-md-4">
                <span>
                    @if(!$user->medicare_card)
                        not submitted
                    @else
                        @if($user->medicare_card['verified'] == 0)
                            <i class="fa fa-refresh"></i>&nbspsubmitted
                        @elseif($user->medicare_card['verified'] == 1)
                            <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                        @elseif($user->medicare_card['verified'] == -1)
                            <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                        @endif
                    @endif
                </span>
            </div>
        </div>
        @if ($user->medicare_card && $user->allow_dvs_check && $user->medicare_card['verified'] != 1)
        <div class="row form-row">
            <div class="col-md-3">
                <strong>DVS Check</strong>
            </div>
            <div class="col-md-4">
                <a class="btn btn-themed btn-primary" href="{{ route('admin.board.medicare-card-dvs', $user->id) }}">Check now</a>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="full-width">
                    <div class="actions">
                        <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
