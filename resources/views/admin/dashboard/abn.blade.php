@extends('admin.app')

@section('title', 'ABN Verification')

@section('page-header', 'ABN Verification')
@section('optional-description', ' ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
    <div class="container padd-top-30" id="verifed-abn">
        <div class="row">
            <div class="col-md-12">
                <div class="main-heading">
                    <h4>ABN Verification</h4>
                </div>
            </div>
        </div>
        <hr>
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>ABN number *</strong>
                </div>
                <div class="col-md-6">
                    <span class="abn_number">{{ $user->info->abn_number }}</span>
                </div>
            </div>

            <div class="row form-row showStatus" style="display: none;">
                <div class="col-md-3">
                    <strong>Status (at the moment of changing)</strong>
                </div>
                <div class="col-md-4">
                    <span class="status"></span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            @if($user->abn_verifed == 0)
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="1" style="background-color: #22b92c !important; border: none;">Verify</a>
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="2" style="background-color: #FF1F1F !important; border: none;">Reject</a>
                            @endif
                            <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Chat with user</strong>
                </div>
                <div class="col-md-6">
                    @include('admin.dashboard.partial.chat-view', ['chat'=>$user->getVerificationChat('abn'),'type'=>'abn', 'user'=>$user, 'panelId'=>'chat_abn', 'id'=>null])
                </div>
            </div>
        </div>

        <div class="container">
            <h2>History of changings</h2>
            {{--<p></p>--}}
            <table class="table table-striped">
                <thead>

                <tr>
                    <th>WWCC</th>
                    <th>Date of submitting</th>
                    <th>Date of changing</th>
                    <th>Status (at moment of changing)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $user->info->abn_number }}</td>
                    <td>{{ date("d/m/Y H:i:s", $user->info->abn_number_submit) }}</td>
                    <td style="color: green">Actual</td>
                    <td style="color: green">Actual</td>
                </tr>
                @foreach($historyData as $history)
                    <?php $dataUser = json_decode($history->data); ?>
                    <tr>
                        @foreach($dataUser as $key=>$data)
                            <input type="hidden" id="{{ $key }}-{{ $history->id }}" value="{{ $data }}">
                        @endforeach
                        <td><a data-id ="{{ $history->id }}" class="refreshHistory set-{{ $history->id }}">{{ $dataUser->abn_number }}</td>
                        <td>{{ $history->created_at->format("d/m/Y H:i:s") }}</td>
                        <td>{{ date("d/m/Y H:i:s", $dataUser->abn_number_submit) }}</td>
                        <td>
                            @if($dataUser->abn_verifed == 0)
                                submitted
                            @elseif($dataUser->abn_verifed == 1)
                                <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                            @else
                                <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
    <script>
        $(".saveform").on('click', function(){
            var user_id = $(this).attr('data-id');
            var statusUser = $(this).attr('data-status');
//            var wwcc_status_expdate = $("#wwcc_status_expdate").val();
            $.ajax({
                type: "get",
                data: {status: statusUser, "user":user_id },
                success: function(done){
                    if(done == 1){
                        location.reload();
                    }
                }
            });
        });
        $(".actions").on('click', ".refreshPage", function(e){
            e.preventDefault();
            location.reload();
        });


        $(".refreshHistory").on('click', function(){
            var _this = $(this);
            var id = _this.attr('data-id');
            $(".back").addClass("refreshPage");
            $(".back").html("Back to current version");
            $(".refreshHistory").css({'font-weight': 'normal', 'color':"#337ab7"});
            $(".set-"+id).css({'font-weight': 'bold', 'color': 'green'});
            UpdateInfo(id);
        });
        function UpdateInfo(id){

            var abn_number = $("#abn_number-"+id).val();
            $(".abn_number").html(abn_number);


            var wwcc_status = $("#abn_verifed-"+id).val();
            $(".showStatus").css('display', 'block');
            if(wwcc_status == 0){
                $(".status").html("submitted");
            }
            if(wwcc_status == 1){
                $(".status").html("Verified");
            }
            if(wwcc_status == 2){
                $(".status").html("Rejected");
            }
            var date_submit = $("#date_submit-"+id).val();
            $(".date_submit").html(date_submit);

            $(".btn-main-action").css('display', 'none');
        }
    </script>
    <style>
        .form-row{
            margin-top: 20px; !important;
        }
    </style>
@endsection
