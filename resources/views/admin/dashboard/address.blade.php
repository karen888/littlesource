@extends('admin.app')

@section('title', 'Address vefification')

@section('page-header', 'Address verification')
@section('optional-description', ' ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
    <div class="container padd-top-30" id="adress-verifed" >
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Address *</strong>
                </div>
                <div class="col-md-4">
                    @if($user->info->loc_adress == '')- @endif
                    <span class="loc_adress">{{ $user->info->loc_adress }}</span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong></strong>
                </div>
                <div class="col-md-4">
                    @if($user->info->loc_adress2 == '')- @endif
                    <span class="loc_adress">{{ $user->info->loc_adress2 }}</span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>City *</strong>
                </div>
                <div class="col-md-4">
                    @if($user->info->loc_city == '')- @endif
                    <span class="loc_city">{{ $user->info->loc_city }}</span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Postcode *</strong>
                </div>
                <div class="col-md-4">
                    @if($user->info->loc_zip == '')- @endif
                    <span class="loc_zip">{{ $user->info->loc_zip }}</span>
                </div>
            </div>

            <div class="row form-row ">
                <div class="col-md-3">
                    <strong>Scan of request document * </strong>
                </div>
                <div class="col-md-4 full-width">
                    @if($user->info->loc_scan == '') No photo @else

                        @include('admin.dashboard.partial.scans_preview', ['file' => $user->info->loc_scan])
                        @endif

                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Document Type *</strong>
                </div>
                <div class="col-md-4 no_padding">
                    @if($user->info->loc_type_text == '')- @endif
                    <span class="loc_type_text">{{ $user->info->loc_type_text }}</span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Date of Issuing *</strong>
                </div>
                <div class="col-md-4">
                    @if($user->info->loc_date == '' || strtotime($user->info->loc_date) == 0)- @else
                    <span class="loc_date">{{ date("d/m/Y", strtotime($user->info->loc_date)) }}</span>
                        @endif
                </div>
            </div>
            {{--<div class="row form-row">--}}
                {{--<div class="col-md-3">--}}
                    {{--<strong>Submision date</strong>--}}
                {{--</div>--}}
                {{--<div class="col-md-4">--}}
                    {{--@if($user->address_status == 0)--}}
                        {{--<input id="address_status_expdate" data-inputmask="'alias': 'date'" type="text" name="address_status_expdate"/>--}}
                    {{--@else--}}
                        {{--<span class="address_status_expdate">{{ @date('d/m/Y', strtotime(@$user->address_status_expdate)) }}</span>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row form-row showStatus" style="display: none;">--}}
                {{--<div class="col-md-3">--}}
                    {{--<strong>Status (at the moment of changing)</strong>--}}
                {{--</div>--}}
                {{--<div class="col-md-4">--}}
                    {{--<span class="status"></span>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            @if($user->address_status == 0)
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="1" style="background-color: #22b92c !important; border: none;">Verify</a>
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="2" style="background-color: #FF1F1F !important; border: none;">Reject</a>
                            @endif
                            <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Chat with user</strong>
                </div>
                <div class="col-md-6">
                    @include('admin.dashboard.partial.chat-view', ['chat'=>$user->getVerificationChat('address'),'type'=>'address', 'user'=>$user, 'panelId'=>'chat_address', 'id'=>null])
                </div>
            </div>
        </div>
        <div class="container">
            <h2>History of changings</h2>
            {{--<p></p>--}}
            <table class="table table-striped">
                <thead>

                <tr>
                    <th>Adress</th>
                    <th>Date of submitting</th>
                    <th>Date of changing</th>
                    <th>Status (at moment of changing)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $user->info->loc_adress }}</td>
                    <td>{{ date("d/m/Y H:i:s", $user->info->loc_time) }}</td>
                    <td style="color: green">Actual</td>
                    <td style="color: green">Actual</td>
                </tr>
                @foreach($historyData as $history)
                    <?php $dataUser = json_decode($history->data);
                    if(json_last_error() !== JSON_ERROR_NONE) {
                        continue;
                    }

                    ?>

                    <tr>
                        @foreach($dataUser as $key=>$data)
                            <input type="hidden" id="{{ $key }}-{{ $history->id }}" value="{{ $data }}">
                        @endforeach
                        <td><a data-id ="{{ $history->id }}" class="refreshHistory set-{{ $history->id }}">{{ $dataUser->loc_adress }}</td>
                        <td>{{ $history->created_at->format("d/m/Y H:i:s") }}</td>
                        <td>{{ date("d/m/Y H:i:s", $dataUser->loc_time) }}</td>
                        <td>
                            @if($dataUser->address_status == 0)
                                submitted
                            @elseif($dataUser->address_status == 1)
                                <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                            @else
                                <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
    <script>
        $(".saveform").on('click', function(){
            var user_id = $(this).attr('data-id');
            var statusUser = $(this).attr('data-status');
            var address_status_expdate = $("#address_status_expdate").val();
            $.ajax({
                type: "get",
                data: {status: statusUser, "user":user_id, 'address_status_expdate': address_status_expdate},
                success: function(done){
                    if(done == 1){
                        location.reload();
                    }
                }
            });
        });
        $(".actions").on('click', ".refreshPage", function(e){
            e.preventDefault();
            location.reload();
        });


        $(".refreshHistory").on('click', function(){
            var _this = $(this);
            var id = _this.attr('data-id');
            $(".back").addClass("refreshPage");
            $(".back").html("Back to current version");
            $(".refreshHistory").css({'font-weight': 'normal', 'color':"#337ab7"});
            $(".set-"+id).css({'font-weight': 'bold', 'color': 'green'});
            UpdateInfo(id);
        });
        function UpdateInfo(id){
            var loc_adress = $("#loc_adress-"+id).val();
            $(".loc_adress").html(loc_adress);

            var loc_adress2 = $("#loc_adress2-"+id).val();
            $(".loc_adress2").html(loc_adress2);

            var loc_city = $("#loc_city-"+id).val();
            $(".loc_city").html(loc_city);

            var loc_zip = $("#loc_zip-"+id).val();
            $(".loc_zip").html(loc_zip);

            var loc_scan = $("#loc_scan-"+id).val();
            $(".loc_scan").attr('src', loc_scan);


            var loc_type_text = $("#loc_type_text-"+id).val();
            $(".loc_type_text").html(loc_type_text);

            var loc_date = $("#loc_date-"+id).val();
            $(".loc_date").html(loc_date);


            var address_status = $("#address_status-"+id).val();
            $(".showStatus").css('display', 'block');
            if(address_status == 0){
                $(".status").html("submitted");
            }
            if(address_status == 1){
                $(".status").html("Verified");
            }
            if(address_status == 2){
                $(".status").html("Rejected");
            }
            var address_status_expdate = $("#address_status_expdate-"+id).val();
            $(".address_status_expdate").html(address_status_expdate);

            $(".btn-main-action").css('display', 'none');

        }
    </script>
    <style>
        .form-row{
            margin-top: 20px; !important;
        }
    </style>
@endsection
