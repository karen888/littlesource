@extends('admin.app')

@section('title', 'Dashboard')

@section('page-header', 'Dashboard')
@section('optional-description', ' ')

@section('breadcrumbs')
{!! Breadcrumbs::render('dashboard') !!}
@endsection

@section('content')
    <div class="box-header">
        <a class="pull-right btn btn-themed btn-primary" href="{{ route('admin.export-caregivers') }}">Export Caregivers</a>
    </div>

    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Filled</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>State</th>
                <th>Post Code</th>
                <th>Registration</th>
                <th>Photo</th>
                <th>DOB</th>
                <th>Phone</th>
                <th>Video</th>
                <th>Qualification</th>
                <th>Reference</th>
                <th>Identity Check</th>
                <th>Medicare Card</th>
                <th>WWCC</th>
                <th>Action</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Filled</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>State</th>
                <th>Post Code</th>
                <th>Registration</th>
                <th>Photo</th>
                <th>DOB</th>
                <th>Phone</th>
                <th>Video</th>
                <th>Qualification</th>
                <th>Reference</th>
                <th>Identity Check</th>
                <th>Medicare Card</th>
                <th>WWCC</th>
                <th>Action</th>
            </tr>
        </tfoot>
        <tbody>
        @foreach($caregivers as $caregiver)
        <tr>
            <td>{{$caregiver['completeness'] . '%'}}</td>
            <td>{{$caregiver['first_name']}}</td>
            <td>{{$caregiver['last_name']}}</td>
            <td>{{$caregiver['email']}}</td>
            <td>{{$caregiver['state']}}</td>
            <td>{{$caregiver['post_code']}}</td>
            <td>
                @if($caregiver['created_at']  )
                    {{ date("d/m/Y H:i:s", strtotime($caregiver['created_at'])) }}
                @else - @endif
            </td>
            <td>
                @if($caregiver['photo'] )
                    <img src="{{$caregiver['photo']}}" width="60px" alt="">
                @else - @endif
            </td>
            <td>{{$caregiver['dob']}}</td>
            <td>{{$caregiver['phone']}}</td>
            <td>{{$caregiver['video']}}</td>
            <td>{{$caregiver['qualifications']}}</td>
            <td>{{$caregiver['references']}}</td>
            <td>{{$caregiver['identity_check']}}</td>
            <td>{{$caregiver['medicare_card']}}</td>
            <td>{{$caregiver['wwcc']}}</td>
            <td><a href="{{ route('admin.caregiverinfo', $caregiver['id']) }}">Info</a> | <a href="{{ route('admin.board', $caregiver['id']) }}">Documents</a></td>
        </tr>
            @endforeach
        </tbody>
    </table>
@endsection
