@extends('admin.app')

@section('title', 'WWCC Verification')

@section('page-header', 'WWCC Verification')
@section('optional-description', ' ')

@section('breadcrumbs')
{!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
<div class="container padd-top-30" id="wwcc" v-cloak>
    <div class="row">
        <div class="col-md-12">
            <div class="main-heading">
                <h4>Working with Children Check (WWCC)</h4>
            </div>
        </div>
    </div>
    <hr>
    <div class="verify-form job-form">
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Issuing state</strong>
            </div>
            <div class="col-md-4">
                @if ($user->wwcc && $user->wwcc->state)
                    {{ $user->wwcc->state->name }}
                @else
                    -
                @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Application or Card Number</strong>
            </div>
            <div class="col-md-4">
                @if ($user->wwcc && $user->wwcc->application_card_number)
                    {{ $user->wwcc->application_card_number }}
                @else
                    -
                @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Name as it Appears on Card</strong>
            </div>
            <div class="col-md-4">
                @if ($user->wwcc && $user->wwcc->card_name)
                    {{ $user->wwcc->card_name }}
                @else
                    -
                @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Expiry Date</strong>
            </div>
            <div class="col-md-4">
                @if ($user->wwcc && $user->wwcc->expiry_date)
                    {{ $user->wwcc->expiry_date }}
                @else
                    -
                @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Your photo with document</strong>
            </div>
            <div class="col-md-4">
                @if($user->government_id && $user->government_id->user_holding_document_photo)
                    @include('admin.dashboard.partial.scans_preview', ['file' => $user->government_id->user_holding_document_photo])
                @else
                    No photo
                @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Updated at</strong>
            </div>
            <div class="col-md-4">
                @if ($user->wwcc && $user->wwcc->updated_at)
                    {{ $user->wwcc->updated_at }}
                @else
                    -
                @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Status</strong>
            </div>
            <div class="col-md-4">
                @if(!$user->wwcc)
                    not submitted
                @else
                    @if($user->wwcc->verified == 0)
                        <i class="fa fa-refresh"></i>&nbspsubmitted
                    @elseif($user->wwcc->verified == 1)
                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                    @elseif($user->wwcc->verified == 2)
                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                    @endif
                @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Verify</strong>
            </div>
            <div class="col-md-4">
                <div class="full-width">
                    <div class="actions">
                        @if ($user->wwcc)
                        <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="1" data-wwcc-id="{{ $user->wwcc->id }}" style="background-color: #22b92c !important; border: none;">Verify</a>
                        <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="2" data-wwcc-id="{{ $user->wwcc->id }}" style="background-color: #FF1F1F !important; border: none;">Reject</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row form-row">
            <div class="col-md-3"></div>
            <div class="col-md-4">
                <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
            </div>
        </div>

        <div class="row form-row" style="display:none">
            <div class="col-md-3">
                <strong>Chat with user</strong>
            </div>
            <div class="col-md-6">
                @include('admin.dashboard.partial.chat-view', ['chat'=>$user->getVerificationChat('wwcc'),'type'=>'wwcc', 'user'=>$user, 'panelId'=>'chat_wwcc', 'id'=>null])
            </div>
        </div>
    </div>
	    <div class="container"  style="display:none">
        <h2>History of changings</h2>
        {{--<p></p>--}}
        <table class="table table-striped">
            <thead>

            <tr>
                <th>WWCC</th>
                <th>Date of submitting</th>
                <th>Date of changing</th>
                <th>Status (at moment of changing)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $user->info->wwcc_number }}</td>
                <td>{{  date("d/m/Y H:i:s", $user->info->wwcc_date) }}</td>
                <td style="color: green">Actual</td>
                <td style="color: green">Actual</td>
            </tr>
            @foreach($historyData as $history)
               <?php $dataUser = json_decode($history->data); ?>
            <tr>
                @foreach($dataUser as $key=>$data)
                    <input type="hidden"  id="{{ $key }}-{{ $history->id }}" value="{{ $data }}">
                @endforeach
                <td><a data-id ="{{ $history->id }}" class="refreshHistory set-{{ $history->id }}">{{ $dataUser->wwcc_number }}</a></td>
                <td>{{ $history->created_at }}</td>
				<td>{{ date("d/m/Y H:i:s", $dataUser->wwcc_date) }}</td>
                <td>
                    @if($dataUser->wwcc_status == 0)
                        submitted
                    @elseif($dataUser->wwcc_status == 1)
                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverifed </span>
                    @else
                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                    @endif
                </td>
            </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
<div class="spinner-modal">
</div>
    <script>
        $(".saveform").on('click', function(){
            var user_id = $(this).attr('data-id');
            var wwcc_id = $(this).attr('data-wwcc-id');
            var statusUser = $(this).attr('data-status');
			var wwcc_status_expdate = $("#wwcc_status_expdate").val();
            var go = true;
            if(statusUser == 1){
                if(wwcc_status_expdate == ''){
                    go = false;
                    alert("Expiry Date");

                }
            }
            if(go == true) {
                $.ajax({
                    type: "get",
                    data: {"status": statusUser, "user_id":user_id, 'wwcc_status_expdate': wwcc_status_expdate, "wwcc_id" : wwcc_id},
                    success: function(done){
                        if(done == 1){
                            location.reload();
                        }
                    }
                });
            }
        });
        $(".actions").on('click', ".refreshPage", function(e){
            e.preventDefault();
            location.reload();
        });
        $(".manual-verification").on('click', function(e) {
            var user_id = $(this).attr('data-id');
            var wwcc_id = $(this).attr('data-wwcc-id');
            var wwcc_state = $(this).attr('data-state');
                $.ajax({
                    url: "wwcc/manual",
                    type: "get",
                    data: {"user_id": user_id, "wwcc_id": wwcc_id, "state": wwcc_state},
                    success: function(done){
                        if(done == 1){
                            window.location.href = window.location.href.substr(0, window.location.href.lastIndexOf('/') + 1);
                        }
                    }
            });
        });

        $(".refreshHistory").on('click', function(){
            var _this = $(this);
            var id = _this.attr('data-id');
            $(".back").addClass("refreshPage");
            $(".back").html("Back to current version");
            $(".refreshHistory").css({'font-weight': 'normal', 'color':"#337ab7"});
            $(".set-"+id).css({'font-weight': 'bold', 'color': 'green'});
            UpdateInfo(id);
        });
        function UpdateInfo(id){
            var wwcc_image = $("#wwcc_image-"+id).val();
            $(".wwcc_image").attr('src', wwcc_image);

            var wwcc_number = $("#wwcc_number-"+id).val();
            $(".wwcc_number").html(wwcc_number);

            var last_name = $("#last_name-"+id).val();
            $(".last_name").html(last_name);

            var birthday = $("#birthday-"+id).val();
            $(".birthday").html(birthday);

            var prof_scan_id = $("#prof_scan_id-"+id).val();
            $(".prof_scan_id").attr('src', prof_scan_id);

            var prof_scan_id2 = $("#prof_scan_id2-"+id).val();
            $(".prof_scan_id2").attr('src', prof_scan_id2);

            var prof_id_type = $("#prof_id_type-"+id).val();
            $(".prof_id_type").html(prof_id_type);

            var prof_dateof = $("#prof_dateof-"+id).val();
            $(".prof_dateof").html(prof_dateof);

            var prof_id_sc = $("#prof_id_sc-"+id).val();
            $(".prof_id_sc").html(prof_id_sc);

            var prof_visa = $("#prof_visa-"+id).val();
            $(".prof_visa").attr('src', prof_visa);
            $(".showVisa").css('display', 'block');

            var wwcc_status = $("#wwcc_status-"+id).val();
            $(".showStatus").css('display', 'block');
            if(wwcc_status == 0){
                $(".status").html("submitted");
            }
            if(wwcc_status == 1){
                $(".status").html("Verifed");
            }
            if(wwcc_status == 2){
                $(".status").html("Rejected");
            }
            var date_submit = $("#date_submit-"+id).val();
            $(".date_submit").html(date_submit);

            $(".btn-main-action").css('display', 'none');

            var verifed_id_visa_exp = $("#verifed_id_visa_exp-"+id).val();
            $(".verifed_id_visa_exp").html(verifed_id_visa_exp);

            var verifed_id_expdate = $("#verifed_id_expdate-"+id).val();
            $(".verifed_id_expdate").html(verifed_id_expdate);
        }
        $body = $("body");

        $(document).on({
            ajaxStart: function() { $body.addClass("loading");    },
            ajaxStop: function() { $body.removeClass("loading"); }
        });
    </script>
    <style>
        .form-row{
            margin-top: 20px; !important;
        }

        .spinner-modal {
            display:    none;
            position:   fixed;
            z-index:    1000;
            top:        0;
            left:       0;
            height:     100%;
            width:      100%;
            background: rgba( 255, 255, 255, .8 )
            url('http://i.stack.imgur.com/FhHRx.gif')
            50% 50%
            no-repeat;
        }

        body.loading .spinner-modal {
            overflow: hidden;
        }

        body.loading .modal {
            display: block;
        }
    </style>
@endsection
