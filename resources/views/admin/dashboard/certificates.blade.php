@extends('admin.app')

@section('title', 'Certificate verification')

@section('page-header', 'Certificate verification')
@section('optional-description', ' ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
    <div class="container padd-top-30" id="cetrificates-verifed">
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Certification Name *</strong>
                </div>
                <div class="col-md-4">
                    <span class="name">{{ $certificat->name }}</span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Provider</strong>
                </div>
                <div class="col-md-4">
                    <span class="provider">{{ $certificat->provider }}</span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Description (optional)</strong>
                </div>
                <div class="col-md-4">
                    <span class="description">{{ $certificat->description }}</span>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Date of Issuing *</strong>
                </div>
                <div class="col-md-4">
                    <span class="date_earned">{{ date("d/m/Y", strtotime($certificat->date_earned)) }}</span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Certification Number *</strong>
                </div>
                <div class="col-md-4">
                    <span class="submission">{{ $certificat->submission }}</span>
                </div>
            </div>
            <div class="row form-row showStatus" style="display: none;">
                <div class="col-md-3">
                    <strong>Status (at the moment of changing)</strong>
                </div>
                <div class="col-md-4">
                    <span class="status"></span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            @if($certificat->status == 0)
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ @$_GET['id'] }}" data-status="1" style="background-color: #22b92c !important; border: none;">Verify</a>
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ @$_GET['id'] }}" data-status="2" style="background-color: #FF1F1F !important; border: none;">Reject</a>
                            @endif
                            <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Chat with user</strong>
                </div>
                <div class="col-md-6">
                    @include('admin.dashboard.partial.chat-view', ['chat'=>$user->getVerificationChat('certifications'),'type'=>'certifications', 'user'=>$user, 'panelId'=>'chat_certifications', 'id'=>$certificat->id])
                </div>
            </div>
        </div>
        <div class="container">
            <h2>History of changings</h2>
            {{--<p></p>--}}
            <table class="table table-striped">
                <thead>

                <tr>
                    <th>Name</th>
                    <th>Date of submitting</th>
                    <th>Date of changing</th>
                    <th>Status (at moment of changing)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $certificat->name }}</td>
                    <td>{{ date("d/m/Y H:i:s", strtotime($certificat->created_at)) }}</td>
                    <td style="color: green">Actual</td>
                    <td style="color: green">Actual</td>
                </tr>
                @foreach($historyData as $history)
                    <?php $dataUser = json_decode($history->data); ?>
                    @if($dataUser->id == @$_GET['id'])
                    <tr>
                        @foreach($dataUser as $key=>$data)
                            <?php
                                if(is_object($data)){
                                    $data  = preg_replace("#\..+#is", "", $data->date);
                                }
                            ?>
                            <input type="hidden" id="{{ $key }}-{{ $history->id }}" value="{{ $data }}">
                        @endforeach
                        <td><a data-id ="{{ $history->id }}" class="refreshHistory set-{{ $history->id }}">{{ $dataUser->name }}</td>
                        <td>{{ $history->created_at->format("d/m/Y H:i:s") }}</td>
                        <td>{{ preg_replace("#\..+#is", "", date('d/m/Y', strtotime($dataUser->created_at->date))) }}</td>
                        <td>
                            @if($dataUser->status == 0)
                                submitted
                            @elseif($dataUser->status == 1)
                                <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                            @else
                                <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                            @endif
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
    <script>
        $(".saveform").on('click', function(){
            var user_id = $(this).attr('data-id');
            var statusUser = $(this).attr('data-status');
            $.ajax({
                type: "get",
                data: {status: statusUser, "user":user_id},
                success: function(done){
                    if(done == 1){
                        location.reload();
                    }
                }
            });
        });
        $(".actions").on('click', ".refreshPage", function(e){
            e.preventDefault();
            location.reload();
        });


        $(".refreshHistory").on('click', function(){
            var _this = $(this);
            var id = _this.attr('data-id');
            $(".back").addClass("refreshPage");
            $(".back").html("Back to current version");
            $(".refreshHistory").css({'font-weight': 'normal', 'color':"#337ab7"});
            $(".set-"+id).css({'font-weight': 'bold', 'color': 'green'});
            UpdateInfo(id);
        });
        function UpdateInfo(id){
            var name = $("#name-"+id).val();
            $(".name").html(name);

            var provider = $("#provider-"+id).val();
            $(".provider").html(provider);

            var description = $("#description-"+id).val();
            $(".description").html(description);

            var notes = $("#notes-"+id).val();
            $(".notes").html(notes);

            var date_earned = $("#date_earned-"+id).val();
            $(".date_earned").html(date_earned);

            var submission = $("#submission-"+id).val();
            $(".submission").html(submission);

            var updated_at = $("#updated_at-"+id).val();
            $(".updated_at").html(updated_at);

            var created_at = $("#created_at-"+id).val();
            $(".created_at").html(created_at);



            var status = $("#status-"+id).val();
            $(".showStatus").css('display', 'block');
            if(status == 0){
                $(".status").html("submitted");
            }
            if(status == 1){
                $(".status").html("Verifed");
            }
            if(status == 2){
                $(".status").html("Rejected");
            }
            $(".btn-main-action").css('display', 'none');

        }
    </script>
    <style>
        .form-row{
            margin-top: 20px; !important;
        }
    </style>
@endsection
