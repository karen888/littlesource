@extends('admin.app')

@section('title', 'Board user')

@section('page-header', 'Board user '. $user->first_name." ".$user->last_name . " (" . $user->email . ")")
@section('optional-description', ' ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')

    <div id="get-verified">
        <div class="" style="margin-top: 20px;">
            <div class="container">
                @if($user->user_photo_100)
                <img src="{{ $user->user_photo_100 }}" width="100px" alt="">
                @endif
                <h4>Information</h4>
                <div class="table-hover">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>First Name</td>
                            <td>{{ $user->first_name }}</td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td>{{ $user->last_name }}</td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td>@if(!$user->employee_info || $user->employee_info->birthday == '') - @else {{ date("d/m/Y",strtotime($user->employee_info->birthday)) }} @endif</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>@if(!$user->phone || $user->phone->phone == '') - @else {{ $user->phone->phone }} @endif</td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>
                                @if($user->location)
                                    Street:  @if($user->location->street == '') - @else {{ $user->location->street }} @endif <br>
                                    City/Suburb: @if($user->location->suburb == '') - @else {{ $user->location->suburb }} @endif <br>
                                    State: @if($user->location->state == '') - @else {{ $user->location->state }} @endif <br>
                                    Post Code: @if($user->location->post_code == '') - @else {{ $user->location->post_code }} @endif <br>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Short Bio</td>
                            <td>
                                @if($user->employee_info && $user->employee_info->overview)
                                    {{ substr($user->employee_info->overview, 0, 100) }} @if(strlen($user->employee_info->overview) > 100) ... <a data-toggle="modal" data-target="#overview-modal" href="#">view more</a> @endif
                                @else
                                -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Video Presentation</td>
                            <td>
                                @if($user->video_id || $user->video_url)
                                <a data-toggle="modal" data-target="#video-modal" href="#">Watch</a>
                                @else
                                -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Years of Experience</td>
                            <td>{{ $user->years_experience }}</td>
                        </tr>
                        <tr>
                            <td>More</td>
                            <td>
                                @if($user->more_about_me['non_smoker'])
                                    Non-Smoker<br>
                                @endif
                                @if($user->more_about_me['own_children'])
                                    Has own children<br>
                                @endif
                                @if($user->more_about_me['pets'])
                                    Comfortable with Pets<br>
                                @endif
                                @if($user->more_about_me['own_car'])
                                    Has own car<br>
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>What I offer</td>
                            <td>
                                @foreach ($user->service as $service)
                                    {{ $service->name }}<br>
                                @endforeach
                            </td>
                        </tr>
                        <tr>
                            <td>Age Groups</td>
                            <td>
                                @if($user->childAge)
                                    @foreach ($user->childAge as $child_age)
                                        {{ $child_age->name }}<br>
                                    @endforeach
                                @else
                                -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Maximum Children per Booking</td>
                            <td>{{ $user->max_per_booking }}</td>
                        </tr>
                        <tr>
                            <td>Maximum Distance Willing to Travel</td>
                            <td>{{ $user->location_range }}</td>
                        </tr>
                        <tr>
                            <td>Availability</td>
                            <td>
                                @if($user->availability)
                                    @foreach ($user->availability as $availability)
                                        {{ $availability->name }}<br>
                                    @endforeach
                                @else
                                -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <td>Hourly Rate</td>
                            <td>@if(!$user->employee_info || $user->employee_info->rate == '') - @else $ {{ $user->employee_info->rate }} @endif</td>
                        </tr>
                        <tr>
                            <td>Skills</td>
                            <td>
                                @if($user->skill)
                                    @foreach ($user->skill as $sk)
                                        {{ $sk->name }}<br>
                                    @endforeach
                                @else
                                -
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="pull-right">
                    <a href="/admin/etc" type="button" class="btn btn-themed btn-preview back" >Back</a>
                </div>
            </div>
        </div>
    </div>

    @if($user->employee_info && $user->employee_info->overview)
    <div id="overview-modal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Short Bio</h4>
          </div>
          <div class="modal-body">
            {!! nl2br(e($user->employee_info->overview)) !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    @endif

    @if($user->video_id || $user->video_url)
    <div id="video-modal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Video Presentation</h4>
          </div>
          <div class="modal-body">
            @if($user->video_id)
            <iframe style="max-width: 100%;" src="https://www.youtube.com/embed/{{ $user->video_id }}">
            </iframe>
            @endif
            @if(!$user->video_id && $user->video_url)
            <video controls style="max-width: 100%;">
                <source src="{{ $user->video_url }}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                <source src="{{ $user->video_url }}" type='video/ogg; codecs="theora, vorbis"'>
                <source src="{{ $user->video_url }}" type='video/webm; codecs="vp8, vorbis"'>
                Your browser does not support HTML5 video.
            </video>
            @endif
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>

      </div>
    </div>
    @endif
@endsection
