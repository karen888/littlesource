@extends('admin.app')

@section('title', 'Passport verification')

@section('page-header', 'Passport verification (' . $user->getNameAttribute() . ')')
@section('optional-description', ' ')

@section('breadcrumbs')
{!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
<div class="container padd-top-30">
    <div class="verify-form job-form"  >
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Passport Photo</strong>
            </div>
            <div class="col-md-4">
                <span>@if(!$user->passport) - @else <img src="{{ $user->passport['photo'] }}" width="100px" alt=""> @endif</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Travel Document Number</strong>
            </div>
            <div class="col-md-4">
                <span>@if(!$user->passport) - @else {{ $user->passport['id_number'] }} @endif</span>
            </div>
        </div>
       
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Passport only shows year of birth</strong>
            </div>
            <div class="col-md-4">
                <span>@if(!$user->passport) - 
                @else 
                    @if($user->passport['year_only']) Yes @else No  @endif
                @endif</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Status</strong>
            </div>
            <div class="col-md-4">
                <span>
                    @if(!$user->passport)
                        not submitted
                    @else
                        @if($user->passport['verified'] == 0)
                            <i class="fa fa-refresh"></i>&nbspsubmitted
                        @elseif($user->passport['verified'] == 1)
                            <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                        @elseif($user->passport['verified'] == -1)
                            <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                        @endif
                    @endif
                </span>
            </div>
        </div>
        @if ($user->passport && $user->allow_dvs_check && $user->passport['verified'] != 1)
        <div class="row form-row">
            <div class="col-md-3">
                <strong>DVS Check</strong>
            </div>
            <div class="col-md-4">
                <a class="btn btn-themed btn-primary" href="{{ route('admin.board.passport-dvs', $user->id) }}">Check now</a>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="full-width">
                    <div class="actions">
                        <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
