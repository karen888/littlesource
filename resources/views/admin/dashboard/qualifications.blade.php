@extends('admin.app')

@section('title', 'Qualifications verification')

@section('page-header', 'Qualifications verification')
@section('optional-description', ' ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
    <div class="container padd-top-30" id="qualification-verify" v-cloak>
        <div class="row">
            <div class="col-md-12">
                <div class="main-heading">
                    <h4>Qualification</h4>
                </div>
            </div>
        </div>
        <hr>
        <div class="verify-form job-form" >
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Name *</strong>
                </div>
                <div class="col-md-4">
                    @if($qualification['name'] == '')- @endif
                    <span class="school">{{ $qualification['name']}}</span>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Completion Year *</strong>
                </div>
                <div class="col-md-4">
                    <span class="dates_attended_from">{{ $qualification['completion_year'] }}</span>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Institution</strong>
                </div>
                <div class="col-md-4">
                    @if($qualification['institution'] == '')- @endif
                    <span class="degree">{{ $qualification['institution'] }}</span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Scan of my qualification*</strong>
                </div>
                <div class="col-md-4">
                    @if($qualification['path'] == '')-
                         @else

                        @include('admin.dashboard.partial.scans_preview', ['file' => $qualification['path']])


                    @endif

                </div>
            </div>
{{--            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Description (optional)</strong>
                </div>
                <div class="col-md-4">
                    @if($qualification->description == '')- @endif
                    <span class="description">{{ $qualification->description }}</span>
                </div>
            </div>--}}

            <div class="row form-row showStatus" style="display: none;">
                <div class="col-md-3">
                    <strong>Status (at the moment of changing)</strong>
                </div>
                <div class="col-md-4">
                    <span class="status"></span>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            @if($qualification['verified'] == 0)
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $qualification['pivot_user_id'] }}" data-status="1" style="background-color: #22b92c !important; border: none;">Verify</a>
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $qualification['pivot_user_id'] }}" data-status="2" style="background-color: #FF1F1F !important; border: none;">Reject</a>
                            @endif
                            <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Chat with user</strong>
                </div>
                <div class="col-md-6">
                    @include('admin.dashboard.partial.chat-view', ['chat'=>$user->getVerificationChat('qualification'),'type'=>'qualification', 'user'=>$user, 'panelId'=>'chat_qualifications', 'id'=>$qualification['id']])
                </div>
            </div>
        </div>
        <div class="container">
            <h2>History of changings</h2>
            {{--<p></p>--}}
            <table class="table table-striped">
                <thead>

                <tr>
                    <th>Name</th>
                    <th>Date of submitting</th>
                    <th>Date of changing</th>
                    <th>Status (at moment of changing)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $qualification['name'] }}</td>
                    <td>{{ date("d/m/Y",strtotime($qualification['created_at'])) }}</td>
                    <td style="color: green">Actual</td>
                    <td style="color: green">Actual</td>
                </tr>
                @foreach($historyData as $history)
                    <?php $dataUser = json_decode($history->data);
                    if(json_last_error() !== JSON_ERROR_NONE) {
                        continue;
                    }
                    ?>
                    @if($dataUser->id == @$_GET['id'])
                        <tr>
                            @foreach($dataUser as $key=>$data)
                                <?php
                                if(is_object($data)){
                                    $data  = preg_replace("#\..+#is", "", $data->date);
                                }
                                ?>
                                <input type="hidden" id="{{ $key }}-{{ $history->id }}" value="{{ $data }}">
                            @endforeach
                            <td><a data-id ="{{ $history->id }}" class="refreshHistory set-{{ $history->id }}">{{ $dataUser->school }}</td>
                            <td>{{ $history->created_at->format("d/m/Y H:i:s") }}</td>
                            <td>{{ date("d/m/Y H:i:s", strtotime($dataUser->created_at->date)) }}</td>
                            <td>
                                @if($dataUser->status == 0)
                                    submitted
                                @elseif($dataUser->status == 1)
                                    <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                                @else
                                    <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                                @endif
                            </td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
    <script>
        $(".saveform").on('click', function(){
            var user_id = $(this).attr('data-id');
            var statusUser = $(this).attr('data-status');
            $.ajax({
                type: "get",
                data: {status: statusUser, "user":user_id},
                success: function(done){
                    if(done == 1){
                        location.reload();
                    }
                }
            });
        });
        $(".actions").on('click', ".refreshPage", function(e){
            e.preventDefault();
            location.reload();
        });


        $(".refreshHistory").on('click', function(){
            var _this = $(this);
            var id = _this.attr('data-id');
            $(".back").addClass("refreshPage");
            $(".back").html("Back to current version");
            $(".refreshHistory").css({'font-weight': 'normal', 'color':"#337ab7"});
            $(".set-"+id).css({'font-weight': 'bold', 'color': 'green'});
            UpdateInfo(id);
        });
        function UpdateInfo(id){
            var school = $("#school-"+id).val();
            $(".school").html(school);

            var degree = $("#degree-"+id).val();
            $(".degree").html(degree);

            var scan = $("#scan-"+id).val();
            $(".scan").attr('src', scan);

            var area_study = $("#area_study-"+id).val();
            $(".area_study").html(area_study);

            var dates_attended_from = $("#dates_attended_from-"+id).val();
            $(".dates_attended_from").html(dates_attended_from);

            var dates_attended_to = $("#dates_attended_to-"+id).val();
            $(".dates_attended_to").html(dates_attended_to);

            var description = $("#description-"+id).val();
            $(".description").html(description);

            var updated_at = $("#updated_at-"+id).val();
            $(".updated_at").html(updated_at);

            var created_at = $("#created_at-"+id).val();
            $(".created_at").html(created_at);



            var status = $("#status-"+id).val();
            $(".showStatus").css('display', 'block');
            if(status == 0){
                $(".status").html("submitted");
            }
            if(status == 1){
                $(".status").html("Verifed");
            }
            if(status == 2){
                $(".status").html("Rejected");
            }
            $(".btn-main-action").css('display', 'none');
        }
    </script>
    <style>
        .form-row{
            margin-top: 20px; !important;
        }
    </style>
@endsection
