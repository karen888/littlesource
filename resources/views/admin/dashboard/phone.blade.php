@extends('admin.app')

@section('title', 'Phone Number verification')

@section('page-header', 'Phone Number verification')
@section('optional-description', ' ')

@section('breadcrumbs')
{!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
<div class="container padd-top-30">
    <div class="verify-form job-form"  >
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Mobile Number</strong>
            </div>
            <div class="col-md-4">
                <span class="phone">{{ $phone ? $phone->phone : '' }}</span>
            </div>
        </div>   
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Updated At</strong>
            </div>
            <div class="col-md-4">
                <span class="phone">{{ $phone ? date("d/m/Y H:i:s", strtotime($phone->updated_at)) : '' }}</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Status</strong>
            </div>
            <div class="col-md-4">
                <span class="phone">
                    @if(!$phone || $phone->phone == '')
                        not submitted
                    @else
                        @if($phone->verified == 0)
                            <i class="fa fa-refresh"></i>&nbspsubmitted
                        @elseif($phone->verified == 1)
                            <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                        @else
                            <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                        @endif
                    @endif
                </span>
            </div>
        </div>  
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="full-width">
                    <div class="actions">
                        <a href="/admin/etc/board/{{ $user_id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                    </div>
                </div>
            </div>
        </div>		
    </div>   
</div>
@endsection
