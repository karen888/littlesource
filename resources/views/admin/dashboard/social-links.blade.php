@extends('admin.app')

@section('title', 'Social links verification')

@section('page-header', 'Social links verification')
@section('optional-description', ' ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
    <div class="container padd-top-30" id="socialLinks" v-cloak>
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Social links </strong>
                </div>
                <div class="col-md-7">
                    <div class="social type">
                        <img style="margin-right: 15px; width: 50px;" src="http://head-media.ru/userfiles/image/reklama-na-facebook.png" />
                        <img style="margin-right: 15px; width: 50px;" src="https://static.licdn.com/scds/common/u/images/logos/favicons/v1/favicon.ico" />
                        <img style="margin-right: 15px; width: 50px;" src="https://avatanplus.com/files/resources/original/5761ba6c7938615555c037d1.png" />
                        <img style="margin-right: 15px; width: 50px;" src="http://icons.iconarchive.com/icons/marcus-roberto/google-play/128/Google-plus-icon.png" />
                    </div>
                    <div  style="margin-top: 20px; margin-bottom: 20px;">
                    <span class="soc_link">
                        <a href="{{ $user->info->soc_link }}">{{ $user->info->soc_link }} </a>


                        @if(($social_add = json_decode($user->info->soc_links_notes, true)) && json_last_error() === JSON_ERROR_NONE)

                            @foreach($social_add as $link)
                                <br/>
                                <a target="_blank" rel="noopener" href="{{$link['text']}}">{{$link['text']}}</a>

                            @endforeach
                        @endif
                    </span>
                        </div>
                </div>
            </div>
            <div class="row form-row showStatus" style="display: none;">
                <div class="col-md-3">
                    <strong>Status (at the moment of changing)</strong>
                </div>
                <div class="col-md-4">
                    <span class="status"></span>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Submision date</strong>
                </div>
                <div class="col-md-4">
                        <span class="soc_link_submit">{{ @date('d/m/Y H:i:s', @$user->info->soc_link_submit) }}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            @if($user->social_verifed == 0)
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="1" style="background-color: #22b92c !important; border: none;">Verify</a>
                                <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="2" style="background-color: #FF1F1F !important; border: none;">Reject</a>
                            @endif
                            <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Chat with user</strong>
                </div>
                <div class="col-md-6">
                    @include('admin.dashboard.partial.chat-view', ['chat'=>$user->getVerificationChat('social'),'type'=>'social', 'user'=>$user, 'panelId'=>'chat_social', 'id'=>null])
                </div>
            </div>
        </div>
        <div class="container">
            <h2>History of changings</h2>
            {{--<p></p>--}}
            <table class="table table-striped">
                <thead>

                <tr>
                    <th>WWCC</th>
                    <th>Date of submitting</th>
                    <th>Date of changing</th>
                    <th>Status (at moment of changing)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{{ $user->info->soc_link }}</td>
                    <td>{{ date("d/m/Y H:i:s", $user->info->soc_link_submit) }}</td>
                    <td style="color: green">Actual</td>
                    <td style="color: green">Actual</td>
                </tr>
                @foreach($historyData as $history)
                    <?php $dataUser = json_decode($history->data); ?>
                    <tr>
                        @foreach($dataUser as $key=>$data)
                            <input type="hidden" id="{{ $key }}-{{ $history->id }}" value="{{ $data }}">
                        @endforeach
                        <td><a data-id ="{{ $history->id }}" class="refreshHistory set-{{ $history->id }}">{{ $dataUser->soc_link }}</td>
                        <td>{{ $history->created_at->format("d/m/Y") }}</td>
                        <td>{{ date("d/m/Y H:i:s", $dataUser->soc_link_submit) }}</td>
                        <td>
                            @if($dataUser->social_verifed == 0)
                                submitted
                            @elseif($dataUser->social_verifed == 1)
                                <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                            @else
                                <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
    <script>
        $(".saveform").on('click', function(){
            var user_id = $(this).attr('data-id');
            var statusUser = $(this).attr('data-status');
//            var wwcc_status_expdate = $("#wwcc_status_expdate").val();
            $.ajax({
                type: "get",
                data: {status: statusUser, "user":user_id },
                success: function(done){
                    if(done == 1){
                        location.reload();
                    }
                }
            });
        });
        $(".actions").on('click', ".refreshPage", function(e){
            e.preventDefault();
            location.reload();
        });


        $(".refreshHistory").on('click', function(){
            var _this = $(this);
            var id = _this.attr('data-id');
            $(".back").addClass("refreshPage");
            $(".back").html("Back to current version");
            $(".refreshHistory").css({'font-weight': 'normal', 'color':"#337ab7"});
            $(".set-"+id).css({'font-weight': 'bold', 'color': 'green'});
            UpdateInfo(id);
        });
        function UpdateInfo(id){

            var soc_link = $("#soc_link-"+id).val();
            $(".soc_link").html(soc_link);

            var social_verifed = $("#social_verifed-"+id).val();
            $(".showStatus").css('display', 'block');
            if(social_verifed == 0){
                $(".status").html("submitted");
            }
            if(social_verifed == 1){
                $(".status").html("Verified");
            }
            if(social_verifed == 2){
                $(".status").html("Rejected");
            }

            var soc_link_submit = $("#soc_link_submit-"+id).val();
            $(".soc_link_submit").html(timeConverter(soc_link_submit));

            $(".btn-main-action").css('display', 'none');
        }


        function timeConverter(UNIX_timestamp){
            var a = new Date(UNIX_timestamp * 1000);
//            var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
//            var month = a.getMonth();
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
//            var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
            var time = year+'-'+month+"-"+date+" "+hour+":"+min+":"+sec;
            return time;
        }


    </script>
    <style>
        .form-row{
            margin-top: 20px; !important;
        }
    </style>
@endsection
