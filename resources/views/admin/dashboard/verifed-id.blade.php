@extends('admin.app')

@section('title', 'ID verification')

@section('page-header', 'ID verification')
@section('optional-description', ' ')

@section('breadcrumbs')
{!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')
<div class="container padd-top-30" id="verifed-id" v-cloak>
    <hr>
    <div class="verify-form job-form">
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Profile photo * </strong>
            </div>
            <div class="col-md-9">
                <div class="profile photo">
                    <div class="col-md-2" style="padding: 0;">
                        @if($user->user_photo_100 == '')No photo @endif
                        <img  class = 'profilePhoto user_photo_100' width="125px" src="{{ $user->user_photo_100 }}" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>First name *</strong>
            </div>
            <div class="col-md-4">
               <span class="first_name">{{ $user->first_name }}</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Last name *</strong>
            </div>
            <div class="col-md-4">
                <span class="last_name">{{ $user->last_name }}</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Date of Birth *</strong>
            </div>
            <div class="col-md-4">
                <span class="birthday">{{ date("d/m/Y", strtotime($user->employee_info->birthday)) }}</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Scan of ID * </strong>
            </div>
            <div class="col-md-4">
                @if($user->info->prof_scan_id == '') - @else
                @include('admin.dashboard.partial.scans_preview', ['file' => $user->info->prof_scan_id])
                    @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Your photo with document*</strong>
            </div>
            <div class="col-md-4">
                @if($user->government_id && $user->government_id->user_holding_document_photo)
                    @include('admin.dashboard.partial.scans_preview', ['file' => $user->government_id->user_holding_document_photo])
                @else
                    No photo 
                @endif
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>ID Type *</strong>
            </div>
            <div class="col-md-4">
                @if($user->info->prof_id_type == '')- @endif
                <span class="prof_id_type">{{ $user->info->prof_id_type }}</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>ID Issuing Country *</strong>
            </div>
            <div class="col-md-4">
                @if($user->info->prof_id_sc == '')- @endif
                <span class="prof_id_sc">{{ $user->info->prof_id_sc }}</span>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Date of issuing *</strong>
            </div>
            <div class="col-md-4">
                <span class="prof_dateof">


                    @if( $user->info->prof_dateof !== null && \Carbon\Carbon::createFromFormat('d/m/Y', $user->info->prof_dateof)->getTimestamp() == 0)
                    -
                    @else
                    {{ $user->info->prof_dateof }}
                    @endif
                </span>
            </div>
        </div>

        <div class="row form-row">
            <div class="col-md-3">
                <strong>Expiry Date</strong>
            </div>
            <div class="col-md-4">
                @if($user->verifed_id == 0)
                    <input id="verifed_id_expdate" data-inputmask="'alias': 'date'" type="text" name="prof_dateof"/>
                @else
                    <span class="verifed_id_expdate">
                        @if($user->verifed_id_expdate == 0)
                            -
                        @else
                        {{ date('d/m/Y', $user->verifed_id_expdate) }}
                        @endif
                    </span>
                @endif
            </div>
        </div>

        <div class="showVisa" style="@if($user->info->prof_visa == '') display:none; @endif">
            <div class="row form-row " >
                <div class="col-md-3">
                    <strong>My Working Visa</strong>
                </div>
                <div class="col-md-4">
                    <a href="{{ $user->info->prof_visa }}" data-lightbox="image-3" data-title="My caption"><img  class = 'profilePhoto prof_visa' width="300px" src="{{ $user->info->prof_visa }}" /></a>
                    @if($user->info->prof_visa == '')No photo @endif
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Visa Expiry Date</strong>
                </div>
                <div class="col-md-4">
                    @if($user->verifed_id == 0)
                        <input id="verifed_id_visa_exp" data-inputmask="'alias': 'date'" type="text" name="prof_dateof"/>
                    @else
                        <span class="verifed_id_visa_exp">
                            @if($user->verifed_id_visa_exp == 0)
                                -
                            @else
                            {{ date("d/m/Y", $user->verifed_id_visa_exp) }}
                            @endif
                        </span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Date of Submission</strong>
            </div>
            <div class="col-md-4">

                <span class="date_submit">{{ $user->updated_at->format("d/m/Y") }}</span>

            </div>
        </div>

        <div class="row form-row showStatus" style="display: none;">
            <div class="col-md-3">
                <strong>Status (at the moment of changing)</strong>
            </div>
            <div class="col-md-4">
                <span class="status"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="full-width">
                    <div class="actions">
                        @if($user->verifed_id == 0)
                        <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="1" style="background-color: #22b92c !important; border: none;">Verify</a>
                        <a class="btn btn-themed btn-primary btn-main-action saveform" data-id="{{ $user->id }}" data-status="2" style="background-color: #FF1F1F !important; border: none;">Reject</a>
                        @endif
                        <a href="/admin/etc/board/{{ $user->id }}" type="button" class="btn btn-themed btn-preview back" >Back</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Chat with user</strong>
            </div>
            <div class="col-md-6">
                @include('admin.dashboard.partial.chat-view', ['chat'=>$user->getVerificationChat('prof_id'),'type'=>'prof_id', 'user'=>$user, 'panelId'=>'chat_prof_id', 'id'=>null])
            </div>
        </div>
    </div>

    <div class="container">
        <h2>History of changings</h2>
        {{--<p></p>--}}
        <table class="table table-striped">
            <thead>

            <tr>
                <th>First Name</th>
                <th>Date of submitting</th>
                <th>Date of changing</th>
                <th>Status (at moment of changing)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $user->first_name }}</td>
                <td>{{ $user->created_at->format("d/m/Y") }}</td>
                <td style="color: green">Actual</td>
                <td style="color: green">Actual</td>
            </tr>
            @foreach($historyData as $history)
               <?php
                $dataUser = json_decode($history->data);

                if(false === is_array($dataUser) && json_last_error() !== JSON_ERROR_NONE) {
                    $dataUser = [];
                }

               ?>
            <tr>
                @if(count($dataUser ))
                @foreach($dataUser as $key=>$data)
                    <input type="hidden" id="{{ $key }}-{{ $history->id }}" value="{{ $data }}">
                @endforeach
                <td><a data-id ="{{ $history->id }}" class="refreshHistory set-{{ $history->id }}">{{ $dataUser->first_name }}</td>
                <td>{{ $history->created_at->format('d/m/Y') }}</td>
                <td>{{ date('d/m/Y', strtotime($dataUser->date_submit)) }}</td>
                <td>
                    @if($dataUser->verifed_id == 0)
                        submitted
                    @elseif($dataUser->verifed_id == 1)
                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                    @else
                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                    @endif
                </td>
                @endif
            </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>
    <script>
        $(".saveform").on('click', function(){
            var user_id = $(this).attr('data-id');
            var statusUser = $(this).attr('data-status');
            var verifed_id_visa_exp = $("#verifed_id_visa_exp").val();
            var verifed_id_expdate = $("#verifed_id_expdate").val();

            var go = true;
            if(statusUser == 1){
                if(verifed_id_expdate == ''){
                    go = false;
                    alert("Expiry Date");
                }
            }
            if(go == true) {
                $.ajax({
                    type: "get",
                    data: {
                        status: statusUser,
                        "user": user_id,
                        'verifed_id_visa_exp': verifed_id_visa_exp,
                        'verifed_id_expdate': verifed_id_expdate
                    },
                    success: function (done) {
                        if (done == 1) {
                            location.reload();
                        }
                    }
                });
            }
        });
        $(".actions").on('click', ".refreshPage", function(e){
            e.preventDefault();
            location.reload();
        });


        $(".refreshHistory").on('click', function(){
            var _this = $(this);
            var id = _this.attr('data-id');
            $(".back").addClass("refreshPage");
            $(".back").html("Back to current version");
            $(".refreshHistory").css({'font-weight': 'normal', 'color':"#337ab7"});
            $(".set-"+id).css({'font-weight': 'bold', 'color': 'green'});
            UpdateInfo(id);
        });
        function UpdateInfo(id){
            var user_photo_100 = $("#user_photo_100-"+id).val();
            $(".user_photo_100").attr('src', user_photo_100);

            var first_name = $("#first_name-"+id).val();
            $(".first_name").html(first_name);

            var last_name = $("#last_name-"+id).val();
            $(".last_name").html(last_name);

            var birthday = $("#birthday-"+id).val();
            $(".birthday").html(birthday);

            var prof_scan_id = $("#prof_scan_id-"+id).val();
            $(".prof_scan_id").attr('src', prof_scan_id);

            var prof_scan_id2 = $("#prof_scan_id2-"+id).val();
            $(".prof_scan_id2").attr('src', prof_scan_id2);

            var prof_id_type = $("#prof_id_type-"+id).val();
            $(".prof_id_type").html(prof_id_type);

            var prof_dateof = $("#prof_dateof-"+id).val();
            $(".prof_dateof").html(prof_dateof);

            var prof_id_sc = $("#prof_id_sc-"+id).val();
            $(".prof_id_sc").html(prof_id_sc);

            var prof_visa = $("#prof_visa-"+id).val();
            $(".prof_visa").attr('src', prof_visa);
            $(".showVisa").css('display', 'block');

            var verifed_id = $("#verifed_id-"+id).val();
            $(".showStatus").css('display', 'block');
            if(verifed_id == 0){
                $(".status").html("submitted");
            }
            if(verifed_id == 1){
                $(".status").html("Verifed");
            }
            if(verifed_id == 2){
                $(".status").html("Rejected");
            }
            var date_submit = $("#date_submit-"+id).val();
            $(".date_submit").html(date_submit);

            $(".btn-main-action").css('display', 'none');

            var verifed_id_visa_exp = $("#verifed_id_visa_exp-"+id).val();
            $(".verifed_id_visa_exp").html(verifed_id_visa_exp);

            var verifed_id_expdate = $("#verifed_id_expdate-"+id).val();
            $(".verifed_id_expdate").html(verifed_id_expdate);
        }
    </script>
    <style>
        .form-row{
            margin-top: 20px; !important;
        }
    </style>
@endsection
