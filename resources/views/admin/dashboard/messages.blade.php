@extends('admin.app')

@section('title', 'Messages')

@section('page-header', 'Messages ')
@section('optional-description', ' ')

@section('breadcrumbs')
    {!! Breadcrumbs::render('dashboard') !!}
@endsection
@section('content')

    <div class="col-md-12">
        <table class="table table-hover table-responsive table-bordered table-condensed">
            <thead>
                <tr>
                    <th>User name</th>
                    <th>Verification type</th>
                    <th>Unread messages</th>
                    <th></th>
                </tr>
            </thead>
            @foreach($chats as $chat)
            <tr>
                <td><a href="{{route('admin.board', ['id'=>$chat->user->id])}}">{{$chat->user->first_name}}</a></td>
                <td>{{ucfirst($chat->verification_type)}}</td>
                <td><span class="badge" id="unreadBadge_{{$chat->user_id}}_{{$chat->verification_type}}_{{$chat->verification_id}}">{{$chat->unread}}</span></td>
                <td><a href="<?php
                    switch($chat->verification_type) {
                        case 'wwcc':
                            echo route('admin.board.wwcc', [$chat->user_id]);
                            break;
                        case 'education':
                            echo route('admin.board.qualification', ['id'=>$chat->user_id]).'?id=' . $chat->verification_id;
                            break;
                        case 'prof_id':
                            echo route('admin.board.id', [$chat->user_id]);
                            break;
                        case 'address':
                            echo route('admin.board.address', [$chat->user_id]);
                            break;
                        case 'certifications':
                            echo route('admin.board.certificates', [$chat->user_id]) . '?id=' . $chat->verification_id;
                            break;
                        case 'abn':
                            echo route('admin.board.abn', [$chat->user_id]);
                            break;
                        case 'social':
                            echo route('admin.board.social-links', [$chat->user_id]);
                            break;
                        default:
                            echo 'javascript:alert(\'No route found for this type\');';
                            break;

                    }
                    ?>">Go to chat</a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
@endsection