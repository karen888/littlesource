@if(preg_match("#base64|jpeg|png|jpg|gif|bmp#i", $file))
    <a class="fancybox" rel="group" href="{{$file}}">
        <img data-lightbox="scan" caption="asdd" width="100px" src="{{$file }}" />
    </a>

@else
    <a target="_blank" href="{{ $file }}"><i class="fa fa-external-link"></i> Attachment [{{@explode(".", $file)[1]}}]</a>
@endif