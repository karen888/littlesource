<?php
$expand = true;
?>
<div class="panel-collapse {{isset($expand) && $expand === true ? "" : "collapse"}}" id="{{$panelId}}" style="max-width: 600px;margin:0 auto">
    <div class="panel-body" id="chat_view_{{$panelId}}">
        <ul class="chat" id="chat_{{$panelId}}">
            @foreach($chat as $msg)
            <li class="{{$msg->message_sender == 'admin' ? 'left' : 'right'}} clearfix"><span class="chat-img pull-{{$msg->message_sender == 'admin' ? 'left' : 'right'}}">
                            <img width="50" height="50" src="{{$msg->message_sender == 'admin' ? 'http://placehold.it/50/55C1E7/fff&text=ADMIN' : ($user->user_photo_100 != '' ? $user->user_photo_100 :'/img/no_photo_100.jpg')}}" alt="User Avatar" class="img-circle" />
                        </span>
                <div class="chat-body clearfix">
                    <div class="header">
                        @if($msg->message_sender == 'admin')
                        <strong class="primary-font">Admin</strong> <small class="pull-right text-muted">
                            <i class="fa fa-clock-o"></i> {{$msg->created_at->format("H:i:s d.m.Y")}}</small>
                        @else
                            <small class=" text-muted"><i class="fa fa-clock-o"></i> {{$msg->created_at->format("H:i:s d.m.Y")}}</small>
                            <strong class="pull-right primary-font">{{$user->first_name}}</strong>
                        @endif
                    </div>
                    <p>
                        {{$msg->message}}
                    </p>
                </div>
            </li>
            @endforeach
        </ul>
    </div>


    <div class="panel-footer">
        <div class="input-group">
            <input id="chat_{{$panelId}}_message" type="text" class="form-control input-sm chat-message-input" placeholder="Type your message here..." />
            <span class="input-group-btn">
                            <button class="btn btn-default ver btn-sm btn-send-{{$panelId}}" id="btn-chat">
                                Send</button>
                        </span>
        </div>
    </div>
</div>
<script>





    $(function(){
        var onMessage = function(message){
            console.log(message);

            if(message.type == '{{$type}}' && (message.id == '' || message.id == '{{$id}}')) {
                var template = '<li class="'+(message.sender === 'admin' ? 'left' : 'right')+' clearfix"><span class="chat-img pull-'+(message.sender === 'admin' ? 'left' : 'right')+'">\
                    <img width="50" height="50" src="'+(message.sender === 'admin' ? 'http://placehold.it/50/55C1E7/fff&text=ADMIN' : '{{($user->user_photo_100 != '' ? strtr($user->user_photo_100,["\r\n" => "\\\r\n"]) :'/img/no_photo_100.jpg')}}')+'" alt="User Avatar" class="img-circle" />\
                    </span>\
                    <div class="chat-body clearfix">\
                    <div class="header">\
                        '+(message.sender === 'admin' ? '\
                    <strong class="primary-font">Admin</strong> <small class="pull-right text-muted">\
                    <i class="fa fa-clock-o"></i> Now</small>\
                        ' : '\
                    <small class=" text-muted"><i class="fa fa-clock-o"></i> Now</small>\
                    <strong class="pull-right primary-font">{{$user->first_name}}</strong>\
                        ')+'\
                    </div>\
                    <p>\
                        '+message.text+'\
                    </p>\
                    </div>\
                    </li>';

                document.getElementById('chat_{{$panelId}}').innerHTML += template;
                scrollBottom();
            }
        };

        var scrollBottom = function() {
            document.getElementById('chat_view_{{$panelId}}').scrollTop = document.getElementById('chat_view_{{$panelId}}').scrollHeight;
        };

        window.setTimeout(scrollBottom, 200);

        function stripTags(str) {
            return str.replace(/<\/?[^>]+>/gi, '');
        }




        var sendMsg = function(e) {

            var text = document.getElementById('chat_{{$panelId}}_message');
            var val = text.value;

            val = stripTags(val);

            if(val.replace(/\s/g, "").length === 0) {
                text.value = '';
                return;
            }

            $('.btn-send-{{$panelId}}').attr('disabled', true);
            text.disabled = true;


            onMessage({type: '{{$type}}', id: '{{$id}}', text: text.value, sender: 'admin'});

            $.ajax({
                url: '{{route('admin.messages.send')}}',
                type: 'POST',
                data: {user_id: '{{$user->id}}', type: '{{$type}}', id: '{{$id}}', text: text.value, _token: window._csrf._token},
                success:function(){

                    e.preventDefault();
                    e.stopPropagation();

                    text.value = '';
                    $('.btn-send-{{$panelId}}').attr('disabled', false);
                    text.disabled = false;
                }
            });


        };

        $('.btn-send-{{$panelId}}').on('click', sendMsg);
        $('#chat_{{$panelId}}_message').on('keydown', function(e){
            if(e.keyCode === 13) {
                e.preventDefault();
                sendMsg(e);
            }
        });


    })
</script>

