@extends('pre-launch-docs-layout')

@section('content')

<div class="container">

    <div class="back-button">
        <a class="Prelaunch__beta--back" href="{{ URL::previous() }}"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back to Previous Page</a>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h1>LITTLE ONES COMPETITIONS TERMS AND CONDITIONS</h1>

            <p>
                By entering the Little Ones Pre-Register & Win competition (“Competition”) you agree to the following terms and conditions (“Terms and Conditions”) and Privacy Statement:
            </p>

            <p>
                By entering this competition, you agree to receive via email information and marketing communications from Little Ones. Communications may contain 3rd party content as directed by Little Ones. Little Ones assume no responsibility for the content of 3rd party communication. You have the right to unsubscribe from emails at any time via a link provided at the bottom of all email communications.
            </p>

            <p>
                Information on how to enter and the description of the prize form part of these Terms and Conditions.
            </p>

            <p>
                Entry is only open to residents of Australia. Directors, officers, management, employees and other staff (and the immediate families of directors, officers, management, employees and other staff) of the Promoter or of its related bodies corporate or of the agencies or companies associated with this competition are ineligible to enter.
            </p>

            <p>
                All entrants must be over the age of 18 to enter.
            </p>

            <p>
                The Competition commences on 10-October-16 at 09:00 AEST and concludes on 31-Jan-17 at 23:59 AEST (“Competition Period”).
            </p>

            <h4>IN ORDER TO ENTER, ENTRANTS MUST:</h4>

            <p>
                Entrants must provide their name and email as well as check the terms and conditions check box to successfully enter the competition.
            </p>

            <p>
                Entrants must ensure that the information they provide is valid, complete and accurate. Any invalid, incomplete or inaccurate entries will be automatically disqualified from the Competition.
            </p>

            <p>
                Entries must be received by the Promoter during the Competition Period. Entrants may enter the Competition once during the Competition Period.
            </p>

            <p>
                The Promoter accepts no responsibility for late, lost or misdirected entries. The Promoter reserves the sole right to disqualify any entrant who it believes has tampered with the entry process or has otherwise engaged in any conduct that is fraudulent or inconsistent with these Terms and Conditions.
            </p>

            <p>
                All eligible entries received during the Competition Period will be entered into a random draw. The draw will be conducted by Little Ones on 01-February-17 at 12:00 PM.
            </p>

            <p>
                1 winner will be selected from the draw (“Winner”) and the Winner will receive the following prize (“Prize”):
                3 night stay in a one bedroom apartment at any Adina Apartment Hotel in Australia.
            </p>

            <p>
                The total Prize pool is valued at AUD$1500. Prize values are the recommended retail value as provided by the supplier and are correct at the time of publication. The Promoter is not responsible or liable for any change in the value of the Prize between the date of publication of these Terms and Conditions and the date the Prize is claimed.
            </p>

            <p>
                The Winners will be notified in writing by email and their name will be published via social media. The Prize is not transferable and cannot be redeemed for cash. The Promoter accepts no responsibility for any tax implications that may arise from winning the Prize; the Winner should seek independent tax advice.
            </p>

            <p>
                The Prize may be subject to redemption restrictions or other terms and conditions of the supplier.
            </p>

            <p>
                The Promoter is not responsible for the cancellation of all or any part of the Prize. Subject to regulations, the Promoter reserves the right to change or substitute the Prize or any part of the Prize at any time if the Prize becomes unavailable.
            </p>

            <p>
                Subject to regulations, if any Prizes remain unclaimed for more than 3 months after the notification date specified in paragraph 9, Little Ones will conduct an unclaimed prize draw on 01-April-17 and the Winner’s name will be published on Little Ones social media channels.
            </p>

            <p>
                The Promoter and its associated agencies and companies will not be liable for any loss (including, without limitation, indirect, special or consequential loss or loss of profits), expense, damage, misadventure, accident, personal injury or death which is suffered or sustained (whether or not arising from any person’s negligence) in connection with this Competition or accepting or using any Prize, except for any liability which cannot be excluded by law (in which case that liability is limited to the minimum allowable by law).
            </p>

            <p>
                Subject to any restrictions at law, the liability of the Promoter however arising is limited to the total value of the relevant Prize.
            </p>

            <p>
                The Promoter is not responsible for any technical error that may occur in the course of the administration of this Competition including but not limited to any omission, interruption, deletion, defect, delay in operation or transmission, communications, mobile or satellite network failure, theft or destruction or unauthorised access to or alteration of entries.
            </p>

            <p>
                If for any reason any aspect of this Competition is not capable of running as planned, including but not limited to infection by computer virus, bugs, tampering, unauthorised intervention, fraud, technical failures or any cause beyond the control of the Promoter, the Promoter may in its sole discretion cancel, terminate, modify or suspend the Competition.
            </p>

            <p>
                Without limiting any other paragraph, the Promoter may at its discretion amend any aspect of this Competition, subject to any applicable laws.
            </p>

            <p>
                The Promoter is Little Ones Pty Ltd
            </p>
        </div>
    </div>
</div>
@endsection