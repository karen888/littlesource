@extends('pre-launch-docs-layout')

@section('content')

<div class="container">

    <div class="back-button">
        <a class="Prelaunch__beta--back" href="{{ URL::previous() }}"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back to Previous Page</a>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h1>PEACE OF MIND FOR PARENTS</h1>

            <p>Little Ones’ founder, Viviana, is a mother of three – she gets it! There’s nothing more important than the safety of your children. What’s the point in taking some time for yourself if you’re worried about the kids the whole time? That’s why safety and security is our number one priority. We only list strictly verified caregivers, with the same requirements as a child care centre.</p>

            <h4>MINIMUM REQUIREMENTS</h4>
            <p>
                Little Ones has the same ‘minimum requirement’ standards for our caregivers as any child care centre in Australia. These include:
                <ul>
                    <li>Current WWCC (Working with Children Check).</li>
                    <li>A certificate III, or higher in a related field.</li>
                    <li>The right to work in Australia.</li>
                </ul>
            </p>

            <h4>ADDITIONAL INFORMATION PROVIDED BY INDIVIDUALS</h4>
            <p>
                In addition to our minimum requirements, individuals can also share additional documentation to provide parents with extra assurance and peace of mind. This include:
                <ul>
                    <li>Police checks.</li>
                    <li>References from previous employers – child care centres and/or individuals.</li>
                    <li>Links to personal social media pages.</li>
                </ul>
            </p>

            <h4>IDENTITY VERIFICATION</h4>
            <p>
                We have a strict identity variation process to ensure our caregivers are listing accurate and current information. This process includes:
                <ul>
                    <li>One photo ID, one proof of address. </li>
                    <li>A current photo of the individual holding the photo ID next to them.</li>
                    <li>A profile photo (of their face) must be uploaded to their profile.</li>
                    <li>Email activation is required when creating an account.</li>
                    <li>Our security system quickly identifies duplicate accounts, banned users and unauthorised sign-ins.</li>
                </ul>
            </p>

            <h4>COMMUNITY VERIFICATION</h4>
            <p>
                Little Ones encourages you to review our caregivers – whether it be a quick star rating, or some more detailed feedback. This helps to inform and assure other parents about the standard and style of care provided by the individual care giver. Community reviews include:
                <ul>
                    <li>A simple star rating out of five.</li>
                    <li>More detailed feedback listed as “comments”.</li>
                    <li>Data on the number of past hires.</li>
                    <li>Personal reference checks are available upon request.</li>
                </ul>
            </p>

            <h4>QUESTIONS?</h4>
            <p>If you have any further questions or concerns about the safety standards of Little Ones, we would be happy to chat with you further to put your mind at ease! Please get in touch with our team at comms@littleones.com.au</p>
        </div>
    </div>
</div>
@endsection