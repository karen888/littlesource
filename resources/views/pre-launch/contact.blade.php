@extends('pre-launch-contact')

@section('content')

    <div style="padding: 100px;">
        <div class="container">
            <div class="back-button">
                <a class="Prelaunch__beta--back" href="{{ URL::previous() }}"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back to Previous Page</a>
            </div>
            <div class="row">
                <h1 align="center">Contact Us</h1><br>
            <div class="alert alert-success" id="show" style="display:none">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <strong>Success!</strong> Email Successfully Sent!.
            </div>
            <div class="alert alert-danger" role="alert" style="display:none" id="validate">
                <strong>Warning!</strong> <p></p>
            </div>
            </div>
            <div class="form-horizontal" style="font-size: 18px;">
                {!!Form::open() !!}
                <div class="form-group ">
                    {!!Form::label('Name:')!!}
                    {!!Form::text('name',null,['id' => 'name' ,'class'=>'form-control','placeholder'=>'Name','maxlength' => '40'])!!}
                </div>
                <div class="form-group">
                    {!!Form::label('Email:')!!}
                    {!!Form::text('email',null,['id' => 'email', 'class'=>'form-control','placeholder'=>'Email','maxlength' => '50'])!!} 
                </div>
                <div class="form-group">
                    {!!Form::label('Subject:')!!}
                    {!!Form::text('subject',null,['id' => 'subject', 'class'=>'form-control','placeholder'=>'Subject','maxlength' => '40'])!!}  
                </div>
                <div class="form-group">
                    {!!Form::label('Message:')!!}
                    {!!Form::textarea('message',null,['id' => 'message', 'class'=>'form-control','placeholder'=>'Message','maxlength' => '300'])!!}
                </div>
                <div class="form-group">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                      {!!link_to('#', $title='SEND', $attributes=['id' => 'send','class'=>'btn btn-default','style' => 'color:white'] ,$secure=null)!!}  
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

      
    <footer class="footer" style="background: #C0C0C0;padding:10px 0;margin-top: 200px;">
        <div class="container text-center">
            <a href="https://www.facebook.com/LittleOnes.AU1" title="Our Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="#" target="_blank"><i class="fa fa-instagram" title="Our Instagram"></i></a>
            <a href="/pre-launch/contact" title="Contact us"><i class="fa fa-envelope-o"></i></a>
            <a href="/pre-launch/faqs" title="FAQs">FAQs</a>
            <a href="/pre-launch/caregivers-verification" title="How we get caregivers"><i class="fa fa-users"></i></a>
        </div>
    </footer>

    <script type="text/javascript">
        $(document).ready(function(){
            var expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

                   $("#name").keyup(function () {
                   $("#validate").fadeOut('slow');
                   $("#show").fadeOut('slow');
                    }).keyup();
                   $("#email").keyup(function () {
                   $("#validate").fadeOut('slow');
                    $("#show").fadeOut('slow');
                    }).keyup();
                     $("#subject").keyup(function () {
                     $("#validate").fadeOut('slow');
                    $("#show").fadeOut('slow');
                    }).keyup();
                      $("#message").keyup(function () {
                      $("#validate").fadeOut('slow');
                    $("#show").fadeOut('slow');
                    }).keyup();

                $('#send').click(function()
                {
                   var name=$("#name").val();
                   var email=$("#email").val();
                   var subject=$("#subject").val();
                   var message=$("#message").val();

                   if(name == "")
                   {
                        $("#validate p").text("Name field is Required");
                        $("#validate").fadeIn('slow');
                        $("#name").focus();
                        return false;
                   }else{
                        if(email == "")
                        {
                            $("#validate p").text("Email field is Required");
                            $("#validate").fadeIn('slow');
                             $("#email").focus();
                            return false;
                        }else{
                            if(!expr.test(email))
                            {
                                $("#validate p").text("The email must be a valid email address");
                                $("#validate").fadeIn('slow');
                                $("#email").focus();
                                 return false;
                            }else{
                            if(subject == "")
                            {
                                 $("#validate p").text("Subject field is Required");
                                 $("#validate").fadeIn('slow');
                                 $("#subject").focus();
                                return false;
                            }
                            else{
                                if(message==""){
                                    $("#validate p").text("Message field is Required");
                                    $("#validate").fadeIn('slow');
                                     $("#message").focus();
                                    return false;
                                }
                            }
                        }
                   }

               }

                   var route = "/pre-launch/contactsend";
                   var token = $('#token').val();

                   $.ajax({
                        url: route,
                        headers: {'X-CSRF-TOKEN':token},
                        type: 'POST',
                        dataType : 'json',
                        data: {
                            name:name,
                            email:email,
                            subject:subject,
                            message:message
                        },
                        success: function() {
                            $("#show").show();
                            $("#name").val("");
                            $("#email").val("");
                            $("#subject").val("");
                            $("#message").val("");
                        }
                   });  
                })
        });
    </script>
@endsection



 