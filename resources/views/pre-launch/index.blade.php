@extends('pre-launch-layout')

@section('content')
    <component :is="currentView" keep-alive></component>
@endsection
