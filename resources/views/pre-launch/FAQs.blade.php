@extends('pre-launch-docs-layout')

@section('content')

<div class="container">

    <?php
        $redirectUrl = URL::previous();
        if(strpos($redirectUrl, '/pre-launch/caregivers-verification'))
            $redirectUrl = '/pre-launch';
    ?>

    <div class="back-button">
        <a class="Prelaunch__beta--back" href="{{ $redirectUrl }}"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Back to Previous Page</a>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <h1>FAQ:</h1>

            <h2>Information for Caregivers</h2>

            <h4>1. Is there a fee to be listed as a caregiver?</h4>
            <p>By entering the Little Ones Pre-Register & Win competition (“Competition”) you agree to the following terms and conditions (“Terms and Conditions”) and Privacy Statement:</p>

            <h4>2. Do I get to set my own rates or are they pre-determined?</h4>
            <p>You are completely free to set your own rate of pay. We encourage you to base this on your qualifications, years of experience and the amount of ‘trusted’ ratings you receive on Little Ones. We know these things are all important to parents and many of them will be willing to pay more for a caregiver they can see easily from the outset that they can trust.</p>

            <h4>3. Do I get paid more for short periods of care? (not sure here)</h4>
            <p>
                Yes! Our "care at a click" system starts with a minimum of 1 hour of care. Our tiered payment system operates as follows:
                <ul>
                    <li>Up to 1 hour  = Double time</li>
                    <li>1- 2 hours  = Time and a half</li>
                    <li>2 - 3 hours = Time and a quarter</li>
                    <li>3 hours plus = Normal rate</li>
                </ul>
            </p>

            <h4>4. What are the qualifications I need to be listed as a caregiver?</h4>
            <p>We only list certified caregivers on Little Ones with a valid ABN. Please click here to view our requirements for <a href="{{ route('pre-launch.caregivers-verification')  }}">caregiver qualifications</a> and click here to <a href="https://abr.gov.au/For-Business,-Super-funds---Charities/Applying-for-an-ABN/Apply-for-an-ABN/">apply for an ABN</a></p>

            <h4>5. How will I be paid?</h4>
            <p>All payments will be made via the Little Ones secure payment portal from users to carers, you can then request to have your available funds transferred to your nominated bank account.</p>

            <h4>6. How do I get notified of a potential job?</h4>
            <p>An email and/or SMS notification will be send to all possible applicants available for the job requested. The first one to accept, gets the job!</p>

            <h4>7. What If I accepted a job but can no longer attend a job?</h4>
            <p>Please notify us ASAP that you can no longer attend a job and we will re-open that job as available for other carers. All you have to do is select "cancel job" and select the reason why you are canceling.</p>

            <h2>Parents / Childcare centres</h2>

            <h4>1. How can I trust that Little Ones is safe to use?</h4>
            <p>The safety of your little ones is our number one priority. We inforce a strict verification process for all of our caregivers. All of our caregivers have the same legal requirements and qualifications that are required for caregivers in childcare centres. You can also read reviews and feedback on all of our caregivers, as well as check out their past work experience and references. Please click here for detailed information about our <a href="{{ route('pre-launch.caregivers-verification')  }}">caregiver verification</a> process.</p>

            <h4>2. Is there a fee to request a carer?</h4>
            <p>No! This service is completely free for parents to use. We take an 11% commission on all bookings, which is taken out of the payment you submit to the caregiver.</p>

            <h4>3. Do I pay the carer directly or through Little Ones?</h4>
            <p>All payments are exchanged via Little Ones to ensure a smooth and secure process for you and the caregiver. Little Ones uses a safe and secure portal that always keeps your details private.</p>

            <h4>4. What if I want to extend my hours that I've requested for a carer after the job has been accepted/started?</h4>
            <p>If you have requested your carer to stay on past the agreed hours originally arranged, simply log back into your profile, extend the hours that the carer stayed/has been requested to stay and your payment will be adjusted accordingly. The caregiver will be sent an SMS notification to confirm whether or not they are able to extend the hours, as per your request.</p>

            <h4>5. What if the carer doesn't show up?</h4>
            <p>Please contact us immediately and we will organise alternate carer ASAP and/or a refund.</p>

            <h4>6. Will it cost extra to have someone pop over for an hour or so?</h4>
            <p>
                Our "care at a click" system is what we describe as the ‘Uber of babysitting’. It starts with a minimum of 1 hour of care. Our tiered payment system operates as follows:
                <ul>
                    <li>Up to 1 hour  = Double time</li>
                    <li>1- 2 hours  = Time and a half</li>
                    <li>2 - 3 hours = Time and a quarter</li>
                    <li>3 hours plus = Normal rate</li>
                </ul>
            </p>
        </div>
    </div>
</div>
@endsection