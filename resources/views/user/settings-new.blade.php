@extends('app')

@section('content')
<div class="container">
    <div class="content">
        <div class="row padd-top">

            <div class="col-md-3">
                <h4>Settings</h4>
                <div id="settings-nav">
                    <ul class="nav nav-stacked">
                        <li></li>
                        <li class={{ active_class(if_route(['client.settings.my-info'])) }}>
                            <a href="{{ route('client.settings.my-info') }}">
                                <i class="fa fa-user"></i> My Info
                            </a>
                        </li>
                        @if(!$user->is_caregiver)
                            <li class={{ active_class(if_route(['client.billing-methods.index'])) }}>
                                <a href="{{ route('client.billing-methods.index') }}">
                                    <i class="fa fa-credit-card"></i> Billing Methods
                                </a>
                            </li>
                        @endif
                        <li class={{ active_class(if_route(['client.settings.password'])) }}>
                            <a href="{{ route('client.settings.password') }}">
                                <i class="fa fa-lock"></i> Password and Security
                            </a>
                        </li>
                        <li class={{ active_class(if_route(['client.settings.notifications'])) }}>
                            <a href="{{ route('client.settings.notifications') }}">
                                <i class="fa fa-bell-o"></i> Notifications
                            </a>
                        </li>
                        @if($user->is_caregiver)
                        <li class={{ active_class(if_route(['client.settings.get-verifed'])) }}>
                            <a href="{{ route('client.settings.get-verifed') }}">
                                <i class="fa fa-file-o"></i> Documents Verification

                            </a>

                            @if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread()) > 0)
                                <span  style="display: inline-block;position: relative;float:left;left: 170px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:35px;" class="unread-settings-badge">
                                        {{$unread}}
                                    </span>
                            @endif
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                @yield('settings_content')
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection

@section('js')
    @yield('js')
@endsection