@extends('user.setting')

@section('content_user_settings')

    <h3>Contact Info</h3>
    <div>

        <form class="form-horizontal" role="form" method="POST" action="">
        	<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="form-group" data-name="name">
				<label class="col-md-4 control-label">Name</label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="name" value="{{ Auth::user()->name }}">
				</div>
			</div>

			<div class="form-group" data-name="email">
				<label class="col-md-4 control-label">E-Mail Address</label>
				<div class="col-md-6">
					<input type="email" class="form-control" name="email" value="{{ Auth::user()->email }}">
				</div>
			</div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary send-req-form">Save</button>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4 alert alert-success alert-hidden">
                </div>
            </div>
        </form>
    </div>

@endsection
