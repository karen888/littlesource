@extends('user.setting')

@section('content_user_settings')

    <h3>Change Password</h3>
    <div>

        <form class="form-horizontal" role="form" method="POST" action="">
        	<input type="hidden" name="_token" value="{{ csrf_token() }}">

			<div class="form-group" data-name="old_password">
				<label class="col-md-4 control-label">Old Password</label>
				<div class="col-md-6">
				    <input type="password" class="form-control" name="old_password">
				</div>
			</div>

			<div class="form-group" data-name="password">
				<label class="col-md-4 control-label">New Password</label>
				<div class="col-md-6">
				    <input type="password" class="form-control" name="password">
				</div>
			</div>

			<div class="form-group" data-name="password_confirmation">
				<label class="col-md-4 control-label">Retype Mew Password</label>
				<div class="col-md-6">
				    <input type="password" class="form-control" name="password_confirmation">
				</div>
			</div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary send-req-form">Save</button>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4 alert alert-success alert-hidden">
                </div>
            </div>
        </form>
    </div>

@endsection

