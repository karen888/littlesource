@extends('new-app')

@push('styles')
    <link  href="/croper/cropper.css" rel="stylesheet">
    <link  href="{{elixir('css/profile-builder-modals.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div id="profile-edit-base"></div>
@endsection

@push('scripts')
    <script src="{{elixir('js/profile-edit.js')}}"></script>
@endpush