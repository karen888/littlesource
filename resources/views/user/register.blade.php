@extends('new-app')

@section('content')
    <div id="registration"></div>
@endsection

@push('scripts')
    <script src="{{elixir('js/registration.js')}}"></script>
@endpush