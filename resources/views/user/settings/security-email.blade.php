<div class="panel-row">
    <div class="field">
        <strong>Security (SMS) Email</strong>
    </div>
    <div class="value">
        <p>
            Receive SMS security notifications on your mobile phone<br>
            <a href="#">Add SMS Email Now</a><br>
            e.g. 8881231234@yourprovider.net
        </p>
    </div>
    <div class="actions">
        <a href="#" class="btn btn-themed">
            Edit
        </a>
    </div>
</div>