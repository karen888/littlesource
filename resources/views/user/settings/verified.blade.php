@extends('app')
@section('content')

<div id="get-verified">
    <div class="" style="margin-top: 20px;">
        <div class="container">
            @if(Auth::user()->canApplyToJob() && Auth::user()->allEducationsSubmitted())
                <div class="alert alert-success">
                    Thank you for submitting your verification documents. We will be in touch with you via email, once your account has been verified. In the meantime, you can complete your profile
                    <a href="/employee/profile" style="font-weight: bold; text-decoration:underline;">here</a>.
                </div>

            @endif
          <h4>Verification Dashboard</h4>

            <p style="line-height: 1.2;text-align: justify;">Little Ones prides itself on only listing qualified caregivers, so make sure you check out our <a href="http://prelaunch.littleones.com.au/caregivers-verification/">minimum requirements</a> before starting your profile.</p>
            <p style="line-height: 1.2;text-align: justify;">Once you’ve entered all your details, we will take some time to verify your profile before you can start applying for jobs.</p>
            <p style="line-height: 1.2;text-align: justify;">You can check on the status of your verification here.</p>

            <p>&nbsp;</p>


          <div class="table-hover">
              <div class="vertable-block">
          <table class="table table-responsive table-condensed verified-list-table rwd-table">
            <thead>
              <tr>
                <th></th>
                <th>Identification</th>
                <th>File Type</th>
                <th>Submission date</th>
                <th>Status</th>
                <th>Comments</th>
              </tr>
            </thead>
            <tbody>
              <tr class="no-border">
                <td data-th=""><i  data-title="Delete info" class="fa fa-trash-o" v-on:click="confDel('Are you sure you want to delete your phone number information?', 'phone', $event)"></i>
                </td>
                <td data-th="Identification"><a href="{{ route('client.settings.mobile-number') }}">Verify phone</a> *</td>
                <td data-th="Phone">@if($user->phone == '') - @else {{ $user->phone }} @endif</td>
                <td data-th="Submission date">@if($user->phone == '') - @else {{ date("d/m/Y",$user->phoneDate) }} @endif</td>
                <td data-th="Status">
                  @if($user->phone == '')
                    not submitted
                  @else
                    @if($user->user->phone_verifed == 0)
                      <i class="fa fa-refresh"></i>&nbsp;submitted
                    @elseif($user->user->phone_verifed == 1)
                      <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span>
                    @else
                      <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsp;rejected </span>
                    @endif
                  @endif
                </td>
                <td data-th="Comments">-</td>
              </tr>
              <tr class="no-border">
                  <td><i data-toggle="s" data-title="Delete info" class="fa fa-trash-o"  v-on:click="confDel('Are you sure you want to delete your ID (photo ID) information?', 'id', $event)"></i></td>
                  <td data-th="Identification"><a href="{{ route('client.settings.id') }}">ID (photo ID) *</a></td>
                  <td>
                      @if($user->prof_scan_id == '')
                          -
                      @else
                          @if(preg_match("#base64|jpeg|png|jpg|gif|bmp#i", $user->prof_scan_id))
                              <a class="fancybox" rel="group" href="{{$user->prof_scan_id}}">
                                  <img data-lightbox="prof_scan_id" caption="asdd" width="100px" src="{{ $user->prof_scan_id }}" />
                              </a>

                          @else
                              <a target="_blank" href="{{ $user->prof_scan_id }}"><i class="fa fa-external-link"></i> Attachment [{{explode(".", $user->prof_scan_id)[1]}}]</a>
                          @endif
                      @endif
                  </td>
                  <td data-th="Submitssion date">@if($user->prof_scan_id == '') - @else {{ date("d/m/Y",strtotime($user->updated_at)) }} @endif</td>
                  <td data-th="Status">
                      @if($user->prof_scan_id == '') not submitted
                      @else
                          @if($user->user->verifed_id == 0)
                              <i class="fa fa-refresh"></i>&nbsp;submitted
                          @elseif($user->user->verifed_id == 1)
                              <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span>
                          @else
                              <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsp;rejected </span>
                          @endif
                      @endif
                  </td>
                  <td data-th="Comments" class="ver-comments">
                      <?php
                      $id_chat = Auth::user()->getVerificationChat('prof_id');
                      ?>
                      @if(count($id_chat))
                          @if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread('prof_id')) > 0)
                              <span  style="display: inline-block;position: relative;left: -2px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:0px;" class="unread-settings-badge">
                                    {{$unread}}
                                </span>
                          @endif
                          <span style="color:green">{{$id_chat[count($id_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'You'}}: </span>
                          <a href="{{ route('client.settings.id') }}#chat_prof_id">{{$id_chat[count($id_chat) - 1]->message}}</a>
                      @else
                              <a href="{{ route('client.settings.id') }}#chat_prof_id">-</a>
                      @endif
                  </td>
              </tr>
              {{--<tr style="border:none">--}}
                  {{--<td colspan="6" style="border:none">--}}
                      {{--@include('user.settings.chat-view', ['type' => 'prof_id', 'id'=>null, 'panelId' => 'chat_prof_id', 'chat' => $id_chat])--}}
                  {{--</td>--}}

              {{--</tr>--}}
              <tr class="no-border">
                  <td><i  data-title="Delete info" class="fa fa-trash-o"  v-on:click="confDel('Are you sure you want to delete your Address verification information?', 'adress', $event)"></i></td>
                  <td data-th="Identification"><a href="{{ route('client.settings.address') }}">Address verification *</a></td>
                  <td data-th="Scan">@if($user->loc_scan == '') - @else
                          @if(preg_match("#base64|jpeg|png|jpg|gif|bmp#i", $user->loc_scan))
                              <a class="fancybox" rel="group" href="{{$user->loc_scan}}">
                                  <img data-lightbox="prof_scan_id" caption="asdd" width="100px" src="{{ $user->loc_scan }}" />
                              </a>

                          @else
                              <a target="_blank" href="{{ $user->loc_scan }}"><i class="fa fa-external-link"></i> Attachment [{{explode(".", $user->loc_scan)[1]}}]</a>
                          @endif
                      @endif</td>
                  <td data-th="Submission date">@if($user->loc_city == '') - @else {{ date("m/d/Y", $user->loc_time ) }} @endif</td>
                  <td data-th="Status">@if($user->loc_city == '')
                          not submitted
                      @else
                          @if($user->user->address_status == 0)
                              <i class="fa fa-refresh"></i>&nbspsubmitted
                          @elseif($user->user->address_status == 1)
                              <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                          @else
                              <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                          @endif
                      @endif</td>
                  <td data-th="Comments" class="ver-comments">
                      <?php
                      $address_chat = Auth::user()->getVerificationChat('address');
                      ?>
                      @if(count($address_chat))
                              @if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread('address')) > 0)
                                  <span  style="display: inline-block;position: relative;left: -2px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:0px;" class="unread-settings-badge">
                                    {{$unread}}
                                </span>
                              @endif
                          <span style="color:green">{{$address_chat[count($address_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'You'}}: </span>
                          <a href="{{ route('client.settings.address') }}#chat_address">{{$address_chat[count($address_chat) - 1]->message}}</a>
                      @else
                              <a href="{{ route('client.settings.address') }}#chat_address">-</a>
                      @endif
                  </td>
              </tr>
              {{--<tr style="border:none">--}}
                  {{--<td colspan="6" style="border:none">--}}
                      {{--@include('user.settings.chat-view', ['type' => 'address', 'id'=>null, 'panelId' => 'chat_address', 'chat' => $address_chat])--}}
                  {{--</td>--}}

              {{--</tr>--}}
              <tr class="no-border">
                <td><i  data-title="Delete info" class="fa fa-trash-o" v-on:click="confDel('Are you sure you want to delete your WWCC information?', 'wwcc', $event)"></i></td>
                <td data-th="Identification"><a href="{{ route('client.settings.wwcc') }}">WWCC *</a></td>
                <td data-th="Scan">@if($user->wwcc_image == '') - @else
                        @if(preg_match("#base64|jpeg|png|jpg|gif|bmp#i", $user->wwcc_image))
                            <a class="fancybox" rel="group" href="{{$user->wwcc_image}}">
                                <img data-lightbox="prof_scan_id" caption="asdd" width="100px" src="{{ $user->wwcc_image }}" />
                            </a>

                        @else
                            <a target="_blank" href="{{ $user->wwcc_image }}"><i class="fa fa-external-link"></i> Attachment [{{explode(".", $user->wwcc_image)[1]}}]</a>
                        @endif
                    @endif</td>
                <td data-th="Submission date">@if($user->wwcc_image == '') - @else {{ date("d/m/Y",$user->wwcc_date) }} @endif</td>
                <td data-th="Status">
                  @if($user->wwcc_image == '')
                    not submitted
                  @else
                    @if($user->user->wwcc_status == 0)
                      <i class="fa fa-refresh"></i>&nbsp;submitted
                    @elseif($user->user->wwcc_status == 1)
                      <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span>
                    @else
                      <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsp;rejected </span>
                    @endif
                  @endif
                </td>
                  <?php
                  $wwcc_chat = Auth::user()->getVerificationChat('wwcc');
                  ?>
                <td data-th="Comments" class="ver-comments">@if(count($wwcc_chat))
                        @if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread('wwcc')) > 0)
                            <span  style="display: inline-block;position: relative;left: -2px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:0px;" class="unread-settings-badge">
                                    {{$unread}}
                                </span>
                        @endif
                    <span style="color:green">{{$wwcc_chat[count($wwcc_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'You'}}: </span>
                        <a href="{{ route('client.settings.wwcc') }}#chat_wwcc">{{$wwcc_chat[count($wwcc_chat) - 1]->message}}</a>
                    @else
                        <a href="{{ route('client.settings.wwcc') }}#chat_wwcc">-</a>
                      @endif
                </td>
              </tr>

              {{--<tr style="border:none">--}}
                  {{--<td colspan="6" style="border:none">--}}
                      {{--@include('user.settings.chat-view', ['type'=>'wwcc', 'id'=>null, 'panelId' => 'chat_wwcc', 'chat' => $wwcc_chat])--}}
                  {{--</td>--}}

              {{--<tr>--}}
                {{--<td>&nbsp;<i class="fa fa-trash-o"  v-on:click="confDel('Are you sure you want to delete your Qualifications information?', 'qualifications', $event)"></i></td>--}}
              <tr>
                  <th></th>
                <td data-th="" style="font-weight: 800">Education *</td>
                  <td> <a style="font-weight: 800;" href="{{ route('client.settings.qualification') }}">Add New</a></td>

              </tr>
              @foreach($user->educations as $edu)
                <tr class="no-border">
                  <td><i  data-title="Delete info" class="fa fa-trash-o"  v-on:click="confDel('Are you sure you want to delete your Qualifications information?', 'qualifications', '{{ $edu->id }}', $event)"></i></td>
                  <td data-th="Identification"><a href="{{ route('client.settings.qualification', ['id'=>$edu->id]) }}">{{ $edu->school }} </a></td>
                  <td  data-th="Scan">@if($edu->scan == '') - @else
                          @if(preg_match("#base64|jpeg|png|jpg|gif|bmp#i", $edu->scan))
                              <a class="fancybox" rel="group" href="{{$edu->scan}}">
                                  <img data-lightbox="prof_scan_id" caption="asdd" width="100px" src="{{ $edu->scan }}" />
                              </a>

                          @else
                              <a target="_blank" href="{{ $edu->scan }}"><i class="fa fa-external-link"></i> Attachment [{{explode(".", $edu->scan)[1]}}]</a>
                          @endif
                      @endif</td>
                  <td data-th="Submission date">@if(@$edu->id == '') - @else {{ date("d/m/Y",strtotime($edu->created_at)) }} @endif</td>
                  <td  data-th="Status">@if($edu->id == '')
                      not submitted
                    @else
                      @if($edu->status == 0)
                        <i class="fa fa-refresh"></i>&nbsp;submitted
                      @elseif($edu->status == 1)
                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span>
                      @else
                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsp;rejected </span>
                      @endif
                    @endif</td>
                  <td data-th="Comments" class="ver-comments">
                      <?php
                      $edu_chat = Auth::user()->getVerificationChat('education', $edu->id);
                      ?>
                    @if(count($edu_chat))
                              @if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread('education')) > 0)
                                  <span  style="display: inline-block;position: relative;left: -2px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:0px;" class="unread-settings-badge">
                                    {{$unread}}
                                </span>
                              @endif
                            <span style="color:green">{{$edu_chat[count($edu_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'You'}}: </span>
                            <a href="{{ route('client.settings.qualification', ['id'=>$edu->id]) }}#chat_education">{{$edu_chat[count($edu_chat) - 1]->message}}</a>
                        @else
                              <a href="{{ route('client.settings.qualification', ['id'=>$edu->id]) }}#chat_education">-</a>
                        @endif

                  </td>

                </tr>
                {{--<tr style="border:none">--}}
                    {{--<td colspan="6" style="border:none">--}}
                        {{--@include('user.settings.chat-view', ['type' => 'education', 'id'=>$edu->id, 'panelId' => 'chat_edu_' . $edu->id, 'chat' => $edu_chat])--}}
                    {{--</td>--}}

                {{--</tr>--}}
              @endforeach

              <tr>
                  <th></th>
                  <td style="font-weight: 800;">Billing</td>

              </tr>
              <tr class="no-border">
                  <td><i  data-title="Delete info" class="fa fa-trash-o"    v-on:click="confDel('Are you sure you want to delete your ABN information?', 'abn', $event)"></i></td>
                  <td  data-th="Identification"><a href="{{ route('client.settings.abn') }}">ABN</a></td>
                  <td  data-th="ABN Number">@if($user->abn_number == '') - @else {{ $user->abn_number }} @endif</td>
                  <td  data-th="Submission date">@if($user->abn_number == '') - @else {{ date("d/m/Y", $user->abn_number_submit ) }} @endif</td>
                  <td  data-th="Status">
                      @if($user->abn_number == '')
                          not submitted
                      @else
                          @if($user->user->abn_verifed == 0)
                              <i class="fa fa-refresh"></i>&nbsp;submitted
                          @elseif($user->user->abn_verifed == 1)
                              <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span>
                          @else
                              <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsp;rejected </span>
                          @endif
                      @endif
                  </td>
                  <td  data-th="Comments" class="ver-comments">
                      <?php
                      $abn_chat = Auth::user()->getVerificationChat('abn');
                      ?>
                      @if(count($abn_chat))
                              @if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread('abn')) > 0)
                                  <span  style="display: inline-block;position: relative;left: -2px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:0px;" class="unread-settings-badge">
                                    {{$unread}}
                                </span>
                              @endif
                          <span style="color:green">{{$abn_chat[count($abn_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'You'}}: </span>
                          <a href="{{ route('client.settings.abn') }}#chat_abn">{{$abn_chat[count($abn_chat) - 1]->message}}</a>
                      @else
                              <a href="{{ route('client.settings.abn') }}#chat_abn">-</a>
                      @endif
                  </td>
              </tr>
              {{--<tr style="border:none">--}}
                  {{--<td colspan="6" style="border:none">--}}
                      {{--@include('user.settings.chat-view', ['type' => 'abn', 'id'=>null, 'panelId' => 'chat_abn', 'chat' => $abn_chat])--}}
                  {{--</td>--}}

              {{--</tr>--}}
              <tr>
                <th></th>

                <td style="font-weight: 800;">Additional information</td>
              </tr>
              <tr class="no-border" style="position: relative;left: -1px;">
                  <th></th>
                  <td style="font-weight: bold">Certificates</td>
                  <td><a href="{{ route('client.settings.certificates') }}">Add New</a></td>

              </tr>
              @foreach($user->certificates as $cert)
                <tr class="no-border">
                  <td><i  data-title="Delete info" class="fa fa-trash-o"   v-on:click="confDel('Are you sure you want to delete your Certification information?', 'certification', {{ $cert->id }}, $event)"></i></td>
                  <td  data-th="Identification"><a href="{{ route('client.settings.certificates', ['id'=>$cert->id]) }}">{{ $cert->name }} </a></td>
                  <td  data-th="Certificate">@if($cert->id == '') - @else {{ $cert->provider }} @endif</td>
                  <td  data-th="Submission date">@if(@$cert->id == '') - @else {{ date("d/m/Y",strtotime($cert->updated_at)) }} @endif</td>
                  <td  data-th="Status">@if($cert->id == '')
                      not submitted
                    @else
                      @if($cert->status == 0)
                        <i class="fa fa-refresh"></i>&nbsp;submitted
                      @elseif($cert->status == 1)
                        <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbsp;verified </span>
                      @else
                        <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsp;rejected </span>
                      @endif
                    @endif</td>
                  <td  data-th="Comments" class="ver-comments">
                      <?php
                      $cert_chat = Auth::user()->getVerificationChat('certifications', $cert->id);
                      ?>
                      @if(count($cert_chat))
                              @if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread('certifications')) > 0)
                                  <span  style="display: inline-block;position: relative;left: -2px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:0px;" class="unread-settings-badge">
                                    {{$unread}}
                                </span>
                              @endif
                          <span style="color:green">{{$cert_chat[count($cert_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'You'}}: </span>
                          <a href="{{ route('client.settings.certificates', ['id'=>$cert->id]) }}#chat_certifications">{{$cert_chat[count($cert_chat) - 1]->message}}</a>
                      @else
                          <a href="{{ route('client.settings.certificates', ['id'=>$cert->id]) }}#chat_certifications">-</a>
                      @endif
                  </td>
                </tr>
                {{--<tr style="border:none">--}}
                    {{--<td colspan="6" style="border:none">--}}
                        {{--@include('user.settings.chat-view', ['type' => 'certifications', 'id'=>$cert->id, 'panelId' => 'chat_cert_' . $cert->id, 'chat' => $cert_chat])--}}
                    {{--</td>--}}

                {{--</tr>--}}
              @endforeach
              {{--<tr class="no-border" style="position: relative;left: -18px;">--}}
                  {{--<th></th>--}}
                  {{--<td style="font-style: italic"></td>--}}
                  {{--<th></th>--}}
                  {{--<td></td>--}}
                  {{--<td></td>--}}
                  {{--<td>-</td>--}}
              {{--</tr>--}}
              <tr class="no-border">
                <td><i  data-title="Delete info" class="fa fa-trash-o" v-on:click="confDel('Are you sure you want to delete your Social Links information?', 'social', $event)"></i></td>
                <td  data-th="Identification"><a href="{{ route('client.settings.socialLinks') }}">Social Links</a></td>
                <td  data-th="Link">@if($user->soc_link == '') - @else
                        <a href="{{$user->soc_link}}" target="_blank" rel="noopener">{{ $user->soc_link }}</a>
                    @endif</td>
                <td  data-th="Submission date">@if($user->soc_link == '') - @else {{ date("m/d/Y", $user->soc_link_submit ) }} @endif</td>
                <td  data-th="Status">@if($user->soc_link == '')
                    not submitted
                  @else
                    @if($user->user->social_verifed == 0)
                      <i class="fa fa-refresh"></i>&nbspsubmitted
                    @elseif($user->user->social_verifed == 1)
                      <span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverified </span>
                    @else
                      <span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>
                    @endif
                  @endif</td>
                <td  data-th="Comments" class="ver-comments">
                    <?php
                    $soc_chat = Auth::user()->getVerificationChat('social');
                    ?>
                    @if(count($soc_chat))
                            @if(($unread = \App\Http\Models\VerificationChats::getCountUserUnread('social')) > 0)
                                <span  style="display: inline-block;position: relative;left: -2px;background: #ff3826;color: #fff;border-radius: 50%;min-width:19px;text-align:center;bottom:0px;" class="unread-settings-badge">
                                    {{$unread}}
                                </span>
                            @endif
                        <span style="color:green">{{$soc_chat[count($soc_chat) - 1]->message_sender == 'admin' ? 'Admin' : 'You'}}: </span>
                        <a href="{{ route('client.settings.socialLinks') }}#chat_social">{{$soc_chat[count($soc_chat) - 1]->message}}</a>
                    @else
                            <a href="{{ route('client.settings.socialLinks') }}#chat_social">-</a>
                    @endif
                </td>
              </tr>
              {{--<tr style="border:none">--}}
                  {{--<td colspan="6" style="border:none">--}}
                      {{--@include('user.settings.chat-view', ['type' => 'social', 'id'=>null, 'panelId' => 'chat_soc', 'chat' => $soc_chat])--}}
                  {{--</td>--}}

              {{--</tr>--}}
              <tr>
            </tbody>
          </table>
              </div>
          </div>
        </div>
    </div>
</div>



@endsection
