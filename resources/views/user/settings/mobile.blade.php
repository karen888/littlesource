@extends('app')



@section('content')
<div class="container padd-top-30" id="mobile-verify" v-cloak>
    <div class="row back-action">
        <div class="col-md-12">
            <a href="/user/settings/get-verified" class="back-button"><span class="back-icon"></span>Back to Verification</a>
        </div>
    </div>
    @include('user.settings.partial.ver-notif-1')
    <div class="row">
        <div class="col-md-12">
            <div class="main-heading">
              <h4>Add Your Phone Number</h4>
              <p>In order for us to connect you with parents, we need your phone number.</p>
            </div>
        </div>
    </div>
    <hr>
    <div class="verify-form job-form" v-if="nextStep == false">
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Mobile Number</strong>
            </div>
            <div class="col-md-4">
                <div :class="['full-width', formErrors.phone ? 'error' : '' ]">
                    {{--data-inputmask="'mask': '+999999999999[9]', 'greedy':false"--}}
                    <input  type="tel" id="phone" @keyup="reset" @blur="handleNumberBlur" v-model="userInfo.phone" name="mobile">
                    <div class="error-message" v-if="formErrors.phone"> @{{ formErrors.phone }} </div>

                </div>
            </div>
        </div>  
        <div class="row form-row">
            <div class="col-md-3">
                <strong>How should we send you codes?</strong>
            </div>
            <div class="col-md-4">
                <div class="radio">

                    <input checked="checked" type="radio" name="method" id="method_sms" value="sms" v-model="userInfo.method">
                    <label for="method_sms"><span></span> Text message(SMS)</label>
                </div>
                <div class="radio">

                    <input type="radio" name="method" id="method_call" value="call" v-model="userInfo.method">
                      <label for="method_call"><span></span> Voice Call</label>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="full-width">
                    <div class="actions">
                        <a class="btn btn-themed btn-primary btn-main-action" @click="sendCode()">Submit</a>
                        <a href="/user/settings/get-verified" type="button" class="btn btn-themed btn-preview ver" >Back</a>
                    </div>
                </div>
            </div>
        </div>                   
    </div>
    <div class="verify-form job-form" v-if="nextStep == true">
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Enter code</strong>
            </div>
            <div class="col-md-4">
                <div :class="['full-width', code.errors.message ? 'error' : '' ]">
                    <input type="text" @keyup="code.errors.message = ''" v-model="code.text" name="code" data-inputmask="'mask': '9999'"/>
                    <div class="error-message" v-if="code.errors.message"> @{{ code.errors.message }} </div>
                </div>
            </div>
        </div>  
        <div class="row form-row">
            <div class="col-md-3">
                <strong>@{{ question }}</strong>
            </div>
            <div class="col-md-4">
                <a @click.prevent="sendCode()" href="#"><strong>@{{ actionLinkText }}</strong></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="full-width">
                    <div class="actions">
                        <a class="btn btn-themed btn-primary btn-main-action ver submitPhone" @click="verifyCode()">Submit</a>
                        <a type="button" style="margin-right: 20px;" class="btn btn-themed btn-preview ver" @click="goBack()">Back</a>
                    </div>
                </div>
            </div>
        </div>                   
    </div>    
</div>

@endsection
