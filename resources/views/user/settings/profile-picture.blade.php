<template id="profile-picture-template">
    <div class="panel panel-default my-info profile_picture-template">
        <div class="panel-body">
            <div id="main-user-info">
                <div class="profile-picture">
                    <img no-hide="true" :src="picture" alt="">
                </div>
                <div class="user-name">
                    {{ $currentUser->name }}
                </div>
                <div class="actions" v-show="!displayForm">
                    <a @if(Auth::user()->is_caregiver) href="{{route('client.settings.id')}}" @else @click="openForm('profile-picture')" @endif class="btn btn-themed">
                        Edit profile image
                    </a>
                </div>
                <div class="actions open" v-else>
                    <a href="#" id="up_btn" class="btn btn-themed btn-primary" @click="saveProfilePicture">
                        Save
                    </a>
                    <a href="#" class="btn btn-themed cancel" @click="cancel">
                        Cancel
                    </a>
                </div>

                <div class="file-select" v-show="displayForm" transition="ease">
                    {!! Form::open([ 'method' => 'POST', 'files' => true]) !!}
                        <div class="file-select__choose-file">
                            <input type="file" name="profile_picture" id="file" v-model="profile_picture" class="inputfile" accept="image/*"  @change="bindFile" v-el:profilepic />
                            <label for="file" style="padding: 13px;">Choose a file</label>
                            <div class="filename" style="position: relative;margin-top: 10px;">
                                @{{ filePath }}
                            </div>
                            <div class="progress" style="position: relative;margin-top: 15px;" v-show="uploadProgress">
                                <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                </div>
                            </div>
                        </div>
                        <div v-show="picture != '/img/no_photo_100.jpg' && picture.length" class="file-select__delete">
                            <input v-model="delete" type="checkbox" name="delete" id="delete">
                            <label class="fx2" for="delete">Delete photo</label>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</template>

<profile-picture
    :display-form="displayForm.profile_picture"
    @notification="handleNotification"
    @open-form="currentOpenForm"
    @close-form="closeAllForms">
</profile-picture>