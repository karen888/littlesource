<?php
$user = Auth::user();

?>
<div class="panel-collapse {{isset($expand) && $expand === true ? "" : "collapse"}}" id="{{$panelId}}" style="max-width: 600px;margin:0 auto">
    <div class="panel-body" id="chat_view_{{$panelId}}">
        <ul class="chat" id="chat_{{$panelId}}">
            @foreach($chat as $msg)
                <?php
                    $msg->is_answer_read = true;
                    $msg->save();
                ?>
            <li class="{{$msg->message_sender == 'admin' ? 'left' : 'right'}} clearfix"><span class="chat-img pull-{{$msg->message_sender == 'admin' ? 'left' : 'right'}}">
                            <img width="50" height="50" src="{{$msg->message_sender == 'admin' ? 'http://placehold.it/50/55C1E7/fff&text=ADMIN' : ($user->user_photo_100 != '' ? $user->user_photo_100 :'/img/no_photo_100.jpg')}}" alt="User Avatar" class="img-circle" />
                        </span>
                <div class="chat-body clearfix">
                    <div class="header">
                        @if($msg->message_sender == 'admin')
                        <strong class="primary-font">Admin</strong> <small class="pull-right text-muted">
                            <i class="fa fa-clock-o"></i> {{$msg->created_at->format("H:i:s d/m/Y")}}</small>
                        @else
                            <small class=" text-muted"><i class="fa fa-clock-o"></i> {{$msg->created_at->format("H:i:s d/m/Y")}}</small>
                            <strong class="pull-right primary-font">You</strong>
                        @endif
                    </div>
                    <p>
                        {{$msg->message}}
                    </p>
                </div>
            </li>
            @endforeach
        </ul>
    </div>


    <div class="panel-footer">
        <div class="input-group chatig" style="width: 100% !important;">
            <input style="height:28px;" value="" autocomplete="off" aria-autocomplete="none" id="chat_{{$panelId}}_message" type="text" class="form-control input-sm chat-message-input" placeholder="Type your message here..." />
            <span class="input-group-btn">
                <button style="height: 32px; width: 70px !important;;" class="btn btn-primary btn-xs btn-chat-send btn-send-{{$panelId}}" id="btn-chat">
                    <span class="btn-chat-send-icon"><i class="fa fa-send"></i></span>
                    <span class="btn-chat-send-text">Send</span>
                </button>
            </span>
        </div>
    </div>
</div>


@section('page-required-scripts')
<script>





    $(function(){
        var scrollBottom = function() {
            document.getElementById('chat_view_{{$panelId}}').scrollTop = document.getElementById('chat_view_{{$panelId}}').scrollHeight;
            {{--$('#chat_{{$panelId}}_message').focus();--}}

//            if(!$('#get-verified').length) {
//                location.href= '#';
//            }
        };

        $("#{{$panelId}}").on("show.bs.collapse", function(){
            console.log("show");
            window.setTimeout(scrollBottom, 200);
        });

        window.setTimeout(scrollBottom, 200);

        function stripTags(str) {
            return str.replace(/<\/?[^>]+>/gi, '');
        }


        //var socket = io(':3000');

        var sendMsg = function(e) {

            e.preventDefault();

            e.stopPropagation();


            var text = document.getElementById('chat_{{$panelId}}_message');
            var $text = $('#chat_{{$panelId}}_message');

            var val = text.value;

            val = stripTags(val);

            if(val.replace(/\s/g, "").length === 0) {
                $text.val('');
                e.currentTarget.value = "";
                return;
            }

            $('.btn-send-{{$panelId}}').attr('disabled', true);
            text.disabled = true;

            //socket.emit('chat message', {user_id: '{{$user->id}}', type: '{{$type}}', id: '{{$id}}', text: text.value, sender: 'user'});

            var ttext = text.value;
            $.ajax({
                url: '/api/v1/settings/send-message',
                type: 'POST',
                data: {type: '{{$type}}', id: '{{$id}}', text: ttext, _token: window._csrf._token},
                success:function(){


                    text.disabled = false;
                    $text.val("");
                    window.setTimeout(function(){
                        $text.val("____");
                        $text.val("");
                        $text.focus();
                        $text.click();

                    }, 300);
                    onMessageSent({
                        type: '{{$type}}',
                        sender: 'user',
                        id: '',
                        text: ttext

                    });
                    $('.btn-send-{{$panelId}}').attr('disabled', false);
                    $('#chat_notif').slideDown(400);
                }
            });


        };

        $('.btn-send-{{$panelId}}').on('click', sendMsg);
        $('#chat_{{$panelId}}_message').on('keydown', function(e){
            if(e.keyCode === 13) {
                sendMsg(e);
            }
        });

        var onMessageSent = function(message){
            console.log(message);
            if(message.type == '{{$type}}' && (message.id == '' || message.id == '{{$id}}')) {
                var template = '<li class="'+(message.sender === 'admin' ? 'left' : 'right')+' clearfix"><span class="chat-img pull-'+(message.sender === 'admin' ? 'left' : 'right')+'">\
                    <img width="50" height="50" src="'+(message.sender === 'admin' ? 'http://placehold.it/50/55C1E7/fff&text=ADMIN' : '{{($user->user_photo_100 != '' ? strtr($user->user_photo_100,["\r\n" => "\\\r\n"]) :'/img/no_photo_100.jpg')}}')+'" alt="User Avatar" class="img-circle" />\
                    </span>\
                    <div class="chat-body clearfix">\
                    <div class="header">\
                        '+(message.sender === 'admin' ? '\
                    <strong class="primary-font">Admin</strong> <small class="pull-right text-muted">\
                    <i class="fa fa-clock-o"></i> Now</small>\
                        ' : '\
                    <small class=" text-muted"><i class="fa fa-clock-o"></i> Now</small>\
                    <strong class="pull-right primary-font">You</strong>\
                        ')+'\
                    </div>\
                    <p>\
                        '+message.text+'\
                    </p>\
                    </div>\
                    </li>';

                document.getElementById('chat_{{$panelId}}').innerHTML += template;
                scrollBottom();
            }
        };
    })
</script>

@endsection