<password
    :display-form="displayForm.Password"
    @notification="handleNotification"
    @open-form="currentOpenForm"
    @close-form="closeAllForms">
</password>