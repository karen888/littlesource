@extends('app')
@section('content')
<div class="container padd-top-30" id="qualification-verify" v-cloak>
    <div class="row back-action">
        <div class="col-md-12">
            <a href="/user/settings/get-verified" class="back-button"><span class="back-icon"></span>Back to Verification</a>
        </div>
    </div>
    @include('user.settings.partial.ver-notif-1')
    <div class="row"> 
        <div class="col-md-12">
            <div class="main-heading">
              <h4>Education</h4>
              <p>Tell us about your university qualification.</p>
            </div>
        </div>
    </div>
    <hr>
    <div class="verify-form job-form" >
        <div class="row form-row">
            <div class="col-md-3">
                <strong>School *</strong>
            </div>
            <div class="col-md-4">
                <div :class="['full-width', form.school.errors ? 'error' : '' ]">
                    <input type="text" @keyup="form.school.errors  = ''" v-model="form.school.text" name="school"/>
                    <div class="error-message" v-if="form.school.errors"> @{{ form.school.errors }} </div>
                </div>
            </div>
        </div>  

        <div class="row form-row">
            <div class="col-md-3">
                <strong>Dates Attended *</strong>
            </div>
            <div class="col-md-4">
                <div class="col-md-6 no_padding">
                    <div class="col-md-11 no_padding full-width" :class="[form.date_start.errors ? 'error' : '' ]">
                        <custom-select class="choose_year"
                            :options="dateStart"
                            :selected.sync="form.date_start.selected"
                            name="from"
                            @changed-option="form.date_start.errors = ''"
                        >
                        </custom-select>
                        <div class="error-message" v-if="form.date_start.errors"> @{{ form.date_start.errors }} </div>
                    </div>
                </div>
                <div class="col-md-6 no_padding">
                    <div class="col-md-12 no_padding full-width" :class="[form.date_end.errors ? 'error' : '' ]">
                        <custom-select class="choose_year"
                            :options="dateEnd"
                            :selected.sync="form.date_end.selected"
                            name="to"
                            @changed-option="form.date_end.errors= ''"
                        >
                        </custom-select>
                        <div class="error-message" v-if="form.date_end.errors"> @{{ form.date_end.errors }} </div>
                    </div>
                </div>
            </div>
        </div>  

        <div class="row form-row">
            <div class="col-md-3">
                <strong>Degree *</strong>
            </div>
                <!--<div class="col-md-6 no_dding">
                    <div class="col-md-11 no_padding full-width" :class="[form.degree.errors ? 'error' : '' ]">
                        <custom-select class="choose_degree"
                            :options="dateStart"
                            :selected.sync="form.degree.selected"
                            name="from"
                            @keyup="form.degree.errors = ''"
                        >
                        </custom-select>
                        <div class="error-message" v-if="form.degree.errors"> @{{ form.degree.errors }} </div>
                    </div>
                </div>-->
            <div class="col-md-4">
                <div :class="['full-width', form.degree.errors ? 'error' : '' ]">
                    <input type="text" @keyup="form.degree.errors = ''" v-model="form.degree.text" name="degree"/>
                    <div class="error-message" v-if="form.degree.errors"> @{{ form.degree.errors }} </div>
                </div>
            </div>                
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Scan of my qualification*</strong>
            </div>
            <div class="col-md-4">
                <file-uploader
                    :current_file='scans'
                    field='scans'
                    v-on:uploaded="onFileUploaded">
                </file-uploader>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Area of Study (optional)</strong>
            </div>
            <div class="col-md-4">
                <div :class="['full-width', form.area.errors ? 'error' : '' ]">
                    <input type="text" @keyup="form.area.errors = ''" v-model="form.area.text" name="area"/>
                    <div class="error-message" v-if="form.area.errors"> @{{ form.area.errors }} </div>
                </div>
            </div>
        </div>  
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Description (optional)</strong>
            </div>
            <div class="col-md-4">
                <div class="full-width" :class="[form.desc.errors ? 'error' : '' ]">
                    <textarea name="description" rows="3" v-model="form.desc.text" @keyup="form.desc.errors = ''"></textarea>
                    <div class="error-message" v-if="form.desc.errors"> @{{ form.desc.errors }} </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Notes (message to admin)</strong>
                <a href="javascript:void(0);" data-toggle="popover-hover" data-placement="top" data-content="You can give us some more information which can be important for us to verify your documents. We will contact you via email if need any additional information">
                    <i class="fa fa-question-circle text-info"></i>
                </a>
            </div>
            <div class="col-md-6">
                @include('user.settings.chat-view', ['expand'=>true, 'type' => 'education', 'id'=>request('id'), 'panelId' => 'chat_education', 'chat' => Auth::user()->getVerificationChat('education', request('id'))])
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="full-width">
                    <div class="actions">
                        <button :disabled="loading.scans" class="btn btn-themed btn-primary btn-main-action" id="saveBtn" @click="checkForm('saved')" >Submit</button>
{{--                        @if(!@$_GET['id']) <A :disabled="loading.scans" href="javascript:void(0);" class="btn-themed-text" @click="checkForm('savemore')" >Submit and add more</A> @endif--}}
                        <a href="/user/settings/get-verified" type="button" class="btn btn-themed btn-preview ver btn-cancel" >Back</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                @include('user.settings.partial.submit-notif')
            </div>
        </div>
    </div>
</div>

@endsection
