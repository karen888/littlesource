@extends('app')
@section('content')
    <div class="container padd-top-30" id="cetrificates-verifed" v-cloak>
        <div class="row back-action">
            <div class="col-md-12">
                <a href="/user/settings/get-verified" class="back-button"><span class="back-icon"></span>Back to Verification</a>
            </div>
        </div>
        @include('user.settings.partial.ver-notif-1')
        <div class="row">
            <div class="col-md-12">
                <div class="main-heading">
                    <h4>Certificate</h4>
                    <p>Tell us about your qualifications like CPR, First Aid, or other short courses.</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Certification Name *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.name.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.name.error = ''" v-model="form.name.text" name="name" placeholder=""/>
                        <div class="error-message" v-if="form.name.error"> @{{ form.name.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Provider *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.provider.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.provider.error = ''" v-model="form.provider.text" name="provider"/>
                        <div class="error-message" v-if="form.provider.error"> @{{ form.provider.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Description (optional)</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.desc.error ? 'error' : '' ]">
                        <textarea name="description" rows="5" v-model="form.desc.text" @keyup="form.desc.error = ''"></textarea>
                        <div class="error-message" v-if="form.desc.error"> @{{ form.desc.error }} </div>
                    </div>
                </div>
            </div>

            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Date of Issuing *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.date_earned.error ? 'error' : '' ]">
                        <input class="nofuture" type="text" @keyup="form.date_earned.error = ''" v-model="form.date_earned.text" name="date_earned" placeholder=""/>
                        <div class="error-message" v-if="form.date_earned.error"> @{{ form.date_earned.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Certification Number</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.submission.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.submission.error = ''" v-model="form.submission.text" name="submission" placeholder=""/>
                        <div class="error-message" v-if="form.submission.error"> @{{ form.submission.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Copy of certification*</strong>
                </div>
                <div class="col-md-4">
                    <div class="progress" v-show="loading">
                        <div class="progress-bar progress-bar-striped active" role="progressbar" id="scanprogress"
                             aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                            0%
                        </div>
                    </div>
                    <div class="full-wdith">
                        <file-uploader
                            :current_file='scan'
                            field='scan'
                            v-on:uploaded="onFileUploaded">
                        </file-uploader>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Notes (message to admin)</strong>
                    <a href="javascript:void(0);" data-toggle="popover-hover" data-placement="top" data-content="You can give us some more information which can be important for us to verify your documents. We will contact you via email if need any additional information">
                        <i class="fa fa-question-circle text-info"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    @include('user.settings.chat-view', ['expand'=>true, 'type' => 'certifications', 'id'=>request('id'), 'panelId' => 'chat_certifications', 'chat' => Auth::user()->getVerificationChat('certifications', request('id'))])
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            <a class="btn btn-themed btn-primary btn-main-action" id="saveBtn" @click="saveForm('saved')">Submit</a>
                            {{--@if(!@$_GET['id']) <a class="btn-themed-text" @click="saveForm('savemore')" >Submit and add more</a> @endif--}}
                            <a href="/user/settings/get-verified" type="button" class="btn btn-themed btn-preview ver" >Back</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @include('user.settings.partial.submit-notif')
                </div>
            </div>
        </div>
    </div>

@endsection
