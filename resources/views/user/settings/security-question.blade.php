<template id="security-question-template">
<div class="panel-row security-question-template">
    <div class="panel-row" v-if="!displayForm">
        <div class="field">
            <strong>Security Question</strong>
        </div>
        <div class="value">
            <p>
                ●●●●●●●●
            </p>
        </div>
        <div class="actions">
            <a href="javascript:;" class="btn btn-themed" @click="openForm">
                Edit
            </a>
        </div>
    </div>
    <div class="panel-row" v-else transition="ease">
        <div class="field">
            <strong>Security Question</strong>
        </div>
        <div class="value">
            <div class="text">Answer your existing security question in order to create a new one. Please choose a question you can remember! You will get locked out of your account if you are unable to answer correctly. <a href="#"> What’s this?</a>
            </div>
            <div class="form form--security-question">
                <form accept-charset="utf-8" @submit.prevent>
                    <div class="form-row">
                        <div class="form-field">
                            <strong>
                                Existing Question
                            </strong>
                        </div>
                        <div class="input-field" style="position: relative;top: 19px;">
                            @{{ formData.old_question }}
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <strong>
                                Answer
                            </strong>
                        </div>
                        <div class="input-field" :class="[formData.old_answer.error ? 'error' : '' ]">
                            <input type="text" name="old_answer" v-model="formData.old_answer.value" @keyup='formData.old_answer.error = ""'> </input>
                            <div class="error-message">@{{ formData.old_answer.error }}</div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <strong>
                                New Question
                            </strong>
                        </div>
                        <div class="input-field" :class="[formData.new_question.error ? 'error' : '' ]">
                            <custom-select
                                :options="{{ json_encode($security_questions) }}"
                                :selected.sync="formData.new_question.value"
                                name="new question">
                            </custom-select>
                            <div class="error-message">@{{ formData.new_question.error }}</div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field">
                            <strong>
                                New Answer
                            </strong>
                        </div>
                        <div class="input-field" :class="[formData.new_answer.error ? 'error' : '' ]">
                            <input type="answer" name="new_answer" v-model="formData.new_answer.value" @keyup='formData.new_answer.error = ""'> </input>
                            <div class="error-message">@{{ formData.new_answer.error }}</div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-field important-field">
                            <strong>
                                Important
                            </strong>
                        </div>
                        <div class="input-field" :class="[formData.important.error ? 'error' : '' ]">
                            <div class="check">
                                <input type="checkbox" id="option3" name="locked-status" v-model="formData.important.value">
                                <label for="option3">
                                    I understand my will be locked if I am unable to asnwer this question
                                </label>
                            </div>
                            <div class="error-message">@{{ formData.important.error }}</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="actions open">
            <a href="javascript:;" class="btn btn-themed btn-primary" @click="saveQuestion">
                Save
            </a>
            <a href="javascript:;" class="btn btn-themed cancel" @click="cancel">
                Cancel
            </a>
        </div>
    </div>
</div>
</template>

<security-question
    :display-form="displayForm.SecurityQuestion"
    @notification="handleNotification"
    @open-form="currentOpenForm"
    @close-form="closeAllForms">
</security-question>


