<address-settings
        :display-form="displayForm.address"
@notification="handleNotification"
:user-info="userInfo"
@open-form="currentOpenForm"
@close-form="closeAllForms">
</address-settings>