@extends('app')
@section('content')
    <div class="container padd-top-30" id="verified-abn" v-cloak>
        <div class="row back-action">
            <div class="col-md-12">
                <a href="/user/settings/get-verified" class="back-button"><span class="back-icon"></span>Back to Verification</a>
            </div>
        </div>
        @include('user.settings.partial.ver-notif-1')
        <div class="row">
            <div class="col-md-12">
                <div class="main-heading">
                    <h4>Add Your ABN Verification</h4>
                    {{--<p>An Australian Business Number (ABN) is a unique 11 digit number that identifies your business to the government and community.</p>--}}
                    {{--<p>An ABN doesn't replace your tax file number, but it is used for various tax and other business purposes.</p>--}}
                    {{--<p>You may apply for an ABN for free at any time through the Australian Business Register website.</p>--}}
                    {{--<p>LINK: <a href="https://www.business.gov.au/info/plan-and-start/start-your-business/business-and-company-registration/register-for-an-australian-business-number-abn">https://www.business.gov.au/info/plan-and-start/start-your-business/business-and-company-registration/register-for-an-australian-business-number-abn</a></p>--}}
                    {{--<p>Not everyone needs an ABN. To get one you need to be carrying on an enterprise.</p>--}}
                    {{--<p>Without an ABN, parents are meant to withhold 49% of payments you receive and pay that amount to the Australian Taxation Office.</p>--}}
                    <p>It’s free and easy to apply for an ABN. If you don’t have one, <a href="https://abr.gov.au/ABRWeb/Default.aspx?Target=AbnApply&pid=71" target="_blank" rel="noopener">click here to apply.</a></p>
                    <p>Not everyone needs an ABN, only if you’re undertaking this work as an enterprise (like running a business, to make profit!).</p>
                    <p>If you don’t have an ABN, tax gets a little complicated. So, <a href="https://abr.gov.au/For-Business%2C-Super-funds---Charities/ABN-explained/" target="_blank" rel="noopener">read up on ABNs</a>,
                        <a href="https://abr.gov.au/ABRWeb/Default.aspx?Target=AbnApply&pid=71" target="_blank" rel="noopener">apply for one</a>, or list yours below.</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>ABN number</strong>
                </div>
                <div class="col-md-6">
                    <div :class="['full-width', form.abn_number.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.abn_number.error = ''" v-model="form.abn_number.text" data-inputmask="'mask': '999-9999-9999'" name="abn_number"/>
                        <div class="error-message" v-if="form.abn_number.error"> @{{ form.abn_number.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <strong>Notes (message to admin)</strong>
                    <a href="javascript:void(0);" data-toggle="popover-hover" data-placement="top" data-content="You can give us some more information which can be important for us to verify your documents. We will contact you via email if need any additional information">
                        <i class="fa fa-question-circle text-info"></i>
                    </a>
                </div>
                @include('user.settings.chat-view', ['expand'=>true, 'type' => 'abn', 'id'=>null, 'panelId' => 'chat_abn', 'chat' => Auth::user()->getVerificationChat('abn')])
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            <a class="btn btn-themed btn-primary btn-main-action" @click="submit()">Submit</a>
                            <a href="/user/settings/get-verified" type="button" class="btn btn-themed btn-preview ver" >Back</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @include('user.settings.partial.submit-notif')
                </div>
            </div>
        </div>
    </div>

@endsection
