@extends('app')
@section('content')
    <div class="container padd-top-30" id="socialLinks" v-cloak>
        <div class="row back-action">
            <div class="col-md-12">
                <a href="/user/settings/get-verified" class="back-button"><span class="back-icon"></span>Back to Verification</a>
            </div>
        </div>
        @include('user.settings.partial.ver-notif-1')
        <div class="row">
            <div class="col-md-12">
                <div class="main-heading">
                    <h4>Add Your Social links</h4>
                    <p>Authorise us to 'link' your social account to your profile.</p>
                    <p>Parents can't see your social profile, it's just so we can verify you.</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>My social links </strong>
                </div>
                <div class="col-md-7">
                    <div class="social type">
                        <img src="http://head-media.ru/userfiles/image/reklama-na-facebook.png" />
                        <img src="https://static.licdn.com/scds/common/u/images/logos/favicons/v1/favicon.ico" />
                        <img src="https://avatanplus.com/files/resources/original/5761ba6c7938615555c037d1.png" />
                        <img src="http://icons.iconarchive.com/icons/marcus-roberto/google-play/128/Google-plus-icon.png" />
                    </div>
                    <p><b>Add links for one or more of your social media pages</b></p>

                    <div class="col-md-12" v-if="form.soc_links_notes !== null">
                        <a href="javascript:void(0);" @click="addMoreLink()" v-show="form && form.soc_links_notes && form.soc_links_notes.length < 3">+ Add one more link</a>
                    </div>
                    <div :class="['full-width', form.soc_link.error ? 'error' : '' ]">
                        <input

                                style="width:87%; max-width: 259px;"
                                placeholder="http://www.example.com" type="text" @keyup="form.soc_link.error = ''" v-model="form.soc_link.text" name="soc_link"/>
                        <div class="error-message" v-if="form.soc_link.error"> @{{ form.soc_link.error }} </div>
                    </div>


                    <div style="position: relative;top: 25px;">
                        <div class="row form-group" style="margin-left: 0 !important;margin-right: 0 !important;" v-for="(link_n, link) in form.soc_links_notes">
                            <div style="position: relative;width: 100%;">
                                <input style=" max-width: 259px; width: 87%;" v-model="form.soc_links_notes[link_n].text" type="text">
                                &nbsp; <a style="font-size: 1.3em" href="javascript:void(0);" @click="removeRow(link_n)">
                                    <i class="fa fa-close"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Notes (message to admin)</strong>
                    <a href="javascript:void(0);" data-toggle="popover-hover" data-placement="top" data-content="You can give us some more information which can be important for us to verify your documents. We will contact you via email if need any additional information">
                        <i class="fa fa-question-circle text-info"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    @include('user.settings.chat-view', ['expand'=>true, 'type' => 'social', 'id'=>null, 'panelId' => 'chat_social', 'chat' => Auth::user()->getVerificationChat('social')])
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            <a class="btn btn-themed btn-primary btn-main-action" @click="saveLink()">Submit</a>
                            <a href="/user/settings/get-verified" type="button" class="btn btn-themed btn-preview ver" >Back</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @include('user.settings.partial.submit-notif')
                </div>
            </div>
        </div>
    </div>

@endsection
