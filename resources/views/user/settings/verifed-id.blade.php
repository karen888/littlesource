@extends('app')
@section('content')
    <link  href="/croper/cropper.css" rel="stylesheet">
    <div class="container padd-top-30" id="verifed-id" v-cloak>
        <div class="row back-action">
            <div class="col-md-12">
                <a href="/user/settings/get-verified" class="back-button"><span class="back-icon"></span>Back to Verification</a>
            </div>
        </div>
        @include('user.settings.partial.ver-notif-1')
        <div class="row">
            <div class="col-md-12">
                <div class="main-heading">
                    <p>
                        Build your profile to highlight your qualifications, skills, work history and preferences. </p>
                        <p>Little Ones prides itself on only listing qualified caregivers, so make sure you check out our
                        <a href="http://prelaunch.littleones.com.au/caregivers-verification/" style="font-weight: bold; text-decoration: none;">minimum requirements</a> before starting your profile.
                        </p>
                    <p>Once you’ve entered all your details, we will take some time to verify your profile before you can start applying for jobs.</p>
                    </p>
                </div>
            </div>
        </div>
        <hr>
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Profile photo * </strong>
                </div>
                <div class="col-md-9" v-bind:class="['full-width','profile','photo', noPhotoSubmitted ? 'error' : '' ]">
                    <div v-if="((user_photo_100 && add_photo == false) || user_photo_100_temp != '')">
                        <img v-if="user_photo_100_temp != ''" class='profilePhoto img-thumbnail' style="width: 125px" :src="profilePhotoPreviev" />
                        <img v-else class='profilePhoto img-thumbnail' style="width: 125px" :src="user_photo_100" />
                        <label v-if="user_photo_100 != '/img/no_photo_100.jpg'" v-on:click="addPhoto" style="cursor: pointer;" >Change photo</label>
                    </div>
                    <div v-if="(add_photo == false && user_photo_100 == '/img/no_photo_100.jpg')" class="col-md-12">
                        <h6>You need to add a photo!</h6>
                        <p>@{{{ profilePhotoText }}}</p>
                        <label style="cursor: pointer;" v-on:click="addPhoto">Add a photo</label>
                    </div>
                    <div v-if="(add_photo == true && user_photo_100_temp == '')" class="col-md-5">
                        <file-uploader
                            :current_file='user_photo_100_temp'
                            field='user_photo_100_temp'
                            v-on:uploaded="onUserPhotoploaded">
                        </file-uploader>
                    </div>
                    <div v-show="(user_photo_100_temp != '')" class="col-md-5" style="padding: 10px 0;">
                        <p>Please upload a photo so we can verify your identity.</p>
                        <p>This must be an actual photo of you. No logos, digitally altered photos, clip art or group photos.</p>
                        <div id="cropBox">
                            <img no-hide="true" width="264px" :src="user_photo_100_temp" class="resize-image" />
                            <p v-if="photoSelected" style="padding: 15px 0 5px;">Drag frame to adjust portrait.</p>
                            {{--<p v-if="noPhotoSubmitted" style="color:red;">Please upload a photo, we can not verify your ID without a portrait</p>--}}
                        </div>
                        <a style="padding: 16px 18px !important" class="btn btn-themed btn-primary ver btn-main-action form-main-btn" @click="setProfileImage">Submit</a>
                        <a style="padding: 16px 18px !important" class="btn btn-themed btn-preview form-main-btn" data-dismiss="modal" @click="cancelCropPhoto">Cancel</a>
                    </div>
                </div>
                <div class="error-message" v-if="photo_error">@{{ photo_error }}</div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>First name *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.first_name.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.first_name.error = ''" v-model="form.first_name.text" name="first_name"/>
                        <div class="error-message" v-if="form.first_name.error"> @{{ form.first_name.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Last name *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.last_name.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.last_name.error = ''" v-model="form.last_name.text" name="last_name"/>
                        <div class="error-message" v-if="form.last_name.error"> @{{ form.last_name.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Date of Birth *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.birthday.error ? 'error' : '' ]">
                        <input class="nofuture" type="text" @keyup="form.birthday.error = ''" v-model="form.birthday.text" name="birthday"/>
                        <div class="error-message" v-if="form.birthday.error"> @{{ form.birthday.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Scan of ID * </strong>
                </div>
                <div class="col-md-4">
                    <file-uploader
                        :current_file='wwcc_scan'
                        field='wwcc_scan'
                        v-on:uploaded="onFileUploaded">
                    </file-uploader>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Your photo with document*</strong>
                    <a href="javascript:void(0);" class="text-info" data-placement="top" data-toggle="popover-hover" data-content="To make sure it’s you, we need a photo of yourself (face must be visible) holding your ID. The quality of the photo must be good enough that we can see all the information on the ID.">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
                <div class="col-md-4">
                    <file-uploader
                        :current_file='wwcc_scan2'
                        field='wwcc_scan2'
                        v-on:uploaded="onFileUploaded">
                    </file-uploader>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>ID Type *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.prof_id_type.error ? 'error' : '' ]">

                        <custom-select class="choose_doctype"
                                :options="form.prof_id_type.types"
                        :selected.sync="form.prof_id_type.text"
                                       name="prof_id_type"

                        @changed-option="form.prof_id_type.error = ''"

                        >
                        </custom-select>
                        <div style="position: relative; top: 20px;margin-bottom: 20px;" v-if="form.prof_id_type.text === 'Other'">
                            <div class="field-email input-placeholder field-profid-show" style="display: block !important;"><span>Enter document type</span><div class="back"></div></div>
                            <input @keyup="form.prof_id_type.error = ''; form.prof_id_custom_type.error = '';" style="font-size:14px !important;" v-model="form.prof_id_custom_type.text" required placeholder="Enter document type" class="form-control"  type="text" id="field-profid-show" name="profid" />
                        </div>

                        <div class="error-message" v-if="form.prof_id_type.error"> @{{ form.prof_id_type.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>ID Issuing Country *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.prof_id_sc.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.prof_id_sc.error = ''" v-model="form.prof_id_sc.text" name="prof_id_sc"/>
                        <div class="error-message" v-if="form.prof_id_sc.error"> @{{ form.prof_id_sc.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Date of Expiry *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.prof_dateof.error ? 'error' : '' ]">
                        <input class="nopast" type="text" @keyup="form.prof_dateof.error = ''" v-model="form.prof_dateof.text" name="prof_dateof"/>
                        <div class="error-message" v-if="form.prof_dateof.error"> @{{ form.prof_dateof.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong></strong>
                </div>
                <div class="col-md-4" style="">

                    <div class="s">

                        <input type="checkbox" id="notau" v-model="form.notau.checked"/>
                        <label for="notau" class="fx">I am not an Australian citizen</label>
                    </div>


                </div>
            </div>
            <div class="row form-row" v-if="form.notau.checked == true">
                <div class="col-md-3">
                    <strong>My Working Visa</strong>
                </div>
                <div class="col-md-4">
                    <file-uploader
                        :current_file='work_visa'
                        field='work_visa'
                        v-on:uploaded="onFileUploaded">
                    </file-uploader>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Notes (message to admin)</strong>
                    <a href="javascript:void(0);" data-toggle="popover-hover" data-placement="top" data-content="You can give us some more information which can be important for us to verify your documents. We will contact you via email if need any additional information">
                        <i class="fa fa-question-circle text-info"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    @include('user.settings.chat-view', ['expand'=>true, 'type' => 'prof_id', 'id'=>null, 'panelId' => 'chat_prof_id', 'chat' => Auth::user()->getVerificationChat('prof_id')])
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">
                            <button :disabled="loading.scan1 || loading.scan2 || loading.work_visa" class="btn btn-themed btn-primary btn-main-action" @click="postForm()" id="btnsubmit">Submit</button>
                            <a href="/user/settings/get-verified" type="button" class="btn btn-themed btn-preview ver" >Back</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @include('user.settings.partial.submit-notif')
                </div>
            </div>

        </div>


        
    </div>



    @if(request('edit') === 'photo')
        <script>
            $('#photoCrop').modal('show');

        </script>
    @endif

@endsection
