@extends('app')
@section('content')
<div class="container padd-top-30" id="wwcc" v-cloak>
    <div class="row back-action">
        <div class="col-md-12">
            <a href="/user/settings/get-verified" class="back-button"><span class="back-icon"></span>Back to Verification</a>
        </div>
    </div>
    @include('user.settings.partial.ver-notif-1')
    <div class="row">
        <div class="col-md-12">
            <div class="main-heading">
                <h4>Add your Working with Children Check (WWCC)</h4>
                <p>You must have a WWCC card if you want to work with children</p>
            </div>
        </div>
    </div>
    <hr>
    <div class="verify-form job-form">
        <div class="row form-row">
            <div class="col-md-3">
                <strong>WWCC number *</strong>
            </div>
            <div class="col-md-4">
                <div :class="['full-width', form.wwcc.error ? 'error' : '' ]">
                    <input type="text" @keyup="form.wwcc.error = ''" v-model="form.wwcc.text" name="wwcc"/>
                    <div class="error-message" style="color:red;" v-if="form.wwcc.error"> @{{ form.wwcc.error }} </div>
                </div>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>WWCC Scan * </strong>
            </div>
            <div class="col-md-4 full-width">
                <file-uploader
                    :current_file='image'
                    field='image'
                    v-on:uploaded="onFileUploaded">
                </file-uploader>
            </div>
        </div>
        <div class="row form-row">
            <div class="col-md-3">
                <strong>Notes (message to admin)</strong>
                <a href="javascript:void(0);" data-toggle="popover-hover" data-placement="top" data-content="You can give us some more information which can be important for us to verify your documents. We will contact you via email if need any additional information">
                    <i class="fa fa-question-circle text-info"></i>
                </a>
            </div>
            <div class="col-md-6">
                @include('user.settings.chat-view', ['expand'=>true, 'type' => 'wwcc', 'id'=>null, 'panelId' => 'chat_wwcc', 'chat' => Auth::user()->getVerificationChat('wwcc')])
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div class="full-width">
                    <div class="actions">
                        <button :disabled="loading.image" class="btn btn-themed btn-primary btn-main-action" @click="postForm()">Submit</button>
                        <a href="/user/settings/get-verified" type="button" class="btn btn-themed btn-preview ver" >Back</a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                @include('user.settings.partial.submit-notif')
            </div>
        </div>
    </div>
</div>

@endsection
