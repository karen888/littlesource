@extends('user.settings-new')

@section('settings_content')
<div class="row my-info">
    <div class="col-md-12">
        <h5>Notification Settings</h5>
    </div>
</div>
<div class="panel" id="notification-settings">
    <notifications
        :value="notificationValues"
        v-if="displayNotification"
        @notification="handleNotification">
    </notifications>
    <div class="panel-row">
        <div class="with-padding">
            Send email notification to <strong>{{ $currentUser->email }}</strong> when...
        </div>
        <div class="panel-row-title">
            <h5>When i Receive a message</h5>
        </div>
    </div>
    <div class="panel-row">
        <div class="options">
            <strong>Email notifications for unread messages</strong> <br>
            <div class="email-notification-settings">
                Send email aproximately
                <custom-select class="choose_job"
                    :options="{{ json_encode([
                        ['value' => '30min', 'text' => 'Every 30 Minutes'],
                        ['value' => '1h', 'text' => 'Every hour'],
                        ['value' => '1d', 'text' => 'Every day']
                    ])}}"
                    :selected.sync="data.aproximately"
                    name="aproximately"
                >
                </custom-select>
            </div>
            <div class="activity-settings">

                     <input type="radio" name="activity" id="option1" autocomplete="off" v-model="data.activity" value="all"> <label for="option1"><span></span> For All Activity
                </label>

                     <input type="radio" name="activity" id="option2" autocomplete="off" v-model="data.activity" value="important">  <label for="option2"><span></span> Important Activity Only
                </label>
            </div>
            <div class="status-settings">

                    <input type="checkbox" id="option3" name="user-status" v-model="data.send_when_offline"> <label for="option3">Only send when Offline/idle
                </label>
            </div>
        </div>

    </div>
    @if(!$user->is_caregiver)
    <div class="panel-row title">
        <div class="panel-row-title">
            <h5>Recruiting</h5>
        </div>
    </div>
    <div class="panel-row">
        <ul>
            <li class="">
                <input id="rec_1" type="checkbox" name="option1" v-model="data.proposal_received">
                <label class="fx" for="rec_1">A Proposal is received </label>
            </li>
            <li>
                <input id="rec_2" type="checkbox" name="option1" v-model="data.interview_accepted">
                <label class="fx" for="rec_2">An interview is accepted</label>
            </li>
            <li>
                <input id="rec_3" type="checkbox" name="option1" v-model="data.interview_offer_declined_withdrawn">
                <label class="fx" for="rec_3">An interview or offer is declined or withdrawn</label>
            </li>
            <li>
                <input id="rec_4" type="checkbox" name="option1" v-model="data.offer_accepted">
                <label class="fx" for="rec_4">An offer is accepted </label>
            </li>
            <li>
                <input id="rec_5" type="checkbox" name="option1" v-model="data.job_expire_soon">
                <label class="fx" for="rec_5">A job posting will expire soon </label>
            </li>
            <li>
                <input id="rec_6" type="checkbox" name="option1" v-model="data.job_expired">
                <label class="fx" for="rec_6">A job posting expired </label>
            </li>
        </ul>
    </div>
    @else
    <div class="panel-row title">
        <div class="panel-row-title">
            <h5>Caregiver and Agency Proposals</h5>
        </div>
    </div>
    <div class="panel-row">
        <ul>
            <li>
                    <input id="prop_1" type="checkbox" name="option1" v-model="data.offer_interview_received">
                <label class="fx" for="prop_1">An offer or interview invitation is received</label>

            </li>
            <li>
                    <input id="prop_2" type="checkbox" name="option1" v-model="data.offer_interview_withdrawn">
                <label class="fx" for="prop_2">An offer or interview invitation is withdrawn</label>

            </li>
            <li>
                    <input id="prop_3" type="checkbox" name="option1" v-model="data.proposal_rejected">
                <label class="fx" for="prop_2">A Proposal is rejected</label>

            </li>
            <li>
                    <input id="prop_4" type="checkbox" name="option1" v-model="data.job_applied_modified_canceled">
                <label class="fx" for="prop_2">A job I applied to is modified or canceled</label>

            </li>
        </ul>
    </div>
    @endif
    <div class="panel-row title">
        <div class="panel-row-title">
            <h5>Contracts</h5>
        </div>
    </div>
    <div class="panel-row">
        <ul>
            <li>
                <input id="cont_1" type="checkbox" name="option1" v-model="data.contract_ends">
                <label class="fx" for="cont_1">A contract ends</label>
            </li>
            <li>
                <input id="cont_2" type="checkbox" name="option1" v-model="data.feedback_made">
                <label for="cont_2" class="fx">Feedback changes are made  </label>
            </li>
            <li>
                <input id="cont_3" type="checkbox" name="option1" v-model="data.contract_automatically_paused">
                <label for="cont_3" class="fx">A contract is going to be automatically paused</label>
            </li>
        </ul>
    </div>
    <div class="panel-row title">
        <div class="panel-row-title">
            <h5>Tips and Advice</h5>
        </div>
    </div>
    <div class="panel-row">
        <ul>
            <li>
                <input type="checkbox" name="option1" v-model="data.tip_help_start" id="tips">
                <label class="fx" for="tips">Little Ones has a tip to help me start</label>

            </li>
        </ul>
    </div>
    @if($user->is_caregiver)
        <div class="panel-row title">
            <div class="panel-row-title">
                <h5>Job Recommendations</h5>
            </div>
        </div>
        <div class="panel-row">
            <ul style="margin-top: 9px;">
                <li>
                    Little Ones has job recommendations for me
                    <custom-select class="choose_job"
                        :options="{{ json_encode([
                            ['value' => 'daily', 'text' => 'Daily'],
                            ['value' => 'weekly', 'text' => 'Weekly'],
                            ['value' => 'monthly', 'text' => 'Monthly']
                        ])}}"
                       :selected.sync="data.job_recommendations"
                       name="job_recommendations"
                    >
                    </custom-select>
                </li>
            </ul>
        </div>
    @endif
    <div class="panel-row">
        <div class="main-action">
            <a href="javascript:;" class="btn btn-themed btn-primary" @click="saveNotificationSettings()">Save Changes</a>
        </div>
    </div>
</div>

@endsection
