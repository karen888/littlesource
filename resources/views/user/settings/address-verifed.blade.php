@extends('app')
@section('content')
    <div class="container padd-top-30" id="adress-verifed" v-cloak>
        <div class="row back-action">
            <div class="col-md-12">
                <a href="/user/settings/get-verified" class="back-button"><span class="back-icon"></span>Back to Verification</a>
            </div>
        </div>
        @include('user.settings.partial.ver-notif-1')
        <div class="row">
            <div class="col-md-12">
                <div class="main-heading">
                    <h4>Add Your location</h4>
                    <p>Pleace input your full address</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="verify-form job-form">
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Address *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.loc_adress.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.loc_adress.error = ''" v-model="form.loc_adress.text" name="loc_adress" placeholder="Street address"/>
                        <div class="error-message" v-if="form.loc_adress.error"> @{{ form.loc_adress.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>State *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.loc_adress2.error ? 'error' : '' ]">
                        <custom-select class="choose_doctype"
                                       :options="form.loc_adress2.states"
                        @click=""
                        :selected.sync="form.loc_adress2.text"
                        @changed-option="form.loc_adress2.error = ''"
                        name="adress2"
                        :placeholder="Select state from list"
                        >
                        </custom-select>
                        {{--<input type="text" @keyup="form.loc_adress2.error = ''" v-model="form.loc_adress2.text" name="loc_adress2"/>--}}
                        <div class="error-message" v-if="form.loc_adress2.error"> @{{ form.loc_adress2.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>City *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.loc_city.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.loc_city.error = ''" v-model="form.loc_city.text" name="loc_city" placeholder="Suburb"/>
                        <div class="error-message" v-if="form.loc_city.error"> @{{ form.loc_city.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Postcode *</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.loc_zip.error ? 'error' : '' ]">
                        <input type="text" @keyup="form.loc_zip.error = ''" v-model="form.loc_zip.text" name="loc_zip" placeholder="0000"/>
                        <div class="error-message" v-if="form.loc_zip.error"> @{{ form.loc_zip.error }} </div>
                    </div>
                </div>
            </div>

            <div class="row form-row ">
                <div class="col-md-3">
                    <strong>Proof of Address * </strong>
                </div>
                <div class="col-md-4">
                    <file-uploader
                        :current_file='image'
                        field='image'
                        v-on:uploaded="onFileUploaded">
                    </file-uploader>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Document Type *</strong>
                </div>
                <div class="col-md-4" style="">
                    <div class="full-width" :class="[form.loc_type_id.error ? 'error' : '' ]">
                        <custom-select2 class="choose_doctype"
                            :options="documentType"
                            :selected.sync="form.loc_type_id.selected"
                            name="Document Type"
                            @changed-option="form.loc_type_id.error = ''"
                        >
                        </custom-select2>
                        <div style="position: relative; top: 20px;margin-bottom: 20px;" v-if="form.loc_type_id.selected == 9">
                            <div class="field-email input-placeholder field-profid-show" style="display: block !important;"><span>Enter document type</span><div class="back"></div></div>
                            <input v-model="form.loc_type_id.text" required placeholder="Enter document type" class="form-control"  type="text" id="field-profid-show" name="profid"/>
                        </div>
                        <div class="error-message" v-if="form.loc_type_id.error"> @{{ form.loc_type_id.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Date of Issue (if any)</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.date_earned.error ? 'error' : '' ]">
                        <input class="nofuture" type="text" @keyup="form.issue_date.error = ''" v-model="form.issue_date.text" name="issue_date" placeholder=""/>
                        <div class="error-message" v-if="form.issue_date.error"> @{{ form.issue_date.error }} </div>
                    </div>
                </div>
            </div>
            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Date of expiry (if any)</strong>
                </div>
                <div class="col-md-4">
                    <div :class="['full-width', form.date_earned.error ? 'error' : '' ]">
                        <input class="nopast" type="text" @keyup="form.expiry_date.error = ''" v-model="form.expiry_date.text" name="expiry_date" placeholder=""/>
                        <div class="error-message" v-if="form.expiry_date.error"> @{{ form.expiry_date.error }} </div>
                    </div>
                </div>
            </div>
            {{--<div class="row form-row">--}}
                {{--<div class="col-md-3">--}}
                    {{--<strong>Date of Issuing *</strong>--}}
                {{--</div>--}}
                {{--<div class="col-md-4">--}}
                    {{--<div :class="['full-width', form.loc_date.error ? 'error' : '' ]">--}}
                        {{--<input class="nofuture" id="dateis" type="text" @keyup="form.loc_date.error = ''" v-model="form.loc_date.text" name="loc_date" placeholder=""/>--}}
                        {{--<div class="error-message" v-if="form.loc_date.error"> @{{ form.loc_date.error }} </div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="row form-row">
                <div class="col-md-3">
                    <strong>Notes (message to admin)</strong>
                    <a href="javascript:void(0);" data-toggle="popover-hover" data-placement="top" data-content="You can give us some more information which can be important for us to verify your documents. We will contact you via email if need any additional information">
                        <i class="fa fa-question-circle text-info"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    @include('user.settings.chat-view', ['expand'=>true, 'type' => 'address', 'id'=>null, 'panelId' => 'chat_address', 'chat' => Auth::user()->getVerificationChat('address')])
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <div class="full-width">
                        <div class="actions">

                            <button :disabled="loading.image" class="btn btn-themed btn-primary btn-main-action" @click="postForm()">Submit</button>
                            <a href="/user/settings/get-verified" type="button" class="btn btn-themed btn-preview ver btn-cancel" >Back</a>

                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    @include('user.settings.partial.submit-notif')
                </div>
            </div>
        </div>
    </div>

@endsection
