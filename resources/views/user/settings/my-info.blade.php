@extends('user.settings-new')

@section('settings_content')
<div id="my-info">
    <notifications
        :value="notificationValues"
        v-if="displayNotification"
        @notification="handleNotification">
    </notifications>
    @include('user.settings.profile-picture')

    <div class="row my-info">
        <div class="col-md-12">
            <h4>Contact Details</h4>
        </div>
    </div>
    <div class="panel panel-default">
        <name-settings
            :display-form="displayForm.name"
            @notification="handleNotification"
            :user-info="userInfo"
            @open-form="currentOpenForm"
            @close-form="closeAllForms">
        </name-settings>

        <birthday-settings
            :display-form="displayForm.birthday"
            @notification="handleNotification"
            :user-info="userInfo"
            @open-form="currentOpenForm"
            @close-form="closeAllForms">
        </birthday-settings>        

        @if(Auth::user()->is_caregiver)
            <div class="panel-row address-template">
                <div class="field">
                    <strong>Address</strong>
                </div>
                <div class="value">
            @if(Auth::user()->info->loc_address != '')
            <div>
                <a href="/user/settings/get-verified/address">Enter your Address</a> so you can apply to jobs
            </div>
            @else
            <div>
                <?php $user6 = Auth::user()->info;?>
                @if(!empty($user6->loc_adress))
                <p>{{$user6->loc_adress}}</p>
                <p>{{$user6->loc_city}}, {{$user6->loc_adress2}}</p>
                @endif
                <p>{{$user6->loc_zip}}</p>
            </div>

            @endif
                </div>
                <div class="actions">
                    <a href="/user/settings/get-verified/address" id="editAddress" class="btn btn-themed">
                        Edit
                    </a>
                </div>
            </div>

        @else
            @include('user.settings.address')
        @endif

        <email-settings
            :display-form="displayForm.email"
            @notification="handleNotification"
            :user-info="userInfo"
            @open-form="currentOpenForm"
            @close-form="closeAllForms">
        </email-settings>

        <timezone-settings
            :display-form="displayForm.timezone"
            @notification="handleNotification"
            :user-info="userInfo"
            @open-form="currentOpenForm"
            @close-form="closeAllForms">
        </timezone-settings>

        <phone-settings
            :display-form="displayForm.phone"
            @notification="handleNotification"
            :user-info="userInfo"
            @open-form="currentOpenForm"
            @close-form="closeAllForms">
        </phone-settings>
        @if(false)
            <abn-settings
                :display-form="displayForm.abn_number"
                @notification="handleNotification"
                :user-info="userInfo"
                @open-form="currentOpenForm"
                @close-form="closeAllForms">
            </abn-settings>
        @endif
    </div>
</div>
@endsection
