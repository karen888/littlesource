@extends('user.settings-new')

@section('settings_content')
<div id="password-and-security">
    <notifications
        :value="notificationValues"
        v-if="displayNotification"
        @notification="handleNotification">
    </notifications>
    <div class="row my-info">
        <div class="col-md-12">
            <h5>Password & Security</h5>
        </div>
    </div>
    <div class="panel">
        @include('user.settings.password')

        @if(!Auth::user()->is_caregiver)
        @include('user.settings.security-question')
        @include('user.settings.security-email')
        @endif
        {{-- <security-question
            @open-form="currentFormOpen">
        </security-question> --}}


    </div>
</div>
@endsection

@section('js')
    <script src="https://rawgit.com/matoilic/jquery.pwstrength/master/jquery.pwstrength.min.js"></script>

    <script>
        $(document).ready(function(){
            window.bindpwdstr_checker = window.setInterval(function(){
                if((typeof window.bindpwdstr) !== "undefined") {
                    window.bindpwdstr();
                    window.clearInterval(window.bindpwdstr_checker);
                }
            }, 200);
        });
    </script>
@endsection
