@extends('app')

@section('content')
<div class="row padd-top">

    <div class="col-md-2">
        <h4>User Settings</h4>
        <div>
            <ul class="nav nav-pills nav-stacked">
                <li><a href="/user/setting/contact">Contact Info</a></li>
                <li><a href="/user/setting/profile">Profile Settings</a></li>
                <li><a href="{{ routeClear('user_link') }}/{{ Auth::user()->user_path }}">My Profile</a></li>
                <li><a href="/user/setting/deposit">Billing Methods</a></li>
                <li><a href="/user/setting/password">Change Password</a></li>

            </ul>
        </div>
    </div>

    <div class="col-md-10">
        @yield('content_user_settings')
    </div>

</div>
@endsection
