@extends('user.setting')

@section('content_user_settings')
<h3>Billing Methods</h3>

<div class="row text-right">
    <a href="{{ routeClear('user_setting') }}/deposit/add" class="btn btn-primary">Add a Credit card</a>
</div>

<br>
<div class="row">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>
                </th>
                <th>
                    Status
                </th>
                <th>
                    Actions
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($cards_list as $val)
                <tr>
                    <td>
                        {{ $val->card_description }}
                    </td>
                    <td>
                        @if ($val->card_status == 1)
                            <i class="fa fa-check text-success" aria-hidden="true"></i>
                        @else
                            <i class="fa fa-exclamation text-danger" aria-hidden="true"></i>
                        @endif
                    </td>
                    <td>
                        <form class="form-horizontal" role="form" method="POST" action="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="card_id" value="{{ $val->id }}">
                            
                            <a href="{{ $val->link_edit }}" class="btn btn-success btn-xs"><i class="fa fa-edit "></i></a>
                            <button type="submit" class="btn btn-danger btn-xs send-req-form" data-confirm="Are you sure?" data-loading-text=".."><i class="fa fa-remove"></i></button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection
