@extends('user.setting')

@section('content_user_settings')

    <h3>Profile Settings</h3>

    <div class="panel panel-default">
        <div class="panel-heading"></div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="proc" value="photo">

    			<div class="form-group" data-name="user_title">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="thumbnail pull-left">
                            <img src="{{ userPhoto(\Auth::user())->photo_100 }}" width="100" heigth="100" class="">
                        </div>
                        <div class="clearfix"></div>
                    </div>
    			</div>

    			<div class="form-group" data-name="file_name">
    				<label class="col-md-4 control-label">Choose a file</label>
                    <div class="col-md-6 upload-file-block">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-themed btn-default btn-file btn-sm"> Browse&hellip; <input type="file" name="file" class="input-sm"></span>
                            </span>
                            <input type="text" class="form-control input-sm" name="file_name" readonly>
                            <span class="input-group-btn">
                                <a href="" class="btn btn-themed btn-default btn-sm upload-file-clear">Clear</a>
                            </span>
                            <span class="input-group-btn" style="display:none;">
                                <a href="" class="btn btn-themed btn-default btn-sm upload-file-remove">Remove</a>
                            </span>
                        </div>
                    </div>
    			</div>

    			<div class="form-group" data-name="">
    				<label class="col-md-4 control-label">Delete photo</label>
    				<div class="col-md-6 form-control-static">
        				<input type="checkbox" name="delete">
    				</div>
    			</div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary send-req-form">Save</button>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4 alert alert-success alert-hidden">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">Info</div>
        <div class="panel-body">

            <form class="form-horizontal" role="form" method="POST" action="">
            	<input type="hidden" name="_token" value="{{ csrf_token() }}">

    			<div class="form-group" data-name="user_rate">
    				<label class="col-md-4 control-label">Rate</label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input type="text" class="form-control" name="user_rate" value="{{ Auth::user()->user_rate }}">
                            <span class="input-group-addon">/hr</span>
                        </div>
    				</div>
    			</div>

    			<div class="form-group" data-name="user_title">
    				<label class="col-md-4 control-label">Title</label>
    				<div class="col-md-6">
    					<input type="text" class="form-control" name="user_title" value="{{ Auth::user()->user_title }}">
    				</div>
    			</div>

    			<div class="form-group" data-name="user_overview">
    				<label class="col-md-4 control-label">Overview</label>
    				<div class="col-md-6">
    					<textarea  class="form-control" name="user_overview" rows="10">{{ Auth::user()->user_overview }}</textarea>
    				</div>
    			</div>

    			<div class="form-group" data-name="my_skills">
    				<label class="col-md-4 control-label">Skills</label>
    				<div class="col-md-6">
                        <select id="my-skills" class="form-control" name="skills[]" multiple="multiple">
                        @foreach ($skills_list as $val)
                            <option value="{{ $val->skill_id }}" selected="selected">{{ $val->skill_name }}</option>
                        @endforeach
                        </select>
                    </div>
    			</div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary send-req-form">Save</button>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4 alert alert-success alert-hidden">
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">Availability</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="proc" value="avail">

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default {{ $user_info->available_active }}">
                                <input type="radio" name="available" value="1" id="available1" autocomplete="off" {{ $user_info->available_checked }}>Available
                            </label>
                            <label class="btn btn-default {{ $user_info->not_available_active }}">
                                <input type="radio" name="available" value="0" id="available2" autocomplete="off" {{ $user_info->not_available_checked }}>Not Available
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary send-req-form">Save</button>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4 alert alert-success alert-hidden">
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">Location</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="proc" value="location">

    			<div class="form-group" data-name="country">
    				<label class="col-md-4 control-label">Country</label>
    				<div class="col-md-6">
                        <select class="form-control" name="country">
                            <option value="0">Choose a country...</option>
                            @foreach($countries as $val)
                                <option value="{{ $val->id }}" {{ ($val->id == Auth::user()->country_id) ? 'selected' : '' }}>{{ $val->country_name }}</option>
                            @endforeach
                        </select>
    				</div>
                </div>

    			<div class="form-group" data-name="city">
    				<label class="col-md-4 control-label">City</label>
    				<div class="col-md-6">
    					<input type="text" class="form-control" name="city" value="{{ Auth::user()->user_city }}">
    				</div>
                </div>

    			<div class="form-group" data-name="timezone">
    				<label class="col-md-4 control-label">Time Zone</label>
    				<div class="col-md-6">
                        <select class="form-control" name="timezone">
                            <option value="0">Choose your timezone...</option>
                            @foreach($timezones as $val)
                                <option value="{{ $val->id }}" {{ ($val->id == Auth::user()->timezone_id) ? 'selected' : '' }}>{{ $val->timezone_description }}</option>
                            @endforeach
                        </select>
    				</div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary send-req-form">Save</button>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4 alert alert-success alert-hidden">
                    </div>
                </div>

            </form>
        </div>
    </div>

@endsection
