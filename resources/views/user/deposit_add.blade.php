@extends('user.setting')

@section('content_user_settings')
<div class="page-header">
    <h3>Add a Credit or Debit Card</h3>
</div>

<div class="row">
    <form class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

    	<div class="form-group" data-name="description">
    		<label class="col-md-4 control-label">Description</label>
    		<div class="col-md-6">
        		<input type="text" class="form-control" name="description" value="{{ $data_info->card_description or '' }}">
    		</div>
    	</div>

    	<div class="form-group" data-name="firstname">
    		<label class="col-md-4 control-label">First Name</label>
    		<div class="col-md-6">
        		<input type="text" class="form-control" name="firstname" value="{{ $data_info->card_firstname or '' }}">
    		</div>
    	</div>

    	<div class="form-group" data-name="lastname">
    		<label class="col-md-4 control-label">Last Name</label>
    		<div class="col-md-6">
        		<input type="text" class="form-control" name="lastname" value="{{ $data_info->card_lastname or '' }}">
    		</div>
    	</div>

    	<div class="form-group" data-name="card_type">
    		<label class="col-md-4 control-label">Card Type</label>
    		<div class="col-md-6">
                <select class="form-control" name="card_type">
                    <option value="0">choose please...</option>
                    @foreach($card_type_list as $val)
                        <option value="{{ $val }}" {{ ($val == $data_info->card_type) ? 'selected' : '' }}>{{ $val }}</option>
                    @endforeach
                </select>
    		</div>
    	</div>

    	<div class="form-group" data-name="card_number">
    		<label class="col-md-4 control-label">Credit Card Number</label>
    		<div class="col-md-6">
        		<input type="text" class="form-control" name="card_number" value="{{ $data_info->card_number or '' }}">
    		</div>
    	</div>

    	<div class="form-group" data-name="expdate">
    		<label class="col-md-4 control-label">Expiration Date</label>
            <div class="col-md-3">
                <select class="form-control" name="expdate_month">
                    <option value="0">month</option>
                    @foreach($expdate_list->months as $key => $val)
                        <option value="{{ $key }}" {{ ($key == $data_info->expdate_month) ? 'selected' : '' }}>{{ $key }} ({{ $val }})</option>
                    @endforeach
                </select>
    		</div>
            <div class="col-md-3">
                <select class="form-control" name="expdate_year">
                    <option value="0">year</option>
                    @foreach($expdate_list->years as $val)
                        <option value="{{ $val }}" {{ ($val == $data_info->expdate_year) ? 'selected' : '' }}>{{ $val }}</option>
                    @endforeach
                </select>
    		</div>
    	</div>

    	<div class="form-group" data-name="cvv2">
    		<label class="col-md-4 control-label">Security Code (CVV2)</label>
    		<div class="col-md-6">
        		<input type="text" class="form-control" name="cvv2" value="{{ $data_info->card_cvv2 or '' }}">
    		</div>
    	</div>

    	<div class="form-group" data-name="country">
    		<label class="col-md-4 control-label">Country</label>
    		<div class="col-md-6">
                <select class="form-control" name="country">
                    <option value="0">Choose a country...</option>
                    @foreach($countries as $val)
                        <option value="{{ $val->country_code }}" {{ ($val->country_code == $data_info->card_country_code) ? 'selected' : '' }}>{{ $val->country_name }}</option>
                    @endforeach
                </select>
    		</div>
    	</div>

    	<div class="form-group" data-name="address1">
    		<label class="col-md-4 control-label">Address</label>
    		<div class="col-md-6">
        		<input type="text" class="form-control" name="address1" value="{{ $data_info->card_address1 or '' }}">
    		</div>
    	</div>

    	<div class="form-group" data-name="address2">
    		<div class="col-md-6 col-md-offset-4">
        		<input type="text" class="form-control" name="address2" value="{{ $data_info->card_address2 or '' }}">
    		</div>
    	</div>

    	<div class="form-group" data-name="city">
    		<label class="col-md-4 control-label">City</label>
    		<div class="col-md-6">
        		<input type="text" class="form-control" name="city" value="{{ $data_info->card_city or '' }}">
    		</div>
    	</div>

    	<div class="form-group" data-name="zip_code">
    		<label class="col-md-4 control-label">Postcode</label>
    		<div class="col-md-6">
        		<input type="text" class="form-control" name="zip_code" value="{{ $data_info->card_zip_code or '' }}">
    		</div>
    	</div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                In order to verify your card, <b>we may make 1 temporary charge totaling $10 (it can take some time).</b> These charges will be refunded to your credit card within 10 days.
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary send-req-form">Save</button>
                <a href="{{ routeClear('user_setting') }}/deposit" class="btn btn-default">Cancel</a>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-6 col-md-offset-4 alert alert-warning alert-hidden">aaaa
            </div>
        </div>
    </form>
</div>

@endsection
