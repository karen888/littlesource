
<div class="billing-method-form">
    <div class="form-row">
        <div class="field">
            <strong>Status</strong>
        </div>
        <div class="value">
            <div class="col-sm-1">
                <input type="checkbox" v-model="card_status" id="card_status" checked data-group-cls="btn-group-justified">
            </div>
             <p style="margin: 10px 0px 0px 0px; line-height: 20px;">This will be your Primary Payment Method</p>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>First Name</strong>
        </div>
        <div class="value" :class="[fname.errors.message ? 'error' : '' ]">
            <input type="text" v-model="fname.text" @keyup="fname.errors.message = ''" value={{ $data_cards->card_firstname }} />
            <div class="error-message" v-if="fname.errors.message">@{{ fname.errors.message }}</div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Last Name</strong>
        </div>
        <div class="value" :class="[lname.errors.message ? 'error' : '' ]">
            <input type="text" v-model="lname.text" @keyup="lname.errors.message = ''" value={{ $data_cards->card_lastname }} />
            <div class="error-message" v-if="lname.errors.message">@{{ lname.errors.message }}</div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Card Number</strong>
        </div>
        <div class="value" :class="[card_number.errors.message ? 'error' : '' ]">
            <input type="text" v-model="card_number.text" @keyup="card_number.errors.message = ''"  value={{ $data_cards->card_number }} />
            <div class="error-message" v-if="card_number.errors.message">@{{ card_number.errors.message }}</div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Expiration Date</strong>
        </div>

        <div class="value">
            <div class="expiration-date">
                <div class="month">
                    <?php $card_d= explode("-", $data_cards->card_expdate); ?>
                    <div :class="[exp_date_m.errors.message ? 'error' : '' ]">
                        <custom-select class="choose_job"
                                       :options="{{ json_encode([['value' => 1, 'text' => '1'],
                                       ['value' => 2, 'text' => '2'],['value' => 3, 'text' => '3'],['value' => 4, 'text' => '4'],
                                       ['value' => 5, 'text' => '5'],['value' => 6, 'text' => '6'],['value' => 7, 'text' => '7'],
                                       ['value' => 8, 'text' => '8'],['value' => 9, 'text' => '9'],['value' => 10, 'text' => '10'],
                                       ['value' => 11, 'text' => '11'],['value' => 12, 'text' => '12']]) }}"
                        @click="exp_date_m.errors.message = ''"
                        :selected.sync="{{ $card_d[0] }}"
                        @selected-value="expDateMSelectedValue"
                        name="expiration month"
                        >
                        </custom-select>
                        <div class="error-message" v-if="exp_date_m.errors.message">@{{ exp_date_m.errors.message }}</div>
                    </div>
                </div>
                <div class="year">
                    <div :class="[exp_date_y.errors.message ? 'error' : '' ]">
                        <custom-select class="choose_job"
                                       :options="{{ json_encode([
                                       ['value' => intval($year), 'text' => $year++],['value' => intval($year), 'text' => $year++],['value' => intval($year), 'text' => $year++],
                                       ['value' => intval($year), 'text' => $year++],['value' => intval($year), 'text' => $year++],
                                       ['value' => intval($year), 'text' => $year++],['value' => intval($year), 'text' => $year++],
                                       ['value' => intval($year), 'text' => $year++],['value' => intval($year), 'text' => $year++],
                                       ['value' => intval($year), 'text' => $year++]]) }}"
                        @click="exp_date_y.errors.message = ''"
                        :selected.sync="{{ $card_d[1] }}"
                        @selected-value="expDateYSelectedValue"
                        name="expiration year"
                        >
                        </custom-select>
                        <div class="error-message" v-if="exp_date_y.errors.message">@{{ exp_date_y.errors.message }}</div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Security Code</strong>
        </div>
        <div class="value">
            <div class="security-code" :class="[security_code.errors.message ? 'error' : '' ]">
                <input type="text" v-model="security_code.text" @keyup="security_code.errors.message = ''" value={{ $data_cards->card_cvv2 }} />
                <a href="#">What’s this?</a>
            </div>
            <div class="error-message" v-if="security_code.errors.message">@{{ security_code.errors.message }}</div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Address</strong>
        </div>
        <div class="value" :class="[address.errors.message ? 'error' : '' ]">
            <input type="text" v-model="address.text" @keyup="address.errors.message = ''"  value={{ $data_cards->card_address1 }} />
            <div class="error-message" v-if="address.errors.message">@{{ address.errors.message }}</div>
        </div>
    </div>

    <div class="form-row">
        <div class="field">
            <strong>Address</strong> (optional)
        </div>
        <div class="value">
            <input type="text" v-model="address_optional" value="{{ $data_cards->card_address2 }}" />
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>City</strong>
        </div>
        <div class="value" :class="[city.errors.message ? 'error' : '' ]">
            <input type="text" v-model="city.text" @keyup="city.errors.message = ''" value={{ $data_cards->card_city }} />
            <div class="error-message" v-if="city.errors.message">@{{ city.errors.message }}</div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Postal Code</strong>
        </div>
        <div class="value" :class="[postal_code.errors.message ? 'error' : '' ]">
            <input type="text" v-model="postal_code.text" @keyup="postal_code.errors.message = ''" value={{ $data_cards->card_zip_code }} />
            <div class="error-message" v-if="postal_code.errors.message">@{{ postal_code.errors.message }}</div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Phone</strong>
        </div>
        <div class="value">
            <div class="phone">
                <div class="int-code">
                    <div class="inner-addon phone-addon left-addon">
                        <div class="icon-phone-addon">+</div>
                        <input type="text"  />
                    </div>
                </div>
                <div class="area-code">
                    <div class="inner-addon phone-addon left-addon right-addon">
                        <div class="icon-phone-addon">(</div>
                        <input type="text" />
                        <div class="icon-phone-addon-right">)</div>
                    </div>
                </div>
                <div class="number">
                    <input type="text" />
                </div>
            </div>
        </div>
    </div>
    <div class="billing-alert">
        <div class="alert alert-info">
            <strong>Note:</strong><br>
            In order to verify your card, we may make 2 temporary charges totally $10. These charges will be refunded to your card within 10 days.
            <a href="#">Learn more</a>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#toggle-event').change(function() {
           alert("fsd");
        });
    });
</script>