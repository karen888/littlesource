@extends('user.settings-new')

<?php $billingMethods = collect([]); ?>

@section('settings_content')
<div class="row my-info">
    <div class="col-md-12">
        <h5>Billing Methods</h5>
    </div>
</div>

@if(!$user->cards)
<div class="row">
    <div class="col-md-12">
        <div class="alert alert-info clearfix">
            <div class="icon"></div>
            <p>
                In order to hire a caregiver on Little Ones we require at least one payment method on file.
                Remember, you don't pay for anything until after the caregiver starts working.
            </p>
        </div>
    </div>
</div>
@endif


    @if(!$user->cards)
    <div class="panel-row empty">
        <h5>No Billing methods set up yet. <a href="{{ route('client.billing-methods.create') }}"> Add Billing Method</a></h5>
    </div>
    @else
            @foreach($card as $cards)

            <div class="panel panel--billing-methods" id="{{ $cards->id }}">
                    <div class="panel-row">
                        <div class="card-info">
                            Credit Card — Account ending in {{ $cards->card_expdate }}
                            <div class="date-posted">

                            </div>
                        </div>
                        <div class="autopay-status">
                            AutoPay status:
                            @if($cards->card_status==0)
                              Off
                                @else
                                On
                            @endif

                        </div>
                        <div class="actions">
                            <div class="actions-group">
                                <div class="trigger">
                                    <div class="text_outer"><div class="text trn"> Actions </div></div>
                                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                </div>
                                <div class="list">
                                    <div class="option"
                                    data-action="view-profile"
                                    class="trn">
                                        <a  href="{{ route('client.billing-methods.update', ['id' => $cards->id]) }}" class="updateInfo" data-id="{{ $cards->id }}">Update Info</a>
                                    </div>
                                    <div class="option"
                                    data-action="send-message"
                                    class="trn">
                                        <a href="#" class="remove" data-remove="{{ $cards->id }}">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            @endforeach
    @endif



@if($user->cards)
<a href="{{ route('client.billing-methods.create') }}">+ Add a Billing Method</a>
@endunless


    <script type="text/javascript">
        $(document).on("click", ".remove", function(e) {
            e.preventDefault();
            var id = $(this).attr("data-remove");
            var token = $("#token").val()
            if (confirm('Are you sure you want to delete')) {
                $.ajax({
                    url: "/ajax/delete-billing-method",
                    type: "POST",
                    dataType: "json",
                    data: { _token: token,id: id},

                    success: function (data) {
                       $("#"+id).remove();


            },
                    error: function () {
                        alert("No");

                    }
                });
            }

        });


    </script>

@endsection
