<div class="billing-method-form">
    <div class="form-row">
        <div class="field">
            <strong>Status</strong>
        </div>
        <div class="value">
            This will be your Primary Payment Method
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>First Name</strong>
        </div>
        <div class="value" :class="[fname.errors.message ? 'error' : '' ]">
            <input type="text" v-model="fname.text" @keyup="fname.errors.message = ''"  />
            <div class="error-message" v-if="fname.errors.message"><span v-html="fname.errors.message"></span></div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Last Name</strong>
        </div>
        <div class="value" :class="[lname.errors.message ? 'error' : '' ]">
            <input type="text" v-model="lname.text" @keyup="lname.errors.message = ''" />
            <div class="error-message" v-if="lname.errors.message"><span v-html="lname.errors.message"></span></div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Card Number</strong>
        </div>
        <div class="value" :class="[card_number.errors.message ? 'error' : '' ]">
            <input type="text" v-model="card_number.text" @keyup="card_number.errors.message = ''" />
            <div class="error-message" v-if="card_number.errors.message"><span v-html="card_number.errors.message"></span></div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Expiration Date</strong>
        </div>
        <div class="value">
            <div class="expiration-date">
                <div class="month">
                    <div :class="[exp_date_m.errors.message ? 'error' : '' ]">
                        <custom-select class="choose_job"
                            :options="{{ json_encode([['value' => '1', 'text' => '1'],['value' => '2', 'text' => '2'],['value' => '3', 'text' => '3'],['value' => '4', 'text' => '4'],['value' => '5', 'text' => '5'],['value' => '6', 'text' => '6'],['value' => '7', 'text' => '7'],['value' => '8', 'text' => '8'],['value' => '9', 'text' => '9'],['value' => '10', 'text' => '10'],['value' => '11', 'text' => '11'],['value' => '12', 'text' => '12']]) }}"
                            @click="exp_date_m.errors.message = ''"
                            :selected.sync="exp_date_m.selected"
                            @selected-value="expDateMSelectedValue"
                            name="expiration month"
                        >
                        </custom-select>
                        <div class="error-message" v-if="exp_date_m.errors.message"><span v-html="exp_date_m.errors.message"></span></div>
                    </div>
                </div>
                <div class="year">
                    <div :class="[exp_date_y.errors.message ? 'error' : '' ]">
                        <custom-select class="choose_job"
                            :options="{{ json_encode([['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++],['value' => $year, 'text' => $year++]]) }}"
                            @click="exp_date_y.errors.message = ''"
                            :selected.sync="exp_date_y.selected"
                            @selected-value="expDateYSelectedValue"
                            name="expiration year"
                        >
                        </custom-select>
                        <div class="error-message" v-if="exp_date_y.errors.message"><span v-html="exp_date_y.errors.message"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Security Code</strong>
        </div>
        <div class="value">
            <div class="security-code" :class="[security_code.errors.message ? 'error' : '' ]">
                <input type="text" v-model="security_code.text" @keyup="security_code.errors.message = ''" />
                <a href="#">What’s this?</a>
            </div>
            <div class="error-message" v-if="security_code.errors.message"><span v-html="security_code.errors.message"></span></div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Address</strong>
        </div>
        <div class="value" :class="[address.errors.message ? 'error' : '' ]">
            <input type="text" v-model="address.text" @keyup="address.errors.message = ''" />
            <div class="error-message" v-if="address.errors.message"><span v-html="address.errors.message"></span></div>
        </div>
    </div>

    <div class="form-row">
        <div class="field">
            <strong>Address</strong> (optional)
        </div>
        <div class="value">
            <input type="text" v-model="address_optional" />
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>City</strong>
        </div>
        <div class="value" :class="[city.errors.message ? 'error' : '' ]">
            <input type="text" v-model="city.text" @keyup="city.errors.message = ''" />
            <div class="error-message" v-if="city.errors.message"><span v-html="city.errors.message"></span></div>
        </div>
    </div>
    <div class="form-row">
        <div class="field">
            <strong>Post Code</strong>
        </div>
        <div class="value" :class="[postal_code.errors.message ? 'error' : '' ]">
            <input type="text" v-model="postal_code.text" @keyup="postal_code.errors.message = ''" />
            <div class="error-message" v-if="postal_code.errors.message"><span v-html="postal_code.errors.message"></span></div>
        </div>
    </div>
    <div class="billing-alert">
        <div class="alert alert-info">
            <strong>Note:</strong><br>
            In order to verify your card, we may make 2 temporary charges totally $10. These charges will be refunded to your card within 10 days.
            <a href="#">Learn more</a>
        </div>
    </div>
</div>