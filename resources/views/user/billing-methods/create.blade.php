@extends('app')

@section('content')
<div class="container">
    <div class="content">
        <div class="row padd-top">
            <div class="col-md-10 col-md-push-1">
                <h4>Add a Credit or Debit Card</h4>
                <h6>Remember, you don’t pay anything until after the caregiver starts working.</h6>
                <div class="panel add-billing-method" id="hire-employee">
                    {!! Form::open() !!}
                        <div class="panel-row">
                            <h5><strong>Payment Information</strong></h5>
                            @include('user.billing-methods.form')
                        </div>
                        <div class="panel-row">
                            <div class="actions">
                                <a href="javascript:;" class="btn btn-themed btn-primary btn-main-action" @click="createCardBilling()">Add Card</a>
                                <a href="javascript:;" class="btn btn-themed btn-preview" onclick="window.location='/user/settings/billing'">Cancel</a>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection