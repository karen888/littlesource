import Vue from 'vue';
import CustomSelect from './components/CustomSelect.vue';
import $ from 'jquery';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

/*
 For password and security Settings
 */
if (document.getElementById('invite-job')) {
    var vm = new Vue({
        el: '#invite-job',
        data: {
            job: {
                selected: '',
                errors: {
                    message: ''
                },
                text: '',
            },
            duration: {
                selected: '',
                errors: {
                    message: ''
                },
                text: '',
            },
            workload: {
                selected: '',
                errors: {
                    message: ''
                },
                text: '',
            },
            message: {
                text: "Hello!\n\nI'd like invite you to apply my job. Please review the job post and apply if you are available.",
                errors: {
                    message: '',
                },
            },
            caregiver: {
                text: '',
                errors: {
                    message: '',
                },
            }
        },
        components: {
            'custom-select': CustomSelect
        },
        methods: {
            cancelJobInvite: function() {
                window.location = '/employee/my-caregivers/hired';
            },

            createJobInvite: function()  {
                // disable input
                this.$http.post('/api/v1/clients/job/invite', this.formData ).then(function(response) {
                    this.storedJobInvite(response);
                }, function(response) {
                    this.handleError(response);
                }).bind(this);
                // enable input again...
            },

            storedJobInvite: function(response) {
                if (response.data.success) {
                    window.location = response.data.redirect;
                }
            },

            jobSelectedText: function(selectedText) {
                this.job.text = selectedText;
            },

            durationSelectedText: function(selectedText) {
                this.duration.text = selectedText;
            },

            workloadSelectedText: function(selectedText) {
                this.workload.text = selectedText;
            },

            handleError: function(response) {
                $('html, body').animate({
                    scrollTop: $("#invite-job").offset().top
                }, 400);
                if (response.status === 422) {
                    this.job.errors.message = typeof response.data.job != 'undefined' ? response.data.job[0] : '';
                    this.duration.errors.message = typeof response.data.duration != 'undefined' ? response.data.duration[0] : '';
                    this.workload.errors.message = typeof response.data.workload != 'undefined' ? response.data.workload[0] : '';
                    this.message.errors.message = typeof response.data.message != 'undefined' ? response.data.message[0] : '';
                    this.caregiver.errors.message = typeof response.data.caregiver != 'undefined' ? response.data.caregiver[0] : '';
                }
            }
        },
        computed: {
            formData: function() {
                return {
                    job: this.job.selected,
                    duration: this.duration.selected,
                    workload: this.workload.selected,
                    message: this.message.text,
                    caregiver: this.caregiver.text
                }
            }
        }
    });
}
