import Vue from 'vue';
import CustomSelect from './components/CustomSelect.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

/*
 For password and security Settings
 */
if (document.getElementById('accept-invite-job')) {

    var vm = new Vue({
        el: '#accept-invite-job',
        data: {
            decline_reason: {
                selected: 0,
                errors: {
                    message: ''
                }
            },
            decline_text: {
                text: ''
            },
            accept_message: {
                text: '',
                errors: {
                    message: ''
                }
            },
            accept_rate: {
                float: "0.00",
                errors: {
                    message: ''
                }
            },
            questions: {
                items: [],
                errors: false
            },
            current_rate: "0.00",
            user_rate: "",
            job_invite: 0,
            block_client_invitation: true,
            job_id: 0
        },
        components: {
            'custom-select': CustomSelect
        },
        methods: {
            acceptJobInvite: function()  {
                // disable input
                this.$http.put('/api/v1/employee/job/accept', this.formDataAccept ).then(function(response) {
                    this.storedJobInvite(response);
                }, function(response) {
                    this.handleErrorAccept(response);
                }).bind(this);
                // enable input again...
            },
            declineJobInvite: function() {
                this.$http.put('/api/v1/employee/job/decline', this.formDataDecline ).then(function(response) {
                    this.storedJobInvite(response);
                }, function(response) {
                    this.handleErrorDecline(response);
                }).bind(this);
            },
            storedJobInvite: function(response) {
                if (response.data.success) {
                    window.location.reload();
                }
            },
            handleErrorAccept: function(response) {
                if (response.status === 422) {
                    this.accept_message.errors.message = typeof response.data.message != 'undefined' ? response.data.message[0] : '';
                    this.accept_rate.errors.message = typeof response.data.rate != 'undefined' ? response.data.rate[0] : '';
                    this.questions.errors = typeof response.data.questions != 'undefined' ? response.data.questions[0] : '';
                }
            },
            handleErrorDecline: function(response) {
                if (response.status === 422) {
                    this.decline_reason.errors.message = typeof response.data.reason != 'undefined' ? response.data.reason[0] : '';
                }
            },
            setRate: function(rate) {
                this.accept_rate.errors.message = '';

                switch(rate) {
                    case 'accept':
                        if (this.accept_rate.float.length > 6) {
                            this.accept_rate.float = this.accept_rate.float.substr(0, 6);
                        }

                        if (Number(this.accept_rate.float)) {
                            this.accept_rate.errors.message = "";
                            this.current_rate = (this.accept_rate.float*0.9).toFixed(2);
                        } else {
                            this.accept_rate.errors.message = "The accept rate is invalid.";
                            this.current_rate = "0.00";
                        }
                        break;
                    case 'current':
                        if (this.current_rate.length > 6) {
                            this.current_rate = this.current_rate.substr(0, 6);
                        }

                        if (Number(this.current_rate)) {
                            this.accept_rate.float = (this.current_rate*1.11111).toFixed(2);
                        } else {
                            this.accept_rate.float = "0.00";
                        }
                        break;
                }
            }
        },
        computed: {
            formDataAccept: function() {
                return {
                    message: this.accept_message.text,
                    rate: this.accept_rate.float,
                    job_invite: this.job_invite,
                    questions: this.questions.items
                }
            },
            formDataDecline: function() {
                return {
                    reason: this.decline_reason.selected,
                    message: this.decline_text.text,
                    block_client_invite: this.block_client_invitation,
                    job_invite: this.job_invite
                }
            }
        },
        ready: function () {
            this.$http.get('/api/v1/employee/get-info').then(function(response) {
                /** Employee Info **/
                var min_rate = response.data.data.employee_info.min_rate;
                var max_rate = response.data.data.employee_info.max_rate;
                
                this.accept_rate.float = max_rate.toFixed(2);
                this.setRate('accept');
                this.user_rate = (min_rate == undefined) ? max_rate : min_rate+'-'+max_rate;
            }).bind(this);

            this.$http.get('/api/v1/employee/job/'+this.job_id+'/questions').then(function(response) {
                var questions = response.data.data;
                for (var q in questions) {
                    this.questions.items.push({
                        question: questions[q],
                        answer: ''
                    });
                }
                console.log(response.data);
            }).bind(this);

            var _this = this;
            jQuery('[name="accept_rate"]').blur(function(){
                _this.accept_rate.float = parseFloat(_this.accept_rate.float);

                if (_this.accept_rate.float > 1000) {
                    _this.accept_rate.float = 999;
                }

                if (Number(_this.accept_rate.float)) {
                    _this.accept_rate.float = _this.accept_rate.float.toFixed(2);
                    if (_this.current_rate > 0) {
                        _this.current_rate = (_this.accept_rate.float*0.9).toFixed(2);
                        _this.accept_rate.errors.message = "";
                    } else {
                        _this.current_rate = "0.00";
                        _this.accept_rate.errors.message = "The accept rate is invalid.";
                    }

                } else {
                    _this.current_rate = "0.00";
                    _this.accept_rate.float = "0.00";
                    _this.accept_rate.errors.message = "The accept rate is invalid.";
                }
            });

            jQuery('[name="current_rate"]').blur(function(){
                _this.current_rate = parseFloat(_this.current_rate);

                if (_this.current_rate > 1000) {
                    _this.current_rate = 999;
                }

                if (Number(_this.current_rate)) {
                    _this.current_rate = _this.current_rate.toFixed(2);
                    if (_this.current_rate > 0) {
                        _this.accept_rate.float = (_this.current_rate*1.11111).toFixed(2);
                    } else {
                        _this.accept_rate.float = "0.00";
                    }
                } else {
                    _this.accept_rate.float = "0.00";
                    _this.current_rate = "0.00";
                }
            });
        }
    });
}
