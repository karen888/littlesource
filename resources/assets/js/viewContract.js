import Vue from 'vue';
import StarRating from './components/StarRating.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('view-contract')) {
    var vm = new Vue({
        el: '#view-contract',
        components: {
            'star-rating' : StarRating
        }
    });
}