import Vue from 'vue';
import customselect from './customselect';
import _ from 'lodash';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('my-caregivers')) {
    var vm = new Vue({
        el: '#my-caregivers',
        data: {
            type: null,
            users: [],
            computed_data: {},
            search: ''
        },
        methods: {
            setEdit: function (key) {
                this.users[key].edit = true;
                if(this.users[key].notes_origin == 'Your private notes goes here'){
                    this.users[key].notes_origin = '';
                }
                this.users[key].users = this.users[key].notes_origin;
            },
            saveNotes: function (key) {
                this.$http.put('/api/v1/favorites/notes', {
                    'id': this.users[key].id,
                    'notes': this.users[key].users
                }).then(function(response) {
                    this.users[key].edit = false;
                    this.users[key].notes_origin = this.users[key].users;
                }, function(response) {
                    this.handleError(response, key);
                }).bind(this);
            },
            cancelNotes: function (key) {
                this.users[key].edit = false;
                if(this.users[key].notes_origin == ''){
                  this.users[key].notes_origin = 'Your private notes goes here';
                }
                this.users[key].users = this.users[key].notes_origin;
            },
            handleError: function (response, key) {
                if(response.status === 422) {
                    this.users[key].error = typeof response.data.users != 'undefined' ? response.data.users[0] : '';
                }
            },
            getData: function () {
                switch (this.type) {
                    case 'favorites':
                        this.$http.get('/api/v1/favorites/list', this.formData).then(function(response) {
                            this.users = response.data.data;
                            setTimeout(function(){
                                customselect.createActionGroups();
                            },100);
                        }).bind(this);
                        break;
                    case 'massaged':
                        this.$http.get('/api/v1/client/caregivers/messaged', this.formData).then(function(response) {
                            this.users = response.data.data.messaged;
                            setTimeout(function(){
                                customselect.createActionGroups();
                            },100);
                        }).bind(this);
                        break;
                    case 'hired':
                        this.$http.get('/api/v1/client/caregivers/hired', this.formData).then(function(response) {
                            this.users = response.data.data.hired;
                            
                        }).bind(this);
                        break;
                }
            }
        },
        computed: {
            emptyUsers: function () {
                return _.isEmpty(this.users)
            },
            formData: function () {
                return {
                    'search': this.search
                }
            }
        },
        watch: {
            'search': function (val) {
                if (val.length >= 3 || val.length == 0) {
                    this.getData();
                }
            }
        },
        ready: function() {
            this.getData();
            setTimeout(function(){
                customselect.createActionGroups();
            },2100);
        }
    });
}
