import Vue from 'vue';
import ProfileBox from './components/AuthMenu/ProfileBox.vue'

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;


/*
    Auth Menu
 */
if (document.getElementById('auth-menu')) {
    var profile = new Vue({
        el: '#auth-menu',
        components: {
            'profile-box': ProfileBox
        }
    });
}