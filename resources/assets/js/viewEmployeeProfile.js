import Vue from 'vue';
import CustomSelect from './components/CustomSelect.vue';
import StarRating from './components/StarRating.vue';
import _ from 'lodash';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

/*
 For password and security Settings
 */
if (document.getElementById('view-employee-profile')) {
    var vm = new Vue({
        el: '#view-employee-profile',
        data: {
            hidden_buttons: true,
            favorite: false,
            employee_id: '',
            experience: {
                selected: ''
            },
            name: '',
            photo: '/img/no_photo_100.jpg',
            route_hire: '',
            show_hire:false,
            contact_now: '',
            available: {
                value: false,
                selected: false
            },
            availability_days: [],
            expect_ready: '',
            display_name: true,
            title: '',
            overview: '',
            rate: '',
            qualifications: {
                willing_travel: '5',
                smoker: false,
                own_children: false,
                sick_children: false,
                comfortable_with_pets: false,
                have_transportation: false,
                qualification: {
                    selected: [],
                    items: []
                },
                care_type: [],
                child_age: []
            },
            skills: [],
            educations: [],
            languages: [],
            selects: null,
            job_history: [],
            view_feedback: [],
            load_more: true,
            employee_rating: false
        },
        components: {
            'custom-select': CustomSelect,
            'star-rating' : StarRating
        },
        methods: {
            getUserData: function () {
                this.name = '';
                this.photo = '/img/no_photo_100.jpg';
                this.route_hire = '';
                this.contact_now = '';
                this.available = {
                    value: false,
                    selected: false
                };
                this.expect_ready = '';
                this.availability_days = [];
                this.display_name = true;
                this.title = '';
                this.overview = '';
                this.rate = '';
                this.qualifications.have_transportation = false;
                this.qualifications.willing_travel = '5';
                this.qualifications.smoker = false;
                this.qualifications.own_children = false;
                this.qualifications.sick_children = false;
                this.qualifications.comfortable_with_pets = false;
                this.skills = [];
                this.educations = [];
                this.languages = [];

                this.$http.get('/api/v1/employee/get-info/' + this.employee_id).then(function(response) {
                    /** User Data **/
                    this.name = response.data.data.user_data.name;
                    this.photo = response.data.data.user_data.photo;
                    this.route_hire = response.data.data.user_data.route_hire;
                    this.contact_now = response.data.data.user_data.contact_now;
                    this.hidden_buttons = response.data.data.user_data.hidden_buttons;
                    this.favorite = response.data.data.user_data.favorite;
                    this.job_history = response.data.data.job_history;
                    this.employee_rating = response.data.data.employee_rating;
                    this.showFeedbacks();
                    //checkhire
                    if(
                        response.data.data.verifed_id == 1
                        && response.data.data.address_status == 1
                        && response.data.data.wwcc_status == 1
                        && response.data.data.qualifications_verifed == 1
                    ){
                        this.show_hire = true;
                    }

                    /** Employee Info **/
                    this.available.value = Boolean(parseInt(response.data.data.employee_info.available));
                    this.available.selected = response.data.data.user_data.employment_type[0];
                    this.availability_days = response.data.data.user_data.availability_days;
                    this.expect_ready = response.data.data.employee_info.expect_ready;
                    this.display_name = Boolean(parseInt(response.data.data.employee_info.short_name));
                    this.title = response.data.data.employee_info.title;
                    this.overview = response.data.data.employee_info.overview;
                    this.rate = response.data.data.employee_info.rate;
                    this.experience.selected = response.data.data.user_data.experience;
                    this.qualifications.have_transportation = response.data.data.user_data.have_transportation;
                    this.qualifications.willing_travel = response.data.data.user_data.willing_travel;
                    this.qualifications.smoker = response.data.data.user_data.smoker;
                    this.qualifications.own_children = response.data.data.user_data.own_children;
                    this.qualifications.sick_children = response.data.data.user_data.sick_children;
                    this.qualifications.comfortable_with_pets = response.data.data.user_data.comfortable_with_pets;

                    if (!_.isEmpty(response.data.data.user_data.qualification)) {
                        this.qualifications.qualification.selected = response.data.data.user_data.qualification;
                    }

                    if (!_.isEmpty(response.data.data.user_data.care_type)) {
                        this.qualifications.care_type = response.data.data.user_data.care_type;
                    }
                    if (!_.isEmpty(response.data.data.user_data.child_age_range)) {
                        this.qualifications.child_age = response.data.data.user_data.child_age_range;
                    }

                    /** Skills **/
                    if (!_.isEmpty(response.data.data.skills)) {
                        for (var data in response.data.data.skills) {
                            this.skills.push(response.data.data.skills[data].skill_name);
                        }
                    }

                    /** Education **/
                    if (!_.isEmpty(response.data.data.educations)) {
                        for (var i in response.data.data.educations) {
                            var data = {
                                school: {
                                    text: response.data.data.educations[i].school,
                                    errors: {
                                        message: ''
                                    }
                                },
                                degree: {
                                    text: response.data.data.educations[i].degree,
                                    errors: {
                                        message: ''
                                    }
                                },
                                area_study: {
                                    text: response.data.data.educations[i].area_study,
                                    errors: {
                                        message: ''
                                    }
                                },
                                dates_attended_from: {
                                    selected: response.data.data.educations[i].dates_attended_from,
                                    errors: {
                                        message: ''
                                    }
                                },
                                dates_attended_to: {
                                    selected: response.data.data.educations[i].dates_attended_to,
                                    errors: {
                                        message: ''
                                    }
                                }
                            };

                            this.educations.push(data);
                        }
                    }

                    /** Languages **/
                    if (!_.isEmpty(response.data.data.languages)) {
                        for (var i in response.data.data.languages) {
                            var data = {
                                name: response.data.data.languages[i].name,
                                errors: {
                                    message: '',
                                }
                            };
                            this.languages.push(data);
                        }
                    }

                }).bind(this);
            },
            toogleFavorite: function() {
                if (this.favorite) {
                    var url = '/api/v1/favorites/delete/' + this.employee_id;
                } else {
                    var url = '/api/v1/favorites/add/' + this.employee_id;
                }

                this.$http.post(url).then(function() {
                    this.favorite = this.favorite ? false : true;
                }).bind(this);
            },
            openFeedback: function (key) {
                if (this.job_history[key].feedback.rating) {
                    this.view_feedback = this.job_history[key];

                    $('#view-feedback-modal').modal('show');
                }
            },
            showFeedbacks: function () {
                var show = 5;
                var hide = true;
                for (var i in this.job_history) {
                    if (show && !this.job_history[i].show) {
                        this.job_history[i].show = true;
                        show--;
                    }

                    if (!this.job_history[i].show) {
                        hide = false;
                    }
                }

                if (hide) {
                    this.load_more = false;
                }
            }
        },
        computed: {
            favoriteText: function () {
                return this.favorite ? 'Remove from' : 'Add to';
            },
            getAvailabilityDays: function () {
                if (_.isEmpty(this.availability_days)) {
                    return false;
                }

                var data = [];

                for (var available in this.selects.availability.items) {
                    for (var days in this.availability_days) {
                        if (this.selects.availability.items[available].value == this.availability_days[days]) {
                            data.push(this.selects.availability.items[available].text);
                        }
                    }
                }

                return data.join(', ');
            }
        },
        ready: function () {
            this.$http.get('/api/v1/client/caregivers/find')
            .then(function(response) {
                for (var d in response.data) {
                    for (var i in response.data[d].items) {
                        response.data[d].items[i].is_selected = false;
                    }
                }
                this.selects = response.data;
            })
            .catch(function(response) { console.log(['Error!', response]); })
            .finally(function(response) {})
            .bind(this);

            this.$http.get('/api/v1/common/qualifications').then(function(response) {
                this.qualifications.qualification.items = response.data.data.qualifications;
            }, function(response) {
                console.log(['Error!', response]);
            }).bind(this);

            var _this = this;
            jQuery('.caregiver.employee_list').eq(0).addClass('active');
            jQuery('.caregiver.employee_list').on('click', function() {
                jQuery('.caregiver.employee_list').removeClass('active');
                jQuery(this).addClass('active');
                _this.employee_id = jQuery(this).data('id');

            });
        },
        watch: {
            employee_id: function() {
                this.getUserData();
            }
        }
    });
}
