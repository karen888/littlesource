import Vue from 'vue';
import Notifications from './components/Notifications.vue';
import Threads from './components/Messages/Threads.vue';
import Corespondents from './components/Messages/Corespondents.vue';
import Conversation from './components/Messages/Conversation.vue';
import Composer from './components/Messages/Composer.vue';
import moment from 'moment';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;


if (document.getElementById('messages')) {
    var vm = new Vue({
        el: '#messages',
        data: {
            currentView: 'threads',
            viewOptions: 'inbox',
            showSidebar: false
        },

        components: {
            'notifications': Notifications,
            'threads': Threads,
            'corespondents': Corespondents,
            'conversation': Conversation,
            'composer': Composer
        },

        ready: function() {
            var that = this;
            var hm = new Hammer(document.getElementById('messages'));
            hm.on('swiperight', function(ev) { that.showSidebar = true; });
            hm.on('swipeleft', function(ev) { that.showSidebar = false; });
        },

        methods: {
            toggleSidebar: function() {
                this.showSidebar = !this.showSidebar;
            },

            scrollTo: function() {
                var scrollTo = document.getElementById('messages');
                scrollTo.scrollIntoView();
            },

            setPath: function(path, archived = false) {
                this.viewOptions = archived ? 'archive' : 'inbox';

                this.$set('currentView', null);
                this.$nextTick(function() {
                    this.$set('currentView', path);
                    this.currentView = path;
                });
            }
        },

        events: {
            'load-thread': function($thread_id) {
                this.showSidebar = false;
                this.$broadcast('load-thread', $thread_id);
            },
            'send-message': function($params) { //receives the event and forwards it to childers
                this.$broadcast('send-message', $params);
            }
        }
    });

    Vue.filter('strLimit', function (str, num) {
        if(str.length > num) str = str.substring(0,num) + ' ...';
        return str;
    });

    Vue.filter('diffForHumans', function(date) {
        return moment(date).fromNow();
    });

    Vue.filter('dateFull', function(date) {
        return moment(date).format("dddd, MMMM Do YYYY, h:mm:ss a");
    });

    Vue.directive('on').keyCodes.ctrl = 17;
}
