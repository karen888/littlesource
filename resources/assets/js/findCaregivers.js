import Vue from 'vue';
import noUiSlider from './nouislider';
import _ from 'lodash';
import CustomSelect from './components/CustomSelectExtended.vue';
import custom_select from './customselect';
Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

function isInArray(value, array) {
    return array.indexOf(value) > -1;
}

if (document.getElementById('find-caregivers')) {
    var vm = new Vue({
        el: '#find-caregivers',
        data: {
            selects: null,
            showAdvanced: false,
            urls: {
                get: '/api/v1/client/caregivers/find',
                post: '/api/v1/client/caregivers/find'
            },
            prev_page: false,
            next_page: false,
            current_page: 1,
            pages: [],
            users: [],
            markers: [],
            postcode: {
                value: '',
                error: false
            },
            loc: {
                lat: null,
                lng: null
            },
            autocomplete: null,
        },


        components: {
            'care-type': CustomSelect,
            'child-age': CustomSelect,
            'availability': CustomSelect,
            'employment-type': CustomSelect,
            'qualification': CustomSelect,
            'years-experience': CustomSelect,
            'care-sick': CustomSelect,
            'pets': CustomSelect,
            'age-range': CustomSelect,
            'drivers-licence': CustomSelect,
            'smoker': CustomSelect,
            'gender': CustomSelect,
            'own-children': CustomSelect
        },

        ready: function() {
            const _this = this;

            window.googleCheckingTmp = window.setInterval(function(){
                if(typeof google != "undefined") {
                    window.clearInterval(window.googleCheckingTmp);
                    _this.initAutocomplete();
                }
            },200);

            this.$http.get(this.urls.get)
                .then(function(response) {
                    for (var d in response.data) {
                        if (d != 'location_range') {
                            for (var i in response.data[d].items) {
                                response.data[d].items[i].is_selected = true;
                            }
                        }
                    }
                    this.selects = response.data;


                    this.createSlider('slider', this.selects.location_range.items);
                    this.postSearch();
                    setTimeout(function(){
                        jQuery('.years-experience .no_preference').trigger('click');
                        jQuery('.care-sick .no_preference').trigger('click');
                        jQuery('.pets .no_preference').trigger('click');
                        jQuery('.age-range .no_preference').trigger('click');
                        jQuery('.gender .no_preference').trigger('click');
                        jQuery('.own-children .no_preference').trigger('click');
                        jQuery('.drivers-licence .no_preference').trigger('click');
                        jQuery('.smoker .no_preference').trigger('click');
                    },100);
                })
                .catch(function(response) { console.log(['Error!', response]); })
                .finally(function(response) {

                })
                .bind(this);
        },

        methods: {
            initAutocomplete: function() {
                console.log("Initialize autocomplete...");
                // Create the autocomplete object, restricting the search to geographical
                // location types.
                this.autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                    {types: ['geocode']});

                // When the user selects an address from the dropdown, populate the address
                // fields in the form.
                this.autocomplete.addListener('place_changed', this.fillInAddress);
                $('#autocomplete').on('keyup', function(){
                    if($(this).val().trim().length == 0) {
                        window.radiusCircle.setCenter(userLocation);

                    }
                });

                console.log('Done');
            },
            fillInAddress: function() {
                // Get the place details from the autocomplete object.
                var place = this.autocomplete.getPlace();

                if(place && place.geometry && place.geometry.location) {
                    this.loc.lat = place.geometry.location.lat();
                    this.loc.lng = place.geometry.location.lng();

                    window.radiusCircle.setCenter(this.loc);
                    window.radiusCircle.setRadius(Math.round(this.locationRange) * 1000);
                    map.setCenter(this.loc);
                } else {
                    alert('No coords available for place');
                }
                console.log(place);
            },

            getSlider: function() {
                return document.getElementById('slider').noUiSlider.get();
            },
            postSearch: function(page, click) {
                if(this.selects) {
                    //check if selects isValid: this is going to be weird to do
                    var frm = this.validateSelects();


                    frm['location_range'] = Math.round(this.locationRange);


                    frm['postcode'] = Math.round(this.postcode.value);
                    if (Number(page)) {
                        frm['page'] = page;
                    }

                    console.log(frm['postcode']);

                    if(!isNaN(frm['postcode'])) {
                        console.log('postcode is not null, clearing coords');
                        this.loc.lat = null;
                        this.loc.lng = null;

                        if(frm['postcode'] != 0 ) {
                            window.radiusCircle.setRadius(0.01);
                        }
                    }

                    if(this.loc.lat && this.loc.lng) {
                        console.log('setting coords to search');
                        frm['lat'] = this.loc.lat;
                        frm['lng'] = this.loc.lng;
                        frm['postcode'] = 0;
                    }

                    this.$http.post(this.urls.post, frm)
                        .then(function(response) {
                            var pagin = [];
                            jQuery(response.data.data.render).find('li').each(function(){
                                pagin.push(jQuery(this).text());
                            });
                            this.pages = pagin;

                            this.current_page = response.data.data.current_page;
                            this.prev_page = response.data.data.prev_page;
                            this.next_page = response.data.data.next_page;

                            this.users = response.data.data.users;

                            setTimeout(function(){
                                custom_select.createActionGroups();
                            }, 100);

                            /*switch (Math.round(frm['location_range'])) {
                                case 5:
                                    map.setZoom(12);
                                    break;
                                case 10:
                                case 15:
                                    map.setZoom(11);
                                    break;
                                case 20:
                                case 30:
                                    map.setZoom(10);
                                    break;
                                case 50:
                                    map.setZoom(9);
                                    break;
                            }*/

                            var infoWindow = new google.maps.InfoWindow();
                            var users = this.users;
                            var postcode = response.data.data.postcode;
                            var html = [];



                            for (var i in this.markers) {
                                this.markers[i].setMap(null);
                            }

                            var bounds = new google.maps.LatLngBounds();


                            for (var code in postcode) {
                                html[code] = [];

                                var placecount = 0;

                                for (var c in postcode[code]) {
                                    placecount++;
                                    console.log(users[postcode[code][c]]);

                                    html[code].push(
                                        '<div style="float:left;"><img style="width:50px;height:50px;border-radius:50px;margin-right:10px;" src="' + users[postcode[code][c]].photo + '"></div> <div style="float:left;"><b style="white-space: nowrap;">' +
                                        users[postcode[code][c]].name + '</b><br>' +
                                        ' ($' + users[postcode[code][c]].rate + '/hour)<br>' +
                                        ' <a href="' + users[postcode[code][c]].view + '" target="_blank">View Profile</a></div><br/>'
                                    );
                                }

                                console.log("Lat: " + users[postcode[code][c]].lat);
                                var latlng = new google.maps.LatLng(Number(users[postcode[code][c]].lat), Number(users[postcode[code][c]].lng));

                                var image;

                                if(placecount == 1) {
                                    image = {
                                        url: '/usermarker/' + postcode[code][c],
                                        scaledSize: new google.maps.Size(38, 38)
                                    };
                                } else {
                                    image = {
                                        url: '/photo_core/int/' + placecount,
                                        scaledSize: new google.maps.Size(38, 38)
                                    };
                                }


                                var marker = new google.maps.Marker({
                                    position: latlng,
                                    icon: image,
                                    map: map,
                                });

                                bounds.extend(latlng); // Iceland

                                this.markers[code] = marker;
                                var markers = this.markers;


                                google.maps.event.addListener(markers[code], 'click', (function(markers, code, html) {
                                    return function() {
                                        infoWindow.setContent(html[code].join('<br><hr style="margin:10px 0;">'));
                                        infoWindow.open(map, markers[code]);
                                    }
                                })(markers, code, html));

                            }

                            map.fitBounds(bounds);

                            // if (click) {
                                // $('html, body').animate({
                                //     scrollTop: ($("#map").offset().top - 450)
                                // }, 400);
                            // } 
                        })
                        .catch(function(response) { console.log(['Error sending search params', response]); })
                        .finally(function(response) {})
                        .bind(this);

                }
            },
            validateSelects: function() {
                var that = this;
                var advKeys = [
                  "qualification",
                  "gender",
                  "years_experience",
                  "own_children",
                  "drivers_licence",
                  "care_sick",
                  "pets",
                  "smoker",
                ];
                var frm = _(this.selects).mapValues(function(s) {


                    if(Array.isArray(s.selected)) {
                        if (s.items.length !== s.selected.length && s.selected.length != 0) {
                            return s.selected;
                        } else {
                            return undefined;
                        }
                    }
                    return s.selected;
                }).value();
                _.forEach(frm, function(value, key) {
                    if(value == undefined) delete frm[key];


                    if(!that.showAdvanced && isInArray(key, advKeys)) {
                        delete frm[key];
                    }
                });

                console.log(frm);

                return frm;
            },
            setShowAdvanced: function (set) {
                this.showAdvanced = set;
                $('html, body').animate({
                    scrollTop: $(".FindCaregivers__toggle-filters").offset().top
                }, 400);
            },
            setUsersPage: function(page) {
                if (Number(page)) {
                    this.postSearch(page);
                }else if(page == '«') {
                    if (this.prev_page) {
                        this.postSearch((this.current_page-1));
                    }
                }else if(page == '»') {
                    if (this.next_page) {
                        this.postSearch((this.current_page+1));
                    }
                }
            },
            createSlider: function($id, $values) {
                var that = this;
                var rangeSlider = document.getElementById($id);
                var values = $values; //JSON.parse(rangeSlider.dataset.locationRanges)
                var items = values.length - 1;
                var start = [];
                var range = values.reduce(function (r, i, idx) {
                    var key = Math.round(100 / items * idx);
                    key = idx == 0 ? 'min' : (idx == items ? 'max' : key + '%');
                    r[key] = Number(i.data);
                    if (i.is_selected) start.push(i.data);
                    return r;
                }, {});

                if (rangeSlider !== null && values !== null) {
                    noUiSlider.create(rangeSlider, {
                        start: start,
                        snap: true,
                        range: range,
                        pips: {
                            mode: 'range',
                            density: 3,
                            format: {
                                to: function (value) {
                                    return value + 'km';
                                },
                                from: function (value) {
                                    return value.replace('km', '');
                                }
                            }
                        }
                    }).on('set', function () {
                        window.radiusCircle.setRadius(Math.round(that.locationRange) * 1000);
                    });

                }
            }
        },
        computed: {
            locationRange: {
                cache: false,
                get: function() {
                    return document.getElementById('slider').noUiSlider.get();
                }
            },
            usersIsEmpty: function() {
                return _.isEmpty(this.users);
            }
        }

    });




}
