import Vue from 'vue';
import $ from 'jquery';
import CustomSelect from './components/CustomSelect.vue';
import FileUploader from './components/FileUploader.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('qualification-verify')) {
    var vm = new Vue({
        el: '#qualification-verify',
        data: {
           	form: {
                school:{
                    errors: '',
                    text: ''
                },
                date_start:{
                    errors: '',
                    selected: ''
                },
                date_end:{
                    errors: '',
                    selected: ''
                },
                degree:{
                    errors: '',
                    text: ''
                },
                area:{
                    errors: '',
                    text: ''
                },
                desc:{
                    errors: '',
                    text: ''
                },
                notes: {
                    errors: '',
                    text: ''
                },
                scan:{
                    errors: '',
                }
            },         
            dateStart: [],
            scans: "",
            scan_type: 'file',
            dateEnd: [],
            loading: {
           	    scans: false,
            }
        },
        methods: {
            onFileUploaded: function(prop_name, remote_file) {
                this[prop_name] = remote_file;
            },
            detectScanType() {
                // Detect whether file is image
                if( this.scans.indexOf("JFIF") !== -1 ||
                    this.scans.indexOf("base64") !== -1 ||
                    this.scans.indexOf("PNG") !== -1 ||
                    this.scans.indexOf("GIF") !== -1 ||
                    this.scans.indexOf("BMP") !== -1 ||
                    this.scans.indexOf("jpeg") !== -1 ||
                    this.scans.indexOf("JPEG") !== -1 ||
                    this.scans.indexOf("gif") !== -1 ||
                    this.scans.indexOf("bmp") !== -1 ||
                    this.scans.indexOf("jpg") !== -1 ||
                    this.scans.indexOf("JPG") !== -1 ||
                    this.scans.indexOf("png") !== -1
                ){
                    this.scan_type = 'image';
                }else{
                    this.scan_type = 'file';
                }
            },
            onFileChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;
                var that = this;

                var data = new FormData();
                $.each(files, function(key, value)
                {
                    data.append(key, value);
                });

                this.loading.scans = true;
                this.form.scan.errors = '';
                $.ajax({
                    xhr: function()
                    {
                        var xhr = new window.XMLHttpRequest();
                        // прогресс загрузки на сервер
                        xhr.upload.addEventListener("progress", function(evt){
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                // делать что-то...
                                console.log("upload:"+percentComplete);
                                var prg = Math.round(percentComplete * 100);

                                $('#'+e.target.name+'progress').attr('aria-valuenow', prg)
                                    .css({width:prg+'%'})
                                    .html(prg+'%');
                            }
                        }, false);
                        return xhr;
                    },
                    url: '/api/v1/upload/image',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function(data, textStatus, jqXHR)
                    {
                        if(typeof data.error === 'undefined')
                        {
                            // Success so call function to process the form

                            that.loading.scans= false;
                            that.scans = data.image_path;

                            that.form.scans.errors = '';
                            that.detectScanType();
                            console.log(data);

                        }
                        else
                        {
                            // Handle errors here
                            console.log('ERRORS: ' + data.error);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        // Handle errors here
                        console.log('ERRORS: ' + textStatus);
                        that.loading.scans = false;
                    }
                });
                //this.createImage(files[0]);
            },
            createImage(file) {
                var image = new Image();
                var reader = new FileReader();
                var vm = this;

                reader.onload = (e) => {
                    vm.scans = e.target.result;
                    this.form.scan.errors = '';
                };
                reader.readAsDataURL(file);
            },
            checkForm: function(fun) {
                var error = false;
                var textError = 'error';
                this.form.school.errors = '';
                this.form.date_start.errors = '';
                this.form.date_end.errors = '';

                if (this.form.school.text == '') {
                    this.form.school.errors =  'This is a required field';
                    error = true;
                }
                if(this.form.degree.text == '') {
                    this.form.degree.errors =  'This is a required field';
                    error =true;
                }
                if (this.form.date_start.selected == '') {
                    this.form.date_start.errors =  'This is a required field';
                    error = true;
                }
                if (this.form.date_end.selected == '') {
                    this.form.date_end.errors =  'This is a required field';
                    error = true;
                }
                if(this.form.date_end.selected<this.form.date_start.selected){
                    this.form.date_end.errors = 'Invalid entry';
                    error = true;
                }
                if(this.scans == '') {
                    this.form.scan.errors =  'This is a required field';;
                    error = true;
                }

                // if (this.form.degree.text == ''){
                //     this.form.degree.errors = textError;
                //     return;
                // }
                // if (this.form.area_study.text == ''){
                //     this.form.area_study.errors = textError;
                //     return;
                // }
                // if (this.form.description.text == '') {
                //     this.form.description.errors = textError;
                //     return;
                // }
                if(error === false){
                    this.saveForm(fun);
                }
            }
            ,
            getURLParam (oTarget, sVar) { return decodeURI(oTarget.replace(new RegExp("^(?:.*[&\\?]" + encodeURI(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1")); },
            saveForm: function (fun){
                $("#saveBtn").attr('disabled', true);
                jQuery("body").LoadingOverlay("show");


                // if(this.loading.scans) {
                //     var _this = this;
                //
                //     var postFormInterval = window.setInterval(()=>{
                //         if(!_this.loading.scans ) {
                //             window.clearInterval(postFormInterval);
                //             _this.saveForm();
                //         }
                //     },200);
                //
                //     return;
                // }

                var formData = {
                    id:  this.getURLParam (window.location.search, "id"),
                    school: this.form.school.text,
                    dates_attended_from: this.form.date_start.selected,
                    dates_attended_to: this.form.date_end.selected,
                    degree: this.form.degree.text,
                    scan: this.scans,
                    area_study: this.form.area.text,
                    description: this.form.desc.text,
                    notes: this.form.notes.text
                };
                if(formData.scan == ''){
                    this.form.scan.errors = 'Please attach a copy';
                }
                this.$http.put('/api/v1/employee/profile/education-verify', formData)
                    .then( function(response) {

                            // this.$dispatch('notification', {type: response.data.notification.type, 'message': response.data.notification.message, 'close': false});
                            if(fun == 'savemore'){
                                this.clearForms();
                                $("#saveBtn").attr('disabled', false);
                                jQuery("body").LoadingOverlay("hide");
                                // window.location.href = "/user/settings/get-verified";r
                                return;
                            }else{
                                window.location.href = "/user/settings/get-verified";
                            }
                        }, function(response) {
                        $("#saveBtn").attr('disabled', false);
                        jQuery("body").LoadingOverlay("hide");
                            for (var key in response.data) {
                                this.form[key].errors = 'This is a required field'
                            }
                            // on unknown error
                            if (response.status === 500) {
                                this.cancel();
                                // this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                            }
                        }
                    );
            },

            clearForms(){
                this.form.school.text = '';
                this.form.degree.text = '';
                this.form.area.text = '';
                this.scans = '';
                this.form.desc.text = '';
                this.form.date_start.selected = '';
                this.form.date_end.selected = '';
                this.form.notes.text= '';
            },
            setUserInfo: function (data) {
                var id = this.getURLParam (window.location.search, "id");

                var _this = this;
                data.data.educations.forEach(function(edu){
                    console.log(id + ' === ' + edu.id);

                    if(id  == edu.id){
                           // var edu = data.data.educations[0];
                        _this.form.school.text = edu.school;
                        _this.form.degree.text = edu.degree;
                        _this.form.area.text = edu.area_study;
                        _this.scans = edu.scan;
                        _this.form.date_start.selected = edu.dates_attended_from;
                        _this.form.date_end.selected = edu.dates_attended_to;

                        console.log(_this.form.date_start.selected);
                        console.log(_this.form.date_end.selected);

                        _this.form.desc.text = edu.description;
                        _this.form.notes.text = edu.notes;
                    }
                });

                this.detectScanType();
            },
            setDates: function(year){
                var dateArray  = [];
                for(var i = 1960; i<=year; i++){
                    dateArray.push({ value: i, text: i});
                }
                this.dateStart = dateArray;
                this.dateEnd = dateArray;
            }        	

        },
        components: {
            'custom-select': CustomSelect,
            'file-uploader': FileUploader
        },        
        computed: {
            
        },
        ready: function(){

        },
        created: function () {
            var date = new Date();
            this.setDates( date.getFullYear() );


            this.$http.get('/api/v1/employee/get-info').then(function(response) {
                var that = this;

                setTimeout(function(){
                    that.setUserInfo(response.data);
                }, 500);

            }, function() {
                this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);
        }        


    });
}