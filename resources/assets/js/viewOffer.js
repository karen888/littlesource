import Vue from 'vue';
import CustomSelect from './components/CustomSelect.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('view_offer')) {
    var vm = new Vue({
        el: '#view_offer',
        data: {
            message_accept: {
                text: '',
                errors: {
                    message: ''
                }
            },
            reason: {
                selected: '',
                errors: {
                    message: ''
                },
                text: ''
            },
            message_decline: '',
            offer: ''
        },
        components: {
            'custom-select': CustomSelect
        },
        methods: {
            acceptOffer: function ()  {
                this.$http.put('/api/v1/employee/offer/accept', this.formDataAccept ).then(function(response) {
                    this.storedResponse(response);
                }, function(response) {
                    this.handleErrorAccept(response);
                }).bind(this);
            },
            declineOffer: function ()  {
                this.$http.put('/api/v1/employee/offer/decline', this.formDataDecline ).then(function(response) {
                    this.storedResponse(response);
                }, function(response) {
                    this.handleErrorDecline(response);
                }).bind(this);
            },
            handleErrorAccept: function (response) {
                if (response.status === 422) {
                    this.message_accept.errors.message = typeof response.data.message_accept != 'undefined' ? response.data.message_accept[0] : '';
                }
            },
            handleErrorDecline: function (response) {
                if (response.status === 422) {
                    this.reason.errors.message = typeof response.data.reason != 'undefined' ? response.data.reason[0] : '';
                }
            },
            storedResponse: function (response) {
                if (response.data.success) {
                    window.location.reload();
                }
            },
            reasonSelectedValue: function (value) {
                this.reason.selected = value;
            }
        },
        computed: {
            formDataAccept: function () {
                return {
                    offer: this.offer,
                    message_accept: this.message_accept.text
                }
            },
            formDataDecline: function () {
                return {
                    offer: this.offer,
                    reason: this.reason.selected,
                    message_decline: this.message_decline
                }
            }
        }
    });
}
