import {InfoStorage} from './userInfoStrorage';
import swal from 'sweetalert2';

export const userInfoMixin = {
    data() {
        return {
            userInfo: InfoStorage.$data.userInfo,
            criterias: InfoStorage.$data.criterias,
            errors: {},
            ownGender: InfoStorage.$data.ownGender,
            progress: null,
        }
    },
    props: ['steps'],
    computed:{
        isConfirm(){
            return typeof this.errors.check_agree !== 'undefined'
                && typeof this.errors.check_confirm !== 'undefined'
                && typeof this.errors.check_confirm_eligible !== 'undefined'
                && Object.keys(this.errors).length === 2;
        }
    },
    methods: {
        saveAndContinue() {
            $.LoadingOverlay('show');
            let data = this.getFormData();            
            if (data) {
                this.$http.post(this.saveRoute, data).then(() => {
                    this.$router.go(this.steps.next);
                    $.LoadingOverlay('hide');
                }, (response) => {
                    this.errors = response.data.data;
                    $.LoadingOverlay('hide');
                    let currentStep = parseInt(this.steps.current);
                    if(currentStep === 4 || (currentStep === 5 && !this.isConfirm)){
                        if (this.errors.valid_creds != undefined && Object.keys(this.errors).length == 1) {
                            var _self = this;
                            let cname = "wwcc errors count";
                            let _cname = cname + "=";
                            let ca = document.cookie.split(';');
                            let cvalue;
                            for(let i = 0; i < ca.length; i++) {
                                let c = ca[i];
                                while (c.charAt(0) == ' ') {
                                    c = c.substring(1);
                                }
                                if (c.indexOf(_cname) == 0) {
                                    cvalue = c.substring(_cname.length, c.length);
                                }
                            }

                            if (!cvalue) {
                                let d = new Date();
                                d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
                                let expires = "expires="+d.toUTCString();
                                document.cookie = cname + "=" + '1' + ";" + expires + ";path=/";

                                swal({

                                    title: "Oops. We couldn't verify your WWCC.",
                                    text: 'Please check the details and try again.',
                                    type: 'warning',
                                    animation: false,
                                    showCancelButton: true,
                                    showConfirmButton: false,
                                    confirmButtonText: 'Fix Later',
                                    cancelButtonText: 'Fix Now',
                                    reverseButtons: true,
                                    showLoaderOnConfirm: true,
                                    preConfirm: () => {
                                        this.clearError()
                                    },
                                });
                            } else if (cvalue == 1) {
                                let d = new Date();
                                d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
                                let expires = "expires="+d.toUTCString();
                                document.cookie = cname + "=" + '2' + ";" + expires + ";path=/";

                                swal({

                                    title: "Oops. We couldn't verify your WWCC.",
                                    text: 'Please check the details and try again.',
                                    type: 'warning',
                                    animation: false,
                                    showCancelButton: true,
                                    showConfirmButton: false,
                                    confirmButtonText: 'Fix Later',
                                    cancelButtonText: 'Fix Now',
                                    reverseButtons: true,
                                    showLoaderOnConfirm: true,
                                    preConfirm: () => {
                                        this.clearError()
                                    },
                                });
                            } else  if (cvalue == 2) {
                                swal({

                                    title: "We're having trouble verifying your WWCC.",
                                    text: "Don't worry. We'll look into it and send you a message once we have verified your information.",
                                    type: 'warning',
                                    animation: false,
                                    showCancelButton: false,
                                    showConfirmButton: true,
                                    confirmButtonText: 'Go to Dashboard',
                                    reverseButtons: true,
                                    showLoaderOnConfirm: true,
                                    preConfirm: () => {
                                        this.clearError()
                                    },
                                }).then(function(isConfirm) {
                                    if (isConfirm) {
                                        _self.$router.go(_self.steps.next);
                                    }
                                });
                            }


                        } else {
                            swal({

                                title: "Oops. You've missed something.",
                                type: 'warning',
                                animation: false,
                                showCancelButton: true,
                                confirmButtonText: 'Fix Later',
                                cancelButtonText: 'Fix Now',
                                reverseButtons: true,
                                showLoaderOnConfirm: true,
                                preConfirm: () => {
                                    this.clearError()
                                },
                            });

                        }
                    }
                }).bind(this);
            }
        },
        getSelectedCriterias() {
            let criterias = {};
            $.each(this.criterias, (key, value) => {
                if (this.userInfo[key]) {
                    this.userInfo[key].map((c) => {
                        if (c.id && c.id === this.ownGender.key && c.data.description) {
                            criterias[c.id] = {data: JSON.stringify(c.data)}
                        } else if (c.id && c.id !== this.ownGender.key) {
                            let data = c.data && typeof c.data.description !== 'undefined' ? null : c.data;
                            criterias[c.id] = {data: JSON.stringify(data)}
                        }
                    });
                }
            });
            return criterias;
        },
        getFieldsForValidation(){
            let fields = {};
            $.each(this.criterias, (key, value) => {
                if (this.userInfo[key]) {
                    this.userInfo[key].map((c) => {
                        if (c.id && c.id === this.ownGender.key && c.data.description) {
                            fields[key] = c.data.description;
                        } else if (c.id && c.id !== this.ownGender.key) {
                            value.map((criteria) => {
                                if(criteria.id === c.id)
                                    fields[key] = criteria.name;
                            });
                        }
                    });
                }
            });
            return fields;
        },
        getCriteria(criteria) {
            return new Promise((resolve, reject) => {
                this.$http.get('/api/v1/common/criteria/' + criteria).then((response) => {
                    this.criterias[criteria] = response.data.data[0];
                    resolve(this.criterias[criteria]);
                }, (response) => {
                    reject(response);
                }).bind(this);
            });
        },
        extractPivots(info) {
            $.each(info.availability, (i, av) => {
                info.availability[i].data = JSON.parse(av.pivot.data);
            });

            $.each(info.qualification, (i, q) => {
                if (q.pivot) {
                    info.qualification[i].data = JSON.parse(q.pivot.data);
                }
            });

            $.each(info.gender, (i, g) => {
                if (g.pivot && g.pivot.data && g.pivot.data !== 'null') {
                    info.gender[i].data = JSON.parse(g.pivot.data);
                } else {
                    info.gender[i].data = {description: ""};
                }
            });
            return info;
        },
        loadProgress() {
            $.LoadingOverlay('show');
            this.$http.get('/api/v1/user/progress').then((response) => {
                this.progress = response.data;
                $.LoadingOverlay('hide');
            }, (response) => {
            }).bind(this);
        },
    },
    init() {
        InfoStorage.$on('userInfoChanged', (info) => {
            info = !this.userInfo ? this.extractPivots(info) : info;
            this.userInfo = info;
        });
        InfoStorage.$on('criteriasChanged', (criterias) => {
            this.criterias = criterias;
        });
    },
    watch: {
        userInfo: {
            handler(val) {
                InfoStorage.$emit('userInfoChanged', val);
                console.log(this.$options.name + ' userInfoMixin: ', val);
            },
            deep: true,
        },
        criterias:{
            handler(val){
                InfoStorage.$emit('criteriasChanged', val);
            }
        }
    }
};