import Vue from 'vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('login-form')) {
    var vm = new Vue({
        el: '#login-form',
        data: {
            email: {
                value: '',
                errors: {
                    message: ''
                }
            },
            password: {
                value: '',
                errors: {
                    message: ''
                }
            },
            handle: false,
            remember: false
        },
        methods: {
            loginAction: function()  {
                jQuery("body").LoadingOverlay("show");
                this.$http.post('/auth/login', this.formData ).then(function() {
                    setTimeout(function(){
                        document.getElementsByTagName("form")[0].submit();
                        jQuery("body").LoadingOverlay("hide");
                    }, 300);

                    // window.location = "/home";

                }, function(response) {
                    jQuery("body").LoadingOverlay("hide");
                    this.handleError(response);
                }).bind(this);
            },
            handleError: function(response) {
                if (response.status === 422) {
                    this.email.errors.message = typeof response.data.email != 'undefined' ? response.data.email[0] : '';
                    this.password.errors.message = typeof response.data.password != 'undefined' ? response.data.password[0] : '';
                    this.handle = typeof response.data.handle != 'undefined' ? response.data.handle : '';
                } else {
                    this.email.errors.message = '';
                    this.password.errors.message = '';
                    this.handle = '';
                }
            }
        },
        computed: {
            formData: function() {
                return {
                    email: this.email.value,
                    password: this.password.value,
                    remember: this.remember
                }
            }
        }
    });
}