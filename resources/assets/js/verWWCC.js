import Vue from 'vue';
Vue.use(require('vue-resource'));
import $ from 'jquery';
import FileUploader from './components/FileUploader.vue';
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('wwcc')) {
    var vm = new Vue({
        el: '#wwcc',
        data: {
            form: {
                wwcc:{
                    error: '',
                    text: ''
                },
                photo:{
                    error: ""
                },
                notes: {
                    error:'',
                    text: ''
                }
            },
            image: '',
            loading: {
                image: false,
            },
            wwcc_scan_type:'file',
        },
        components: {
            FileUploader
        },
        methods: {
            onFileUploaded: function(prop_name, remote_file) {
                this[prop_name] = remote_file;
            },
            detectScanType() {
                // Detect whether file is image
                if( this.image.indexOf("JFIF") !== -1 ||
                    this.image.indexOf("base64") !== -1 ||
                    this.image.indexOf("PNG") !== -1 ||
                    this.image.indexOf("GIF") !== -1 ||
                    this.image.indexOf("BMP") !== -1 ||
                    this.image.indexOf("jpeg") !== -1 ||
                    this.image.indexOf("JPEG") !== -1 ||
                    this.image.indexOf("gif") !== -1 ||
                    this.image.indexOf("bmp") !== -1 ||
                    this.image.indexOf("jpg") !== -1 ||
                    this.image.indexOf("JPG") !== -1 ||
                    this.image.indexOf("png") !== -1
                ){
                    this.wwcc_scan_type = 'image';
                }else{
                    this.wwcc_scan_type = 'file';
                }
            },
            onFileChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;

                var that = this;

                var data = new FormData();
                $.each(files, function(key, value)
                {
                    data.append(key, value);
                });

                this.loading.image = true;
                this.form.photo.error = '';


                $.ajax({
                    xhr: function()
                    {
                        var xhr = new window.XMLHttpRequest();
                        // прогресс загрузки на сервер
                        xhr.upload.addEventListener("progress", function(evt){
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                // делать что-то...
                                console.log("upload:"+percentComplete);
                                var prg = Math.round(percentComplete * 100);

                                $('#'+e.target.name+'progress').attr('aria-valuenow', prg)
                                    .css({width:prg+'%'})
                                    .html(prg+'%');
                            }
                        }, false);
                        return xhr;
                    },
                    url: '/api/v1/upload/image',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function(data, textStatus, jqXHR)
                    {
                        if(typeof data.error === 'undefined')
                        {
                            // Success so call function to process the form

                            that.loading.image = false;
                            that.image = data.image_path;

                            console.log(data);

                            that.detectScanType();

                        }
                        else
                        {
                            // Handle errors here
                            console.log('ERRORS: ' + data.error);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        // Handle errors here
                        console.log('ERRORS: ' + textStatus);
                        that.loading.data = false;
                    }
                });

                //this.createImage(files[0]);
            },
            createImage(file) {
                var image = new Image();
                var reader = new FileReader();
                var vm = this;

                reader.onload = (e) => {
                    vm.image = e.target.result;
                };
                reader.readAsDataURL(file);
                this.form.photo.error = '';
            },
            postForm(){
                jQuery("body").LoadingOverlay("show");

                if(this.loading.image) {
                    var _this = this;

                    var postFormInterval = window.setInterval(()=>{
                        if(!_this.loading.image ) {
                            window.clearInterval(postFormInterval);
                            _this.postForm();
                        }
                    },200);

                    return;
                }

                var formData = {
                    wwcc: this.form.wwcc.text,
                    notes: this.form.notes.text,
                    photo: this.image,
                }
                this.$http.patch('/api/v1/settings/wwcc', formData)
                    .then( function(response) {
                            // this.$dispatch('notification', {type: response.data.notification.type, 'message': response.data.notification.message, 'close': false});
                        window.location.href = "/user/settings/get-verified";
                        }, function(response) {
                        jQuery("body").LoadingOverlay("hide");
                            for (var key in response.data) {
                                this.form[key].error =  'This is required field';
                            }
                            // on unknown error
                            if (response.status === 500) {
                                this.cancel();
                                // this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                            }
                        }
                    );
            },
            setUserInfo: function (data) {
                this.userInfo = data;
                // alert(JSON.stringify(data, null, 4));
                this.form.wwcc.text = data.wwcc_number;
                this.form.notes.text = data.wwcc_notes;
                this.image= data.wwcc_image;

                const that = this;
                window.setTimeout(function(){
                    that.detectScanType();
                }, 300);

            }


        },
        ready: function () {

            this.$http.get('/api/v1/settings/user-info').then(function(response) {
                this.setUserInfo(response.data);
            }, function() {
                this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);
        }
    });
}