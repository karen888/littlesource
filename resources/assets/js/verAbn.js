import Vue from 'vue';
Vue.use(require('vue-resource'));
import VueMask from 'v-mask';

import $ from 'jquery';
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;
Vue.use(VueMask);

if (document.getElementById('verified-abn')) {
    var vm = new Vue({
        el: '#verified-abn',
        data: {
            form: {
                abn_number:{
                    error: '',
                    text: ''
                },
            },
        },
        methods: {
            submit(){
                var abn = this.form.abn_number.text;
                if(abn.replace(/\s/g,'').replace(/\D/g,'').length != 11){
                    this.form.abn_number.error = "ABN must be 11 digits";
                    return;
                }else{

                    var formData = {
                        abn_number: this.form.abn_number.text,
                    };
                    jQuery("body").LoadingOverlay("show");
                    this.$http.patch('/api/v1/settings/abn', formData)
                        .then( function(response) {
                            window.location.href = "/user/settings/get-verified";
                                // this.$dispatch('notification', {type: response.data.notification.type, 'message': response.data.notification.message, 'close': false});
                            }, function(response) {
                            jQuery("body").LoadingOverlay("hide");
                                for (var key in response.data) {
                                    this.form[key].error =  'This is a required field';
                                }
                                // on unknown error
                                if (response.status === 500) {
                                    this.cancel();
                                    // this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                                }
                            }
                        );
                }
            },
            setUserInfo: function (data) {

                this.form.abn_number.text = data.abn_number.replace(/\s/g,'');
            }
        },
        ready(){

            this.$http.get('/api/v1/settings/user-info').then(function(response) {
                this.setUserInfo(response.data);
            }, function() {
                this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);
        }
    });
}