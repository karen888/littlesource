export const listColors = {
    data(){
      return  {
          colors : [
              '#bae0c3',
              '#f4e894',
              '#73adc5',
              '#86c2bb',
              '#c57373',
              '#97c2d3',
              '#b9dfc2',
              '#f4a4c5',
              '#98999b',
              '#d3af5d',
              '#c5738a',
              '#666568',
              '#c57373',
          ]
      }
    },
    methods:{
        getRandomColor() {
            return this.colors[Math.floor(Math.random() * this.colors.length)];
        }
    }
};


