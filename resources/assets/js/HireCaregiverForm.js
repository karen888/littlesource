import Pikaday from 'pikaday';
import Vue from 'vue';
import CustomSelect from './components/CustomSelect.vue';
import _ from 'lodash';
import $ from 'jquery';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;


function todayDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    }

    if(mm<10) {
        mm='0'+mm
    }

    today = dd+'/'+mm+'/'+yyyy;

    return today;
}

var today = todayDate();
/*
 For password and security Settings
 */
if (document.getElementById('hire-employee')) {
    //var picker = new Pikaday({ field: document.getElementById('start-date') });

    var vm = new Vue({
        el: '#hire-employee',
        data: {
            /* Offer data */
            title: {
                text: '',
                errors: {
                    message: ''
                }
            },
            rate: {
                text: '',
                errors: {
                    message: ''
                }
            },
            start_date: {
                text: '',
                errors: {
                    message: ''
                }
            },
            additional_info: {
                text: '',
                errors: {
                    message: ''
                }
            },
            caregiver: {
                text: '',
                errors: {
                    message: ''
                }
            },
            job: {
                selected: '',
                errors: {
                    message: ''
                },
                options: false,
                text: ''
            },
            cards: {
                selected: '',
                errors: {
                    message: ''
                },
                options: false,
                text: ''
            },

            /* Billing data */
            fname: {
                text: '',
                errors: {
                    message: ''
                }
            },
            lname: {
                text: '',
                errors: {
                    message: ''
                }
            },
            card_number: {
                text: '',
                errors: {
                    message: ''
                }
            },
            exp_date_m: {
                selected: '12',
                errors: {
                    message: ''
                },
                text: ''
            },
            exp_date_y: {
                selected: '2016',
                errors: {
                    message: ''
                },
                text: ''
            },
            security_code: {
                text: '',
                errors: {
                    message: ''
                }
            },
            address: {
                text: '',
                errors: {
                    message: ''
                }
            },
            address_optional: '',
            city: {
                text: '',
                errors: {
                    message: ''
                }
            },
            postal_code: {
                text: '',
                errors: {
                    message: ''
                }
            },
            phone: {
                text: '',
                errors: {
                    message: ''
                }
            }
        },
        components: {
            'custom-select': CustomSelect
        },
        methods: {
            createOffer: function () {
                if (parseInt(this.rate.text) < 25) {
                    this.rate.errors.message = "The minimum hourly rate is $25/hr"
                    return;
                } else {
                    this.rate.errors.message = ""
                }

                this.$http.post('/api/v1/clients/offer/create', this.formDataOffer ).then(function(response) {
                    this.storedOffer(response);
                }, function(response) {
                    this.handleErrorOffer(response);
                }).bind(this);
            },

            createCard: function () {
                if (_.isEmpty(this.cards.options)) {
                    this.$http.post('/api/v1/clients/card/create', this.formDataCard ).then(function(response) {
                        if (response.data.data.cards) {
                            if (_.isArray(response.data.data.cards)) {
                                this.cards.options = response.data.data.cards;
                                this.cards.selected = response.data.data.card_id;
                            } else {
                                this.cards.options = false;
                            }
                        }
                        this.createOffer();
                    }, function(response) {
                        this.createOffer();
                        this.handleErrorCard(response);
                    }).bind(this);
                } else {
                    //if (_.isEmpty(this.cards.selected)) {
                    if (! this.cards.selected) {
                        this.cards.errors.message = "The billing methods field is required.";
                        return;
                    }
                    this.createOffer();
                }
            },

            createCardBilling: function () {
                this.$http.post('/api/v1/clients/card/create', this.formDataCard ).then(function(response) {
                    if (response.data.data.cards) {
                        if (_.isArray(response.data.data.cards)) {
                            this.cards.options = response.data.data.cards;
                            this.cards.selected = response.data.data.card_id;
                        } else {
                            this.cards.options = false;
                        }
                    }
                    window.location = '/user/settings/billing';
                }, function(response) {                    
                    this.handleErrorCard(response);
                }).bind(this);                
            },

            updateCardBilling: function ($id) {
                this.$http.post('/api/v1/clients/card/update/'+$id, this.formDataCard ).then(function(response) {
                    if (response.data.data.cards) {
                        if (_.isArray(response.data.data.cards)) {
                            this.cards.options = response.data.data.cards;
                            this.cards.selected = response.data.data.card_id;
                        } else {
                            this.cards.options = false;
                        }
                    }
                    window.location = '/user/settings/billing';
                }, function(response) {                    
                    this.handleErrorCard(response);
                }).bind(this);                
            },

            storedOffer: function (response) {
                if (response.data.success) {
                    window.location = response.data.redirect;
                }
            },

            expDateMSelectedValue: function (value) {
                this.exp_date_m.text = value;
            },

            expDateYSelectedValue: function (value) {
                this.exp_date_y.text = value;
            },

            countrySelectedValue: function (value) {
                this.country.text = value;
            },

            cardsSelectedValue: function (value) {
                this.cards.text = value;
            },

            jobSelectedValue: function (value) {
                this.job.text = value;
            },

            handleErrorOffer: function (response) {
                $('html, body').animate({
                    scrollTop: $("#hire-employee").offset().top
                }, 400);
                if (response.status === 422) {
                    this.job.errors.message = typeof response.data.job_id != 'undefined' ? response.data.job_id[0] : '';
                    this.caregiver.errors.message = typeof response.data.to_user_id != 'undefined' ? response.data.to_user_id[0] : '';
                    this.additional_info.errors.message = typeof response.data.offer_text != 'undefined' ? response.data.offer_text[0] : '';
                    this.rate.errors.message = typeof response.data.hourly_rate != 'undefined' ? response.data.hourly_rate[0] : '';
                    this.title.errors.message = typeof response.data.title != 'undefined' ? response.data.title[0] : '';
                    this.start_date.errors.message = typeof response.data.start_date != 'undefined' ? response.data.start_date[0] : '';
                }
            },

            handleErrorCard: function (response) {
                $('html, body').animate({
                    scrollTop: $("#hire-employee").offset().top
                }, 400);
                if (response.status === 422) {
                    this.phone.errors.message = typeof response.data.card_description != 'undefined' ? response.data.card_description[0] : '';
                    this.card_number.errors.message = typeof response.data.card_number != 'undefined' ? response.data.card_number[0] : '';
                    this.exp_date_m.errors.message = typeof response.data.card_expdate_m != 'undefined' ? response.data.card_expdate_m[0] : '';
                    this.exp_date_y.errors.message = typeof response.data.card_expdate_y != 'undefined' ? response.data.card_expdate_y[0] : '';
                    this.security_code.errors.message = typeof response.data.card_cvv2 != 'undefined' ? response.data.card_cvv2[0] : '';
                    this.fname.errors.message = typeof response.data.card_firstname != 'undefined' ? response.data.card_firstname[0] : '';
                    this.lname.errors.message = typeof response.data.card_lastname != 'undefined' ? response.data.card_lastname[0] : '';
                    this.address.errors.message = typeof response.data.card_address1 != 'undefined' ? response.data.card_address1[0] : '';
                    this.city.errors.message = typeof response.data.card_city != 'undefined' ? response.data.card_city[0] : '';
                    this.postal_code.errors.message = typeof response.data.card_post_code != 'undefined' ? response.data.card_post_code[0] : '';
                }
            },

            setRate: function() {
                if (this.rate.text.length > 6) {
                    this.rate.text = this.rate.text.substr(0, 6);
                }

                if (Number(this.rate.text)) {
                    this.rate.errors.message = ""
                } else {
                    this.rate.errors.message = "The hourly rate is invalid."
                }


                if (parseInt(this.rate.text) < 25) {
                    this.rate.errors.message = "The minimum hourly rate is $25/hr"
                } else {
                    this.rate.errors.message = ""
                }
            },
            setToday: function(){
                this.start_date.text = todayDate();
            }
        },
        computed: {
            formDataOffer: function () {
                return {
                    job_id: this.job.text,
                    card_id: this.cards.selected,
                    to_user_id: this.caregiver.text,
                    offer_text: this.additional_info.text,
                    hourly_rate: this.rate.text,
                    title: this.title.text,
                    start_date: this.start_date.text
                }
            },

            formDataCard: function () {
                return {
                    card_description: 'test',
                    card_type: 'type',
                    card_number: this.card_number.text,
                    card_expdate_m: parseInt(this.exp_date_m.text),
                    card_expdate_y: parseInt(this.exp_date_y.text),
                    card_cvv2: this.security_code.text,
                    card_firstname: this.fname.text,
                    card_lastname: this.lname.text,
                    card_address1: this.address.text,
                    card_address2: this.address_optional,
                    card_city: this.city.text,
                    card_post_code: this.postal_code.text
                }
            }
        },
        watch: {
            'cards.options': function (val) {
                if (!_.isArray(val) && val) {
                    val = JSON.parse(val);
                    if (_.isObject(val)) {
                        this.cards.options = val;
                    } else {
                        this.cards.options = false;
                    }
                }
            }
        },
        ready: function() {
            var _this = this;




            jQuery('[name="rate"]').blur(function(){
                _this.rate.text = parseFloat(_this.rate.text);

                if (_this.rate.text > 1000) {
                    _this.rate.text = 999;
                }

                _this.rate.text = _this.rate.text.toFixed(2);

                if (Number(_this.rate.text)) {
                    _this.rate.errors.message = ""
                } else {
                    _this.rate.text = "0.00";
                    _this.rate.errors.message = "The hourly rate is invalid."
                    return;
                }

                if (parseInt(_this.rate.text) < 25) {
                    _this.rate.errors.message = "The minimum hourly rate is $25/hr"
                } else {
                    _this.rate.errors.message = ""
                }
            });
        }
    });
}
