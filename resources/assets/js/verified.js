import Vue from 'vue';

Vue.use(require('vue-resource'));
import $ from 'jquery';
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;


if (document.getElementById('get-verified')) {
    var vm = new Vue({
        el: '#get-verified',
        data: {
        },
        methods: {
            confDel: function(message, type, id = null, event){
                if (event) event.preventDefault();

                if (confirm(message)){
                    this.$http.post('/api/v1/settings/delete-verifed', {type: type, id:id})
                        .then( function(response) {
                            location.reload();
                            }, function(response) {
                        }
                        );
                }else{
                }
            },
            sendMessage: function(type, id, panelId) {
                var text = jQuery('#chat_' + panelId + '_message').val();

                this.$http.post('/api/v1/settings/send-message', {type: type, id:id, text: text})
                    .then( function(response) {
                            location.reload();
                        }, function(response) {
                        }
                    );
            }
        },
        ready() {
            jQuery(".fancybox").fancybox();
        }
    });
}