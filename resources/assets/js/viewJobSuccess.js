import Vue from 'vue';
import jQuery from 'jquery';
import Notifications from './components/Notifications.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('view_job_success')) {
    var vm = new Vue({
        el: '#view_job_success',
        data: {
            job_id: false,
            employee: [],
            displayNotification: false,
            notificationValues: null,
        },
        components: {
            'notifications': Notifications
        },
        methods: {
            setInviteAll: function () {
                var employee = [];
                jQuery('.caregivers .employee_list').each(function(index, element){
                    employee.push(jQuery(element).data('id'));
                });
                this.employee = employee;

                this.$http.post('/api/v1/clients/job/invite-all', this.formData ).then(function(response) {
					jQuery('.btn-invite').html('Invites sent!');
                    jQuery('.btn-invite').addClass('disabled');
                    this.handleNotification({type: 'success', 'message': 'Invites sent!', 'close': false});
                }, function() {
                    this.handleNotification({type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                }).bind(this);
            },
            handleNotification: function (notification) {
                this.notificationValues = notification;
                this.displayNotification = !notification.close;
            }
        },
        computed: {
            formData: function () {
                return {
                    job_id: this.job_id,
                    employee: this.employee
                }
            },
        }
    });
}
