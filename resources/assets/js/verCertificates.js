import Vue from 'vue';
Vue.use(require('vue-resource'));
import $ from 'jquery';
import Pikaday from 'pikaday';
import FileUploader from './components/FileUploader.vue';

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;
if (document.getElementById('cetrificates-verifed')) {
    var vm = new Vue({
        el: '#cetrificates-verifed',
        data: {
            form: {
                name:{
                    error: '',
                    text: ''
                },
                provider:{
                    error: '',
                    text: ''
                },
                desc:{
                    error:'',
                    text: ''
                },
                notes:{
                    error:'',
                    text: ''
                },
                date_earned:{
                    error: '',
                    text: ''
                },
                submission:{
                    error: '',
                    text: ''
                },
                scan: {
                    error: ''
                }
            },
            scan: '',
            loading: false,
            scan_type: 'image',
        },
        components: {
            FileUploader
        },
        methods: {
            onFileUploaded: function(prop_name, remote_file) {
                this[prop_name] = remote_file;
            },
            onFileChange(e) {
                var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;

                var that = this;

                var data = new FormData();
                $.each(files, function(key, value)
                {
                    data.append(key, value);
                });

                this.loading = true;

                this.form.scan.error = '';
                $.ajax({
                    xhr: function()
                    {
                        var xhr = new window.XMLHttpRequest();
                        // прогресс загрузки на сервер
                        xhr.upload.addEventListener("progress", function(evt){
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                // делать что-то...
                                console.log("upload:"+percentComplete);
                                var prg = Math.round(percentComplete * 100);

                                $('#'+e.target.name+'progress').attr('aria-valuenow', prg)
                                    .css({width:prg+'%'})
                                    .html(prg+'%');
                            }
                        }, false);
                        return xhr;
                    },
                    url: '/api/v1/upload/image',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function(data, textStatus, jqXHR)
                    {
                        if(typeof data.error === 'undefined')
                        {
                            // Success so call function to process the form

                            that.loading= false;
                            that.scan = data.image_path;

                            that.detectScanType();

                        }
                        else
                        {
                            // Handle errors here
                            console.log('ERRORS: ' + data.error);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        // Handle errors here
                        console.log('ERRORS: ' + textStatus);
                        that.loading = false;
                    }
                });

            },
            detectScanType() {
                // Detect whether file is image
                if( this.scan.indexOf("JFIF") !== -1 ||
                    this.scan.indexOf("base64") !== -1 ||
                    this.scan.indexOf("PNG") !== -1 ||
                    this.scan.indexOf("GIF") !== -1 ||
                    this.scan.indexOf("BMP") !== -1 ||
                    this.scan.indexOf("jpeg") !== -1 ||
                    this.scan.indexOf("JPEG") !== -1 ||
                    this.scan.indexOf("gif") !== -1 ||
                    this.scan.indexOf("bmp") !== -1 ||
                    this.scan.indexOf("jpg") !== -1 ||
                    this.scan.indexOf("JPG") !== -1 ||
                    this.scan.indexOf("png") !== -1
                ){
                    this.scan_type = 'image';
                }else{
                    this.scan_type = 'file';
                }
            },
            saveForm(fun){
                var formData = {
                    id: this.getURLParam (window.location.search, "id"),
                    name: this.form.name.text,
                    provider: this.form.provider.text,
                    description: this.form.desc.text,
                    notes: this.form.notes.text,
                    date_earned: this.form.date_earned.text,
                    submission: this.form.submission.text,
                    scan: this.scan
                }
                $("#saveBtn").attr('disabled', true);
                jQuery("body").LoadingOverlay("show");

                this.$http.put('/api/v1/employee/profile/certificates', formData)
                    .then( function(response) {
                            // this.$dispatch('notification', {type: response.data.notification.type, 'message': response.data.notification.message, 'close': false});
                            if(fun == 'savemore'){
                                this.clearForms();
                                $("#saveBtn").attr('disabled', false);
                                jQuery("body").LoadingOverlay("hide");
                                // window.location.href = "/user/settings/get-verified";r
                            }else {
                                window.location.href = "/user/settings/get-verified";
                            }
                        }, function(response) {
                            $("#saveBtn").attr('disabled', false);
                            jQuery("body").LoadingOverlay("hide");
                            for (var key in response.data) {
                                this.form[key].error =  'This is required field';
                            }
                            // on unknown error
                            if (response.status === 500) {
                                this.cancel();
                                // this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                            }
                        }
                    );
            },
            clearForms(){
                this.form.name.text = '';
                this.form.provider.text = '';
                this.form.desc.text = '';
                this.form.notes.text = '';
                this.form.date_earned.text = '';
                this.form.submission.selected = '';
                this.scan = ''
            },
            getURLParam (oTarget, sVar) { return decodeURI(oTarget.replace(new RegExp("^(?:.*[&\\?]" + encodeURI(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1")); },
            setUserInfo: function (data) {
                // if(typeof (data.data.certificates[0]) != 'undefined'){
                //     var cert = data.data.certificates[0];
                    var id = this.getURLParam (window.location.search, "id");
                    var _this = this;
                    data.data.certificates.forEach(function(cert){
                        if(id  == cert.id) {
                            _this.form.name.text = cert.name;
                            _this.form.provider.text = cert.provider;
                            _this.form.desc.text = cert.description;
                            _this.form.notes.text = cert.notes;
                            _this.form.date_earned.text = cert.date_earned;
                            _this.form.submission.text = cert.submission;
                            _this.scan = cert.scan;
                        }
                    });
            }
        },
        ready(){
            var date = new Date();
            var picker = new Pikaday({
                field: document.getElementById('dateis'),
                maxDate: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
                yearRange: [1950, date.getFullYear()]
            });
            this.$http.get('/api/v1/employee/get-info').then(function(response) {
                this.setUserInfo(response.data);
            }, function() {
                this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);
        }
    });
}