import Vue from 'vue';
import CustomSelectExt from './components/CustomSelect.vue';
import StarRating from './components/StarRating.vue';
import _ from 'lodash';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('end-contract')) {
    var vm = new Vue({
        el: '#end-contract',
        components: {
            'custom-select': CustomSelectExt,
            'star-rating' : StarRating
        },
        data: {
            set_type: true,
            contract: false,
            rating_types: [],
            feedback_type: '',
            errors: {
                reason: false,
                comment: false,
                note: false, 
                reason_text: false
            },
            urls: {
                rating_types: '/api/v1/common/rating-options/',
                feedback_reason: '/api/v1/common/feedback-reason',
                post_feedback: '/api/v1/contracts/feedback',
                get_feedback: '/api/v1/contracts/feedback/'
            },
            reason: {
                items: [],
                selected: false
            },
            reason_text: '',
            comment: '',
            note: '',
            prev_rating: 5,
            prev_comment: ''
        },
        methods: {
            endContract: function () {
                this.$http.post(this.urls.post_feedback, this.formData ).then(function(response) {
                    if (response.data.data.url) {
                        window.location = response.data.data.url;
                    }
                }, function(response) {
                    this.handleError(response);
                }).bind(this);
            },
            handleError: function (response) {
                if (response.status === 422) {
                    for (var e in this.errors) {
                        this.errors[e] = typeof response.data[e] != 'undefined' ? response.data[e][0] : '';
                    }
                }
            }
        },
        ready: function () {
            this.$http.get(this.urls.rating_types + this.feedback_type)
                .then(function(response) {
                    this.rating_types = response.data.data.rating_options;

                    this.$http.get(this.urls.get_feedback + this.contract)
                        .then(function(response) {
                            var data = response.data.data;
                            if (data.rating) {
                                this.set_type = false;
                                this.prev_rating = data.rating;
                                this.prev_comment = data.comment;
                                this.comment = data.comment;
                                this.note = data.note;
                                this.reason.selected = data.feedback_reason;

                                for (var i in data.ratings) {
                                    for (var j in this.rating_types) {
                                        if (this.rating_types[j].id == data.ratings[i].id) {
                                            this.rating_types[j].value = data.ratings[i].pivot.data;
                                        }
                                    }
                                }
                            }
                        })
                        .catch(function(response) { console.log(['Error!', response]); })
                        .finally(function(response) {})
                        .bind(this);
                })
                .catch(function(response) { console.log(['Error!', response]); })
                .finally(function(response) {})
                .bind(this);

            this.$http.get(this.urls.feedback_reason)
                .then(function(response) {
                    this.reason.items = response.data.data.feedback_reason;
                })
                .catch(function(response) { console.log(['Error!', response]); })
                .finally(function(response) {})
                .bind(this);

        },
        computed: {
            contractTitle: function() {
                return this.set_type ? 'End Contract' : 'Change Feedback';
            },
            isEmptyRatingTypes: function () {
                return _.isEmpty(this.rating_types);
            },
            formData: function () {
               return {
                    reason: this.reason.selected,
                    comment: this.comment,
                    note: this.note,
                    reason_text: this.reason_text,
                    rating_types: this.rating_types,
                    contract: this.contract
                }
            },
            ifReasonOther: function () {
                if (this.reason.items.length) {
                    for (var r in this.reason.items) {
                        if (this.reason.items[r].value == this.reason.selected && this.reason.items[r].text == 'Other') {
                            return true;
                        }
                    }
                }
                return false
            }
        }
    });
}