import Vue from 'vue';
import CustomSelect from './components/CustomSelect.vue';
import CustomSelectExt from './components/CustomSelectExtended.vue';
import CustomSelectExtqualifications from './components/CustomSelectExtendedqualifications.vue';
import CustomSelectMultiple from './components/CustomSelectMultiple.vue';
import StarRating from './components/StarRating.vue';
import _ from 'lodash';
import Pikaday from 'pikaday';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = false;

/*
 For password and security Settings
 */
if (document.getElementById('employee-profile')) {
    var picker = new Pikaday({ field: document.getElementById('start-date') });
    var picker = new Pikaday({
        field: document.getElementById('birthday'),
        yearRange: [1950, 2000]
    });

    var date = new Date();
    var picker = new Pikaday({
            field: document.getElementById('date-earned'),
            maxDate: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
            yearRange: [1990, date.getFullYear()]
        });   

    jQuery("#skills").tagit({
        allowDuplicates: false,
        allowSpaces: false,
        removeConfirmation: true,
        itemName: 'item',
        fieldName: 'tags',
        singleField: true,
        tagLimit: 10,
        singleFieldNode: document.getElementById('tags_input')
    });

    var vm = new Vue({
        el: '#employee-profile',
        data: {
            editMode: false,

            fileUploadFormData: new FormData(),
            profile_picture: '',
            errors: '',
            delete: false,
            picture: '',

            origin: {
                available: {
                    value: false,
                    selected: false
                },
                availability_days: [],
                expect_ready: '',
                display_name: true,
                title: '',
                first_name: '',
                last_name: '',
                last_name_view: '',
                gender: false,
                birthday: '',
                experience: '',
                overview: '',
                min_rate: '',
                max_rate: '',
                qualifications: {
                    have_transportation: false,
                    willing_travel: false,
                    smoker: false,
                    sick_children: false,
                    comfortable_with_pets: false,
                    qualification: [],
                    care_type: [],
                    child_age: [],
                    own_children: false
                },
                skills: [],
                educations: [],
                languages: [],           
            },
            first_name: {
                text: '',
                errors: {
                    message: ''
                }
            },
            last_name: {
                text: '',
                errors: {
                    message: ''
                }
            },
            last_name_view: {
                text: '',
                errors: {
                    message: ''
                }
            },            
            gender: {
                text: false,
                errors: {
                    message: ''
                }
            },
            birthday: {
                text: '',
                errors: {
                    message: ''
                }
            },
            experience: {
                selected: '',
                errors: {
                    message: ''
                }
            },
            available: {
                value: false,
                selected: false,
                text: 'no',
                errors: {
                    message: ''
                }
            },
            availability_days: {
                selected: [],
                error: false
            },
            expect_ready: {
                text: '',
                errors: {
                    message: ''
                }
            },
            display_name: true,
            title: {
                text: '',
                errors: {
                    message: ''
                }
            },
            overview: {
                text: '',
                errors: {
                    message: ''
                }
            },
            min_rate: {
                text: '',
                commisons: '',
                errors: {
                    message: ''
                }
            },
            max_rate: {
                text: '',
                commisons: '',
                errors: {
                    message: ''
                }
            },
            qualifications: {
                willing_travel: false,
                smoker: false,
                sick_children: false,
                comfortable_with_pets: false,
                have_transportation: false,
                qualification: {
                    selected: [],
                    text: '',
                    items: [],
                    multiple: false
                },
                care_type: {
                    multiple: true,
                    selected: []
                },
                child_age: {
                    selected: []
                },
                own_children: false,
            },
            qualifications_errors: {
                willing_travel: false,
                smoker: false,
                sick_children: false,
                comfortable_with_pets: false,
                pets: false,
                have_transportation: false,
                care_type: false,
                child_age: false,
                own_children: false,
                qualifications: false,
            },
            skills: {
                text: '',
                errors: {
                    message: ''
                }
            },
            educations: {
                list: [{
                    school: {
                        text: '',
                        errors: {
                            message: ''
                        }
                    },
                    degree: {
                        text: '',
                        errors: {
                            message: ''
                        }
                    },
                    area_study: {
                        text: '',
                        errors: {
                            message: ''
                        }
                    },
                    dates_attended_from: {
                        selected: '2016',
                        errors: {
                            message: ''
                        }
                    },
                    dates_attended_to: {
                        selected: '2017',
                        errors: {
                            message: ''
                        }
                    },
                    desc: {
                        text: ''
                    }
                }]
            },
            languages: {
                list: [{
                    name: '',
                    errors: {
                        message: ''
                    }
                }]
            },
            certificates: {
                id: "",
                name: {
                    text: '',
                    errors: {
                        message: ''
                    }
                },
                provider: {
                    text: '',
                    errors: {
                        message: ''
                    }
                },
                description: {
                    text: '',
                    errors: {
                        message: ''
                    }
                },
                date_earned: {
                    text: '',
                    errors: {
                        message: ''
                    }
                },
                submission: {
                    text: '',
                    errors: {
                        message: ''
                    }
                }
            },
            selects: null,
            job_history: [],
            view_feedback: [],
            load_more: true,
            employee_rating: false
        },
        components: {
            'custom-select': CustomSelect,
            'custom-select-ext': CustomSelectExt,
            'custom-select-ext-qualifications': CustomSelectExtqualifications,
            'custom-select-multiple': CustomSelectMultiple,
            'star-rating' : StarRating
        },
        methods: {

            saveProfilePicture() {
                // if (this.delete) {
                    this.fileUploadFormData.append('delete_photo', this.delete);
                    if(this.delete == false){
                        var $input = $("#file");
                        this.fileUploadFormData.append('profile_picture', $input.prop('files')[0]);
                    }
                    this.$http.post('/api/v1/settings/profile-picture', this.fileUploadFormData)
                        .then( function(response) {
                            // resets form to default
                           this.setProfilePicture();
                        }, function(response) {
                            // on validation error
                        }
                    );
                // }
            },

            setProfilePicture() {
                this.$http.get('/api/v1/settings/profile-picture').then(function(response) {
                    this.picture = response.data;
                    $('.profile-picture img').attr('src', response.data);
                    $('.profile-image img').attr('src', response.data);
                    $('.caregiver-info img').attr('src', response.data);
                }, function(response) {

                });
            },

            setAutocomplete: function () {
                jQuery(".language_text").autocomplete({
                    source: ["English"]
                });
            },
            setRequest: function(type) {
                switch (type) {
                    /** save Availability **/
                    case 'availability':
                        var modal = '#availability-modal';
                        var path = '/api/v1/employee/profile/availability';
                        var formData = this.formDataAvailability;
                        var handleError = this.handleErrorAvailability;
                        break;

                    /** save Hourly Rate **/
                    case 'hourly-rate':
                        var error = false;
                        if(!this.min_rate.text) {
                            this.min_rate.errors.message = 'Minimum rate is required!';
                            error = true;
                        }
                        if(!this.max_rate.text) {
                            this.max_rate.errors.message = 'Maximum rate is required!';
                            error = true;
                        }

                        if(parseFloat(this.min_rate.text) < 25) {
                            this.min_rate.errors.message = 'Mininum rate is $25/hour';
                            error = true;
                        }

                        if(error === true) {
                            return;
                        }

                        var modal = '#hourly-rate-modal';
                        var path = '/api/v1/employee/profile/hourlyrate';
                        var formData = this.formDataHourlyRate;
                        var handleError = this.handleErrorHourlyRate;
                        break;

                    /** save Name Title **/
                    case 'name-title':
                        var modal = '#name-title-modal';
                        var path = '/api/v1/employee/profile/nametitle';
                        var formData = this.formDataNameTitle;
                        var handleError = this.handleErrorNameTitle;
                        break;

                    /** save Overview **/
                    case 'overview':
                        var modal = '#overview-modal';
                        var path = '/api/v1/employee/profile/overview';
                        var formData = this.formDataOverview;
                        var handleError = this.handleErrorOverview;
                        break;

                    /** save Qualifications **/
                    case 'qualifications':
                        var modal = '#qualifications-modal';
                        var path = '/api/v1/employee/profile/qualifications';
                        var formData = this.formDataQualifications;
                        var handleError = this.handleErrorQualifications;
                        break;

                    /** save Skills **/
                    case 'skills':
                        var modal = '#skills-modal';
                        var path = '/api/v1/employee/profile/skills';
                        var formData = this.formDataSkills;
                        var handleError = this.handleErrorSkills;
                        break;

                    /** save Educations **/
                    case 'educations':
                        var modal = '#education-modal';
                        var path = '/api/v1/employee/profile/education';
                        var formData = this.formDataEducation;
                        var handleError = this.handleErrorEducation;
                        break;

                    /** save Certificates **/
                    case 'certificates':
                        var modal = '#certificates-modal';
                        var path = '/api/v1/employee/profile/certificates';
                        var formData = this.formDataCertificates;
                        var handleError = this.handleErrorEducation;
                        break;
                    /** save Experience **/
                    case 'experience':
                        var modal = '#experience-modal';
                        var path = '/api/v1/employee/profile/experience';
                        var formData = this.formDataExperience;
                        var handleError = this.handleErrorExperience;
                        break;
                }

                jQuery(modal + " .btn-primary.btn-main-action").addClass('disabled');
                this.$http.put(path, formData).then(function() {
                    this.saveResponse(type);
                    if(location.href.indexOf('edit=') !== -1 || location.href.indexOf('directEdit=') !== -1) {
                        location.href = '/employee/profile';
                    }
                    if(type == 'name-title'){
                        this.saveProfilePicture();
                        location.reload();
                    }
                    jQuery(modal + " .btn-primary.btn-main-action").removeClass('disabled');
                    jQuery(modal).modal('hide');                    
                }, function(response) {
                    jQuery(modal + " .btn-primary.btn-main-action").removeClass('disabled');
                    handleError(response);
                }).bind(this);
            },
            saveResponse: function(type) {
                switch (type) {
                /** Availability **/
                    case 'availability':
                        this.origin.available.value = this.available.value;
                        this.origin.available.selected = this.available.selected;
                        this.origin.availability_days = this.availability_days.selected;
                        break;

                /** Hourly Rate **/
                    case 'hourly-rate':
                        this.origin.min_rate = this.min_rate.text;
                        this.origin.max_rate = this.max_rate.text;
                        break;

                /** Name Title **/
                    case 'name-title':
                        this.origin.title = this.title.text;
                        this.origin.gender = this.gender.text;
                        this.origin.birthday = this.birthday.text;
                        this.origin.first_name = this.first_name.text;
                        this.origin.last_name = this.last_name.text;
                        break;

                /** Overview **/
                    case 'overview':
                        this.origin.overview = this.overview.text;
                        break;

                /** Qualifications **/
                    case 'qualifications':
                        this.origin.qualifications.have_transportation = this.qualifications.have_transportation;
                        this.origin.qualifications.willing_travel = this.qualifications.willing_travel;
                        this.origin.qualifications.smoker = this.qualifications.smoker;
                        this.origin.qualifications.sick_children = this.qualifications.sick_children;
                        this.origin.qualifications.comfortable_with_pets = this.qualifications.comfortable_with_pets;
                        this.origin.qualifications.qualification = this.qualifications.qualification.selected;
                        this.origin.qualifications.care_type = this.qualifications.care_type.selected;
                        this.origin.qualifications.child_age = this.qualifications.child_age.selected;
                        this.origin.qualifications.own_children = this.qualifications.own_children;
                        break;

                /** Skills **/
                    case 'skills':
                        this.origin.skills = [];
                        this.origin.skills = this.skills.text.split(',');
                        break;

                /** Educations **/
                    case 'educations':
                        this.origin.educations= [];
                        this.origin.languages = [];

                        if (!_.isEmpty(this.educations.list)) {
                            for (var data in this.educations.list) {
                                this.origin.educations.push(this.educations.list[data]);
                            }
                        } else {
                            this.origin.educations = [];
                        }

                        if (!_.isEmpty(this.languages.list)) {
                            for (var data in this.languages.list) {
                                this.origin.languages.push(this.languages.list[data]);
                            }
                        } else {
                            this.origin.languages = [];
                        }
                        break;
                    case 'experience':
                        this.origin.experience = this.experience.selected;
                        break;
                }
            },
            cancelEdit: function(type) {
                switch (type) {
                /** Availability **/
                    case 'availability':
                        this.available.value = this.origin.available.value;
                        this.available.selected = this.origin.available.selected;
                        this.availability_days.selected = this.origin.availability_days;
                        break;

                /** Hourly Rate **/
                    case 'hourly-rate':
                        this.min_rate.text = this.origin.min_rate;
                        this.max_rate.text = this.origin.max_rate;
                        break;

                /** Name Title **/
                    case 'name-title':
                        this.title.text = this.origin.title;
                        this.gender.text = this.origin.gender;
                        this.birthday.text = this.origin.birthday;
                        this.first_name.text = this.origin.first_name;
                        this.last_name.text = this.origin.last_name;
                        break;

                /** Overview **/
                    case 'overview':
                        this.overview.text = this.origin.overview;
                        break;

                /** Qualifications **/
                    case 'qualifications':
                        this.$http.get('/api/v1/employee/get-info').then(function(response) {
                            /** Employee Info **/
                            this.origin.qualifications.have_transportation = response.data.data.user_data.have_transportation;
                            this.origin.qualifications.willing_travel = response.data.data.user_data.willing_travel;
                            this.origin.qualifications.smoker = response.data.data.user_data.smoker;
                            this.origin.qualifications.own_children = response.data.data.user_data.own_children;
                            this.origin.qualifications.sick_children = response.data.data.user_data.sick_children;
                            this.origin.qualifications.comfortable_with_pets = response.data.data.user_data.comfortable_with_pets;

                            if (!_.isEmpty(response.data.data.user_data.qualification)) {
                                this.origin.qualifications.qualification = response.data.data.user_data.qualification;
                            }

                            if (!_.isEmpty(response.data.data.user_data.care_type)) {
                                this.origin.qualifications.care_type = response.data.data.user_data.care_type;
                            }
                            if (!_.isEmpty(response.data.data.user_data.child_age_range)) {
                                this.origin.qualifications.child_age = response.data.data.user_data.child_age_range;
                            }

                            this.qualifications.have_transportation = this.origin.qualifications.have_transportation;
                            this.qualifications.willing_travel = this.origin.qualifications.willing_travel;
                            this.qualifications.smoker = this.origin.qualifications.smoker;
                            this.qualifications.own_children = this.origin.qualifications.own_children;
                            this.qualifications.sick_children = this.origin.qualifications.sick_children;
                            this.qualifications.comfortable_with_pets = this.origin.qualifications.comfortable_with_pets;
                            this.qualifications.qualification.selected = this.origin.qualifications.qualification;
                            this.qualifications.care_type.selected = this.origin.qualifications.care_type;
                            this.qualifications.child_age.selected = this.origin.qualifications.child_age;
                        }).bind(this);

                        break;

                /** Skills **/
                    case 'skills':
                        this.skills.text = '';

                        if (!_.isEmpty(this.origin.educations)) {
                            for (var i in this.origin.skills) {
                                jQuery('#skills').tagit("createTag", this.origin.skills[i]);
                            }
                            this.skills.text = this.origin.skills.join(',');
                        }
                        break;

                /** Educations **/
                    case 'educations':
                        this.educations.list = [];
                        this.languages.list = [];

                        if (!_.isEmpty(this.origin.educations)) {
                            for (var i in this.origin.educations) {
                                var data = {
                                    school: {
                                        text: this.origin.educations[i].school.text,
                                        errors: {
                                            message: ''
                                        }
                                    },
                                    degree: {
                                        text: this.origin.educations[i].degree.text,
                                        errors: {
                                            message: ''
                                        }
                                    },
                                    area_study: {
                                        text: this.origin.educations[i].area_study.text,
                                        errors: {
                                            message: ''
                                        }
                                    },
                                    dates_attended_from: {
                                        selected: this.origin.educations[i].dates_attended_from.selected,
                                        errors: {
                                            message: ''
                                        }
                                    },
                                    dates_attended_to: {
                                        selected: this.origin.educations[i].dates_attended_to.selected,
                                        errors: {
                                            message: ''
                                        }
                                    }
                                };

                                this.educations.list.push(data);
                            }
                        }

                        if (!_.isEmpty(this.origin.languages)) {
                            for (var i in this.origin.languages) {
                                var data = {
                                    name: this.origin.languages[i].name,
                                    errors: {
                                        message: ''
                                    }
                                };
                                this.languages.list.push(data);
                            }
                        }
                        break;

                    case 'experience':
                        this.experience.selected = this.origin.experience;

                        for (var e in this.selects.years_experience.items) {
                            if (this.selects.years_experience.items[e].value == this.experience.selected) {
                                this.selects.years_experience.items[e].is_selected = true;
                            }
                        }

                        break;
                }
            },
            removeEducation: function(key) {
                this.educations.list.splice(key, 1);
            },
            addEducation: function() {
                if (this.educations.list.length <  3) {
                    this.educations.list.push({
                        id: '',
                        school: {
                            text: '',
                            errors: {
                                message: ''
                            }
                        },
                        degree: {
                            text: '',
                            errors: {
                                message: ''
                            }
                        },
                        area_study: {
                            text: '',
                            errors: {
                                message: ''
                            }
                        },
                        dates_attended_from: {
                            selected: '2015',
                            errors: {
                                message: ''
                            }
                        },
                        dates_attended_to: {
                            selected: '2016',
                            errors: {
                                message: ''
                            }
                        }
                    });
                }
            },

            addLanguage: function() {
                if (this.languages.list.length <  3) {
                    this.languages.list.push({
                        name: '',
                        errors: {
                            message: ''
                        }
                    });
                }
            },
            removeLanguage: function(key) {
                this.languages.list.splice(key, 1);
            },

            setEdit: function() {
                if (this.editMode) {
                    this.editMode = false;
                } else {
                    this.editMode = true;
                }
            },
            
            setAvailable: function(set) {
                this.available.value = set;
            },

            willingTravelSelectedValue: function (value) {
                this.qualifications.willing_travel = value;
            },

            handleErrorAvailability: function(response) {
                if (response.status === 422) {
                    this.available.errors.message = typeof response.data.available != 'undefined' ? response.data.available[0] : '';
                    this.expect_ready.errors.message = typeof response.data.expect_ready != 'undefined' ? response.data.expect_ready[0] : '';
                    this.availability_days.error = typeof response.data.availability_days != 'undefined' ? response.data.availability_days[0] : '';
                }
            },

            handleErrorEducation: function(response) {
                if (response.status === 422) {
                    var _this = this.educations;
                    _.forEach(response.data.educations, function(value, key) {
                        _this.list[key].school.errors.message = typeof value.school != 'undefined' ? value.school : '';
                        _this.list[key].degree.errors.message = typeof value.degree != 'undefined' ? value.degree : '';
                        _this.list[key].area_study.errors.message = typeof value.area_study != 'undefined' ? value.area_study : '';
                        _this.list[key].dates_attended_from.errors.message = typeof value.dates_attended_from != 'undefined' ? value.dates_attended_from : '';
                        _this.list[key].dates_attended_to.errors.message = typeof value.dates_attended_to != 'undefined' ? value.dates_attended_to : '';
                    });
                    this.educations = _this;

                    var _this = this.languages;
                    _.forEach(response.data.languages, function(value, key) {
                        _this.list[key].errors.message = typeof value.name != 'undefined' ? value.name : '';
                    });
                    this.languages = _this;
                }
            },

            handleErrorHourlyRate: function(response) {
                if (response.status === 422) {
                    this.min_rate.errors.message = typeof response.data.min_rate != 'undefined' ? response.data.min_rate[0] : '';
                    this.max_rate.errors.message = typeof response.data.max_rate != 'undefined' ? response.data.max_rate[0] : '';
                }
            },

            handleErrorNameTitle: function(response) {
                if (response.status === 422) {
                    this.title.errors.message = typeof response.data.title != 'undefined' ? response.data.title[0] : '';
                    this.gender.errors.message = typeof response.data.gender != 'undefined' ? response.data.gender[0] : '';
                    this.birthday.errors.message = typeof response.data.birthday != 'undefined' ? response.data.birthday[0] : '';
                    this.first_name.errors.message = typeof response.data.first_name != 'undefined' ? response.data.first_name[0] : '';
                    this.last_name.errors.message = typeof response.data.last_name != 'undefined' ? response.data.last_name[0] : '';
                }
            },

            handleErrorOverview: function(response) {
                if (response.status === 422) {
                    this.overview.errors.message = typeof response.data.overview != 'undefined' ? response.data.overview[0] : '';
                }
            },

            handleErrorSkills: function(response) {
                if (response.status === 422) {
                    this.skills.errors.message = typeof response.data.skills != 'undefined' ? response.data.skills[0] : '';
                }
            },

            handleErrorExperience: function(response) {
                if (response.status === 422) {
                    this.experience.errors.message = typeof response.data.experience != 'undefined' ? response.data.experience[0] : '';
                }
            },

            handleErrorQualifications: function(response) {
                if (response.status === 422) {
                    for (var q in this.qualifications_errors) {
                        this.qualifications_errors[q] = typeof response.data[q] != 'undefined' ? response.data[q][0] : '';
                    }
                }
            },
            
            setRateMax: function() {
                if (this.max_rate.text.length > 6) {
                    this.max_rate.text = this.max_rate.text.substr(0, 6);
                }

                if (Number(this.max_rate.text)) {
                    this.max_rate.errors.message = "";
                    var max_rate_commisions = this.max_rate.text-((this.max_rate.text/100)*11);
                    this.max_rate.commisons = max_rate_commisions;    
                    $(".com-rate-max").html(Math.ceil(max_rate_commisions*100)/100);           
                } else {
                    this.max_rate.errors.message = "The hourly rate is invalid."
                }
            },

            setRateMin: function() {
                if (this.min_rate.text.length > 6) {
                    this.min_rate.text = this.min_rate.text.substr(0, 6);
                }

                if(this.min_rate.text.length === 0) {
                    this.min_rate.errors.message = "Minimum rate is required field!";
                }

                if (Number(this.min_rate.text)) {
                    if(parseInt(this.min_rate.text) < 25) {
                        this.min_rate.errors.message = "Minimum hourly rate is $25/hour";
                        return;
                    }
                    this.min_rate.errors.message = "";
                     var min_rate_commisions = this.min_rate.text-((this.min_rate.text/100)*11);
                    this.min_rate.commisons = min_rate_commisions; 
                    $(".com-rate-min").html(Math.ceil(min_rate_commisions*100)/100);
                } else {
                    this.min_rate.errors.message = "The hourly rate is invalid."
                }
            },
            openFeedback: function (key) {
                if (this.job_history[key].feedback.rating) {
                    this.view_feedback = this.job_history[key];

                    $('#view-feedback-modal').modal('show');
                }
            },
            showFeedbacks: function () {
                var show = 5;
                var hide = true;
                for (var i in this.job_history) {
                    if (show && !this.job_history[i].show) {
                        this.job_history[i].show = true;
                        show--;
                    }

                    if (!this.job_history[i].show) {
                        hide = false;
                    }
                }

                if (hide) {
                    this.load_more = false;
                }
            }
        },
        computed: {
            filePath() {
                return this.profile_picture.replace(/^.*\\/, "");
            },            
            editButton: function() {
                if(this.editMode) {
                    return "DONE EDITING";
                } else {
                    return "EDIT PROFILE";
                }
            },
            getAvailable: function() {
                return this.available;
            },
            formDataAvailability: function() {
                return {
                    expect_ready: this.expect_ready.text,
                    available_value: this.available.value,
                    available_selected: this.available.selected,
                    availability_days: this.availability_days.selected
                }
            },
            formDataHourlyRate: function() {
                return {
                    min_rate: this.min_rate.text,
                    max_rate: this.max_rate.text
                }
            },
            formDataNameTitle: function() {
                return {
                    picture: this.picture.src,
                    short_name: this.display_name,
                    title: this.title.text,
                    gender: this.gender.text,
                    birthday: this.birthday.text,
                    first_name: this.first_name.text,
                    last_name: this.last_name.text
                }
            },
            formDataExperience: function() { 
                return {
                    experience: this.experience.selected
                }
            },
            formDataOverview: function() {
                return {
                    overview: this.overview.text
                }
            },
            formDataQualifications: function() {
                return {
                    have_transportation: (typeof this.qualifications.have_transportation != 'boolean') ? this.qualifications.have_transportation : '',
                    willing_travel: (typeof this.qualifications.willing_travel != 'boolean') ? this.qualifications.willing_travel : '',
                    smoker: (typeof this.qualifications.smoker != 'boolean') ? this.qualifications.smoker : '',
                    own_children: (typeof this.qualifications.own_children != 'boolean') ? this.qualifications.own_children : '',
                    sick_children: (typeof this.qualifications.sick_children != 'boolean') ? this.qualifications.sick_children : '',
                    comfortable_with_pets: (typeof this.qualifications.comfortable_with_pets != 'boolean') ? this.qualifications.comfortable_with_pets : '',
                    qualifications: (typeof this.qualifications.qualification.selected != 'boolean') ? this.qualifications.qualification.selected : [1],
                    care_type: (typeof this.qualifications.care_type.selected != 'boolean') ? this.qualifications.care_type.selected : '',
                    child_age: (typeof this.qualifications.child_age.selected != 'boolean') ? this.qualifications.child_age.selected : '',
                }
            },
            formDataSkills: function() {
                return {
                    skills: this.skills.text
                }
            },
            formDataEducation: function() {
                return {
                    educations: this.educations.list,
                    languages_: this.languages.list
                }
            },
            formDataCertificates: function(){
                return {
                    id: this.certificates.id,
                    name: this.certificates.name.text,
                    provider: this.certificates.provider.text,
                    description: this.certificates.description.text,
                    date_earned: this.certificates.date_earned.text,
                    submission: this.certificates.submission.text,
                }
            },
            getAvailabilityDays: function () {
                if (_.isEmpty(this.origin.availability_days)) {
                    return false;
                }
                
                var data = [];
                
                for (var available in this.selects.availability.items) {
                    for (var days in this.origin.availability_days) {
                        if (this.selects.availability.items[available].value == this.origin.availability_days[days]) {
                            data.push(this.selects.availability.items[available].text);
                        }
                    }
                }
                
                return data.join(', ');
            }
        },
        ready: function () {

            this.$http.get('/api/v1/client/caregivers/find')
            .then(function(response) {
                for (var d in response.data) {
                    for (var i in response.data[d].items) {
                        response.data[d].items[i].is_selected = false;
                    }
                }
                this.selects = response.data;

                this.$http.get('/api/v1/employee/get-info').then(function(response) {
                    /** Employee Info **/
                    this.origin.available.value = Boolean(parseInt(response.data.data.employee_info.available));
                    this.origin.available.selected = response.data.data.user_data.employment_type[0];
                    this.origin.availability_days = response.data.data.user_data.availability_days;
                    this.origin.expect_ready = response.data.data.employee_info.expect_ready;
                    this.origin.display_name = Boolean(parseInt(response.data.data.employee_info.short_name));
                    this.origin.title = response.data.data.employee_info.title;
                    this.origin.experience = response.data.data.user_data.experience;
                    this.origin.birthday = response.data.data.employee_info.birthday;
                    this.origin.gender = response.data.data.user_data.gender;
                    this.origin.min_rate = response.data.data.employee_info.min_rate;
                    //this.min_rate = response.data.data.employee_info.min_rate;
                    this.origin.max_rate = response.data.data.employee_info.max_rate;
                    //this.max_rate = response.data.data.employee_info.max_rate;
                    this.origin.overview = response.data.data.employee_info.overview;
                    this.origin.first_name = response.data.data.user_data.first_name;
                    this.origin.last_name = response.data.data.user_data.last_name;
                    this.origin.last_name_view = response.data.data.user_data.last_name_view;
                    this.job_history = response.data.data.job_history;
                    this.employee_rating = response.data.data.employee_rating;
                    this.picture = response.data.data.user_data.photo;

                    var max_rate_commisions = (this.origin.max_rate-((this.origin.max_rate/100))*11).toFixed(2);
                    // this.origin.max_rate.commisons = max_rate_commisions; 
                    jQuery(".com-rate-max").html(max_rate_commisions); 
                    var min_rate_commisions = (this.origin.min_rate-((this.origin.min_rate/100))*11).toFixed(2);
                    // this.origin.min_rate.commisons = min_rate_commisions;  
                    jQuery(".com-rate-min").html(min_rate_commisions); 

                    this.showFeedbacks();


                    this.setProfilePicture();


                    /** Skills **/
                    if (!_.isEmpty(response.data.data.skills)) {
                        for (var data in response.data.data.skills) {
                            this.origin.skills.push(response.data.data.skills[data].skill_name);
                        }
                    }

                    /** Education **/
                    if (!_.isEmpty(response.data.data.educations)) {
                        for (var i in response.data.data.educations) {
                            var data = {
                                school: {
                                    text: response.data.data.educations[i].school,
                                    errors: {
                                        message: ''
                                    }
                                },
                                degree: {
                                    text: response.data.data.educations[i].degree,
                                    errors: {
                                        message: ''
                                    }
                                },
                                area_study: {
                                    text: response.data.data.educations[i].area_study,
                                    errors: {
                                        message: ''
                                    }
                                },
                                dates_attended_from: {
                                    selected: response.data.data.educations[i].dates_attended_from,
                                    errors: {
                                        message: ''
                                    }
                                },
                                dates_attended_to: {
                                    selected: response.data.data.educations[i].dates_attended_to,
                                    errors: {
                                        message: ''
                                    }
                                },
                                desc: {
                                    text: response.data.data.educations[i].description
                                },
                                status: response.data.data.educations[i].status
                            };
                            
                            this.origin.educations.push(data);
                        }
                    }

                    /** Languages **/
                    if (!_.isEmpty(response.data.data.languages)) {
                        for (var i in response.data.data.languages) {
                            var data = {
                                name: response.data.data.languages[i].name,
                                errors: {
                                    message: ''
                                }
                            };
                            this.origin.languages.push(data);
                        }
                    }

                    /*certificates*/
                    if (response.data.data.certificates.length>0) {
                        var certificates = response.data.data.certificates[0];
                        this.certificates.id = certificates.id;
                        this.certificates.name.text = certificates.name;
                        this.certificates.provider.text = certificates.provider;
                        this.certificates.description.text = certificates.description;
                        this.certificates.date_earned.text = certificates.date_earned;
                        this.certificates.submission.text = certificates.submission;
                    }

                    this.cancelEdit('availability');
                    this.cancelEdit('hourly-rate');
                    this.cancelEdit('name-title');
                    this.cancelEdit('overview');
                    this.cancelEdit('qualifications');
                    this.cancelEdit('skills');
                    this.cancelEdit('educations');
                    this.cancelEdit('experience');
                }).bind(this);
            })
            .catch(function(response) { console.log(['Error!', response]); })
            .finally(function(response) {})
            .bind(this);

            var _this = this;
            jQuery('[name="min_rate"]').blur(function(){
                _this.min_rate.text = parseFloat(_this.min_rate.text);

                if (_this.min_rate.text > 1000) {
                    _this.min_rate.text = 999;
                }

                _this.min_rate.text = _this.min_rate.text.toFixed(2);

                if (Number(_this.min_rate.text)) {
                    _this.min_rate.errors.message = "";
                } else {
                    _this.min_rate.text = "";
                    _this.min_rate.errors.message = "";
                }
            });
            //jQuery('[name="min_rate"]').trigger('blur');
            jQuery('[name="max_rate"]').blur(function(){
                _this.max_rate.text = parseFloat(_this.max_rate.text);

                if (_this.max_rate.text > 1000) {
                    _this.max_rate.text = 999;
                }

                _this.max_rate.text = _this.max_rate.text.toFixed(2);

                if (Number(_this.max_rate.text)) {
                    _this.max_rate.errors.message = "";
                } else {
                    _this.max_rate.text = "0.00";
                    _this.max_rate.errors.message = "The max rate is invalid.";
                }
            });
            //jQuery('[name="max_rate"]').trigger('blur');
        },
        watch: {
            'languages.list': function () {
                this.setAutocomplete();
            }
        }
    });
}
