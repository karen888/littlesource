import Vue from 'vue';
import _ from 'lodash';
import CustomSelectExt from './components/CustomSelectExtended.vue';
import StarRating from './components/StarRating.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('employee-find-jobs')) {
    var vm = new Vue({
        el: '#employee-find-jobs',
        data: {
            jobs: [],
            checkedCategory: [],
            category: {
                selected: [],
                items: []
            },

            selected: '0',
            options: [
                     { text: 'Select Something :)', value: '0' },
                    { text: 'Select Open', value: '1' },
                    { text: 'Slect Close', value: '2' }
                     ],
            selects: {},
            advanced: false,
            advanced_options: {
                all_words: '',
                any_words: '',
                title: '',
                select: false
            },
            filter: {
                category: {
                    items: [],
                    selected: []
                },
                child_age: {
                    items: [],
                    selected: []
                },
                job_duration: {
                    items: [],
                    selected: []
                },
                job_workloads: {
                    items: [],
                    selected: []
                },
                qualification: {
                    items: [],
                    selected: []
                },
                location_range: {
                    items: [],
                    selected: []
                },
                sick_children: {
                    items: [],
                    selected: []
                },
                pets: {
                    items: [],
                    selected: []
                },
                age_range: {
                    items: [],
                    selected: []
                },
                gender: {
                    items: [],
                    selected: []
                },
                own_children: {
                    items: [],
                    selected: []
                },
                drivers_licence: {
                    items: [],
                    selected: []
                },
                smoker: {
                    items: [],
                    selected: []
                }
            },
            qualifications_options: false,
            pages: [],
            current_page: 0,
            prev_page: false,
            next_page: false,
            set_filter: false,
            set_select: false
        },

        components: {
            'custom-select': CustomSelectExt,
            'star-rating' : StarRating
        },

        ready: function() {
            this.getPost()
            this.GetSavedJobs();

            this.$http.get('/api/v1/common/categories').then(function(response) {
                this.filter.category.items = response.data.data.categories_items;
            }, function(response) {
                console.log(['Error!', response]);
            }).bind(this);

            this.$http.get('/api/v1/common/qualifications').then(function(response) {
                this.filter.qualification.items = response.data.data.qualifications;
            }, function(response) {
                console.log(['Error!', response]);
            }).bind(this);

            this.$http.get('/api/v1/common/job-duration').then(function(response) {
                this.filter.job_duration.items = response.data.data.job_duration;
            }, function(response) {
                console.log(['Error!', response]);
            }).bind(this);

            this.$http.get('/api/v1/common/job-workloads').then(function(response) {
                this.filter.job_workloads.items = response.data.data.job_workload;
            }, function(response) {
                console.log(['Error!', response]);
            }).bind(this);

            this.$http.get('/api/v1/client/caregivers/find').then(function(response) {
                this.selects = response.data;

                for (var selects in response.data) {
                    switch (selects) {
                        case 'qualification':
                            break;
                        case 'care_sick':
                            this.filter.sick_children.items = response.data[selects].items;
                            break;
                        default:
                            if (typeof this.filter[selects] == 'object') {
                                this.filter[selects].items = response.data[selects].items;
                            }
                            break;
                    }
                }

                if (location.search.length) {
                    this.advanced = true;
                    var search = location.search;

                    if (search[0] == '?') {
                        var page = false;
                        search = search.replace('?', '');
                        search = search.split('&');
                        for (var s in search) {
                            var data = search[s].split('=');
                            switch (data[0]) {
                                case 'all_words':
                                    if (_.trim(data[1])) {
                                        this.advanced_options.all_words = _.trim(data[1]);
                                    }
                                    break;
                                case 'any_words':
                                    if (_.trim(data[1])) {
                                        this.advanced_options.any_words = _.trim(data[1]);
                                    }
                                    break;
                                case 'title':
                                    if (_.trim(data[1])) {
                                        this.advanced_options.title = _.trim(data[1]);
                                    }
                                    break;
                                case 'page':
                                    if (_.trim(data[1])) {
                                        page = _.trim(data[1]);
                                    }
                                    break;
                                default:
                                    if (typeof this.filter[data[0]] == 'object') {
                                        if (_.trim(data[1])) {
                                            var value = _.trim(data[1]).split(',');
                                            this.filter[data[0]].selected = value;
                                            for (var v in value) {
                                                   for (var f in this.filter[data[0]].items) {
                                                       if (this.filter[data[0]].items[f].value == value[v]) {
                                                           this.filter[data[0]].items[f].is_selected = true;
                                                       }
                                                   }
                                            }
                                        }
                                    }
                                    break;
                            }
                        }

                        this.getJobs(page);
                    }

                } else {
                    this.$http.get('/api/v1/employee/job/find-jobs').then(function(response) {
                            this.jobs = response.data.data.jobs;
                        })
                        .catch(function(response) { console.log(['Error!', response]); })
                        .bind(this);
                }

            }, function(response) {
                console.log(['Error!', response]);
            }).bind(this);

        },

        methods: {
            setAdvanced: function (search) {
                this.set_select = true;
                this.getJobs(false, search);
                this.advanced = true;

            },
            getPost:function(id)
            {
                 this.$http.get('/api/v1/employee/job/get-post', {id: id}).then(function(response) {
                        console.log("Entra");
                    })
            },
            getByCategory: function (id) {
                if(id == 'array'){
                    id = this.checkedCategory;
                }
                this.$http.get('/api/v1/employee/job/find-jobs-category', {id: id}).then(function(response) {
                        this.jobs = response.data.data.jobs;
                    })
                    .catch(function(response) { console.log(['Error!', response]); })
                    .finally(function(response) {})
                    .bind(this);
            },

            GetSavedJobs: function(){
                 this.$http.get('/employee/saved-jobs',function(data)
                 {
                    this.$set('saveJobs',data)
                 })
            },

            setQualificationsOptions: function() {
                this.qualifications_options = !this.qualifications_options;
            },
            
            getJobs: function(page, search, bywatch) {
                var setUrl = '';
                var setData = {};

                if (!this.set_filter && bywatch) {
                    return;
                }

                if (search) {
                    setData.select = Boolean(this.advanced_options.select)
                }

                for (var i in this.advanced_options) {
                    switch(i) {
                        case 'all_words':
                            if (_.trim(this.advanced_options[i])) {
                                setData.all_words = _.trim(this.advanced_options[i]);
                            }
                            break;
                        case 'any_words':
                            if (_.trim(this.advanced_options[i])) {
                                setData.any_words = _.trim(this.advanced_options[i]);
                            }
                            break;
                        case 'title':
                            if (_.trim(this.advanced_options[i])) {
                                setData.title = _.trim(this.advanced_options[i]);
                            }
                            break;
                    }
                }

                for (var f in this.filter) {
                    if (_.isArray(this.filter[f].selected)) {
                        if (!_.isEmpty(this.filter[f].selected)) {
                            setData[f] = this.filter[f].selected;
                        }
                    } else {
                        if (this.filter[f].selected && this.filter[f].selected !== true) {
                            setData[f] = this.filter[f].selected;
                        }
                    }
                }

                for (var j in setData) {
                    setUrl += j +'=' + setData[j] + '&';
                }

                if (page) {
                    setData.page = page;
                    setUrl += 'page=' + page;
                }

                window.history.pushState({}, 'Find job', '?'+setUrl);


                this.$http.post('/api/v1/employee/job/find-jobs-advanced', setData).then(function(response) {
                    var pagin = [];
                    jQuery(response.data.data.render).find('li').each(function(){
                        pagin.push(jQuery(this).text());
                    });
                    this.pages = pagin;

                    this.current_page = response.data.data.current_page;
                    this.prev_page = response.data.data.prev_page;
                    this.next_page = response.data.data.next_page;

                    if (this.set_select && !_.isEmpty(response.data.data.select)) {
                        this.set_filter = false;
                        var employee_info = response.data.data.select.employee_info[0];
                        var qualification = response.data.data.select.qualification;

                        if (qualification) {
                            this.filter.qualification.selected = qualification;
                            for (var q in qualification) {
                                for (var f in this.filter.qualification.items) {
                                    if (this.filter.qualification.items[f].value == qualification[q]) {
                                        this.filter.qualification.items[f].is_selected = true;
                                    }
                                }
                            }
                        }

                        for (var e in employee_info) {
                            if (employee_info[e] != null) {
                                switch (e) {
                                    case 'child_age_range':
                                        var child_age_range = JSON.parse(employee_info[e]);
                                        this.filter.child_age.selected = child_age_range;
                                        for (var v in child_age_range) {
                                            for (var f in this.filter.child_age.items) {
                                                if (this.filter.child_age.items[f].value == child_age_range[v]) {
                                                    this.filter.child_age.items[f].is_selected = true;
                                                }
                                            }
                                        }
                                        break;
                                    case 'comfortable_with_pets':
                                        if (employee_info[e]) {
                                            this.filter.pets.items[0].is_selected = true;
                                            this.filter.pets.selected = [this.filter.pets.items[0].value];
                                        } else {
                                            this.filter.pets.items[1].is_selected = true;
                                            this.filter.pets.selected = [this.filter.pets.items[1].value];
                                        }
                                        break;
                                    case 'gender':
                                        if (employee_info[e] == 'male') {
                                            this.filter.gender.items[0].is_selected = true;
                                            this.filter.gender.selected = [this.filter.gender.items[0].value];
                                        } else {
                                            this.filter.gender.items[1].is_selected = true;
                                            this.filter.gender.selected = [this.filter.gender.items[1].value];
                                        }
                                        break;
                                    case 'have_transportation':
                                        switch (employee_info[e]) {
                                            case 'no':
                                                this.filter.drivers_licence.items[0].is_selected = true;
                                                this.filter.drivers_licence.selected = [this.filter.drivers_licence.items[0].value];
                                                break;
                                            case 'yes_car':
                                                this.filter.drivers_licence.items[2].is_selected = true;
                                                this.filter.drivers_licence.selected = [this.filter.drivers_licence.items[2].value];
                                                break;
                                            case 'yes':
                                                this.filter.drivers_licence.items[1].is_selected = true;
                                                this.filter.drivers_licence.selected = [this.filter.drivers_licence.items[1].value];
                                                break;
                                        }
                                        break;
                                    case 'sick_children':
                                        if (employee_info[e]) {
                                            this.filter.sick_children.items[1].is_selected = true;
                                            this.filter.sick_children.selected = [this.filter.sick_children.items[1].value];
                                        } else {
                                            this.filter.sick_children.items[0].is_selected = true;
                                            this.filter.sick_children.selected = [this.filter.sick_children.items[0].value];
                                        }
                                        break;
                                    case 'smoker':
                                        if (employee_info[e]) {
                                            this.filter.smoker.items[1].is_selected = true;
                                            this.filter.smoker.selected = [this.filter.smoker.items[1].value];
                                        } else {
                                            this.filter.smoker.items[0].is_selected = true;
                                            this.filter.smoker.selected = [this.filter.smoker.items[0].value];
                                        }
                                        break;
                                }
                            }
                        }
                        this.qualifications_options = true;
                        this.set_select = false;
                        this.getJobs();
                    } else {
                        this.jobs = response.data.data.jobs;
                    }
                    this.set_select = false;
                    this.set_filter = true;

                }, function(response) {
                    console.log(['Error!', response]);
                }).bind(this);
            },
            
            setJobPage: function(page) {
                if (Number(page)) {
                    this.getJobs(page);
                }else if(page == '«') {
                    if (this.prev_page) {
                        this.getJobs((this.current_page-1));
                    }
                }else if(page == '»') {
                    if (this.next_page) {
                        this.getJobs((this.current_page+1));
                    }
                }
            },

            saveJob: function(jobId) {
                this.$http.post('/api/v1/employee/job/' + jobId + '/save-job', this.formData ).then(function(response) {
                    var saveButton = $('#save' + jobId);
                    if(saveButton.text() == "Save")
                        saveButton.text("Saved");
                    else
                        saveButton.text("Save");
                }, function(response) {
                    //console.log(response);
                }).bind(this);
            }
        },
        computed: {
            advancedOptions: function() {
                return (this.qualifications_options) ? 'Hide Qualification Options' : 'Show Qualification Options';
            },
            
            jobsIsEmpty: function() {
                return _.isEmpty(this.jobs);
            }
        },
        watch: {
            'filter.category.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.child_age.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.job_duration.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.job_workloads.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.qualification.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.location_range.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.sick_children.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.pets.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.age_range.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.gender.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.own_children.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.drivers_licence.selected': function () {
                this.getJobs(this.current_page, false, true);
            },
            'filter.smoker.selected': function () {
                this.getJobs(this.current_page, false, true);
            }
        }
    });
}
