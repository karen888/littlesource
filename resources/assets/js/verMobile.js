import Vue from 'vue';
import $ from 'jquery';
import VueMask from 'v-mask';

Vue.use(require('vue-resource'));
Vue.use(VueMask);
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if ($( "#mobile-verify" ).length) {
    var vm = new Vue({
        el: '#mobile-verify',
        data: {
           	formErrors: {
                phone: '',
                phoneValidate: '',
            },
            code: {
                text: '',
                errors: {
                    message: ''
                }
            },
            nextStep: false,
            userInfo: {
                phone: '',
                method: '',
            },
            correctCode: 1111,
            telInput: null,
            question: '',
            actionLinkText: '',
        },
        methods: {
        	goBack: function(){
        		this.nextStep = false;
        		this.code.errors.message = '';
        		this.initPhoneNumber();
        	},
        	sendCode: function (){
        		if(this.userInfo.phone.length == 0 ^ this.userInfo.phone.indexOf("_") !== -1) {
        		    this.formErrors.phone = 'Invalid entry';
        		    return;
                }

                this.userInfo.phone = this.nextStep ? this.userInfo.phone : $('#phone').intlTelInput("getNumber");

                let formData = {
                    phone: this.userInfo.phone,
                    method: this.userInfo.method,
                };

                jQuery("body").LoadingOverlay("show");

                let that = this;
                this.$http.patch('/api/v1/settings/sendSMS', formData)
                    .then( function(response) {
                        that.question = that.userInfo.method == 'sms'? 'Didn\'t reseive Code?': 'Didn\'t reseive Call?';
                        that.actionLinkText = that.userInfo.method == 'sms'? 'Resend Code': 'Call Again';

                        that.nextStep = true;
                        jQuery("body").LoadingOverlay("hide");
                    }, function(response) {
                        jQuery("body").LoadingOverlay("hide");
                        for (let key in response.data) {
                            that.formErrors[key] = response.data[key][0];
                        }

                        // on unknown error
                        if (response.status === 500) {
                            this.cancel();
                        }
                    }
                );

        	},
        	verifyCode(){
                var formData = {
                    phone: this.userInfo.phone,
                    code: this.code.text
                };
                var _this = this;
                this.$http.patch('/api/v1/settings/verifySMS', formData)
                    .then( function(response) {
                            swal(
                                'Success',
                                'You have successfully verified phone number!',
                                'success'
                            ).then(function(){
                                jQuery("body").LoadingOverlay("show");
                                window.location.href = "/user/settings/get-verified";
                            });
                        }, function(response) {
                            jQuery("body").LoadingOverlay("hide");
                            // _this.code.errors.message = 'Woops, Wrong Code';
                            swal({
                                title: 'Error',
                                text: 'You entered wrong code, resend code and/or try again',
                                type: 'error',
                                showCancelButton: true,
                                confirmButtonColor: '#36d674',
                                cancelButtonColor: '#00afdd',
                                confirmButtonText: 'Resend code',
                                cancelButtonText: 'Close'
                            }).then(function () {
                                _this.sendCode();
                            });

                            if (response.status === 500) {
                                this.cancel();
                                // this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                            }
                        }
                    );

        	},
            initPhoneNumber() {
                console.log('this.telInput: ', this.telInput);
                console.log('this.userInfo.phone: ', this.userInfo);
        	    this.telInput.intlTelInput({
                    initialCountry: "au",
                    responsiveDropdown: true,
                    preferredCountries: ['au', 'nz', 'id' , 'pg', 'tk', 'nu', 'as', 'kb', 'fp', 'gu', 'nc', 'nf', 'tv'],
                    nationalMode: true,
                    utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.13/js/utils.js"
                });

                this.telInput.intlTelInput("setNumber", this.userInfo.phone);
                jQuery("body").LoadingOverlay("hide");
            },
            reset(){
                this.formErrors.phone = '';
            },
            handleNumberBlur(){
                this.reset();
                if ($.trim(this.userInfo.phone)) {
                    if (this.telInput.intlTelInput("isValidNumber")) {
                        this.formErrors.phone = '';
                    } else {
                        this.formErrors.phone = 'Invalid entry';
                    }
                }
            }
        },
        created() {
            jQuery("body").LoadingOverlay("show");
        },
        attached(){
            this.telInput = $("#phone");
            let that = this;
            this.$http.get('/api/v1/settings/user-info').then(function(response) {
                that.userInfo.phone = response.data.phone;
                that.initPhoneNumber();
            }, function() {
                this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);
        }
    });
}