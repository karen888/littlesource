import filters from './filters';
import settings from './settings';
import myInfoSettings from './myInfoSettings';
import createJob from './createJob';
import inviteJob from './inviteJob';
import customselect from './customselect';
import HireCaregiverForm from './HireCaregiverForm';
import Applicants from './Applicants';
import AuthMenu from './AuthMenu';
import Messages from './Messages';
import Prelaunch from './Prelaunch';
import tagit from './vendor/tagit';
import acceptInviteJob from './acceptInviteJob';
import editEmployeeProfile from './editEmployeeProfile';
import viewEmployeeProfile from './viewEmployeeProfile';
import viewOffer from './viewOffer';
import findCaregivers from './findCaregivers.js';
import notificationSettings from './notificationSettings.js';
import viewJobSuccess from './viewJobSuccess.js';
import login from './login.js';
import registr from './registr.js';
import emplocontract from './emplocontract.js';
import findJobsEmployee from './findJobsEmployee.js';
import getContract from './getContract.js';
import allJobs from './allJobs.js';
import submitProposal from './submitProposal.js';
import endContract from './endContract.js';
import viewContract from './viewContract.js';
import myCaregivers from './myCaregivers.js';
import verMobile from './verMobile.js';
import verQualification from './verQualification.js';
import verified from './verified.js';
import wwcc from './verWWCC.js';
import verAdress from './verAddress.js';
import verCertificates from './verCertificates.js';
import verSocialLinks from './verSocialLinks.js';
import verAbn from './verAbn.js';
import verID from './verId.js';
import Inputmask from 'inputmask';


function round (value, precision, mode) {
    //  discuss at: http://locutus.io/php/round/
    // original by: Philip Peterson
    //  revised by: Onno Marsman (https://twitter.com/onnomarsman)
    //  revised by: T.Wild
    //  revised by: Rafał Kukawski (http://blog.kukawski.pl)
    //    input by: Greenseed
    //    input by: meo
    //    input by: William
    //    input by: Josep Sanz (http://www.ws3.es/)
    // bugfixed by: Brett Zamir (http://brett-zamir.me)
    //      note 1: Great work. Ideas for improvement:
    //      note 1: - code more compliant with developer guidelines
    //      note 1: - for implementing PHP constant arguments look at
    //      note 1: the pathinfo() function, it offers the greatest
    //      note 1: flexibility & compatibility possible
    //   example 1: round(1241757, -3)
    //   returns 1: 1242000
    //   example 2: round(3.6)
    //   returns 2: 4
    //   example 3: round(2.835, 2)
    //   returns 3: 2.84
    //   example 4: round(1.1749999999999, 2)
    //   returns 4: 1.17
    //   example 5: round(58551.799999999996, 2)
    //   returns 5: 58551.8
    var m, f, isHalf, sgn // helper variables
    // making sure precision is integer
    precision |= 0
    m = Math.pow(10, precision)
    value *= m
    // sign of the number
    sgn = (value > 0) | -(value < 0)
    isHalf = value % 1 === 0.5 * sgn
    f = Math.floor(value)
    if (isHalf) {
        switch (mode) {
            case 'PHP_ROUND_HALF_DOWN':
                // rounds .5 toward zero
                value = f + (sgn < 0)
                break
            case 'PHP_ROUND_HALF_EVEN':
                // rouds .5 towards the next even integer
                value = f + (f % 2 * sgn)
                break
            case 'PHP_ROUND_HALF_ODD':
                // rounds .5 towards the next odd integer
                value = f + !(f % 2)
                break
            default:
                // rounds .5 away from zero
                value = f + (sgn > 0)
        }
    }
    return (isHalf ? value : Math.round(value)) / m
}

$(document).ready(function(){
    customselect.createActionGroups();
    filters();

    window.onbeforeunload = function(e) {
        //jQuery("body").LoadingOverlay("show");
    };

    /*Inputmask().mask(document.querySelectorAll("input"), {

    });*/

    window.maskInputDates = function() {
        $('input.nofuture').inputmask("date", {
            "oncomplete": function () {
                var entered = $(this).val();
                var now = moment();
                entered = moment(entered, 'DD/MM/YYYY');

                if (entered.unix() > now.unix()) {
                    for (var i = 0; i < 10; i++) {
                        $(this).val('');
                    }

                    setTimeout(function () {
                        swal({
                            type: 'warning',
                            text: 'Date must not be in future!'
                        });
                    }, 50);
                }

            },
            "onincomplete": function () {
                $(this).val('')
            }
        });


        $('input.nopast').inputmask("date", {
            "oncomplete": function () {


                var entered = $(this).val();
                var now = moment();
                entered = moment(entered, 'DD/MM/YYYY');

                if (entered.unix() < now.unix()) {
                    for (var i = 0; i < 10; i++) {
                        $(this).val('');
                    }

                    setTimeout(function () {
                        swal({
                            type: 'warning',
                            text: 'Date must not be in past!'
                        });
                    }, 50);


                }

            },
            "onincomplete": function () {
                $(this).val('')
            }
        });
    };

    window.maskInputDates();


    var fbi = window.setInterval(() => {
        //console.log('Iteration');
        jQuery(".fancybox").fancybox();


        // if (jQuery('.fancybox').length) {
        //     window.clearInterval(fbi);
        //     console.log('Done')
        // }

        $('[data-toggle="popover-hover"]').popover({
            trigger: 'hover click focus'

        });

        $('[data-toggle="tooltip"]').tooltip();
    }, 200);




    var l = $('#conditional').val();
  
    if(l=="Active")
    {
       $("#close_job").show();
    }
    if(l=="Closed")
    {
        $("#repost_job").show();
    }

    $("#skills").tagit({
        allowDuplicates: false,
        allowSpaces: false,
        removeConfirmation: true,
        itemName: 'item',
        fieldName: 'tags',
        singleField: true,
        singleFieldNode: document.getElementById('tags_input')
    });
    if (document.getElementById('become_caregiver')) {
        jQuery('#become_caregiver').off('click').on('click', function(){
            window.location = '/auth/register?role=caregiver';
        })
    }
    if(document.getElementById('skills')) {
        $("#skills").data("ui-tagit").tagInput.focus(function() {
            $('#skills').css('border-color', 'hsl(190, 72%, 48%)');
        }).focusout(function() {
            $('#skills').css('border-color', 'hsl(0, 0%, 95%)');
        });
        $('#skills').on('click.tagit', function() {
            if (!_.isEmpty($('#skills').attr('data-skills'))) {
                var tagit = $('#skills').attr('data-skills').split(',');
                for (var lskill in tagit) {
                    $('#skills').tagit("createTag", tagit[lskill]);
                }
                $('#skills').attr('data-skills', '');
            }
        });
    }

    if(document.getElementById('close_job')) {
        $('#close_job').on('click', function() {
            var url = $('#close_job').data('url')
            jQuery.ajax({
                type: 'get',
                url: url,
                success: function(resp) {
                   
                         $("#close_job").hide();
                         $("#repost_job").show();
                    
                }
            });
        });
    }

    if(document.getElementById('repost_job')) {
        $('#repost_job').on('click', function() {
            var url = $('#repost_job').data('url')
            jQuery.ajax({
                type: 'get',
                url: url,
                success: function(resp) {
                         $("#repost_job").hide();
                         $("#close_job").show();
                }
            });
        });
    }


        if(document.getElementById('make_job_public')) {
        $('#make_job_public').on('click', function() {
            var url = $('#make_job_public').data('url')
            jQuery.ajax({
                type: 'get',
                url: url,
                success: function(resp) {
                    if (resp.success) {
                        jQuery('#make_job_public').remove();
                    }
                }
            });
        });
    }

    // style all inputs the same.
    var inputs = document.querySelectorAll( '.inputfile' );
    Array.prototype.forEach.call( inputs, function( input )
    {
        var label    = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener( 'change', function( e )
        {
            var fileName = '';
            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName )
                $('.fileName').innerHTML = fileName;
                // label.querySelector( 'span' ).innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
        input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
    });
});
