import Vue from 'vue';
import Password from './components/Password.vue';
import SecurityQuestion from './components/SecurityQuestion.vue';
import Notifications from './components/Notifications.vue';
import customselect from './customselect';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;
/*
    For password and security Settings
 */
if (document.getElementById('password-and-security')) {
    var vm = new Vue({
        el: '#password-and-security',
        data: {
            displayNotification: false,
            notificationValues: null,
            displayForm : {
                Password: false,
                SecurityQuestion: false,
                SecurityEmail: false,
            }
        },
        components: {
            'password': Password,
            'notifications': Notifications,
            'security-question': SecurityQuestion
        },
        methods: {
            handleNotification: function(notification) {
                this.notificationValues = notification;
                this.displayNotification = !notification.close;
            },
            currentOpenForm: function(form) {
                this.$broadcast('clear-form');
                for (var key in this.displayForm) {
                    if (key == Object.keys(form)[0] ) {
                        this.displayForm[key] = form[Object.keys(form)[0]];
                    } else {
                        this.displayForm[key] = !form[Object.keys(form)[0]];
                    }
                }
            },
            closeAllForms: function() {
                this.$broadcast('clear-form');
                for (var key in this.displayForm) {
                    this.displayForm[key] = false;
                }
            }
        },
        ready: function(){
            setInterval(function(){
                $('#password').pwstrength();
            }, 100);
        }
    });

    //  this loads the customselect input to work.
    vm.$watch('displayForm.SecurityQuestion == true', function () {
        customselect.custom_select();
    })

    vm.$watch('displayForm.Password == true', function(){
        setTimeout(function(){
            $('#password').pwstrength();
        }, 100);
    });
}
