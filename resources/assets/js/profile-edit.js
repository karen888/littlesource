import ProfileEditBase from './components/Profile/Edit/Base.vue';
import Page from './components/Profile/Edit/Page.vue';
import Verification from './components/Profile/Edit/Verification.vue';

window.VueRouter = require('vue-router');
Vue.use(VueRouter);

let router = new VueRouter({history:true});

router.map({
    '/profile/build/:step': {name: 'stepper', component: Page,},
    '/profile/verification': {name: 'verification', component: Verification,},
});

router.start(ProfileEditBase, '#profile-edit-base');