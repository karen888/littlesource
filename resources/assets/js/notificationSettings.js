import Vue from 'vue';
import CustomSelect from './components/CustomSelect.vue';
import Notifications from './components/Notifications.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('notification-settings')) {
    var vm = new Vue({
        el: '#notification-settings',
        data: {
            displayNotification: false,
            notificationValues: null,
            data: {}
        },
        components: {
            'notifications': Notifications,
            'custom-select': CustomSelect
        },
        methods: {
            handleNotification: function (notification) {
                this.notificationValues = notification;
                this.displayNotification = !notification.close;
            },
            saveNotificationSettings: function () {
                this.$http.patch('/api/v1/settings/notifications', this.data).then(function() {
                    this.handleNotification({type: 'success', 'message': 'Notification Settings Saved', 'close': false});
                    $('html, body').animate({
                        scrollTop: $("#notification-settings").offset().top
                    }, 500);
                }, function() {
                    this.handleNotification({type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                }).bind(this);
            }
        },
        ready: function () {
            this.$http.get('/api/v1/settings/notifications').then(function(response) {
                this.data = response.data;
            }, function() {
                this.handleNotification({type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);
        }
    });
}
