import Vue from 'vue'
import VueResource from 'vue-resource'
import Notifications from './components/Notifications.vue'
import CustomSelect from './components/CustomSelect.vue'
import ApplicantList from './components/Applicants/List.vue'
import ApplicantStore from './components/Applicants/store'

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

/*
    For Applicants in a job
 */
if (document.getElementById('applicants')) {

     var vm = new Vue({
        el: '#applicants',
        data: {
            applicants: 0,
            shortlisted: 0,
            hidden: 0,
            messaged: 0,
            currentList: [],
            applicantsActive: false,
            shortlistedActive: false,
            messagedActive: false,
            hiddenActive: false,
            currentRoute: 'applicants',
            notificationValues: '',
            displayNotification: false,
            filters: [{'value' : 1, 'text' : 'Name', 'is_selected' : false} , {'value' : 2, 'text': 'Best Match', 'is_selected': true}],
            filterSelected: 1
        },
        components: {
            'custom-select': CustomSelect,
            'applicant-list': ApplicantList,
            'notifications': Notifications
        },
        methods: {
            setActive: function(filter) {
                this.applicantsActive = false;
                this.shortlistedActive = false;
                this.messagedActive = false;
                this.hiddenActive = false;
                this.currentRoute = filter;

                switch (filter) {
                    case 'shortlisted':
                        this.shortlistedActive = true;
                        this.currentList = ApplicantStore.getShortlistedApplicants();
                        break;
                    case 'hidden':
                        this.hiddenActive = true;
                        this.currentList = ApplicantStore.getHiddenApplicants();
                        break;
                    case 'messaged':
                        this.messagedActive = true;
                        this.currentList = ApplicantStore.getMessagedApplicants();
                        break;
                    case 'applicants':
                    default:
                        this.applicantsActive = true;
                        this.currentList = ApplicantStore.getApplicants();
                        break;
                }
            },
            handleFilter: function(option) {
                switch(option.value) {
                    case 2:
                        this.filterByBestMatch();
                        break;
                    case 1:
                    default:
                        this.filterByName();
                        break;
                }
            },
            filterByBestMatch: function() {
                this.updateData('best-match');
            },

            filterByName: function() {
                this.updateData('name');
            },
            handleNotification: function(notification) {
                this.notificationValues = notification;
                this.displayNotification = !notification.close;
            },

            handleShortlist: function(data) {
                this.updateApplicantStatus('shortlist', data);
            },

            handleHidden: function(data) {
                this.updateApplicantStatus('hide', data);
            },

            updateApplicantStatus: function(status, data) {
                var applicantsUrl = '/api/v1/applicants/' + status + '/' + data.applicant.id;
                this.$http.get(applicantsUrl)
                .then( function(response) {
                    this.updateData();
                    this.handleNotification({type: response.data.notification.type, 'message': response.data.notification.message, 'close': false});
                }, function(response) {
                    // on unknown error
                    if (response.status === 500) {
                        this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                    }
                });
            },

            updateData(filters = 'name') {
                var applicantsUrl = '/api/v1/applicants/' + jobId;
                this.$http.get(applicantsUrl, { filters: filters })
                .then( function(response) {
                    ApplicantStore.setApplicantsForJob(response.data);
                    this.applicants = ApplicantStore.getApplicants().length;
                    this.shortlisted = ApplicantStore.getShortlistedApplicants().length;
                    this.hidden = ApplicantStore.getHiddenApplicants().length;
                    this.messaged = ApplicantStore.getMessagedApplicants().length;
                    this.setActive(this.currentRoute);
                }, function(response) {
                    // on unknown error
                    if (response.status === 500) {
                        this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                    }
                });
            }
        },
        created: function() {
            this.updateData();
        }
    })
}