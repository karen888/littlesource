import Vue from 'vue';
import $ from 'jquery';
import Pikaday from 'pikaday';
import cropper from 'cropper';
import CustomSelect from './components/CustomSelectStringArray.vue';
import FileUploader from './components/FileUploader.vue';

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': Vue.http.headers.common['X-CSRF-TOKEN']}
});

if (document.getElementById('verifed-id')) {
    var image;
    var vm = new Vue({
        el: '#verifed-id',
        components: {
            CustomSelect,
            FileUploader,
        },
        data: {
            fileUploadFormData: new FormData(),
            form: {
                first_name:{
                    error: '',
                    text: ''
                },
                last_name:{
                    error: '',
                    text: ''
                },
                birthday:{
                    error: '',
                    text: ''
                },
                prof_id_type:{
                    error: '',
                    text: '',
                    types: [
                        'Passport',
                        'Driver License',
                        'Government Issued Proof of Age Card ',
                        'WWCC',
                        'WWVP Card',
                        'Public Service Employee ID card',
                        'Armed Forces Identity Card',
                        'Other',
                    ]
                },
                prof_id_custom_type: {
                    error:'',
                    text:''
                },
                prof_id_sc:{
                    error: '',
                    text: '',
                },
                prof_dateof:{
                    error: '',
                    text: ''
                },
                notau:{
                    checked: false
                },
                prof_scan_id:{
                    error: '',
                    loading:false,
                },
                prof_scan_id2:{
                    error: '',
                    loading:false,
                },
                notes: {
                    text: '',
                    error: '',
                }

            },
            user_photo_100: '/img/no_photo_100.jpg',
            profilePhotoResize: '/img/no_photo_100.jpg',
            profilePhotoPreviev: '/img/no_photo_100.jpg',
            profilePhotoText: 'Please upload a professional portrait clearly shows your face',
            profilePhotoSelectText: "+ Add photo",
            image: '',
            wwcc_scan:'',
            wwcc_scan2:'',
            work_visa: '',
            work_visa_type: 'image',
            wwcc_scan_type:'',
            wwcc_scan2_type:'',
            photoSelected: false,
            noPhotoSubmitted: false,
            photo_error: '',
            loading: {
                scan1: false,
                scan2: false,
                work_visa: false,
            },
            add_photo: false,
            user_photo_100_temp: '',
        },
        methods: {
            onFileUploaded: function(prop_name, remote_file) {
                this[prop_name] = remote_file;
            },
            onUserPhotoploaded: function(user_photo, remote_file) {
                this[user_photo] = remote_file;
                // this.image_crop_interval = window.setInterval(() => {
                //     $('.cropper-container .cropper-canvas img').attr('src', this.user_photo_100_temp);
                //     $('.cropper-container .cropper-view-box img').attr('src', this.user_photo_100_temp);
                //     this.cropImage();
                //     this.photoSelected = true;
                //     this.noPhotoSubmitted = false;
                //     clearInterval(this.image_crop_interval);
                // });
                this.cropImage();
                this.photoSelected = true;
                this.noPhotoSubmitted = false;
            },
            addPhoto () {
                this.user_photo_100_temp = '';
                this.add_photo = true;
            },
            onFileChange(e) {
                var files = e.target.files || e.dataTransfer.files;


                console.log(files);
                if (!files.length) {
                    return;
                }

                if(e.target.name == 'profilePhotoResize') {
                    return this.createImage(files[0], e.target.name, e.target);
                }

                e.stopPropagation(); // Stop stuff happening

                e.preventDefault(); // Totally stop stuff happening

                // START A LOADING SPINNER HERE

                console.log(e.target.name);

                if(e.target.name == 'wwcc_scan') {
                    this.loading.scan1 = true;
                    this.wwcc_scan = '';
                    this.form.prof_scan_id.error = '';
                }
                if(e.target.name == 'wwcc_scan2') {
                    this.loading.scan2 = true;
                    this.wwcc_scan2 = '';
                    this.form.prof_scan_id2.error = '';
                }

                if(e.target.name == 'work_visa') {
                    this.loading.work_visa = true;
                    this.work_visa = '';
                }

                // Create a formdata object and add the files
                var data = new FormData();
                $.each(files, function(key, value)
                {
                    data.append(key, value);
                });

                var that = this;

                $.ajax({
                    xhr: function()
                    {
                        var xhr = new window.XMLHttpRequest();
                        // прогресс загрузки на сервер
                        xhr.upload.addEventListener("progress", function(evt){
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                // делать что-то...
                                console.log("upload:"+percentComplete);
                                var prg = Math.round(percentComplete * 100);

                                $('#'+e.target.name+'progress').attr('aria-valuenow', prg)
                                    .css({width:prg+'%'})
                                    .html(prg+'%');
                            }
                        }, false);
                        return xhr;
                    },
                    url: '/api/v1/upload/image',
                    type: 'POST',
                    data: data,
                    files: $('#'+e.target.name+'_file', null),
                    cache: false,
                    dataType: 'json',
                    enctype: "multipart/form-data",
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    iframe: true,
                    success: function(data, textStatus, jqXHR)
                    {
                        alert(JSON.stringify(data));
                        if(typeof data.error === 'undefined')
                        {
                            // Success so call function to process the form
                            if(e.target.name == 'wwcc_scan') {
                                that.loading.scan1 = false;
                                that.wwcc_scan = data.image_path;
                                that.form.prof_scan_id.error = '';
                            }
                            if(e.target.name == 'wwcc_scan2') {
                                that.loading.scan2 = false;
                                that.wwcc_scan2 = data.image_path;
                                that.form.prof_scan_id2.error = '';
                            }

                            if(e.target.name == 'work_visa') {
                                that.loading.work_visa = false;
                                that.work_visa= data.image_path;
                                that.detectWorkingVisaType();
                            }


                            that.detectProfIdType();

                        }
                        else
                        {
                            // Handle errors here
                            console.log('ERRORS: ' + data.error);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        var out = '';
                        for (var i in jqXHR) {
                            out += i + ": " + jqXHR[i] + "\n";
                        }

                        alert(out);

                        // Handle errors here
                        console.log('ERRORS: ' + textStatus);
                        if(e.target.name == 'wwcc_scan') {
                            that.loading.scan1 = false
                        }
                        if(e.target.name == 'wwcc_scan2') {
                            that.loading.scan2 = false;
                        }
                    }
                });


                //this.createImage(files[0], e.target.name, e.target);
            },
            createImage(file, $name, target) {
                console.log($name);

                if($name == 'wwcc_scan') {
                    this.wwcc_scan_type = 'image';
                    this.form.prof_scan_id.error = '';
                }

                if($name == 'wwcc_scan2') {

                    this.form.prof_scan_id2.error = '';
                }

                if(file.type == 'image/png' || file.type == 'image/jpeg' || file.type == 'image/gif' || file.type == 'image/jpg' || file.type == 'image/bitmap') {

                    if($name == 'profilePhotoResize') {
                        $('.resize-image').attr('src', '');
                        $('.cropper-container .cropper-canvas img').attr('src', '');
                        $('.cropper-container .cropper-view-box img').attr('src', '');
                    }

                    var _this = this;
                    var image = new Image();
                    var reader = new FileReader();
                    var vm = this;
                    console.log('create', $name);

                    reader.onload = (e) => {
                        vm[$name] = e.target.result;
                        if($name == 'profilePhotoResize') {
                            $('.resize-image').attr('src', e.target.result);
                            $('.cropper-container .cropper-canvas img').attr('src', e.target.result);
                            $('.cropper-container .cropper-view-box img').attr('src', e.target.result);
                            _this.cropImage();
                        }
                    };
                    reader.readAsDataURL(file);
                    this.photoSelected = true;
                    this.noPhotoSubmitted = false;
                } else {
                    if($name == 'wwcc_scan') {
                        this.wwcc_scan = file;
                        this.wwcc_scan_type = 'file';
                        // $(target).parent().find('img').attr('src', '').attr('alt', file.name);
                        setTimeout(function(){ $(".scanid").html( file.name ); }, 500);
                    }else{
                        alert("You must put image");
                    }

                }

            },
            setProfileImage(){
                this.user_photo_100 = this.profilePhotoPreviev;
                //this.profilePhotoText= "<span style='color:red;'>! We weren't able to detect your face. Please use a photo where your face is the main focus.</span>";



                if(this.user_photo_100 !== '/img/no_photo_100.jpg') {
                    this.noPhotoSubmitted = false;
                    this.photo_error = '';
                    this.user_photo_100_temp = '';
                    this.add_photo = false;
                    this.profilePhotoSelectText= "+ Edit photo";

                } else {
                    this.noPhotoSubmitted = true;
                }
            },
            cancelCropPhoto() {
                this.user_photo_100_temp = '';
                this.add_photo = false;

                image = $('.resize-image');
                image.cropper('destroy');

                this.profilePhotoPreviev = '/img/no_photo_100.jpg';
                this.profilePhotoResize = '/img/no_photo_100.jpg';
                this.photoSelected = false;

            },
            getImageData(){
                var canvas = image.cropper('getCroppedCanvas');
                var canvaURL = canvas.toDataURL('image');
                this.profilePhotoPreviev = canvaURL;
            },
            cropImage(){
                var _this = this;
                // if("undefined" !== typeof(image.cropper)) {
                //     console.log('destroe');
                //     image.cropper('destroy');
                // }
                // console.log("crop");
                // image = $('.resize-image');
                // image.cropper('destroy');
                // image.cropper({
                //     height: 300,
                //     width: 264,
                //     aspectRatio: 4/4,
                //     viewMode: 2,
                //     crop: function(e) {
                //         _this.getImageData();
                //     },
                // });
                image = $('.resize-image');
                image.attr('src', this.user_photo_100_temp);
                image.cropper('destroy');
                image.cropper({
                    height: 300,
                    width: 264,
                    aspectRatio: 4/4,
                    viewMode: 2,
                    crop: function () {
                        $('.cropper-container .cropper-canvas img').attr('src', this.user_photo_100_temp);
                        $('.cropper-container .cropper-view-box img').attr('src', this.user_photo_100_temp);
                        var canvas = image.cropper('getCroppedCanvas');
                        var canvaURL = canvas.toDataURL('image');
                        this.profilePhotoPreviev = canvaURL;
                    }.bind(this)
                });
            },
            postForm(){
                jQuery("body").LoadingOverlay("show");

                $("#btnsubmit").attr('disabled', true).html('Loading...');

                // if(this.loading.scan1 === true || this.loading.scan2 === true || this.loading.work_visa === true) {
                //     var _this = this;
                //
                //     var postFormInterval = window.setInterval(()=>{
                //         if(_this.loading.scan1 === false && _this.loading.scan2 === false && _this.loading.work_visa === false) {
                //             window.clearInterval(postFormInterval);
                //             _this.postForm();
                //         }
                //     },200);
                //
                //     return;
                // }


                if(this.user_photo_100 == '' || this.user_photo_100 == '/img/no_photo_100.jpg') {
                    this.photo_error = 'You must select a photo';
                    $("#btnsubmit").attr('disabled', false).html('Submit');
                    jQuery("body").LoadingOverlay("hide");


                    //return;
                }

                this.fileUploadFormData.append('first_name', this.form.first_name.text);
                this.fileUploadFormData.append('last_name', this.form.last_name.text);
                this.fileUploadFormData.append('user_photo_100', this.user_photo_100);
                this.fileUploadFormData.append('birthday', this.form.birthday.text);
                this.fileUploadFormData.append('prof_scan_id', this.wwcc_scan);
                this.fileUploadFormData.append('prof_scan_id2', this.wwcc_scan2);
                this.fileUploadFormData.append('prof_id_sc', this.form.prof_id_sc.text);
                this.fileUploadFormData.append('prof_dateof', this.form.prof_dateof.text);
                this.fileUploadFormData.append('prof_visa', this.work_visa);
                this.fileUploadFormData.append('notau', this.form.notau.checked);
                this.fileUploadFormData.append('notes', this.form.notes.text);

                if(this.form.prof_id_type.text === 'Other') {
                    this.fileUploadFormData.append('prof_id_type', this.form.prof_id_custom_type.text);
                } else {
                    this.fileUploadFormData.append('prof_id_type', this.form.prof_id_type.text);
                }



                this.$http.post('/api/v1/settings/saveid', this.fileUploadFormData, {
                    progress(e) {
                        console.log(e);
                        if(e.lengthComputable) {
                            console.log(e.loaded / e.total * 100);
                        }
                    }
                })
                    .then( function(response) {
                            // this.$dispatch('notification', {type: response.data.notification.type, 'message': response.data.notification.message, 'close': false});
                        if(this.photo_error || this.noPhotoSubmitted) {
                            return;
                        }

                        window.location.href = "/user/settings/get-verified";
                        }, function(response) {
                            $("#btnsubmit").attr('disabled', false).html('Submit');
                            jQuery("body").LoadingOverlay("hide");
                            for (var key in response.data) {
                                // this.form[key].error = response.data[key][0];
                                this.form[key].error = 'This is required field';
                            }
                            // on unknown error
                            if (response.status === 500) {
                                this.cancel();
                                // this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                            }
                        }
                    );
            },
            setUserInfo: function (data) {
                this.user_photo_100 = data.user.user_photo_100;
                if(!this.user_photo_100 || !this.user_photo_100.length) {
                    this.user_photo_100 = '/img/no_photo_100.jpg';
                }

                this.form.first_name.text = data.user.first_name;
                this.form.last_name.text = data.user.last_name;
                this.form.birthday.text = data.user.birthday;

                // Detect whether file is image
                if( data.prof_scan_id && (data.prof_scan_id.indexOf("JFIF") !== -1 ||
                    data.prof_scan_id.indexOf("base64") !== -1 ||
                    data.prof_scan_id.indexOf("PNG") !== -1 ||
                    data.prof_scan_id.indexOf("GIF") !== -1 ||
                    data.prof_scan_id.indexOf("BMP") !== -1 ||
                    data.prof_scan_id.indexOf("jpeg") !== -1 ||
                    data.prof_scan_id.indexOf("JPEG") !== -1 ||
                    data.prof_scan_id.indexOf("gif") !== -1 ||
                    data.prof_scan_id.indexOf("bmp") !== -1 ||
                    data.prof_scan_id.indexOf("jpg") !== -1 ||
                    data.prof_scan_id.indexOf("JPG") !== -1 ||
                    data.prof_scan_id.indexOf("png") !== -1
                )){
                    this.wwcc_scan_type = 'image';
                }else{
                    this.wwcc_scan_type = 'file';
                }

                // Detect whether visa working is image
                if( data.prof_visa && (data.prof_visa.indexOf("JFIF") !== -1 ||
                    data.prof_visa.indexOf("base64") !== -1 ||
                    data.prof_visa.indexOf("PNG") !== -1 ||
                    data.prof_visa.indexOf("GIF") !== -1 ||
                    data.prof_visa.indexOf("BMP") !== -1 ||
                    data.prof_visa.indexOf("jpeg") !== -1 ||
                    data.prof_visa.indexOf("JPEG") !== -1 ||
                    data.prof_visa.indexOf("gif") !== -1 ||
                    data.prof_visa.indexOf("bmp") !== -1 ||
                    data.prof_visa.indexOf("jpg") !== -1 ||
                    data.prof_visa.indexOf("JPG") !== -1 ||
                    data.prof_visa.indexOf("png") !== -1
                )){
                    this.work_visa_type = 'image';
                }else{
                    this.work_visa_type = 'file';
                }

                this.wwcc_scan = data.prof_scan_id;
                this.wwcc_scan2 = data.prof_scan_id2;

                if($.inArray(data.prof_id_type, this.form.prof_id_type.types) === -1) {
                    this.form.prof_id_type.text = 'Other';
                    this.form.prof_id_custom_type.text = data.prof_id_type;
                } else {
                    this.form.prof_id_type.text = data.prof_id_type.length ? data.prof_id_type : '--- Please select from list ---';
                }

                this.form.prof_id_sc.text = data.prof_id_sc;
                this.form.prof_dateof.text = data.prof_dateof;
                this.work_visa = data.prof_visa;
                this.form.notau.checked = this.work_visa !== '';
                this.form.notes.text = data.bd_id_notes;

                this.detectProfIdType();

            },
            detectProfIdType() {
                // Detect whether file is image
                if( this.wwcc_scan && (this.wwcc_scan.indexOf("JFIF") !== -1 ||
                    this.wwcc_scan.indexOf("base64") !== -1 ||
                    this.wwcc_scan.indexOf("PNG") !== -1 ||
                    this.wwcc_scan.indexOf("GIF") !== -1 ||
                    this.wwcc_scan.indexOf("BMP") !== -1 ||
                    this.wwcc_scan.indexOf("jpeg") !== -1 ||
                    this.wwcc_scan.indexOf("JPEG") !== -1 ||
                    this.wwcc_scan.indexOf("gif") !== -1 ||
                    this.wwcc_scan.indexOf("bmp") !== -1 ||
                    this.wwcc_scan.indexOf("jpg") !== -1 ||
                    this.wwcc_scan.indexOf("JPG") !== -1 ||
                    this.wwcc_scan.indexOf("png") !== -1
                )){
                    this.wwcc_scan_type = 'image';
                }else{
                    this.wwcc_scan_type = 'file';
                }

                if(!this.wwcc_scan2) {
                    return;
                }

                if( this.wwcc_scan2 && (this.wwcc_scan2.indexOf("JFIF") !== -1 ||
                    this.wwcc_scan2.indexOf("base64") !== -1 ||
                    this.wwcc_scan2.indexOf("PNG") !== -1 ||
                    this.wwcc_scan2.indexOf("GIF") !== -1 ||
                    this.wwcc_scan2.indexOf("BMP") !== -1 ||
                    this.wwcc_scan2.indexOf("jpeg") !== -1 ||
                    this.wwcc_scan2.indexOf("JPEG") !== -1 ||
                    this.wwcc_scan2.indexOf("gif") !== -1 ||
                    this.wwcc_scan2.indexOf("bmp") !== -1 ||
                    this.wwcc_scan2.indexOf("jpg") !== -1 ||
                    this.wwcc_scan2.indexOf("JPG") !== -1 ||
                    this.wwcc_scan2.indexOf("png") !== -1
                )){
                    this.wwcc_scan2_type = 'image';
                }else{
                    this.wwcc_scan2_type = 'file';
                }
            },
            detectWorkingVisaType() {
                // Detect whether file is image
                if( this.work_visa && (this.work_visa.indexOf("JFIF") !== -1 ||
                    this.work_visa.indexOf("base64") !== -1 ||
                    this.work_visa.indexOf("PNG") !== -1 ||
                    this.work_visa.indexOf("GIF") !== -1 ||
                    this.work_visa.indexOf("BMP") !== -1 ||
                    this.work_visa.indexOf("jpeg") !== -1 ||
                    this.work_visa.indexOf("JPEG") !== -1 ||
                    this.work_visa.indexOf("gif") !== -1 ||
                    this.work_visa.indexOf("bmp") !== -1 ||
                    this.work_visa.indexOf("jpg") !== -1 ||
                    this.work_visa.indexOf("JPG") !== -1 ||
                    this.work_visa.indexOf("png") !== -1
                )){
                    this.work_visa_type = 'image';
                }else{
                    this.work_visa_type = 'file';
                }
            }
        },
        ready(){
            var date = new Date();
            // var picker = new Pikaday({
            //     field: document.getElementById('bday'),
            //     format: 'DD/MM/YYYY',
            //     maxDate: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
            //     yearRange: [1950, date.getFullYear()]
            // });
            // var picker2 = new Pikaday({
            //     field: document.getElementById('pickdate'),
            //     format: 'DD/MM/YYYY',
            //     maxDate: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
            //     yearRange: [1950, date.getFullYear()]
            // });
            this.$http.get('/api/v1/settings/user-info').then(function(response) {
                this.setUserInfo(response.data);

            }, function() {
                this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);
        }
    });

}