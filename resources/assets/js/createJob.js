import Vue from "vue";
import ExperienceLevels from "./components/ExperienceLevels.vue";
import Ocupation from "./components/Ocupation.vue";
import Questions from "./components/Questions.vue";
import CustomSelect from "./components/CustomSelect.vue";
import CustomSelectExt from "./components/CustomSelectExtended.vue";
import CustomSelectMultiple from "./components/CustomSelectMultiple.vue";
import $ from "jquery";
import _ from "lodash";

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;


/*
 For password and security Settings
 */
if (document.getElementById('create-job')) {
    var vm = new Vue({
        el: '#create-job',
        data: {
            qualifications_id: false,
            have_transportation: false,
            gender: false,
            smoker: false,
            years_experience: false,
            comfortable_with_pets: false,
            own_children: false,
            sick_children: false,
            child_age_range: [],
            age: [],
            ocupation: false,
            ocupationFormData: {
                selected: '',
                errors: {
                    message: ''
                },
                days: [],
                text: []
            },
            category: {
                selected: [],
                errors: {
                    message: ''
                },
                text: '',
                items: [],
                multiple: true
            },
            qualifications: {
                selected: [],
                errors: {
                    message: ''
                },
                text: '',
                items: [],
                multiple: false
            },
            title: {
                text: '',
                errors: {
                    message: ''
                }
            },
            description: {
                text: '',
                errors: {
                    message: ''
                }
            },
            job_duration: {
                selected: '',
                errors: {
                    message: ''
                },
                text: ''
            },
            job_workload: {
                selected: '',
                errors: {
                    message: ''
                },
                text: ''
            },
            minimum_feedback: {
                selected: 1,
                errors: {
                    message: ''
                },
                text: ''
            },
            marketplace_visibility: {
                selected: 1,
                errors: {
                    message: ''
                },
                text: '',
            },
            allowed_locations: {
                selected: 1,
                errors: {
                    message: ''
                },
                text: ''
            },
            prevtext: {
                smoker: 'No preference',
                age_range: 'No preference',
                child_age: 'No preference',
                drivers_licence: 'No preference',
                gender: 'No preference',
                pets: 'No preference',
                own_children: 'No preference',
                experience: 'No preference',
                allowed_locations: 'No preference',
                minimum_feedback: 'No preference',
                marketplace_visibility: 'No preference',
                estimated_workload: 'No preference',
                job_duration: 'No preference',
                qualifications_items_text: 'No preference',
                child_age_range: 'No preference',
                choose_category: 'No preference',
                care_sick: 'No preference',

            },
            cover_letter: true,
            previewMode: false,
            editMode: false,
            mainActionText: '',
            secondaryActionText: '',
            questions: {
                list: [{
                    text: ''
                }]
            },
            previousJob: {
                selected: '',
                errors: {
                    message: ''
                }
            },

            /* Criteria */
            selects: null,
            urls: {
                get: '/api/v1/client/caregivers/find',
                post: '/api/v1/client/caregivers/find'
            }
        },
        components: {
            'qualification': CustomSelectExt,
            'experience-levels': ExperienceLevels,
            'ocupation': Ocupation,
            'questions': Questions,
            'custom-select': CustomSelect,
            'custom-select-multiple': CustomSelectMultiple,
            'custom-select-ext': CustomSelectExt
        },
        methods: {
            createJob: function () {
                // disable input
                this.$http.post('/api/v1/clients/job', this.formData).then(function (response) {
                    this.storedJob(response);
                }, function (response) {
                    this.handleError(response);
                }).bind(this);
                // enable input again...
            },

            editJob: function () {
                var action = document.getElementById('job-form').action;
                this.$http.patch(action, this.formData).then(function (response) {
                    this.updatedJob(response);
                }, function (response) {
                    this.handleError(response);
                }).bind(this);

            },

            storedJob: function (response) {
                if (response.data.success) {
                    window.location = response.data.redirect;
                }
            },

            updatedJob: function (response) {
                if (response.data.success) {
                    window.location = response.data.redirect;
                }
            },

            categorySelectedText: function (selectedText) {
                this.category.text = selectedText;
            },

            jobDurationSelectedText: function (selectedText) {
                this.job_duration.text = selectedText;
            },

            jobWorkloadSelectedText: function (selectedText) {
                this.job_workload.text = selectedText;
            },

            minimumFeedbackSelectedText: function (selectedText) {
                this.minimum_feedback.text = selectedText;
            },

            marketplaceVisibilitySelectedText: function (selectedText) {
                this.marketplace_visibility.text = selectedText;
            },

            allowedLocationsSelectedText: function (selectedText) {
                this.allowed_locations.text = selectedText;
            },

            prevtextSelectedText: function (type, selectedText) {
                if (undefined === type) {
                    this.prevtext.text = selectedText;
                }
                else {
                    this.prevtext[type] = selectedText;
                }
            },

            handleError: function (response) {
                $('html, body').animate({
                    scrollTop: $("#create-job").offset().top
                }, 400);
                if (response.status === 422) {
                    this.category.errors.message = typeof response.data.category != 'undefined' ? response.data.category[0] : '';
                    this.title.errors.message = typeof response.data.title != 'undefined' ? response.data.title[0] : '';
                    this.description.errors.message = typeof response.data.description != 'undefined' ? response.data.description[0] : '';
                    this.job_duration.errors.message = typeof response.data.duration != 'undefined' ? response.data.duration[0] : '';
                    this.job_workload.errors.message = typeof response.data.workload != 'undefined' ? response.data.workload[0] : '';
                    this.ocupationFormData.errors.message = typeof response.data.ocupation != 'undefined' ? response.data.ocupation[0] : '';
                    this.ocupationFormData.errors.message = typeof response.data.days != 'undefined' ? response.data.days[0] : '';
                    this.marketplace_visibility.errors.message = typeof response.data.marketplace_visibility != 'undefined' ? response.data.marketplace_visibility[0] : '';
                    this.minimum_feedback.errors.message = typeof response.data.minimum_feedback != 'undefined' ? response.data.minimum_feedback[0] : '';
                    this.allowed_locations.errors.message = typeof response.data.allowed_locations != 'undefined' ? response.data.allowed_locations[0] : '';
                    // this.experience.errors.message = typeof response.data.minimum_experience != 'undefined' ? response.data.minimum_experience[0] : '';
                }
            },

            fetchEditDataForJob: function () {
                var action = document.getElementById('job-form').action;

                this.$http.get(action).then(function (response) {






                    var that = this;

                    window.setTimeout(function(){
                        that.processJobData(response.data);
                    }, 1000);


                }, function () {

                    this.setDefaultData();
                }).bind(this);

            },

            handlePreviewMode: function () {
                var choose_categorys = [];
                $(".choose_category  li.active").each(function () {
                    var str = $(this).html();
                    console.log(str);
                    // str = str.replace(/&amp;/g, '&');
                    str = str.replace(/\<.*\>/g, "");
                    choose_categorys.push(str);
                });
                var choose = choose_categorys.join(', ');
                this.categorySelectedText(choose);

                var child_age_ranges = [];
                $(".child-age .option.active .option-name").each(function () {
                    child_age_ranges.push($(this).html().trim());
                });
                var child = child_age_ranges.join(', ');
                this.prevtextSelectedText('child_age_range', child);

                var qualifications_items = [];
                $(".qualification li.active").each(function () {
                    qualifications_items.push($(this).text().trim());
                });
                var qua = qualifications_items.join(', ');
                this.prevtextSelectedText('qualifications_items_text', qua);


                if ($(".experience li.active").html() != null) {
                    this.prevtextSelectedText('experience', $(".experience li.active").html().replace(/<.*>/g, ""));
                }
                if ($(".allowed_locations_id li.active").html() != null) {
                    this.prevtextSelectedText('allowed_locations', $(".allowed_locations_id li.active").html().replace(/<.*>/g, ""));
                }
                if ($(".minimum_feedback_id li.active").html() != null) {
                    this.prevtextSelectedText('minimum_feedback', $(".minimum_feedback_id li.active").html().replace(/<.*>/g, ""));
                }
                if ($(".marketplace_visibility_id li.active").html() != null) {
                    this.prevtextSelectedText('marketplace_visibility', $(".marketplace_visibility_id li.active").html().replace(/<.*>/g, ""));
                }
                if ($(".estimated_workload li.active").html() != null) {
                    this.prevtextSelectedText('estimated_workload', $(".estimated_workload li.active").html().replace(/<.*>/g, ""));
                }
                if ($(".job_duration li.active").html() != null) {
                    this.prevtextSelectedText('job_duration', $(".job_duration li.active").html().replace(/<.*>/g, ""));
                }

                if ($(".sick_children li.active").html() != null) {
                    this.prevtextSelectedText('care_sick', $(".sick_children li.active").html().replace(/<.*>/g, ""));
                }


                if (this.qualifications_id) {
                    var qualifications = this.qualifications_id;

                    for (var jq in this.qualifications_id) {

                    }
                }

                if (!_.isEmpty(this.age)) {
                    for (var j in this.age) {
                        for (var i in this.selects.age_range.items) {
                            if (this.selects.age_range.items[i].value == this.age[j]) {
                                this.prevtextSelectedText('age_range', this.selects.age_range.items[i].text);
                            }
                        }
                    }
                }

                for (var i in this.selects.child_age.items) {
                    for (var j in this.child_age_range) {
                        if (this.selects.child_age.items[i].value == this.child_age_range[j]) {
                            this.prevtextSelectedText('child_age', this.selects.child_age.items[i].text);
                        }
                    }
                }

                // if (this.experience > 1) {
                //     for (var i in this.selects.years_experience.items) {
                //         K = i+1;
                //         alert(i);
                //         alert(K);

                //         if (K == this.experience) {
                //             this.prevtextSelectedText('experience', this.selects.years_experience.items[i].text);
                //         }
                //     }
                // }

                if (this.have_transportation > 1) {
                    for (var i in this.selects.drivers_licence.items) {
                        if (this.selects.drivers_licence.items[i].value == this.have_transportation) {
                            this.prevtextSelectedText('drivers_licence', this.selects.drivers_licence.items[i].text);
                        }
                    }
                }

                if (this.gender > 1) {
                    for (var i in this.selects.gender.items) {
                        if (this.selects.gender.items[i].value == this.gender) {
                            this.prevtextSelectedText('gender', this.selects.gender.items[i].text);
                        }
                    }
                }

                if (this.smoker > 1) {
                    for (var i in this.selects.smoker.items) {
                        if (this.selects.smoker.items[i].value == this.smoker) {
                            this.prevtextSelectedText('smoker', this.selects.smoker.items[i].text);
                        }
                    }
                }

                if (this.comfortable_with_pets > 1) {
                    for (var i in this.selects.pets.items) {
                        if (this.selects.pets.items[i].value == this.comfortable_with_pets) {
                            this.prevtextSelectedText('pets', this.selects.pets.items[i].text);
                        }
                    }
                }

                if (this.own_children > 1) {
                    for (var i in this.selects.own_children.items) {
                        if (this.selects.own_children.items[i].value == this.own_children) {
                            this.prevtextSelectedText('own_children', this.selects.own_children.items[i].text);
                        }
                    }
                }


                if (this.sick_children > 1) {
                    for (var i in this.selects.care_sick.items) {
                        if (this.selects.care_sick.items[i].value == this.sick_children) {
                            this.prevtextSelectedText('care_sick', this.selects.care_sick.items[i].text);
                        }
                    }
                }

                this.previewMode = !this.previewMode;
            },

            setPreviousJob: function (option) {
                if (option.value) {
                    this.$http.get('api/v1/clients/job/' + option.value).then(function (response) {

                        jQuery('.no_preference').removeClass('active');
                        $('.no_preference').removeClass('active');

                        const that = this;

                        setTimeout(function(){
                            that.processJobData(response.data, false);
                        },500);

                    }, function (response) {
                    }).bind(this);
                } else {
                    this.setDefaultData();
                }
            },
            processJobData: function (response, setTitle = true) {
                this.ocupationFormData.selected = response.data.ocupationFormData.selected;
                this.ocupationFormData.days = response.data.ocupationFormData.days;
                this.category.selected = response.data.category;
                this.category.items = response.data.categories_items;
                if(setTitle === true) {
                    this.title.text = response.data.title;
                }
                this.description.text = response.data.description;
                this.job_duration.selected = response.data.job_duration;
                this.job_workload.selected = response.data.job_workload;
                this.minimum_feedback.selected = response.data.minimum_feedback;
                this.marketplace_visibility.selected = response.data.marketplace_visibility;
                this.allowed_locations.selected = response.data.allowed_locations;
                this.experience.selected = response.data.experience;
                this.cover_letter = response.data.cover_letter;
                this.questions.list = response.data.questions;
                this.have_transportation = response.data.have_transportation;
                this.gender = response.data.gender;
                this.smoker = response.data.smoker;
                this.comfortable_with_pets = response.data.comfortable_with_pets;
                this.own_children = response.data.own_children;
                this.sick_children = response.data.sick_children;

                this.qualifications.selected = response.data.qualifications;
                // if (!_.isEmpty(response.data.qualification)) {
                //     this.qualifications.items = response.data.qualification.items;
                //     for(var q in this.qualifications.items) {
                //         if(this.qualifications.items[q].is_selected) {
                //             this.qualifications.selected.push(this.qualifications.items[q].value);
                //         }
                //     }
                // }
                //
                if (!_.isEmpty(response.data.child_age_range)) {
                    this.child_age_range = JSON.parse(response.data.child_age_range);
                }

                if (!_.isEmpty(response.data.age)) {
                    this.age = JSON.parse(response.data.age);
                }
            },

            setDefaultDays: function () {
                return [
                    {
                        name: "Monday",
                        selectedFrom: false,
                        selectedTo: false,
                        selected: false
                    },
                    {
                        name: "Tuesday",
                        selectedFrom: false,
                        selectedTo: false,
                        selected: false
                    },
                    {
                        name: "Wednesday",
                        selectedFrom: false,
                        selectedTo: false,
                        selected: false
                    },
                    {
                        name: "Thursday",
                        selectedFrom: false,
                        selectedTo: false,
                        selected: false
                    },
                    {
                        name: "Friday",
                        selectedFrom: false,
                        selectedTo: false,
                        selected: false
                    },
                    {
                        name: "Saturday",
                        selectedFrom: false,
                        selectedTo: false,
                        selected: false
                    },
                    {
                        name: "Sunday",
                        selectedFrom: false,
                        selectedTo: false,
                        selected: false
                    }
                ];
            },

            setDefaultData: function () {
                this.ocupationFormData.selected = '';
                this.ocupationFormData.days = this.setDefaultDays();
                this.category.selected = [];
                this.title.text = '';
                this.description.text = '';
                this.job_duration.selected = '';
                this.job_workload.selected = '';
                this.minimum_feedback.selected = 1;
                this.marketplace_visibility.selected = 1;
                this.allowed_locations.selected = 1;
                this.experience.selected = 1;
                this.cover_letter = true;
                this.questions.list = [{text: ''}];
                this.have_transportation = false;
                this.gender = false;
                this.smoker = false;
                this.comfortable_with_pets = false;
                this.own_children = false;
                this.sick_children = false;
                this.child_age_range = [];
                this.age = [];
            },

            mainAction: function () {
                if (this.editMode) {
                    return this.editJob();
                }

                return this.createJob();
            }
        },
        computed: {
            mainActionText: function () {
                if (this.previewMode || this.editMode) {
                    return "Save";
                } else {
                    return "Post Job";
                }
            },

            secondaryActionText: function () {
                if (this.previewMode) {
                    return "Make Changes";
                } else {
                    return "Preview";
                }
            },
            formData: function () {
                return {
                    category: this.category.selected,
                    title: this.title.text,
                    description: this.description.text,
                    duration: this.job_duration.selected,
                    workload: this.job_workload.selected,
                    ocupation: this.ocupationFormData.selected,
                    days: this.ocupationFormData.days,
                    marketplace_visibility: this.marketplace_visibility.selected,
                    minimum_feedback: this.minimum_feedback.selected,
                    allowed_locations: this.allowed_locations.selected,
                    minimum_experience: this.experience.selected,
                    cover_letter: this.cover_letter,
                    questions: this.questions.list,
                    qualifications: this.qualifications.selected,
                    have_transportation: this.have_transportation,
                    gender: this.gender,
                    smoker: this.smoker,
                    comfortable_with_pets: this.comfortable_with_pets,
                    own_children: this.own_children,
                    sick_children: this.sick_children,
                    child_age_range: this.child_age_range,
                    age: this.age

                }
            },
            getQualification: function () {
                if (this.qualifications.items) {
                    for (var i in this.qualifications.items) {
                        if (this.qualifications.items[i].is_selected) {
                            return this.qualifications.items[i].text
                        }
                    }
                }
            }
        },
        ready: function () {
            //jQuery("body").LoadingOverlay("show");
            this.$http.get('/api/v1/common/categories').then(function (response) {
                this.category.items = response.data.data.categories_items;
            }, function (response) {
                console.log(['Error!', response]);
            }).bind(this);

            this.$http.get('/api/v1/common/qualifications').then(function (response) {
                this.qualifications.items = response.data.data.qualifications;
            }, function (response) {
                console.log(['Error!', response]);
            }).bind(this);

            this.ocupationFormData.days = this.setDefaultDays();

            if (typeof editMode !== "undefined") {
                this.editMode = 1;
                this.fetchEditDataForJob();
            }

            this.$http.get(this.urls.get)
                .then(function (response) {

                    response.data.age_range.selected = [];
                    this.selects = response.data;

                    this.selects.age_range.items[0].is_selected = false;
                    this.selects.age_range.items[1].is_selected = false;
                    this.selects.age_range.items[2].is_selected = false;
                    this.selects.age_range.items[3].is_selected = false;
                    this.selects.gender.items[0].is_selected = false;
                    this.selects.gender.items[1].is_selected = false;


                    if (!this.editMode) {
                        setTimeout(function () {
                            jQuery('.no_preference').each(function () {
                                jQuery(this).trigger('click');
                            });
                        }, 100);
                    } else {
                        this.selects.child_age.items[0].is_selected = false;
                        this.selects.child_age.items[1].is_selected = false;
                        this.selects.child_age.items[2].is_selected = false;


                        if (this.qualifications_id) {
                            var qualifications = this.qualifications_id;

                            for (var jq in this.qualifications_id) {
                                setTimeout(function () {
                                    jQuery('.FindCaregivers__filter--qualifications [data-option-value="' + qualifications[jq] + '"]').each(function () {
                                        jQuery(this).trigger('click');
                                    });
                                }, 200);

                            }
                        }

                        if (!_.isEmpty(this.age)) {
                            for (var j in this.age) {
                                for (var i in this.selects.age_range.items) {
                                    if (this.selects.age_range.items[i].value == this.age[j]) {
                                        this.selects.age_range.items[i].is_selected = true;
                                    }
                                }
                            }
                        } else {
                            setTimeout(function () {
                                jQuery('.FindCaregivers__filter .age .no_preference').each(function () {
                                    jQuery(this).trigger('click');
                                });
                            }, 100);
                        }

                        for (var i in this.selects.child_age.items) {
                            for (var j in this.child_age_range) {
                                if (this.selects.child_age.items[i].value == this.child_age_range[j]) {
                                    this.selects.child_age.items[i].is_selected = true;
                                }
                            }
                        }

                        if (this.have_transportation > 1) {
                            for (var i in this.selects.drivers_licence.items) {
                                if (this.selects.drivers_licence.items[i].value == this.have_transportation) {
                                    this.selects.drivers_licence.items[i].is_selected = true;
                                }
                            }
                        } else {
                            setTimeout(function () {
                                jQuery('.FindCaregivers__filter .have_transportation .no_preference').each(function () {
                                    jQuery(this).trigger('click');
                                });
                            }, 100);
                        }

                        if (this.gender > 1) {
                            for (var i in this.selects.gender.items) {
                                if (this.selects.gender.items[i].value == this.gender) {
                                    this.selects.gender.items[i].is_selected = true;
                                }
                            }
                        } else {
                            setTimeout(function () {
                                jQuery('.FindCaregivers__filter .gender .no_preference').each(function () {
                                    jQuery(this).trigger('click');
                                });
                            }, 100);
                        }

                        if (this.smoker > 1) {
                            for (var i in this.selects.smoker.items) {
                                if (this.selects.smoker.items[i].value == this.smoker) {
                                    this.selects.smoker.items[i].is_selected = true;
                                }
                            }
                        } else {
                            setTimeout(function () {
                                jQuery('.FindCaregivers__filter .smoker .no_preference').each(function () {
                                    jQuery(this).trigger('click');
                                });
                            }, 100);
                        }

                        if (this.comfortable_with_pets > 1) {
                            for (var i in this.selects.pets.items) {
                                if (this.selects.pets.items[i].value == this.comfortable_with_pets) {
                                    this.selects.pets.items[i].is_selected = true;
                                }
                            }
                        } else {
                            setTimeout(function () {
                                jQuery('.FindCaregivers__filter .comfortable_with_pets .no_preference').each(function () {
                                    jQuery(this).trigger('click');
                                });
                            }, 100);
                        }

                        if (this.own_children > 1) {
                            for (var i in this.selects.own_children.items) {
                                if (this.selects.own_children.items[i].value == this.own_children) {
                                    this.selects.own_children.items[i].is_selected = true;
                                }
                            }
                        } else {
                            setTimeout(function () {
                                jQuery('.FindCaregivers__filter .own_children .no_preference').each(function () {
                                    jQuery(this).trigger('click');
                                });
                            }, 100);
                        }

                        if (this.sick_children > 1) {
                            for (var i in this.selects.care_sick.items) {
                                if (this.selects.care_sick.items[i].value == this.sick_children) {
                                    this.selects.care_sick.items[i].is_selected = true;
                                }
                            }
                        } else {
                            setTimeout(function () {
                                jQuery('.FindCaregivers__filter .sick_children .no_preference').each(function () {
                                    jQuery(this).trigger('click');
                                });
                            }, 100);
                        }

                    }
                    //jQuery("body").LoadingOverlay("hide");

                })
                .catch(function (response) {
                    //jQuery("body").LoadingOverlay("hide");
                    console.log(['Error!', response]);
                })
                .bind(this);
        }
    });
}
