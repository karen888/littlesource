import Vue from 'vue';
import Caregivers from './components/Pre-launch/Caregivers.vue';
import Clients from './components/Pre-launch/Clients.vue';
import Beta from './components/Pre-launch/Beta.vue';
import AdinaInfo from './components/Pre-launch/AdinaInfo.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('pre-launch')) {
    var vm = new Vue({
        el: '#pre-launch',
        ready: function() {
            //$(".fancybox").fancybox({
            //    openEffect: "none",
            //    closeEffect: "none"
            //});
        },
        computed: {},
        data: {
            currentView: 'Clients',
            viewIsSelected: false,
            showModal: false,
            showAdinaModal: false
        },
        components: {
            'Caregivers': Caregivers,
            'Clients': Clients,
            'Beta': Beta,
            'VcAdinaInfo': AdinaInfo
        },
        methods: {
            selectPrelaunch: function(currentView) {
                this.currentView = currentView;
                this.$nextTick(function () {
                    this.viewIsSelected = true; //damn reactivity!
                });
            }
        },
        events: {
            'show-beta' : function() {
                this.showModal = true;
            },

            'show-adina-info' : function() {
                this.showAdinaModal = true;
            }
        }
    });
}
