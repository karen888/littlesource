import Vue from 'vue';
import $ from 'jquery';
import CustomSelect from './components/CustomSelect.vue';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

if (document.getElementById('registr-form')) {
    var vm = new Vue({
        el: '#registr-form',
        data: {
            first_name: {
                value: '',
                errors: {
                    message: ''
                }
            },
            last_name: {
                value: '',
                errors: {
                    message: ''
                }
            },
            email: {
                value: '',
                errors: {
                    message: ''
                }
            },
            password: {
                value: '',
                errors: {
                    message: ''
                }
            },
            password_confirmation: {
                value: '',
                errors: {
                    message: ''
                }
            },
            security_questions: {
                value: '',
                errors: {
                    message: ''
                }
            },
            care_type: {
                value: '',
                errors: {
                    message: ''
                }
            },  
            gender: {
                value: '',
                errors: {
                    message: ''
                }
            },            
            security_questions_answer: {
                value: '',
                errors: {
                    message: ''
                }
            },
            post_code: {
                value: '',
                errors: {
                    message: ''
                }
            },
            describe: {
                value: '',
                errors: {
                    message: ''
                }
            },
            showDescribe: false,
            hourly_rate: {
                value: '',
                errors: {
                    message: ''
                }
            },
            role: 'parent',
            handle: '',
            step: 'first',
            genderValue: "",
        },
        components: {
            'custom-select': CustomSelect
        },
        methods: {
            registrAction: function()  {
                jQuery('.register').addClass('disabled');
                switch(this.step) {
                    case 'first':
                        this.$http.post('/auth/check-register', this.formDataFirst).then(function() {
                            this.step = 'second';
                            jQuery('.register').removeClass('disabled');
                        }, function(response) {
                            jQuery('.register').removeClass('disabled');
                            this.handle = '';
                            this.handleErrorFirst(response);
                        }).bind(this);
                        break;
                    case 'gologin':
                        window.location = "/auth/login";
                        break;
                    default:
                        this.$http.post('/auth/register', this.formDataSecond).then(function(response) {
                            $('#registerForm').submit();

                            jQuery('.register').removeClass('disabled');
                            this.handle = response.data.handle;
                            $( ".alert-success" ).append( '<br>Didn\'t receive the email? <button id="resend" data-id="'+response.data.id+'">Send again</button>' );
                            this.step = 'gologin';
                            $(".btn-default.register").css('display', 'none');
                        }, function(response) {
                            jQuery('.register').removeClass('disabled');
                            this.handle = '';
                            this.handleErrorSecond(response);
                        }).bind(this);
                        break;
                }
            },
            handleErrorFirst: function(response) {
                if (response.status === 422) {
                    this.first_name.errors.message = typeof response.data.first_name != 'undefined' ? response.data.first_name[0] : '';
                    this.last_name.errors.message = typeof response.data.last_name != 'undefined' ? response.data.last_name[0] : '';
                    this.email.errors.message = typeof response.data.email != 'undefined' ? response.data.email[0] : '';
                    this.password.errors.message = typeof response.data.password != 'undefined' ? response.data.password[0] : '';
                    this.password_confirmation.errors.message = typeof response.data.password_confirmation != 'undefined' ? response.data.password_confirmation[0] : '';
                } else {
                    this.first_name.errors.message = '';
                    this.last_name.errors.message = '';
                    this.email.errors.message = '';
                    this.password.errors.message = '';
                    this.password_confirmation.errors.message = '';
                }
            },
            handleErrorSecond: function(response) {
                if (response.status === 422) {
                    this.step = (typeof response.data.first_name != 'undefined' || typeof response.data.last_name != 'undefined' || typeof response.data.email != 'undefined' || typeof response.data.password != 'undefined' || typeof response.data.password_confirmation != 'undefined') ? 'first' : 'second';

                    this.first_name.errors.message = typeof response.data.first_name != 'undefined' ? response.data.first_name[0] : '';
                    this.last_name.errors.message = typeof response.data.last_name != 'undefined' ? response.data.last_name[0] : '';
                    this.email.errors.message = typeof response.data.email != 'undefined' ? response.data.email[0] : '';
                    this.password.errors.message = typeof response.data.password != 'undefined' ? response.data.password[0] : '';
                    this.password_confirmation.errors.message = typeof response.data.password_confirmation != 'undefined' ? response.data.password_confirmation[0] : '';

                    this.security_questions.errors.message = typeof response.data.security_questions != 'undefined' ? response.data.security_questions[0] : '';
                    this.care_type.errors.message = typeof response.data.care_type != 'undefined' ? response.data.care_type[0] : '';
                    this.gender.errors.message = typeof response.data.gender != 'undefined' ? response.data.gender[0] : '';
                    this.security_questions_answer.errors.message = typeof response.data.security_questions_answer != 'undefined' ? response.data.security_questions_answer[0] : '';
                    this.post_code.errors.message = typeof response.data.post_code != 'undefined' ? response.data.post_code[0] : '';
                    this.hourly_rate.errors.message = typeof response.data.hourly_rate != 'undefined' ? response.data.hourly_rate[0] : '';
                } else {
                    this.first_name.errors.message = '';
                    this.last_name.errors.message = '';
                    this.email.errors.message = '';
                    this.password.errors.message = '';
                    this.password_confirmation.errors.message = '';
                    this.post_code.errors.message = '';
                    this.hourly_rate.errors.message = '';
                }
            },
            setRole: function(set) {
                this.role = set;
            },
            setRate: function() {
                if (this.hourly_rate.value.length > 6) {
                    this.hourly_rate.value = this.hourly_rate.value.substr(0, 6);
                }

                if (Number(this.hourly_rate.value)) {
                    this.hourly_rate.errors.message = ""
                } else {
                    this.hourly_rate.errors.message = "The hourly rate is invalid."
                }
            },
            genderChange: function(){
                    console.log(this.gender.value);
                    this.gender.errors.message = '';
                    if(this.gender.value == 3){
                        this.showDescribe = true;
                    }else{
                        this.showDescribe = false;
                    }
            }
        },
        computed: {
            formDataFirst: function() {
                return {
                    first_name: this.first_name.value,
                    last_name: this.last_name.value,
                    email: this.email.value,
                    password: this.password.value,
                    password_confirmation: this.password_confirmation.value
                }
            },
            formDataSecond: function() {
                return {
                    first_name: this.first_name.value,
                    last_name: this.last_name.value,
                    email: this.email.value,
                    password: this.password.value,
                    password_confirmation: this.password_confirmation.value,
                    role: this.role,
                    security_questions: this.security_questions.value,
                    care_type: this.care_type.value,
                    gender: this.gender.value,
                    security_questions_answer: this.security_questions_answer.value,
                    post_code: this.post_code.value,
                    hourly_rate: this.hourly_rate.value
                }
            },
            getStepText: function () {
                return this.step == 'first' ? 'Next Step' : 'Sign Up';
            }
        },
        ready: function() {
            var _this = this;
            jQuery('[name="hourly_rate"]').blur(function(){
                _this.hourly_rate.value = parseFloat(_this.hourly_rate.value);

                if (_this.hourly_rate.value > 1000) {
                    _this.hourly_rate.value = 999;
                }

                _this.hourly_rate.value = _this.hourly_rate.value.toFixed(2);

                if (Number(_this.hourly_rate.value)) {
                    _this.hourly_rate.errors.message = "";
                } else {
                    _this.hourly_rate.value = "0.00";
                    _this.hourly_rate.errors.message = "The hourly rate is invalid.";
                }
            });

            this.genderValue = [
                {"value":1, "text": "Male"},
                {"value":5, "text": "Female"},
                {"value":2, "text": "Transgender"},
                {"value":3, "text": "Self-describe as..."},
                {"value":4, "text": "Prefer not to say"},
            ];

        }
    });
}