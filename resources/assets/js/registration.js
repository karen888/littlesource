import Registration from './components/Registration.vue';
import ParentRegistrar from './components/Registration/ParentRegistrar.vue';
import CaregiverRegistrar from './components/Registration/CaregiverRegistrar.vue';
import RoleSelector from './components/Registration/RoleSelector.vue';
import EmailConfirmation from './components/Registration/EmailConfirmation.vue';
import DefaultRegistrar from './components/Registration/DefaultRegistrar.vue';
window.VueRouter = require('vue-router');
Vue.use(VueRouter);

let router = new VueRouter({history:true});

router.map({
    '/register': {
        //component: RoleSelector,
        component: DefaultRegistrar
    },
    '/register/caregiver': {component: CaregiverRegistrar},
    '/register/parent': {component: ParentRegistrar},
    '/register/email': {component: EmailConfirmation}
});

router.start(Registration, '#registration');