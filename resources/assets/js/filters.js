module.exports = function () {

    /**
     * Toggles the active class for the list items
     * TODO: add values to input for filter posting
     */
    $('.FindCaregivers__filter:not(.FindCaregivers__filter--qualifications) li').on('click', function() {
        $(this).parent().find('li').removeClass('active');
        $(this).addClass('active');
    });

    /**
     * Toggles the advanced filters in Find Caregivers Section
     */
    $('.FindCaregivers__toggle-filters a').on('click', function() {
        $('.FindCaregivers__filters--hidden').toggle();
        if($('.FindCaregivers__filters--hidden:visible').length != 0) {
            $('.FindCaregivers__toggle-filters a').text('Show Advanced Filters');
        } else {
            $('.FindCaregivers__toggle-filters .Hide').text('Hide Advanced Filters');
        }
    })    
};