module.exports = {

    /**
     * Opens the actionGroup
     */
    createActionGroups: function() {
        $('.actions-group').each(function(){
            var actionGroup=$(this),t=$('.text',actionGroup),trigger=$('.trigger',actionGroup),ex=$('.list',actionGroup);

            trigger.click(function() {
                // alert('works');
                actionGroup.toggleClass('open');
                actionGroup.addClass('open');
            });

            $(document).click(function(e){
                // Closes Action group on click outside
                var th=$(e.target);
                if(!th.closest('.actions-group').length) {
                    actionGroup.removeClass('open');
                } else if (!th.closest('.actions-group').is(actionGroup)) {
                    actionGroup.removeClass('open');
                }
            });

            $('.option',ex).click(function(){
                actionGroup.removeClass('open');
                sel.val($(this).data('value'));
                t.html($(this).html());
                var data=$(this).data('value');
            });
        });
    },

    custom_select: function() {
        this.custom_select_create();
        var mobile = this.is_mobile();

        if(mobile === true) {
            $('.customselect').removeClass('v2').addClass('v1');
        }

        $('.customselect.v1').each(function(){
            var c=$(this),sel=$('select',c),t=$('.text',c);
            sel.change(function(){
                t.html(sel.children('option:selected').text());
                $('.artist_products_block .list li[data-value="'+sel.val()+'"]').click();
            });
        });

        $('.customselect.v2').each(function(){
            var c=$(this),sel=$('select',c),t=$('.text',c),tr=$('.trigger',c),ex=$('.list',c);
            tr.click(function(){
                if(c.hasClass('open')) {
                    c.removeClass('open');
                } else {
                    c.addClass('open');
                }
            });
            $(document).click(function(e){
                var th=$(e.target);
                if(!th.closest('.customselect').length) {
                    c.removeClass('open');
                } else if (!th.closest('.customselect').is(c))
                {
                    c.removeClass('open');
                }
            });
            $('.option',ex).click(function(){
                c.removeClass('open');
                sel.val($(this).data('value'));
                t.html($('.option-name', this).html());
                var data=$(this).data('value');
                $('.artist_products_block .list li[data-value="'+data+'"]').click();
            });

            // Custom select "selected" option (usage - data-selected=1/0)
            $.each($("select option"), function (index, option){
                if($(option).data('selected') == 1){
                    $(option).prop('selected', 1);
                    $.each($(".option", ex), function(index, opt) {
                        if($(opt).data('selected')==1)
                        {
                            t.html($(opt).html());
                        }
                    });
                };
            });

            if( mobile ){
                c.addClass('mobile');

                sel.on("change", function(){
                    $('.option[data-value="'+$(this).val()+'"]',ex).click();
                });
            }
        });
    },

    custom_select_create: function() {
        $('.customselect .create').each(function(){
            var th = $(this);
            var parent = th.parent();
            var selected = th.children('option:selected');
            parent.append('<div class="trigger"><div class="text_outer"><div class="text">'+selected.text()+'</div></div></div>');
            parent.append('<div class="list"></div>');

            th.children('option').each(function(){
                parent.children('.list').append('<div class="option" data-value="'+$(this).val()+'">'+$(this).text()+'</div>');
            });
        });
    },

    init_uninitialised_custom_selects: function() {
        $('.customselect.v2.uninitialised').each(function(){
            var c=$(this),sel=$('select',c),t=$('.text',c),tr=$('.trigger',c),ex=$('.list',c);
            tr.click(function(){
                if (c.hasClass('open')) {
                    c.removeClass('open');
                } else {
                    c.addClass('open');
                }
            });

            $(document).click(function(e){
                var th=$(e.target);
                if(!th.closest('.customselect').length) {
                    c.removeClass('open');
                } else if(!th.closest('.customselect').is(c)) {
                    c.removeClass('open');
                }
            });

            $('.option',ex).click(function(){
                c.removeClass('open');
                sel.val($(this).data('value'));
                t.html($(this).html());
                var data=$(this).data('value');
                $('.artist_products_block .list li[data-value="'+data+'"]').click();
            });

            $(this).removeClass('uninitialised');
        });
    },

    is_mobile: function() {
        var devices = ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'];
        var ua = navigator.userAgent || navigator.vendor || window.opera;

        // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
        for (var i = 0; i < devices.length; i++) if (ua.toString().toLowerCase().indexOf(devices[i].toLowerCase()) > 0) return devices[i];
        return false;
    },
};