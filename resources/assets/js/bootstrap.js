window._ = require('lodash');

try {
    window.$ = window.jQuery = require('jquery');
    window.popover = require('popper.js');
    require('bootstrap-sass');
    require('intl-tel-input');
    require('cropper');
} catch (e) {}

window.csrf_token = $('meta[name="csrf-token"]').attr('content');

window.Vue = require('vue');

import VueMask from 'v-mask';
require('gasparesganga-jquery-loading-overlay');

Vue.use(require('vue-resource'));
Vue.use(VueMask);
Vue.http.headers.common['X-CSRF-TOKEN'] = csrf_token;

$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': csrf_token }
});

Vue.config.debug = true;
Vue.config.devtools = false;
Vue.config.productionTip = false;