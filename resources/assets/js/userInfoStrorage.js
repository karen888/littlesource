export const InfoStorage = new Vue({
    data() {
        return {
            userInfo: null,
            criterias: null,
            nameOwnGender: 'Self-describe as...',
            ownGender: {
                key: 0,
                index: 0
            },
            listCriterias: [
                'skill',
                'qualification',
                'gender',
                'employment_type',
                'child_age',
                'service',
                'location_range',
                'max_per_booking',
                'years_experience',
                'smoker',
                'own_children',
                'pets',
                'has_own_car',
                'availability'
            ],
        }
    },
    methods: {
        requestUserInfo() {
            return new Promise((resolve, reject) => {
                this.$http.get('/user/settings/my-info').then((response) => {
                    let userInfo = response.data;
                    if (userInfo.qualification){
                        if (!userInfo.qualification.length) {
                            userInfo.qualification.push({data: {}});
                        }
                    }

                    userInfo.phone = userInfo.phone || {};
                    userInfo.youtube_upload = userInfo.youtube_upload || { video_id:false };
                    userInfo.location = userInfo.location || {};
                    userInfo.max_per_booking[0] = userInfo.max_per_booking[0] || {};
                    userInfo.years_experience[0] = userInfo.years_experience[0] || {};
                    userInfo.location_range[0] = userInfo.location_range[0] || {};
                    userInfo.smoker[0] = userInfo.smoker[0] || {};
                    userInfo.gender[0] = userInfo.gender[0] || {};
                    userInfo.own_children[0] = userInfo.own_children[0] || {};
                    userInfo.pets[0] = userInfo.pets[0] || {};
                    userInfo.has_own_car[0] = userInfo.has_own_car[0] || {};
                    userInfo.references[0] = userInfo.references[0] || {};
                    userInfo.passport_id = userInfo.passport_id || { to_check: false };;
                    userInfo.driver_licence_id = userInfo.driver_licence_id || { to_check: false };
                    userInfo.government_id = userInfo.government_id || {};
                    userInfo.secondary_id = userInfo.secondary_id || {};
                    userInfo.wwcc = userInfo.wwcc || {};
                    userInfo.user_photo_100 = userInfo.user_photo_100 || '/img/no_photo_100.jpg';

                    // ------- test data---------
                    userInfo.rating = {
                        value: 0.0,
                        reviews: 0
                    };
                    // userInfo.jobs = [
                    //     {
                    //         id: '',
                    //         parent_name: 'Max O.',
                    //         initials: 'M',
                    //         distance: '9 km away',
                    //         posted: 'Posted 16 minutes ago',
                    //         required_now: true,
                    //         favourite: true,
                    //         description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec euismod ipsum. Maecenas ullamcorper sollicitudin lacus, ac gravida nisl dignissim a. Sed sed dolor justo. Duis ac libero a massa luctus malesuada eu in elit. Suspendisse feugiat eros vitae turpis pretium, nec venenatis magna cursus.',
                    //         requirements: {
                    //             children: '1 Child',
                    //             age: '13 years old',
                    //             car: 'Needs own car',
                    //             care_type: 'Regular caregiving',
                    //         },
                    //         payment_verified: true,
                    //         date: '12/08/2017'
                    //     },
                    //     {
                    //         id: '',
                    //         parent_name: 'Gabriel K.',
                    //         initials: 'G',
                    //         distance: '10 km away',
                    //         posted: 'Posted 30 minutes ago',
                    //         required_now: true,
                    //         favourite: true,
                    //         description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec euismod ipsum. Maecenas ullamcorper sollicitudin lacus, ac gravida nisl dignissim a. Sed sed dolor justo. Duis ac libero a massa luctus malesuada eu in elit. Suspendisse feugiat eros vitae turpis pretium, nec venenatis magna cursus.',
                    //         requirements: {
                    //             children: '1 Child',
                    //             age: '13 years old',
                    //             car: 'Needs own car',
                    //             care_type: 'Regular caregiving',
                    //         },
                    //         payment_verified: true,
                    //         date: '15/12/2017'
                    //     },
                    //     {
                    //         id: '',
                    //         parent_name: 'Mark T.',
                    //         initials: 'M',
                    //         distance: '15 km away',
                    //         posted: 'Posted 45 minutes ago',
                    //         required_now: true,
                    //         favourite: false,
                    //         description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec euismod ipsum. Maecenas ullamcorper sollicitudin lacus, ac gravida nisl dignissim a. Sed sed dolor justo. Duis ac libero a massa luctus malesuada eu in elit. Suspendisse feugiat eros vitae turpis pretium, nec venenatis magna cursus.',
                    //         requirements: {
                    //             children: '1 Child',
                    //             age: '13 years old',
                    //             car: 'Needs own car',
                    //             care_type: 'Regular caregiving',
                    //         },
                    //         payment_verified: true,
                    //         date: '03/05/2017'
                    //     },
                    //     {
                    //         id: '',
                    //         parent_name: 'Mark D.',
                    //         initials: 'A',
                    //         distance: '25 km away',
                    //         posted: 'Posted 50 minutes ago',
                    //         required_now: true,
                    //         favourite: true,
                    //         description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed nec euismod ipsum. Maecenas ullamcorper sollicitudin lacus, ac gravida nisl dignissim a. Sed sed dolor justo. Duis ac libero a massa luctus malesuada eu in elit. Suspendisse feugiat eros vitae turpis pretium, nec venenatis magna cursus.',
                    //         requirements: {
                    //             children: '1 Child',
                    //             age: '13 years old',
                    //             car: 'Needs own car',
                    //             care_type: 'Regular caregiving',
                    //         },
                    //         payment_verified: true,
                    //         date: '11/01/2018'
                    //     },
                    // ];
                    //-----------------------

                    this.userInfo = userInfo;
                    resolve(userInfo)
                }, (response) => {
                    reject(response);
                });
            });
        },
        requestCriterias(){
            return new Promise((resolve, reject) => {
                $.LoadingOverlay('show');
                this.$http.get('/api/v1/common/criterias/', {criterias: this.listCriterias}).then((response) => {
                    this.criterias = response.data;
                    this.initOwnGender();
                    resolve(response.data);
                    $.LoadingOverlay('hide');
                }, (response) => {
                    reject(response);
                });
            });
        },
        initOwnGender(){
            if(this.criterias.gender){
                this.criterias.gender.map((c, i) => {
                    if(c.name === this.nameOwnGender){
                        this.ownGender.key = c.id;
                        this.ownGender.index = i;
                    }
                });
            }
        }
    },
    created() {
        this.requestUserInfo().then((val) => {
            this.userInfo = val;
            this.$emit('userInfoChanged', val);
        });
        this.requestCriterias().then((val) => {
            this.criterias = val;
            this.$emit('criteriasChanged', val);
        });
    }
});