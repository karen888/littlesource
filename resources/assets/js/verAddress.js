import Vue from 'vue';
Vue.use(require('vue-resource'));
import $ from 'jquery';
import Pikaday from 'pikaday';
import CustomSelect from './components/CustomSelectStringArray.vue';
import CustomSelect2 from './components/CustomSelect.vue';
import Inputmask from "inputmask";
import FileUploader from './components/FileUploader.vue';

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;
if (document.getElementById('adress-verifed')) {
    var vm = new Vue({
        el: '#adress-verifed',
        data: {
            form: {
                loc_adress:{
                    error: '',
                    text: ''
                },
                loc_adress2:{
                    error: '',
                    text: '',
                    states: [
                        'SA',
                        'TAS',
                        'VIC',
                        'WA',
                        'NSW',
                        'QLD',
                        'NT',
                        'ACT',

                    ]
                },
                loc_city:{
                    error: '',
                    text: ''
                },
                loc_zip:{
                    error: '',
                    text: ''
                },
                loc_date:{
                    error: '',
                    text: ''
                },
                loc_type_id: {
                    text: '',
                    error:"",
                    selected: ""
                },
                issue_date: {
                    text: '',
                    error: ''
                },
                expiry_date: {
                    text: '',
                    error: ''
                },
                photo:{
                    error: ""
                },
                notes: {
                    error:'',
                    text:''
                }
            },
            image: '',
            scan_type: 'file',
            documentType:'',
            loading: {
                image: false
            }
        },
        methods: {
            onFileUploaded: function(prop_name, remote_file) {
                this[prop_name] = remote_file;
            },
            detectScanType() {
                // If the image is null or undefined.
                if (!this.image) {
                    return;
                }

                // Detect whether file is image
                if( this.image.indexOf("JFIF") !== -1 ||
                    this.image.indexOf("base64") !== -1 ||
                    this.image.indexOf("PNG") !== -1 ||
                    this.image.indexOf("GIF") !== -1 ||
                    this.image.indexOf("BMP") !== -1 ||
                    this.image.indexOf("jpeg") !== -1 ||
                    this.image.indexOf("JPEG") !== -1 ||
                    this.image.indexOf("gif") !== -1 ||
                    this.image.indexOf("bmp") !== -1 ||
                    this.image.indexOf("jpg") !== -1 ||
                    this.image.indexOf("JPG") !== -1 ||
                    this.image.indexOf("png") !== -1
                ){
                    this.scan_type = 'image';
                }else{
                    this.scan_type = 'file';
                }
            },
            postForm(){
                jQuery("body").LoadingOverlay("show");
                if(this.loading.image) {
                    var _this = this;

                    var postFormInterval = window.setInterval(()=>{
                        if(!_this.loading.image ) {
                            window.clearInterval(postFormInterval);
                            _this.postForm();
                        }
                    },200);

                    return;
                }

                var error = false;


                if(this.image == '' || this.image === null) {
                    this.form.photo.error = 'This is a required field';
                    error = true;
                }



                if(this.form.loc_type_id.selected == 0) {
                    this.form.loc_type_id.error = 'This is a required field';

                    error = true;
                }

                if(this.form.loc_adress2.text == 'Please select' || this.form.loc_adress2.text == '') {
                    this.form.loc_adress2.error = 'This is a required field';
                    error = true;
                }

                if(this.form.loc_adress.text == '') {
                    this.form.loc_adress.error = 'This is a required field';
                    error = true;
                }

                if(this.form.loc_city.text == '') {
                    this.form.loc_city.error = 'This is a required field';
                    error = true;
                }

                if(this.form.loc_zip.text == '') {
                    this.form.loc_zip.error = 'This is a required field';
                    error = true;
                }

                // if(this.form.loc_date.text == '') {
                //     this.form.loc_date.error = 'Date of issuing is required';
                //     error = true;
                // }



                if(error === true) {
                    jQuery("body").LoadingOverlay("hide");
                    return;
                }

                var formData = {
                    loc_adress: this.form.loc_adress.text,
                    loc_adress2: this.form.loc_adress2.text,
                    loc_city: this.form.loc_city.text,
                    loc_zip: this.form.loc_zip.text,
                    loc_scan: this.image,
                    loc_type_id: this.form.loc_type_id.selected,
                    loc_type_text: this.form.loc_type_id.text,
                    loc_date: this.form.loc_date.text,
                    loc_issue_date: this.form.issue_date.text,
                    loc_expiry_date: this.form.expiry_date.text,
                    address_notes: this.form.notes.text,
                };
                this.$http.put('/api/v1/settings/save-adress', formData)
                    .then( function(response) {
                            // this.$dispatch('notification', {type: response.data.notification.type, 'message': response.data.notification.message, 'close': false});
                        window.location.href = "/user/settings/get-verified";
                        }, function(response) {
                        jQuery("body").LoadingOverlay("hide");
                            for (var key in response.data) {
                                this.form[key].error = 'This is a required field';
                            }
                            // on unknown error
                            if (response.status === 500) {
                                this.cancel();
                                // this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                            }
                        }
                    );
            },
            uploadCompat(target) {
                var that = this;
                if(typeof window.uploader != "undefined") {
                    window.uploader.uploadCallback = null;
                    window.uploader.close();
                }
                window.uploader = window.open('/compatibility/upload' + ((target == 'avatar') ? '?avatar=1' : '?nocompress'),'upload', "width=500,height=350,resizable=0,toolbar=0,directories=0");
                $(window).on('beforeunload', function(){
                    uploader.close();
                });
                var uploaderInterval = setInterval(function(){
                    uploader.uploadCallback = function(response) {

                        that.loading.image= false;
                        that.image = response;
                        that.detectScanType();
                        uploader.close();
                        clearInterval(uploaderInterval);
                    }
                }, 100);

            },
            onFileChange(e) {
                return;
                // No continue...
                /*var files = e.target.files || e.dataTransfer.files;
                if (!files.length)
                    return;

                var that = this;

                var data = new FormData();
                $.each(files, function(key, value)
                {
                    data.append(key, value);
                });

                this.loading.image = true;

                this.form.photo.error = '';
                $.ajax({
                    xhr: function()
                    {
                        var xhr = new window.XMLHttpRequest();
                        // прогресс загрузки на сервер
                        xhr.upload.addEventListener("progress", function(evt){
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                // делать что-то...
                                console.log("upload:"+percentComplete);
                                var prg = Math.round(percentComplete * 100);

                                $('#'+e.target.name+'progress').attr('aria-valuenow', prg)
                                    .css({width:prg+'%'})
                                    .html(prg+'%');
                            }
                        }, false);
                        return xhr;
                    },
                    url: '/api/v1/upload/image',
                    type: 'POST',
                    data: data,
                    cache: false,
                    dataType: 'json',
                    processData: false, // Don't process the files
                    contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                    success: function(data, textStatus, jqXHR)
                    {
                        if(typeof data.error === 'undefined')
                        {
                            // Success so call function to process the form

                            that.loading.image= false;
                            that.image = data.image_path;

                            that.detectScanType();

                        }
                        else
                        {
                            // Handle errors here
                            console.log('ERRORS: ' + data.error);
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown)
                    {
                        // Handle errors here
                        console.log('ERRORS: ' + textStatus);
                        that.loading.image = false;
                    }
                });*/

            },
            createImage(file) {
                var image = new Image();
                var reader = new FileReader();
                var vm = this;

                reader.onload = (e) => {
                    vm.image = e.target.result;
                };
                reader.readAsDataURL(file);
                this.form.photo.error = '';
            },
            getDocumentType(){
                this.documentType = [
                    {"value":1, "text": "Land Titles Office records"},
                    {"value":2, "text": "Mortgage documents"},
                    {"value":3, "text": "Car registration"},
                    {"value":4, "text": "Utility bill"},
                    {"value":5, "text": "Bank statement"},
                    {"value":6, "text": "Council rates notice"},
                    {"value":7, "text": "Driver's license"},
                    {"value":8, "text": "Insurance renewal document"},
                    {"value":10, "text": "Government Issued ID"},
                    {"value":9, "text": "Other"},
                ]
            },
            setUserInfo: function (data) {
                this.form.loc_adress.text = data.loc_adress;
                if($.inArray(data.loc_adress2, this.form.loc_adress2.states) === -1) {
                    this.form.loc_adress2.text = '';
                    // this.form.loc_adress2.text = data.loc_adress2;
                } else {
                    this.form.loc_adress2.text = data.loc_adress2.length ? data.loc_adress2 : '';
                }
                // this.form.loc_adress2.text = data.loc_adress2;
                this.form.loc_city.text = data.loc_city;
                this.form.loc_zip.text = data.loc_zip;
                this.image = data.loc_scan;
                this.form.loc_type_id.selected = data.loc_type_id;
                this.form.loc_type_id.text = data.loc_type_text;
                this.form.loc_date.text = data.loc_date;
                this.form.notes.text = data.address_notes;
                this.form.issue_date.text = data.loc_issue_date;
                this.form.expiry_date.text = data.loc_expiry_date;


                this.detectScanType();
            }
        },
        components: {
            'file-uploader': FileUploader,
            'custom-select': CustomSelect,
            'custom-select2': CustomSelect2
        },
        ready(){
            this.getDocumentType();
            var date = new Date();
            // var picker = new Pikaday({
            //     field: document.getElementById('dateis'),
            //     maxDate: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
            //     yearRange: [1950, date.getFullYear()]
            // });
            this.$http.get('/api/v1/settings/user-info').then(function(response) {
                this.setUserInfo(response.data);

            }, function() {
                this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);



        }
    });
}