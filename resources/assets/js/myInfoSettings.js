import Vue from 'vue';
import Notifications from './components/Notifications.vue';
import ProfilePicture from './components/Settings/ProfilePicture.vue';
import AddressSettings from './components/Settings/Address.vue';
import EmailSettings from './components/Settings/Email.vue';
import TimezoneSettings from './components/Settings/Timezone.vue';
import PhoneSettings from './components/Settings/Phone.vue';
import AbnSettings from './components/Settings/ABN.vue';
import NameSettings from './components/Settings/Name.vue';
import BirthdaySettings from './components/Settings/Birthday.vue';
import $ from 'jquery';

Vue.use(require('vue-resource'));
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;

/*
    For password and security Settings
 */
if (document.getElementById('my-info')) {

    var vm = new Vue({
        el: '#my-info',
        data: {
            delete_photo: false,
            displayNotification: false,
            notificationValues: null,
            displayForm : {
                profile_picture: false,
                address: false,
                email: false,
                timezone: false,
                phone: false,
                vat_id: false,
                abn_number: false,
                name: false,
                birthday: false
            },
            userInfo: ''
        },
        components: {
            'notifications': Notifications,
            'profile-picture': ProfilePicture,
            'address-settings': AddressSettings,
            'email-settings': EmailSettings,
            'timezone-settings': TimezoneSettings,
            'phone-settings': PhoneSettings,
            'abn-settings': AbnSettings,
            'name-settings': NameSettings,
            'birthday-settings': BirthdaySettings
        },
        methods: {
            handleNotification: function (notification) {
                this.notificationValues = notification;
                this.displayNotification = !notification.close;
            },

            currentOpenForm: function (form) {
                this.$broadcast('clear-form');
                for (var key in this.displayForm) {

                    if (key == Object.keys(form)[0] ) {
                        $('html, body').animate({
                            scrollTop: $("." + key +"-template").offset().top 
                        }, 400);
                        this.displayForm[key] = form[Object.keys(form)[0]];
                    } else {
                        this.displayForm[key] = !form[Object.keys(form)[0]];
                    }
                }
            },

            closeAllForms: function () {
                this.$broadcast('clear-form');
                for (var key in this.displayForm) {
                    this.displayForm[key] = false;
                }
            },

            setUserInfo: function (data) {
                this.userInfo = data;
            }
        },
        created: function () {
            this.$http.get('/api/v1/settings/user-info').then(function(response) {
                this.setUserInfo(response.data);
            }, function() {
                this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
            }).bind(this);
        }
    });
}
