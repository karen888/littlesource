module.exports = {
    applicants: [],
    
    setApplicantsForJob: function(result) {
        this.applicants = result;
    },
    
    getApplicants: function() {
        return this.applicants.filter(function(applicant) {
            return !applicant['is_hidden'];
        });
    },

    getShortlistedApplicants: function() {
        return this.applicants.filter(function(applicant) {
            return applicant['is_shortlisted'];
        });
    },
    
    getHiddenApplicants: function() {
        return this.applicants.filter(function(applicant) {
            return applicant['is_hidden'];
        });
    },
    
    getMessagedApplicants: function() {
        return this.applicants.filter(function(applicant) {
            return applicant['messaged'];
        });
    },

    get: function(filter) {
        switch(filter)  {
            case 'shortlisted':
                return this.getShortlistedApplicants();
            case 'hidden':
                return this.getHiddenApplicants();
            case 'messaged':
                return this.getMessagedApplicants();
            default:
                return this.getApplicants();
        }
    },

    toggleHide: function(applicant) {

    }
};
