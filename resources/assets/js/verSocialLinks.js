import Vue from 'vue';
Vue.use(require('vue-resource'));
import $ from 'jquery';
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
Vue.config.debug = true;


if (document.getElementById('socialLinks')) {
    var vm = new Vue({
        el: '#socialLinks',
        data: {
            form: {
                soc_link:{
                    error: '',
                    text: ''
                },
                type:{
                    value: ""
                },
                notes: {
                    error:'',
                    text:''
                },
                soc_links_notes: [],
            },
            soc_links_additional: 0
        },
        methods: {
            saveLink(){
                jQuery("body").LoadingOverlay("show");

                var url = this.form.soc_link.text;

                if(!this.isValidUrl(url)) {
                    this.form.soc_link.error = 'Invalid entry';
                    jQuery("body").LoadingOverlay("hide");
                    return;
                }

                if(!this.isSocialLink(url)) {
                    this.form.soc_link.error = 'Invalid entry. Reason: Not a social link';
                    jQuery("body").LoadingOverlay("hide");
                    return;
                }

                var formData = {
                    soc_link: this.form.soc_link.text,
                    soc_link_notes: this.form.soc_links_notes,
                };
                this.$http.patch('/api/v1/settings/save-soclink', formData)
                    .then( function(response) {
                            // this.$dispatch('notification', {type: response.data.notification.type, 'message': response.data.notification.message, 'close': false});
                        window.location.href = "/user/settings/get-verified";
                        }, function(response) {
                        jQuery("body").LoadingOverlay("hide");
                            for (var key in response.data) {
                                this.form[key].error = response.data[key][0];
                            }
                            // on unknown error
                            if (response.status === 500) {
                                this.cancel();
                                // this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
                            }
                        }
                    );
            },
            addMoreLink: function() {
              if( this.form.soc_links_notes.length >= 4) {
                  return;
              }

              this.form.soc_links_notes.push({text:''});


            },
            removeRow: function(n) {

                this.form.soc_links_notes.splice(n, 1);
            },
            setUserInfo: function (data) {
                this.form.soc_link.text = data.soc_link;
                try {
                    this.form.soc_links_notes = JSON.parse(data.soc_links_notes);
                } catch(e) {
                    this.form.soc_links_notes = [];
                }

                if(this.form.soc_links_notes == null) {
                    this.form.soc_links_notes = [];
                    this.form.soc_links_notes = [];
                    this.form.soc_links_notes = [];
                    this.form.soc_links_notes = [];
                }

            },
            isValidUrl: function(url) {
                return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(url);
            },
            isSocialLink: function(url) {
                return url.indexOf('facebook.com') !== -1
                    ^ url.indexOf('plus.google.com') !== -1
                    ^ url.indexOf('twitter.com') !== -1
                    ^ url.indexOf('linkedin.com') !== -1;
            }
        },
        ready(){
        this.$http.get('/api/v1/settings/user-info').then(function(response) {
            this.setUserInfo(response.data);
        }, function() {
            this.$dispatch('notification', {type: 'danger', 'message': 'You have found an unknown error, try reloading your browser', 'close': false});
        }).bind(this);
    }
    });
}