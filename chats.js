var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

function stripTags(str) {
    return str.replace(/<\/?[^>]+>/gi, '');
}

io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('chat message', function(message) {
        message.text = stripTags(message.text);

        io.sockets.emit('chat message', message);
        io.sockets.emit('admin unread', message);

        //socket.emit('chat message', message);
       console.log(JSON.stringify(message));
    });
});

http.listen(3000, function(){
    console.log('listening on *:3000');
});