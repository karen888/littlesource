<?php

return Array(
    'client_id' 			=> env('GOOGLE_CLIENT_ID', null),
    'client_secret' 		=> env('GOOGLE_CLIENT_SECRET', null),
    'refresh_token' 		=> env('REFRESH_TOKEN', null),
    'scope'					=> 'https://www.googleapis.com/auth/youtube.upload',
    'redirect_uri'			=> 'http://www.littleones.com.au/'
);
