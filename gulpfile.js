var elixir = require('laravel-elixir');

elixir.config.publicPath = 'public';
elixir.config.js.browserify.watchify.options.poll = true;

require('laravel-elixir-vueify'); // Assets With Vue

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(function(mix) {
    mix.copy('resources/fonts/gotham-rounded', 'public/fonts/gotham-rounded');
    mix.copy('node_modules/intl-tel-input/src/css/intlTelInput.scss', 'resources/assets/sass/vendor/intlTelInput.scss');
    mix.copy('node_modules/intl-tel-input/src/css/sprite.scss', 'resources/assets/sass/vendor/sprite.scss');
    mix.copy('node_modules/intl-tel-input/src/css/sprite@2x.scss', 'resources/assets/sass/vendor/sprite@2x.scss');
    mix.copy('node_modules/intl-tel-input/src/css/sprite@2x.scss', 'resources/assets/sass/vendor/sprite@2x.scss');
    mix.copy('node_modules/intl-tel-input/src/css/sprite@2x.scss', 'resources/assets/sass/vendor/sprite@2x.scss');
    mix.copy('node_modules/intl-tel-input/build/img/flags.png','public/build/img/flags.png');
    mix.copy('node_modules/intl-tel-input/build/img/flags@2x.png','public/build/img/flags@2x.png');
    mix.copy('node_modules/intl-tel-input/build/js/utils.js', 'public/build/js/utils.js');
    mix.copy('resources/img', 'public/img');
    mix.sass(['new-app.scss'], 'public/css/new-app.css');
    mix.sass('profile/profile-builder-modals.scss', 'public/css/profile-builder-modals.css');
    mix.sass(['redefine_styles.scss'], 'public/css/redefine_styles.css');
    mix.browserify(['app.js'], 'public/js/new-app.js');
    mix.browserify(['registration.js'], 'public/js/registration.js');
    mix.browserify(['login-new.js'], 'public/js/login.js');
    mix.browserify(['password-email.js'], 'public/js/password-email.js');
    mix.browserify(['password-reset.js'], 'public/js/password-reset.js');
    mix.browserify(['set.js'], 'public/js/set.js');
    mix.browserify(['caregiver-layout.js'], 'public/js/caregiver-layout.js');
    mix.browserify(['parent-layout.js'], 'public/js/parent-layout.js');
    mix.browserify(['profile-edit.js'], 'public/js/profile-edit.js');

    mix.browserify(['main.js'], 'public/js/all.js');
    mix.version([
        'js/password-reset.js',
        'js/password-email.js',
        'js/caregiver-layout.js',
        'js/parent-layout.js',
        'js/login.js',
        'js/registration.js',
        'js/profile-edit.js',
        'js/set.js',
        'js/all.js',
        'js/new-app.js',
        'css/profile-builder-modals.css',
        'css/new-app.css'
    ]);
});