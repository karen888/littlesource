<?php

namespace App\Providers;

use App\Http\Models\DriverLicenceId;
use App\Http\Models\GovernmentId;
use App\Http\Models\PassportId;
use App\Http\Models\SecondaryId;
use App\Http\Models\Wwcc;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use App\Entities\Criteria\Base;
use phpDocumentor\Reflection\Types\Boolean;

class ValidationRulesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('criterias', function ($attribute, $value, $parameters, $validator) {
            if (is_array($value)) {
                $criterias = Base::whereIn('id', array_keys($value))->get()->keyBy('id');
                foreach ($value as $id => $data) {
                    $class = $criterias->get($id);
                    $validation_data = ['id' => $id];

                    if (isset($data['data']) && $this->isJson($data['data'])) {
                        $data = json_decode($data['data'], true);
                        $validation_data = is_array($data) ? array_merge($validation_data, $data) : $validation_data;
                        if (is_null($class) && isset($data['type'])) {
                            $class = App::make($data['type']);
                        }
                    }

                    $val = Validator::make($validation_data, $class->getRules(), $class->getMessages());
                    if ($val->fails()) {
                        $errors = $val->errors()->toArray();
                        foreach ($errors as $name => $error) {
                            $validator->errors()->add($class->getCriteriaName() . '.' . $id . '.' . $name, $error);
                        }
                    }
                }
            }
            return true;
        });

        Validator::extend('accepted_if_exist', function ($attribute, $value, $parameters, $validator) {
            if(!isset($value))
                return true;
            return $value;
        });

        Validator::extend('passport_id', function ($attribute, $value, $parameters, $validator) {
            if((Boolean)$value['to_check']){
                $this->validateModelProperties($value, PassportId::class, $validator);
            }
            return true;
        });

        Validator::extend('driver_licence_id', function ($attribute, $value, $parameters, $validator) {
            if((Boolean)$value['to_check']){
                $this->validateModelProperties($value, DriverLicenceId::class, $validator);
            }
            return true;
        });

        Validator::extend('government_id', function ($attribute, $value, $parameters, $validator) {
            $this->validateModelProperties($value, GovernmentId::class, $validator);
            return true;
        });

        Validator::extend('secondary_id', function ($attribute, $value, $parameters, $validator) {
            $this->validateModelProperties($value, SecondaryId::class, $validator);
            return true;
        });

        Validator::extend('wwcc', function ($attribute, $value, $parameters, $validator) {
            $this->validateModelProperties($value, Wwcc::class, $validator);
            return true;
        });

        Validator::extend('phone_number', function($attribute, $value, $parameters, $validator) {
            $value = str_replace(['+',' '], '', $value);
            if(!empty($value) && preg_match('/[0-9]{11}/', $value) && strlen($value) == 11){
                return true;
            }
            return false;
        });
    }

    public function register(){}

    public function isJson($string)
    {
        return $string !== 'null' && !preg_match('/[^,:{}\\[\\]0-9.\\-+Eaeflnr-u \\n\\r\\t]/',
                preg_replace('/"(\\.|[^"\\\\])*"/', '', $string));
    }

    private function validateModelProperties($value, $model, $validator){
        $val = Validator::make($value, $model::getRules(), $model::getMessages());
        if ($val->fails()) {
            $errors = $val->errors()->toArray();
            foreach ($errors as $name => $error) {
                $validator->errors()->add($this->getClassName($model) . '.' . $name, $error);
            }
        }
    }

    public function getClassName($model)
    {
        $class = explode('\\', $model);
        return snake_case(array_pop($class));
    }
}
