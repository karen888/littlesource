<?php namespace App\Providers;

use App\Entities\Criteria\Base;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        Blade::directive('audate', function ($expression) {
            return "<?php echo ($expression)->format('d/m/Y'); ?>";
        });
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{

		if ($this->app->environment() == 'local') {
	        $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
	    }

        if ($this->app->environment() !== 'production' && $this->app->environment() !== 'prod') {
//            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);
	}
}
