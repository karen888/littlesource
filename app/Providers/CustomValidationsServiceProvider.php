<?php

namespace App\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class CustomValidationsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Validates There's a selected day
         */
        Validator::extend('at_least_one_selected_day', function($attribute, $value, $parameters) {

            switch ($parameters[0]) {
                case null:
                    return false;
                    break;
                case '0':
                    return true;
                    break;
                case '1':
                    $daySelected = false;
                    foreach ($value as $day) {
                        if ($day['selected'] && $day['selectedFrom'] && $day['selectedTo']) {
                            $daySelected = true;
                        }
                    };
                    return $daySelected;
                    break;
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
