<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider {


    public function register() 
    {
        $path = app_path().'/Http/Exts/Helpers.php';
        require_once($path);
        //$this->app->bind('helpers', function() {
            //return new \App\Http\HelpersCustom;
        //});        
    }
}

