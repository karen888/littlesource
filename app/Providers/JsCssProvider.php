<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class JsCssProvider extends ServiceProvider {

	public function boot()
	{
        //\::resolver(function($translator, $data, $rules, $messages) {
            //return new ($translator, $data, $rules, $messages);
        //});        
	}

    public function register() 
    {
        $this->app->bind('JsCss', function() {
            return new \App\Http\Exts\JsCss;
        });
    }
}


