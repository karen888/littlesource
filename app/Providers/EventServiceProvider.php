<?php namespace App\Providers;

use App\Events\UserPrelaunchSignedUp;
use App\Events\CreateAccount;
use App\Events\ChangePassword;
use App\Events\ChangeEmail;
use App\Events\AddNotificationsData;
use App\Events\SetNotifications;
use App\Listeners\EmailPrelaunchConfirmation;
use App\Listeners\EmailAccountConfirmation;
use App\Listeners\EmailChangePassword;
use App\Listeners\EmailChangeEmail;
use App\Listeners\NotificationsData;
use App\Listeners\EmailNotifications;
use App\Events\FileUploadedEvent;
use App\Listeners\FileUploadedGuard;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        UserPrelaunchSignedUp::class => [
            EmailPrelaunchConfirmation::class,
        ],
        CreateAccount::class => [
            EmailAccountConfirmation::class
        ],
        ChangePassword::class => [
            EmailChangePassword::class
        ],
        ChangeEmail::class => [
            EmailChangeEmail::class
        ],
        AddNotificationsData::class => [
            NotificationsData::class
        ],
        SetNotifications::class => [
            EmailNotifications::class
        ],
        FileUploadedEvent::class => [
            FileUploadedGuard::class
        ],
    ];

    /**
    * Register any other events for your application.
    *
    * @param  DispatcherContract  $events
    */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
    }
}
