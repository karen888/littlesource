<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Entities\Criteria\Base;

class CriteriaServiceProvider extends ServiceProvider
{
    protected $defer = true;

    protected $criterias;

    public function __construct(\Illuminate\Contracts\Foundation\Application $app)
    {
        parent::__construct($app);
        $this->criterias = Base::subclasses();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        foreach ($this->criterias as $criteria => $class) {
            $this->app->bind($criteria, $class);
        }
    }

    public function provides()
    {
        return array_keys($this->criterias);
    }
}
