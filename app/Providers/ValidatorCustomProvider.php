<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidatorCustomProvider extends ServiceProvider {

	public function boot()
	{
        \Validator::resolver(function($translator, $data, $rules, $messages) {
            return new \App\Http\Exts\ValidatorCustom($translator, $data, $rules, $messages);
        });        
	}

    public function register() 
    {
    }
}
