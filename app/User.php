<?php namespace App;

use App\Entities\Criteria\CriteriaTrait;
use App\Entities\Message\MessageTrait;
use App\Http\Models\DriverLicenceId;
use App\Http\Models\Location;
use App\Http\Models\PassportId;
use App\Http\Models\Phone;
use App\Http\Models\GovernmentId;
use App\Http\Models\Reference;
use App\Http\Models\SecondaryId;
use App\Http\Models\VerificationChats;
use App\Http\Models\Wwcc;
use App\Http\Models\YoutubeUploads;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Entities\Offer\Offers;
use App\Entities\Job\Job;
use App\Entities\Job\JobBlockInvite;
use App\Entities\Employee\EmployeeInfo;
use App\Entities\Billing\Cards;
use App\Entities\SecurityQuestion\SecurityQuestions;
//use App\Http\Models\Applicants;
use App\Http\Models\UserInfo;
use App\Entities\Educations\Educations;
use App\Entities\Languages\Languages;
use App\Entities\Favorites\Favorites;
use App\Entities\Notifications\Notifications;
use App\Entities\Criteria\Base as UserCriteria;
use phpDocumentor\Reflection\Types\Boolean;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, MessageTrait, CriteriaTrait;

	/**
	 * The database table used by the model.
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 * @var array
	 */
	protected $fillable = ['first_name', 'last_name', 'email', 'password', 'check_agree', 'check_confirm', 'check_confirm_eligible'];

	/**
	 * The attributes excluded from the model's JSON form.
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	/**
	 * The attributes that should be casted to native types.
	 * @var array
	 */
  	protected $casts = [
        'is_admin' => 'boolean',
        'is_caregiver' => 'boolean',
        'user_available' => 'boolean',
        'check_agree' => 'boolean',
        'check_confirm' => 'boolean',
        'check_confirm_eligible' => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['jobs_count', 'photo_url'];

    /**
     * Get the jobs of the user.
     */
    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    /**
     * Get the jobs the user applied to.
     */
    public function appliedTo()
    {
        return $this->hasMany(Applicants::class);
    }

    /**
     * Get the user info for user
     */
    public function info()
    {
        return $this->hasOne(UserInfo::class);
    }

    /**
     * Get the user info for user
     */
    public function youtube_upload()
    {
        return $this->hasOne(YoutubeUploads::class);
    }

    /**
     * Get the user info for user
     */
    public function security_question()
    {
        return $this->hasOne(SecurityQuestions::class);
    }   

    /**
     * Get user notifications settings
     */
    public function notifications_settings()
    {
        return $this->hasOne(Notifications::class);
    }
    
    public function user_criteria()
    {
        return $this->belongsToMany(UserCriteria::class, 'criterias_users', 'user_id', 'criteria_id');
    }

    /**
     * Return the number of jobs posted by the user.
     * @return [type] [description]
     */
    public function jobsCount()
    {
        return $this->jobs()
            ->selectRaw('user_id, count(*) as aggregate')
            ->groupBy('user_id');
    }

    /**
     * Return the number of open jobs posted by the user.
     * @return [type] [description]
     */
    public function jobsOpenCount()
    {
        return $this->jobs()
            ->where('closed', false)
            ->selectRaw('user_id, count(*) as aggregate')
            ->groupBy('user_id');
    }

    public function getJobsOpenCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if ( ! array_key_exists('jobsOpenCount', $this->relations))
            $this->load('jobsOpenCount');
        $related = $this->getRelation('jobsOpenCount');
        // then return the count directly
        return (!$related->isEmpty()) ? (int) $related->first()->aggregate : 0;
    }

    public function getJobsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if ( ! array_key_exists('jobsCount', $this->relations))
            $this->load('jobsCount');
        $related = $this->getRelation('jobsCount');
        // then return the count directly
        return (!$related->isEmpty()) ? (int) $related->first()->aggregate : 0;
    }

    /**
     * Returns url for the user profile picture ( if user has no photo returns default profile pic)
     * @return [type] [description]
     */
    public function getPhotoUrlAttribute()
    {
        return userPhoto($this)->photo_100;
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function blockByEmployee()
    {
        return $this->belongsTo(JobBlockInvite::class, 'client_id');
    }

    public function employee_info()
    {
        return $this->hasOne(EmployeeInfo::class);
    }

    public function cards()
    {
        return $this->hasMany(Cards::class);
    }

    public function getCardsAttribute()
    {
        if ( ! array_key_exists('cards', $this->relations)) {
            $this->load('cards');
        }
        $related = $this->getRelation('cards');

        if ($related->isEmpty()) {
            return false;
        }

        $data = [];

        foreach ($related->all() as $item) {
            $text = str_split($item->card_number);
            $text = implode('', array_splice($text, -4));
            $data[] = [
                'text' => "Credit Card - Account ending in " . $text,
                'value' => $item->id
            ];
        }

        return $data;
    }

    public function getMedicareCard() {
        $data = [];

        if ($this->secondary_id) {
            switch ($this->secondary_id->card_type) {
                case 0:
                    $card_type_name = 'Green Card';
                    break;
                case 1:
                    $card_type_name = 'Blue Card';
                    break;
                case 2:
                    $card_type_name = 'Yellow Card';
                    break;
                default:
                    $card_type_name = '';
                    break;
            }
            $data = [
                'id' => $this->secondary_id->id,
                'card_name' => $this->secondary_id->card_names,
                'expiry_date' => $this->secondary_id->expiry_date,
                'id_number' => $this->secondary_id->id_number,
                'card_type' => $this->secondary_id->card_type,
                'card_type_name' => $card_type_name,
                'individual_ref_number' => $this->secondary_id->individual_ref_number,
                'verified' => $this->secondary_id->verified,
                'updated_at' => $this->secondary_id->updated_at,
            ];
        }

        return $data;
    }

    public function getPassport() {
        $data = [];

        if ($this->passport_id) {
            
            $data = [
                'id' => $this->passport_id->id,
                'photo' => $this->passport_id->photo,
                'id_number' => $this->passport_id->id_number,
                'year_only' => $this->passport_id->year_only,
                'verified' => $this->passport_id->verified,
                'updated_at' => $this->passport_id->updated_at,
            ];
        }

        return $data;
    }

    public function getDriverLicence() {
        $data = [];

        if ($this->driver_licence_id) {
            switch ($this->driver_licence_id->state_id) {
                case 1:
                    $state_name = 'ACT';
                    break;
                case 2:
                    $state_name = 'NT';
                    break;
                case 3:
                    $state_name = 'SA';
                    break;
                case 4:
                    $state_name = 'WA';
                    break;
                case 5:
                    $state_name = 'NSW';
                    break;
                case 6:
                    $state_name = 'QLD';
                    break;
                case 7:
                    $state_name = 'VIC';
                    break;
                case 8:
                    $state_name = 'TAS';
                    break;
                default:
                    $state_name = '';
                    break;
            }
            $data = [
                'id' => $this->driver_licence_id->id,
                'photo' => $this->driver_licence_id->photo,
                'id_number' => $this->driver_licence_id->id_number,
                'state_id' => $this->driver_licence_id->state_id,
                'year_only' => $this->driver_licence_id->year_only,
                'state_name' => $state_name,
                'verified' => $this->driver_licence_id->verified,
                'updated_at' => $this->driver_licence_id->updated_at,
            ];
        }

        return $data;
    }


    public function certificates()
    {
        return $this->belongsToMany(Certificates::class)->withTimestamps();
    }

    public function educations()
    {
        return $this->belongsToMany(Educations::class)->withTimestamps();
    }
    
    public function languages()
    {
        return $this->belongsToMany(Languages::class)->withTimestamps();
    }

    public function location(){
        return $this->hasOne(Location::class);
    }

    public function references(){
        return $this->hasMany(Reference::class);
    }

    public function government_id(){
        return $this->hasOne(GovernmentId::class);
    }

    public function passport_id(){
        return $this->hasOne(PassportId::class);
    }

    public function driver_licence_id(){
        return $this->hasOne(DriverLicenceId::class);
    }

    public function secondary_id(){
        return $this->hasOne(SecondaryId::class);
    }

    public function phone(){
        return $this->hasOne(Phone::class);
    }

    public function wwcc(){
        return $this->hasOne(Wwcc::class);
    }

    public function favorites()
    {
        return $this->hasMany(Favorites::class);
    }

    public function getEmployeeJobHistory()
    {
        $data = [];
        foreach (Offers::where(['to_user_id' => $this->id, 'status' => 'accept'])->get() as $offer) {
            array_push($data, [
                'title' => $offer->job->title,
                'description' => $offer->job->description,
                'offer_paid' => $offer->offer_paid,
                'start_date' => date('M j, Y', strtotime($offer->start_date)),
                'end_date' => $offer->job->getCloseDate(),
                'feedback' => $offer->job->getJobRatings($this->is_caregiver),
                'show' => false
            ]);
        }
        
        return $data;
    }

    public function getProgress(){
        $data = array(
            'photo' => array(
                'avl' => (Boolean)$this->user_photo_100, 'name' => 'Profile Photo',
                'step' => 1, 'percentage' => 15
            ),
            'name' => array(
                'avl' => (Boolean)$this->first_name, 'name' => 'First, Last Name',
                'step' => 1, 'percentage' => 2
            ),
            'dob' => array(
                'avl' => ($this->employee_info && $this->employee_info->birthday), 'name' => 'Date of Birth',
                'step' => 1, 'percentage' => 2
            ),
            'phone' => array(
                'avl' => $this->phone && $this->phone->phone, 'name' => 'Mobile Number',
                'step' => 1, 'percentage' => 5
            ),
            'address' => array(
                'avl' => $this->isValidAddress(), 'name' => 'Address',
                'step' => 1, 'percentage' => 5
            ),
            'video_url' => array(
                'avl' => (Boolean)$this->video_url, 'name' => 'Video Presentation',
                'step' => 2, 'percentage' => 15
            ),
            'video_id' => array(
                'avl' => (Boolean)$this->youtube_upload, 'name' => 'Video Presentation',
                'step' => 2, 'percentage' => 15
            ),
            'years_experience' => array(
                'avl' => (Boolean)count($this->yearsExperience), 'name' => 'Years of Experience',
                'step' => 2, 'percentage' => 5
            ),
            'more_about_me' => array(
                'avl'=> $this->isValidMoreAboutMe(), 'name' => 'More About Me',
                'step' => 2, 'percentage' => 5
             ),
            'what_i_offer' => array(
                'avl' => (Boolean)count($this->service), 'name' => 'What I Offer',
                'step' => 2, 'percentage' => 5
            ),
            'child_age' => array(
                'avl' => (Boolean)count($this->childAge), 'name' => 'Age Groups',
                'step' => 2, 'percentage' => 2
            ),
            'max_per_booking' => array(
                'avl' => (Boolean)count($this->maxPerBooking), 'name' => 'Maximum Children per Booking',
                'step' => 2, 'percentage' => 2
            ),
            'location_range' => array(
                'avl' => (Boolean)count($this->locationRange), 'name' => 'Maximum Distance Willing to Travel',
                'step' => 2, 'percentage' => 2
            ),
            'availability' => array(
                'avl' => (Boolean)count($this->availability), 'name' => 'Availability',
                'step' => 3, 'percentage' => 5
            ),
            'hourly_rate' => array(
                'avl' => ($this->employee_info && $this->employee_info->rate), 'name' => 'Hourly Rate',
                'step' => 3, 'percentage' => 5
            ),
            'qualifications' => array(
                'avl' => (Boolean)count($this->qualification), 'name' => 'Qualification',
                'step' => 4, 'percentage' => 5
            ),
            'references' => array(
                'avl' => (Boolean)count($this->references), 'name' => 'Reference',
                'step' => 4, 'percentage' => 10
            ),
            'identity_check' => array(
                'avl' => $this->isValidIdentityCheck(), 'name' => 'Identity Check',
                'step' => 5, 'percentage' => 5
            ),
            'id_photo' => array(
                'avl' => $this->isValidIdPhoto(), 'name' => 'Photo ID',
                'step' => 5, 'percentage' => 0
            ),
            'medicare_card' => array(
                'avl' => $this->isValidMedicareCcard(), 'name' => 'Medicare Card',
                'step' => 5, 'percentage' => 0
            ),
            'wwcc' => array(
                'avl' => $this->isValidWWCC(), 'name' => 'WWCC',
                'step' => 5, 'percentage' => 5
            ),
            'id_selfie' => array(
                'avl' => $this->government_id && $this->government_id->user_holding_document_photo, 'name' => 'ID Selfie',
                'step' => 5, 'percentage' => 0
            )
        );


        $fieldsProgressProfile = [
            'photo', 'name', 'dob', 'phone', 'address', 'video_url', 'years_experience',
            'more_about_me', 'what_i_offer', 'child_age', 'max_per_booking', 'location_range',
            'availability', 'hourly_rate', 'qualifications', 'references', 'identity_check', 'wwcc',
        ];

        $fieldsProgressVerification = [
            'name', 'dob', 'phone', 'address', 'qualifications', 'identity_check',
            'id_photo', 'medicare_card', 'wwcc', 'id_selfie'
        ];

        $sumPercentage = 0;
        $profile_completeness = array();
        $verification_requirements = array();
        $all_completeness = array();

        foreach ($data as $key => $value){
            $all_completeness[$key] = $value['avl'];
        }

        foreach ($fieldsProgressProfile as $key => $field){
            if($data[$field]['avl']){
                $sumPercentage += (int)$data[$field]['percentage'];
            }else{
                $profile_completeness[$key] = [
                    'name' => $data[$field]['name'],
                    'step' => $data[$field]['step'],
                    'percentage' => $data[$field]['percentage']
                ];
            }
        }

        $phoneIsValid = $this->phone && $this->phone->phone && $this->phone->verified;

        foreach ($fieldsProgressVerification as $key => $field){
            if($field === 'phone'){
                $data[$field]['name'] = $data[$field]['name'] . ' (Verification required)';
            }

            if(!$data[$field]['avl'] || ($field === 'phone' && !$phoneIsValid)){
                $verification_requirements[$key] = [
                    'name' => $data[$field]['name'],
                    'step' => $data[$field]['step']
                ];
            }
        }

        return array(
            'percentage_completeness' => $sumPercentage,
            'profile_completeness' => $profile_completeness,
            'verification_requirements' => $verification_requirements,
            'all_completeness' => $all_completeness
        );
    }

    /**
     * DVS API Verified
     *
     * @return bool
     */
    public function isDVSVerified(){
        return
            $this->secondary_id && $this->secondary_id->verified &&
            ($this->passport_id && $this->passport_id->verified && $this->passport_id->to_check ||
            $this->driver_licence_id && $this->driver_licence_id->verified && $this->driver_licence_id->to_check);
    }

    public function getEmployeeFeedback()
    {
        $counter = 0;
        $feedbacks = 0;
        
        foreach (Offers::where(['to_user_id' => $this->id, 'status' => 'accept'])->get() as $offer) {
            $rating = $offer->job->getJobRatings($this->is_caregiver);
            if ($rating['rating']) {
                $feedbacks += $rating['rating'];
                $counter++;
            }
        }

        return $counter ? ($feedbacks / $counter) : false;
    }
	
    public function getUserFeedback($user_id)
    {
        $counter = 0;
        $feedbacks = 0;
        // print $user_id."<br>";
        $offers = Offers::where(['from_user_id' => $user_id])->get();
        // print_r($offers);
        foreach ($offers as $offer) {
            $rating = $offer->job->getJobRatings($this->is_caregiver);
            if ($rating['rating']) {
                $feedbacks += $rating['rating'];
                $counter++;
            }
        }

        return $counter ? ($feedbacks / $counter) : false;
    }

    public function verificationChats()
    {
        return $this->hasMany(VerificationChats::class);
    }

    public function getVerificationChat($ver_type, $ver_id = null)
    {
        $q = $this->verificationChats()->where('verification_type', '=', $ver_type);

        if($ver_id !== null) {
            $q = $q->where('verification_id', '=', $ver_id);
        }

        return $q->get();
    }

    public function markVerificationChatAsAnswered($ver_type, $ver_id)
    {
        $q = $this->verificationChats()->where('verification_type', '=', $ver_type);

        if($ver_id !== null) {
            $q = $q->where('verification_id', '=', $ver_id);
        }

        $q->update(['is_answered'=>true]);
    }

    /**
     * Whether user is able to apply for job
     *
     * @return bool
     */
    public function canApplyToJob()
    {
        if ( isset($this->info) ) {
            return $this->info->wwcc_image != '' &&
                $this->info->prof_scan_id != '' &&
                $this->info->phone != '' &&
                $this->info->loc_city != '' &&
                $this->employee_info->overview != '' && count($this->educations) &&
                $this->allEducationsSubmitted();
        }
    }

    /**
     * Whether all educations are verified
     *
     * @return bool
     */
    public function allEducationsVerified() {
        $educations = $this->educations;

        $result = true;

        // If no educations submitted - means user is not verified
        if(!count($educations)) {
            $result = false;
        }

        // Each education should be verified
        foreach($educations as $education) {
            if($education->status != 1) {
                $result = false;
                break;
            }
        }

        return $result;
    }

    /**
     * Whether all educations submitted and not rejected
     *
     * @return bool
     */
    public function allEducationsSubmitted()
    {
        $educations = $this->educations;

        // By default - false
        $result = false;

        foreach($educations as $education) {
            // If education verified - true
            if($education->status == 1) {
                $result = true;
            }
            // If education rejected - false and exit
            elseif ($education->status == 2) {
                $result = false;

                break;
            }
            // If educations submitted - true
            elseif($education->status == 0) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Whether user is able to get hired and start contract
     *
     *
     * @return bool
     */
    public function isUserFullyVerified()
    {
        $result = $this->canApplyToJob() &&
            $this->verifed_id == 1 &&
            $this->address_status == 1 &&
            $this->phone_verifed == 1 &&
            $this->wwcc_status == 1 &&
            $this->allEducationsVerified();
        return $result;
    }

    /**
     * @return bool
     */
    public function isValidMoreAboutMe(){
        return
            $this->hasOwnCar()->where('name', 'Yes')->first() ||
            $this->ownChildren()->where('name', 'Yes')->first() ||
            $this->pets()->where('name', 'Yes')->first() ||
            $this->smoker()->where('name', 'Yes')->first();
    }

    /**
     * @return array
     */
    public function getMoreAboutMe(){
        return array(
            'own_car' => $this->hasOwnCar()->where('name', 'Yes')->first() ? true : false,
            'own_children' => $this->ownChildren()->where('name', 'Yes')->first() ? true : false,
            'pets' => $this->ownChildren()->where('name', 'Yes')->first() ? true : false,
            'non_smoker' => $this->smoker()->where('name', 'Yes')->first() ? true : false,
        );
    }

    /**
     * @return bool
     */
    public function isValidIdPhoto(){
        return
            $this->passport_id && $this->passport_id->to_check && $this->passport_id->photo ||
            $this->driver_licence_id && $this->driver_licence_id->to_check && $this->driver_licence_id->photo;
    }

    /**
     * @return bool
     */
    public function isValidWWCC(){
        return $this->wwcc &&
            $this->wwcc->state_id &&
            $this->wwcc->application_card_number &&
            $this->wwcc->card_name &&
            $this->wwcc->expiry_date;
    }

    /**
     * @return bool
     */
    public function isValidMedicareCcard(){
        return $this->secondary_id &&
            $this->secondary_id->card_names &&
            $this->secondary_id->expiry_date &&
            $this->secondary_id->id_number  &&
            $this->secondary_id->card_type !== null &&
            $this->secondary_id->individual_ref_number;
    }

    /**
     * @return bool
     */
    public function isValidIdentityCheck()
    {
        return $this->isValidPassport() || $this->isValidDriverLicence();
    }

    /**
     * @return bool
     */
    public function isValidAddress(){
        return $this->location &&
            $this->location->street &&
            $this->location->suburb &&
            $this->location->post_code &&
            $this->location->state;
    }

    /**
     * @return bool
     */
    public function isValidPassport(){
        return $this->passport_id &&
            $this->passport_id->to_check &&
            $this->passport_id->id_number;
    }

    /**
     * @return bool
     */
    public function isValidDriverLicence(){
        return $this->driver_licence_id &&
            $this->driver_licence_id->to_check &&
            $this->driver_licence_id->id_number &&
            $this->driver_licence_id->state_id;
    }
}
