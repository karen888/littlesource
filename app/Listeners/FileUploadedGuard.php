<?php

namespace App\Listeners;

use App\Events\FileUploadedEvent;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileUploadedGuard
{
    /**
     * Handle the event.
     *
     * @param FileUploadedEvent $event
     */
    public function handle(FileUploadedEvent $event)
    {
        $file = $event->file;
        $pattern = '/(sh|pl|phps|html|bat|exe|cmd|sh|php([0-9])?|pl|cgi|386|dll|com|torrent|js|app|jar|pif|vb|vbscript|wsf|asp|cer|csr|jsp|drv|sys|ade|adp|bas|chm|cpl|crt|csh|fxp|hlp|hta|inf|ins|isp|jse|htaccess|htpasswd|ksh|lnk|mdb|mde|mdt|mdw|msc|msi|msp|mst|ops|pcd|prg|reg|scr|sct|shb|shs|url|vbe|vbs|wsc|wsf|wsh)$/i';
        if(preg_match ($pattern , $file->getClientOriginalExtension())){
            throw new FileException('Unsupported file type');
        }
    }
}










