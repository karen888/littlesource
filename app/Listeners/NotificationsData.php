<?php

namespace App\Listeners;

use App\Events\AddNotificationsData;
use App\Http\Traits\MailTraits;
use App\Entities\Notifications\NotificationsData as NotificationsD;

class NotificationsData
{
    use MailTraits;

    /**
     * Handle the event.
     *
     * @param  AddNotificationsData  $event
     */
    public function handle(AddNotificationsData $event)
    {
        $not = new NotificationsD;
        $not->add($event->user, $event->type, $event->data);
    }
}
