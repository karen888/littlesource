<?php

namespace App\Listeners;

use App\Events\UserPrelaunchSignedUp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Contracts\Mail\Mailer;
use App\Http\Models\PrelaunchSignup;

class EmailPrelaunchConfirmation
{
    /**
     * The sender of the email.
     * @var string
     */
    protected $from;

    /**
     * The sender name
     * @var string
     */
    protected $from_name;

    /**
     * The recipient of the email.
     * @var string
     */
    protected $to;

    /**
     * Message subject
     * @var string
     */
    protected $subject = 'LittleOnes - Pre-launch Join Confirmation';

    /**
     * The view for the email.
     * @var string
     */
    protected $view = ['emails.pre-launch.confirmation', 'emails.pre-launch.confirmation-plain'];

    /**
     * The data associated with the view for the email.
     * @var array
     */
    protected $data = [];

    /**
     * The mailer driver.
     * @var array
     */
    protected $mailer;

    /**
     * Create the event listener.
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->from = env('MAIL_SENDER');
        $this->from_name = env('MAIL_SENDER_NAME');
    }

    /**
     * Handle the event.
     *
     * @param  UserPrelaunchSignedUp  $event
     * @return void
     */
    public function handle(UserPrelaunchSignedUp $event)
    {
        $prelaunchUser = $event->prelaunchUser;
        $this->sendEmailConfirmationTo($prelaunchUser);
    }

    /**
     * Deliver the email confirmation.
     *
     * @param PrelaunchSignup $prelaunchUser
     * @internal param User $user
     * @return void
     */
    public function sendEmailConfirmationTo(PrelaunchSignup $prelaunchUser)
    {
        $this->to = $prelaunchUser->email;
        $this->data = compact('prelaunchUser');
        $this->deliver();
    }

    /**
     * Deliver the email.
     * @return void
     */
    public function deliver()
    {
        $this->mailer->send($this->view, $this->data, function ($message) {
            $message->subject($this->subject)
                    ->from($this->from, $this->from_name)
                    ->to($this->to);
        });
    }
}
