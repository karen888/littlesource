<?php

namespace App\Listeners;

use App\Events\SetNotifications;
use App\Http\Traits\MailTraits;

class EmailNotifications
{
    use MailTraits;

    /**
     * Handle the event.
     *
     * @param  SetNotifications  $event
     */
    public function handle(SetNotifications $event)
    {
        $this->sendMail(
            ['emails.notifications.html', 'emails.notifications.plain'],
            $event->user,
            'Little Ones Notifications',
            $event->data
        );
    }
}
