<?php

namespace App\Listeners;

use App\Events\ChangeEmail;
use App\Http\Traits\MailTraits;

class EmailChangeEmail
{
    use MailTraits;

    /**
     * Handle the event.
     *
     * @param  ChangeEmail  $event
     */
    public function handle(ChangeEmail $event)
    {
        $userData = $event->userData;
        $this->sendMail(
            ['emails.user.change-email', 'emails.user.change-email-plain'],
            $userData['user'],
            'Change Email.',
            [
                'old_email' => $userData['user']->email,
                'new_email' => $userData['new_email']
            ]
        );
    }
}
