<?php

namespace App\Listeners;

use App\Events\ChangePassword;
use App\Http\Traits\MailTraits;

class EmailChangePassword
{
    use MailTraits;

    /**
     * Handle the event.
     *
     * @param  ChangePassword  $event
     */
    public function handle(ChangePassword $event)
    {
        $userData = $event->userData;
        $this->sendMail(
            ['emails.user.change-pass', 'emails.user.change-pass-plain'],
            $userData,
            'Change Password.'
        );
    }
}
