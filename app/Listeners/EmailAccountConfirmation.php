<?php

namespace App\Listeners;

use App\Events\CreateAccount;
use App\Http\Traits\MailTraits;

class EmailAccountConfirmation
{
    use MailTraits;

    /**
     * Handle the event.
     *
     * @param  CreateAccount  $event
     */
    public function handle(CreateAccount $event)
    {
        $userData = $event->userData;
        $this->sendMail(
            ['emails.user.activate', 'emails.user.activate-plain'],
            $userData,
            'Account activation code!'
        );
    }
}
