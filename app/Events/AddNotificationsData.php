<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\User;

class AddNotificationsData extends Event
{
    use SerializesModels;

    /**
     * User Details
     * @var $user
     * @var $type
     * @var $data
     */
    public $user;
    public $type;
    public $data;


    /**
     * Create a new event instance.
     * @param User $user
     * @param $type
     * @param $data
     */
    public function __construct($user, $type, $data)
    {
        $this->user = $user;
        $this->type = $type;
        $this->data = $data;
    }


    /**
     * Get the channels the event should be broadcast on.
     */
    public function broadcastOn()
    {
        return [];
    }
}
