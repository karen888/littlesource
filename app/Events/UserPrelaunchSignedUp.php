<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Http\Models\PrelaunchSignup;

class UserPrelaunchSignedUp extends Event
{
    use SerializesModels;

    /**
     * User Details For the Prelaunch Signup
     * @var PrelaunchSignup
     */
    public $prelaunchUser;


    /**
     * Create a new event instance.
     * @param PrelaunchSignup $prelaunchUser
     * @return \App\Events\UserPrelaunchSignedUp
     */
    public function __construct(PrelaunchSignup $prelaunchUser)
    {
        $this->prelaunchUser = $prelaunchUser;
    }


    /**
     * Get the channels the event should be broadcast on.
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
