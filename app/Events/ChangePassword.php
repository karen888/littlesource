<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class ChangePassword extends Event
{
    use SerializesModels;

    public $userData;

    /**
     * Create a new event instance.
     * @param $userData
     */
    public function __construct($userData)
    {
        $this->userData = $userData;
    }


    /**
     * Get the channels the event should be broadcast on.
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
