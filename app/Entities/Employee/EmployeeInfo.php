<?php

namespace App\Entities\Employee;

use App\Http\Models\Model;
use App\Entities\Criteria\YearsExperience;

class EmployeeInfo extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'employee_info';

    public function get_experience() {
        return $this->hasOne(YearsExperience::class, 'id', 'experience');
    }

    public function getViewRateAttribute()
    {
        return (!(int)$this->min_rate) ? $this->max_rate : "$this->min_rate - $this->max_rate";
    }
}
