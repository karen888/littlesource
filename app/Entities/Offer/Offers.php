<?php namespace App\Entities\Offer;

use App\Http\Models\Model;
use App\Entities\Job\Job;
use App\User;

class Offers extends Model
{
    protected $table = 'offers';

    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function client()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'to_user_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

  public function getIs_ActiveAttribute($value)
    {
        return $value ? 'true' : 'false';
    }
    public function add($request, $parent)
    {
        $this->job_id = $request->job_id;
        $this->card_id = $request->card_id;
        $this->from_user_id = $parent->id;
        $this->to_user_id = $request->to_user_id;
        $this->offer_text = $request->offer_text;
        $this->offer_paid = $request->hourly_rate;
        $this->title = $request->title;
        $this->start_date = $request->start_date;
        $this->save();
    }
}