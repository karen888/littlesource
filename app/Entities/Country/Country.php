<?php

namespace App\Entities\Country;

use App\Http\Models\Model;

class Country extends Model
{
    /**
     * Table name of model
     * @var string
     */
    protected $table = 'countries';

    public static function getCustomSelectCountries() {
        $countries = static::all();

        $customSelect = $countries->map(function ($item, $key) {
            return ['value' => $item->id, 'text' => $item->name];
        });

        return $customSelect;
    }
}
