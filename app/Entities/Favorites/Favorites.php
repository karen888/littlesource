<?php

namespace App\Entities\Favorites;

use App\Http\Models\Model;
use Auth;
use App\User;

class Favorites extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'favorites';
    protected $fillable = ['user_id', 'favorite_id', 'notes'];

    private function checkFavorite($favorite_id)
    {
        return (bool) self::where([
            'user_id' => Auth::user()->id,
            'favorite_id' => $favorite_id
        ])->first();
    }

    public function addFavorite($favorite_id)
    {
        if ($this->checkFavorite($favorite_id)) {
            return false;
        }

        return (bool) self::create([
            'user_id' => Auth::user()->id,
            'favorite_id' => $favorite_id
        ]);
    }

    public function removeFavorite($favorite_id)
    {
        if (!$this->checkFavorite($favorite_id)) {
            return false;
        }
        
        return (bool) self::where([
            'user_id' => Auth::user()->id,
            'favorite_id' => $favorite_id
        ])->delete();
    }
    
    public function favorite()
    {
        return $this->belongsTo(User::class, 'favorite_id');
    }
}
