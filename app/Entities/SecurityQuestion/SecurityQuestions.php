<?php

namespace App\Entities\SecurityQuestion;

use App\Http\Models\Model;

class SecurityQuestions extends Model
{
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'security_questions';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['type_id', 'user_id', 'answer'];

    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */
    protected $hidden = ['answer'];
    
    public function type()
    {
        return $this->hasOne(SecurityQuestionTypes::class, 'id', 'type_id');
    }
    
    public function getQuestionAttribute()
    {
        return $this->type->type;
    }

    public static function add($type_id, $user_id, $answer)
    {
        self::create([
            'type_id' => $type_id,
            'user_id' => $user_id,
            'answer' => $answer
        ]);
    }

    public static function updateSecurityQuestion($security_question_id, $type_id, $user_id, $answer)
    {
        self::find($security_question_id)->update([
            'type_id' => $type_id,
            'user_id' => $user_id,
            'answer' => $answer
        ]);
    }
}
