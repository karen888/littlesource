<?php

namespace App\Entities\SecurityQuestion;

use App\Http\Models\Model;

class SecurityQuestionTypes extends Model
{
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'security_question_types';

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['type'];

    /**
     * Show only active Security Question Types on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('show', 1);
    }

    /**
     * Returns a customselect object of Active Security Question Types;
     * @return [type] [description]
     */
    public static function getCustomSelectList()
    {
        $list = static::active()->get();

        $list = $list->map(function ($item) {
            return ['value' => $item->id, 'text' => $item->type];
        });

        return $list;
    }
}