<?php

namespace App\Entities\Contact;

use App\Http\Models\Model;

class Contact extends Model
{
	 protected $table = 'contacts';

	 protected $fillable = [
        'name',
        'email',
        'subject',
        'message',
    	];
}
