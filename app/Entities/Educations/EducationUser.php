<?php

namespace App\Entities\Educations;

use App\Http\Models\Model;

class EducationUser extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'educations_user';

    protected $fillable = [
        'user_id',
        'educations_id'
    ];
}
