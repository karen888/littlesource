<?php

namespace App\Entities\Educations;

use App\Http\Models\Model;

class Educations extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'educations';

    protected $fillable = [
        'school',
        'degree',
        'scan',
        'area_study',
        'dates_attended_from',
        'dates_attended_to',
        'description'
    ];
}
