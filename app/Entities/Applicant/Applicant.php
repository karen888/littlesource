<?php

namespace App\Entities\Applicant;

use App\Http\Models\Model;
use App\Entities\Job\Job;
use App\User;

class Applicant extends Model
{
    /**
     * Fillable for eloquent model
     * @var [type]
     */
    protected $fillable = [
        'is_hidden', 'is_shortlisted', 'job_id', 'user_id', 'by_invite', 'hourly_rate', 'participation_method', 'is_active'
    ];

    /**
     * Returns the job associated with this application
     * @return [type] [description]
     */
    public function job() {
        return $this->belongsTo(Job::class);
    }

    /**
     * Returns the user associated with this application
     * @return [type] [description]
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Check if the user who applied and the job creator have messages between them
     *
     * @return bool
     */
    public function getMessagedAttribute()
    {
        return 1;
    }

    /**
     * Check if the user was invited to the job
     *
     * @return bool
     */
    public function getInvitedAttribute()
    {
        return 1;
    }

    protected $appends = ['messaged', 'invited'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_hidden'      => 'boolean',
        'is_shortlisted' => 'boolean',
        'messaged'       => 'boolean',
        'invited'        => 'boolean',
    ];


}
