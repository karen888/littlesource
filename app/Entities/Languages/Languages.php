<?php

namespace App\Entities\Languages;

use App\Http\Models\Model;

class Languages extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'languages';

    protected $fillable = [
        'name'
    ];
}
