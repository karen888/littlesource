<?php

namespace App\Entities\Languages;

use App\Http\Models\Model;

class LanguagesUser extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'languages_user';

    protected $fillable = [
        'user_id',
        'languages_id'
    ];
}
