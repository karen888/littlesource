<?php

namespace App\Entities\Job;

use App\Http\Models\Model;

class JobDuration extends Model
{
    /**
     * Fillable for eloquent model
     * @var array
     */
    protected $fillable = ['name', 'active'];

    /**
     * Show only active Job Durations on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('active', 1);
    }

    /**
     * Returns a customselect object of Active Job Durations;
     * @return [type] [description]
     */
    public static function customSelectListForActiveJobDurations()
    {
        $jobDuration = static::active()->get();

        $customSelect = $jobDuration->map(function ($item) {
            return ['value' => $item->id, 'text' => $item->name];
        });

        return $customSelect;
    }

}
