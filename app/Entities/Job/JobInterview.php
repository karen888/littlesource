<?php

namespace App\Entities\Job;

use App\Http\Models\Model;
use App\User;

class JobInterview extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'job_interview';

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function employee()
    {
        return $this->belongsTo(User::class);
    }
}
