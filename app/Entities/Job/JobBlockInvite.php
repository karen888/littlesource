<?php

namespace App\Entities\Job;

use App\Http\Models\Model;
use App\User;

class JobBlockInvite extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'job_block_invite';

    public function employee()
    {
        return $this->belongsTo(User::class);
    }

    public function client()
    {
        return $this->belongsTo(User::class);
    }

}
