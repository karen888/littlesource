<?php

namespace App\Entities\Job;

use App\User;
use App\Http\Models\Model;

class JobInvite extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'job_invite';

    public function client()
    {
        return $this->belongsTo(User::class, 'parent_id');
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'caregiver_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    /**
     * Get the Job Duration of the job.
     */
    public function jobDuration()
    {
        return $this->belongsTo(JobDuration::class);
    }

    /**
     * Get the Job Workload of the job.
     */
    public function jobWorkload()
    {
        return $this->belongsTo(JobWorkload::class);
    }

}
