<?php

namespace App\Entities\Job;

use App\User;
use App\Http\Models\Model;
use Illuminate\Support\Facades\Auth;

class SavedJob extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'saved_jobs';

    protected $fillable = [
        'caregiver_id', 'job_id'
    ];

    public function employee()
    {
        return $this->belongsTo(User::class, 'caregiver_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    private function checkSavedJob($job_id)
    {
        return self::where([
            'caregiver_id' => Auth::user()->id,
            'job_id' => $job_id
        ])->first();
    }

    public function saveJob($job_id)
    {
        $existingSavedJob = $this->checkSavedJob($job_id);
        if ($existingSavedJob) {
            $existingSavedJob->delete();
            return false;
        }

        return (bool) self::create([
            'caregiver_id' => Auth::user()->id,
            'job_id' => $job_id
        ]);
    }
}
