<?php

namespace App\Entities\Job;

use App\User;
use App\Http\Models\Model;
use App\Http\Models\Category;
use App\Http\Models\MarketplaceVisibility;
use App\Http\Models\MinimumFeedback;
use App\Http\Models\AllowedLocations;
use App\Http\Models\MinimumExperience;
use App\Http\Models\Question;
use App\Entities\Applicant\Applicant;
use App\Entities\Message\MessagesThread;
use App\Entities\Offer\Offers;
use App\Entities\Qualification\Qualification;
use App\Entities\Criteria\Criterias;
use App\Entities\Feedback\Feedback;
use Illuminate\Support\Facades\Auth;

class Job extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'jobs';

    /**
     * The attributes that should be casted to native types.
     * @var array
     */
    protected $casts = [
        'closed' => 'boolean',
    ];

    /**
     * Get the user of the job.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the category of the job.
     */
    public function category()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Get the qualification of the job.
     */
    public function qualification()
    {
        return $this->belongsToMany(Qualification::class);
    }

    /**
     * Get the feedback of the job.
     */
    public function feedback()
    {
        return $this->hasMany(Feedback::class);
    }

    /**
     * Get the Job Duration of the job.
     */
    public function jobDuration()
    {
        return $this->belongsTo(JobDuration::class);
    }

    /**
     * Get the Job Workload of the job.
     */
    public function jobWorkload()
    {
        return $this->belongsTo(JobWorkload::class);
    }

    /**
     * Get the Marketplace Visibility of the job.
     */
    public function marketplaceVisibility()
    {
        return $this->belongsTo(MarketplaceVisibility::class);
    }

    /**
     * Get the Minimum Feedback of the job.
     */
    public function minimumFeedback()
    {
        return $this->belongsTo(MinimumFeedback::class);
    }

    /**
     * Get the Allowed Locations of the job.
     */
    public function allowedLocations()
    {
        return $this->belongsTo(AllowedLocations::class);
    }

    /**
     * Get the Minimum Experience of the job.
     */
    public function minimumExperience()
    {
        return $this->belongsTo(MinimumExperience::class);
    }

    /**
     * get the selected days for this job
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobSelectedDays()
    {
        return $this->hasMany(JobSelectedDay::class);
    }

    /**
     * get the questions for the job
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questions()
    {
        return $this->belongsToMany(Question::class);
    }

    /**
     * get the Applicants for the job
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applicants()
    {
        return $this->hasMany(Applicant::class)->where('is_active', 1);
    }

    /**
     * get the Message Thread for the job
     */
    public function messageThread()
    {
        return $this->hasMany(MessagesThread::class);
    }

    public function jobInvite()
    {
        return $this->hasMany(JobInvite::class);
    }

    public function jobInterview()
    {
        return $this->hasMany(JobInterview::class)
            ->join('applicants', 'job_interview.job_id', '=', 'applicants.job_id')
            ->where('applicants.is_active', 1)
            ->select('job_interview.*');
    }

    public function jobOffers()
    {
        return $this->hasMany(Offers::class);
    }

    public function get_have_transportation()
    {
        return $this->belongsTo(Criterias::class, 'have_transportation');
    }

    public function get_gender()
    {
        return $this->belongsTo(Criterias::class, 'gender');
    }

    public function get_smoker()
    {
        return $this->belongsTo(Criterias::class, 'smoker');
    }

    public function get_comfortable_with_pets()
    {
        return $this->belongsTo(Criterias::class, 'comfortable_with_pets');
    }

    public function get_own_children()
    {
        return $this->belongsTo(Criterias::class, 'own_children');
    }

    public function get_sick_children()
    {
        return $this->belongsTo(Criterias::class, 'sick_children');
    }

    public function get_child_age_range()
    {
        return $this->belongsTo(Criterias::class, 'child_age_range');
    }

    public function get_age()
    {
        return $this->belongsTo(Criterias::class, 'age');
    }

    /**
     * Returns The job status for the closed option in human readable form
     * @param $value
     * @return string
     */
    public function getClosedAttribute($value)
    {
        return $value ? 'Closed' : 'Active';
    }

    public function jobProposalCount()
    {
        return $this->applicants()
            ->where('participation_method', 'proposal')
            ->count();
    }
    
    public function jobInterviewCount()
    {
        return $this->jobInterview()
            ->where('participation_method', 'invitation')
            ->count();
    }

    public function jobOffersCount()
    {
        return $this->jobOffers->count();
    }

    /**
     * Return the number of applicants for a job posted by the user.
     * @return int
     */
    public function getApplicantsCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if ( ! array_key_exists('applicantsCount', $this->relations))
            $this->load('applicantsCount');
        $related = $this->getRelation('applicantsCount');
        // then return the count directly
        return (!$related->isEmpty()) ? (int) $related->first()->aggregate : 0;
    }

    public function getJobInterviewCountAttribute()
    {
        // if relation is not loaded already, let's do it first
        if ( ! array_key_exists('jobInterviewCount', $this->relations))
            $this->load('jobInterviewCount');
        $related = $this->getRelation('jobInterviewCount');
        // then return the count directly
        return (!$related->isEmpty()) ? (int) $related->first()->aggregate : 0;
    }

    /**
     * Returns the number of applicants messaged by the user
     * @return int
     */
    public function getMessagedCountAttribute()
    {
        return 0;
    }

    public function isSaved()
    {
        return (bool) SavedJob::where('job_id', $this->id)->where('caregiver_id', Auth::user()->id)->first();
    }

    public function closeJob()
    {
        $this->closed = true;
        $this->save();
    }

    public function getCloseDate()
    {
        if ($this->feedback->count()) {
            return $this->feedback->first()->created_at->toFormattedDateString();
        }

        return date('Y-m-d H:i:s');
    }
    
    public function getJobRatings($for_employee)
    {
        $data = [];
        $rating = false;
        $text = false;

        foreach ($this->feedback as $feed) {
            if ($for_employee == $feed->by_employee) {
                $rating = $feed->getRating();
                $text = $feed->comment;
            }

            array_push($data, [
                'rating' => $feed->getRating(),
                'comment' => $feed->comment,
                'by_employee' => $feed->by_employee,
                'ratings' => $feed->rating
            ]);
        }

        return [
            'feedbacks' => $data,
            'text' => $text,
            'rating' => $rating
        ];
    }

    public function acceptedOffer()
    {
        return $this->jobOffers()->where('status', 'accept')->first();
    }
}
