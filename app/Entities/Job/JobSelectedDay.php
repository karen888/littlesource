<?php

namespace App\Entities\Job;

use App\Http\Models\Model;

class JobSelectedDay extends Model
{
    /**
     * Fillable for eloquent model
     * @var [type]
     */
    protected $fillable = ['day', 'from', 'to', 'selected'];


    /**
     * Returns the job for the selected day.
     * @return [type] [description]
     */
    public function job() {
        return $this->belongsTo(Job::class);
    }

}
