<?php

namespace App\Entities\Job;

use App\Http\Models\Model;

class JobDeclineReason extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'decline_reason';

    public static function customSelectList()
    {
        $reason = self::all();

        $customSelect = $reason->map(function ($item, $key) {
            return [
                'value' => $item->id,
                'text' => $item->reason
            ];
        });

        return $customSelect;
    }

}
