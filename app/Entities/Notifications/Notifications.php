<?php

namespace App\Entities\Notifications;

use App\Http\Models\Model;
use App\User;

class Notifications extends Model
{
    /**
     * Database table name for the eloquent Model
     */
    protected $table = 'notifications';

    protected $fillable = ['user_id'];

    /**
     * Get the user of the job.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get Notifications Data
     */
    public function data()
    {
        return $this->hasMany(NotificationsData::class, 'user_id', 'user_id');
    }
}
