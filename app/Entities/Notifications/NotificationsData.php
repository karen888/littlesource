<?php

namespace App\Entities\Notifications;

use App\Http\Models\Model;
use Illuminate\Support\Facades\Event;
use App\Events\SetNotifications;
use App\Entities\Offer\Offers;
use App\Entities\Job\JobInterview;
use App\Entities\Job\JobInvite;
use App\Entities\Job\Job;

class NotificationsData extends Model
{
    /**
     * Database table name for the eloquent Model
     */
    protected $table = 'notifications_data';

    protected $fillable = ['user_id', 'type', 'data'];

    /**
     * Register any other events for your application.
     *
     * @param $user
     * @param $type
     * @param $data
     */
    public function add($user, $type, $data)
    {
        if ((bool)$user->notifications_settings->$type) {
            self::create(
                [
                    'user_id' => $user->id,
                    'type' => $type,
                    'data' => json_encode($data)
                ]
            );
        }
    }

    /**
     * set Notifications
     *
     * @param $notifications
     */
    public function setNotifications($notifications)
    {
        $data_text = [];
        $notifications_data = $notifications->data()->orderBy('type', 'desc')->get();
        foreach ($notifications_data as $notification) {
            $data = json_decode($notification->data);
            switch ($notification->type) {
                case 'send_when_offline':
                    $this->setSendWhenOffline();
                    break;
                case 'proposal_received':
                    $this->setProposalReceived();
                    break;
                case 'interview_accepted':
                    array_push(
                        $data_text,
                        $this->setInterviewAccepted($data)
                    );
                    break;
                case 'interview_offer_declined_withdrawn':
                    array_push(
                        $data_text,
                        $this->setInterviewOfferDeclinedWithdrawn($data)
                    );
                    break;
                case 'offer_accepted':
                    array_push(
                        $data_text,
                        $this->setOfferAccepted($data)
                    );
                    break;
                case 'job_expire_soon':
                    $this->setJobExpireSoon();
                    break;
                case 'job_expired':
                    $this->setJobExpired();
                    break;
                case 'offer_interview_received':
                    array_push(
                        $data_text,
                        $this->setOfferInterviewReceived($data)
                    );
                    break;
                case 'offer_interview_withdrawn':
                    $this->setOfferInterviewWithdrawn();
                    break;
                case 'proposal_rejected':
                    $this->setProposalRejected();
                    break;
                case 'job_applied_modified_canceled':
                    array_push(
                        $data_text,
                        $this->setJobAppliedModifiedCanceled($data)
                    );
                    break;
                case 'contract_ends':
                    $this->setContractEnds();
                    break;
                case 'feedback_made':
                    $this->setFeedbackMade();
                    break;
                case 'contract_automatically_paused':
                    $this->setContractAutomaticallyPaused();
                    break;
                case 'tip_help_start':
                    $this->setTipHelpStart();
                    break;
            }
        }

        $notifications->data()->update(['send' => true]);

        if (!empty($data_text)) {
            $this->setEventEmail($notifications->user, $data_text);
        }
    }

    /**
     * Set Event for sending
     *
     * @param $user
     * @param $data
     */
    private function setEventEmail($user, $data)
    {
        Event::fire(
            new SetNotifications(
                $user,
                $data
            )
        );
    }

    /**
     * Set Send When Offline
     *
     */
    private function setSendWhenOffline()
    {

    }

    /**
     * Set Proposal Received
     *
     */
    private function setProposalReceived()
    {

    }

    /**
     * Set Interview Accepted
     *
     * @param $data
     * @return string
     */
    private function setInterviewAccepted($data)
    {
        $interview = JobInterview::find($data->job_interview_id);
        return "Interview for job '{$interview->job->title}' was accepted by {$interview->employee->name} {$interview->updated_at->format('M j, Y')}";
    }

    /**
     * Set Interview Offer Declined Withdrawn
     * @param $data
     * @return string
     */
    private function setInterviewOfferDeclinedWithdrawn($data)
    {
        switch ($data->type) {
            case 'offer':
                $offer = Offers::find($data->offer_id);
                return "Offer '{$offer->title}' was declined by {$offer->employee->name} {$offer->updated_at->format('M j, Y')}";
                break;
            case 'invite':
                $invite = JobInvite::find($data->invite_id);
                return "Invite for job '{$invite->job->title}' declined by {$invite->employee->name} {$invite->updated_at->format('M j, Y')}";
                break;
        }
    }

    /**
     * Set Offer Accepted
     *
     * @param $data
     * @return string
     */
    private function setOfferAccepted($data)
    {
        $offer = Offers::find($data->offer_id);
        return "Offer '{$offer->title}' was accepted by {$offer->employee->name} {$offer->updated_at->format('M j, Y')}";
    }

    /**
     * Set Job Expire Soon
     *
     */
    private function setJobExpireSoon()
    {

    }

    /**
     * Set Job Expired
     *
     */
    private function setJobExpired()
    {

    }

    /**
     * Set Offer Interview Received
     * @param $data
     * @return string
     */
    private function setOfferInterviewReceived($data)
    {
        switch ($data->type) {
            case 'invite':
                $invite = JobInvite::find($data->job_invite_id);
                return "Invitation to Interview: {$invite->job->title}";
                break;
        }

    }

    /**
     * Set Offer Interview Withdrawn
     *
     */
    private function setOfferInterviewWithdrawn()
    {

    }

    /**
     * Set Proposal Rejected
     *
     */
    private function setProposalRejected()
    {

    }

    /**
     * Set Job Applied Modified Canceled
     * @param $data
     * @return string
     */
    private function setJobAppliedModifiedCanceled($data)
    {
        $job = Job::find($data->job_id);
        
        switch ($data->type) {
            case 'cancel':
                 return "Job '{$job->title}' canceled.";
                break;
            case 'modified':
                return "Job '{$job->title}' modified.";
                break;
        }
    }

    /**
     * Set Contract Ends
     *
     */
    private function setContractEnds()
    {

    }

    /**
     * Set Feedback Made
     *
     */
    private function setFeedbackMade()
    {

    }

    /**
     * Set Contract Automatically Paused
     *
     */
    private function setContractAutomaticallyPaused()
    {

    }

    /**
     * Set Tip Help Start
     *
     */
    private function setTipHelpStart()
    {

    }
}
