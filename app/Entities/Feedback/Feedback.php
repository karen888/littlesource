<?php namespace App\Entities\Feedback;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Entities\Job\Job;

class Feedback extends Model
{
    protected $table = 'feedbacks';

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id');
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function rating()
    {
        return $this->belongsToMany(RatingOptions::class, 'feedback_ratings', 'feedback_id', 'rating_id')->withPivot('data');
    }

    public function getRating()
    {
        $stat = 0;
        foreach ($this->rating as $rating) {
            $stat += $rating->pivot->data;
        }
        
        return ($stat / count($this->rating));
    }
}
