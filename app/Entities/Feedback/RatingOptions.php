<?php namespace App\Entities\Feedback;

use App\Http\Models\Model;

class RatingOptions extends Model {
    protected $table = 'rating_options';
    protected $timestamp = false;

    public static function getList($type)
    {
        return static::where('type', $type)->get()->map(function ($item) {
            return [
                'id' => $item->id,
                'value' => $item->by_default,
                'text' => $item->text
            ];
        });
    }
}
