<?php namespace App\Entities\Feedback;

use App\Http\Models\Model;

class FeedbackReason extends Model {
    protected $table = 'feedback_reason';
    protected $timestamp = false;

    public static function customList($selected = [])
    {
        return static::get()->map(function ($item) use ($selected) {
            return [
                'value' => $item->id,
                'text' => $item->reason,
                'is_selected' => in_array($item->id, $selected)
            ];
        });
    }
}

