<?php

namespace App\Entities\Billing;

use App\Http\Models\Model;

class PaymentPeriod extends Model
{
    protected $fillable = [
        'start_date', 'end_date'
    ];
}