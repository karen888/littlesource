<?php

namespace App\Entities\Billing;

use App\Http\Models\Model;

class Cards extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'cards';

    public function add($request, $client)
    {
        $this->card_description = $request->card_description;
        $this->card_type = $request->card_type;
        $this->card_number = $request->card_number;
        $this->card_expdate = $request->card_expdate_m . '-' . $request->card_expdate_y;
        $this->card_cvv2 = $request->card_cvv2;
        $this->card_firstname = $request->card_firstname;
        $this->card_lastname = $request->card_lastname;
        $this->card_country_code = 36;
        $this->card_address1 = $request->card_address1;
        $this->card_address2 = $request->card_address2;
        $this->card_city = $request->card_city;
        $this->card_zip_code = $request->card_post_code;
        $this->user_id = $client->id;
        $this->save();
    }

    public function edit($request, $client)
    {
        $this->id = $request->id;
        $this->card_description = $request->card_description;
        $this->card_type = $request->card_type;
        $this->card_number = $request->card_number;
        $this->card_expdate = $request->card_expdate_m . '-' . $request->card_expdate_y;
        $this->card_cvv2 = $request->card_cvv2;
        $this->card_firstname = $request->card_firstname;
        $this->card_lastname = $request->card_lastname;
        $this->card_country_code = 36;
        $this->card_address1 = $request->card_address1;
        $this->card_address2 = $request->card_address2;
        $this->card_city = $request->card_city;
        $this->card_zip_code = $request->card_post_code;
        $this->user_id = $client->id;
        $this->update();
    }    
}
