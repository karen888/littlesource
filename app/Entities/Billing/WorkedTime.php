<?php

namespace App\Entities\Billing;

use App\Http\Models\Model;
use App\Entities\Job\Job;

class WorkedTime extends Model
{
    protected $fillable = [
        'job_id', 'payment_period_id', 'date', 'start_time', 'end_time', 'thirty_min_intervals'
    ];

    public function job() {
        return $this->belongsTo(Job::class);
    }
}