<?php

namespace App\Entities\Qualification;

use App\Http\Models\Model;

class Qualification extends Model {
    /**
     * Table name of model
     * @var string
     */
    protected $table = 'qualifications';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Show only active qualifications on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('active', 1);
    }

    /**
     * Returns a customselect object of Active qualifications;
     * @return [type] [description]
     */
    public static function customSelectList()
    {
        $query = static::active()->get();

        $customSelect = $query->map(function ($item) {
            return [
                'value' => $item->id,
                'text' => $item->name
            ];
        });

        return $customSelect;
    }

    /**
     * Returns a customselect object of Active qualifications;
     * @return [type] [description]
     */
    public static function customListQualifications($selected = [])
    {
        $query = static::active()->get();

        $customSelect = $query->map(function ($item) use ($selected) {
            return [
                'value' => $item->id,
                'text' => $item->name,
                'is_selected' => in_array($item->id, $selected)
            ];
        });

        return $customSelect;
    }
}
