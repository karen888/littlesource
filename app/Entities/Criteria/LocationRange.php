<?php

namespace App\Entities\Criteria;


class LocationRange extends Base
{
    public static $multiple = false;
    public static $validationRule = 'sometimes|integer';
    public static $ordinal = -1;
}
