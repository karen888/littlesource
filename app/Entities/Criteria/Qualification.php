<?php

namespace App\Entities\Criteria;


class Qualification extends Base
{
    public static $multiple = true;
    public static $validationRule = 'sometimes|array';
    public static $ordinal = 0;

    public function getRules()
    {
        return array_merge(parent::getRules(), [
            'completion_year' => 'required|integer',
            'institution' => 'string|min:3|max:255',
            'path' => 'required|string',
            'student_id' => 'string',
            'previous_last_name' => 'string',
        ]);
    }

    public function getMessages()
    {
        return [
            'id.exists' => 'Please select one of the proposed qualifications',
            'path.required' => 'Please upload a copy of your certification.'
        ];
    }
}
