<?php

namespace App\Entities\Criteria;


class Smoker extends Base
{
    public static $multiple = false;
    public static $validationRule = 'sometimes|array';
}
