<?php

namespace App\Entities\Criteria;


class AgeRange extends Base
{
    public static $multiple = true;
    public static $validationRule = 'sometimes|array';
    public static $ordinal = 1;
}
