<?php

namespace App\Entities\Criteria;


class DriversLicence extends Base
{
    public static $multiple = false;
    public static $validationRule = 'sometimes|array';
}
