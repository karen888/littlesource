<?php

namespace App\Entities\Criteria;


class Pets extends Base
{
    public static $multiple = false;
    public static $validationRule = 'sometimes|array';
}
