<?php

namespace App\Entities\Criteria;

trait CriteriaTrait {

    public function allCriteria()
    {
        // returns current user criteria, including the ones not configured
        // shares the same as ClientController@findCaregivers: moved func to Base
        return Base::allCriteria($this);
    }
    

    /**
     * Search collection by multiple criterias
     * @param $query
     * @param $input
     */
    public static function scopeCriteriaSearch($query, $input)
    {
        // OBS: could remove checking for .all(), it's handled client side
        $classes = Base::subclasses();



        foreach($input as $key=>$value) {
            $class = array_key_exists($key, $classes) ? $classes[$key] : null;

            $values = is_array($value) ? $value : [$value];


            $isAll = $class::all()->count() == count($values);


            $method = method_exists(static::class, camel_case($key)) ? camel_case($key) : null;


            if($method && $class && !$isAll && $values) {
                $query->whereHas($method, function($q) use ($class, $value, $values) {
                    if ($class::$ordinal==0) {

                        $q->whereIn('criteria_id', $values);

                    } else {
                        $q->where('criteria_id', $class::$ordinal >0 ? '>=' : '<=', $value);
                    }
                });
            }


        }
    }

    /**
     * Returns Care Types
     * @return mixed
     */
    public function careType()
    {
        return $this->multipleCriteria(CareType::class);
    }


    /**
     * Returns available Child Ages
     * @return mixed
     */
    public function childAge()
    {
        return $this->multipleCriteria(ChildAge::class);
    }


    /**
     * Returns users availability
     * @return mixed
     */
    public function availability()
    {
        return $this->multipleCriteria(Availability::class)->withPivot('data');
    }


    /**
     * Returns user prefeered employment type
     * @return mixed
     */
    public function employmentType()
    {
        return $this->multipleCriteria(EmploymentType::class);
    }

    /**
     * Returns user interests
     * @return mixed
     */
    public function interest()
    {
        return $this->multipleCriteria(Interest::class);
    }

    /**
     * Returns user max_per_booking
     * @return mixed
     */
    public function maxPerBooking()
    {
        return $this->multipleCriteria(MaxPerBooking::class);
    }

    /**
     * Returns user services
     * @return mixed
     */
    public function service()
    {
        return $this->multipleCriteria(Service::class);
    }

    /**
     * Returns user skills
     * @return mixed
     */
    public function skill()
    {
        return $this->multipleCriteria(Skill::class);
    }


    /**
     * Workaround for STI OneToOne Relationships
     * (could had used magic methods, but in a trait it's a bad practice)
     * (the each first methods should have been 'private', but 'whereHas' cannot find them)
    */
    public function qualification() { return $this->multipleCriteria(Qualification::class)->withPivot('data'); }

    public function gender() {
        return $this->multipleCriteria(Gender::class)->withPivot('data');
    }

    public function yearsExperience() { return $this->multipleCriteria(YearsExperience::class); }

    public function driversLicence() { return $this->multipleCriteria(DriversLicence::class); }
    public function getDriversLicenceAttribute() { return $this->driversLicence()->first(); }
    public function setDriversLicenceAttribute($value) { $this->singleCriteriaSetter('driversLicence', DriversLicence::class, $value); }

    public function hasOwnCar() { return $this->multipleCriteria(HasOwnCar::class); }
    public function ownChildren() { return $this->multipleCriteria(OwnChildren::class); }
    public function pets() { return $this->multipleCriteria(Pets::class); }
    public function smoker() { return $this->multipleCriteria(Smoker::class); }


    public function locationRange()
    {
        return $this->multipleCriteria(LocationRange::class);
    }

    public function ageRange()
    {
        return $this->multipleCriteria(AgeRange::class);
    }


    /* ######### Helpers ######### */

    /**
     * Helper to keep set*Attribute func single line for PSR-2 derrogation :P
     * STI One2One Relationship Workaround
     * @param $rel
     * @param \App\Entities\Criteria\Base $cls
     * @param $val
     */
    protected function singleCriteriaSetter($rel, $cls, $val)
    {
        call_user_func([$this, $rel])->detach($cls::all()->lists('id')->toArray());
        call_user_func([$this, $rel])->attach($val);
    }

    /**
     * Helper for many-to-many relationships
     * @param $cls
     * @return mixed
     */
    protected function multipleCriteria($cls)
    {
        return $this->belongsToMany($cls, 'criterias_users', 'user_id', 'criteria_id');
    }

    
}