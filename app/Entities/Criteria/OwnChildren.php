<?php

namespace App\Entities\Criteria;


class OwnChildren extends Base
{
    public static $multiple = false;
    public static $validationRule = 'sometimes|array';
}
