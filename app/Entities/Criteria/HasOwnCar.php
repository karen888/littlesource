<?php

namespace App\Entities\Criteria;


class HasOwnCar extends Base
{
    public static $multiple = false;
    public static $validationRule = 'sometimes|array';
}
