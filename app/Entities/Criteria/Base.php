<?php

namespace App\Entities\Criteria;

use App\Entities\Models\StiBase;
use App\User;
use ReflectionClass;

class Base extends StiBase
{
    protected $table = 'criterias';
    protected $stiClassField = 'type';
    protected $stiBaseClass = Base::class;
    protected $order_column = 'order';
    protected $order_direction = 'asc';
    public $timestamps = true;
    protected $fillable = ['name', 'description', 'data', 'default', 'order'];

    protected $casts = [
        'default' => 'boolean',
        'data' => 'array' //stored as JSON
    ];

    public static $multiple = true;
    public static $validationRule = 'sometimes|array';

    /** Possible values:
     * -1: also lower
     *  0: disabled
     * +1: also higher
     */
    public static $ordinal = 0;


    /**
     * Global scope to get ordered entities
     * @param bool $excludeDeleted
     * @return mixed
     */
    public function newQuery($excludeDeleted = true)
    {
        return parent::newQuery($excludeDeleted)->orderBy($this->order_column, $this->order_direction);
    }


    /**
     * Returns the default entities
     * @param $query
     * @return mixed
     */
    public static function scopeDefaults($query)
    {
        return $query->where('default', true);
    }


    /**
     * Has any defaults set
     * @return bool
     */
    public static function hasDefaults()
    {
        return (bool) static::defaults()->count();
    }


    /**
     * Returns all the subclasses that inherit from this STI
     * @return array
     */
    public static function allSubClasses() {
        return [
            LocationRange::class,
            MaxPerBooking::class,
            AgeRange::class,
            CareType::class,
            ChildAge::class,
            Availability::class,
            EmploymentType::class,
            Qualification::class,
            Service::class,
            Skill::class,
            Interest::class,
            Gender::class,
            YearsExperience::class,
            OwnChildren::class,
            DriversLicence::class,
            HasOwnCar::class,
            Pets::class,
            Smoker::class,
        ];
    }

    /**
     * Returns all the subclasses that inherit from this STI
     * to be consumed in JSON format
     * @param bool $json [true: snake_case, false: camel_case]
     * @return array
     */
    public static function subclasses($json=true)
    {
        $subClasses = static::allSubClasses();
        $ret = [];
        foreach($subClasses as $cls) {
            $clsName = (new ReflectionClass($cls))->getShortName();
            $ret[$json ? snake_case($clsName) : camel_case($clsName)] = $cls;
        }
        return $ret;
    }


    /**
     * Returns all the criterias
     * @param null $user
     * @return array
     */
    public static function allCriteria($user = null)
    {
        $criteria = [];
        foreach(static::subclasses() as $key=>$cls) {
            $selection = $user ? call_user_func([$user, camel_case($key)])->lists('id') : null;
            //TODO: return to this, after adding it to the user profile
            $criteria[$key] = [
                'name' => $key,
                'has_defaults' => $cls::hasDefaults(),
                'multiple' => $cls::$multiple,
                'items' => static::mapCriteriaToSelect($cls::all(), $selection)
            ];
        }
        return $criteria;
    }


    /**
     * Maps the collection to an custom named array
     * @param $criteria
     * @param $selection
     * @return mixed
     */
    protected static function mapCriteriaToSelect($criteria, $selection)
    {
        return $criteria->map(function($obj) use ($selection) {
            return [
                'value' => $obj->id,
                'text' => $obj->name,
                'description' => $obj->description,
                'data' => $obj->data,
                'is_selected' => $selection ? in_array($obj->id,$selection) : $obj->default
            ];
        });
    }


    /**
     * Returns user relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany(User::class, 'criterias_users', 'criteria_id');
    }


    /**
     * Helper to get only the "names" array
     * @param $query
     * @return mixed
     */
    public static function scopeNames($query)
    {
        return $query->lists('name')->toArray();
    }


    /**
     * Returns the validation rules for each subclasses
     * @return array
     */
    public static function validationRules()
    {
        $rules = [];
        foreach(static::subclasses() as $key=>$cls) {
            $rules[$key] = $cls::$validationRule;
        }
        return $rules;
    }

    /**
     * Returns the array of subclass entities
     * @return array
     */
    public static function pillsList()
    {
        return static::all()->toArray();
    }

    public function getRules()
    {
        return ['id' => 'required|exists:criterias,id'];
    }

    public function getMessages()
    {
        return [];
    }

    public function getCriteriaName()
    {
        $class = explode('\\', static::class);
        return snake_case(array_pop($class));
    }
}
