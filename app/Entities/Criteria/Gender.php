<?php

namespace App\Entities\Criteria;


class Gender extends Base
{
    public static $multiple = true;
    public static $validationRule = 'sometimes|array';
}
