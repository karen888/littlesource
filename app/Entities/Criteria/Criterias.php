<?php

namespace App\Entities\Criteria;

use App\Http\Models\Model;

class Criterias extends Model
{
    /**
     * Database table name for the eloquent Model
     * @var string
     */
    protected $table = 'criterias';
}
