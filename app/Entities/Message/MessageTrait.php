<?php

namespace App\Entities\Message;

use App\Entities\Job\Job;

trait MessageTrait {

    /**
     * Returns messages threads initiated by other
     * @return mixed
     */
    public function receivedThreads()
    {
        return $this->hasMany(MessagesThread::class, 'receiver_id');
    }


    /**
     * Returns own initiated messages threads
     * @return mixed
     */
    public function initiatedThreads()
    {
        return $this->hasMany(MessagesThread::class, 'sender_id');
    }


    /**
     * Returns the all message threads
     * @param bool|array $with  Can be an relations array to be eager loaded
     * @param bool|null $archived   null for all (archived or non-archived)
     * @return mixed
     */
    public function messageThreads($with = true ,$archived = false)
    {
        $eagerLoad = is_array($with) ? $with : ($with ? ['receiver', 'sender', 'job', 'messages'] : []);

        $received = $this->receivedThreads()->with($eagerLoad);
        if($archived !== null) $received = $received->whereIsArchived($archived);

        $initiated = $this->initiatedThreads()->with($eagerLoad);
        if($archived !== null) $initiated = $initiated->whereIsArchived($archived);

        return $received->get()->merge($initiated->get());
    }


    public function findThread($job_id, $receiver_id)
    {
        $received = $this->receivedThreads()->where(['job_id'=>$job_id, 'sender_id'=>$receiver_id]);
        $initiated = $this->initiatedThreads()->where(['job_id'=>$job_id, 'receiver_id'=>$receiver_id]);
        return $received->get()->merge($initiated->get())->first();
    }


    /**
     * Returns Inbox message threads
     * @return mixed
     */
    public function inbox()
    {
        return $this->messageThreads();
    }


    /**
     * Returns archived message threads
     * @return mixed
     */
    public function archive()
    {
        return $this->messageThreads(true, true);
    }


    /**
     * Returns the messages relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }


    /**
     * Returns the messages thread for a particular id
     * @param $id
     * @return mixed
     */
    public function conversation($id)
    {
        $eagerLoad = ['job.user', 'messages.user', 'receiver', 'sender'];
        $userId = $this->id;
        return MessagesThread::with($eagerLoad)->whereId($id)->where(function($q) use ($userId) {
            $q->where('sender_id', $userId)->orWhere('receiver_id', $userId);
        })->first();
        //TODO: rewrite above, unnecessary "where" user
        //TODO: need to enforce message ordering by created_at, even if implementing msg editing, the order matters
    }


    /**
     * Returns who this user coresponded with
     * @return mixed
     */
    public function corespondents()
    {
        //TODO: this query should be optimized
        $currentUser = $this;
        return $this->messageThreads(false, null)->map(function($obj) use ($currentUser) {
            $ret = [];
            if ($currentUser != $obj->sender) $ret[] = $obj->sender;
            if ($currentUser != $obj->receiver) $ret[] = $obj->receiver;
            return $ret;
        })->flatten()->unique();
    }

} 