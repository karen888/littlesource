<?php

namespace App\Entities\Message;

use App\Entities\Message\MessagesThread;
use App\Http\Models\Model;
use App\User;
use Auth;

class Message extends Model
{

    protected $fillable = ['message'];
    
    /**
     * Returns the parent MessagesThread
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo(MessagesThread::class, 'message_thread_id');
    }


    /**
     * Returns the associated user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Returns the receiver, accounting for the fact that a user can be a
     * thread initiator or receiver
     * @return mixed
     */
    public function receiver()
    {
        $this->load(['thread', 'user']);
        if($this->thread->sender == $this->user) return $this->thread->receiver;
        return $this->thread->sender;
    }


    /**
     * Scope to return messages after a date,
     * used for the latest unseed messages
     * @param $query
     * @param $date
     * @return mixed
     */
    public function scopeAfterDate($query, $date)
    {
        return $query->where('created_at', '>=', $date);
    }

    /**
     * Send offer message to employee
     * @param $job
     * @param $client
     * @param $employee
     * @param $offer
     * @param $thread
     */
    public function sendOfferMessage($job, $client, $employee, $offer, $thread)
    {
        $this->message_thread_id = $thread->id;
        $this->user_id = $client->id;
        $this->message = "Offer: {$job->title}<br><br>OFFER DETAILS<br><br>Client: {$client->name}<br>Babysitter: {$employee->name}
                 <br>Hourly Rate: $" . $offer->offer_paid . "/hr<br><br><a href='" . route('employee.view.offer', $offer->id) . "'>View Offer</a>
                 <br><br>This offer will expire " . date('l, F j, Y', strtotime("+2 week")) . ". Accept this offer to start working";
        $this->save();
    }
    
    public static function getDirect($direct)
    {
        if (!User::find($direct)) {
            return null;
        }

        $thread = MessagesThread::where([
            'receiver_id' => $direct,
            'sender_id' => Auth::user()->id,
            'job_id' => NULL
        ])->first();

        if (!$thread) {
            $thread = new MessagesThread;
            $thread->add(null, Auth::user()->id, $direct);
        }

        return $thread->id;
    }
}
