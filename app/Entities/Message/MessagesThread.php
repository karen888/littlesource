<?php

namespace App\Entities\Message;

use App\Http\Models\Model;
use App\Entities\Job\Job;
use App\User;
use Illuminate\Support\Facades\Auth;

class MessagesThread extends Model
{

    // protected $appends = [
    //     'last_message'
    // ];


    /**
     * Returns the Job associated with this message thread
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo(Job::class);
    }


    /**
     * Returns the receiver of this message thread
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }


    /**
     * Returns the initiator of the thread, that's the owner of the associated job
     * only by convention called "sender"
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }


    /**
     * Returns the other party of this conversation thread
     * @return mixed
     */
    public function getOtherPartyAttribute()
    {
        if($this->sender == Auth::user()) return $this->receiver;
        return $this->sender;
    }


    /**
     * Get all the messages in this thread
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'message_thread_id');
    }


    /**
     * Returns the last message
     * @return mixed
     */
    public function getLastMessageAttribute()
    {
        return $this->messages()->with('user')->get()->sortByDesc('updated_at')->first();
    }


    /**
     * Sets other user messages as read
     * (since the auth user just read them)
     * @return mixed
     */
    public function setMessagesAsRead()
    {
        return $this->messages()->where('user_id', '!=', Auth::user()->id)->update(['is_read'=>true]);
    }

    /**
     * add Messages Thread
     * @param $job_id
     * @param $sender_id
     * @param $receiver_id
     */
    public function add($job_id, $sender_id, $receiver_id)
    {
        $this->job_id = $job_id;
        $this->sender_id = $sender_id;
        $this->receiver_id = $receiver_id;
        $this->save();
    }
}
