<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08.09.17
 * Time: 16:38
 */

namespace App;


class DBConfigAccess
{

    const TABLE_NAME = 'app_config';


    public static function getBulk($keys = [], $key_prepend_symbol = '')
    {
        $query = \DB::table(static::TABLE_NAME);

        if(count($keys) > 0) {
            $query = $query->whereIn('key', $keys);
        }

        $res = $query->get();
        $result = [];

        foreach($res as $model) {
            $result[$key_prepend_symbol.$model->key] = $model->value;
        }

        return $result;
    }
    /**
     * @param $key
     * @return string
     */
    public static function get($key)
    {
        $record = \DB::table(static::TABLE_NAME)->where('key', $key)
            ->first();

        if($record === null) {
            return '';
        }

        return $record->value;

    }

    /**
     * @param $key
     * @param $value
     * @return bool|int
     */
    public static function set($key, $value)
    {
        $record = \DB::table(static::TABLE_NAME)->where('key', $key)
            ->first();

        if($record === null) {
            return \DB::table(static::TABLE_NAME)->insert(compact('key', 'value'));
        } else {
            return \DB::table(static::TABLE_NAME)
                ->where('key', '=', $key)
                ->update(compact($value));
        }
    }

    /**
     * @param $key
     * @return int
     */
    public static function delete($key)
    {
        return \DB::table(static::TABLE_NAME)->where('key', '=', $key)->delete();
    }
}