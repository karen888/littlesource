<?php

namespace App\Services;

use App\Http\Requests\ProfileBuilderRequest;

abstract class ProfileBuilderService
{
    protected $request;
    protected $user;

    public function __construct(ProfileBuilderRequest $request)
    {
        $this->request = $request;
        $this->user = $request->user();
    }

    protected function deleteOldFile($file, $path = '')
    {
        $file = public_path($path . $file);
        if (file_exists($file) && is_file($file) && is_writable($file)) {
            unlink($path . $file);
        }
    }

    protected function saveCriterias()
    {
        $criterias = $this->request->get('criterias', []);
        if ($this->request->criteriasValid()) {
            $this->user->user_criteria()->sync($criterias);
        }
    }

    protected function saveBlobImage($requestData, $path = 'upload/user/')
    {
        preg_match("/data:image\/([a-z]+)\;/", $requestData, $imgInfo);
        if (count($imgInfo)) {
            $newFilename = md5(time() . rand() . rand()) . '.' . $imgInfo[1];
            $imgData = preg_split("/data:image\/([a-z]+);base64,/", $requestData);
            $imgData = base64_decode($imgData[1]);
            file_put_contents(public_path($path . $newFilename), $imgData);
            return '/' . $path . $newFilename;
        }
        return false;
    }

    abstract public function saveValidValues();
}