<?php

namespace App\Services;

use App\Http\Models\DriverLicenceId;
use App\Http\Models\GovernmentId;
use App\Http\Models\PassportId;
use App\Http\Models\SecondaryId;
use App\Http\Models\Wwcc;
use Carbon\Carbon;

class WwccSaver extends ProfileBuilderService
{
    /**
     * Saves valid values
     */
    public function saveValidValues()
    {
        $this->saveRelation('passport_id', PassportId::class);
        $this->saveRelation('driver_licence_id', DriverLicenceId::class);
        $this->saveRelation('government_id', GovernmentId::class);
        $this->saveRelation('secondary_id', SecondaryId::class);
        $this->saveRelation('wwcc', Wwcc::class);
        $this->saveCriterias();

        $this->user->check_agree = $this->request->check_agree;
        $this->user->check_confirm = $this->request->check_confirm;
        $this->user->check_confirm_eligible = $this->request->check_confirm_eligible;

        $this->user->save();
    }

    private function saveRelation($relation, $class)
    {
        if ($this->request->isValid($relation)) {
            $attributes = $this->request->$relation;

            if (!is_null($this->user->$relation)) {
                $attributes['user_id'] = $this->user->id;
                $this->user->$relation->update($attributes);
            } else {
                $this->user->{$relation}()->save(new $class($attributes));
            }
        }
    }
}