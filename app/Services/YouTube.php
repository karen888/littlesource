<?php
namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class YouTube {

    protected $client;

    protected $youtube;

    function __construct() {
        /* Get config variables */
        $clientId = Config::get('youtube.client_id');
        $clientSecret = Config::get('youtube.client_secret');
        $scope = Config::get('youtube.scope');
        $redirectUri = Config::get('youtube.redirect_uri');
        $refreshToken = Config::get('youtube.refresh_token');

        $this->client = new \Google_Client();
        $this->youtube = new \Google_Service_YouTube($this->client);//Test with Books Service        

        $this->client->setClientId($clientId);
        $this->client->setClientSecret($clientSecret);          
        $this->client->setAccessType('offline');
        $this->client->setApprovalPrompt('force');

        $this->client->setScopes($scope);
        $this->client->setRedirectUri($redirectUri);

        if ($accessToken = $this->getLatestAccessTokenFromDB()) {
            $this->client->setAccessToken($accessToken);
        } else {
            $this->client->setAccessToken($this->client->refreshToken($refreshToken));
            $this->saveAccessTokenToDB($this->client->refreshToken($refreshToken));
        }
    }

    /**
     * Upload the video to YouTube
     *
     * @param  string $path
     * @param  array $data
     * @param  string $privacyStatus
     * @return self
     * @throws Exception
     */

    public function uploadVideo($videoPath, $title){

        $this->handleAccessToken();

        $snippet = new \Google_Service_YouTube_VideoSnippet();
        $snippet->setTitle($title);
        // $snippet->setDescription('test');
        // $snippet->setTags('test');


        // Numeric video category. See
        // https://developers.google.com/youtube/v3/docs/videoCategories/list
        $snippet->setCategoryId("22");

        // Set the video's status to "public". Valid statuses are "public",
        // "private" and "unlisted".
        $status = new \Google_Service_YouTube_VideoStatus();
        $status->privacyStatus = "unlisted";

        // Associate the snippet and status objects with a new video resource.
        $video = new \Google_Service_YouTube_Video();
        $video->setSnippet($snippet);
        $video->setStatus($status);

        // Specify the size of each chunk of data, in bytes. Set a higher value for
        // reliable connection as fewer chunks lead to faster uploads. Set a lower
        // value for better recovery on less reliable connections.
        $chunkSizeBytes = 1 * 1024 * 1024;

        // Setting the defer flag to true tells the client to return a request which can be called
        // with ->execute(); instead of making the API call immediately.
        $this->client->setDefer(true);

        // Create a request for the API's videos.insert method to create and upload the video.
        $insertRequest = $this->youtube->videos->insert("status,snippet", $video);

        // Create a MediaFileUpload object for resumable uploads.
        $media = new \Google_Http_MediaFileUpload(
            $this->client,
            $insertRequest,
            'video/*',
            null,
            true,
            $chunkSizeBytes
        );

        $media->setFileSize(filesize($videoPath));
        $media->setChunkSize($chunkSizeBytes);


        // Read the media file and upload it chunk by chunk.
        $status = false;
        $handle = fopen($videoPath, "rb");
        while (!$status && !feof($handle)) {
            $chunk = fread($handle, $chunkSizeBytes);
            $status = $media->nextChunk($chunk);
        }
        fclose($handle);

        // If you want to make other calls after the file upload, set setDefer back to false
        $this->client->setDefer(false);

        return $status->id;
    }

    /**
     * Delete a YouTube video by it's ID.
     *
     * @param  int $id
     * @return bool
     * @throws Exception
     */
    public function deleteVideo($id)
    {
        $this->handleAccessToken();

        $response = $this->youtube->videos->listVideos('status', ['id' => $id]);

        if (!empty($response->items) && $response->items[0]->status->rejectionReason != 'duplicate') {
            $this->youtube->videos->delete($id);
        }

    }

    /**
     * Saves the access token to the database.
     *
     * @param  string  $accessToken
     */
    public function saveAccessTokenToDB($accessToken)
    {
        return DB::table('youtube_access_tokens')->insert([
            'access_token' => json_encode($accessToken),
            'created_at'   => Carbon::createFromTimestamp($accessToken['created'])
        ]);
    }

    /**
     * Get the latest access token from the database.
     *
     * @return string
     */
    public function getLatestAccessTokenFromDB()
    {
        $latest = DB::table('youtube_access_tokens')
                    ->latest('created_at')
                    ->first();

        return $latest ? (is_array($latest) ? $latest['access_token'] : $latest->access_token ) : null;
    }

    /**
     * Handle the Access Token
     *
     * @return void
     */
    public function handleAccessToken()
    {
        if (is_null($accessToken = $this->client->getAccessToken())) {
            throw new \Exception('An access token is required.');
        }

        if($this->client->isAccessTokenExpired())
        {
            // If we have a "refresh_token"
            if (array_key_exists('refresh_token', $accessToken))
            {
                // Refresh the access token
                $this->client->refreshToken($accessToken['refresh_token']);

                // Save the access token
                $this->saveAccessTokenToDB($this->client->getAccessToken());
            }
        }
    }

    /**
     * Check if a YouTube video exists by it's ID.
     *
     * @param  int  $id
     *
     * @return bool
     */
    public function exists($id)
    {
        $this->handleAccessToken();

        $response = $this->youtube->videos->listVideos('status', ['id' => $id]);

        if (empty($response->items)) return false;

        return true;
    }
}