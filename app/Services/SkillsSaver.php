<?php

namespace App\Services;

// use App\Http\Models\Reference;

class SkillsSaver extends ProfileBuilderService
{
    /**
     * Saves valid values
     */
    public function saveValidValues()
    {
        $this->saveCriterias();
        // $this->saveReferences();
    }

    // private function saveReferences()
    // {
    //     foreach ($this->request->references as $reference) {
    //         $reference['user_id'] = isset($reference['user_id']) ? $reference['user_id'] : $this->user->id;
    //         Reference::firstOrCreate($reference);
    //     }
    // }
}