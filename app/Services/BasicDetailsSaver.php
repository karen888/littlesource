<?php

namespace App\Services;

use Carbon\Carbon;
use DateTime;
use App\Http\Models\Location;
use App\Http\Models\Phone;
use App\Http\Traits\GeolocationTrait;

class BasicDetailsSaver extends ProfileBuilderService
{
    use GeolocationTrait;

    /**
     * Saves valid values
     */
    public function saveValidValues()
    {
        $this->saveCriterias();

        $employeeInfo = $this->user->employee_info;

        if ($this->request->isValid('user_photo_100') && $this->user->user_photo_100 != $this->request->user_photo_100) {
            $this->deleteOldFile($this->user->user_photo_100);
            $this->user->user_photo_100 = $this->saveBlobImage($this->request->user_photo_100);
        }

        if ($this->request->isValid('first_name')) {
            $this->user->first_name = $this->request->first_name;
        }

        if ($this->request->isValid('last_name')) {
            $this->user->last_name = $this->request->last_name;
        }

        $this->savePhone();
        $this->saveLocation();

        if ($this->request->isValid('birthday')) {
            $employeeInfo->birthday = Carbon::createFromFormat("d/m/Y", $this->request->birthday);
        }

        $this->user->save();
        $employeeInfo->save();
    }

    /**
     * Checks and updates location fields
     */
    private function saveLocation()
    {
        $location = !is_null($this->user->location) ? $this->user->location : new Location;
        foreach (['street', 'state', 'suburb', 'post_code'] as $field) {
            if ($this->request->isValid('location.' . $field)) {
                if ($field == 'post_code' && $location->post_code !== $this->request->location['post_code']) {
                    $geoData = $this->getGeoDataByPostcode($this->request->location['post_code']);
                    if (!empty($geoData->results)) {
                        $location->latitude = $geoData->results[0]->geometry->location->lat;
                        $location->longitude = $geoData->results[0]->geometry->location->lng;
                    }
                }
                $location->$field = $this->request->location[$field];
            }
        }

        $this->user->location()->save($location);
    }

    /**
     * Checks and updates phone fields
     */
    private function savePhone()
    {
        $phone = !is_null($this->user->phone) ? $this->user->phone : new Phone;
        foreach (['phone', 'verified'] as $field) {
            if ($this->request->isValid('phone.' . $field)) {
                $phone->$field = $this->request->phone[$field];
            }
        }

        $this->user->phone()->save($phone);
    }
}