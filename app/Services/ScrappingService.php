<?php

namespace App\Services;


class ScrappingService
{
    const SCRAPPERS_MAPPING = [
        "VIC" => 1,
        "WA" => 2,
        "TAS" => 3, //d
        "QLD" => 4,
        "NT" => 5,
        "NSW" => 6, //d
        "SA" => 7,
        "ACT" => 8
    ];

    public static function scrap($wwcc, $user, $state) {
        $cliParams = [$wwcc->application_card_number, $user->last_name];
        $scrapperNumber = self::SCRAPPERS_MAPPING[$state];
        switch ($state) {
            case "TAS":
            case "NSW":
                $birthDate = explode('-', $user->employee_info->birthday);
                array_reverse($birthDate);
                array_merge($cliParams, $birthDate);
                break;
            case "QLD":
                $fullname = str_replace(' ', '_', $wwcc->card_name);
                $cliParams = [$wwcc->application_card_number, $fullname];            
                $expiryDate = explode('/', $wwcc->expiry_date);
                $cliParams = array_merge($cliParams, $expiryDate);
                break;
            case "ACT":
            case "SA":
                $fullname = str_replace(' ', '_', $wwcc->card_name);
                $cliParams = [$wwcc->application_card_number, $fullname]; 
                $birthDate = array_reverse(explode('-', $user->employee_info->birthday));
                $cliParams = array_merge($cliParams, $birthDate);        
                break;
            case "NT":
                echo "Not Implemented"; // The website uses captcha
                return false;
                break;
        }

        self::sanitize($cliParams);
        $result =  self::runScrapper($cliParams, $scrapperNumber);
        return $result === 'valid';
    }

    /*
     * Sanitizing cli command parameters for security purposes
     * see: http://php.net/manual/en/function.exec.php
     */
    private static function sanitize($parameters) {
        foreach ($parameters as $value) {
            escapeshellarg($value);
            escapeshellcmd($value);
        }
    }

    private static function runScrapper($params, $scrapperNumber) {
        $paramsString = implode(" ", $params);
        exec("cd ./scrapping-scripts/ && node index{$scrapperNumber}.js {$paramsString} 2>&1", $output);
        if ($scrapperNumber == 7) {
            echo "<pre>"; print_r($output); echo "</pre>"; 
        }
        return $output[0];
    }
}
