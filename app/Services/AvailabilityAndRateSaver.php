<?php

namespace App\Services;

use App\Http\Models\Reference;

class AvailabilityAndRateSaver extends ProfileBuilderService
{
    /**
     * Saves valid values
     */
    public function saveValidValues()
    {
        $this->saveCriterias();
        $this->saveReferences();
        $employeeInfo = $this->user->employee_info;

        if ($this->request->isValid('overview')) {
            $employeeInfo->overview = $this->request->overview;
        }

        // if ($this->request->isValid('rate')) {
        //     $employeeInfo->rate = $this->request->rate;
        // }

        // if ($this->request->isValid('rate_visible')) {
        //     $employeeInfo->rate_visible = $this->request->get('rate_visible', false);
        // }

        $employeeInfo->save();
    }

    private function saveReferences()
    {
        foreach ($this->request->references as $reference) {
            $reference['user_id'] = isset($reference['user_id']) ? $reference['user_id'] : $this->user->id;
            Reference::firstOrCreate($reference);
        }
    }
}