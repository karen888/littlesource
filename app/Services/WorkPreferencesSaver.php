<?php

namespace App\Services;

class WorkPreferencesSaver extends ProfileBuilderService
{
    /**
     * Saves valid values
     */
    public function saveValidValues()
    {
        $this->saveCriterias();
        $employeeInfo = $this->user->employee_info;
        if ($this->request->isValid('rate')) {
            $employeeInfo->rate = $this->request->rate;
        }

        if ($this->request->isValid('rate_visible')) {
            $employeeInfo->rate_visible = $this->request->get('rate_visible', false);
        }
        $employeeInfo->save();
    }
}