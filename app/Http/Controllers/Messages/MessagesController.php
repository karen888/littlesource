<?php

namespace App\Http\Controllers\Messages;

use App\Entities\Message\Message;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    protected $user;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * Display a listing of the Current User Messages.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($direct = null)
    {

        // TODO: remove
        //return redirect()->route('employee.profile');

        if(!\App::environment() == "staging") {
            return redirect()->to('/home');
        }

        $thread_id = Message::getDirect($direct);

        return view('messages.index')->with([
            'messages_threads' => $this->user->inbox(),
            'thread_id' => $thread_id
        ]);
    }

}
