<?php namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class NotificationsMsgsCtl extends Controller {

    public function invite($user_id, $job_info, $url = '')
    {
        if (is_numeric($job_info)) {
            $table = $this->mdlOrm('Jobs.JobsMdl');
            $job_info = $table::find($job_info);
        }

        if (!empty($job_info)) {
            $url_job = routeClear('job_link', Array(), false).'/'.$job_info->path;
            if (empty($url)) $url = $url_job;

            $text = 'You have received an invitation to interview for the <a href="'.$url_job.'">job</a> "'.$job_info->title.'"';
            $this->addNotif($user_id, $text, $url);
        }

    }

    public function applyJob($user_id, $job_info, $user_info)
    {
        if (is_numeric($job_info)) {
            $table = $this->mdlOrm('Jobs.JobsMdl');
            $job_info = $table::find($job_info);
        }
        
        if (!empty($job_info)) {
            $url_job = routeClear('job_link', Array(), false).'/'.$job_info->path;
            $user_link = routeClear('user_link', Array(), false).'/'.$user_info->user_path;

            $text = $user_info->name.' applied to your for the <a href="'.$url_job.'">job</a> "'.$job_info->title.'"';
            $this->addNotif($user_id, $text, $user_link);
        }
    }

    public function offer($user_id, $job_info, $url = '', $status = 0)
    {
        if (is_numeric($job_info)) {
            $table = $this->mdlOrm('Jobs.JobsMdl');
            $job_info = $table::find($job_info);
        }

        if (!empty($job_info)) { 
            $url_job = routeClear('job_link', Array(), false).'/'.$job_info->path;
            if (empty($url)) $url = $url_job;

            if ($status == 1) {
                $text = 'Your offer is accepted for the <a href="'.$url_job.'">job</a> "'.$job_info->title.'"';
            } else if ($status == 2) {
                $text = 'Your offer is rejected for the <a href="'.$url_job.'">job</a> "'.$job_info->title.'"';
            } else if ($status == 3) {
                $text = 'Your contract is closed for the <a href="'.$url_job.'">job</a> "'.$job_info->title.'"';
            } else {
                $text = 'You have received an offer for the <a href="'.$url_job.'">job</a> "'.$job_info->title.'"';
            }

            $this->addNotif($user_id, $text, $url);
        }
    }

    public function addNotif($user_id, $text, $url = '')
    {
        $table = $this->mdlOrm('NotificationsMdl');
        $table->user_id = $user_id;
        $table->notification_text = $text;
        $table->notification_url = $url;

        $table->save();
    }
}
