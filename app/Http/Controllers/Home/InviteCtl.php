<?php namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class InviteCtl extends Controller {

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $info = $this->getInfoData();
        $this->withViewComp($view_file, 'data_info', $info);
    }

    public function getInfoData($request = null)
    {
        $res = new \StdClass();
        $res->employer = false;
        $res->can_proc = false;
        $res->invite = Array();
        
        if ($request) {
            $invite_id  = $request->get('invite');
        } else {
            $invite_id = \Route::input('one');
        }

        if ($invite_id) {
            $table = $this->mdlOrm('Jobs.AppliesMdl');
            $res->invite = $this->mdl('Jobs.AppliesMdl')->chInviteByUser($invite_id, \Auth::user()->id);
            //$res->invite = $table::find($invite_id);
            if (!empty($res->invite)) {
                $res->invite->created_at = convToTimezone($res->invite->created_at, true);

                if ($res->invite->from_user_id == \Auth::user()->id) {
                    $res->employer = true;

                    $res->employer_info = userPhotoFull(\Auth::user());
                } else $res->employer_info = userPhotoFull(\App\User::find($res->invite->from_user_id));

                if ($res->invite->user_id == \Auth::user()->id) {
                    $res->contractor_info = userPhotoFull(\Auth::user());
                } else $res->contractor_info = userPhotoFull(\App\User::find($res->invite->user_id));


                if (!$res->employer && ($res->invite->active == 0)) $res->can_proc = true;

            }
        }


        return $res;
    }

    public function postData($request)
    {
        $stat = 1;
        $msg = routeClear('home');

        $proc = $request->get('proc');
        $data_info = $this->getInfoData($request);
        if ($data_info->can_proc) {
            $table = $this->mdlOrm('Jobs.AppliesMdl');
            $table = $table::find($data_info->invite->id);
            if (!empty($table)) {

                if ($proc == 1) {
                    $table->active = 1;
                    $table->save();
                } else {
                    $this->ctl('ActionsCtl')->delApplies($table->job_id, $table->user_id);
                }

            }
        }


        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
