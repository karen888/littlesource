<?php namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class NotificationsCtl extends Controller {

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {


        $this->withViewComp($view_file, 'notifications_list', $this->getInfoData());
    } 

    private function getInfoData($limit = 0, $unreaded = false, $mark_as_readed = false)
    {
        $res = $this->mdl('NotificationsMdl')->getList(\Auth::user()->id, $limit, $unreaded);
        if ($limit && !empty($res)) {
            foreach ($res as $val) {
                $val->notification_text = strip_tags($val->notification_text);
            }

            if ($mark_as_readed) {
                $table = $this->mdlOrm('NotificationsMdl');
                $table::where('user_id', \Auth::user()->id)->update(Array('notification_readed' => 1));
            }
        }

        return $res;
    }

    public function postData($request)
    {
        $stat = 3;
        $msg = Array();

        $proc = $request->get('proc');
        if ($proc == 'del') {
            $table = $this->mdlOrm('NotificationsMdl');
            $info = $table::where('user_id', \Auth::user()->id)->where('id', $request->get('id'))->delete();

            $data_msg['name'] = 'delTd';
            $data_msg['val'] = '';
            $msg[] = $data_msg;
        } else if ($proc == 'ch_notifications') {
            return $this->chNotifications();
        } else {
            $data_msg['name'] = 'dropdownNotificationRes';
            $data_msg['val'] = '';
            $msg[] = $data_msg;
            $msg[] = $this->getInfoData(5, false, true);
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }

    public function chNotifications()
    {
        $stat = 3;
        $msg = Array();

        $notif = $this->getInfoData(1, true);
        if (!empty($notif)) $notif = 1;
        else $notif = 0;

        $new_msg = $this->mdl('MessagesMdl')->chNewMsg(\Auth::user()->id);
        if ($new_msg) $new_msg = 1;
        else $new_msg = 0;

        $data_msg['name'] = 'chNotificationsRes';
        $data_msg['val'] = '';
        $msg[] = $data_msg;
        $msg[] = $notif;
        $msg[] = $new_msg;
        $msg[] = csrf_token();
    
        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
