<?php namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class OfferCtl extends Controller {

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $info = $this->getInfoData();
        $this->withViewComp($view_file, 'offer_info', $info);
    }

    public function getInfoData($request = null)
    {
        $res = new \StdClass();
        $res->employer = false;
        $res->can_proc = false;
        $res->accepted = false;
        $res->rejected = false;
        $res->data = Array();

        if ($request) {
            $offer_id  = $request->get('offer');
        } else {
            $offer_id = \Route::input('one');
        }

        if ($offer_id) {
            $res->data = $this->mdl('OffersMdl')->chOfferByUser($offer_id, \Auth::user()->id);

            if (!empty($res->data)) {
                $res->data->created_at = convToTimeZone($res->data->created_at, true);

                if ($res->data->from_user_id == \Auth::user()->id) {
                    $res->employer = true;

                    $res->employer_info = userPhotoFull(\Auth::user());
                } else $res->employer_info = userPhotoFull(\App\User::find($res->data->from_user_id));

                if ($res->data->to_user_id == \Auth::user()->id) {
                    $res->contractor_info = userPhotoFull(\Auth::user());
                } else $res->contractor_info = userPhotoFull(\App\User::find($res->data->to_user_id));


                if (!$res->employer && ($res->data->status == 0)) $res->can_proc = true;
            }
        }


        return $res;
    }

    public function postData($request)
    {
        $stat = 1;
        $msg = routeClear('home');

        $proc = $request->get('proc');
        $offer_info = $this->getInfoData($request);
        if ($offer_info->can_proc) {
            $table = $this->mdlOrm('OffersMdl');
            $table = $table::find($offer_info->data->id);
            if (!empty($table)) {

                $url_link = routeClear('home_offer', Array(), false).'/'.$offer_info->data->id;

                if ($proc == 1) {
                    $table->status = 1;

                    // notification
                    $this->ctl('Home.NotificationsMsgsCtl')->offer($offer_info->data->from_user_id, $offer_info->data->job_id, $url_link, 1);
                } else {
                    $table->status = 2;

                    // notification
                    $this->ctl('Home.NotificationsMsgsCtl')->offer($offer_info->data->from_user_id, $offer_info->data->job_id, $url_link, 2);
                }

                $table->save();
            }
        }


        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
