<?php namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;

class HomeController extends Controller {

//	const ROUTE_CAREGIVER = 'employee.profile.edit';
	const ROUTE_CAREGIVER = 'employee.find-work';
	const ROUTE_ADMIN = 'admin.dashboard';
	const ROUTE_PARENT = 'client.jobs.my-jobs';

    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
    {
        $route = self::ROUTE_PARENT;
        if ($request->user()->is_caregiver == 1) {
            $route = App::environment() == 'staging' ? self::ROUTE_CAREGIVER : 'employee.profile.edit';
        } else if ($request->user()->is_admin == 1) {
            $route = self::ROUTE_ADMIN;
        }

        return redirect()->route($route);
	}

    public function getOffer()
    {
        $view = 'home.offer';
        $ctl = 'Home.OfferCtl';

        return $this->setVC($view, $ctl);
    }

    public function postOffer(Request $request)
    {
        return $this->ctl('Home.OfferCtl')->postData($request);
    }

    public function getInvite()
    {
        $view = 'home.invite';
        $ctl = 'Home.InviteCtl';

        return $this->setVC($view, $ctl);
    }

    public function postInvite(Request $request)
    {
        return $this->ctl('Home.InviteCtl')->postData($request);
    }

    public function getNotifications()
    {
        $view = 'home.notifications';
        $ctl = 'Home.NotificationsCtl';

        return $this->setVC($view, $ctl);
    }
    
    public function postNotifications(Request $request)
    {
        return $this->ctl('Home.NotificationsCtl')->postData($request);
    }
}
