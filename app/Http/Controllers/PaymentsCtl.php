<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentsCtl extends Controller {


    public function make($data, $order_data, $currency_code = 'AUD')
    {
        if (is_numeric($data)) {
            $table = $this->mdlOrm('CardsMdl');
            $data = $table::where('user_id', \Auth::user()->id)->where('id', $data)->where('card_status', 1)->first();
        }


        $requestParams = array(
           'IPADDRESS' => \Request::ip(),
           'PAYMENTACTION' => 'Sale'
        );

        $creditCardDetails = array(
           'CREDITCARDTYPE' => $data->cart_type,
           'ACCT' => $data->card_number,
           'EXPDATE' => $data->card_expdate,
           'CVV2' => $data->card_cvv2,
        );

        $payerDetails = array(
           'FIRSTNAME' => $data->card_firstname,
           'LASTNAME' => $data->card_lastname,
           'COUNTRYCODE' => $data->card_country_code,
           //'STATE' => 'NY',
           'CITY' => $data->card_city,
           'STREET' => $data->card_address1,
           'STREET2' => $data->card_address2,
           'ZIP' => $data->card_zip_code,
        );

        $orderParams = array(
           'AMT' => $order_data['amount'],
           //'ITEMAMT' => '496',
           //'SHIPPINGAMT' => '4',
           'CURRENCYCODE' => $currency_code,
        );

        $item = array(
           'L_NAME0' => $order_data['name'],
           'L_DESC0' => $order_data['description'],
           'L_AMT0' => $order_data['amount'],
           'L_QTY0' => '1',
        );

        $response = $this->send(
            'DoDirectPayment',
            $requestParams + $creditCardDetails + $payerDetails + $orderParams + $item,
            $order_data
        );

        return $response;
    }

    private function send($method, $data, $order_data)
    {
        $res = null;
        if (!isset($order_data['refund'])) $order_data['refund'] = 0;
        if (!isset($order_data['job_id'])) $order_data['job_id'] = 0;
        if (!isset($order_data['offer_id'])) $order_data['offer_id'] = 0;
        if (!isset($order_data['to_user_id'])) $order_data['to_user_id'] = 0;

        $paypal = $this->ext('PayPal');
        $response = $paypal -> request($method, $data);

        //print_r($response);
        //print_r($order_data);
        //print_r($data);

        if( is_array($response)) {
            if ($response['ACK'] == 'Success') { 
                $transaction_id = $response['TRANSACTIONID'];

                $table = $this->mdlOrm('PaymentsMdl');
                $table->from_user_id = \Auth::user()->id;
                $table->to_user_id = $order_data['to_user_id'];
                $table->job_id = $order_data['job_id'];
                $table->transaction_id = $transaction_id;
                $table->payment_amount = $order_data['amount'];
                $table->payment_refund = $order_data['refund'];
                $table->payment_description = $order_data['description'];

                $table->save();

                // user balance
                if ($order_data['to_user_id'] != 0) {
                    $table = $this->mdlOrm('UsersMdl');
                    $table = $table::find($order_data['to_user_id']);
                    $table->user_balance = $table->user_balance + $order_data['amount'];
                    $table->save();
                }
            } else {
                $res = $response['L_ERRORCODE0'].': '.$response['L_SHORTMESSAGE0'].' - '.$response['L_LONGMESSAGE0'];
            }
        } else {
            $res = 'try later...';
        }

        return $res;
    }
}
