<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoryCtl extends Controller {

    public function getList()
    {
        $mdl = $this->mdlOrm('CategoryMdl');
        $res = new \StdClass();
        $res->main = $mdl->where('main_category', '=',0)->get();
        $res->sub = $mdl->where('main_category', '!=',0)->get();

        return $res;
    }

    //public function getMainCatBySub($id)
    //{
        //$res = 0;

        //$info = $this->mdl('CategoryMdl')->where('id', $id)->first();
        //if (!empty($info)) $res = $info->main_category;

        //return $res;
    //}

}
