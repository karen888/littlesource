<?php namespace App\Http\Controllers;


use App\User;
use Illuminate\Auth\Passwords\PasswordResetServiceProvider;

class IndexController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return \App\Http\Controllers\IndexController
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index()
    {
        $this->setView('index');
        $this->setMenu();
        return $this->view;
    }

    public function showEmailActivation($code){
        $user = User::query()->where('activation_code' , $code)->first();
        if(!$user)
            return redirect('/');
        return view('emails.user.activate', ['user' => $user]);
    }

    public function showEmailResetPassword($token){
        return view('emails.password', ['token' => $token]);
    }
}
