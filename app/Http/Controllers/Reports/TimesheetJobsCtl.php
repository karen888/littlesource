<?php namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TimesheetJobsCtl extends TimesheetCtl {

    public function subRoute($request = null)
    {
        $res = $this->procRoute($this, $request);
        return $res;
    }

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $week = $this->week();
        $this->withViewComp($view_file, 'week', $week);

        print_r($week);

        $contracts = $this->getInfoData();
        $this->withViewComp($view_file, 'contracts', $contracts);

    }

    public function getInfoData()
    {
        $res = $this->mdl('OffersMdl')->getOffersForContractorTimesheet(\Auth::user()->id, $this->week->date_start, $this->week->date_end);

        if (!empty($res)) {
            foreach ($res as $val) {
                $val->timesheets = $this->getTimeByIds($val->timesheets_ids);
            }
        }

        print_r($res);

        return $res;
    }

    public function postData($request)
    {
        $stat = 4;
        $msg = 'err';

        $week = $this->week();
        if ($week->curr_week) {

            \Validator::extend('ch_hm_inp', function($attribute, $value, $parameters) use ($request){
                $hours = $value;
                $minutes = $request->get('minutes');

                if (($hours == 0) && ($minutes == 0)) return false;

                return true;
            });

            $hours = sprintf('%02d', $request->get('hours'));
            $minutes =  sprintf('%02d', $request->get('minutes'));
            $hm_inp = $hours . '.' . $minutes;

            //echo $hm_inp . '<br>';

            $sum_day = $this->mdl('TimesheetsMdl')->sumTimeDay($request->get('day'));
            $calc_sum_day = $sum_day + $hm_inp;

            //echo $sum_day . ' !! ' . $calc_sum_day;

            \Validator::extend('ch_hm_val', function($attribute, $value, $parameters) use ($hm_inp, $calc_sum_day){
                if ($calc_sum_day > 24) return false;

                return true;
            });

            $validator = \Validator::make(
                $request->all(),
                Array(
                    'hours' => 'ch_hm_inp|ch_hm_val',
                ),
                Array(
                    'hours.ch_hm_inp' => 'Choose hours or minutes',
                    'hours.ch_hm_val' => 'Wrong data. '.number_format($calc_sum_day, 2, ':', '') . ' hours!!'.$sum_day,
                )
            );

            $ch_valid = $this->chValid($validator);

            if (!empty($ch_valid)) {
                $stat = 0;
                $msg = $ch_valid;
            } else {
                $msg = '1';
            }
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}


