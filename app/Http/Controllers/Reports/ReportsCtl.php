<?php namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportsCtl extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function getRoute()
    {
        return $this->procRoute('reports', null, null, 'transaction-history');
    }

    public function postRoute(Request $request)
    {
        return $this->procRoute('reports', $request, null, 'transaction-history');
    }

}
