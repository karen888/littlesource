<?php namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransactionHistoryCtl extends Controller {

    public function subRoute($request = null)
    {
        $res = null;
 
        $two = \Route::input('two');
        if ($two == 'billing') {
            $res = $this->procRoute('reports', $request, 'trans-hist-billing');

        } else if ($two == 'earning') {
            $res = $this->procRoute('reports', $request, 'trans-hist-earning');
        } else {
            //$res = $this->procRoute($this, $request);
            $res = $this->procRoute('reports', $request, 'trans-hist-billing');
        }


        return $res;
    }

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {

    }

    public function getInfo($billing = true)
    {
        $res = Array();

        $date_from = \Input::get('date_from');
        $date_to = \Input::get('date_to');
        if ($date_from && $date_to) {
            $date_to = \Carbon\Carbon::parse($date_to)->addDay();
            $res = $this->mdl('PaymentsMdl')->getByDate(\Auth::user()->id, $date_from, $date_to, $billing);
            if (!empty($res)) {
                foreach ($res as $val) {
                    $val->created_at = convToTimezone($val->created_at)->format('j M Y');
                }
            }
        }

        return $res;
    }

    public function postData($request)
    {

    }
}
