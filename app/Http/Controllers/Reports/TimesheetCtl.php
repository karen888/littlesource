<?php namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TimesheetCtl extends Controller {

    protected $week = Array();

    public function subRoute($request = null)
    {
        $res = null;
 
        $two = \Route::input('two');
        if ($two == 'freelancers') {
            $res = $this->procRoute('reports', $request, 'timesheet-freelancers');

        } else if ($two == 'jobs') {
            $res = $this->procRoute('reports', $request, 'timesheet-jobs');
        } else {
            //$res = $this->procRoute($this, $request);
            $res = $this->procRoute('reports', $request, 'timesheet-freelancers');
        }


        return $res;
    }

    protected function week()
    {
        $week = \Input::get('week');
        $year = \Input::get('year');

        $res = $this->getDatesByWeek($week, $year);

        $this->week = $res;

        return $res;
    }

    public function getDatesByWeek($week_number = null, $year = null) 
    {
        $res = new \StdClass();
        $res->days_of_week = Array();
        $res->next_week = 0;
        $res->last_week = 0;
        $res->next_year = 0;
        $res->last_year = 0;
        $res->week_date = '';
        $res->curr_week = false;
        $res->date_start = 0;
        $res->date_end = 0;
        $res->hours = range(0, 24);
        $res->minutes = range(0, 60);

        if ($week_number == null) $week_number = date('W', \Carbon\Carbon::now()->timestamp);

        $year = $year ? $year : date('Y');
        $week_number = sprintf('%02d', $week_number);

        $curr_week = date('W', \Carbon\Carbon::now()->timestamp);
        $curr_week = sprintf('%02d', $curr_week);
        $curr_year = date('Y', \Carbon\Carbon::now()->timestamp);
        if (($week_number == $curr_week) && ($year == $curr_year)) $res->curr_week = true;
        //$date_start = strtotime($year . 'W' . $week_number . '1 00:00:00');
        //$date_end = strtotime($year . 'W' . $week_number . '7 23:59:59');

        for ($i = 1; $i <= 7; $i++)  {
            $res_tmp = new \StdClass();
            $date = strtotime($year . 'W' . $week_number . $i.' 00:00:00');

            $date_sql = \Carbon\Carbon::createFromTimeStamp($date)->toDateTimeString();

            if ($i == 1) {
                $res->date_start = $date_sql;

                $week_date['start']['month'] = date('M', $date);
                $week_date['start']['day'] = date('j', $date);
                $week_date['start']['year'] = date('Y', $date);
            }

            if ($i == 7) {
                $res->date_end = $date_sql;

                $week_date['end']['month'] = date('M', $date);
                $week_date['end']['day'] = date('j', $date);
                $week_date['end']['year'] = date('Y', $date);
            }

            $res_tmp->view_d = date('D', $date);
            $res_tmp->view_md = date('M j', $date);
            $res_tmp->sql = $date_sql;
            $res->days_of_week[] = $res_tmp;
        }

        $date_start = '';
        $date_end = '';
        if ($week_date['start']['month'] == $week_date['end']['month']) {
            $date_start .= $week_date['start']['month'];
            $date_end .= '';
        } else {
            $date_start .= $week_date['start']['month'];
            $date_end .= $week_date['end']['month'];
        }

        $date_start .= ' ' . $week_date['start']['day'];
        $date_end .= ' ' . $week_date['end']['day'];

        if ($week_date['start']['year'] == $week_date['end']['year']) {
            $date_start .= '';
            $date_end .= ', ' . $week_date['start']['year'];
        } else {
            $date_start .= ', ' . $week_date['start']['year'];
            $date_end .= ', ' . $week_date['end']['year'];
        }

        $res->week_date = $date_start .' - ' . $date_end;


        if ($week_number == 1) {
            $res->last_week = $this->getWeeksInYear($year - 1);
            $res->last_year = $year - 1;

            $res->next_week = $week_number + 1;
            $res->next_year = $year;
        } else if ($week_number == $this->getWeeksInYear($year)){
            $res->last_week = $week_number - 1;
            $res->last_year = $year;

            $res->next_week = 1;
            $res->next_year = $year + 1;
        } else {
            $res->last_week = $week_number - 1;
            $res->last_year = $year;

            $res->next_week = $week_number + 1;
            $res->next_year = $year;
        }
 
        return $res;
    }

    private function getWeeksInYear($year) 
    { 
        $date = date('w', mktime(0, 0, 0, 12, 31, $year)); 
        $day = ($date < 4 ? 31 - $date : 31); 

        $res = date('W', mktime(0, 0, 0, 12, $day, $year)); 

        return $res;
    } 

    protected function toFormat($data)
    {
        $res = number_format($data, 2, '.', '');

        return $res;
    }

    public function getTimeByIds($ids)
    {
        $res = Array();

        $list = $this->mdl('TimesheetsMdl')->getTimeByIds($ids);
        if (!empty($list)) {
            foreach ($list as $val) {
                //$val->timesheet_hours = (int) $val->timesheet_hours;

                if (!isset($res[$val->timesheet_day_of_week])) {
                    $res[$val->timesheet_day_of_week] = $val->timesheet_hours;
                } else {
                    $res[$val->timesheet_day_of_week] = $res[$val->timesheet_day_of_week] + $val->timesheet_hours;
                }

                $res[$val->timesheet_day_of_week] = number_format($res[$val->timesheet_day_of_week], 2, '.', '');
            }
        }

        return $res;
    }
}
