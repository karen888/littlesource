<?php namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TransHistEarningCtl extends TransactionHistoryCtl {

    public function subRoute($request = null)
    {
        $res = $this->procRoute($this, $request);
        return $res;
    }

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $info = $this->getInfo(false);
        $this->withViewComp($view_file, 'data_history', $info);
 
        $this->withViewComp($view_file, 'earning_active', 'active');
    }
}

