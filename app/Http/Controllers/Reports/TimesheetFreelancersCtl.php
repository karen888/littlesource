<?php namespace App\Http\Controllers\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TimesheetFreelancersCtl extends TimesheetCtl {

    public function subRoute($request = null)
    {
        $res = $this->procRoute($this, $request);
        return $res;
    }

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $week = $this->week();

        $this->withViewComp($view_file, 'week', $week);
    }

}

