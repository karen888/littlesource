<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TimeZoneCtl extends Controller {

    public function getDateUser($user_timezone_id)
    {
        $res = new \StdClass();
        $res->timezone_name = '';
        $res->timezone_description = '';
        $res->timezone_date = '';

        if ($user_timezone_id) {
            $table = $this->mdlOrm('TimezonesMdl');
            $info = $table::find($user_timezone_id);
            if (!empty($info)) {
                $info->timezone_date = \Carbon\Carbon::now($info->timezone_name)->format('D g:i A');
                $res = $info;
            }
        }

        return $res;
    }

    public function getCountryUser($user_country_id)
    {
        $res = new \StdClass();
        $res->country_code = '';
        $res->country_name = '';

        if ($user_country_id) {
            $table = $this->mdlOrm('CountriesMdl');
            $info = $table::find($user_country_id);
            if (!empty($info)) {
                $res = $info;
            }
        }

        return $res;
    }
}
