<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingsCtl extends Controller {

    public function getByName($name)
    {
        $info = $this->mdl('SettingsMdl')->getByName($name);

        if (!empty($info)) {
            $setting_values = json_decode($info->setting_values);

            $info = (object) array_merge((array) $info, (array) $setting_values);
        }

        return $info;
    }

    public function saveByName($name, $data, $info_sett = Array())
    {
        if (empty($info_sett)) $info_sett = $this->getByName($name);

        $sett_values = json_decode($info_sett->setting_values, true);
        $sett_values = array_merge($sett_values, $data);
        $sett_values = json_encode($sett_values);

        $data_sql['setting_values'] = $sett_values;

        $table = $this->mdlOrm('SettingsMdl');
        $table::where('setting_name', $name)->update($data_sql);
    }
}
