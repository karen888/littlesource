<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Traits\GeolocationTrait;
use DB;
use Auth;
use App\User;
use App\Entities\Job\SavedJob;
use App\Http\Controllers\Controller;
use App\Http\Models\AllowedLocations;
use App\Http\Models\Category;
use App\Entities\Job\Job;
use App\Entities\Job\JobDuration;
use App\Entities\Job\JobWorkload;
use App\Entities\Job\JobDeclineReason;
use App\Entities\Offer\Offers;
use App\Entities\Qualification\Qualification;
use App\Entities\Applicant\Applicant;
use App\Entities\Criteria\AgeRange;
use App\Entities\Criteria\ChildAge;
use App\Http\Models\MarketplaceVisibility;
use App\Http\Models\MinimumExperience;
use App\Http\Models\MinimumFeedback;
use App\Http\Requests;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class JobsController extends Controller
{
    use GeolocationTrait;


    public function __construct()
    {
        // TODO: remove
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
    }


    /**
     * Display a listing of the Jobs and Contracts..
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // TODO: remove
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }

        $jobs = Auth::user()->jobs()->where(['hired' => 0])->take(3)->orderBy('created_at', 'desc')->get();

        $offers = Offers::where([
            'from_user_id' => Auth::user()->id,
            'status' => 'accept'
        ])->take(3)->orderBy('created_at', 'desc')->get();

        return view('jobs.index', compact('jobs', 'offers'));
    }

    /**
     * Display a listing of the Jobs and Contracts..
     *
     * @return \Illuminate\Http\Response
     */
    public function allJobs()
    {
        // TODO: Remove
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        $jobs =  Job::where(['user_id' => Auth::user()->id])->orderBy('created_at', 'desc')->get();
        $jobs = compact('jobs');
        foreach($jobs['jobs'] as $key=>$v){
            preg_match("#(\d{4})-(\d{1,2})-(\d{1,2})\s(\d{1,2}):(\d{1,2}):(\d{1,2})#ism", $v->created_at, $t);
            $time =  mktime($t[4], $t[5], $t[6], $t[2], $t[3], $t[1]);
            $time = date("M d, Y", $time);
            $jobs['jobs'][$key]->createdata_normal = $time;
        }
        return  view('jobs.my-jobs')->with($jobs);
    }

    /**
     * Display a listing of the contracts.
     *
     * @return \Illuminate\Http\Response
     */
    public function contracts()
    {
        // TODO: remove
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        $contracts = Offers::where(['from_user_id' => Auth::user()->id, 'status' => 'accept'])->orderBy('created_at', 'desc')->get();
        $contracts = $contracts->load('employee');

        return  view('jobs.contracts.index')->with(compact('contracts'));
    }

    public function EmpContracts()
    {
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }

        $empcontract = Offers::where(['to_user_id' => Auth::user()->id])->orderBy('created_at', 'desc')->get();
        
        foreach($empcontract as $key=>$contract){
           $empcontract[$key]->end_date = $contract->job->getCloseDate();
        }
        
        return  view('employee.employee_contracts')->with(compact('empcontract'));
    }

    public function WorkDiary()
    {
        // TODO: REmove
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return  view('employee.employee_work_diary');
    }

    /**
     * Display the My Jobs page for caregivers
     *
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|mixed
     */
    public function myJobs()
    {
        // TODO:: remove
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        $jobs = new Collection();

        $offers = Offers::where('to_user_id', Auth::user()->id)
            ->where('is_active', 1)->get();

        foreach ($offers as $offer) {
            $jobs->add($offer->job);
        }
        return  view('employee.employee_my_jobs')->with(compact('jobs'));
    }

    /**
     * Display Details of The contract
     *
     * @return \Illuminate\Http\Response
     */
    public function contractDetails($contract)
    {
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }

        $contract = Offers::find((int)$contract);
        $by_employee = Auth::user()->is_caregiver;
        $edit = false;
        $feedback = [];

        foreach ($contract->job->feedback as $feed) {
            if (Auth::user()->is_caregiver == $feed->by_employee) {
                $edit = true;
            }

            array_push($feedback, [
                'rating' => $feed->getRating(),
                'comment' => $feed->comment,
                'by_employee' => $feed->by_employee
            ]);
        }
        // print_r($contract);
        return view('jobs.contracts.show', compact('contract', 'feedback', 'edit', 'by_employee'));
    }

    /**
     * Display End Contract
     *
     * @return \Illuminate\Http\Response
     */
    public function endContract($contract)
    {
        $contract = Offers::find((int)$contract);
        $feedback_type = Auth::user()->is_caregiver ? 'client' : 'employee';
        
        return view('jobs.contracts.end-contract', compact('contract', 'feedback_type', 'by_employee'));
    }

    /**
     * Display Form for a Job posting
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $previousJobs = Auth::user()->jobs;
        $previousJobs = $previousJobs->map(function ($job) {
            return ['value' => $job->id, 'text' => $job->title];
        })->toArray();

        return view('jobs.create', [
            'previousJobs' => $previousJobs,
            'categories' => Category::customSelectListForActiveCategories(),
            'jobDurations' => JobDuration::customSelectListForActiveJobDurations(),
            'jobWorkloads' => JobWorkload::customSelectListForActiveJobWorkloads(),
            'marketplaceVisibilities' => MarketplaceVisibility::customSelectListForActiveMarketplaceVisibilities(),
            'minimumFeedbacks' => MinimumFeedback::customSelectListForActiveMinimumFeedback(),
            'allowedLocations' => AllowedLocations::customSelectListForActiveAllowedLocations(),
            'minimumExperiences' => MinimumExperience::customSelectListForActiveMinimumExperiences(),
        ]);
    }

    public function searchContract()
    {
        return redirect()->route('employee.profile');
        //$offers = Offers::where(['from_user_id' => Auth::user()->id, 'status' => 'accept'])->orderBy('created_at', 'desc')->get();
        //return $offers;
        $query= Request::input('query');
        $id = Auth::user()->id;
        $offers =DB::table('offers')
            ->join('users', 'offers.to_user_id', '=', 'users.id')
            ->select('offers.*','users.first_name','users.last_name','users.user_photo_64')
            ->where('offers.from_user_id',$id)
            ->where('offers.status','accept')
            ->orderBy('offers.created_at', 'desc')
            ->get();

           return  $offers;
            
    }

    /**
     * View Details for job posting
     *
     * @return \Illuminate\Http\Response
     */
    public function details(Job $job)
    {
        $job->load('user.info');

        return view('jobs.details', compact('job'));
    }

    /**
     * View Details for job posting for Caregiver
     *
     * @return \Illuminate\Http\Response
     */
    public function detailsCaregiver(Job $job)
    {

        $job->load('user.info');
        $is_caregiver = Auth::user()->is_caregiver;
        
        $qualifications = $job->qualification;
        $qualifications = $qualifications->map(function($qualification) {
            return $qualification->name;
        })->toArray();
        $qualifications = implode(', ', $qualifications);

        $child_age_range = json_decode($job->child_age_range);
        if (is_array($child_age_range)) {
            $child_age_range = ChildAge::whereIn('id', $child_age_range)->get();
        } else {
            $child_age_range = ChildAge::where('id', $child_age_range)->get();
        }
        $child_age_range = $child_age_range->map(function($age_range) {
            return $age_range->name;
        })->toArray();
        $child_age_range = implode(', ', $child_age_range);

        $age = json_decode($job->age);
        if (is_array($age)) {
            $age = AgeRange::whereIn('id', $age)->get();
        } else {
            $age = AgeRange::where('id', $age)->get();
        }
        $age = $age->map(function($age_range) {
            return $age_range->name;
        })->toArray();
        $age = implode(', ', $age);

        $offer = (bool)Offers::where([
            'job_id' => $job->id,
            'to_user_id' => Auth::user()->id
        ])->where('status', '<>', 'decline')->count();

        $questions = $job->questions->map(function($item) {
            return $item->body;
        })->toArray();
        
        $jobInvite = $job->jobInvite->where('caregiver_id', Auth::user()->id)->where('status', 'sended')->first();
        $jobInterview = $job->jobInterview()->where('employee_id', Auth::user()->id)->where('status', 'accept')->first();
        $proposal = (bool)Applicant::where(['job_id' => $job->id,'user_id' => Auth::user()->id])->count();
        $declineReason = JobDeclineReason::customSelectList();
        return view('jobs.details_caregiver', compact(
            'job',
            'jobInvite',
            'declineReason',
            'jobInterview',
            'proposal',
            'offer',
            'qualifications',
            'child_age_range',
            'age',
            'questions',
            'is_caregiver'
        ));
    }

    /**
     * View Edit form for job posting
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {

        return view('jobs.edit', [
            'categories' => Category::customSelectListForActiveCategories(),
            'jobDurations' => JobDuration::customSelectListForActiveJobDurations(),
            'jobWorkloads' => JobWorkload::customSelectListForActiveJobWorkloads(),
            'marketplaceVisibilities' => MarketplaceVisibility::customSelectListForActiveMarketplaceVisibilities(),
            'minimumFeedbacks' => MinimumFeedback::customSelectListForActiveMinimumFeedback(),
            'allowedLocations' => AllowedLocations::customSelectListForActiveAllowedLocations(),
            'minimumExperiences' => MinimumExperience::customSelectListForActiveMinimumExperiences(),
            'job' => $job,
        ]);
    }

    /**
     * View Applicants for job posting
     *
     * @return \Illuminate\Http\Response
     */
    public function applicants(Job $job)
    {
        return view('jobs.applicants', compact('job'));
    }

    /**
     * Display Success message for a Job posting
     *
     * @return \Illuminate\Http\Response
     */
    public function success(Job $job, $view = false)
    {
        $criteria = [];
        $needle_criteria = [
            'have_transportation',
            'gender',
            'smoker',
            'comfortable_with_pets',
            'own_children',
            'sick_children'
        ];

        
        foreach ($job->toArray() as $key=>$item) {
            if (in_array($key, $needle_criteria) && $item) {
                switch ($key) {
                    case 'have_transportation':
                        $criteria['drivers_licence'] = $item;
                        break;
                    case 'comfortable_with_pets':
                        $criteria['pets'] = $item;
                        break;
                    case 'sick_children':
                        $criteria['care_sick'] = $item;
                        break;
                    default:
                        $criteria[$key] = $item;
                        break;
                }

            }
        }

        $users_range = $this->getCaregiversByRange(25);
        $employee = [];

        $employee_all = User::criteriaSearch($criteria)
            ->where('is_caregiver', true)
            ->whereIn('id', $users_range)
            ->get();

        if (count($employee_all) < 10) {
            $users = User::where(['is_caregiver' => 1])->whereNotIn('id', $users_range)->take((10 - count($employee_all)))->orderBy('created_at', 'desc')->get();
            $employee = $users->map(function($item) {
                if(
                    $item->info->loc_adress != NULL
                    and $item->phone_verifed == 1
                ) {
                    return [
                        'id' => $item->id,
                        'photo_url' => $item->photo_url,
                        'name' => $item->name,
                    ];
                }
            })->toArray();

            foreach ($employee_all as $itm) {
                array_unshift($employee, [
                    'id' => $itm->id,
                    'photo_url' => $itm->photo_url,
                    'name' => $itm->name,
                ]);
            }

        } elseif (count($employee_all) > 11) {
            $ids = [];
            foreach ($employee_all as $itm) {
                $ids[$itm->id] = (float)$itm->getEmployeeFeedback();
            }
            arsort($ids, SORT_NUMERIC);
            foreach ($ids as $key=>$itm) {
                array_push($employee, $key);
            }

            $employee = array_slice($employee, 0, 10);
            $employee = User::whereIn('id', $employee)->get()->map(function($item) {
                return [
                    'id' => $item->id,
                    'photo_url' => $item->photo_url,
                    'name' => $item->name,
                ];
            })->toArray();

        } else {
            foreach ($employee_all as $itm) {
                array_unshift($employee, [
                    'id' => $itm->id,
                    'photo_url' => $itm->photo_url,
                    'name' => $itm->name,
                ]);
            }
        }

        return view('jobs.success', compact('job', 'employee', 'view'));
    }

    public function searchResults()
    {
        return view('jobs.searchresults');
    }

    public function findWork(Request $request)
    {
        if(App::environment() != 'staging') {
            return redirect()->route('employee.profile');
        }

        $user = $request->user();

        $user->cert_status = $user->certificates()->where('status', 1)->exists();
        $user->qua_status = $user->educations()->where('status', 1)->exists();

        return view('jobs.find-jobs.find_work',[
            'user_progress'=>Auth::user()->getProgress(),
            'user' => $user,
            'jobDurations' => JobDuration::customSelectListForActiveJobDurations(),
            'jobWorkloads' => JobWorkload::customSelectListForActiveJobWorkloads(),
            'qualifications' => Qualification::customListQualifications()
        ]);
    }

    public function savedJobs()
    {

        $saveJobs = SavedJob::where('caregiver_id', Auth::user()->id)->get();
        $jobs = new Collection();

        foreach ($saveJobs as $saveJob) {
            $jobs->add($saveJob->job);
        }

        foreach ($jobs as $key=>$item) {
            $categories = $item->category;
            $categories = $categories->map(function($category) {
                return $category->name;
            })->toArray();

            $jobs[$key]['categories'] = $categories;
        }
        return view('jobs.saved_jobs', compact('jobs'));
    }
}
