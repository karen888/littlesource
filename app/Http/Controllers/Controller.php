<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App\Http\Exts\ExtCtl;

abstract class Controller extends BaseController {

    use DispatchesCommands, ValidatesRequests, ExtCtl;

    protected $view = null;
    protected $mdl;
    protected $ctl;
    public $item = Array();

    public function setView($name)
    {
        $this->view = view($name);
    }

    public function setMenu()
    {
        //if ($this->view) {
            //$menu_ctl = new \App\Http\Controllers\MenuController();
            //$view = $menu_ctl->get();
            //$this->view->with('main_menu', $view);
        //}
    }

    protected function procRoute($catalog, $request = null, $item = null, $default = 'my-contracts')
    {
        if (is_object($catalog)) {
            if ($request) {
                $res =  $catalog->postData($request);
            } else {
                $res = $this->setVC($catalog->item['view'], $catalog);
            }
        } else {

            if (!$item) $item = \Route::input('one');
            $item = strtolower($item);
            if (!$item) $item = $default;

            $item_view = str_replace('-', '_', $item);

            $item_arr = explode('_', $item_view);
            $item_ctl = '';
            foreach ($item_arr as $val) {
                $val = ucfirst($val);
                $item_ctl .= $val;
            }

            $catalog = strtolower($catalog);

            $item_view = $catalog.'.'.$item_view;
            $item_ctl = ucfirst($catalog).'.'.$item_ctl.'Ctl';

            $ctl = $this->ctl($item_ctl);
            if (method_exists($ctl, 'SubRoute')) {
                $ctl->item['view'] = $item_view;
                $ctl->item['ctl'] = $item_ctl;
                $res = $ctl->subRoute($request);
            } else {
                if ($request) {
                    $res = $ctl->postData($request);
                } else {
                    //$res = $ctl->setVC($item_view, $item_ctl);
                    $res = $ctl->setVC($item_view, $ctl);
                }
            }

            //try {
                //$res =  $this->ctl($item_ctl)->subRoute($request);
                //$res->item['view'] = $item_view;
                //$res->item['ctl'] = $item_ctl;
            //} catch (\Exception $e) {
                //if ($request) {
                    //$res =  $this->ctl($item_ctl)->postData($request);
                //} else {
                    //$res = $this->setVC($item_view, $item_ctl);
                //}
            //}
        }

        return $res;
    }
}
