<?php 

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CommonCtl extends Controller {

    public function getJobInfo()
    {
        $view = 'common.job_info';
        $ctl = 'Common.JobInfoCtl';

        return $this->setVC($view, $ctl);
    }

    public function postJobInfo(Request $request)
    {
        return $this->ctl('Common.JobInfoCtl')->postData($request);
    }

    public function getUserInfo()
    {
        $view = 'common.user_info';
        $ctl = 'Common.UserInfoCtl';

        return $this->setVC($view, $ctl);
    }

}
