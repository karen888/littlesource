<?php namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HowitworksController extends Controller {

	public function index() {
		
        return view('common/how-it-works');
    }
	
	public function sqlbuddy() {
		
        return view('sqlbuddy/index');
    }
}
