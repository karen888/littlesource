<?php 

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JobInfoCtl extends Controller {

    protected $view_sub_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $job_info = Array();
        $res = null;

        $job = \Route::input('job');
        if ($job) {
            $mdl = $this->mdl('Jobs.JobsMdl');
            $job_info = $mdl->getInfoJobByPath($job);
            if (!empty($job_info)) {
                $file = \Input::get('file');
                if ($file) {
                    $res = $this->downFile($job_info);
                } else {
                    $job_info->skills = $this->mdl('Skills.JobsMdl')->getListByJob($job_info->id);
                    $job_info->job_duration = $this->ext('JobDuration')->getByKey($job_info->job_duration);

                    if ($job_info->job_fixed_price == '0.00') $job_info->fixed = false;
                    else $job_info->fixed = true;

                    if ($job_info->job_end_date != '0000-00-00') { 
                        $job_info->job_end_date = \Carbon\Carbon::parse($job_info->job_end_date)->format('F j, Y');
                    } else $job_info->job_end_date = '';

                    $applies = $this->getApplies($job_info);
                    $ch_apply = $this->chAppliedByUser($job_info);

                    $this->withViewComp($view_file, 'ch_apply', $ch_apply);
                    $this->withViewComp($view_file, 'applies_list', $applies);
                    $this->withViewComp($view_file, 'job_info', $job_info);
                }
            }
        }

        if ($res) return $res;
    }

    private function chAppliedByUser($job_info)
    {
        $res = Array();
        if (!\Auth::guest()) $res = $this->mdl('Jobs.AppliesMdl')->chAppliedByUser($job_info->id, \Auth::user()->id);

        return $res;
    }

    private function getApplies($info)
    {
        $res = new \StdClass();
        $res->count = 0;
        $res->list = Array();

        if (!empty($info->applies_ids)) {
            $res->list  = $this->mdl('Jobs.AppliesMdl')->getByIds($info->applies_ids);
            $res->count = count($res->list);
            if (!empty($res->list)) {
                foreach ($res->list as $key => $val) {
                    $val->created_human = convToTimeZone($val->created_at)->diffForHumans();
                }

            }
        }

        return $res;
    }

    private function downFile($info)
    {
        $info->file = base64_decode($info->file);

        $response = \Response::make($info->file, 200);
        $response->header('Content-length', $info->file_size);
        $response->header('Content-Type', $info->file_mime);
        $response->header('Content-Disposition', 'attachment; filename='.$info->file_name);

        return $response;                
    }

    public function postData($request)
    {
        $stat = 1;
        $msg = routeClear('home').'/';

        $job = $request->get('job');
        
        $mdl = $this->mdl('Jobs.JobsMdl');
        $job_info = $mdl->getInfoJobByPath($job);
        if (!empty($job_info)) {
            $res_ch = $this->mdl('Jobs.AppliesMdl')->chAppliedByUser($job_info->id, \Auth::user()->id);
            if (empty($res_ch->offer_status)) {
                $this->ctl('ActionsCtl')->delApplies($job, \Auth::user()->id);
            }
        }
        
        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
