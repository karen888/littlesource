<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserInfoCtl extends Controller {

    protected $view_sub_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $user_info = $this->userInfo();
        $this->withViewComp($view_file, 'user_info', $user_info);
    }

    private function userInfo()
    {
        $res = Array();
        $path = \Route::input('user');
        if ($path) {
            $res = \App::make('\\App\\User')->where('user_path', $path)->first();
            if (!empty($res)) {
                $res->user_photo_100 = userPhoto($res)->photo_100;
                $res->skills = $this->mdl('Skills.UsersMdl')->getListByUser($res->id);

                $res->history = $this->getHistory($res->id);

                $res->timezone_date = $this->ctl('TimeZoneCtl')->getDateUser($res->timezone_id)->timezone_date;
                $res->country_name = $this->ctl('TimeZoneCtl')->getCountryUser($res->country_id)->country_name;
            }
        }

        return $res;
    }

    private function getHistory($user_id)
    {
        $res = new \StdClass();
        $res->progress = Array();
        $res->finished = Array();
        $res->rate = 0;
        $res->rate_percent = 0;
        $res->jobs_count = 0;
        $rate_c = 0;

        //$this->ctl('FeedbackCtl')->getRate($user_id);

        $offers = $this->mdl('OffersMdl')->getAllOfferByUser($user_id);
        if(!empty($offers)) {
            $res->jobs_count = count($offers);
            //\Carbon\Carbon::format
            foreach ($offers as $val) {

                $val->created_at = \Carbon\Carbon::parse($val->created_at)->format('M Y');
                $val->finished_at = \Carbon\Carbon::parse($val->updated_at)->format('M Y');

                $feedback = $this->getFeedback($user_id, $val);
                $val->feedback = $feedback['feedback'];

                $res->rate = $res->rate + $feedback['rate'];
                if (!empty($feedback['feedback'])) $rate_c++;

                // work in progress
                if ($val->status == 1) {
                    $res->progress[] = $val;
                } else {
                    $res->finished[] = $val;
                }
            }
        }


        if ($rate_c !== 0) {
            $res->rate = $res->rate / $rate_c;
            $res->rate_percent = ($res->rate/5) * 100;
        }

        //print_r($offers);
        return $res;
    }

    private function getFeedback($user_id, $val)
    {
        $res['feedback'] = Array();
        $res['rate'] = 0;

        if ($user_id == $val->from_user_id) {
            $feedback_user_id = $val->to_user_id;
            $feedback_this_user_id = $user_id;
        } else {
            $feedback_user_id = $val->from_user_id;
            $feedback_this_user_id = $user_id;
        }

        $table = $this->mdlOrm('FeedbacksMdl');
        //$feedbacks = $table::where('job_id', $val->job_id)->whereIN('from_user_id', Array($val->from_user_id, $val->to_user_id))->get();
        //$feedbacks = $table::where('job_id', $val->job_id)->whereIN('from_user_id', Array($val->from_user_id, $val->to_user_id))->get();
        $feedbacks = $table::where('offer_id', $val->id)->get();

        //print_r($feedbacks);
        //if (!empty($feedbacks)) {
            //$feedback->feedback_rate_procent = ($feedback->feedback_rate/5) * 100;
        //}
        if (count($feedbacks) == 2) {
            foreach ($feedbacks as $vall) {
                if ($user_id == $vall->to_user_id) {
                    $vall->feedback_rate_procent = ($vall->feedback_rate/5) * 100;

                    $res['feedback'] = $vall;

                    $res['rate'] = $vall->feedback_rate;
                }
            }
        }

        return $res;
    }

    public function getRate($user_id)
    {
        $list_tmp = Array();
        $res = new \StdClass();
        $res->numeric = 0;
        $res->percent = 0;
        $rate_c = 0;

        $list = $this->mdl('OffersMdl')->getDataForRate($user_id);
        if (!empty($list)) {
            foreach($list as $val) {
                if (isset($list_tmp[$val->id])) {
                    if ($user_id == $val->feedback_to_user_id) {
                        $list_tmp[$val->id] = $val->feedback_rate;
                    }
                    $res->numeric = $res->numeric + $list_tmp[$val->id];

                    $rate_c++;
                } else {
                    if ($user_id == $val->feedback_to_user_id) {
                        $list_tmp[$val->id] = $val->feedback_rate;
                    } else $list_tmp[$val->id] = 0;
                }
            }
        }

        if ($rate_c) {
            $res->numeric = $res->numeric / $rate_c;
            $res->percent = ($res->numeric/5) * 100;
        }

        return $res;
    }

    public function postData($request)
    {

    }
}
