<?php

namespace App\Http\Controllers\Parent;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Support\Facades\View;

class ParentController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $currentUserId = Auth::user()->id;

        $user_type = DB::table('users')
            ->select('is_caregiver')
            ->where('id', $currentUserId)
            ->get();
        // TODO # need to change logic
        if ($user_type[0]->is_caregiver == 0){
            $this->setView('parent.dashboard');
        }
        else{
            $this->setView('caregiver.dashboard');
        }
        $this->setMenu();
        return $this->view;

    }

}
