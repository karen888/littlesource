<?php

namespace App\Http\Controllers\Api\V1\Offers;

use App\Entities\Applicant\Applicant;
use Auth;
use Illuminate\Support\Facades\Event;
use App\Events\AddNotificationsData;
use App\Http\Requests;
use App\Http\Requests\HireEmployeeRequest;
use App\Http\Requests\AcceptOfferRequest;
use App\Http\Requests\DeclineOfferRequest;
use App\Http\Controllers\Controller;
use App\Entities\Offer\Offers;
use App\Entities\Message\Message;
use App\Entities\Message\MessagesThread;
use App\User;
use App\Entities\Job\Job;

class OffersController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;
    
    /**
     * Add offer.
     *
     * @param HireEmployeeRequest $request
     */
    public function store(HireEmployeeRequest $request)
    {
        $client = Auth::user();
        $job = Job::find($request->job_id);
        $employee = User::find($request->to_user_id);

        if (!$employee->is_caregiver) {
            return $this->responseValidationOld(
                [
                    "caregiver" => [
                        "Incorrect caregiver"
                    ]
                ]
            );
        }

        if (!$job) {
            return $this->responseValidationOld(
                [
                    "job_id" => [
                        "Incorrect job"
                    ]
                ]
            );
        }

        if ($job->user_id !== $client->id) {
            return $this->responseValidationOld(
                [
                    "job_id" => [
                        "Incorrect job"
                    ]
                ]
            );
        }

        if (Offers::where(["job_id" => $job->id, "to_user_id" => $employee->id])->count()) {
            return $this->responseValidationOld(
                [
                    "offer" => [
                        "Incorrect offer"
                    ]
                ]
            );
        }

        if ((int)$request->hourly_rate < 1) {
            return $this->responseValidationOld(['hourly_rate' => ['The hourly rate must be at least 1.']]);
        }

        $offer = new Offers;
        $offer->add($request, $client);

        $thread = MessagesThread::where(
            [
                'job_id' => $job->id,
                'sender_id' => $client->id,
                'receiver_id' => $employee->id,
                'is_archived' => false
            ]
        )->first();

        if (!$thread) {
            $thread = new MessagesThread;
            $thread->add($job->id, $client->id, $employee->id);
        }

        $message = new Message;
        $message->sendOfferMessage($job, $client, $employee, $offer, $thread);

        $sameApplicantOccurrences = Applicant::where('job_id', $job->id)
            ->where('is_active', 1)
            ->where('user_id', $employee->id)
            ->get();
        foreach ($sameApplicantOccurrences as $sameApplicant) {
            $sameApplicant->is_active = 0;
            $sameApplicant->save();
        }

        Applicant::create([
            'job_id' => $job->id,
            'user_id' => $offer->to_user_id,
            'participation_method' => 'offer'
        ]);

        flash()->success('Offer successfully sent!');

        return $this->setResponse(
            false,
            route('client.jobs.my-jobs.details', $job->id)
        );
    }

    /**
     * Accept offer by Employee.
     *
     * @param AcceptOfferRequest $request
     */
    public function accept(AcceptOfferRequest $request)
    {

        if(Auth::user()->isUserFullyVerified() === false) {
            return ['security_error' => true];
        }

        $offer = Offers::find($request->offer);
        $job = $offer->job;

        if ($offer->status !== 'sended' && $offer->from_user_id !== Auth::user()->id) {
            return $this->setResponse();
        }

        $offer->status = 'accept';
        $offer->is_active = true;
        $offer->save();

        $thread = MessagesThread::where(
            [
                'job_id' => $offer->job_id,
                'sender_id' => $offer->from_user_id,
                'receiver_id' => $offer->to_user_id,
                'is_archived' => false
            ]
        )->first();

        $message = new Message;
        $message->message_thread_id = $thread->id;
        $message->user_id = Auth::user()->id;
        $message->message = $request->message_accept;
        $message->save();

        $job->hired = true;
        $job->closed = true;
        $job->save();

        Event::fire(
            new AddNotificationsData(
                User::find($offer->from_user_id),
                'offer_accepted',
                ['offer_id' => $offer->id]
            )
        );

        return $this->setResponse();
    }

    /**
     * Decline offer by Employee.
     *
     * @param DeclineOfferRequest $request
     */
    public function decline(DeclineOfferRequest $request)
    {
        $offer = Offers::find($request->offer);

        if ($offer->status != 'sended' && $offer->from_user_id != Auth::user()->id) {
            return $this->setResponse();
        }

        $offer->status = 'decline';
        $offer->decline_reason_id = $request->reason;
        $offer->save();

        if (!empty($request->message_decline)) {
            $thread = MessagesThread::where(
                [
                    'job_id' => $offer->job_id,
                    'sender_id' => $offer->from_user_id,
                    'receiver_id' => $offer->to_user_id,
                    'is_archived' => false
                ]
            )->first();

            $message = new Message;
            $message->message_thread_id = $thread->id;
            $message->user_id = Auth::user()->id;
            $message->message = $request->message_decline;
            $message->save();
        }

        Event::fire(
            new AddNotificationsData(
                User::find($offer->from_user_id),
                'interview_offer_declined_withdrawn',
                ['offer_id' => $offer->id, 'type' => 'offer']
            )
        );

        return $this->setResponse();
    }
}
