<?php

namespace App\Http\Controllers\Api\v1\Applicants;

use App\Http\Controllers\Controller;
use App\Entities\Applicant\Applicant;
use App\Entities\Job\Job;
use Illuminate\Http\Request;
use App\User;

class ApplicantsController extends Controller
{
    use \App\Http\Traits\GeolocationTrait;
    /**
     * Display a listing of Applicants for a Job Posting.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Job $job, Request $request)
    {
        if ($filter = $request->filters) {
            $return = [];
            switch ($filter) {
                case 'name':
                    $applicants = $job->applicants->load('user')->sortBy(function ($applicant) {
                        return strtolower($applicant->user->name);
                    })->values();
                    break;
                case 'best-match':
                    $users_ids = [];
                    $applicants = $job->applicants->load('user');

                    foreach ($applicants as $item) {
                        array_push($users_ids, $item->user_id);
                    }
                    $distances = $this->getCaregiversRange($users_ids);
                    asort($distances, SORT_NUMERIC);

                    $criteria = [];
                    $needle_criteria = [
                        'have_transportation',
                        'gender',
                        'smoker',
                        'comfortable_with_pets',
                        'own_children',
                        'sick_children'
                    ];

                    foreach ($job->toArray() as $key=>$item) {
                        if (in_array($key, $needle_criteria) && $item) {
                            switch ($key) {
                                case 'have_transportation':
                                    $criteria['drivers_licence'] = $item;
                                    break;
                                case 'comfortable_with_pets':
                                    $criteria['pets'] = $item;
                                    break;
                                case 'sick_children':
                                    $criteria['care_sick'] = $item;
                                    break;
                                default:
                                    $criteria[$key] = $item;
                                    break;
                            }
                        }
                    }

                    $employee_all = User::criteriaSearch($criteria)
                        ->select('id')
                        ->where('is_caregiver', true)
                        ->whereIn('id', $users_ids)
                        ->get();

                    if (!empty($employee_all)) {
                        foreach ($distances as $key=>$d) {
                            foreach ($employee_all as $itm) {
                                if ($key == $itm->id) {
                                    unset($distances[$key]);
                                    foreach ($applicants as $item) {
                                        if ($item->user->id != $itm->id) {
                                            continue;
                                        }

                                        $experience = null;
                                        if ($item->user->employee_info->get_experience) {
                                            $experience = $item->user->employee_info->get_experience->name;
                                        }

                                        array_push($return, [
                                            'id' => $item->id,
                                            'is_invitation' => ($item->participation_method == 'invitation'),
                                            'is_offer' => ($item->participation_method == 'offer'),
                                            'is_proposal' => ($item->participation_method == 'proposal'),
                                            'is_hidden' => $item->is_hidden,
                                            'is_shortlisted' => $item->is_shortlisted,
                                            'job_id' => $item->job_id,
                                            'messaged' => $item->messaged,
                                            'user_id' => $item->user_id,
                                            'user' => [
                                                'id' => $item->user->id,
                                                'photo_url' => $item->user->photo_url,
                                                'name' => $item->user->name,
                                                'rate' => $item->user->employee_info->view_rate,
                                                'experience' => $experience,
                                            ],
                                        ]);
                                    }
                                }
                            }
                        }

                        foreach ($distances as $key=>$d) {
                            foreach ($applicants as $item) {
                                if ($item->user->id != $key) {
                                    continue;
                                }

                                $experience = null;
                                if ($item->user->employee_info->get_experience) {
                                    $experience = $item->user->employee_info->get_experience->name;
                                }

                                array_push($return, [
                                    'id' => $item->id,
                                    'is_invitation' => ($item->participation_method == 'invitation'),
                                    'is_offer' => ($item->participation_method == 'offer'),
                                    'is_proposal' => ($item->participation_method == 'proposal'),
                                    'is_hidden' => $item->is_hidden,
                                    'is_shortlisted' => $item->is_shortlisted,
                                    'job_id' => $item->job_id,
                                    'messaged' => $item->messaged,
                                    'user_id' => $item->user_id,
                                    'user' => [
                                        'id' => $item->user->id,
                                        'photo_url' => $item->user->photo_url,
                                        'name' => $item->user->name,
                                        'rate' => $item->user->employee_info->view_rate,
                                        'experience' => $experience,
                                    ],
                                ]);
                            }
                        }
                    } else {
                        foreach ($distances as $key=>$itm) {
                            foreach ($applicants as $item) {
                                if ($item->user->id != $key) continue;

                                $experience = null;
                                if ($item->user->employee_info->get_experience) {
                                    $experience = $item->user->employee_info->get_experience->name;
                                }

                                array_push($return, [
                                    'id' => $item->id,
                                    'is_invitation' => ($item->participation_method == 'invitation'),
                                    'is_offer' => ($item->participation_method == 'offer'),
                                    'is_proposal' => ($item->participation_method == 'proposal'),
                                    'is_hidden' => $item->is_hidden,
                                    'is_shortlisted' => $item->is_shortlisted,
                                    'job_id' => $item->job_id,
                                    'messaged' => $item->messaged,
                                    'user_id' => $item->user_id,
                                    'user' => [
                                        'id' => $item->user->id,
                                        'photo_url' => $item->user->photo_url,
                                        'name' => $item->user->name,
                                        'rate' => $item->user->employee_info->view_rate,
                                        'experience' => $experience,
                                    ],
                                ]);
                            }
                        }
                    }

                    return response()->json($return);
                    break;
                default:
                    $applicants = $job->applicants->load('user');
                break;
            }

            foreach ($applicants as $item) {
                $experience = null;
                if ($item->user->employee_info->get_experience) {
                    $experience = $item->user->employee_info->get_experience->name;
                }

                array_push($return, [
                    'id' => $item->id,
                    'is_invitation' => ($item->participation_method == 'invitation'),
                    'is_offer' => ($item->participation_method == 'offer'),
                    'is_proposal' => ($item->participation_method == 'proposal'),
                    'is_hidden' => $item->is_hidden,
                    'is_shortlisted' => $item->is_shortlisted,
                    'job_id' => $item->job_id,
                    'messaged' => $item->messaged,
                    'user_id' => $item->user_id,
                    'user' => [
                        'id' => $item->user->id,
                        'photo_url' => $item->user->photo_url,
                        'name' => $item->user->name,
                        'rate' => $item->user->employee_info->view_rate,
                        'experience' => $experience,
                    ],
                ]);
            }

            return response()->json($return);
        }
    }

    /**
     * Togle the shortlist status of the applicant
     *
     * @return \Illuminate\Http\Response
     */
    public function shortlist(Applicant $applicant)
    {
        $applicant->is_shortlisted = !$applicant->is_shortlisted;
        $applicant->save();

        if ($applicant->is_shortlisted) {
            $message = 'Applicant shortlisted successfuly';
        } else {
            $message = 'Applicant is no longer shortlisted';
        }

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => $message],
            ]
        );
    }

    /**
     * Togle the hidden status of the applicant
     *
     * @return \Illuminate\Http\Response
     */
    public function hide(Applicant $applicant)
    {
        $applicant->is_hidden = !$applicant->is_hidden;
        $applicant->save();

        if ($applicant->is_hidden) {
            $message = 'Applicant hidden successfuly';
        } else {
            $message = 'Applicant is no longer hidden';
        }

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => $message],
            ]
        );
    }
}
