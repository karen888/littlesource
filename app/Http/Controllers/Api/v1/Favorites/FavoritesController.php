<?php

namespace App\Http\Controllers\Api\V1\Favorites;

use Auth;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Favorites\Favorites;
use App\Http\Requests\FavoritesNotesRequest;
use App\Http\Requests\MyCaregiversRequest;

class FavoritesController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;
    
    public function getList(MyCaregiversRequest $request)
    {
        $ids = [];
        $data = [];
        $favorites = [];

        foreach (Auth::user()->favorites->all() as $key => $itm) {
            array_push($ids, $itm->favorite->id);
            $favorites[$itm->favorite->id]['id'] = $itm->id;
            $favorites[$itm->favorite->id]['photo'] = $itm->favorite->PhotoUrl;
            $favorites[$itm->favorite->id]['name'] = $itm->favorite->name;
            $favorites[$itm->favorite->id]['created_at'] = $itm->created_at->format('M j, Y');
            $favorites[$itm->favorite->id]['notes'] = $itm->notes;
            $favorites[$itm->favorite->id]['notes_origin'] = $itm->notes;
            $favorites[$itm->favorite->id]['employee_profile'] = route('employee.profile', [$itm->favorite->id]);
            $favorites[$itm->favorite->id]['employee_invite'] = route('client.employee.invite', [$itm->favorite->id]);
            $favorites[$itm->favorite->id]['employee_send_message'] = route('messages.index', [$itm->favorite->id]);
            $favorites[$itm->favorite->id]['edit'] = false;
            $favorites[$itm->favorite->id]['error'] = '';
        }

        $users = User::whereIn('id', $ids);

        if (!empty($request->search)) {
            $users = $users->where(function ($query) use ($request) {
                $query->where('first_name', 'like', "%$request->search%")
                    ->orWhere('last_name', 'like', "%$request->search%");
            });
        }

        $users = $users->get();

        foreach ($users as $item) {
            array_push($data, $favorites[$item->id]);
        }

        return $this->setResponse($data);
    }

    public function add($id)
    {
        $favorites = new Favorites;
        $favorites->addFavorite((int)$id);
        $this->setResponse();
    }

    public function delete($id)
    {
        $favorites = new Favorites;
        $favorites->removeFavorite((int)$id);
        $this->setResponse();
    }

    public function updateNotes(FavoritesNotesRequest $request) {
        Favorites::where([
            'id' => $request->id,
            'user_id' => Auth::user()->id
        ])->update(['notes' => $request->notes]);

        $this->setResponse();
    }
}
