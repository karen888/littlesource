<?php

namespace App\Http\Controllers\Api\v1\Common;

use App\Http\Models\State;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Country\Country;
use App\Entities\Qualification\Qualification;
use App\Entities\Job\JobDuration;
use App\Entities\Job\JobWorkload;
use App\Entities\Feedback\RatingOptions;
use App\Entities\Feedback\FeedbackReason;
use App\Http\Models\Category;
use App\Http\Models\Timezone;
use Illuminate\Support\Facades\App;
use ReflectionException;
use Illuminate\Http\Request;

class CommonController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;
    
    /**
     * Returns the list of countries as customselect friendly
     * @return [type] [description]
     */
    public function getCountries() {
        return Country::getCustomSelectCountries();
    }

    public function getStates() {
        return State::all()->toArray();
    }

    /**
     * Returns the list of timezones as customselect friendly
     * @return [type] [description]
     */
    public function getTimezones() {
        return Timezone::getCustomSelectTimezones();
    }
    
    /**
     * Returns the list of qualifications as customselect friendly
     * @return [type] [description]
     */
    public function getListQualifications()
    {
        return $this->setResponse([
            'qualifications'  => Qualification::customListQualifications()
        ]);
    }
    
    /**
     * Returns the list of categories as customselect friendly
     * @return [type] [description]
     */
    public function getListCategories()
    {
        return $this->setResponse([
            'categories_items'  => Category::customListCategories()
        ]);
    }

    /**
     * Returns the list of Job Durations as customselect friendly
     * @return [type] [description]
     */
    public function getListJobDurations()
    {
        return $this->setResponse([
            'job_duration'  => JobDuration::customSelectListForActiveJobDurations()
        ]);
    }

    /**
     * Returns the list of Job Workloads as customselect friendly
     * @return [type] [description]
     */
    public function getListJobWorkloads()
    {
        return $this->setResponse([
            'job_workload'  => JobWorkload::customSelectListForActiveJobWorkloads(),
        ]);
    }

    /**
     * Returns the list of Rating Options
     * @return [type] [description]
     */
    public function getRatingOptions($type)
    {
        return $this->setResponse([
            'rating_options'  => RatingOptions::getList($type)
        ]);
    }
    
    /**
     * Returns the list of Feedback Reason
     * @return [type] [description]
     */
    public function getFeedbackReason()
    {
        return $this->setResponse([
            'feedback_reason'  => FeedbackReason::customList()
        ]);
    }

    /**
     * Returns the list of Age Ranges
     * @param $criteria
     * @return array
     */
    public function getCriteria($criteria){
        try {
            return $this->setResponse([
                (App::make($criteria))::pillsList()
            ]);
        } catch (ReflectionException $e) {
            return $this->responseValidation('Criteria '. $criteria. ' not registered');
        }
    }

    public function getCriteriasList(Request $request){
        $result = [];
        foreach ($request->criterias as $criteria){
            try {
                $result[$criteria] = (App::make($criteria))::pillsList();
            } catch (ReflectionException $e) {
                return $this->responseValidation('Criteria '. $criteria. ' not registered');
            }
        }
        return $result;
    }

    /**
     * Adds criteria of specific type
     * @param $criteria
     * @return array
     */
    public function postCriteria(Request $request, $criteria){
        try {
            $new_creteria = (App::make($criteria))::firstOrCreate(['name'=>ucfirst(strtolower($request->name))]);
            return $this->setResponse([
                $new_creteria->toArray()
            ]);
        } catch (ReflectionException $e) {
            return $this->responseValidation('Criteria '. $criteria. ' not registered');
        }
    }


}
