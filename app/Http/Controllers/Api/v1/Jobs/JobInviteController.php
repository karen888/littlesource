<?php

namespace App\Http\Controllers\Api\V1\Jobs;

use Auth;
use App\User;
use App\Events\AddNotificationsData;
use App\Http\Requests;
use App\Http\Requests\JobInviteRequest;
use App\Http\Requests\Job\JobInviteAcceptRequest;
use App\Http\Requests\JobInviteDeclineRequest;
use App\Http\Requests\JobInviteAllRequest;
use App\Http\Controllers\Controller;
use App\Entities\Job\Job;
use App\Entities\Job\JobDuration;
use App\Entities\Job\JobWorkload;
use App\Entities\Job\JobInvite;
use App\Entities\Job\JobBlockInvite;
use App\Entities\Job\JobInterview;
use App\Entities\Message\Message;
use App\Entities\Message\MessagesThread;
use App\Entities\Applicant\Applicant;
use Illuminate\Support\Facades\Event;

class JobInviteController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;

    public function store(JobInviteRequest $request)
    {
        $job = Job::find($request->job);
        $caregiver = User::find($request->caregiver);
        $parent = Auth::user();
        $jobDuration = JobDuration::find($request->duration);
        $jobWorkload = JobWorkload::find($request->workload);

        if (!$caregiver->is_caregiver) {
            return $this->responseValidationOld([
                "caregiver" => [
                    "Incorrect caregiver"
                ]
            ]);
        }

        if (!$caregiver->canApplyToJob()) {
            return $this->responseValidationOld([
                "caregiver" => [
                    "Caregiver cannot be invited"
                ]
            ]);
        }

        foreach ($job->jobInvite as $key => $itm) {
            if ($itm->caregiver_id == $caregiver->id) {
                return $this->setResponse(
                    false,
                    route('client.jobs.create.success', $job->id)
                );
            }
        }

        $jobInvite = new JobInvite;
        $jobInvite->message = $request->message;
        $jobInvite->client()->associate($parent);
        $jobInvite->employee()->associate($caregiver);
        $jobInvite->job()->associate($job);
        $jobInvite->jobDuration()->associate($jobDuration);
        $jobInvite->jobWorkload()->associate($jobWorkload);

        $jobInvite->save();

        /***** START Temporary Solution *****/
        $thread = MessagesThread::where([
            'job_id' => $job->id,
            'sender_id' => $parent->id,
            'receiver_id' => $caregiver->id,
            'is_archived' => false
        ])->first();

        if (!$thread) {
            $thread = new MessagesThread;
            $thread->add($job->id, $parent->id, $caregiver->id);
        }

        $message = new Message;
        $message->message_thread_id = $thread->id;
        $message->user_id = $parent->id;
        $message->message = $request->message . " <a href='".route('caregiver.jobs.details', [$job->id])."'>Go to the Proposal</a>";
        $message->save();
        /***** END Temporary Solution *****/

        flash()->success('Invite successfully send!');

        Event::fire(
            new AddNotificationsData(
                User::find($caregiver->id),
                'offer_interview_received',
                ['job_invite_id' => $jobInvite->id, 'type' => 'invite']
            )
        );

        return $this->setResponse(
            false,
            route('client.jobs.my-jobs.details', $job->id)
        );
    }

    public function inviteAll(JobInviteAllRequest $request) {
        $job = Job::find($request->job_id);
        $parent = Auth::user();
        $jobDuration = JobDuration::find($job->job_duration_id);
        $jobWorkload = JobWorkload::find($job->job_workload_id);

        foreach ($request->employee as $item) {
            $caregiver = User::find($item);
            $next = false;

            if ($caregiver) {
                if (!$caregiver->is_caregiver) {
                    $next = true;
                }

                foreach ($job->jobInvite as $key => $itm) {
                    if ($itm->caregiver_id == $caregiver->id) {
                        $next = true;
                    }
                }

                if (!$next) {
                    $jobInvite = new JobInvite;
                    $jobInvite->message = "Hello!\n\nI'd like invite you to apply my job. Please review the job post and apply if you are available.";
                    $jobInvite->client()->associate($parent);
                    $jobInvite->employee()->associate($caregiver);
                    $jobInvite->job()->associate($job);
                    $jobInvite->jobDuration()->associate($jobDuration);
                    $jobInvite->jobWorkload()->associate($jobWorkload);

                    $jobInvite->save();

                    /***** START Temporary Solution *****/
                    $thread = MessagesThread::where([
                        'job_id' => $job->id,
                        'sender_id' => $parent->id,
                        'receiver_id' => $caregiver->id,
                        'is_archived' => false
                    ])->first();

                    if (!$thread) {
                        $thread = new MessagesThread;
                        $thread->add($job->id, $parent->id, $caregiver->id);
                    }

                    $message = new Message;
                    $message->message_thread_id = $thread->id;
                    $message->user_id = $parent->id;
                    $message->message = $jobInvite->message . " <a href='" . route('caregiver.jobs.details', [$job->id]) . "'>Go to the Proposal</a>";
                    $message->save();
                    /***** END Temporary Solution *****/

                    // Event::fire(
                    //     new AddNotificationsData(
                    //         User::find($caregiver->id),
                    //         'offer_interview_received',
                    //         ['job_invite_id' => $jobInvite->id, 'type' => 'invite']
                    //     )
                    // );
                }
            }
        }

        return $this->setResponse();
    }

    public function accept(JobInviteAcceptRequest $request)
    {
        $jobInvite = JobInvite::find($request->job_invite);

        if ($jobInvite->status != 'sended') {
            return $this->setResponse();
        }

        if ((int)$request->rate < 1) {
            return $this->responseValidationOld(['rate' => ['The rate must be at least 1.']]);
        }

        $job = Job::find($jobInvite->job_id);

        if ($job->questions) {
            if (empty($request->questions)) {
                return $this->responseValidationOld(['questions' => ['The questions answer is invalid.']]);
            }

            $request->message .= "<br><br><b>Answers for questions:</b><br><br>";

            foreach ($request->questions as $q) {
                if (empty(trim($q['answer']))) {
                    return $this->responseValidationOld(['questions' => ['The questions answer is invalid.']]);
                }

                $request->message .= "<b>{$q['question']}:</b><br>{$q['answer']}<br>";
            }
        }

        $jobInvite->status = 'accept';
        $jobInvite->save();

        $jobInterview = new JobInterview;
        $jobInterview->job_id = $jobInvite->job_id;
        $jobInterview->employee_id = Auth::user()->id;
        $jobInterview->hourly_rate = $request->rate;
        $jobInterview->save();

        $thread = MessagesThread::where('job_id', $jobInvite->job_id)
            ->where('receiver_id', Auth::user()->id)
            ->where('is_archived', false)
            ->first();

        $message = new Message;
        $message->message_thread_id = $thread->id;
        $message->user_id = Auth::user()->id;
        $message->message = $request->message;
        $message->save();

        if (!Applicant::where(['job_id' => $request->job_id,'user_id' => Auth::user()->id])->count()) {
            Applicant::create([
                'job_id' => $job->id,
                'user_id' => Auth::user()->id,
                'by_invite' => true,
                'participation_method' => 'invitation'
            ]);
        }

        Event::fire(
            new AddNotificationsData(
                User::find($jobInvite->parent_id),
                'interview_accepted',
                ['job_interview_id' => $jobInterview->id]
            )
        );

        return $this->setResponse();
    }

    public function decline(JobInviteDeclineRequest $request)
    {
        $jobInvite = JobInvite::find($request->job_invite);
        
        if ($jobInvite->status != 'sended') {
            return $this->setResponse();
        }

        if ($request->block_client_invite && !JobBlockInvite::where('employee_id', Auth::user()->id)->where('client_id', $jobInvite->parent_id)->count()) {
            $blockInvite = new JobBlockInvite;
            $blockInvite->employee_id = Auth::user()->id;
            $blockInvite->client_id = $jobInvite->parent_id;
            $blockInvite->save();
        }

        $jobInvite->status = 'decline';
        $jobInvite->decline_reason_id = $request->reason;
        $jobInvite->decline_text = $request->message;
        $jobInvite->save();

        Event::fire(
            new AddNotificationsData(
                User::find($jobInvite->parent_id),
                'interview_offer_declined_withdrawn',
                ['invite_id' => $jobInvite->id, 'type' => 'invite']
            )
        );

        return $this->setResponse();
    }
}
