<?php

namespace App\Http\Controllers\Api\V1\Jobs;

use Auth;
use App\Events\AddNotificationsData;
use App\Http\Requests;
use App\Http\Requests\Job\JobSubmitProposalRequest;
use App\Http\Controllers\Controller;
use App\Entities\Job\Job;
use App\Entities\Applicant\Applicant;
use App\Entities\Message\Message;
use App\Entities\Message\MessagesThread;
use Illuminate\Support\Facades\Event;

class JobProposalController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;

    public function store(JobSubmitProposalRequest $request)
    {
        if (Applicant::where(['job_id' => $request->job_id,'user_id' => Auth::user()->id])->count()) {
            return $this->setResponse();
        }

        if ((int)$request->rate < 1) {
            return $this->responseValidationOld(['rate' => ['The rate must be at least 1.']]);
        }

        $job = Job::find($request->job_id);

        if ($job->questions) {
            if (empty($request->questions)) {
                return $this->responseValidationOld(['questions' => ['The questions answer is invalid.']]);
            }

            $request->message .= "<br><br><b>Answers for questions:</b><br><br>";

            foreach ($request->questions as $q) {
                if (empty(trim($q['answer']))) {
                    return $this->responseValidationOld(['questions' => ['The questions answer is invalid.']]);
                }

                $request->message .= "<b>{$q['question']}:</b><br>{$q['answer']}<br>";
            }
        }
    
        Applicant::create([
            'job_id' => $job->id,
            'user_id' => Auth::user()->id,
            'hourly_rate' => $request->rate,
            'participation_method' => 'proposal'
        ]);

        $thread = new MessagesThread;
        $thread->add($job->id, $job->user->id, Auth::user()->id);
        $message = new Message;
        $message->message_thread_id = $thread->id;
        $message->user_id = Auth::user()->id;
        $message->message = $request->message;
        $message->save();

//        Event::fire(
//            new AddNotificationsData(
//                User::find($jobInvite->parent_id),
//                'interview_accepted',
//                ['job_interview_id' => $jobInterview->id]
//            )
//        );

        return $this->setResponse();
    }
}
