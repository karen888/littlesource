<?php

namespace App\Http\Controllers\Api\V1\Jobs;

use DB;
use Auth;
use App\User;
use App\Http\Requests;
use App\Http\Requests\JobRequest;
use App\Http\Requests\Job\JobsByCategoryRequest;
use App\Http\Requests\Job\AdvancedFindJobsRequest;
use App\Http\Requests\Job\FeedbackRequest;
use App\Http\Controllers\Controller;
use App\Events\AddNotificationsData;
use App\Entities\Offer\Offers;
use App\Entities\Job\Job;
use App\Entities\Job\SavedJob;
use App\Entities\Job\JobDuration;
use App\Entities\Job\JobWorkload;
use App\Entities\Job\JobSelectedDay;
use App\Entities\Feedback\FeedbackReason;
use App\Entities\Feedback\Feedback;
use App\Entities\Feedback\RatingOptions;
use App\Http\Models\Question;
use App\Http\Models\MarketplaceVisibility;
use App\Http\Models\MinimumFeedback;
use App\Http\Models\AllowedLocations;
use App\Http\Models\MinimumExperience;
use App\Http\Models\Category;
use Illuminate\Support\Facades\Event;

class JobsController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;
    /**
     * Store a newly created resource in storage.
     * //TODO: this should be refactored to DRY PRINCIPLE
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        $jobDuration = JobDuration::find($request->duration);
        $jobWorkload = JobWorkload::find($request->workload);
        $marketplaceVisibility = MarketplaceVisibility::find($request->marketplace_visibility);
        $minimumFeedback = MinimumFeedback::find($request->minimum_feedback);
        $allowedLocations = AllowedLocations::find($request->allowed_locations);
        $minimumExperience = MinimumExperience::find($request->minimum_experience);

        $job = new Job;
        $job->title = $request->title;
        $job->description = $request->description;
        $job->ocupation = $request->ocupation;
        $job->have_transportation = (int)$request->have_transportation;
        $job->gender = (int)$request->gender;
        $job->smoker = (int)$request->smoker;
        $job->comfortable_with_pets = (int)$request->comfortable_with_pets;
        $job->own_children = (int)$request->own_children;
        $job->sick_children = (int)$request->sick_children;
        $job->child_age_range = json_encode($request->child_age_range);
        $job->age = json_encode($request->age);
        $job->cover_letter = $request->cover_letter;
        $job->user()->associate(Auth::user());
        $job->jobDuration()->associate($jobDuration);
        $job->jobWorkload()->associate($jobWorkload);
        $job->marketplaceVisibility()->associate($marketplaceVisibility);
        $job->minimumFeedback()->associate($minimumFeedback);
        $job->allowedLocations()->associate($allowedLocations);
        $job->minimumExperience()->associate($minimumExperience);

        // Store job in Job Model
        $job->save();

        // handle days selected
        foreach($request->days as $day) {
            $jobSelectedDay = new JobSelectedDay([
                'day' => $day['name'],
                'from' => $day['selectedFrom'],
                'to' => $day['selectedTo'],
                'selected' => $day['selected']
            ]);
            $jobSelectedDay->job()->associate($job);
            $jobSelectedDay->save();
        }

        // Handle Questions (should refactor into DRY principle)
        $questions = [];
        foreach($request->questions as $question) {
            $foundQuestion = Question::where('body', $question['text'])->first();
            if(!$foundQuestion) {
                $foundQuestion = Question::create(['body' => $question['text'] ]);
            }
            $questions[] = $foundQuestion->id;
        }
        
        $job->category()->sync($request->category);
        $job->questions()->sync($questions);
        if (is_array($request->qualifications)){
            $job->qualification()->sync($request->qualifications);
        } else {
            $job->qualification()->sync([$request->qualifications]);
        }

        return $this->setResponse(false, route('client.jobs.create.success', $job->id));
    }

    public function getContract()
    {
        //$offers = Offers::where(['from_user_id' => Auth::user()->id, 'status' => 'accept'])->orderBy('created_at', 'desc')->get();
        //return $offers;

      
        $id = Auth::user()->id;
        $contracts =DB::table('offers')
            ->join('users', 'offers.to_user_id', '=', 'users.id')
            ->select('offers.*','users.first_name','users.last_name','users.user_photo_64')
            ->where('offers.from_user_id',$id)
            ->where('offers.status','accept')
            ->orderBy('offers.created_at', 'desc')
            ->get();

            return  compact($contracts);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        $days = $job->jobSelectedDays;

        $categories = $job->category;
        $categories = $categories->map(function($category) {
            return $category->id;
        })->toArray();

        $qualifications = $job->qualification;
        $qualifications = $qualifications->map(function($qualification) {
            return $qualification->id;
        })->toArray(); 

        $categories_items = Category::customListCategories($categories);

        $days = $days->map(function($item){
            return [
                'name' => $item->day,
                'selectedFrom' => (int)$item->from,
                'selectedTo' => (int)$item->to,
                'selected' => (bool)$item->selected,
            ];
        })->toArray();

        $questions = $job->questions->map(function($item) {
            return [
                'text' => $item->body
            ];
        })->toArray();

        return $this->setResponse([
            'ocupationFormData'  => [
                'selected' => $job->ocupation,
                'days' => $days
            ],
            'experience_level'  => (int)$job->experience_level_id,
            'category'  => $categories,
            'title'  => $job->title,
            'description'  => $job->description,
            'job_duration'  => $job->job_duration_id,
            'job_workload'  => $job->job_workload_id,
            'minimum_feedback'  => $job->minimum_feedback_id,
            'marketplace_visibility'  => $job->marketplace_visibility_id,
            'allowed_locations'  => $job->allowed_locations_id,
            'number_of_hires'  => $job->number_of_hires_id,
            'experience'  => $job->minimum_experience_id,
            'cover_letter'  => (bool)$job->cover_letter,
            'questions' => $questions,
            'qualifications' => $qualifications,
            'categories_items' => $categories_items,
            'have_transportation' => $job->have_transportation,
            'gender' => $job->gender,
            'smoker' => $job->smoker,
            'comfortable_with_pets' => $job->comfortable_with_pets,
            'own_children' => $job->own_children,
            'sick_children' => $job->sick_children,
            'child_age_range' => $job->child_age_range,
            'age' => $job->age,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobRequest $request, Job $job)
    {
        $jobDuration = JobDuration::find($request->duration);
        $jobWorkload = JobWorkload::find($request->workload);
        $marketplaceVisibility = MarketplaceVisibility::find($request->marketplace_visibility);
        $minimumFeedback = MinimumFeedback::find($request->minimum_feedback);
        $allowedLocations = AllowedLocations::find($request->allowed_locations);
        $minimumExperience = MinimumExperience::find($request->minimum_experience);

        $job->title = $request->title;
        $job->description = $request->description;
        $job->ocupation = $request->ocupation;
        $job->cover_letter = $request->cover_letter;
        $job->jobDuration()->associate($jobDuration);
        $job->jobWorkload()->associate($jobWorkload);
        $job->marketplaceVisibility()->associate($marketplaceVisibility);
        $job->minimumFeedback()->associate($minimumFeedback);
        $job->allowedLocations()->associate($allowedLocations);
        $job->minimumExperience()->associate($minimumExperience);
        $job->have_transportation = (int)$request->have_transportation;
        $job->gender = (int)$request->gender;
        $job->smoker = (int)$request->smoker;
        $job->comfortable_with_pets = (int)$request->comfortable_with_pets;
        $job->own_children = (int)$request->own_children;
        $job->sick_children = (int)$request->sick_children;
        $job->child_age_range = json_encode($request->child_age_range);
        $job->age = json_encode($request->age);
        $job->save();

        // handle questions (if any)
        $questions = [];

        foreach($request->questions as $question) {
            $foundQuestion = Question::where('body', $question['text'])->first();
            if(!$foundQuestion) {
                $foundQuestion = Question::create(['body' => $question['text']]);
            }
            $questions[] = $foundQuestion->id;
        }

        // handle days selected
        foreach($request->days as $day) {
            JobSelectedDay::where('job_id', $job->id)
                    ->where('day', $day['name'])->update([
                        'from' => $day['selectedFrom'],
                        'to' => $day['selectedTo'],
                        'selected' => $day['selected']
                    ]);
        }

        $job->category()->sync($request->category);
        $job->questions()->sync($questions);

        if (is_array($request->qualifications)){
            $job->qualification()->sync($request->qualifications);
        } else {
            $job->qualification()->sync([$request->qualifications]);
        }

        flash()->success('Job successfully edited!');

        $jobInterview = $job->jobInterview()->where('status', 'accept')->get();
        
        if (!empty($jobInterview)) {
            foreach ($jobInterview as $item) {
                Event::fire(
                    new AddNotificationsData(
                        User::find($item->employee->id),
                        'job_applied_modified_canceled',
                        ['job_id' => $job->id, 'type' => 'modified']
                    )
                );
            }
        }

        return $this->setResponse(false, route('client.jobs.my-jobs'));
    }

    public function close(Job $job)
    {
        if ($job->closed == "Active") {
            $jobInterview = $job->jobInterview()->where('status', 'accept')->get();

            if (!empty($jobInterview)) {
                foreach ($jobInterview as $item) {
                    Event::fire(
                        new AddNotificationsData(
                            User::find($item->employee->id),
                            'job_applied_modified_canceled',
                            ['job_id' => $job->id, 'type' => 'cancel']
                        )
                    );
                }
            }

            $job->closed = true;
            $job->save();
        }

        return $this->setResponse();
    }

    public function repostJob(Job $job)
    {
        DB::table('jobs')->where('id', $job->id)->update(['closed' => 0]);
    }

    public function removeJob(Job $job)
    {
            $user= Auth::user()->id;
            DB::table('saved_jobs')->where('job_id', $job->id)->where('caregiver_id',$user)->delete();
    }

    public function makePublic(Job $job)
    {
        if ($job->marketplace_visibility_id != 1) {
            $job->marketplace_visibility_id = 1;
            $job->save();
        }

        return $this->setResponse();
    }
    
    public function getFindJobs()
    {
        $qualifications = Auth::user()->qualification->map(function($item) {
            return ((int)$item->id - 26); // temporary solution
        })->toArray();

        $jobs_id = [];
        $jobs = [];

        $get = DB::table('job_qualification')->limit(10)->whereIn('qualification_id', $qualifications)->orderBy('id', 'desc')->get();

        foreach ($get as $job) {
            $jobs_id[] = $job->job_id;
        }

        $get = Job::whereIn('id', $jobs_id)->orderBy('id', 'desc')->get();

        foreach ($get as $key=>$item) {
            // print_r($item->minimum_feedback_id); 
            // exit;
            $categories = $item->category;
            $categories = $categories->map(function($category) {
                return $category->name;
            })->toArray();

            $jobs[$key]['title'] = $item->title;
            $jobs[$key]['description'] = $item->description;
            $jobs[$key]['categories'] = $categories;
            $jobs[$key]['user_name'] = $item->user->name;
            $jobs[$key]['id'] = $item->id;
            $jobs[$key]['created_at'] = $item->created_at->diffForHumans();
            $jobs[$key]['ratings'] = $item->user->getUserFeedback($item->user->id);
        }

        return $this->setResponse([
            'jobs'  => $jobs
        ]);
    }

    public function getJobsByCategory(JobsByCategoryRequest $request)
    {
        if(is_array($request->id)){
            $category_job = DB::table('category_job')->limit(10)->whereIn('category_id', $request->id)->orderBy('id', 'desc')->get();
        }else{
            $category_job = DB::table('category_job')->limit(10)->where('category_id', $request->id)->orderBy('id', 'desc')->get();
        }

        $jobs_id = [];
        $jobs = [];

        foreach ($category_job as $category) {
            $jobs_id[] = $category->job_id;
        }

        $get = Job::whereIn('id', $jobs_id)->orderBy('id', 'desc')->get();

        foreach ($get as $key=>$item) {
            $categories = $item->category;
            $categories = $categories->map(function($category) {
                return $category->name;
            })->toArray();

            $jobs[$key]['title'] = $item->title;
            $jobs[$key]['description'] = $item->description;
            $jobs[$key]['categories'] = $categories;
            $jobs[$key]['user_name'] = $item->user->name;
            $jobs[$key]['id'] = $item->id;
            $jobs[$key]['created_at'] = $item->created_at->diffForHumans();
            $jobs[$key]['ratings'] = $item->user->getUserFeedback($item->user->id);
        }

        return $this->setResponse([
            'jobs'  => $jobs
        ]);
    }

    public function getPost()
    {

    }

    public function getAdvancedFindJobs(AdvancedFindJobsRequest $request)
    {
        $data = $request->all();
        $job_ids = [];
        $select = [];
        $jobsList = Job::orderBy('id', 'desc');

        
        
        foreach ($data as $key=>$item) {
            switch($key) {
                case 'all_words':
                    // $jobs = Job::select('id');
                 
                    $jobs = $jobsList->where(function ($query) use ($item) {
                            $query->where('title', 'like', "%$item%")
                            ->orWhere('description', 'like', "%$item%");
                    })->where('closed','!=',1)->get();
              
                    break;
                case 'any_words':
                    // $jobs = Job::select('id');
                    $any_words = explode(' ', $item);

                    foreach ($any_words as $akey=>$aitem) {
                        if (empty($aitem)) {
                            unset($any_words[$aitem]);
                        }
                    }

                    $any_words = implode('|', $any_words);

                    $jobs =  $jobsList->where(function ($query) use ($any_words) {
                        $query->where('title', 'REGEXP', $any_words)
                            ->orWhere('description', 'REGEXP', $any_words);
                    })->get();
                    break;
                case 'title':
                    // $jobs = Job::select('id');
                    $jobs = $jobsList->where('title', 'like', "%$item%")->get();
                    break;
                case 'select':
                    if ($item) {
                        $qualifications_user = DB::table('qualification_user')->where('user_id', Auth::user()->id)->get();

                        foreach ($qualifications_user as $q) {
                            $select['qualification'][] = $q->qualification_id;
                        }

                        $select['employee_info'] = Auth::user()->employee_info->get()->toArray();
                    }
                    break;
            }
            // print_r($jobs);
            if (count(@$jobs)) {
                foreach ($jobs as $job) {
                    $job_ids[$job->id] = $job->id;
                }
            }

        }

        $advanced_ids = [];
        $search_ids = [];

        foreach ($data as $key=>$item) {
            switch($key) {
                case 'job_duration':
                    $get = $jobsList->whereIn('job_duration_id', $item);
                    break;
                case 'job_workloads':
                    $get = $jobsList->whereIn('job_workload_id', $item);
                    break;
                case 'location_range':
                    $get = $jobsList->whereIn('allowed_locations_id', $item);
                    break;
                case 'sick_children':
                    $get = $jobsList->whereIn('sick_children', $item);
                    break;
                case 'pets':
                    $get = $jobsList->whereIn('comfortable_with_pets', $item);
                    break;
                case 'gender':
                    $get = $jobsList->whereIn('gender', $item);
                    break;
                case 'own_children':
                    $get = $jobsList->whereIn('own_children', $item);
                    break;
                case 'drivers_licence':
                    $get = $jobsList->whereIn('have_transportation', $item);
                    break;
                case 'smoker':
                    $get = $jobsList->whereIn('smoker', $item);
                    break;
                case 'age_range':
                    $age = implode('|', $item);
                    $get = $jobsList->where('age', 'REGEXP', $age);
                    break;
                case 'child_age':
                    $child_age = implode('|', $item);
                    $get = $jobsList->where('child_age_range', 'REGEXP', $child_age);
                    break;
                case 'category':
                    foreach (DB::table('category_job')->whereIn('category_id', $item)->select('job_id')->get() as $category) {
                        $advanced_ids[] = $category->job_id;
                    }
                    break;
                case 'qualification':
                    foreach (DB::table('job_qualification')->whereIn('qualification_id', $item)->select('job_id')->get() as $qualification) {
                        $advanced_ids[] = $qualification->job_id;
                    }
                    break;
            }
        }

        if (!empty($data['category']) || !empty($data['qualification'])) {
            foreach ($job_ids as $job) {
                foreach ($advanced_ids as $advanced) {
                    if ($job == $advanced) {
                        $search_ids[] = $advanced;
                    }
                }
            }
        } else {
            // $search_ids = $job_ids;
            $search_ids = array_merge($search_ids, $job_ids);
        }
        //maza
        $search_ids = array_merge($search_ids, $advanced_ids);

        $search_ids = array_unique($search_ids);
        // print_r($jobsList);

        // $findList = $jobsList->whereIn('id', [8,10,11,13,14,16,20,23,28,33,35,38])->paginate(10);
        $findList = Job::whereIn('id', $search_ids)->orderBy('id', 'desc')->paginate(10);

        // print_r($findList);
        $jobs = []; 

        foreach ($findList as $key=>$item) {
            $categories = $item->category;
            $categories = $categories->map(function($category) {
                return $category->name;
            })->toArray();

            
            $jobs[$key]['title'] = $item->title;
            $jobs[$key]['description'] = $item->description;
            $jobs[$key]['categories'] = $categories;
            $jobs[$key]['user_name'] = $item->user->name;
            $jobs[$key]['id'] = $item->id;
            $jobs[$key]['is_saved'] = $item->isSaved();
        }

        return $this->setResponse([
            'jobs'  => $jobs,
            'render' => $findList->render(),
            'count' => $findList->count(),
            'current_page' => $findList->currentPage(),
            'prev_page' => (bool)$findList->previousPageUrl(),
            'next_page' => (bool)$findList->nextPageUrl(),
            'search_ids' => $search_ids,
            'select' => $select
        ]);
    }

    public function getJobQuestions($job_id)
    {
        $job = Job::find($job_id);
        $questions = [];

        if ($job) {
            $questions = $job->questions->map(function($item) {
                return $item->body;
            })->toArray();
        }

        return $this->setResponse($questions);
    }

    public function saveJob($job_id)
    {
        $savedJob = new SavedJob();
        $savedJob->saveJob($job_id);
        return $this->setResponse();
    }

    /**
     * Post Feedback
     *
     * @return \Illuminate\Http\Response
     */
    public function postFeedback(FeedbackRequest $request)
    {
        $offer = Offers::find($request->contract);
        $rating_types = [];

        $rating_options = RatingOptions::get()->map(function($item) {
            return $item->id;
        })->toArray();

        foreach ($request->rating_types as $itm) {
            if (!in_array($itm['id'], $rating_options)) {
                return $this->responseValidationOld(['rating_types' => ['The rating is invalid']]);
            }
            $rating_types[$itm['id']] = ['data' => $itm['value']];
        }

        if (FeedbackReason::where('reason', 'Other')->first()->id == $request->reason) {
            if (empty($request->reason_text)) {
                return $this->responseValidationOld(['reason_text' => ['The reason text is required']]);
            }
        }

        $feedback = Feedback::where([
            'job_id' => $offer->job_id,
            'client_id' => Auth::user()->is_caregiver ? $offer->from_user_id : Auth::user()->id,
            'employee_id' => !Auth::user()->is_caregiver ? $offer->from_user_id : Auth::user()->id,
            'by_employee' => Auth::user()->is_caregiver
        ])->first();

        if (!count($feedback)) {
            $feedback = new Feedback;
        }

        $feedback->job_id = $offer->job_id;
        $feedback->client_id = Auth::user()->is_caregiver ? $offer->from_user_id : Auth::user()->id;
        $feedback->employee_id = !Auth::user()->is_caregiver ? $offer->from_user_id : Auth::user()->id;
        $feedback->comment = $request->comment;
        $feedback->feedback_rate = $offer->offer_paid;
        $feedback->feedback_reason = $request->reason;
        $feedback->note = $request->note;
        $feedback->by_employee = Auth::user()->is_caregiver;
        $feedback->save();

        $feedback->rating()->sync($rating_types);
        $feedback->job->closeJob();

        $offer->is_active = false;
        $offer->save();

        return $this->setResponse([
            'url' => route('jobs.contracts.details', $offer->id)
        ]);
    }

    public function getContractFeedback($contract) {
        $offer = Offers::findOrFail((int)$contract);

        $feedback = Feedback::where([
            'job_id' => $offer->job_id,
            'client_id' => Auth::user()->is_caregiver ? $offer->from_user_id : Auth::user()->id,
            'employee_id' => !Auth::user()->is_caregiver ? $offer->from_user_id : Auth::user()->id,
            'by_employee' => Auth::user()->is_caregiver
        ])->first();

        if (!count($feedback)) {
            return $this->setResponse([
                'rating' => false
            ]);
        }

        return $this->setResponse([
            'rating' => $feedback->getRating(),
            'comment' => $feedback->comment,
            'by_employee' => $feedback->by_employee,
            'note' => $feedback->note,
            'feedback_reason' => $feedback->feedback_reason,
            'ratings' => $feedback->rating
        ]);
    }
}
