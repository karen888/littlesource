<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Entities\Criteria\OwnChildren;
use App\Entities\Criteria\Smoker;
use Carbon\Carbon;
use \Image;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Employee\EmployeeInfo;
use App\Entities\Educations\Educations;
use App\Entities\Educations\EducationUser;
use App\Entities\Languages\Languages;
use App\Entities\Criteria\Base as Criteria;
use App\Http\Requests\Employee\Profile\AvailabilityRequest;
use App\Http\Requests\Employee\Profile\EducationRequest;
use App\Http\Requests\Employee\Profile\EducationRequestVerify;
use App\Http\Requests\Employee\Profile\HourlyRateRequest;
use App\Http\Requests\Employee\Profile\NameTitleRequest;
use App\Http\Requests\Employee\Profile\OverviewRequest;
use App\Http\Requests\Employee\Profile\QualificationsRequest;
use App\Http\Requests\Employee\Profile\SkillsRequest;
use App\Http\Requests\Employee\Profile\ExperienceRequest;
use App\Http\Requests\Employee\Profile\CertificatesRequest;
use App\Http\Requests\Employee\Find\FindCaregiversRequest;
use App\Http\Models\Skill;
use App\Http\Models\UserInfo;
use App\User;
use App\Certificates;
use App\CertificatesUser;
use App\UserHistoryData;
use DB;

class EmployeeController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;
    use \App\Http\Traits\GeolocationTrait;


    public function getEmployeeInfo($id = null)
    {
        $hidden_buttons = true;
        $favorite = false;

        if (empty((int)$id)) {
            $user = Auth::user();
        } else {
            $user = User::find((int)$id);
            $hidden_buttons = (Auth::user()->id === $user->id) ? true : false;
            $favorite = (bool) Auth::user()->favorites->where('favorite_id', $user->id)->first();
        }

        if($user->employee_info->short_name == 0){
            $user->last_name_view = $user->last_name[0].".";
        }else{
            $user->last_name_view = $user->last_name;
        }

        $qua_status  = 0;
        foreach($user->educations as $qua){
            if($qua->status == 1){
                $qua_status = 1;
                break;
            }else{
                $qua_status = $qua->status;
            }
        }

        $cert_status = 0;
        foreach($user->certificates as $cert){
            if($cert->status == 1){
                $cert_status = 1;
                break;
            }else{
                $cert_status =  $cert->status;
            }
        }

        return $this->setResponse([
            'user_data' => [
                'hidden_buttons' => $hidden_buttons,
                'favorite' => $favorite,
                'name' => $user->name,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'last_name_view' => $user->last_name_view,
                'photo' => $user->photo_url,
                'route_hire' => route('client.employee.hire', $user->id),
                'contact_now' => route('messages.index', $user->id),
                'have_transportation' => ($user->driversLicence) ? $user->driversLicence->id : false,
                'care_type' => $user->careType->map(function($item) {
                    return $item->id;
                })->toArray(),
                'child_age_range' => $user->childAge->map(function($item) {
                    return $item->id;
                })->toArray(),
                'comfortable_with_pets' => ($user->pets) ? $user->pets->id : false,
                'sick_children' => ($user->careSick) ? $user->careSick->id : false,
                'smoker' => ($user->smoker) ? $user->smoker->id : false,
                'own_children' => ($user->own_children) ? $user->own_children->id : false,
                'willing_travel' => $user->locationRange->map(function($item) {
                    return $item->id;
                })->toArray(),
                'experience' => ($user->yearsExperience) ? $user->yearsExperience->id : false,
                'employment_type' => $user->employmentType->map(function($item) {
                    return $item->id;
                })->toArray(),
                'qualification' => $user->qualification->map(function($item) {
                    return $item->id;
                })->toArray(),
                'gender' => ($user->gender) ? $user->gender->id : false,
                'availability_days' => $user->availability->map(function($item) {
                    return $item->id;
                })->toArray()
            ],
            'employee_info' => $user->employee_info,
            'skills' => $user->skills,
            'certificates' => $user->certificates,
            'educations' => $user->educations,
            'languages' => $user->languages,
            'job_history' => $user->getEmployeeJobHistory(),
            'employee_rating' => $user->getEmployeeFeedback(),
            'verifed_id' => $user->verifed_id,
            'wwcc_status' => $user->wwcc_status,
            'address_status' => $user->address_status,
            'phone_verifed' => $user->phone_verifed,
            'abn_verifed' => $user->abn_verifed,
            'social_verifed' => $user->social_verifed,
            'qualifications_verifed' => $qua_status,
            'certifications_verifed' => $cert_status,
        ]);
    }

    public function saveAvailability(AvailabilityRequest $request)
    {
        $employeeInfo = EmployeeInfo::where('user_id', Auth::user()->id)->first();

        if ($request->available_value) {
            if ((int)$request->available_selected) {
                if (empty($request->availability_days)) {
                    return $this->responseValidationOld(['availability_days' => ['The availability days is required.']]);
                }
                $save = [];
                $criteria = Criteria::where('type', 'App\Entities\Criteria\EmploymentType')
                    ->orWhere('type', 'App\Entities\Criteria\Availability')->get()->map(function($item) {
                    return $item->id;
                })->toArray();
                $criteria_user = Auth::user()->user_criteria->map(function($item) {
                    return $item->id;
                })->toArray();

                foreach ($criteria_user as $item) {
                    if (!in_array($item, $criteria)) {
                        array_push($save, $item);
                    }
                }

                array_push($save, $request->available_selected);
                foreach ($request->availability_days as $days) {
                    array_push($save, $days);
                }

                Auth::user()->user_criteria()->sync($save);
            } else {
                return $this->responseValidationOld(['available' => ['The availability is invalid. Please set availability type']]);
            }
        }
        
        $employeeInfo->available = $request->available_value;
        $employeeInfo->expect_ready = $request->expect_ready;

        $employeeInfo->save();

        return $this->setResponse();
    }


    /**
     * @param EducationRequestVerify $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function saveEducationVerify(EducationRequestVerify $request){
        $educations_ids = [];
        $errors = [];
        if (empty($request->school)) {
            $errors['school'] = 'The school field is required.';
        }
        if (empty($request->degree)) {
            $errors['degree'] = 'The degree field is required.';
        }

        if (!empty($errors)) {
            return $this->responseValidationOld($errors);
        }



        $education['school'] = trim($request->school);
        $education['degree'] = trim($request->degree);
        $education['scan'] = trim($request->scan);
        $education['area_study'] = trim($request->area_study);
        $education['dates_attended_from'] = $request->dates_attended_from;
        $education['dates_attended_to'] = $request->dates_attended_to;
        $education['description'] = $request->description;
        $education['notes'] = $request->notes;

        $foundEducation = Educations::where('id', '=', $request->id)->first();
        
        if(!$foundEducation) {
            $foundEducation = Educations::create($education);
            $ed_user = new EducationUser();
            $ed_user->educations_id = $foundEducation->id;
            $ed_user->user_id = Auth::user()->id;
            $ed_user->save();
        }else{
            $historyData = array(
                'id' => $foundEducation->id,
                'school' => $foundEducation->school,
                'degree' => $foundEducation->degree,
                'scan' => $foundEducation->scan,
                'area_study' => $foundEducation->area_study,
                'dates_attended_from' => $foundEducation->dates_attended_from,
                'dates_attended_to' => $foundEducation->dates_attended_to,
                'description' => $foundEducation->description,
                'notes' => $foundEducation->notes,
                'updated_at' => $foundEducation->updated_at,
                'created_at' => $foundEducation->created_at,
                'status' => $foundEducation->status,
            );
            $histoty = new UserHistoryData();
            $histoty->type = 'qualifications';
            $histoty->user_id = Auth::user()->id;
            $histoty->data = json_encode($historyData);
            $histoty->save();

            $foundEducation->school = $education['school'];
            $foundEducation->degree = $education['degree'];
            $foundEducation->scan = $education['scan'];
            $foundEducation->area_study = $education['area_study'];
            $foundEducation->dates_attended_from = $education['dates_attended_from'];
            $foundEducation->dates_attended_to = $education['dates_attended_to'];
            $foundEducation->description = $education['description'];
            $foundEducation->notes = $education['notes'];
            $foundEducation->status = 0;
            $foundEducation->save();
        }

        return $this->setResponse();
    }

    public function saveEducation(EducationRequest $request)
    {
        $educations = [];
        $educations_ids = [];
        $languages = [];
        $languages_ids = [];
        $errors = [];

        foreach ($request->educations as $key=>$item) {
            $educations[$key]['school'] = trim($item['school']['text']);
            $educations[$key]['degree'] = trim($item['degree']['text']);
            $educations[$key]['area_study'] = trim($item['area_study']['text']);
            $educations[$key]['dates_attended_from'] = $item['dates_attended_from']['selected'];
            $educations[$key]['dates_attended_to'] = $item['dates_attended_to']['selected'];

            if (empty($educations[$key]['school'])) {
                $errors['educations'][$key]['school'] = 'The school field is required.';
            }

            if (empty($educations[$key]['degree'])) {
                $errors['educations'][$key]['degree'] = 'The degree field is required.';
            }
        }

        foreach ($request->languages_ as $key=>$item) {
            $languages[$key]['name'] = trim($item['name']);

            if (empty($languages[$key]['name'])) {
                $errors['languages'][$key]['name'] = 'The field is empty.';
            }
        }

        if (!empty($errors)) {
            return $this->responseValidationOld($errors);
        }

        foreach ($educations as $item) {
            $foundEducation = Educations::where($item)->first();

            if(!$foundEducation) {
                $foundEducation = Educations::create($item);
            }

            $educations_ids[] = $foundEducation->id;
        }

        foreach ($languages as $item) {
            $foundLanguages = Languages::where($item)->first();

            if(!$foundLanguages) {
                $foundLanguages = Languages::create($item);
            }

            $languages_ids[] = $foundLanguages->id;
        }

        Auth::user()->educations()->sync($educations_ids);
        Auth::user()->languages()->sync($languages_ids);

        return $this->setResponse();
    }

    public function saveHourlyRate(HourlyRateRequest $request)
    {
        if (!empty($request->rate)) {
            if (!preg_match("/^[0-9]{1,3}.[0-9][0-9]$/", $request->rate)) {
                return $this->responseValidationOld(['rate' => ['The rate is invalid.']]);
            }
        }

        $employeeInfo = EmployeeInfo::where('user_id', Auth::user()->id)->first();
        $employeeInfo->rate = $request->rate;
        $employeeInfo->save();

        return $this->setResponse();
    }

    public function saveNameTitle(NameTitleRequest $request)
    {
        $employeeInfo = EmployeeInfo::where('user_id', Auth::user()->id)->first();
        $employeeInfo->title = $request->title;
        $employeeInfo->short_name = $request->short_name;
        $employeeInfo->birthday = $request->birthday;
        $employeeInfo->save();

        Auth::user()->first_name = $request->first_name;
        Auth::user()->last_name = $request->last_name;
        Auth::user()->save();

        $save = [];
        $criteria = Criteria::where('type', 'App\Entities\Criteria\Gender')->get()->map(function($item) {
            return $item->id;
        })->toArray();
        $criteria_user = Auth::user()->user_criteria->map(function($item) {
            return $item->id;
        })->toArray();

        foreach ($criteria_user as $item) {
            if (!in_array($item, $criteria)) {
                array_push($save, $item);
            }
        }

        array_push($save, $request->gender);

        Auth::user()->user_criteria()->sync($save);

        return $this->setResponse("Response Success", true);
    }

    public function saveExperience(ExperienceRequest $request)
    {
        $save = [];
        $criteria = Criteria::where('type', 'App\Entities\Criteria\YearsExperience')->get()->map(function($item) {
            return $item->id;
        })->toArray();
        $criteria_user = Auth::user()->user_criteria->map(function($item) {
            return $item->id;
        })->toArray();

        foreach ($criteria_user as $item) {
            if (!in_array($item, $criteria)) {
                array_push($save, $item);
            }
        }

        if ($request->experience) {
            array_push($save, $request->experience);
        }

        Auth::user()->user_criteria()->sync($save);

        return $this->setResponse();
    }

    public function saveOverview(OverviewRequest $request)
    {
        $employeeInfo = EmployeeInfo::where('user_id', Auth::user()->id)->first();
        $employeeInfo->overview = $request->overview;
        $employeeInfo->save();

        return $this->setResponse();
    }

    public function saveCertificates(CertificatesRequest $request)
    {

//        $certificatId = DB::table('certificates_user')->select('certificates_id')->where("user_id", '=', Auth::user()->id)->first();
        $certificates = Certificates::where('id', $request->id  )->first();
        if(isset($certificates->name)){
            $historyData = array(
                'id' => $certificates->id,
                'name' => $certificates->name,
                'provider' => $certificates->provider,
                'description' => $certificates->description,
                'notes' => $certificates->notes,
                'date_earned' => $certificates->date_earned,
                'submission' => $certificates->submission,
                'updated_at' => $certificates->updated_at,
                'created_at' => $certificates->created_at,
                'status' => $certificates->status,
                'scan' => $certificates->scan
            );

            $histoty = new UserHistoryData();
            $histoty->type = 'certificates';
            $histoty->user_id = Auth::user()->id;
            $histoty->data = json_encode($historyData);
            $histoty->save();

            $certificates->name = $request->name;
                $certificates->provider = $request->provider;
                $certificates->description = $request->description;
                $certificates->notes = $request->notes;
                $certificates->date_earned = $request->date_earned;
                $certificates->submission = $request->submission;
                $certificates->scan = $request->scan;

                $certificates->status = 0;
                $certificates->save();
        }else{
            $certificates = new Certificates();
            $certificates->name = $request->name;
            $certificates->provider = $request->provider;
            $certificates->description = $request->description;
            $certificates->notes = $request->notes;
            $certificates->date_earned = $request->date_earned;
            $certificates->submission = $request->submission;
            $certificates->scan = $request->scan;
            $certificates->save();

            $cert_user = new CertificatesUser();
            $cert_user->certificates_id = $certificates->id;
            $cert_user->user_id = Auth::user()->id;
            $cert_user->save();
        }
        return $this->setResponse();
    }

    public function saveQualifications(QualificationsRequest $request)
    {
        $data = $request->all();
        $save = [];
        
        $criteria = Criteria::where('type', 'App\Entities\Criteria\YearsExperience')
        ->orWhere('type', 'App\Entities\Criteria\Gender')
        ->orWhere('type', Smoker::class)
        ->orWhere('type', OwnChildren::class)
        ->orWhere('type', 'App\Entities\Criteria\EmploymentType')
        ->orWhere('type', 'App\Entities\Criteria\Availability')->get()->map(function($item) {
            return $item->id;
        })->toArray();
        $criteria_user = Auth::user()->user_criteria->map(function($item) {
            return $item->id;
        })->toArray();

        foreach ($criteria_user as $item) {
            if (in_array($item, $criteria)) {
                array_push($save, $item);
            }
        }

        foreach ($data as $key=>$item) {
            if (is_array($item)) {
                foreach ($item as $itm) {
                    array_push($save, $itm);
                }

            } else {
                array_push($save, $item);
            }
        }
        Auth::user()->user_criteria()->sync($save);

        return $this->setResponse();
    }

    public function saveSkills(SkillsRequest $request)
    {
        $skills = [];
        $request_skills = explode(',', $request->skills);

        foreach ($request_skills as $key => $skill)
        {

            $foundSkill = Skill::where('skill_name', $skill)->first();

            if(!$foundSkill) {
                $foundSkill = Skill::create(['skill_name' => $skill]);
            }

            $skills[] = $foundSkill->id;
        }

        Auth::user()->skills()->sync($skills);

        return $this->setResponse();
    }

    /**
     * @param FindCaregiversRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findCaregiversPost(FindCaregiversRequest $request)
    {
        if (!empty($request->postcode) && $request->postcode != 0) {
            $users = UserInfo::where('zipcode', '=', $request->postcode)
                ->orWhere('loc_zip', '=', $request->postcode)->select('user_id')->get()->map(function($item) {
                    return $item->user_id;
            })->toArray();
        } else {

            $users = $this->getCaregiversByRange($request->location_range);
        }


        if (empty($users)) {
            return $this->setResponse([
                'users'  => $users,
                'render' => false,
                'count' => false,
                'current_page' => false,
                'prev_page' => false,
                'next_page' => false,
                'no_users' => true,
            ]);
        }

        $get = User::where('is_caregiver', '=', 1)->whereIn('id', $users)->paginate(1000000);
        $users = [];
        $postcode = [];

        foreach ($get as $key=>$item) {




            if(
                $item->canApplyToJob() &&
                $item->employee_info->available === 1
            ) {
                $criteria = $request->except(['location_range', 'postcode', 'lat', 'lng']);
                $skipUser = false;



                foreach($criteria as $k=>$criterion) {

                    if(is_array($criterion)) {
                        if($k === 'age_range') {
                            // 19: 18-25
                            // 20: 26-30
                            // 21: 31-40
                            // 22: 40+
                            $eInfo = EmployeeInfo::whereNotNull('birthday')->where('user_id', '=', $item->id);

                            foreach($criterion as $kk=>$c) {
                                $mindate = null;
                                $maxdate = null;

                                switch($c) {
                                    case 19:
                                        $mindate = Carbon::now()->subYears(25)->format('Y-m-d');
                                        $maxdate = Carbon::now()->subYears(18)->format('Y-m-d');
                                        break;
                                    case 20:
                                        $mindate = Carbon::now()->subYears(30)->format('Y-m-d');
                                        $maxdate = Carbon::now()->subYears(26)->format('Y-m-d');
                                        break;
                                    case 21:
                                        $mindate = Carbon::now()->subYears(40)->format('Y-m-d');
                                        $maxdate = Carbon::now()->subYears(31)->format('Y-m-d');
                                        break;
                                    case 22:
                                        $mindate = Carbon::now()->subYears(150)->format('Y-m-d');
                                        $maxdate = Carbon::now()->subYears(41)->format('Y-m-d');
                                        break;
                                }

                                if($maxdate == null) continue;

                                if($kk === 0) {
                                    $eInfo = $eInfo->where(function($query) use( $mindate, $maxdate) {
                                        return $query->where('birthday', '>=', $mindate)->where('birthday', '<', $maxdate);
                                    });
                                } else {
                                    $eInfo = $eInfo->orWhere(function ($query) use ($mindate, $maxdate) {
                                        return $query->where('birthday', '>=', $mindate)->where('birthday', '<', $maxdate);
                                    });
                                }
                            }

                            $eInfo = $eInfo->get();

                            if(count($eInfo) === 0) {
                                $skipUser = true;
                                if($item->id == 135) {
                                    dd(__LINE__);
                                }
                                break;
                            }
                        } else {
                            if (DB::table('criterias_users')
                                    ->whereIn('criteria_id', $criterion)
                                    ->where('user_id', '=', $item->id)
                                    ->count() === 0
                            ) {
                                $skipUser = true;
                                if($item->id == 135) {
                                    dd(__LINE__);
                                }
                                break;
                            }
                        }
                    } else {
                        $qq = DB::table('criterias_users')
                            ->where('user_id', '=', $item->id)
                            ->where('criteria_id', '=', (int) $criterion);

                        $qq = $qq->first();


                        if(is_null($qq)) {
                            $skipUser = true;
                            break;
                        }
                        //continue;
                    }
                }


                if($skipUser === true) {
                    continue;
                }

                $users[$item->id]['name'] = $item->name;
                $users[$item->id]['photo'] = $item->PhotoUrl;
                $users[$item->id]['rate'] = ($item->employee_info) ? $item->employee_info->view_rate : false;
                $users[$item->id]['view'] = route('employee.profile', $item->id);
                $users[$item->id]['invite'] = route('client.employee.invite', $item->id);
                $users[$item->id]['lat'] = $item->info->lat;
                $users[$item->id]['lng'] = $item->info->lng;
                $users[$item->id]['can_apply_to_jobs'] = $item->canApplyToJob();
                $users[$item->id]['verified'] = $item->isUserFullyVerified();

                if (!empty(@$postcode[$item->info->zipcode])) {
                    array_push($postcode[$item->info->zipcode], $item->id);
                } else {
                    @$postcode[$item->info->zipcode] = [];
                    array_push($postcode[$item->info->zipcode], $item->id);
                }
            }
        }

        return $this->setResponse([
            'users'  => $users,
            'render' => $get->render(),
            'count' => $get->count(),
            'current_page' => $get->currentPage(),
            'prev_page' => (bool)$get->previousPageUrl(),
            'next_page' => (bool)$get->nextPageUrl(),
            'postcode' => $postcode,
            'haveUser' => true,
        ]);
    }
}