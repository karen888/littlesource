<?php

namespace App\Http\Controllers\Api\v1\User;

use App\Services\ScrappingService;
use App\Http\Models\State;
use App\Http\Models\Wwcc;
use App\User;
use App\Entities\SecurityQuestion\SecurityQuestionTypes;
use App\Http\Models\Reference;
use App\Http\Models\VerificationChats;
use App\SmsCodes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use \Image;
use Auth;
use Hash;
use App\Http\Traits\MailTraits;
use App\Http\Traits\ResponsesTrait;
use App\Entities\Country\Country;
use App\Entities\Educations\Educations;
use App\Entities\Educations\EducationUser;
use App\Entities\Notifications\Notifications;
use App\Entities\SecurityQuestion\SecurityQuestions;
use App\Entities\Employee\EmployeeInfo;
use App\Http\Models\Timezone;
use App\Http\Requests\AddressRequest;
use App\Http\Requests\EmailAddressRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfilePictureRequest;
use App\Http\Requests\TimezoneRequest;

use App\Http\Requests\PhoneNumberRequest;
use App\Http\Requests\WwccRequest;
use App\Http\Requests\saveVeridRequest;
use App\Http\Requests\saveAdressRequest;
use App\Http\Requests\saveSoclinkRequest;
use App\Http\Requests\deleteVerifedRequest;

use App\Http\Requests\ABNNumberRequest;
use App\Http\Requests\NotificationsSettingsRequest;
use App\Http\Requests\SecurityQuestionRequest;
use App\Http\Requests\UserNameRequest;
use App\Http\Requests\BirthdayRequest;
use App\Http\Controllers\Controller;
use App\Events\ChangePassword;
use App\Events\ChangeEmail;
use App\UserHistoryData;
use Illuminate\Support\Facades\Event;
use App\Certificates;
use App\CertificatesUser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Http\Requests\BasicDetailsRequest;
use App\Http\Requests\WorkPreferencesRequest;
use App\Http\Requests\AvailabilityAndRateRequest;
use App\Http\Requests\Employee\Profile\SkillsRequest;
use App\Http\Requests\Employee\Profile\WwccRequest as WwccProfileRequest;
use App\Http\Requests\Employee\Profile\ReferenceRequest;
use App\Services\YouTube;
use App\Http\Models\YoutubeUploads;

class SettingsController extends Controller
{
    use MailTraits;
    use ResponsesTrait;
    use \App\Http\Traits\GeolocationTrait;

    public function updatePassword(PasswordRequest $request)
    {

        if (!Hash::check($request->old_password, Auth::user()->password)) {
            //echo "No";
            $response = [
                'notification' => [
                    'type' => 'danger',
                    'message' => 'Incorrect old password',
                ]
            ];
            return response()->json($response);
        } else {
            //echo "Yes";
            Auth::user()->update(
                ['password' => bcrypt($request->new_password)]
            );
            $response = [
                'notification' => [
                    'type' => 'success',
                    'message' => 'Password Changed',
                ]
            ];
            Event::fire(new ChangePassword(Auth::user()));
            return response()->json($response);
        }
    }

    public function updateProfilePicture(ProfilePictureRequest $request)
    {
        $user = Auth::user();
        $file = $request->file('profile_picture');

        if ($file && $file->isValid()) {
            $profilePhoto = Image::make($file->getRealPath());
            $profilePhoto2 = Image::make($file->getRealPath());
            $user->user_photo_100 = $profilePhoto->fit(100, 100)->encode('data-url');
            $user->user_photo_64 = $profilePhoto2->fit(64, 64)->encode('data-url');
            $message = 'Profile picture Successfully updated!';
        }

        $delete_photo = $request->get('delete_photo') === 'true' ? true : false;
        $delete = $request->get('delete');
        if ($delete == 'on') {
            $delete_photo = true;
        }

        // Handles photo delete
        if ($delete_photo) {
            $user->user_photo_100 = '';
            $user->user_photo_64 = '';
            $message = 'Profile picture Successfully Deleted!';
        }

        $user->save();

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => $message]
            ]
        );
    }

    /**
     *
     * @return mixed
     */
    public function getUserProgress(){
        return Auth::user()->getProgress();
    }

    public function getCurrentUserProfilePicture()
    {

        if (Auth::check() && $userPhoto = Auth::user()->user_photo_100) {
            return $userPhoto;
        }

        return '/img/no_photo_100.jpg';
    }

    /**
     * Returns Logged in user info
     */
    public function getCurrentUserInfo()
    {

        $user = Auth::user()->info;
        $user2 = Auth::user();
        /** A temporary solution for already created accounts **/
        if (empty($user2->employee_info->user_id)) {
            $employeeInfo = new EmployeeInfo;
            $employeeInfo->user_id = $user2->id;
            $employeeInfo->save();
            return redirect()->route('employee.profile.edit');
        }

        $bdayTime = strtotime($user2->employee_info->birthday);


        if ($bdayTime !== false) {
            $user->user->birthday = date('d/m/Y', $bdayTime);
        } else {
            $user->user->birthday = null;
        }

        if (\request()->has('field')) {
            $user = array_only($user->toArray(), request()->get('field'));
        }


        $data = self::array_utf8_encode($user->toArray());

        return response()->json($data);
    }

    public static function array_utf8_encode($dat)
    {
        if (is_string($dat))
            return utf8_encode($dat);
        if (!is_array($dat))
            return $dat;
        $ret = array();
        foreach ($dat as $i => $d)
            $ret[$i] = self::array_utf8_encode($d);
        return $ret;
    }

    /**
     * Updates the current user address
     * @param AddressRequest|Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setCurrentUserAddress(AddressRequest $request)
    {
        $userInfo = Auth::user()->info;

        $file = $request->get('profile_picture');

//        if ($file && $file->isValid()) {
//            $profilePhoto = Image::make($file->getRealPath());
//            $profilePhotoSave = $profilePhoto->encode('data-url');
//            $userInfo->document_scan =$profilePhotoSave;
//        }
        $userInfo->document_scan = $file;
        $location = $this->getGeoDataByPostcode($request->get('zipcode'));


        dd($location);
//        if (empty($location->results)) {
//             return $this->responseValidationOld([
//                 'zipcode' => ['The post code is invalid']
//             ]);
//        }

        $userInfo->document_type_id = $request->get('document_type_id');
        $userInfo->document_type_name = $request->get('document_type_name');
        $userInfo->document_date = $request->get('document_date');

        $userInfo->line_1 = $request->get('line_1');
        $userInfo->line_2 = $request->get('line_2');
        $userInfo->city = $request->get('city');
        $userInfo->zipcode = $request->get('zipcode');
        if (@isset($location->results[0])) {
            $userInfo->lat = $location->results[0]->geometry->location->lat;
            $userInfo->lng = $location->results[0]->geometry->location->lng;
            $userInfo->country()->associate(Country::find($request->get('country')));
        }
        $userInfo->save();

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => 'User Address Updated Correctly']
            ]
        );
    }

    /**
     * Updates the current user email address
     * @param EmailAddressRequest|Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setCurrentUserNewEmail(EmailAddressRequest $request)
    {
        $old_email = Auth::user()->email;
        Auth::user()->update(['email' => $request->new_email]);

        $user = Auth::user();
        $user->email = $old_email;

        Event::fire(new ChangeEmail(collect([
            'user' => $user,
            'new_email' => $request->new_email
        ])));

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => 'New email Address Updated Correctly']
            ]
        );
    }

    /**
     * Updates the current user Timezone
     * @param TimezoneRequest|Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setCurrentUserTimezone(TimezoneRequest $request)
    {
        $userInfo = Auth::user()->info;
        $userInfo->timezone()->associate(Timezone::find($request->timezone));
        $userInfo->save();

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => 'Timezone Updated Correctly']
            ]
        );
    }

    /**
     * Updates the current user Phone Number
     * @param PhoneNumberRequest|Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setCurrentUserPhone(PhoneNumberRequest $request)
    {


        $userInfo = Auth::user()->info;

        $historyData = array(
            'phone' => $userInfo->phone,
            'phoneDate' => $userInfo->phoneDate,
        );
        $histoty = new UserHistoryData();
        $histoty->type = 'phone';
        $histoty->user_id = $userInfo->id;
        $histoty->data = json_encode($historyData);
        $histoty->save();

        $userInfo->phone = $request->phone;
        $userInfo->phoneDate = time();
        $userInfo->save();

        $user = Auth::user();
        $user->phone_verifed = 1;
        $user->save();
        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => 'Phone Number Updated Correctly']
            ]
        );
    }

    public function setCurrentUserWWCC(WwccRequest $request)
    {
        $userInfo = Auth::user()->info;
        $user = Auth::user();

        $historyData = array(
            'wwcc_number' => $userInfo->wwcc_number,
            'wwcc_notes' => $userInfo->wwcc_notes,
            'wwcc_image' => $userInfo->wwcc_image,
            'wwcc_date' => $userInfo->wwcc_date,
            'wwcc_status' => $user->wwcc_status,
            'wwcc_status_expdate' => $user->wwcc_status_expdate,
        );

        $histoty = new UserHistoryData();
        $histoty->type = 'wwcc';
        $histoty->user_id = $userInfo->id;
        $histoty->data = json_encode($historyData);
        $histoty->save();

        $userInfo->wwcc_number = $request->wwcc;
        $userInfo->wwcc_notes = $request->notes;
        $userInfo->wwcc_image = $request->photo;
        $userInfo->wwcc_date = time();
        $userInfo->save();


        $user->wwcc_status = 0;
        $user->wwcc_status_expdate = '';
        $user->save();

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => 'WWCC Number Updated Correctly']
            ]
        );
    }

    function saveVerid(saveVeridRequest $request)
    {
        $user = Auth::user();
        $employeeInfo = EmployeeInfo::where('user_id', $user->id)->first();
        $userInfo = Auth::user()->info;

        $historyData = array(
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'user_photo_100' => $user->user_photo_100,
            'birthday' => $employeeInfo->birthday,
            'prof_scan_id' => $userInfo->prof_scan_id,
            'prof_scan_id2' => $userInfo->prof_scan_id2,
            'prof_id_type' => $userInfo->prof_id_type,
            'prof_id_sc' => $userInfo->prof_id_sc,
            'prof_dateof' => $userInfo->prof_dateof,
            'prof_visa' => $userInfo->prof_visa,
            'bd_id_notes' => $userInfo->bd_id_notes,
            "verifed_id" => $user->verifed_id,
            "date_submit" => $user->updated_at->toDateTimeString(),
            'verifed_id_visa_exp' => $user->verifed_id_visa_exp,
            'verifed_id_expdate' => $user->verifed_id_expdate
        );
        $histoty = new UserHistoryData();
        $histoty->type = 'verifed-id';
        $histoty->user_id = $user->id;
        $histoty->data = json_encode($historyData);
        $histoty->save();

        if (strlen($user->user_photo_100_ != strlen($request->user_photo_100))) {

            preg_match("/data:image\/([a-z]+)\;/", $request->user_photo_100, $imginfo);

            if (count($imginfo)) {
                $new_filename = md5(time() . rand() . rand()) . '.' . $imginfo[1];

                $imgdata = preg_split("/data:image\/([a-z]+);base64,/", $request->user_photo_100)[1];
                $imgdata = base64_decode($imgdata);

                file_put_contents("upload/user/" . $new_filename, $imgdata);

                $user->user_photo_100 = '/upload/user/' . $new_filename;
            }

        }


        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
//        $user->user_photo_100 = chunk_split($request->user_photo_100);
        $user->verifed_id = 0;
        $user->verifed_id_visa_exp = '';
        $user->verifed_id_expdate = '';
        $user->save();

        $bday = Carbon::createFromFormat("d/m/Y", $request->birthday);
        $employeeInfo->birthday = $bday->format('d/m/Y');
        $employeeInfo->save();

        $file = $request->file('prof_scan_id');
        if ($file && $file->isValid()) {
            $destinationPath = 'upload/user'; // upload path
            $extension = $file->getClientOriginalExtension();
            $fileName = $user->id . "_" . time() . '.' . $extension;
            $file->move($destinationPath, $fileName);
            $userInfo->prof_scan_id = $fileName;
        } else {
            $userInfo->prof_scan_id = $request->prof_scan_id;
        }
        $userInfo->prof_scan_id2 = $request->prof_scan_id2;
        $userInfo->prof_id_type = $request->prof_id_type;
        $userInfo->prof_id_sc = $request->prof_id_sc;
        $userInfo->prof_dateof = $request->prof_dateof;

        if ($request->notau === "true") {
            $userInfo->prof_visa = $request->prof_visa;
        } elseif ($request->notau === "false") {
            $userInfo->prof_visa = '';
        }

        $userInfo->bd_id_notes = $request->notes;
        $userInfo->save();

    }

    public function uploadImage(Request $request)
    {
        /** @var UploadedFile $file */
        $files = $request->file();

        // If no files is present, return the error.
        if (count($files) < 1) {
            return [
                'error' => 'Please, select a file to upload.'
            ];
        }

        $file = $files[0];
        // If the file is not valid or wasn't moved, return an error.
        if (!$file || !$file->isValid()) {
            return [
                'error' => 'Unable to upload the document. Please, try again.'
            ];
        }

        $name = md5(rand(0, 1000) . time() . rand(0, 100)) . "." . $file->getClientOriginalExtension();
        $file->move("upload/user/", $name);

        return ['image_path' => '/upload/user/' . $name];

    }

    public function uploadVideo(Request $request)
    {
        $user = Auth::user();
        $path = 'upload/user/';

        if($user->video_url){
            $file = $path . basename($user->video_url);
            if (file_exists($file) && is_file($file) && is_writable($file)) {
                unlink($file);
                $user->video_url = null;
                $user->save();
            }
        }

        foreach ($request->files as $file) {

            $youtubeService = new YouTube();
            $video_id = $youtubeService->uploadVideo($file->getRealPath(), 'caregiver-'.$user->id);

            $youtubeUpload = new YoutubeUploads;
            $youtubeUpload->user_id = $user->id;
            $youtubeUpload->video_id = $video_id;
            $youtubeUpload->save();
        }

        return ['video_url' => $user->video_url, 'video_id' => $user->youtube_upload->video_id];
    }

    public function removeVideo(Request $request)
    {
        $user = Auth::user();
        $youtubeService = new YouTube();
        $youtubeService->deleteVideo($request->id);
        return YoutubeUploads::where('user_id', '=', $user->id)
            ->where('video_id', '=', $request->id)
            ->delete();
    }

    function saveAdress(saveAdressRequest $request)
    {
        $userInfo = Auth::user()->info;
        $user = Auth::user();

        $text = array("Land Titles Office records",
            "Mortgage documents",
            "Car registration",
            "Utility bill",
            "Bank statement",
            "Council rates notice",
            "Electoral enrolment card",
            "Insurance renewal document",
            "Other");

        $historyData = array(
            'loc_adress' => $userInfo->loc_adress,
            'loc_adress2' => $userInfo->loc_adress2,
            'loc_city' => $userInfo->loc_city,
            'loc_zip' => $userInfo->loc_zip,
            'loc_scan' => $userInfo->loc_scan,
            'loc_type_id' => $userInfo->loc_type_id,
            'loc_type_text' => @$text[$userInfo->loc_type_id - 1],
            'loc_date' => $userInfo->loc_date,
            'loc_time' => $userInfo->loc_time,
            'loc_issue_date' => $userInfo->loc_issue_date,
            'loc_expiry_date' => $userInfo->loc_expiry_date,
            'address_status' => $user->address_status,
            'address_status_expdate' => $user->address_status_expdate,
        );

        $histoty = new UserHistoryData();
        $histoty->type = 'address';
        $histoty->user_id = $userInfo->id;
        $histoty->data = json_encode($historyData);
        $histoty->save();


        $user->address_status = 0;
        $user->address_status_expdate = '';
        $user->save();

        $userInfo->loc_adress = $request->loc_adress;
        $userInfo->loc_adress2 = $request->loc_adress2;
        $userInfo->loc_city = $request->loc_city;
        $userInfo->zipcode = $request->loc_zip;
        $userInfo->loc_zip = $request->loc_zip;
        $userInfo->loc_scan = $request->loc_scan;
        $userInfo->loc_type_id = $request->loc_type_id;
        $userInfo->loc_type_text = $request->loc_type_text;
        $userInfo->loc_date = $request->loc_date;
        $userInfo->loc_issue_date = $request->loc_issue_date;
        $userInfo->loc_expiry_date = $request->loc_expiry_date;
        $userInfo->loc_time = time();
        $userInfo->address_notes = $request->address_notes;


        $location = $this->getGeoDataByPostcode($request->loc_zip);


        if (!empty($location)) {
            $userInfo->lat = $location->results[0]->geometry->location->lat;
            $userInfo->lng = $location->results[0]->geometry->location->lng;
        }

        $userInfo->save();

        return ['success' => true];
    }

    function saveSoclink(saveSoclinkRequest $request)
    {
        $userInfo = Auth::user()->info;
        $user = Auth::user();

        $historyData = array(
            'soc_link' => $userInfo->soc_link,
            'soc_links_notes' => json_encode($userInfo->soc_links_notes),
            'soc_link_submit' => $userInfo->soc_link_submit,
            'social_verifed' => $user->social_verifed,
        );

        $histoty = new UserHistoryData();
        $histoty->type = 'social';
        $histoty->user_id = $userInfo->id;
        $histoty->data = json_encode($historyData);
        $histoty->save();

        $user->social_verifed = 0;
        $user->save();

        $userInfo->soc_link = $request->soc_link;
        $userInfo->soc_links_notes = json_encode($request->soc_link_notes);
        $userInfo->soc_link_submit = time();
        $userInfo->save();
    }

    function deleteVerifed(deleteVerifedRequest $request)
    {
        switch ($request->type) {
            case 'phone':
                $userInfo = Auth::user()->info;
                $userInfo->phone = '';
                $userInfo->phoneDate = time();
                $userInfo->save();
                break;
            case 'wwcc':
                $userInfo = Auth::user()->info;
                $userInfo->wwcc_number = "";
                $userInfo->wwcc_image = "";
                $userInfo->wwcc_date = time();
                $userInfo->save();
                break;
            case 'qualifications':
//                Auth::user()->educations()->delete();
                EducationUser::where('educations_id', '=', $request->id)->delete();
                Educations::where('id', '=', $request->id)->delete();
                break;
            case 'id':
                $user = Auth::user();
                $user->user_photo_100 = '';
                $user->save();

                $userInfo = Auth::user()->info;
                $userInfo->prof_scan_id = '';
                $userInfo->prof_scan_id2 = '';
                $userInfo->prof_id_type = '';
                $userInfo->prof_id_sc = '';
                $userInfo->prof_dateof = '';
                $userInfo->prof_visa = '';
                $userInfo->save();
                break;
            case 'adress':
                $userInfo = Auth::user()->info;
                $userInfo->loc_adress = '';
                $userInfo->loc_adress2 = '';
                $userInfo->loc_city = '';
                $userInfo->loc_zip = '';
                $userInfo->loc_scan = '';
                $userInfo->loc_type_id = '';
                $userInfo->loc_date = '';
                $userInfo->loc_time = time();
                $userInfo->save();
                break;
            case 'certification':
//                Auth::user()->certificates()->delete();
                CertificatesUser::where('certificates_id', '=', $request->id)->delete();
                Certificates::where('id', '=', $request->id)->delete();
                break;
            case 'abn':
                $userInfo = Auth::user()->info;
                $userInfo->abn_number = '';
                $userInfo->abn_number_submit = time();
                $userInfo->save();
                break;
            case 'social':
                $userInfo = Auth::user()->info;
                $userInfo->soc_link = '';
                $userInfo->soc_link_submit = time();
                $userInfo->save();
                break;
        }
    }

    /**
     * Updates the current user ABN
     * @param ABNNumberRequest|Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setCurrentUserABN(ABNNumberRequest $request)
    {
        $userInfo = Auth::user()->info;

        $historyData = array(
            'abn_number' => $userInfo->abn_number,
            'abn_number_submit' => $userInfo->abn_number_submit,
            'abn_verifed' => Auth::user()->abn_verifed,
        );
        $histoty = new UserHistoryData();
        $histoty->type = 'abn';
        $histoty->user_id = $userInfo->id;
        $histoty->data = json_encode($historyData);
        $histoty->save();

        $userInfo->abn_number = $request->abn_number;
        $userInfo->abn_number_submit = time();
        $userInfo->save();


        Auth::user()->abn_verifed = 0;
        Auth::user()->save();


        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => 'ABN Number Updated Correctly']
            ]
        );
    }

    /**
     * Updates the current user Name
     * @param UserNameRequest|Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setCurrentUserName(UserNameRequest $request)
    {
        Auth::user()->first_name = $request->first_name;
        Auth::user()->last_name = $request->last_name;
        Auth::user()->save();

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => 'Name Updated Correctly']
            ]
        );
    }

    public function setCurrentUserBirthday(BirthdayRequest $request)
    {
        $employeeInfo = EmployeeInfo::where('user_id', Auth::user()->id)->first();
        $bday = $request->birthday;
        $bday = Carbon::createFromFormat("d/m/Y", $bday)->format("d/m/Y");

        $employeeInfo->birthday = $bday;
        $employeeInfo->save();

        $userInfo = Auth::user()->info;
        $file = $request->get('bd_scan_of_id');
        $userInfo->bd_scan_of_id = $file;


        $userInfo->bd_id_type_id = $request->get('bd_id_type_id');
        $userInfo->bd_id_type_name = $request->get('bd_id_type_name');
        $userInfo->bd_id_issuing = $request->get('bd_id_issuing');
        $userInfo->bd_id_number = $request->get('bd_id_number');

        $userInfo->save();

        return response()->json(
            ['notification' => [
                'type' => 'success',
                'message' => 'Birthday Updated Correctly']
            ]
        );
    }

    /**
     * Validates if the old_password matches the authenticated user password.
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @internal param $ $request
     */
    private function handleInvalidPassword($request)
    {
        if (!Hash::check($request->old_password, Auth::user()->password)) {
            return response()->json(
                ['notification' => [
                    'type' => 'danger',
                    'message' => 'Password Doesn\'t Match']
                ]
            );
        }
    }

    public function getCurrentUserCriteria()
    {
        return Auth::user()->allCriteria();
    }

    public function getNotificationsSettings()
    {
        return response()->json(Auth::user()->notifications_settings, 200, [], JSON_NUMERIC_CHECK);
    }

    public function updateNotificationsSettings(NotificationsSettingsRequest $request)
    {
        Notifications::where('user_id', Auth::user()->id)->update($request->all());

        return $this->setResponse();
    }

    public function updateSecurityQuestion(SecurityQuestionRequest $request)
    {
        if (Auth::user()->security_question !== null && Auth::user()->security_question->answer != $request->old_answer) {
            return $this->responseValidationOld([
                'old_answer' => ['The old answer is invalid']
            ]);
        }


        if (Auth::user()->security_question === null) {
            SecurityQuestions::add(SecurityQuestionTypes::whereType($request->new_question)->first()->id, Auth::user()->id, $request->new_answer);
        } else {
            SecurityQuestions::updateSecurityQuestion(
                Auth::user()->security_question->id,
                $request->new_question,
                Auth::user()->id,
                $request->new_answer
            );
        }

        return $this->setResponse([
            'new_question_text' => Auth::user()->security_question->question
        ]);
    }

    public function getUserSecurityQuestion()
    {

        $question = Auth::user()->security_question;

        return $this->setResponse([
            'security_question' => $question !== null ? $question->question : ''
        ]);
    }

    public function sendMessage()
    {
        $type = request('type');
        $id = request('id');

        if (!($id > 0)) {
            $id = null;
        }

        $text = request('text');

        $text = strip_tags($text);

        $message = new VerificationChats();
        $message->user_id = Auth::user()->id;
        $message->verification_type = $type;
        $message->verification_id = $id;
        $message->message = $text;
        $message->message_sender = 'user';
        $message->save();


        return ['success' => true];
    }

    public function sendSmsCode()
    {
        $phone = \request('phone');


        $types = ['sms' => 'sms', 'call' => 'call'];
        $defaultType = $types['sms'];

        $type = \request('method', $defaultType);

        if (!in_array($type, $types)) {
            $type = $defaultType;
        }


        SmsCodes::where('phone', '=', $phone)->update(['active' => 0]);

        $code = new SmsCodes();
        $code->code = rand(1000, 9999);
        $code->phone = $phone;
        $code->user_id = \Auth::user()->id;
        $code->active = 1;
        $code->save();

        switch ($type) {
            case 'sms':
                $client = new \GuzzleHttp\Client();
                $client->request('POST', 'https://api.plivo.com/v1/Account/' . env('PLIVO_AUTH_ID') . '/Message/', [
                    'auth' => [env('PLIVO_AUTH_ID'), env('PLIVO_AUTH_TOKEN')],
                    'json' => [
                        'src' => 'LittleOnes',
                        'dst' => $phone,
                        'text' => 'LittleOnes Code: ' . $code->code
                    ]
                ]);
                break;
            case 'call':
                $client = new \GuzzleHttp\Client();
                $client->request('POST', 'https://api.plivo.com/v1/Account/' . env('PLIVO_AUTH_ID') . '/Call/', [
                    'auth' => [env('PLIVO_AUTH_ID'), env('PLIVO_AUTH_TOKEN')],
                    'json' => [
                        'from' => env('PLIVO_OUTCOME_NUMBER'),
                        'to' => $phone,
                        'answer_url' => env('PLIVO_CALL_RESPONSE_URL') . $code->code,
                        'answer_method' => 'GET'
                    ]
                ]);
                break;
        }

        return ['success' => true];
    }

    public function verifySmsCode()
    {
        $phone = \request('phone');

        $code = request('code');

        if (!$phone || !$code) {
            return response()->json(['success' => false], 422);
        }

        $sms = SmsCodes::where('phone', '=', $phone)
            ->where('code', '=', $code)
            ->where('active', '=', 1)
            ->first();

        if ($sms === null) {
            return response()->json(['success' => false], 422);
        } else {
            $userPhone = Auth::user()->phone->where('phone', $phone)->first();
            if($userPhone){
                $userPhone->verified = 1;
                $userPhone->save();
            }

            return response()->json(
                ['notification' => [
                    'type' => 'success',
                    'message' => 'Phone Number Updated Correctly']
                ]
            );
        }
    }

    public function speakCode($code)
    {
        $xml = new \SimpleXMLElement('<Response/>');
        $speak = $xml->addChild('Speak', 'Hello. This is code to verify you mobile number on Little Ones website.');
        $speak->addAttribute('language', 'en-AU');
        $speak->addAttribute('voice', 'WOMAN');

        $speak = $xml->addChild('Speak', 'Verification code is: ' . implode(', ', str_split($code, 1)));
        $speak->addAttribute('language', 'en-AU');
        $speak->addAttribute('voice', 'WOMAN');
        $speak->addAttribute('loop', '3');

        return response($xml->asXML(), '200')->header('Content-Type', 'text/xml');
    }

    public function saveBasicDetails(BasicDetailsRequest $request)
    {
        return $this->setSplitResponse($request);
    }

    public function saveWorkPreferences(WorkPreferencesRequest $request)
    {
        return $this->setSplitResponse($request);
    }

    public function saveAvailability(AvailabilityAndRateRequest $request)
    {
        return $this->setSplitResponse($request);
    }

    public function saveSkills(SkillsRequest $request)
    {
        return $this->setSplitResponse($request);
    }

    public function saveIdAndWWWC(WwccProfileRequest $request)
    {
        $errors = $request->getErrors();

        if ($request->wwcc && empty($errors)) {
            $user = User::where('id', (int)Auth::user()->id)->with(['employee_info', 'wwcc'])->get();
            $wwcc = $user[0]->wwcc;
            $stateId = $wwcc->state_id;
            $state = State::where('id', (int)$stateId)->get();

            if ($state && isset($state[0]) && in_array($state[0]->name, ['QLD', 'VIC', 'WA', 'SA'])) {

                $result = ScrappingService::scrap($wwcc, $user[0], $state[0]->name);

                if (!$result) {
                    if (is_null($request->getErrors())) {
                        $errors = ['valid_creds'=>'Your card credentials are not valid'];
                    } else {
                        $errors = array_merge(['valid_creds'=>'Your card credentials are not valid'], $request->getErrors());
                    }

                    $wwcc->verified = 0;
                    $wwcc->save();
                } else {
                    $wwcc->verified = 1;
                    $wwcc->save();
                }
            }
        }

        return response([
            "data" => $errors,
            "success" => is_null($errors)
        ], is_null($errors) ? 200 : 422);
    }

    public function deleteReference(ReferenceRequest $request, Reference $reference)
    {
        $reference->delete();
        return $this->setResponse();
    }
}
