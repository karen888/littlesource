<?php

namespace App\Http\Controllers\Api\v1\Prelaunch;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PrelaunchSignupRequest;
use App\Http\Controllers\Controller;

use App\Http\Models\PrelaunchSignup;
use App\Events\UserPrelaunchSignedUp;
use Illuminate\Support\Facades\Event;


class SignupsController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param PrelaunchSignupRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PrelaunchSignupRequest $request)
    {
        $user = PrelaunchSignup::create([
            'email' => $request->email,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'type' => $request->type == 'client' ? 1 : 0,
            'opt_beta' => $request->opt_beta,
            'opt_competition' => $request->opt_competition,
            'opt_here_more' => $request->opt_here_more,
            'confirmation_code' => str_random(30)
        ]);

        Event::fire(new UserPrelaunchSignedUp($user));

        return 'stored';
    }


}
