<?php

namespace App\Http\Controllers\Api\V1\Billing;

use Auth;
use App\Http\Requests;
use App\Http\Requests\CardsRequest;
use App\Http\Controllers\Controller;
use App\Entities\Billing\Cards;
use Illuminate\Support\Facades\Input;

class CardsController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;

    public function store(CardsRequest $request)
    {
        $client = Auth::user();
        $card = new Cards;
        $card->add($request, $client);
        
        return $this->setResponse([
            'card_id' => $card->id,
            'cards' => $client->cards
        ]);
    }

    public function update(CardsRequest $request, $id)
    {
        
        $client = Auth::user();
        $request['id'] = $id;
        $card = new Cards;
        $card->edit($request, $client);

        return $this->setResponse([
            'card_id' => $card->id,
            'cards' => $client->cards
        ]);
    }

}
