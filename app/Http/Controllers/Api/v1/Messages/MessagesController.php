<?php

namespace App\Http\Controllers\Api\v1\Messages;

use App\Entities\Job\Job;
use App\Entities\Message\Message;
use App\Entities\Message\MessagesThread;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessageRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    protected $user = null;

    public function __construct(Auth $auth) {

        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }

        $this->middleware('auth');
        $this->user = Auth::user();
    }


    /**
     * Returns The Current inbox of authenticated user
     */
    public function inbox()
    {
        $messages = $this->mapThreadToJson($this->user->inbox());
        $messages = $messages->sortByDesc('updated_at_timestamp');
       
        $values = $messages->values();
        $values->all();
        // print_r($values);
        return $values;
    }


    /**
     * Returns the archived threads
     * @return mixed
     */
    public function archive()
    {
        return $this->mapThreadToJson($this->user->archive());
    }


    /**
     * Maps a message thread to a JSON format to be used in Inbox/Archive
     * @param $threads
     * @return mixed
     */
    protected function mapThreadToJson($threads)
    {
        return $threads->map(function($obj) {
            //TODO: convert to date obj like @formatMessage
            $new_msg = $this->mdl('MessagesMdl')->chNewMsgThreads(\Auth::user()->id, $obj->id);
            $obj->last_message = $obj->messages->last();  
            return [
                'thread_id' => $obj->id,
                'is_archived' => $obj->is_archived, 
                'updated_at_timestamp' => $obj->updated_at->getTimestamp(),
                'updated_at' => $obj->updated_at->diffForHumans(),
                'iso8061' => $obj->updated_at->toIso8601String(),
                'updated_at_date' => $obj->updated_at->formatLocalized('%d %b %Y'),
                'job' => !$obj->job ? null : [
                    'id' => $obj->job->id,
                    'title' => $obj->job->title,
                    'closed' => $obj->job->closed
                ],
                'sender' => [
                    'name' => $obj->sender->name,
                    'img' => $obj->sender->photo_url
                ],
                'receiver' => [
                    'name' => $obj->receiver->name,
                    'img' => $obj->receiver->photo_url
                ],
                'me' => [
                    'name' => $this->user->name,
                    'img' => $this->user->photo_url
                ],
                'other' => [
                    'name' => $obj->other_party->name,
                    'img' => $obj->other_party->photo_url
                ],
                'last_message' => !$obj->last_message ? null : [
                    'is_read' => \Auth::user()->id == $obj->last_message->user->id ? 1 : $obj->last_message->is_read,
                    'message' => $obj->last_message->message,
                    'from' => [
                        'name' => $obj->last_message->user->name,
                        'img' => $obj->last_message->user->photo_url
                    ]
                ],
                'new_message_count'=> $new_msg,
            ];
        });
    }


    /**
     * Returns a JSON versions of a conversation thread
     * @param $id
     * @return array
     */
    public function conversation($id)
    {
        $thread = MessagesThread::findOrFail($id);
        $output = [
            'thread_id' => $thread->id,
            'me' => [
                'id' => $this->user->id,
                'name' => $this->user->name,
                'img' => $this->user->photo_url,
            ],
            'other' => [
                'id' => $thread->other_party->id,
                'name' => $thread->other_party->name,
                'img' => $thread->other_party->photo_url,
            ],
            'job' => !$thread->job ? null : [
                'id' => $thread->job->id,
                'title' => $thread->job->title,
                'closed' => $thread->job->getAttributes()['closed'],
                'owner' => [
                    'name' => $thread->job->user->name,
                    'img' => $thread->job->user->photo_url,
                ]
            ],
            'messages' => $this->getMessagesArray($thread->messages)
        ];
        //don't know if this is right, the user still sees the msg's as unread
        if($thread) $thread->setMessagesAsRead();
        return $output;
    }


    /**
     * Maps thread messages to a JSON output
     * @param $messages
     * @return mixed
     */
    protected function getMessagesArray($messages)
    {
        return $messages->map(function($obj) {
            return $this->formatMessage($obj);
        });
    }


    /**
     * Local method for generating JSON data for consuming through AJAX
     * @param $msg
     * @return array|null
     */
    protected function formatMessage($msg)
    {
        if($msg instanceof Collection) {
            $ret = [];
            foreach($msg as $m) { $ret[] = $this->formatMessage($m); }
            return $ret;
        }
        return $msg ? [
            'id' => $msg->id,
//            'date' => [
//                'iso8061' => $msg->updated_at->toIso8601String(),
////                //TODO: remove all other dates, now using moment.js
//                'for_humans' => $msg->updated_at->diffForHumans(),
//                'day' => $msg->updated_at->formatLocalized('%d %b'),
//                'time' => $msg->updated_at->formatLocalized('%H:%M %p')
//            ],
            'date' => $msg->updated_at->toIso8601String(),
            'is_read' => $msg->is_read,
            'message' => $msg->message,
            'from' => [
                'id' => $msg->user->id,
                'name' => $msg->user->name,
                'img' => $msg->user->photo_url
            ]
        ] : null;
    }


    /**
     * POST method for adding a new message to an new/existing thread
     * Receives: job_id[int|null], receiver_id[int], message[string]
     * also: message_id when editing a message (in the future)
     * @param MessageRequest $request
     * @return array
     */
    public function messagePost(MessageRequest $request)
    {
        $thread = $this->user->findThread($request['job_id'], $request['receiver_id']);
        if(!$thread) {
            $thread = new MessagesThread();
            $thread->sender()->associate($this->user);
            $thread->receiver()->associate(User::find($request['receiver_id']));
            $thread->job()->associate(Job::find($request['job_id'])); //can be null
            $thread->save();
        }

        $msg = Message::find($request['message_id']);
        if(!$msg) {
            $msg = new Message();
            $msg->thread()->associate($thread);
            $msg->user()->associate($this->user);
        }
        $msg->message = nl2br($request['message']);
        $saved = $msg->save();
        $msgs = $msg;
        if($thread->updated_at->toIso8601String() != $request['last_timestamp']) {
            $msgs = $thread->messages()->afterDate(new Carbon($request['last_timestamp']))->get();
        }

        $thread->touch();
        return response()->json([
            'success' => $saved,
            'messages' => $this->formatMessage($msgs)
        ]);
    }



    /**
     * Returns the list of users that the current user has chatted with
     * @return \Illuminate\Database\Eloquent\Collection|static|static[]
     */
    public function corespondents()
    {
        $corespondents = $this->user->corespondents();
        // $corespondents = User::all(); //TODO: temp while designing the message form
        // $corespondents = $this->user->inbox();
        $corespondents = $corespondents->map(function($obj) {
            // print_r($obj); exit;
            $ret['id'] = $obj->id;
            $ret['name'] = $obj->first_name." ".$obj->last_name;
            $ret['photo_url'] = $obj->photo_url;
            $ret['is_caregiver'] = $obj->is_caregiver;
            return $ret;
        });
        return $corespondents;
    }



}

