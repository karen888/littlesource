<?php

namespace App\Http\Controllers\Api\v1\Clients;

use App\Entities\Criteria\Base;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Entities\Offer\Offers;
use App\Entities\Message\MessagesThread;
use App\Http\Requests\MyCaregiversRequest;
use App\User;

class ClientController extends Controller
{
    use \App\Http\Traits\ResponsesTrait;

    protected $user = null;


    /**
     * @param Auth $auth
     */
    public function __construct(Auth $auth) {
        $this->user = Auth::user();
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function findCaregivers()
    {
        return response()->json(Base::allCriteria());
    }

    public function getMessaged(MyCaregiversRequest $request)
    {
        $messageThreadsIStarted = MessagesThread::where('sender_id', Auth::user()->id)->where('is_archived', 0)->get();
        $messageThreadsIDidNotStarted = MessagesThread::where('receiver_id', Auth::user()->id)->where('is_archived', 0)->get();

        $ids = [];
        $data = [];

        foreach ($messageThreadsIStarted as $messageThread) {
            array_push($ids, $messageThread->receiver->id);
        }

        foreach ($messageThreadsIDidNotStarted as $messageThread) {
            array_push($ids, $messageThread->sender->id);
        }

        $users = User::whereIn('id', $ids);

        if (!empty($request->search)) {
            $users = $users->where(function ($query) use ($request) {
                $query->where('first_name', 'like', "%$request->search%")
                    ->orWhere('last_name', 'like', "%$request->search%");
            });
        }
        $users = $users->get();

        foreach ($users as $item) {
            array_push($data, [
                    'id' => $item->id,
                    'photo_url' => $item->photoUrl,
                    'name' => $item->name,
                'profile' => route('employee.profile', [$item->id]),
                'invite' => route('client.employee.invite', [$item->id]),                    
                    'route' => route('messages.index', ['direct' => $item->id])
            ]);
        }

        return $this->setResponse([
            'messaged' => $data
        ]);
    }

    public function getHired(MyCaregiversRequest $request)
    {
        $ids = [];
        $data = [];

        $offers = Offers::where(['from_user_id' => Auth::user()->id])->get();

        foreach ($offers as $item) {
            array_push($ids, $item->employee->id);
        }

        $users = User::whereIn('id', $ids);

        if (!empty($request->search)) {
            $users = $users->where(function ($query) use ($request) {
                $query->where('first_name', 'like', "%$request->search%")
                    ->orWhere('last_name', 'like', "%$request->search%");
            });
        }
        $users = $users->get();

        foreach ($users as $item) {
            if(!$item->canApplyToJob()) continue;

            array_push($data, [
                'id' => $item->id,
                'photo_url' => $item->PhotoUrl,
                'name' => $item->name,
                'profile' => route('employee.profile', [$item->id]),
                'invite' => route('client.employee.invite', [$item->id]),
                'messages' => route('messages.index', ['direct' => $item->id])
            ]);
        }

        return $this->setResponse([
            'hired' => $data
        ]);
    }
}
