<?php namespace App\Http\Controllers;

use Auth;

class MenuController extends Controller {

    public function get()
    {
        if (\Auth::guest()) {
            $view = $this->getGuestMenu();
        } else {
            $view = $this->getAuthMenu();
        }

        return $view->render();
    }

    private function getSubMenu($list, $main_menu_id, $sec = true)
    {
        $res['submenu'] = Array();
        $res['active'] = '';

        foreach ($list as $val) {
            if ($val->main_menu == $main_menu_id) {
                if (empty($res['active'])) {
                    $active = $this->chActiveMenu($val->url);
                    $res['active'] = $active;
                }

                if ($sec) {
                    $submenu = $this->getSubMenu($list, $val->id, false);

                    $val->submenu = $submenu['submenu'];
                    if (empty($res['active'])) $res['active'] = $submenu['active'];
                }

                $res['submenu'][] = $val;
            }
        }

        return $res;
    }

    private function chActiveMenu($url)
    {
        $ch_url = trim($url, '/');
        $active_menu = '';

        $curr_url = strpos(\Request::path(), $ch_url);
        if (($curr_url !== false)) {
            $active_menu = 'active';
        }

        return $active_menu;
    }

    public function getAuthMenu()
    {
        $view = view('menu.auth');
        $res = Array();

        // check new message
        $new_msg = $this->mdl('MessagesMdl')->chNewMsg(\Auth::user()->id);

        $list = $this->mdl('MenuModel')->getTop();
        foreach ($list as $val) {
            if (!$val->main_menu) {
                $ch_url = trim($val->url, '/');
                $val->active_menu = '';
                if ($ch_url == \Request::path()) $val->active_menu = 'active';
                
                $val->name_css = strtolower($val->name);
                $val->name_css = str_replace(' ', '-', $val->name_css);

                $submenu = $this->getSubMenu($list, $val->id);
                $val->submenu = $submenu['submenu'];
                if (empty($val->active_menu)) $val->active_menu = $submenu['active'];

                $res[$val->id] = $val;
            }
        }

        $view->with('top_menu_list', $res);

        // notifications
        $notif = false;
        if (!empty($info)) $notif = true;
		$view->with('count_new_message', $new_msg);
        $view->with('notification_unread', $notif);
        $view->with('isCaregiver', Auth::user()->is_caregiver);

        return $view;
    }

    public function getGuestMenu()
    {
        return view('menu.guest');
    }
}


