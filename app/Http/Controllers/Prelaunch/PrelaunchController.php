<?php

namespace App\Http\Controllers\Prelaunch;

use App\Entities\Contact\Contact;
use App\Http\Models\PrelaunchSignup;
use App\Http\Requests;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use Mail;
use Session;
use Redirect;
use App\Http\Controllers\Controller;

class PrelaunchController extends Controller
{
    /**
     * Displays the Pre-launch role selection page.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pre-launch.index');
    }


    /**
     * Shows /pre-launch/client page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function clients()
    {
        return view('pre-launch.clients');
    }


   

    /**
     * Shows /pre-launch/caregivers page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function caregivers()
    {
        return view('pre-launch.caregivers');
    }


    /**
     * Updates the user status as confirmed
     * @param $confirmationCode
     * @return \Illuminate\Http\RedirectResponse
     */
    public function confirm($confirmationCode)
    {
        $signup = PrelaunchSignup::whereConfirmationCode($confirmationCode)->firstOrFail();
        $signup->is_confirmed = true;
        $signup->confirmation_code = null;
        $signup->save();

        flash()->success('Email Successfully Confirmed!');

        //return redirect()->route('index');

        if($signup->type == 1)
            return redirect()->route('pre-launch.clients');
        else
            return redirect()->route('pre-launch.caregivers');
    }

    public function unsubscribe($email)
    {
        $signup = PrelaunchSignup::whereEmail($email)->firstOrFail();
        $signup->delete();

        flash()->warning('Email Successfully Removed!');
        return redirect()->route('index');
    }


    public function contactsend(Request $request)
    {
        if($request->ajax()) {
              $data = array(
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
                'bodyMessage' => $request->message
            );
            $fromEmail='postmaster@littleones.net.au';
            $fromName='Administrador';
            Mail::send('emails.contact',$data,function($message) use($fromName,$fromEmail)
                {
                    $message->to($fromEmail,$fromName);
                    $message->from($fromEmail,$fromName);
                    $message->subject('New Messagge');
                });
            $contact = new Contact;
            $contact->name = $request->name;
            $contact->email = $request->email;
            $contact->subject = $request->subject;
            $contact->message = $request->message;
            $contact->save();
            return response()->json([
                    "message" => "Sent"
                ]);
       }  
    }

    public function contact()
    {
        return view('pre-launch.contact');
    }

    /**
     * Temporary route for testing email template designs.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function emailTemplate()
    {
        // TODO: this route & ctrl@method should be delete after testing email design!
        return view('emails.pre-launch.confirmation');
    }

    public function competitionTermsAndConditions()
    {
        return view('pre-launch.competitionTC');
    }

    public function faqs()
    {
        return view('pre-launch.FAQs');
    }

    public function caregiversVerification()
    {
        return view('pre-launch.caregivers-verification');
    }
}
