<?php namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MyContractsCloseCtl extends Controller {

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $id = \Route::input('three');

        $this->withViewComp($view_file, 'data_info', $this->getInfo($id));

        //\App::make('JsCss')->hz();
        //\JsCss::addJs('hz.js');
        //\JsCss::setVal('!!!!!');
        //\JsCss::getVal();
    }

    private function getInfo($id)
    {
        //$res['offer'] = Array();
        //$res['feedback'] = Array('something');
        $res = new \StdClass();
        $res->feedback_message = false;
        $res->employer = false;
        $res->can_proc = false;
        $res->offer = Array();

        if ($id) {
            //$res['offer'] = $this->mdl('OffersMdl')->getOfferByIdUser($id, \Auth::user()->id, 1);
            $res->offer = $this->mdl('OffersMdl')->chOfferByUser($id, \Auth::user()->id, null, 0);
            if (!empty($res->offer)) {
                
                $table = $this->mdlOrm('FeedbacksMdl');
                $res->feedback = $table::where('from_user_id', \Auth::user()->id)->where('job_id', $res->offer->job_id)->where('offer_id', $id)->first();
                if (empty($res->feedback)) {
                    $res->can_proc = true;

                    if ($res->offer->from_user_id == \Auth::user()->id) {
                        $res->employer = true;

                        $res->user_info = userPhotoFull(\App\User::find($res->offer->to_user_id));

                        $table = $this->mdlOrm('CardsMdl');
                        $res->cards = $table::where('user_id', \Auth::user()->id)->where('card_status', 1)->get();

                    } else $res->user_info = userPhotoFull(\App\User::find($res->offer->from_user_id));

                } else $res->feedback_message = true;
            }
        }

        return $res;
    }

    public function postData($request)
    {
        $stat = 1;
        $msg = routeClear('home');

        $data_info = $this->getInfo($request->get('id'));

        if ($data_info->can_proc) {
            \Validator::extend('ch_rate', function($attribute, $value, $parameters) {
                $rate = Array(1,2,3,4,5,6,7,8,9,10);
                if (in_array($value, $rate)) return true;
                else return false;
            });

            //\Validator::extend('ch_card', function($attribute, $value, $parameters) {
                //$table = $this->mdlOrm('CardsMdl');
                //$res = $table::where('user_id', \Auth::user()->id)->where('id', $value)->where('card_status', 1)->first();
                //if (empty($res)) return false;
                //return true;
            //});

            if ($data_info->employer && ($data_info->offer->job_apply_fixed_price == 1)) {
                $valid_rule = Array(
                    'rate' => 'required|ch_rate',
                    'feedback' => 'required|min:2',
                    'pay' => 'required|numeric',
                    'card' => 'required|not_in:0|card_id',
                );
                $valid_errs = Array(
                    'card.card_id' => '',
                );
            } else {
                $valid_rule = Array(
                    'rate' => 'required|ch_rate',
                    'feedback' => 'required|min:2',
                );
                $valid_errs = Array();
            }

            $validator = \Validator::make(
                $request->all(),
                $valid_rule,
                $valid_errs
            );

            $ch_valid = $this->chValid($validator, Array('rate' => 'rate_group'));

            if (!empty($ch_valid)) {
                $stat = 0;
                $msg = $ch_valid;
            } else {
                $save = true;
                    if ($data_info->employer) { 
                        $msg = routeClear('home').'/contracts/my-contracts';

                        if ($data_info->offer->job_apply_fixed_price == 1) {

                            // payment
                            $data_pay['amount'] = $request->get('pay');
                            $data_pay['name'] = $data_info->offer->title;
                            $data_pay['description'] = $data_info->offer->title;
                            $data_pay['job_id'] = $data_info->offer->job_id;
                            $data_pay['offer_id'] = $data_info->offer->id;
                            $data_pay['to_user_id'] = $data_info->offer->to_user_id;
                            $payment_err = $this->ctl('PaymentsCtl')->make($request->get('card'), $data_pay);
                            if ($payment_err) {
                                $save = false;

                                $stat = 2;
                                $msg = $payment_err;
                            } else {
                                $data_offer['offer_paid'] = $request->get('pay');
                            }
                        }
                    }

                if ($save) {
                    $table = $this->mdlOrm('FeedbacksMdl');
                    $table->from_user_id = \Auth::user()->id;
                
                    //if ($data_info->offer->from_user_id == \Auth:user()->id) $to_user_id = $data_info->offer->to_user_id;
                    //else $to_user_id = $data_info->offer->from_user_id;
                    $table->to_user_id = $data_info->user_info->id;

                    $table->offer_id = $data_info->offer->id;
                    $table->job_id = $data_info->offer->job_id;
                    $table->feedback_text = $request->get('feedback');
                    $table->feedback_rate = ((int) $request->get('rate')) / 2;

                    //$stat = 4; $msg = ((int) $request->get('rate')) / 2;
                    $table->save();

                    // close offer
                    $table = $this->mdlOrm('OffersMdl');
                    $table = $table::where('id', $request->get('id'));
                    $data_offer['status'] = 3;
                    $table->update($data_offer);

                    // email
                    $data_mail['job_title'] = $data_info->offer->title;
                    $this->ctl('EmailCtl')->msgFeedback($data_info->user_info, $data_mail);

                    // if you have already feedback then use this condition
                    if ($data_info->offer->status != 3) {
                        // to messages
                        $data_message['feedback_offer_id'] = $data_info->offer->id;
                        $data_message['message'] = 'Feedback';
                        $this->ctl('Messages.ActionsCtl')->createMessage(\Auth::user()->id, $data_info->user_info->id, $data_message, $data_info->offer->job_id);
    
                        // notification
                        $url_link = routeClear('home_offer', Array(), false).'/'.$request->get('id');
                        $this->ctl('Home.NotificationsMsgsCtl')->offer($data_info->offer->to_user_id, $data_info->offer, $url_link, 3);
                    }
                }
            }
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
