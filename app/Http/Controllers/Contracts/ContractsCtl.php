<?php namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContractsCtl extends Controller {

	public function __construct()
	{
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
		$this->middleware('auth');
    }

    public function getRoute()
    {
        return $this->procRoute('contracts');
    }

    public function postRoute(Request $request)
    {
        return $this->procRoute('contracts', $request);
    }
}
