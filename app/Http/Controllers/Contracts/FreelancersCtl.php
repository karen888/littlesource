<?php namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FreelancersCtl extends Controller {

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $data_list = $this->getFreelancers();

        $this->withViewComp($view_file, 'data_list', $data_list);
    }

    public function getFreelancers()
    {
        $list = $this->mdl('OffersMdl')->getOffersFrelancers(\Auth::user()->id);
        if (!empty($list)) {
            $table = $this->mdlOrm('Jobs.JobsMdl');
            foreach($list as $val) {

                $val->jobs_id = explode(',', $val->jobs_id);
                $val->jobs = $table::find($val->jobs_id);
                $val->jobs_count = count($val->jobs) - 1;
            }
        }

        return $list;
    }
}
