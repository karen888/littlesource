<?php namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MyContractsCtl extends Controller {

    public function subRoute($request = null)
    {
        $res = null;

        $id = \Route::input('two');
        if ($id) {
            $res = $this->procRoute('contracts', $request, 'my-contracts-close');

        } else {
            $res = $this->procRoute($this, $request);
        }


        return $res;
    }

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {

        $contracts_list = $this->getContracts();
        $this->withViewComp($view_file, 'contracts_list',  $contracts_list);

    }

    public function getContracts()
    {
        $res = $this->mdl('OffersMdl')->getOffersForEmployer(\Auth::user()->id, true, true);


        return $res;
    }

    public function postData($request)
    {
    }
}
