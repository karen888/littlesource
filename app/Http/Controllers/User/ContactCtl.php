<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactCtl extends Controller {

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {

    }

    public function postData(Request $request)
    {
        $user = \Auth::user();

        $stat = 6;
        $msg = 'saved';


        $validator = \Validator::make(
            $request->all(),
            Array(
                'name' => Array('required', 'min:2'),
                'email' => 'required|email|unique:users,email,'.$user->id
            )
        );

        $ch_valid = $this->chValid($validator);

        if (!empty($ch_valid)) {
            $stat = 0;
            $msg = $ch_valid;
        } else {
            $user->name = $request->get('name');
            $user->email = $request->get('email');

            $user->save();
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }

}
