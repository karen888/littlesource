<?php

namespace App\Http\Controllers\User\Settings;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('user.settings.notifications', ['user' => $user]);
    }
}
