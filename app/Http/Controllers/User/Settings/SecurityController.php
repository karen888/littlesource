<?php

namespace App\Http\Controllers\User\Settings;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\SecurityQuestion\SecurityQuestionTypes;

class SecurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('user.settings.password-and-security', ['user' => $user, 'security_questions' => SecurityQuestionTypes::getCustomSelectList()]);
    }
}
