<?php

namespace App\Http\Controllers\User\Settings;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Country\Country;
use Auth;
use App\Entities\Employee;
use App\Certificates;
use App\CertificatesUser;

use App\Entities\Employee\EmployeeInfo;
use App\Entities\Educations\Educations;
use App\Entities\Languages\Languages;
use App\Entities\Criteria\Base as Criteria;
use App\Http\Requests\Employee\Profile\AvailabilityRequest;
use App\Http\Requests\Employee\Profile\EducationRequest;
use App\Http\Requests\Employee\Profile\EducationRequestVerify;
use App\Http\Requests\Employee\Profile\HourlyRateRequest;
use App\Http\Requests\Employee\Profile\NameTitleRequest;
use App\Http\Requests\Employee\Profile\OverviewRequest;
use App\Http\Requests\Employee\Profile\QualificationsRequest;
use App\Http\Requests\Employee\Profile\SkillsRequest;
use App\Http\Requests\Employee\Profile\ExperienceRequest;
use App\Http\Requests\Employee\Profile\CertificatesRequest;
use App\Http\Requests\Employee\Find\FindCaregiversRequest;
use App\Http\Models\Skill;
use App\Http\Models\UserInfo;
use App\User;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class MyInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $countries = Country::getCustomSelectCountries();
        $timezones = [];
        $user = Auth::user();

        /** A temporary solution for already created accounts **/
        if (empty($user->employee_info->user_id)) {
            $employeeInfo = new EmployeeInfo;
            $employeeInfo->user_id = $user->id;
            $employeeInfo->save();
            return redirect()->route('employee.profile.edit');
        }

        $bday = $user->employee_info->birthday;

        if(!is_null($bday)) {
            $bday = date('d/m/Y', strtotime($bday));
        }

        $user->birthday = $bday;

        if($request->ajax()){
            $user->can_apply_to_job = $user->canApplyToJob();
            $user->is_user_fully_verified = $user->isUserFullyVerified();
            $user->user_progress = $user->getProgress();
            $user->load([
                'phone', 'employee_info', 'passport_id', 'driver_licence_id', 'government_id', 'secondary_id', 'cards', 'certificates', 'location', 'references', 'youtube_upload',
                'maxPerBooking', 'availability', 'yearsExperience', 'wwcc', 'skill', 'careType', 'ownChildren',
                'service', 'childAge', 'locationRange', 'qualification', 'smoker', 'pets', 'hasOwnCar', 'gender'
            ]);
        }

        return $request->ajax() ? $user->toArray() : view('user.settings.my-info', compact('countries', 'timezones', 'user'));
    }

    public function verified()
    {
        $user = Auth::user()->info;
        $user2 = Auth::user();
        /** A temporary solution for already created accounts **/
        if (empty($user2->employee_info->user_id)) {
            $employeeInfo = new EmployeeInfo;
            $employeeInfo->user_id = $user2->id;
            $employeeInfo->save();
            return redirect()->route('employee.profile.edit');
        }

        $user->user->birthday = $user2->employee_info->birthday;
        if(isset($user2->educations)) {
            $user->educations = $user2->educations;
        }
        if(isset($user2->certificates)) {
            $user->certificates = $user2->certificates;
        }
//        print_r($user->educations[1]);
        return view('user.settings.verified', compact('user'));
    }

    public function mobile()
    {
        $user = Auth::user();
        return view('user.settings.mobile', compact('user'));
    } 

    public function qualification()
    {
        $user = Auth::user();
        return view('user.settings.qualification', compact('user'));
    }

    public function wwcc()
    {
        $user = Auth::user();
        return view('user.settings.wwcc', compact('user'));
    }
    public function address()
    {
        $user = Auth::user();
        return view('user.settings.address-verifed', compact('user'));
    }
    public function certificates()
    {
        $user = Auth::user();
        return view('user.settings.certificates', compact('user'));
    }

    public function socialLinks()
    {
        $user = Auth::user();
        return view('user.settings.social-links', compact('user'));
    }

    public function abn()
    {
        $user = Auth::user();
        return view('user.settings.abn', compact('user'));
    }

    public function id()
    {
        $user = Auth::user();
        return view('user.settings.verifed-id', compact('user'));
    }

    public function convertImageTo64($request)
    {
        print $request->files;
    }
}
