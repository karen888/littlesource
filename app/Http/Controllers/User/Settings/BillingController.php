<?php

namespace App\Http\Controllers\User\Settings;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Country\Country;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class BillingController extends Controller
{
    /**
     * Display a listing of the Billing Methods of the user.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();//dd($user);
        //echo "<pre>"; var_dump($user);die;
       $data_cards = DB::table('cards')->where('user_id',$user["id"] )->get();
        return view('user.billing-methods.index', ['user' => $user,'card'=>$data_cards]);
    }

    /**
     * Display a Form To Create a Billing Method .
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $year = date('Y');
        $user = Auth::user();

        return view('user.billing-methods.create', [
            'year' => $year,
            'user' => $user
        ]);
    }
    public function delete(Request $request){

        $json_data=array();
        $get_id = Input::get('id');
        $user = Auth::user();
        $data_cards = DB::table('cards')->where('id',$get_id )->first();
        if ($user["id"] && $data_cards->user_id == $user["id"]){
            DB::table('cards')->where('id',$get_id )->delete();
          $json_data["success"]="The Billing Method was deleted";
        }else{
            $json_data["error"] = "You don't have permission";
        }
        echo json_encode($json_data);

    }

    public function update(Request $request,$id){
//        https://basecamp.com/1927295/projects/6184792/todos/225595430


            $year = date('Y');
            $country = Country::getCustomSelectCountries();
            $user = Auth::user();

            $data_cards = DB::table('cards')->where('id',$id )->first();

        return view('user.billing-methods.update', [
            'year' => $year,
            'country' => $country,
            'user' => $user,
            'data_cards'=>$data_cards

        ]);

        

    }

}
