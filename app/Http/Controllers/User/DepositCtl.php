<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DepositCtl extends Controller {

    protected $view_file;

    public function subRoute($request = null)
    {
        $res = null;

        $id = \Route::input('two');
        if ($id) {
            $res = $this->procRoute('user', $request, 'deposit-add');

        } else {
            $res = $this->procRoute($this, $request);
        }


        return $res;
    }

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $this->withViewComp($view_file, 'cards_list', $this->getInfo());
    }

    private function getInfo()
    {
        $table = $this->mdlOrm('CardsMdl');
        $res = $table::where('user_id', \Auth::user()->id)->get();
        if (!empty($res)) {
            foreach($res as $val) {
                $val->link_edit = routeClear('user_setting').'/deposit/add/'.$val->id;
            }
        }

        return $res;
    }

    public function postData($request)
    {
        $stat = 3;
        $msg = Array();

        $data_msg['name'] = 'delTd';
        $data_msg['val'] = '';
        $msg[] = $data_msg;
        
        $id = $request->get('card_id');
        if ($id) {
            $offer = $this->mdl('OffersMdl')->chCardId($id);
            if (!empty($offer)) {
                $stat = 4;
                $msg = 'This card uses in your projects.';
            
            } else {
                $table = $this->mdlOrm('CardsMdl');
                $table::where('user_id', \Auth::user()->id)->where('id', $id)->delete();
            }
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
