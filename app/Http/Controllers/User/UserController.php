<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function getRoute()
    {
        return $this->procRoute('user', null, null, 'contact');
    }

    public function postRoute(Request $request)
    {
        return $this->procRoute('user', $request, null, 'contact');
    }

    //public function getSetting()
    //{
        //$item = \Route::input('one');
        //$item = strtolower($item);
        //if (!$item) $item = 'contact';

        //$this->setView('user.'.$item);

        //$item_u = ucfirst($item);

        ////return $this->setVC('user.'.$item, 'User.'.$item_u.'Ctl');
        //$ctl = $this->ctl('User.'.$item_u.'Ctl');
        //if (method_exists($ctl, 'getData')) {
            //$ctl->getData('user.'.$item);
        //}
        ////try {
            ////$this->ctl('User.'.$item_u.'Ctl')->getData('user.'.$item);
        ////} catch(\Exception $e) {
            //////print_r($e);
        ////}

        //return $this->view;
    //}

    //public function postSetting(Request $request)
    //{
        //$item = \Route::input('one');
        //$item = strtolower($item);
        //if (!$item) $item = 'contact';
        //$item = ucfirst($item);

        //return $this->ctl('User.'.$item.'Ctl')->postData($request);
    //}
}
