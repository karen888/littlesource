<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileCtl extends Controller {

    protected $view_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        //$hz = \Carbon\Carbon::now('Etc/UTC');
        //print_r($hz);

        $list = $this->mdl('Skills.UsersMdl')->getListByUser(\Auth::user()->id);
        $this->withViewComp($view_file, 'skills_list', $list);

        $user_info = $this->setAvailable();
        $this->withViewComp($view_file, 'user_info', $user_info);

        $timezones = $this->mdlOrm('TimezonesMdl');
        $timezones = $timezones::get();
        $this->withViewComp($view_file, 'timezones', $timezones);

        $table = $this->mdlOrm('CountriesMdl');
        $countries = $table::get();
        $this->withViewComp($view_file, 'countries', $countries);
    }

    private function setAvailable()
    {
        $res = new \StdClass();

        if (\Auth::user()->user_available) {
            $res->available_active = 'active';
            $res->available_checked = 'checked';

            $res->not_available_active = '';
            $res->not_available_checked = '';
        } else {
            $res->available_active = '';
            $res->available_checked = '';

            $res->not_available_active = 'active';
            $res->not_available_checked = 'checked';
        }


        return $res;
    }

    public function postData(Request $request)
    {
        $proc = $request->get('proc');
        if ($proc == 'avail') {
            $res = $this->procAvail($request);
        } else if ($proc == 'photo') {
            $res = $this->procPhoto($request);
        } else if ($proc == 'location') {
            $res = $this->procLocation($request);
        } else {
            $res = $this->procInfo($request);
        }

        return response()->json($res);
    }

    private function procPhoto($request)
    {
        $stat = 1;
        $msg = '/user/setting/profile';

        $validator = \Validator::make(
            $request->all(),
            Array(
                'file' => 'mimes:jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF',
            )
        );

        $ch_valid = $this->chValid($validator);

        if (!empty($ch_valid)) {
            $stat = 0;
            $msg = $ch_valid;

            foreach ($msg as $key => $val) {
                if ($val['name'] = 'file') {
                    $msg[$key]['name'] = 'file_name';
                }
            }
        } else {
            $user = \Auth::user();

            $file = $request->file('file');
            if ($file) {
                if ($file->isValid()) {
                    $img = \Image::make($file->getRealPath());
                    $img2 = \Image::make($file->getRealPath());
 
                    $user->user_photo_100 = $img->fit(100, 100)->encode('data-url');
                    $user->user_photo_64 = $img2->fit(64, 64)->encode('data-url');
                    //$user->user_photo_100 = $img
                        //->heighten(100, function ($constraint) { $constraint->upsize(); })
                        //->widen(100, function ($constraint) { $constraint->upsize(); })
                        //->encode('data-url');

                } else {
                    $stat = 0;
                    $msg = Array(
                        Array(
                            'name' => 'file_name',
                            'error' => $file->getErrorMessage(),
                        ),
                    );
                }
            }

            if ($request->get('delete')) {
                $user->user_photo_100 = '';
                $user->user_photo_64 = '';
            }

            $user->save();
            
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;

        return $res;
    }

    private function procAvail($request)
    {
        $user = \Auth::user();

        $stat = 6;
        $msg = 'saved';

        $available = $request->get('available');
        if ($available != 1) $available = 0;

        $user->user_available = $available;

        $user->save();

        $res['status'] = $stat;
        $res['msg'] = $msg;

        return $res;
    }

    private function procInfo($request)
    {
        $user = \Auth::user();

        $stat = 6;
        $msg = 'saved';

        $user->user_title = $request->get('user_title');
        $user->user_overview = $request->get('user_overview');
        $user->user_rate = $request->get('user_rate');

        $user->save();

        $this->ctl('SkillsCtl')->saveSkillsUser(\Auth::user()->id, $request->get('skills'));

        $res['status'] = $stat;
        $res['msg'] = $msg;

        return $res;
    }

    private function procLocation($request)
    {
        $user = \Auth::user();
        $save = false;

        $stat = 6;
        $msg = 'saved';


        if ($request->get('timezone')) {
            $table = $this->mdlOrm('TimezonesMdl');
            $info = $table::find($request->get('timezone'));

            if (!empty($info)) {
                $save = true;
                $user->timezone_id = $info->id;
            }
        }

        if ($request->get('country')) {
            $table = $this->mdlOrm('CountriesMdl');
            $info = $table::find($request->get('country'));

            if (!empty($info)) {
                $save = true;
                $user->country_id = $info->id;
            }
        }

        if ($request->get('city')) {
            $save = true;
            $user->user_city = $request->get('city');
        }

        if ($save) $user->save();

        $res['status'] = $stat;
        $res['msg'] = $msg;

        return $res;
    }
}
