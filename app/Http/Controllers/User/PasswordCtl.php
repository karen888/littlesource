<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PasswordCtl extends Controller {

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {

    }

    public function postData(Request $request)
    {
        $user = \Auth::user();

        $stat = 7;
        $msg = 'saved';


        $validator = \Validator::make(
            $request->all(),
            Array(
                'old_password' => 'required',
                'password' => 'required|confirmed',
            )
        );

        $ch_valid = $this->chValid($validator);

        if (!empty($ch_valid)) {
            $stat = 0;
            $msg = $ch_valid;
        } else {
            if (!\Hash::check($request->get('old_password'), $user->password)) {
                $stat = 0;
                $msg = Array(
                    Array(
                        'name' => 'old_password',
                        'error' => 'Your old password does not match',
                    ),
                );
            } else {
                $user->password = \Hash::make($request->get('password'));
                $user->save();
            }
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }

}

