<?php namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DepositAddCtl extends Controller {

    protected $info_item;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $this->chEdit();

        $this->withViewComp($view_file, 'data_info', $this->info_item);

        $countries = $this->mdlOrm('CountriesMdl');
        $countries = $countries->all();
        $this->withViewComp($view_file, 'countries', $countries);

        $this->withViewComp($view_file, 'expdate_list', $this->expDate());
        $this->withViewComp($view_file, 'card_type_list', $this->cardType());
    }

    private function chEdit()
    {
        $res = new \StdClass();
        $res->id = 0;
        $res->card_type = '';
        $res->expdate_month = '';
        $res->expdate_year = '';
        $res->card_country_code = '';

        $id = \Route::input('three');
        if ($id) {
            $table = $this->mdlOrm('CardsMdl');
            $card_info = $table::where('user_id', \Auth::user()->id)->where('id', $id)->first();
            if (!empty($card_info)) {
                $card_info->expdate_month = substr($card_info->card_expdate, 0, 2);
                $card_info->expdate_year = substr($card_info->card_expdate, 2, 6);

                $res = $card_info;
            }
        }

        $this->info_item = $res;
    }

    private function cardType($card_type_post = null)
    {
        $res = Array(
            'Visa',
            'MasterCard',
            'Discover',
            'Amex',
            'JCB',
            'Maestro',
        );

        if ($card_type_post !== null) {
            $res_ch = 'Visa';
            if (in_array($card_type_post, $res)) {
                $res_ch = $card_type_post;
            }

            $res = $res_ch;
        }

        return $res;
    }

    private function expDate($month_post = null, $year_post = null)
    {
        $res = new \StdClass();
        $res->months = Array(
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'June',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        );
        $res->years = Array();

        $year = \Carbon\Carbon::now()->year;
        for ($i = $year; $i <= $year+20; $i++) {
            $res->years[] = $i;
        }

        // checking $_post data
        if (($month_post !== null) && ($year_post != null)) {
            $res_ch = new \StdClass();
            $res_ch->month = 01;
            $res_ch->year = $year;

            foreach ($res->months as $key => $val) {
                if ($month_post == $key) {
                    $res_ch->month = $key;
                    break;
                }
            }

            if (in_array($year_post, $res->years)) {
                $res_ch->year = $year_post;
            } 

            $res = $res_ch;
        }

        return $res;
    }

    public function postData($request)
    {
        $stat = 1;
        $msg = routeClear('user_setting').'/deposit';
        $this->chEdit();

        \Validator::extend('ch_card_number', function($attribute, $value, $parameters) {
            $card = \CreditCard::validCreditCard($value);
            if ($card['valid'] == 1) return true;
            return false;
        });

        $validator = \Validator::make(
            $request->all(),
            Array(
                'description' => 'required|min:3',
                'firstname' => 'required',
                'lastname' => 'required',
                'card_number' => 'required|integer|ch_card_number',
                'cvv2' => 'required|integer|numeric|digits_between:3,4',
                'country' => 'required',
                'address1' => 'required',
                'city' => 'required',
                'zip_code' => 'required|integer',
                'expdate_month' => 'required|not_in:0',
                'expdate_year' => 'required|not_in:0',
                'country' => 'required|not_in:0',
                'card_type' => 'required|not_in:0',
            ),
            Array (
                'card_number.ch_card_number' => 'error',
            )
        );

        $ch_valid = $this->chValid($validator);

        if (!empty($ch_valid)) {
            $stat = 0;
            $msg = $ch_valid;
        } else {
            $table = $this->mdlOrm('CardsMdl');
            if ($this->info_item->id) $table = $table::find($this->info_item->id);

            $country = $this->mdlOrm('CountriesMdl');
            $country = $country::where('country_code', $request->get('country'))->first();

            if (empty($country)) {
                $country = new \StdClass();
                $country->country_code = 'AF';
            }
            $expdate = $this->expDate($request->get('expdate_month'), $request->get('expdate_year'));

            $table->user_id = \Auth::user()->id;
            $table->card_description = $request->get('description');
            $table->card_firstname = $request->get('firstname');
            $table->card_lastname = $request->get('lastname');
            $table->card_number = $request->get('card_number');
            $table->card_cvv2 = $request->get('cvv2');
            $table->card_country_code = $country->country_code;
            $table->card_address1 = $request->get('address1');
            $table->card_address2 = $request->get('address2');
            $table->card_city = $request->get('city');
            $table->card_zip_code = $request->get('zip_code');
            $table->card_expdate = $expdate->month.$expdate->year;
            $table->card_type = $this->cardType($request->get('card_type'));

            $url = parse_url(\Request::url());
            $url = $url['host'];

            $data_pay['amount'] = '10';
            $data_pay['name'] = $url.' temporary charge.';
            $data_pay['description'] = 'Temporary charge to verify your card.';
            $data_pay['refund'] = 1;
            $payment_err = $this->ctl('PaymentsCtl')->make($table, $data_pay);
            if ($payment_err) {
                $stat = 2;
                $msg = $payment_err;
            } else {
                $table->card_status = 1;
                $table->save();
            }

        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
