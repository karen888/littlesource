<?php

namespace App\Http\Controllers\Auth;

use App\Entities\Country\Country;
use App\Http\Models\Location;
use Auth;
use DB;
use Illuminate\Support\Facades\Event;
use App\Events\CreateAccount;
use App\Http\Traits\MailTraits;
use App\Http\Traits\ResponsesTrait;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Models\UserInfo;
use App\Entities\Employee\EmployeeInfo;
use App\Entities\Notifications\Notifications;
use App\Entities\SecurityQuestion\SecurityQuestionTypes;
use App\Entities\SecurityQuestion\SecurityQuestions;
use App\User;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\RegisterCheckRequest;
use App\Http\Requests\Auth\RegisterResendRequest;
use App\Entities\Criteria\Base as Criteria;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use MailTraits;
    use AuthenticatesAndRegistersUsers;
    use ResponsesTrait;
    use \App\Http\Traits\GeolocationTrait;

    /**
     * Create a new authentication controller instance.
     *
     * @param  \Illuminate\Contracts\Auth\Guard  $auth
     * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
     */
    public function __construct(Guard $auth, Registrar $registrar)
    {
        $this->auth = $auth;
        $this->registrar = $registrar;

        $this->middleware('guest', ['except' => array('getLogout', 'getActivateAccount','getSet')]);
    }

    public function getLogin()
    {
        $this->setView('auth.login');

        $this->setMenu();
        return $this->view;
    }

    public function getSet()
    {
        $this->setView('auth.set');

        $this->setMenu();
        return $this->view;
    }

    public function getRegister()
    {
        $this->setView('auth.register');
        $this->setMenu();

        $data['security_questions'] = SecurityQuestionTypes::getCustomSelectList();
        $data['care_type'] = $this->getCareTypes();

        if(\App::environment() == "staging") {
            $data['role'] = (isset($_GET['role']) && !empty($_GET['role'])) ? $_GET['role'] : 'parent';
            $data['parent'] = ($data['role'] == 'parent') ? 'block' : 'none';
            $data['caregiver'] = ($data['role'] == 'caregiver') ? 'block' : 'none';
        } else {
            $data['role'] = 'caregiver';
            $data['parent'] = 'none';
            $data['caregiver'] = 'block';
        }


        // TODO: change back when parent is allowed
//        $data['role'] = 'caregiver';
//        $data['parent'] = 'none';
//        $data['caregiver'] = 'block';

        return $this->view->with($data);

    }

    public function getCareTypes(){
        $type = $user = \DB::table('criterias')->select('name', 'id')->where('type', 'App\Entities\Criteria\CareType')->get();
        $result = array();
        foreach ($type as $key => $value) {
            $result[$key]['value'] = $value->id;
            $result[$key]['text'] = $value->name;
        }

        return $result;
    }
    public function postCheckRegister(RegisterCheckRequest $request) {
        return $this->setResponse();
    }

    public function postResend(RegisterResendRequest $request){
        $user = User::find((int)$request->id);
        if($request->has('email')){
            $user->email = $request->input('email');
            $user->save();
        }
        Event::fire(new CreateAccount($user));

        return [
            'id' => $user->id,
            'email'=>$user->email,
        ];
    }

    public function postRegister(RegisterRequest $request)
    {
        /*if ($request->role == 'caregiver') {
            $this->validate($request, [
//                'care_type' => 'required',
                'gender' => 'required|min:3',
            ]);
        }

        $location = $this->getGeoDataByPostcode($request->post_code);

        if (empty($location->results)) {
            return $this->responseValidationOld([
                'post_code' => ['The post code is invalid']
            ]);
        }*/
        $pass = Hash::make(str_random(8));
        $userData = array();
        $userData['first_name'] = $request->all()['first_name'];
        $userData['last_name'] = $request->all()['last_name'];
        $userData['email'] = $request->all()['email'];
        $userData['password'] = $pass;


        $user = $this->registrar->create($userData);
        //dd($user);
/*        if ($request->role == 'caregiver') {
            $criterias = $request->get('criterias', []);
            $user->user_criteria()->sync($criterias);
        }*/
        //dd($user);
        $userInfo = new UserInfo;
        $userInfo->user()->associate($user);
        /*$user->location()->save(new Location([
            'post_code' => $request->post_code,
            'country_id' => Country::where('name', 'Australia')->first()->id,
            'latitude' => $location->results[0]->geometry->location->lat,
            'longitude' => $location->results[0]->geometry->location->lng,
        ]));*/

        $userInfo->save();

        $md5 = $user->id . $user->email.time();
        $user->user_path = md5($md5);
        $user->activation_code = md5(md5($md5));

        switch($request->role) {
            case 'caregiver':
                $user->is_caregiver = 1;
                $employeeInfo = new EmployeeInfo;
                $employeeInfo->user_id = $user->id;
                $employeeInfo->save();
            break;
        }

        if ($request->role != 'caregiver') {
            // Disable for now, pending direction from Product re: whether or not to keep this for Parent Reg.
            // SecurityQuestions::add($request->security_questions, $user->id, $request->security_questions_answer);
        }

        $user->save();

        $notif = new Notifications();
        $notif->user_id = $user->id;
        $notif->send_when_offline = true;
        $notif->proposal_received = true;
        $notif->interview_accepted = true;
        $notif->interview_offer_declined_withdrawn = true;
        $notif->offer_accepted = true;
        $notif->job_expire_soon = true;
        $notif->job_expired = true;
        $notif->interview_initiated = true;
        $notif->offer_interview_received = true;
        $notif->offer_interview_withdrawn = true;
        $notif->proposal_rejected = true;
        $notif->job_applied_modified_canceled = true;
        $notif->hire_made_contract_begins = true;
        $notif->contract_ends = true;
        $notif->feedback_made= true;
        $notif->contract_automatically_paused = true;
        $notif->tip_help_start = true;
        $notif->save();
//        Notifications::create(['user_id' => $user->id]);

        Event::fire(new CreateAccount($user));

        return [
            'id' => $user->id,
            'email'=>$user->email,
        ];
    }

    public function postLogin(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if ($this->auth->attempt($credentials, $request->has('remember'))) {
            if (!Auth::user()->user_available) {
                Auth::logout();
                return $this->responseValidationOld(
                    ['account' => 'Account not activated.']
                );
            } else {
                if(Notifications::where('user_id', '=', Auth::user()->id)->exists() === false) {
                    $notif = new Notifications();
                    $notif->user_id = Auth::user()->id;
                    $notif->send_when_offline = true;
                    $notif->proposal_received = true;
                    $notif->interview_accepted = true;
                    $notif->interview_offer_declined_withdrawn = true;
                    $notif->offer_accepted = true;
                    $notif->job_expire_soon = true;
                    $notif->job_expired = true;
                    $notif->interview_initiated = true;
                    $notif->offer_interview_received = true;
                    $notif->offer_interview_withdrawn = true;
                    $notif->proposal_rejected = true;
                    $notif->job_applied_modified_canceled = true;
                    $notif->hire_made_contract_begins = true;
                    $notif->contract_ends = true;
                    $notif->feedback_made= true;
                    $notif->contract_automatically_paused = true;
                    $notif->tip_help_start = true;
                    $notif->save();
                }

                return $this->setResponse();
            }
        } else {
            return $this->responseValidationOld(
                ['account' => 'These credentials do not match our records.']
            );
        }
    }

    public function getActivateAccount($code)
    {
        $route = Auth::guest() ? '/auth/set' : '/caregiver/dashboard';
        $user = User::where(['activation_code' => $code])->first();
        DB::table('users')
                ->where(['activation_code' => $code])
                ->update(['activation_code' => '',
                        'user_available' => 1]);
        Auth::login($user);

        $handle = $user ? $code : 'Activation code not found!';

        return redirect($route)->with('handle', $handle);
    }

}

