<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\Http\Traits\ResponsesTrait;
use DB;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords {
        postEmail as traitPostEmail;
        postReset as traitPostReset;
    }

    use ResponsesTrait;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
		$this->auth = $auth;
		$this->passwords = $passwords;

		$this->middleware('guest',['except' => array('postSet')]);
	}

	public function getEmail()
	{
        $this->setView('auth.password');

        $this->setMenu();

        return $this->view;
	}

	public function getReset($token = null)
	{
		if (is_null($token))
		{
			throw new NotFoundHttpException;
		}

        $this->setView('auth.reset');
        $this->view->with('token', $token);

        $this->setMenu();
        return $this->view;
	}

    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);

        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        return $this->getApropriateResponse($response);
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:8',
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

        return $this->getApropriateResponse($response);
    }

    public function postSet(Request $request)
    {
        $this->validate($request, [
            'user_type' => 'required',
            'password' => 'required|confirmed|min:8',
        ]);

        $user = User::where(['email' => Auth::user()->email])->first();
        if ($user !== null) {
            $user->password = bcrypt($request->input('password'));
            $user->save();
            DB::table('users')
                ->where('id', $user->id)
                ->update(['is_caregiver' => $request->input('user_type')]);
            $route = ($request->input('user_type') == 0) ? '/parent/dashboard' : '/caregiver/dashboard';
            return $route;
        }
        else{
            return   $this->getApropriateResponse('');
        }

    }

    private function getApropriateResponse($response){
        switch ($response) {
            case Password::PASSWORD_RESET:
            case Password::RESET_LINK_SENT:
                return trans($response);
            default:
                return response(trans($response), 422);
        }
    }

}
