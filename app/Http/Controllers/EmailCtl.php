<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmailCtl extends Controller {

    protected $user_data;
    protected $sendgrid = null;

    public function msgFeedback($user_data, $data)
    {
        $view = 'emails.msg_feedback';
        $data_mail['title'] = 'Feedback: '.$data['job_title'];
        $this->chData($user_data, $data, $view, $data_mail);
    }

    public function msgInvite($user_data, $data)
    {
        $view = 'emails.msg_invite';
        $data_mail['title'] = 'Invite: '.$data['job_title'];
        $this->chData($user_data, $data, $view, $data_mail);
    }

    public function msgOffer($user_data, $data)
    {
        $view = 'emails.msg_offer';
        $data_mail['title'] = 'Offer: '.$data['job_title'];
        $this->chData($user_data, $data, $view, $data_mail);
    }

    public function msgNew($user_data, $data)
    {
        $view = 'emails.msg_new';
        $data_mail['title'] = 'New message: '.$data['job_title'];
        $this->chData($user_data, $data, $view, $data_mail);
    }

    public function msgJob($user_data, $data)
    {
        $view = 'emails.msg_job';
        $data_mail['title'] = 'Job Applicant Update: '.$data['job_title'];
        $this->chData($user_data, $data, $view, $data_mail);
    }

    private function chData($user_data, $data, $view, $data_mail = Array())
    {
        if (is_numeric($user_data)) $user_data = \App\User::find($user_data);
        if (!empty($user_data)) {
            $this->sent($user_data, $data, $view, $data_mail);
        }
    }

    private function sent($user_data, $data, $view, $data_mail = Array())
    {
        $this->chVars();

        if (!isset($data_mail['title'])) {
            $url = parse_url(\Request::url());
            $data_mail['title'] = 'E-Mail from '.$url['host'];
        }
        // set view data
        $view = view($view);
        foreach ($data as $key => $val) {
            $view->with($key, $val);
        }
        $view = $view->render();

        $email = new \SendGrid\Email();
        $email
            ->addTo($user_data->email, $user_data->name)
            ->setFrom(\Config::get('sendgrid.from_email'))
            ->setFromName(\Config::get('sendgrid.from_name'))
            ->setSubject($data_mail['title'])
            ->setHtml($view);
        $this->sendgrid->send($email);
        //\Mail::send($view, $data, function($message) use ($user_data, $data_mail) {
            //$message->to($user_data->email, $user_data->name)->subject($data_mail['title']);
        //});
    }

    private function chVars()
    {
        if (!$this->sendgrid) {
            $this->sendgrid = new \SendGrid(\Config::get('sendgrid.username'), \Config::get('sendgrid.password'));
        }

    }
}
