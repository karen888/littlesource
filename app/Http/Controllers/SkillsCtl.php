<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SkillsCtl extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function postAjaxList(Request $request)
    {
        $list = Array();

        $skill = $request->get('skill');

        if ($skill) {
            $list = $this->mdl('Skills.SkillsMdl')->searchSkill($skill);
        }

        return response()->json($list);
    }

    public function saveSkillsUser($user_id, $skills_id)
    {
        $table = $this->mdlOrm('Skills.UsersMdl');
        $table_del = $table::where('user_id', $user_id)->delete();

        if ($skills_id) {
            $data = Array();
            $date = \Carbon\Carbon::now()->toDateTimeString();

            foreach($skills_id as $val) {
                $data_tmp['user_id'] = $user_id;
                $data_tmp['skill_id'] = $val;
                $data_tmp['created_at'] = $date;
                $data_tmp['updated_at'] = $date;

                $data[] = $data_tmp;
            }

            $table::insert($data);
        }
    }

    public function saveSkillsJob($job_id, $skills_id)
    {
        $table = $this->mdlOrm('Skills.JobsMdl');
        $table_del = $table::where('job_id', $job_id)->delete();

        if ($skills_id) {
            $data = Array();
            $date = \Carbon\Carbon::now()->toDateTimeString();

            foreach($skills_id as $val) {
                $data_tmp['job_id'] = $job_id;
                $data_tmp['skill_id'] = $val;
                $data_tmp['created_at'] = $date;
                $data_tmp['updated_at'] = $date;

                $data[] = $data_tmp;
            }

            $table::insert($data);
        }
    }
}
