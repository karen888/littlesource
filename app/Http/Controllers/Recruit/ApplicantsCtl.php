<?php namespace App\Http\Controllers\Recruit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApplicantsCtl extends Controller {
    
    protected $view_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $job_id = \Input::get('id');

        $appls = $this->getApplicants($job_id);
        $this->withViewComp($view_file, 'appls_list', $appls);
        $this->withViewComp($view_file, 'job_id', $job_id);

    }

    private function getApplicants($job_id)
    {
        $res = Array();

        if ($job_id) {
            $job_info = $this->mdl('Jobs.JobsMdl')->getInfoJob($job_id, \Auth::user()->id);
            if (!empty($job_info)) {
                $res = $this->mdl('Jobs.AppliesMdl')->getApplicants($job_info->id);
                if (!empty($res)) {
                    $user_info_ctl = $this->ctl('Common.UserInfoCtl');
                    foreach ($res as $val) {
                        $val->rate = $user_info_ctl->getRate($val->user_id);
                    }
                }
            }
        }

        return $res;
    }

    public function postData($request)
    {
        $stat = 4;
        $msg = '';

        $validator = \Validator::make(
            $request->all(),
            Array(
                'message' => 'required|min:10',
            )
        );

        $ch_valid = $this->chValid($validator);

        if (!empty($ch_valid)) {
            $stat = 0;
            $msg = $ch_valid;
        } else {
            $job_info = $this->mdl('Jobs.JobsMdl')->getInfoJob($request->get('job_id', \Auth::user()->id));
            if (!empty($job_info)) {
                //$msgs = $this->mdlOrm('MessagesMdl');
                //$msgs->job_id = $job_info->id;
                //$msgs->from_user_id = \Auth::user()->id;
                //$msgs->to_user_id  = $request->get('user_id');
                //$msgs->message = $request->get('message');

                //$msgs->save();
                $data_sql = Array();
                $data_sql['message'] = $request->get('message');

                $this->ctl('Messages.ActionsCtl')->createMessage(\Auth::user()->id, $request->get('user_id'), $data_sql, $job_info->id);
                
                $data['job_title'] = $job_info->title;
                $data['message_text'] = $data_sql['message'];
                $data['author_info'] = \Auth::user();
                $this->ctl('EmailCtl')->msgNew($request->get('user_id'), $data);
            }

            $stat = 3;
            $data['name'] = 'closeApplModal';
            $data['val'] = '';
            $msg = Array($data);
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
