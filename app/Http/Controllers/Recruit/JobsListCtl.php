<?php namespace App\Http\Controllers\Recruit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class JobsListCtl extends Controller {
    
    protected $view_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $list = $this->getJobs();
        $this->withViewComp($view_file, 'jobs_list', $list);
    }

    private function getJobs()
    {
        $list = $this->mdl('Jobs.JobsMdl')->getJobsByUserId(\Auth::user()->id);
        if (!empty($list)) {
            $uri = '/'.str_replace('jobs-list', 'post-job', \Request::path());
            $uri_appl = '/'.str_replace('jobs-list', 'applicants', \Request::path());

            foreach ($list as $key => $val) {
                $val->created_human = convToTimeZone($val->created_at)->diffForHumans();
                $val->link_edit = $uri.'/edit?id='.$val->id;
                $val->link_delete = '/'.\Request::path().'?del_id='.$val->id;
                $val->link_applicants = $uri_appl.'?id='.$val->id;
            }
        }

        return $list;
    }

    public function postData($request)
    {
        $res = null;

        $proc = $request->get('proc');
        if ($proc == 'del') {
            $res = $this->postDel($request);
        }

        return $res;
    }

    private function postDel($request)
    {
        $stat = 3;
        $msg = Array();

        $data_msg['name'] = 'delJobsList';
        $data_msg['val'] = '';
        $msg[] = $data_msg;


        $job_id = $request->get('id');
        if ($job_id) {
            $this->delJob($job_id, \Auth::user()->id);
            //$job_info = $this->mdl('Jobs.JobsMdl')->getInfoJob($job_id, \Auth::user()->id);
            //if (!empty($job_info)) {
                //$mdl = $this->mdl('Jobs.JobsMdl');
                //$mdl = $mdl::destroy($job_info->id);

                //$mdl = $this->mdl('Jobs.AppliesMdl');
                //$mdl = $mdl::where('job_id', $job_info->id)->delete();
            //} else {
                //$stat = 4;
                //$msg = 'You are a bad guy!';
            //}
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }

    private function delJob($job_id, $user_id)
    {
        $res = Array();

        $job_info = $this->mdl('Jobs.JobsMdl')->getInfoJob($job_id, $user_id);
        if (!empty($job_info)) {
            // deleting from jobs table
            $mdl = $this->mdl('Jobs.JobsMdl');
            $mdl = $mdl::destroy($job_info->id);

            // deleting from job_applies table
            $mdl = $this->mdl('Jobs.AppliesMdl');
            $mdl = $mdl::where('job_id', $job_info->id)->delete();

            // deleting from messages_threads, messages tables
            $mdl_mt = $this->mdlOrm('MessagesThreadsMdl');
            $mdl_mt = $mdl_mt::where('job_id', $job_info->id);
            $mdl_mt_res = $mdl_mt->get();
            if (!empty($mdl_mt_res)) {
                $messages_ids = Array();
                foreach($mdl_mt_res as $val) {
                    $messages_ids[] = $val->id;
                }

                $mdl = $this->mdlOrm('MessagesMdl');
                $mdl = $mdl::whereIn('message_thread_id', $messages_ids)->delete();

                $mdl_mt->delete();
            }

            // deleting from offers table
            $mdl = $this->mdlOrm('OffersMdl');
            $mdl = $mdl::where('job_id', $job_id)->delete();

            // deleting from files table
            $mdl = $this->mdlOrm('FilesMdl');
            $mdl::where('job_id', $job_id)->delete();
        }


    }
}
