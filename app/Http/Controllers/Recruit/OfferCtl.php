<?php namespace App\Http\Controllers\Recruit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OfferCtl extends Controller {
    
    protected $view_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $data = $this->getInfoData();

        $this->withViewComp($view_file, 'offer_info', $data);
    }

    public function getInfoData($request = null)
    {
        $res = new \StdClass();
        $res->user = Array();
        $res->sent_offer = false;
        $res->job_apply_active = 1;

        if ($request) {
            //$user_id = $request->get('user');
            //$job_id = $request->get('job');

            $job_apply_id = $request->get('job_apply_id');
        } else {
            //$user_id = (int) \Route::input('one');
            //$job_id = (int) \Route::input('two');

            $job_apply_id = \Route::input('one');
        }

        if ($job_apply_id) {
            $ch_offer = $this->mdl('OffersMdl')->chOfferByApply($job_apply_id, \Auth::user()->id);

            if (empty($ch_offer)) {
                $res->job_apply = $this->mdl('Jobs.AppliesMdl')->chAppliedByFromUser($job_apply_id, \Auth::user()->id);

                //print_r($ch_offer);
                if (!empty($res->job_apply) && ($res->job_apply->active == 1)) {

                    if (!empty($res->job_apply->title)) {
                        // if hourly price
                        if ($res->job_apply->job_apply_fixed_price == 0) {
                            $res->cards_list = $this->mdl('CardsMdl')->getList(\Auth::user()->id);
                        }

                        $res->user = \App\User::find($res->job_apply->user_id);
                    }
                }
            } else $res->sent_offer = true;
        }

        return $res;
    }

    public function postData($request)
    {
        $stat = 1;
        $msg = routeClear('home').'/recruit/jobs-list';

        $info_offer = $this->getInfoData($request);

        if (!empty($info_offer->user)) {
            // if hourly price
            if ($info_offer->job_apply->job_apply_fixed_price == 0) {
                $valid_rule = Array(
                    'offer_text' => 'required|min:5',
                    'card' => 'required|not_in:0|card_id',
                );
                $valid_errs = Array(
                    'card.card_id' => '',
                );
            } else {
                $valid_rule = Array(
                    'offer_text' => 'required|min:5',
                );
                $valid_errs = Array();
            }


            $validator = \Validator::make(
                $request->all(),
                $valid_rule,
                $valid_errs
            );

            $ch_valid = $this->chValid($validator);

            if (!empty($ch_valid)) {
                $stat = 0;
                $msg = $ch_valid;
            } else {
                $table = $this->mdlOrm('OffersMdl');

                // if hourly price
                if ($info_offer->job_apply->job_apply_fixed_price == 0) {
                    $table->card_id = $request->get('card');
                }

                $table->job_apply_id = $request->get('job_apply_id');
                $table->job_id = $info_offer->job_apply->job_id;
                $table->from_user_id = \Auth::user()->id;
                $table->to_user_id = $info_offer->user->id;
                $table->offer_text = $request->get('offer_text');

                $table->save();

                $data_msg['offer_id'] = $table->id;
                $data_msg['message'] = 'Offer';
                $this->ctl('Messages.ActionsCtl')->createMessage(\Auth::user()->id, $info_offer->user->id, $data_msg, $info_offer->job_apply->job_id);

                // email
                $data['job_title'] = $info_offer->job_apply->title;
                $data['offer_text'] = $request->get('offer_text');
                $data['offer_link'] = routeClear('home_offer').'/'.$table->id;
                $data['author_info'] = \Auth::user();
                $this->ctl('EmailCtl')->msgOffer($info_offer->user->id, $data);


                // notification
                $url_link = routeClear('home_offer', Array(), false).'/'.$table->id;
                $this->ctl('Home.NotificationsMsgsCtl')->offer($info_offer->user->id, $info_offer->job_apply->job_id, $url_link);
            }
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
