<?php namespace App\Http\Controllers\Recruit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InviteCtl extends Controller {
    
    protected $view_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $data_info = $this->setInfoData();
        $this->withViewComp($view_file, 'data_info', $data_info);
    }

    public function setInfoData($request = null)
    {
        $res = new \StdClass();

        if ($request) {
            $user_id = $request->get('user');
        } else {
            $user_id = (int) \Route::input('one');
        }

        $user_info = \App\User::where('id', $user_id)->where('user_available', 1)->first();
        if (!empty($user_info)) {
            $res->user = $user_info;

            $table = $this->mdl('Jobs.JobsMdl');
            $res->jobs_list = $table::where('user_id', \Auth::user()->id)->get();

        }

        return $res;
    }

    public function postData(Request $request)
    {
        $stat = 1;
        $msg = routeClear('home').'/';

        $data_info = $this->setInfoData($request);
        if (!empty($data_info)) {


            \Validator::extend('own_job', function($attribute, $value, $parameters) {
                $table = $this->mdlOrm('Jobs.JobsMdl');
                $res = $table::where('id', $value)->where('user_id', \Auth::user()->id)->first();
                if (empty($res)) $res = false;
                else $res = true;

                return $res;
            });

            \Validator::extend('user_applied', function($attribute, $value, $parameters) {
            });

            $validator = \Validator::make(
                $request->all(),
                Array(
                    'message' => 'required|min:5',
                    'job_id' => 'required|own_job',
                ),
                Array(
                    'job_id.own_job' => 'This is not your job!',
                )
            );


            $ch_valid = $this->chValid($validator);

            if (!empty($ch_valid)) {
                $stat = 0;
                $msg = $ch_valid;
            } else {
                $ch_appl = $this->mdl('Jobs.AppliesMdl')->chAppliedByUser($request->get('job_id'), $request->get('user'));
                if (!empty($ch_appl)) {
                    $stat = 2;
                    $msg = 'This user already applied to this job.';
                } else {
                    $table = $this->mdlOrm('Jobs.AppliesMdl');

                    $table->from_user_id = \Auth::user()->id;
                    $table->job_id = $request->get('job_id');
                    $table->user_id = $request->get('user');
                    $table->initiated_by_fr = 0;
                    $table->cover_letter_employer = $request->get('message');

                    $table->save();

                    $invite_id = $table->id;

                    $data_msg['message'] = 'Invite';
                    $data_msg['invite_id'] = $invite_id;
                    $this->ctl('Messages.ActionsCtl')->createMessage(
                        \Auth::user()->id, 
                        $request->get('user'), 
                        $data_msg, 
                        $request->get('job_id')
                    );

                    $table = $this->mdlOrm('Jobs.JobsMdl');
                    $data['job_title'] = $table::find($request->get('job_id'))->title;
                    $data['invite_id'] = $invite_id;
                    $this->ctl('EmailCtl')->msgInvite($data_info->user, $data);

                    $this->ctl('Home.NotificationsMsgsCtl')->invite($data_info->user->id, $request->get('job_id'), routeClear('home_invite').'/'.$invite_id);
                }
            }
        }
        
        
        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }
}
