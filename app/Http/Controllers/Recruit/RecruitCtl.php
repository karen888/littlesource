<?php namespace App\Http\Controllers\Recruit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RecruitCtl extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
    }

    public function getApplicants()
    {
        $view = 'recruit.applicants';
        $ctl = 'Recruit.ApplicantsCtl';

        return $this->setVC($view, $ctl);
    }

    public function postApplicants(Request $request)
    {
        return $this->ctl('Recruit.ApplicantsCtl')->postData($request);
    }

    public function getPostJob()
    {
        $view = 'recruit.post_job';
        $ctl = 'Recruit.PostJobCtl';

        return $this->setVC($view, $ctl);
    }

    public function postPostJob(Request $request)
    {
        return $this->ctl('Recruit.PostJobCtl')->postData($request);
    }

    public function getJobsList()
    {
        $view = 'recruit.jobs_list';
        $ctl = 'Recruit.JobsListCtl';

        return $this->setVC($view, $ctl);
    }

    public function postJobsList(Request $request)
    {
        return $this->ctl('Recruit.JobsListCtl')->postData($request);
    }

    public function getFindFreelancer()
    {
        $view = 'recruit.find_freelancer';
        $ctl = 'Recruit.FindFreelancerCtl';

        return $this->setVC($view, $ctl);
    }

    public function getOffer()
    {
        $view = 'recruit.offer';
        $ctl = 'Recruit.OfferCtl';

        return $this->setVC($view, $ctl);
    }

    public function postOffer(Request $request)
    {
        return $this->ctl('Recruit.OfferCtl')->postData($request);
    }

    public function getOffers()
    {
        $view = 'recruit.offers';
        $ctl = 'Recruit.OffersCtl';

        return $this->setVC($view, $ctl);
    }

    public function postOffers(Request $request)
    {
        return $this->ctl('Recruit.OffersCtl')->postData($request);
    }

    public function getInvite()
    {
        $view = 'recruit.invite';
        $ctl = 'Recruit.InviteCtl';

        return $this->setVC($view, $ctl);
    }

    public function postInvite(Request $request)
    {
        return $this->ctl('Recruit.InviteCtl')->postData($request);
    }
}
