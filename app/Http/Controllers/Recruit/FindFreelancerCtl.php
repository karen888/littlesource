<?php namespace App\Http\Controllers\Recruit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FindFreelancerCtl extends Controller {
    
    protected $view_sub_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $this->view_sub_file = $view_file;
        $res = $this->findRes();

        $this->withViewComp($view_file, 'find_result', $res);
        
    }

    private function findRes()
    {
        $res = Array();
        $search = \Input::get('search', 'empty');

        if ($search != 'empty') {
            $data['search'] = $search;
            $res = $this->mdlOrm('UsersMdl')->search($data);

            if (!empty($res)) {
                foreach ($res as $val) {
                    $val->user_path = $this->routeClear('user_link').'/'.$val->user_path;
                    $val->user_overview = substr($val->user_overview, 0, 200).'...';

                    $user_photo = userPhoto($val);
                    $val->user_photo_64 = $user_photo->photo_64;

                    $val->skills = $this->mdl('Skills.SkillsMdl')->getSkillsByIds($val->skills_ids);
                }

            }

            
            $this->withViewComp($this->view_sub_file, 'search', $search);
        }

        return $res;
    }
}
