<?php namespace App\Http\Controllers\Recruit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostJobCtl extends Controller {
    
    protected $view_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $res = null;
        $info = new \StdClass();
        $info->category_main_id = 0;
        $info->category_sub_id = 0;
        $info->skills = Array();
        $info->type_price_hourly = 'selected';
        $info->type_price_fixed = '';
        $info->type_price_fixed_display = 'display: none;';
        $info->type_price_hourly_display = '';
        $info->job_duration = 0;
        $info->job_end_date = '';

        $list_cat_sub = $this->mdlOrm('CategoryMdl')->where('main_category', '!=', 0)->get();

        $cats = $this->ctl('CategoryCtl')->getList();
        $this->withViewComp($view_file, 'categories', $cats);

        $act = \Route::input('one');
        if ($act == 'edit') {
            $info = $this->infoJob();

        } else if ($act == 'file') {
            $res =  $this->downFile();
        }

        $this->withViewComp($view_file, 'list_cat_sub', $list_cat_sub);
        $this->withViewComp($view_file, 'info_job', $info);
        $this->withViewComp($view_file, 'durations', $this->ext('JobDuration')->getList());

        return $res;
    }

    public function downFile()
    {
        $job_id = \Input::get('id');
        if ($job_id) {
            $info = $this->mdl('Jobs.JobsMdl')->getInfoJob($job_id, \Auth::user()->id);
            if (!empty($info)) {
                $info->file = base64_decode($info->file);

                $response = \Response::make($info->file, 200);
                $response->header('Content-length', $info->file_size);
                $response->header('Content-Type', $info->file_mime);
                $response->header('Content-Disposition', 'attachment; filename='.$info->file_name);

                return $response;                
            }
        }

    }

    public function infoJob()
    {
        $res = Array();

        $job_id = \Input::get('id');
        if ($job_id) {
            $res = $this->mdl('Jobs.JobsMdl')->getInfoJob($job_id, \Auth::user()->id);
            if (!empty($res)) {
                $uri = str_replace('edit', '', \Request::path());
                $uri = $uri.'file?id='.$res->id;
                $res->download_link = $uri;

                $res->skills = $this->mdl('Skills.JobsMdl')->getListByJob($res->id);

                if ($res->job_end_date == '0000-00-00') $res->job_end_date = '';

                $res->job_fixed_price = str_replace('.00', '', $res->job_fixed_price);

                if ($res->job_fixed_price == '0') {
                    $res->job_fixed_price = '';
                    $res->type_price_hourly = 'selected';
                    $res->type_price_fixed = '';
                    $res->type_price_fixed_display = 'display: none;';
                    $res->type_price_hourly_display = '';
                } else {
                    $res->type_price_hourly = '';
                    $res->type_price_fixed = 'selected';
                    $res->type_price_fixed_display = '';
                    $res->type_price_hourly_display = 'display: none;';
                }
            }
        }

        return $res;
    }

    public function postData($request)
    {
        $stat = 1;
        $msg = '/home/recruit/jobs-list';

        $type_price = $request->get('type_price');
        \Validator::extend('ch_fixed_price', function($attribute, $value, $parameters) use ($type_price){
            if ($type_price == 'fixed') {
                if (filter_var($value, FILTER_VALIDATE_INT) !== false) return true;
                else return false;
            }

            return true;
        });
        \Validator::extend('ch_fixed_price_min', function($attribute, $value, $parameters) {
            if ($value < 5) return false;

            return true;
        });

        $validator = \Validator::make(
            $request->all(),
            Array(
                'category_main' => 'required|not_in:0',
                'category_sub' => 'required|not_in:0',
                'description' => 'required|min:10',
                'title' => 'required|min:2',
                'fixed_price' => 'required_if:type_price,fixed|ch_fixed_price|ch_fixed_price_min',
                'duration' => 'required_if:type_price,hourly',
            ),
            Array(
                'fixed_price.ch_fixed_price' => 'The fixed price must be an integer.',
                'fixed_price.ch_fixed_price_min' => 'Minimum budget is 5 US Dollars',
            )
        );

        $ch_valid = $this->chValid($validator);

        if (!empty($ch_valid)) {
            $stat = 0;
            $msg = $ch_valid;
        } else {
            $file_upl = false;
            $table = $this->mdlOrm('Jobs.JobsMdl');

            $upd_id = $request->get('edit');
            if ($upd_id) {
                $job_upd = $this->mdl('Jobs.JobsMdl')->getInfoJob($upd_id, \Auth::user()->id);
                if (!empty($job_upd)) {
                    $table = $table::find($upd_id);
                    $file_delete = $request->get('file_delete');
                    if ($file_delete) {
                        $table->file_name = '';
                        $table->file_mime = ''; 
                        $table->file_size = '';
                        $table->file = '';
                    }

                }
            } else {
                $str = \Auth::user()->id.$request->get('title').time();
                $table->path = md5($str);
            }

            $file = $request->file('file');
            if ($file) {
                if ($file->isValid()) {
                    $file_upl = true;
                    $table->file_name = $file->getClientOriginalName();
                    $table->file_mime = $file->getMimeType();
                    $table->file_size = $file->getSize();
                    $table->file = base64_encode(file_get_contents($file->getRealPath()));
                } else {
                    $stat = 0;

                    $msg = Array(
                        Array(
                            'name' => 'file',
                            'error' => $file->getErrorMessage(),
                        ),
                    );
                }
            }

            if ($stat) {
                $table->title = $request->get('title');
                $table->description = $request->get('description');
                $table->category_main_id = $request->get('category_main');
                $table->category_sub_id = $request->get('category_sub');
                $table->user_id = \Auth::user()->id;

                if ($request->get('type_price') == 'fixed') {
                    $table->job_fixed_price = $request->get('fixed_price');
                    $table->job_end_date = $request->get('end_date');
                    $table->job_duration = 0;
                } else {
                    $table->job_fixed_price = 0;
                    $table->job_end_date = '0000-00-00';
                    $table->job_duration = $this->ext('JobDuration')->chDuration($request->get('duration'));
                }

                $table->save();

                $this->ctl('SkillsCtl')->saveSkillsJob($table->id, $request->get('skills'));
            }
        }

        //$stat = 4;
        //$msg = $request->get('fixed_price');

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }

    private function getFile($file, $table)
    {

    }

}
