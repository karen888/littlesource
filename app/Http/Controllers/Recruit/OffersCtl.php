<?php namespace App\Http\Controllers\Recruit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OffersCtl extends Controller {
    
    protected $view_file;

    //$this->withViewComp($view_file, 'hhz', '5555');
    public function getData($view_file)
    {
        $offers_list = $this->mdl('OffersMdl')->getOffersForEmployer(\Auth::user()->id);

        $this->withViewComp($view_file, 'offers_list', $offers_list);
    }

    public function postData($request)
    {
        $stat = 3;
        $msg = Array();

        $data_msg['name'] = 'delTd';
        $data_msg['val'] = '';
        $msg[] = $data_msg;

        $id = $request->get('id');
        if ($id) {
            $table = $this->mdl('OffersMdl');
            $offer_info = $table::where('id', $id)->where('from_user_id', \Auth::user()->id)->where('status', 0)->first();
            if (!empty($offer_info)) {
                $offer_info->delete();
            }
        }

        $res['status'] = $stat;
        $res['msg'] = $msg;
        return response()->json($res);
    }

}
