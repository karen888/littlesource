<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FilesCtl extends Controller {

    protected $date;
    protected $data;

	public function __construct()
	{
		$this->middleware('auth');
	}

    public function getFile()
    {
        $file_id = \Route::input('one');
        if ($file_id) {
            $table = $this->mdlOrm('FilesMdl');
            $file_info = $table::find($file_id);
            if (!empty($file_info)) {
                $users_ids = Array();

                // info from jobs table
                $table = $this->mdlOrm('Jobs.JobsMdl');
                $job_info = $table::find($file_info->job_id);
                $users_ids[] = $job_info->user_id;

                // info from messages table
                $table = $this->mdlOrm('MessagesMdl');
                $message_info = $table::find($file_info->message_id);

                // info from messages threads table
                $table = $this->mdlOrm('MessagesThreadsMdl');
                //$messages_threads_info = $table::where('job_id', $file_info->job_id)->get();
                $message_thread_info = $table::find($message_info->message_thread_id);
                $message_thread_users = explode(',', $message_thread_info->users);

                $users_ids = array_merge($users_ids, $message_thread_users);

                if (in_array(\Auth::user()->id, $users_ids)) {
                    $file_info->file_file = base64_decode($file_info->file_file);

                    $response = \Response::make($file_info->file_file, 200);
                    $response->header('Content-length', $file_info->file_file_size);
                    $response->header('Content-Type', $file_info->file_file_mime);
                    $response->header('Content-Disposition', 'attachment; filename='.$file_info->file_file_name);

                    return $response;
                }
            }
        }
    }

    public function proc($file)
    {
        $res['err'] = Array();
        $res['data_sql'] = Array();

        if ($file) {
            $this->date = \Carbon\Carbon::now()->toDateTimeString();

            if (is_array($file)) {
                foreach ($file as $val) {
                    if (!empty($val)) {
                        $res_tmp = $this->validFile($val);
                        $res['data_sql'] = array_merge($res['data_sql'], $res_tmp['data_sql']);
                        if (!empty($res_tmp['err'])) {
                            $res['err'] = $res_tmp['err'];
                        }
                    }
                }
            } else $res = $this->validFile($file);
        }

        //if (empty($res['err']) && !empty($res['data_sql'])) {
            //$this->saveFile($res);
        //}
        $this->data = $res;

        return $this;
    }

    public function chErr()
    {
        if (empty($this->data['err'])) return false;
        else return true;
    }

    public function getErr()
    {
        return $this->data['err'];
    }

    public function saveFile($user_id, $job_id, $message_id, $apply_id = 0)
    {
        if (empty($this->data['err']) && !empty($this->data['data_sql'])) {
            $table = $this->mdlOrm('FilesMdl');

            foreach ($this->data['data_sql'] as $key => $val) {
                $this->data['data_sql'][$key]['user_id'] = $user_id;
                $this->data['data_sql'][$key]['message_id'] = $message_id;
                $this->data['data_sql'][$key]['job_id'] = $job_id;
                if ($apply_id) $this->data['data_sql'][$key]['job_apply_id'] = $apply_id;
            }

            $table::insert($this->data['data_sql']);
        }
    }

    private function validFile($file)
    {
        $res['err'] = Array();
        $res['data_sql'] = Array();
        
        if ($file->isValid()) {

            $data_sql['file_file_name'] = $file->getClientOriginalName();
            $data_sql['file_file_mime'] = $file->getMimeType();
            $data_sql['file_file_size'] = $file->getSize();
            $data_sql['file_file'] = base64_encode(file_get_contents($file->getRealPath()));

            $data_sql['created_at'] = $this->date;
            $data_sql['updated_at'] = $this->date;

            $res['data_sql'][] = $data_sql;
        } else {
            $res['err'] = Array(
                Array(
                    'name' => 'file',
                    'error' => $file->getErrorMessage(),
                ),
            );
        }

        return $res;
    }
}
