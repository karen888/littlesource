<?php

namespace App\Http\Controllers\Employee;

use App\Entities\Message\MessagesThread;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Entities\Job\JobDuration;
use App\Entities\Job\JobWorkload;
use App\Entities\Job\JobDeclineReason;
use App\Entities\Offer\Offers;
use App\Entities\Employee\EmployeeInfo;
use Illuminate\Database\Eloquent\Collection;

class CaregiversController extends Controller
{

    public function __construct()
    {

    }
    /**
     * Display a listing of the Hired Caregivers for Logged in Client.
     *
     * @return \Illuminate\Http\Response
     */
    public function hired()
    {
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return view('employee.hired');
    }

    /**
     * Display a listing of the Messaged Caregivers for Logged in Client.
     *
     * @return \Illuminate\Http\Response
     */
    public function messaged()
    {
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return view('employee.messaged');
    }

    /**
     * Display a listing of the Favorite Caregivers for Logged in Client.
     *
     * @return \Illuminate\Http\Response
     */
    public function favorites()
    {
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return view('employee.favorites');
    }

    /**
     * Display a listing of the Recently Viewed Caregivers for Logged in Client.
     *
     * @return \Illuminate\Http\Response
     */
    public function recentlyViewed()
    {
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return view('employee.recently-viewed', ['recently' => collect()]);
    }

    /**
     * Display the public profile of a caregiver
     *
     * // TODO: Move to own Caregiver controller.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($caregiver) {
        $employee = User::find($caregiver);

        if (empty($employee)) {
            return redirect(route('index'));
        }

        return view('employee.public-profile', ['employee' => $employee]);
    }

    /**
     * Display the form to find a caregiver
     *
     * @return \Illuminate\Http\Response
     */
    public function find() {
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        $user_info = Auth::user()->info;
        
        return view('employee.find', [
            'lat' => $user_info->lat,
            'lng' => $user_info->lng
        ]);
    }

    protected function mapCriteriaToSelect($criteria)
    {
        return $criteria->map(function($obj) {
            return [
                'value' => $obj->id,
                'name' => $obj->name, //remove this after converting to CustomSelect.vue
                'text' => $obj->name,
                'description' => $obj->description,
                'data' => $obj->data,
                'is_selected' => $obj->default
            ];
        });
    }



    /**
     * Display the form to hire a caregiver
     *
     * @return \Illuminate\Http\Response
     */
    public function hire($employee, $job_id = null) {

        $employee = User::find($employee);
        $year = date('Y');
        $cards = Auth::user()->cards;
        $jobs_items = [];

        $jobs = Auth::user()->jobs;
        $jobs = $jobs->map(function ($job) use ($employee) {
            foreach ($job->jobOffers as $itm) {
                if ($itm->status !== 'decline' && $itm->to_user_id == $employee->id) {
                    return false;
                }
            }
            return ['value' => $job->id, 'text' => $job->title];
        })->toArray();

        foreach ($jobs as $key => $itm) {
            if ($itm) {
                array_push($jobs_items, $itm);
            }
        }
        
        return view('employee.hire.form', [
            'year' => $year,
            'cards' => $cards,
            'jobs' => $jobs_items,
            'employee' => $employee,
            'job' => ($job_id) ? $job_id->id : null
        ]);
    }

    /**
     * Edit Employee profile
     *
     * @return \Illuminate\Http\Response
     */
    public function offer($offer) {
        $offer_category = [];
        $offer = Offers::find((int)$offer);
        $declineReason = JobDeclineReason::customSelectList();
        $accept_offer = $offer->status == 'sended' && $offer->to_user_id == Auth::user()->id;

        if ($offer->to_user_id !== Auth::user()->id && $offer->from_user_id !== Auth::user()->id) {
            return redirect('/');
        }

        foreach($offer->job->category as $itm) {
            $offer_category[] = $itm->name;
        }

        $offer_category = implode(',', $offer_category);

        return view('jobs.offer', [
            'offer' => $offer,
            'accept_offer' => $accept_offer,
            'offer_category' => $offer_category,
            'declineReason' => $declineReason
        ]);
    }

    /**
     * Edit Employee profile
     *
     * @return \Illuminate\Http\Response
     */
    public function profile() {
        $user = Auth::user();
        // print_r($user);

        /** A temporary solution for already created accounts **/
        if (empty($user->employee_info->user_id)) {
            $employeeInfo = new EmployeeInfo;
            $employeeInfo->user_id = $user->id;
            $employeeInfo->save();
            return redirect()->route('employee.profile.edit');
        }
        $user_progress = $user->getProgress();
        $user->cert_status = $user->certificates()->where('status', 1)->exists();
        $user->qua_status = $user->educations()->where('status', 1)->exists();
        $user->load([
            'phone', 'employee_info', 'government_id', 'secondary_id', 'cards', 'certificates', 'location', 'references',
            'maxPerBooking', 'availability', 'yearsExperience', 'wwcc', 'skill', 'careType', 'ownChildren',
            'service', 'childAge', 'locationRange', 'qualification', 'smoker', 'pets', 'hasOwnCar'
        ]);
        return view('employee.profile.edit', ['user' => $user, 'user_progress'=>$user_progress]);
    }

    
    
    /**
     * Display the form to invite a caregiver to job
     *
     * @return \Illuminate\Http\Response
     */
    public function invite($caregiver) {
        $caregiver = User::find($caregiver);
        // $parentJob = Auth::user()->jobs;

        $jobs_items = [];

        // $parentJob = $parentJob->map(function ($job) use ($caregiver) {
        //     foreach ($job->jobInvite as $itm) {
        //         if ($itm->caregiver_id == $caregiver->id) {
        //             return false;
        //         }
        //     }
        //     if($job->closed == 'Active') return ['value' => $job->id, 'text' => $job->title];
        // })->toArray();

        // foreach ($parentJob as $key => $itm) {
        //     if ($itm) {
        //         array_push($jobs_items, $itm);
        //     }
        // }
        $jobs = Auth::user()->jobs()->where(['hired' => 0])->take(3)->orderBy('created_at', 'desc')->get();
        foreach($jobs as $job){
            array_push($jobs_items, ['value' => $job->id, 'text' => $job->title]);
        }
        return view('employee.invite', [
            'jobDurations' => JobDuration::customSelectListForActiveJobDurations(),
            'jobWorkloads' => JobWorkload::customSelectListForActiveJobWorkloads(),
            'parentJob' => $jobs_items,
            'caregiver' => $caregiver
        ]);
    }

    public function employeeProposals(){
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        $id = Auth::user()->id;

        $inter =DB::table('jobs')
            ->join('job_interview', 'jobs.id', '=', 'job_interview.job_id')
            ->join('users', 'jobs.user_id', '=', 'users.id')
            ->select('jobs.*', 'job_interview.status','job_interview.hourly_rate','users.last_name','users.first_name')
            ->where('job_interview.employee_id',$id)
            ->where('job_interview.status','accept')
            ->orderBy('jobs.id', 'desc')
            ->get();

        $invit =DB::table('jobs')
            ->join('job_invite', 'jobs.id', '=', 'job_invite.job_id')
            ->join('users', 'jobs.user_id', '=', 'users.id')
            ->select('jobs.*', 'job_invite.status','users.last_name','users.first_name')
            ->where('job_invite.caregiver_id',$id)
            ->where('job_invite.status','sended')
            ->orderBy('jobs.id', 'desc')
            ->get();

         $app =DB::table('jobs')
            ->join('applicants', 'jobs.id', '=', 'applicants.job_id')
            ->join('users', 'jobs.user_id', '=', 'users.id')
            ->select('jobs.*', 'applicants.by_invite','applicants.hourly_rate','users.last_name','users.first_name')
            ->where('applicants.user_id',$id)
            ->where('applicants.by_invite','=',"0")
            ->orderBy('jobs.id', 'desc')
            ->get();

        return view('employee.employee_proposals',compact('inter','invit','app'));

    }

    public function employeeContracts(){
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return view('employee.employee_contracts');
    }
    public function myJobs(){
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return view('employee.employee_my_jobs');
    }
    public function emptyJobs(){
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return view('employee.empty_jobs');
    }

    public function proposalsList(){
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
        return view('employee.proposals_list');
    }
    public function proposalsCat(){
        if(\App::environment() != "staging") {
            header("Location: /home", true, 307);
            exit;
        }
         return view('employee.proposals_cat');
    }

}
