<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ActionsCtl extends Controller {

    public function delApplies($job_id_path, $user_id)
    {
        if ($job_id_path) {
            $info_job = $this->mdl('ActionsMdl')->infoJob($job_id_path);
            if (!empty($info_job)) {
                // deleting from jobs_applies table
                $table = $this->mdlOrm('Jobs.AppliesMdl');
                $table::where('job_id', $info_job->id)->where('user_id', $user_id)->delete();

                // deleting from offers table
                $this->delOfferByUserJob($user_id, $info_job);

                // deleting mesages
                $thread = $this->delMessagesByUserJob($user_id, $info_job);

                // deleting files
                $this->delMessagesFilesByThread($info_job, $user_id, $thread);
            }
        }
    }

    public function delMessagesFilesByThread($info_job, $user_id, $thread)
    {
        if (!empty($thread)) {
            $users = explode(',', $thread->users);

            $table = $this->mdlOrm('FilesMdl');
            $table::where('job_id', $info_job->id)->whereIn('user_id', $users)->delete();
        }
        
    }

    public function delOfferByUserJob($user_id, $info_job)
    {
        $mdl = $this->mdlOrm('OffersMdl');
        $mdl = $mdl::where('job_id', $info_job->id)->where('to_user_id', $user_id)->delete();
    }

    public function delMessagesByUserJob($user_id, $info_job)
    {
        $thread = $this->mdl('MessagesMdl')->chThreadByUsersJob($user_id, $info_job->id);
        if (!empty($thread)) {
            $table = $this->mdlOrm('MessagesThreadsMdl');
            $table::destroy($thread->id);

            $table = $this->mdlOrm('MessagesMdl');
            $table::where('message_thread_id', $thread->id)->delete();
        }

        return $thread;
    }
}
