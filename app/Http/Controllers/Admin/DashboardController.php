<?php

namespace App\Http\Controllers\Admin;

use App\DBConfigAccess;
use App\Entities\Qualification\Qualification;
use App\Http\Models\VerificationChats;
use App\Services\ScrappingService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Entities\Employee\EmployeeInfo;
use App\Entities\Educations\Educations;
use App\Entities\Languages\Languages;
use App\Entities\Criteria\Base as Criteria;
use App\Http\Models\Skill;
use App\Http\Models\UserInfo;
use App\Http\Models\SecondaryId;
use App\Http\Models\PassportId;
use App\Http\Models\DriverLicenceId;
use App\Http\Models\Wwcc;
use App\User;
use App\UserHistoryData;
use App\Certificates;
use App\CertificatesUser;
use App\ApiServices\DvsApiService;
use DB;
use Illuminate\Mail\Message;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where(['is_caregiver' => 1])->orderBy('updated_at', 'desc')->get();

        $caregivers = $users->map(function($item) {
            $progress = $item->getProgress();
            $completeness = $progress['all_completeness'];

            return [
                'id' => $item->id,
                'first_name' => $item->first_name ? $item->first_name : 'No',
                'last_name' => $item->last_name ? $item->last_name : 'No',
                'email' => $item->email,
                'state' => !empty($item->location->state) ? $item->location->state : 'No',
                'post_code' => !empty($item->location->post_code) ? $item->location->post_code : 'No',
                'created_at' => $item->created_at ? $item->created_at : 'No',
                'photo' => $item->user_photo_100,
                'dob' => $completeness['dob'] ?  date("d/m/Y", strtotime($item->employee_info->birthday)) : 'No',
                'phone' => $completeness['phone'] ? $item->phone->phone : 'No',
                'video' => ($completeness['video_id'] || $completeness['video_url']) ? 'Yes' : 'No',
                'qualifications' => $completeness['qualifications'] ? 'Yes' : 'No',
                'references' => $completeness['references'] ? 'Yes' : 'No',
                'identity_check' => $completeness['identity_check'] ? 'Yes' : 'No',
                'medicare_card' => $completeness['medicare_card'] ? 'Yes' : 'No',
                'wwcc' => $completeness['wwcc'] ? 'Yes' : 'No',
                'completeness' => $progress['percentage_completeness']
            ];
        })->toArray();

        return view( 'admin.dashboard.index', compact('caregivers') );
    }

    private function getStatus($id){
        if($id == 0) $data = '<i class="fa fa-refresh"></i>&nbspsubmitted';
        if($id == 1) $data = '<span style="color: #22b92c;"> <i class="fa fa-thumbs-o-up"></i>&nbspverifed </span>';
        if($id == 2) $data = '<span style="color: #FF1F1F;"> <i class="fa fa-times"></i>&nbsprejected </span>';
        return $data;
    }

    public function caregiverinfo($id){
        $user = User::find((int)$id);

        $user->years_experience = !empty($user->yearsExperience[0]) ? $user->yearsExperience[0]->name : '-';

        $user->more_about_me = $user->getMoreAboutMe();

        $user->max_per_booking = !empty($user->maxPerBooking[0]) ? $user->maxPerBooking[0]->name : '-';
        $user->location_range = !empty($user->locationRange[0]) ? $user->locationRange[0]->name : '-';
        $user->video_id = $user->youtube_upload ? $user->youtube_upload->video_id : false;
        return view('admin.dashboard.caregiverinfo', compact('user'));
    }

    public function board($id){
        $user_load = User::find((int)$id);
        $user =  $user_load->info;

        $user2 =  $user_load;

        /** A temporary solution for already created accounts **/
        if (empty($user2->employee_info->user_id)) {
            $employeeInfo = new EmployeeInfo;
            $employeeInfo->user_id = $user2->id;
            $employeeInfo->save();
            return redirect()->route('employee.profile.edit');
        }
        if ( isset($user2->employee_info->birthday) ){
            $user->user->birthday = $user2->employee_info->birthday;
        }

        if(isset($user2->educations)) {
            $user->educations = $user2->educations;
        }

        if(isset($user2->certificates)) {
            $user->certificates = $user2->certificates;
        }

         if (isset($user2->qualification)){
             $subset = $user2->qualification->map(function ($qualifications) {
                 $progress = $qualifications->getOriginal();
                 return [
                     'id' => $progress['id'],
                     'name' => $progress['name'],
                     'created_at' => $progress['created_at'],
                     'pivot_user_id' => $progress['pivot_user_id'],
                     'institution' => json_decode($progress['pivot_data'])->institution,
                     'completion_year' => json_decode($progress['pivot_data'])->completion_year,
                     'path' => json_decode($progress['pivot_data'])->path,
                     'verified' => (isset($progress['verified'])) ? $progress['verified'] : 0
                 ];
             })->toArray();
             $user->qualifications = $subset;
         }
        $user->phone = $user_load->phone;

        $user->medicare_card = $user_load->getMedicareCard();
        $user->passport = $user_load->getPassport();
        $user->driver_licence = $user_load->getDriverLicence();

        return view('admin.dashboard.board', compact('user'));
    }

    public function boardWwcc($id){

        if (request('user_id') && request('wwcc_id')) {
            $status = request('status') ? request('status') : 0;
            $wwcc = Wwcc::find((int)request('wwcc_id'))->where('user_id', (int)request('user_id'))->update(['verified' => (int)$status]);
            return 1;
        }

        $user = User::find((int)$id);
        $historyData = UserHistoryData::where(['type'=>'wwcc', 'user_id' => $user->id ])->orderBy('created_at', 'desc')->get();
        return view('admin.dashboard.wwcc', compact('user', 'historyData'));
    }

    public function boardWwccManual(){
        if (request('user_id') && request('wwcc_id') && request('state')) {
            $wwcc = Wwcc::find((int)request('wwcc_id'))->where('user_id', (int)request('user_id'))->get();
            $user = User::where('id', (int)request('user_id'))->with('employee_info')->get();
            $result = ScrappingService::scrap($wwcc[0], $user[0], request('state'));
            Wwcc::find((int)request('wwcc_id'))->where('user_id', (int)request('user_id'))->update(['verified' => $result ? 1 : 2]);
        }
        return 1;
    }

    public function boardId($id){
        if(@$_GET["user"]){
            $userUpdate = User::find((int)$_GET["user"]);
            $userUpdate->verifed_id = @$_GET["status"];
            if(@$_GET["verifed_id_visa_exp"] !=''){
                $exp = Carbon::createFromFormat('d/m/Y', $_GET['verifed_id_visa_exp'])->getTimestamp();
                $userUpdate->verifed_id_visa_exp = $exp;
            }
            if(@$_GET["verifed_id_expdate"] !=''){
                $exp = Carbon::createFromFormat('d/m/Y', $_GET['verifed_id_expdate'])->getTimestamp();
                $userUpdate->verifed_id_expdate = $exp;
            }
            $userUpdate->save();
            return 1;
        }
        $user = User::find((int)$id);
        $historyData = UserHistoryData::where(['type'=>'verifed-id', 'user_id' => $user->id ])->orderBy('created_at', 'desc')->get();
        return view('admin.dashboard.verifed-id', compact('user', 'historyData'));
    }

    public function medicareCard($id) {
        $user = User::find((int)$id);
        $user->medicare_card = $user->getMedicareCard();

        $user->allow_dvs_check = $user->isValidMedicareCcard();

        return view('admin.dashboard.medicare', compact('user'));
    }

    public function passport($id) {
        $user = User::find((int)$id);
        $user->passport = $user->getPassport();

        $user->allow_dvs_check = $user->isValidPassport();

        return view('admin.dashboard.passport', compact('user'));
    }

    public function driverLicence($id) {
        $user = User::find((int)$id);
        $user->driver_licence = $user->getDriverLicence();

        $user->allow_dvs_check = $user->isValidDriverLicence();

        return view('admin.dashboard.driver-licence', compact('user'));
    }


    public function medicareCardDvsCheck($id) {
        $user = User::find((int)$id);

        $medicare_card = $user->getMedicareCard();

        if ($medicare_card) {
            $dvs = new DvsApiService();
            $result = $dvs->medicareVerification($user);

            if (isset($result['success'])) {
                $secondaryId = SecondaryId::find($medicare_card['id']);
                if ($result['success'] == 'true') {
                    $secondaryId->verified = 1;
                } else {
                    $secondaryId->verified = -1;
                }
                $secondaryId->save();
            }
        }

        return redirect()->route('admin.board.medicare-card', $id);
    }

    public function passportDvsCheck($id) {
        $user = User::find((int)$id);

        $passport = $user->getPassport();

        if ($passport) {
            $dvs = new DvsApiService();
            $result = $dvs->passportVerification($user);

            if (isset($result['success'])) {
                $passportId = PassportId::find($passport['id']);
                if ($result['success'] == 'true') {
                    $passportId->verified = 1;
                } else {
                    $passportId->verified = -1;
                }
                $passportId->save();
            }
        }

        return redirect()->route('admin.board.passport', $id);
    }

    public function driverLicenceDvsCheck($id) {
        $user = User::find((int)$id);

        $driver_licence = $user->getDriverLicence();

        if ($driver_licence) {
            $dvs = new DvsApiService();
            $result = $dvs->driverLicenceVerification($user);

            if (isset($result['success'])) {
                $driverLicenceId = DriverLicenceId::find($driver_licence['id']);
                if ($result['success'] == 'true') {
                    $driverLicenceId->verified = 1;
                } else {
                    $driverLicenceId->verified = -1;
                }
                $driverLicenceId->save();
            }
        }

        return redirect()->route('admin.board.driver-licence', $id);
    }

	public function phone($id){
		$user = User::find((int)$id);
        $user_id = $id;
		$phone = $user->phone;
		return view('admin.dashboard.phone', compact('phone', 'user_id'));
	}

	public function address($id){
        if(@$_GET["user"]){
            $userUpdate = User::find((int)$_GET["user"]);
            $userUpdate->address_status = @$_GET["status"];
            if(@$_GET["address_status_expdate"] !=''){
                $userUpdate->address_status_expdate = date("Y-m-d H:i:s" ,strtotime($_GET["address_status_expdate"]));
            }
            $userUpdate->save();
            return 1;
        }

		$user = User::find((int)$id);
		$text = array("Land Titles Office records",
                    "Mortgage documents",
                    "Car registration",
                    "Utility bill",
                    "Bank statement",
                    "Council rates notice",
                    "Electoral enrolment card",
                        "Insurance renewal document",
                   "Other");
		if($user->info->loc_type_id == null || $user->info->loc_type_id == 0) {
            $user->info->loc_type_text = "";
        } else {
            $user->info->loc_type_text = $text[$user->info->loc_type_id - 1];
        }

		$historyData = UserHistoryData::where(['type'=>'address', 'user_id' => $user->id ])->orderBy('created_at', 'desc')->get();
		return view('admin.dashboard.address', compact('user', 'historyData'));
	}


    function certificates($id){
        if(@$_GET["user"]){
            $certificates = Certificates::where('id', (int)$_GET["user"] )->first();
            $certificates->status = $_GET["status"];
            $certificates->save();
            return 1;
        }
        $user = User::find((int)$id);
        foreach($user->certificates as $cer){
            if($cer->id == @$_GET['id']){
                $certificat = $cer;
            }
        }
//        print_r($certificat);
        $historyData = UserHistoryData::where(['type'=>'certificates', 'user_id' => $user->id ])->orderBy('created_at', 'desc')->get();
        return view('admin.dashboard.certificates', compact('user', 'historyData', 'certificat'));
    }

    function abn($id){
        if(@$_GET["user"]){
            $userUpdate = User::find((int)$_GET["user"]);
            $userUpdate->abn_verifed = @$_GET["status"];
            $userUpdate->save();
            return 1;
        }
        $user = User::find((int)$id);
        $historyData = UserHistoryData::where(['type'=>'abn', 'user_id' => $user->id ])->orderBy('created_at', 'desc')->get();
        return view('admin.dashboard.abn', compact('user', 'historyData'));
    }

    function qualification($id){
         if(@$_GET["user"]){
             $foundEducation = Educations::where('id', '=', (int)$_GET["user"])->first();
             $foundEducation->status = $_GET["status"];
             $foundEducation->save();
             return 1;
         }
         $user = User::find((int)$id);

         $subset = $user->qualification->map(function ($qualifications) {
             $progress = $qualifications->getOriginal();
             return [
                 'id' => $progress['id'],
                 'name' => $progress['name'],
                 'created_at' => $progress['created_at'],
                 'pivot_user_id' => $progress['pivot_user_id'],
                 'institution' => json_decode($progress['pivot_data'])->institution,
                 'completion_year' => json_decode($progress['pivot_data'])->completion_year,
                 'path' => json_decode($progress['pivot_data'])->path,
                 'verified' => (isset($progress['verified'])) ? $progress['verified'] : 0
             ];
         })->toArray();

         foreach($subset as $cer){
             if($cer['id'] == @$_GET['id']){
                 $qualification = $cer;
             }
         }

         $historyData = UserHistoryData::where(['type'=>'qualifications', 'user_id' => $user->id ])->orderBy('created_at', 'desc')->get();
         return view('admin.dashboard.qualifications', compact('user', 'historyData', 'qualification'));
    }

    function socialLinks($id){
        if(@$_GET["user"]){
            $userUpdate = User::find((int)$_GET["user"]);
            $userUpdate->social_verifed = @$_GET["status"];
            $userUpdate->save();
            return 1;
        }
        $user = User::find((int)$id);
        $historyData = UserHistoryData::where(['type'=>'social', 'user_id' => $user->id ])->orderBy('created_at', 'desc')->get();
        return view('admin.dashboard.social-links', compact('user', 'historyData'));
    }

    public function messages()
    {

        $chats = VerificationChats::selectRaw('*, count(id) as unread')->where('is_answered', '=', '0')
            ->where('message_sender','=','user')
            ->groupBy('verification_type','verification_id', 'user_id')
            ->get();



        return view('admin.dashboard.messages', compact('chats'));
    }

    public function sendMessage()
    {
        $type = request('type');
        $id = request('id');
        $user_id = request('user_id');
        $text = request('text');

        $text = strip_tags($text);

        if(!($id > 0)) {
            $id = null;
        }

        if(!$user_id) {
            app()->abort(404);
        }

        if(!($user = User::find($user_id))) {
            app()->abort(404);
        }


        $message = new VerificationChats();
        $message->user_id = $user_id;
        $message->verification_type = $type;
        $message->verification_id = $id;
        $message->message = $text;
        $message->message_sender = 'admin';
        $message->save();

        $user->markVerificationChatAsAnswered($type, $id);

        // Encrypt data and generate REPLY-TO email address
        $msgdata = [
            'user_id' => $user->id,
            'vertype' => $type,
            'verid' => $id,
        ];
        $msgdata = json_encode($msgdata);
        $msgdata = openssl_encrypt($msgdata, 'AES-256-CBC', config('app.key'), 0, substr(config('app.key'), 0, 16));

        $reply_to_address = bin2hex($msgdata);
        unset($msgdata);


        $link = '/user/settings/get-verified';
        $subj = '';

        switch($type) {
            case 'prof_id':
                $link .='/id';
                $subj = 'ID (Photo ID)';
                break;
            case 'wwcc':
                $link .= '/wwcc';
                $subj = 'WWCC';;
                break;

            case 'education':
                $link .= '/qualifications?id='.$id;
                $subj = 'Education';
                break;
            case 'certifications':
                $link .= '/certificates?id='.$id;
                $subj = 'Certificate';
                break;
            case 'address':
                $link .= '/address';
                $subj = 'Address';
                break;
            case 'abn':
                $link .= '/abn';
                $subj = 'ABN';
                break;
            case 'social':
                $link .= '/social-links';
                $subj = 'Social Links';
                break;
        }


        // Send email to user
        \Mail::send('emails.ver_chat', [
            'text' => (string)$text,
            'link' => (string) $link,
            'subj' => $subj
        ], function (Message $message) use ($user, $reply_to_address, $subj) {
            $message->to($user->email)
                ->replyTo($reply_to_address . "@messages".(\App::environment() == 'staging' ? 'staging' : '').".littleones.com.au")
                ->from('noreply@messages'.(\App::environment() == 'staging' ? 'staging' : '').'.littleones.com.au')
                ->subject($subj.' Verification - Little Ones')
                ->priority(\Swift_Mime_SimpleMessage::PRIORITY_HIGHEST);
        });


        return ['success' => true];
    }

    public function config()
    {
        $config = DBConfigAccess::getBulk();

        return view('admin.config', compact('config'));
    }

    public function configPost()
    {

        $data = request()->all();

        foreach($data as $key=>$value) {
            DB::table('app_config')->where(['key' => $key])->update(['value' => $value]);
        }

        return response()->json(['success' => true]);
    }

    public function configAdd()
    {
        DB::table('app_config')->insert(request()->only(['key', 'value']));
    }

    public function configRemove()
    {
        DB::table('app_config')->where(['key' => \request('key')])->delete();
    }

    public function exportCaregivers()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=caregivers.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );

        $caregivers = User::where(['is_caregiver' => 1])->orderBy('updated_at', 'desc')->get();

        $columns = array('Filled', 'First Name', 'Last Name', 'Email', 'State', 'Post Code', 'Registration', 'DOB', 'Phone', 'Qualification', 'Reference', 'Identity Check', 'Medicare Card', 'WWCC');

        $callback = function() use ($caregivers, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($caregivers as $caregiver) {
                $progress = $caregiver->getProgress();
                $completeness = $progress['all_completeness'];

                $item = [
                    'completeness' => $progress['percentage_completeness'] . '%',
                    'first_name' => $caregiver->first_name ? $caregiver->first_name : 'No',
                    'last_name' => $caregiver->last_name ? $caregiver->last_name : 'No',
                    'email' => $caregiver->email,
                    'state' => !empty($caregiver->location->state) ? $caregiver->location->state : 'No',
                    'post_code' => !empty($caregiver->location->post_code) ? $caregiver->location->post_code : 'No',
                    'created_at' => $caregiver->created_at ? date("d/m/Y H:i:s", strtotime($caregiver->created_at)) : 'No',
                    'dob' => $completeness['dob'] ?  date("d/m/Y", strtotime($caregiver->employee_info->birthday)) : 'No',
                    'phone' => $completeness['phone'] ? $caregiver->phone->phone : 'No',
                    'qualifications' => $completeness['qualifications'] ? 'Yes' : 'No',
                    'references' => $completeness['references'] ? 'Yes' : 'No',
                    'identity_check' => $completeness['identity_check'] ? 'Yes' : 'No',
                    'medicare_card' => $completeness['medicare_card'] ? 'Yes' : 'No',
                    'wwcc' => $completeness['wwcc'] ? 'Yes' : 'No',
                ];

                fputcsv($file, $item);
            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
