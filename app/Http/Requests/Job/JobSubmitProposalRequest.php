<?php

namespace App\Http\Requests\Job;

use Auth;
use App\Http\Requests\Request;

class JobSubmitProposalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'PUT':
            {
                return [
                    'message' => 'required',
                    'rate' => 'required|min:1|regex:/^[0-9]{1,3}.[0-9][0-9]$/',
                    'job_id' => 'required|exists:jobs,id',
                    'questions' => '',
                ];
            }
            default:break;
        }
    }
}
