<?php

namespace App\Http\Requests\Job;

use Auth;
use App\Http\Requests\Request;

class FeedbackRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'contract' => 'required|exists:offers,id',
                    'reason' => 'required|exists:feedback_reason,id',
                    'comment' => 'required',
                    'note' => 'required',
                    'reason_text' => '',
                    'rating_types' => ''
                ];
            }
            default:break;
        }
    }
}
