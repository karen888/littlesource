<?php

namespace App\Http\Requests\Job;

use Auth;
use App\Http\Requests\Request;

class AdvancedFindJobsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'all_words' => 'max:60',
                    'any_words' => 'max:60',
                    'title' => 'max:60',
                    'select' => 'boolean',
                    'page' => 'integer'
                ];
            }
            default:break;
        }
    }
}
