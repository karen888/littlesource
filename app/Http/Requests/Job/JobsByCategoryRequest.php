<?php

namespace App\Http\Requests\Job;

use Auth;
use App\Http\Requests\Request;

class JobsByCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            {
                return [
                    'id' => 'required|exists:categories'
                ];
            }
            default:break;
        }
    }
}
