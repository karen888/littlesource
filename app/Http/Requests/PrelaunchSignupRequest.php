<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PrelaunchSignupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'         => 'unique:prelaunch_signups,email|required|email|max:30',
            'first_name'    => 'required|max:30',
            'last_name'     => 'required|max:30',
            'opt_beta'      => 'required',
            'type'          => 'required',
        ];
    }
}
