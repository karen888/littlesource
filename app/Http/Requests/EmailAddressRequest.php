<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class EmailAddressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_email' => 'email|required|exists:users,email,id,' . Auth::user()->id,
            'new_email' => 'email|required|confirmed|different:old_email|unique:users,email',
            'new_email_confirmation' => 'email|required',
        ];
    }
}
