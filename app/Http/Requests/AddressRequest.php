<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class AddressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'line_1' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
            'document_type_id' => 'required',
            'document_type_name' => 'required',
            // 'document_date' => 'required',
            // 'profile_picture' => 'mimes:jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF',
        ];
    }
}
