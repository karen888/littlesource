<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;


class NotificationsSettingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aproximately' => 'required|in:30min,1h,1d',
            'activity' => 'required|in:all,important',
            'job_recommendations' => 'required|in:daily,weekly,monthly',
            'send_when_offline' => 'required|boolean',
            'proposal_received' => 'required|boolean',
            'interview_accepted' => 'required|boolean',
            'interview_offer_declined_withdrawn' => 'required|boolean',
            'offer_accepted' => 'required|boolean',
            'job_expire_soon' => 'required|boolean',
            'job_expired' => 'required|boolean',
            'interview_initiated' => 'required|boolean',
            'offer_interview_received' => 'required|boolean',
            'offer_interview_withdrawn' => 'required|boolean',
            'proposal_rejected' => 'required|boolean',
            'job_applied_modified_canceled' => 'required|boolean',
            'contract_ends' => 'required|boolean',
            'feedback_made' => 'required|boolean',
            'contract_automatically_paused' => 'required|boolean',
            'tip_help_start' => 'required|boolean',
        ];
    }
}
