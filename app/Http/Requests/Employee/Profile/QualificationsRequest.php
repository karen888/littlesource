<?php

namespace App\Http\Requests\Employee\Profile;

use App\Http\Requests\Request;
use Auth;

class QualificationsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'have_transportation' => 'required',
            'willing_travel' => 'required',
            'smoker' => 'required',
            'sick_children' => 'required',
            'comfortable_with_pets' => 'required',
//            'qualifications' => 'required',
            'care_type' => 'required',
            'child_age' => 'required',
            'own_children' => 'required',
        ];
    }
}
