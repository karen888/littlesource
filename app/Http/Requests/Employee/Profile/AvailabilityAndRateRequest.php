<?php

namespace App\Http\Requests;

use App\Services\AvailabilityAndRateSaver;

class AvailabilityAndRateRequest extends ProfileBuilderRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rate' => 'sometimes|required|regex:/^[0-9]{1,3}(.[0-9][0-9])?$/',
            'rate_visible' => 'boolean',
            'overview' => 'sometimes|required|string|max:255|min:8',
            'criterias' => 'sometimes|required|array',
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'rate.regex' => 'Please use numbers only.'
        ];
    }

    /**
     * @return AvailabilityAndRateSaver
     */
    public function saver()
    {
        return new AvailabilityAndRateSaver($this);
    }
}
