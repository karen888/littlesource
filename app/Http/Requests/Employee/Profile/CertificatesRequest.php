<?php

namespace App\Http\Requests\Employee\Profile;

use App\Http\Requests\Request;
use Auth;

class CertificatesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'provider' => 'required',
            'date_earned' => 'required',
//            'submission' => 'required',
            'scan' => 'required'
        ];
    }
}
