<?php

namespace App\Http\Requests\Employee\Profile;

use App\Http\Requests\Request;
use Auth;

class NameTitleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'max:255',
            'short_name' => 'required|boolean',
            'gender' => 'required',
            'birthday' => 'required|date',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
        ];
    }
}
