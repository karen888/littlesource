<?php

namespace App\Http\Requests\Employee\Profile;

use App\Http\Requests\ProfileBuilderRequest;
use App\Services\SkillsSaver;

class SkillsRequest extends ProfileBuilderRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'criterias' => 'sometimes|array|criterias',
        ];
    }

    /**
     * @return SkillsSaver
     */
    public function saver()
    {
        return new SkillsSaver($this);
    }
}
