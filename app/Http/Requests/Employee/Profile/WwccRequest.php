<?php

namespace App\Http\Requests\Employee\Profile;

use App\Http\Requests\ProfileBuilderRequest;
use App\Services\WwccSaver;

class WwccRequest extends ProfileBuilderRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'passport_id' => 'array|passport_id',
            'driver_licence_id' => 'array|driver_licence_id',
            'government_id' => 'array|government_id',
            'secondary_id' => 'array|secondary_id',
            'wwcc' => 'array|wwcc',
            'check_agree' => 'boolean|accepted_if_exist',
            'check_confirm' => 'boolean|accepted_if_exist',
            'check_confirm_eligible' => 'boolean|accepted_if_exist',
            'criterias' => 'sometimes|array|criterias',
        ];
    }

    public function messages(){
        return [
            'check_agree.accepted_if_exist' => 'You must agree to continue.',
            'check_confirm.accepted_if_exist' => 'You must confirm to continue.',
            'check_confirm_eligible.accepted_if_exist' => 'You must confirm to continue.',
        ];
    }

    /**
     * @return WwccSaver
     */
    public function saver()
    {
        return new WwccSaver($this);
    }
}