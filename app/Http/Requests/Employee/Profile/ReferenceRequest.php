<?php

namespace App\Http\Requests\Employee\Profile;

use App\Http\Models\Reference;
use App\Http\Requests\ProfileBuilderRequest;
use Illuminate\Support\Facades\Auth;

class ReferenceRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() && $this->user()->id === $this->reference->user_id;
    }
}
