<?php

namespace App\Http\Requests\Employee\Profile;

use App\Http\Requests\Request;
use Auth;

class AvailabilityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'expect_ready' => '',
            'available_value' => 'required|boolean',
            'available_selected' => '',
            'availability_days' => 'sometimes|array'
        ];
    }
}
