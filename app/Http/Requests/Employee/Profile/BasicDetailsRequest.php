<?php

namespace App\Http\Requests;

use App\Services\BasicDetailsSaver;
use Carbon\Carbon;
use DateTime;
use App\Http\Models\State;

class BasicDetailsRequest extends ProfileBuilderRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $beforeDate = Carbon::now()->subYears(18)->addDay(1)->format('d/m/Y');
        return [
            'first_name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'user_photo_100' => 'sometimes|required|string',
            'birthday' => 'sometimes|required|date_format:d/m/Y|before:' . $beforeDate,
            'phone' => 'sometimes|required',
            'phone.phone' => 'string|phone_number|regex:/^\+61/',
            'phone.verified' => 'boolean',
            'location.id' => 'integer|exists:locations,id',
            'location.user' => 'integer|exists:users,id',
            'location.country_id' => 'integer|exists:countries,id',
            'location.post_code' => 'digits:4',
            'location.street' => 'string|min:8|max:255',
            'location.state' => 'integer|exists:states,id',
            'location.suburb' => 'string|min:3|max:255',
            'criterias' => 'sometimes|required|array',
            'gender' => 'required|min:3',
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_photo_100.string' => $this->getStringMessage('Profile Photo'),

            'birthday.date' => $this->getDateMessage('Birthday'),
            'birthday.before' => $this->getDate18OrOverMessage(),
            'birthday.date_format' => $this->getDateFormatMessage('Birthday', 'DD/MM/YYYY'),

            'location.post_code.digits' => 'Please Enter a Valid Australian Post Code',

            'location.street.string' => $this->getStringMessage('Street', 8, 255),
            'location.street.min' => $this->getMinMessage('Street', 8),
            'location.street.max' => $this->getMaxMessage('Street', 255),

            // 'location.state.string' => $this->getStringMessage('State', 2, 3),
            // 'location.state.min' => $this->getMinMessage('State', 2),
            // 'location.state.max' => $this->getMaxMessage('State', 3),
            // 'location.state.exists' => 'The State must be one of Australian states (' . State::all()->implode('name', ', ') . ')',

            'location.suburb.string' => $this->getStringMessage('Suburb', 3, 255),
            'location.suburb.min' => $this->getMinMessage('Suburb', 3),
            'location.suburb.max' => $this->getMaxMessage('Suburb', 255),

            'phone.phone.string' => $this->getStringMessage('Phone Number', 8, 15),
            'phone.phone.phone_number' => $this->getNumberInvalidMessage('Phone Number'),
            'phone.phone.regex' => $this->getRegexMessage('Phone Number', '+61'),
        ];
    }

    /**
     * @return BasicDetailsSaver
     */
    public function saver()
    {
        return new BasicDetailsSaver($this);
    }
}
