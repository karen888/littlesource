<?php

namespace App\Http\Requests;

use App\Services\WorkPreferencesSaver;

class WorkPreferencesRequest extends ProfileBuilderRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rate' => 'sometimes|required|regex:/^[0-9]{1,3}(.[0-9][0-9])?$/',
            'rate_visible' => 'boolean',
            'criterias' => 'sometimes|required|array',
        ];
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'rate.regex' => 'Please use numbers only.'
        ];
    }

    /**
     * @return WorkPreferencesSaver
     */
    public function saver()
    {
        return new WorkPreferencesSaver($this);
    }
}
