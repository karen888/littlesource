<?php

namespace App\Http\Requests\Employee\Find;

use App\Http\Requests\Request;
use App\Entities\Criteria\Base;
use Auth;

class FindCaregiversRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
//        foreach(Base::subclasses() as $key=>$cls) {
//            $rules[$key] = $cls::$validationRule;
//        }

        $rules['page'] = '';
        $rules['postcode'] = 'sometimes|integer';
        
        return $rules;
    }
}

