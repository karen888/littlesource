<?php

namespace App\Http\Requests;

use Auth;

class JobInviteAllRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'job_id' => 'required|integer|min:1',
                    'employee' => 'required|array'
                ];
            }
            default:break;
        }
    }
}
