<?php

namespace App\Http\Requests;

use App\Entities\Criteria\Base;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\Request as FormRequest;

abstract class ProfileBuilderRequest extends FormRequest {

    protected $invalidFields;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $this->invalidFields = $validator->errors()->toArray();
    }

    public function getErrors()
    {
        return $this->invalidFields;
    }

    public function isValid($field)
    {
        return $this->has($field) && !array_has($this->invalidFields, $field);
    }

    public function criteriasValid(){
        static $criterias = null;
        $criterias = is_null($criterias) ? array_keys(Base::subclasses()) : $criterias;

        return !is_array($this->invalidFields) || empty(array_intersect($criterias, array_keys($this->invalidFields)));
    }

    protected function getNumberInvalidMessage($field){
        return "The $field is invalid.";
    }

    protected function getRegexMessage($field, $firstCharacters){
        return "The $field should start with $firstCharacters.";
    }

    protected function getMaxMessage($field, $characters)
    {
        return "The $field may not be greater than $characters characters.";
    }

    protected function getMinMessage($field, $characters)
    {
        return "The $field must be at least $characters characters.";
    }

    protected function getStringMessage($field, $min = null, $max = null)
    {
        $message = "The $field must be a string";
        return $message .= !is_null($min) && !is_null($max) ? ' from 3 to 255 characters' : '';
    }

    protected function getDateMessage($field){
        return "The $field must be a valid date string format DD/MM/YYYY";
    }

    protected function getDateBeforeMessage($field, $before){
        return "The $field must be a date before $before.";
    }

    protected function getDate18OrOverMessage(){
        return "You must be 18+ to register as a caregiver.";
    }

    protected function getDateFormatMessage($field, $format){
        return "The $field does not match the format $format.";
    }

    public function validate(){
        parent::validate();
        $this->saver()->saveValidValues();
    }

    /**
     * @return \App\Services\ProfileBuilderService
     */
    abstract public function saver();
}