<?php

namespace App\Http\Requests;

use Auth;

class HireEmployeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'job_id' => 'required',
                    'card_id' => 'required',
                    'to_user_id' => 'required',
                    'offer_text' => 'required',
                    'hourly_rate' => 'required|regex:/^[0-9]{1,3}.[0-9][0-9]$/',
                    'title' => 'required',
                    'start_date' => 'required',
                ];
            }
            default:break;
        }
    }
}
