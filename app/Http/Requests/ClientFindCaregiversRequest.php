<?php

namespace App\Http\Requests;

use App\Entities\Criteria\Base;

class ClientFindCaregiversRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Base::validationRules();
    }

    public function messages()
    {
        return [];
    }

}
