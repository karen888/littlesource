<?php

namespace App\Http\Requests;

use Auth;

class FavoritesNotesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'PUT':
            {
                return [
                    'id' => 'required|integer|exists:favorites,id',
                    'notes' => 'required|max:255'
                ];
            }
            default:break;
        }
    }
}
