<?php

namespace App\Http\Requests;

use Auth;

class DeclineOfferRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'PUT':
            {
                return [
                    'offer' => 'required|numeric|min:1',
                    'reason' => 'required|numeric|min:1',
                    'message_decline' => ''
                ];
            }
            default:break;
        }
    }
}
