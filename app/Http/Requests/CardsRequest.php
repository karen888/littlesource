<?php

namespace App\Http\Requests;

use Auth;

class CardsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'card_description' => 'required',
                    'card_type' => 'required',
                    'card_number' => 'required|numeric',
                    'card_expdate_m' => 'required|min:1',
                    'card_expdate_y' => 'required|min:4',
                    'card_cvv2' => 'required|numeric',
                    'card_firstname' => 'required',
                    'card_lastname' => 'required',
                    'card_address1' => 'required',
                    'card_address2' => '',
                    'card_city' => 'required',
                    'card_post_code' => 'required|numeric'
                ];
            }
            default:break;
        }
    }
}
