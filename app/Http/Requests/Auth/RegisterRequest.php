<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use App\Services\RegistrarSaver;
use Auth;


class RegisterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'first_name' => 'required|min:2|max:255',
                    'last_name' => 'required|min:2|max:255',
                    'email' => 'required|email|unique:users',
                    /*'password' => 'required|confirmed|min:8',
                    'password_confirmation' => 'required|min:8',
                    'role' => 'required',
                    'post_code' => 'required|digits:4',*/
                ];
            }
            default:break;
        }
    }

    /**
     * Set custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'post_code.digits' => 'Please Enter a Valid Australian Post Code',
        ];
    }
}
