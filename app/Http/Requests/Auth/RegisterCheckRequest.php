<?php

namespace App\Http\Requests\Auth;

use Auth;
use App\Http\Requests\Request;

class RegisterCheckRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'first_name' => 'required|min:2|max:255',
                    'last_name' => 'required|min:2|max:255',
                    'email' => 'required|email|unique:users',
                    /*'password' => 'required|confirmed|min:8',
                    'password_confirmation' => 'required|min:8'*/
                ];
            }
            default:break;
        }
    }
}
