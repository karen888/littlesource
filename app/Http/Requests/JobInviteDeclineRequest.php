<?php

namespace App\Http\Requests;

use Auth;

class JobInviteDeclineRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'PUT':
            {
                return [
                    'reason' => 'required|integer|min:1',
                    'message' => '',
                    'block_client_invite' => 'required|boolean',
                    'job_invite' => 'required|integer|min:1'
                ];
            }
            default:break;
        }
    }
}
