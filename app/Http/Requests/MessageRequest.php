<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MessageRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //TODO: "Custom Validation Rule" needed, search "to" exists in "$user->corespondents" list
        //TODO: needs a rule for new thread, and a rule just for sending a message (an existing thread already has a recipient and a job)
        return [
            'receiver_id' => 'required|exists:users,id',
            'job_id' => 'sometimes|exists:jobs,id',
            'message_id' => 'sometimes|exists:messages,id',
            'message' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'receiver_id.required' => 'Recipient missing.',
            'receiver_id.exists' => 'The user doesn\'t exists.',
            'job.exists' => 'Job doesn\'t exists.',
            'content.required' => 'Please enter a message',
            //'files.mimes' => 'The only files accepted are: jpeg, bmp, png, and pdf'
        ];
    }
}
