<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class SecurityQuestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'PATCH':
            {
                return [
                    'old_answer' => 'required|max:255',
                    'new_question' => 'required|exists:security_question_types,id',
                    'new_answer' => 'required|max:255',
                    'important' => 'required|in:1,true'
                ];
            }
            default:break;
        }
    }
}
