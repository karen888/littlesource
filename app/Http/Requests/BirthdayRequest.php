<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;


class BirthdayRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'birthday' => 'required|max:255',
            // 'bd_scan_of_id' => 'mimes:jpg,JPG,jpeg,JPEG,png,PNG,gif,GIF',
            'bd_id_issuing' => 'required',
            'bd_id_number' => 'required',
        ];
    }
}
