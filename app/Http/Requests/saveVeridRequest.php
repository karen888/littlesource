<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;


class saveVeridRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_photo_100' => 'required',
            'first_name'=>'required',
            'last_name'=>'required',
            'birthday'=>'required',
            'prof_scan_id'=>'required',
            'prof_scan_id2'=>'required',
            'prof_id_type'=>'required',
            'prof_id_sc'=>'required',
            'prof_dateof'=>'required',
            'notau' => 'required',
        ];
    }
}
