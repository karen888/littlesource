<?php

namespace App\Http\Requests;

use Auth;

class JobRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'category'                  => 'required|array',
                    'title'                     => 'required',
                    'description'               => 'required',
                    'duration'                  => 'required|exists:job_durations,id',
                    'workload'                  => 'required|exists:job_workloads,id',
                    'ocupation'                 => 'required|integer',
                    'days'                      => 'required|array|at_least_one_selected_day:' . $this->ocupation,
                    'marketplace_visibility'    => 'required',
                    'minimum_feedback'          => 'required',
                    'allowed_locations'         => 'required',
                    'minimum_experience'        => 'required',
                    'qualifications'            => 'required',
                    'questions'                 => '',
                    'have_transportation'       => '',
                    'gender'                    => '',
                    'smoker'                    => '',
                    'comfortable_with_pets'     => '',
                    'own_children'              => '',
                    'sick_children'             => '',
                    'child_age_range'           => '',
                    'age'                       => ''
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'category'                  => 'required|array',
                    'title'                     => 'required',
                    'description'               => 'required',
                    'duration'                  => 'required|exists:job_durations,id',
                    'workload'                  => 'required|exists:job_workloads,id',
                    'ocupation'                 => 'required|integer',
                    'days'                      => 'required|array|at_least_one_selected_day:' . $this->ocupation,
                    'marketplace_visibility'    => 'required',
                    'minimum_feedback'          => 'required',
                    'allowed_locations'         => 'required',
                    'minimum_experience'        => 'required',
                    'qualifications'            => 'required',
                    'questions'                 => '',
                    'have_transportation'       => '',
                    'gender'                    => '',
                    'smoker'                    => '',
                    'comfortable_with_pets'     => '',
                    'own_children'              => '',
                    'sick_children'             => '',
                    'child_age_range'           => '',
                    'age'                       => ''
                ];
            }
            default:break;
        }
    }
}
