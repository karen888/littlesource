<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class saveAdressRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'loc_adress' => 'required',
            'loc_city' => 'required',
            'loc_zip' => 'required',
            'loc_type_id' => 'required',
//            'loc_date' => 'required'
        ];
    }
}
