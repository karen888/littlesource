<?php

namespace App\Http\Requests;

use Auth;

class JobInviteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'caregiver' => 'required|integer|min:1|exists:users,id',
                    'job' => 'required|integer|min:1',
                    'message' => 'required',
                    'duration' => 'required|integer|min:1',
                    'workload' => 'required|integer|min:1',
                ];
            }
            default:break;
        }
    }
}
