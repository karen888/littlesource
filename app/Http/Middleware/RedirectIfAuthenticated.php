<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;

class RedirectIfAuthenticated {
    const ROUTE_CAREGIVER = '/caregiver/dashboard';
    const ROUTE_ADMIN = '/admin/etc';
    const ROUTE_PARENT = '/client/jobs/my-jobs';
    const ROUTE_SET = '/auth/set';

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($this->auth->check())
		{
		    $route = self::ROUTE_SET;
            if ($this->auth->user()->is_admin){
                $route = self::ROUTE_ADMIN;
            }else if($this->auth->user()->is_caregiver){
                $route = self::ROUTE_CAREGIVER;
            }

            return new RedirectResponse(url($route));
		}
		return $next($request);
	}

}
