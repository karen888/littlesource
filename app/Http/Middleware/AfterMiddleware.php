<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;

class AfterMiddleware implements Middleware
{

    public function handle($request, Closure $next)
    {
        $response = $next($request);

        // // Perform action
        $response->headers->set('Cache-Control', 'no-cache, must-revalidate, no-store, max-age=0, private');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');

        return $response;
    }
}
