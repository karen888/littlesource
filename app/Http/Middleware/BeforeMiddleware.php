<?php namespace App\Http\Middleware;

use Closure;
use HttpResponse;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Support\Facades\Event;
use App\Events\FileUploadedEvent;

class BeforeMiddleware implements Middleware {

    public function handle($request, Closure $next)
    {
        $is_plivo_call = strpos($request->path(), 'generate/code') !== false;
        $is_email_inbound = strpos($request->path(), 'sys/email/inbound') !== false;
        $is_staging = strpos($request->getHost(), 'staging.littleones.com.au') !== false;
        $is_live = \App::environment() == 'production';
        $is_development = \App::environment() == 'development';

        $is_uploading = strpos($request->getRequestUri(), 'upload') !== false;

        \View::composer(['app', 'layouts.master'], function($view) {
            
            // this should be deleted and moved to view composer;
            // top menu 
            $menu_ctl = new \App\Http\Controllers\MenuController();
            $view_menu = $menu_ctl->get();
            $view->with('main_menu', $view_menu);

            // js/css files
            $js_css = \JsCss::getData();
            $view->with('javascripts', $js_css['javascripts']);
            $view->with('csss', $js_css['csss']);
        });


        foreach ($request->file() as $file) {
            Event::fire(new FileUploadedEvent($file));
        }


        return $next($request);
        //return $response;
    }
}
