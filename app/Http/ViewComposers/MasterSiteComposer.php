<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Auth;


class MasterSiteComposer
{

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $userData = Auth::user();
        // print_r($userData);
        if (Auth::check()) {
            if(@$userData->employee_info->short_name == 0){
                $userData->last_name = $userData->last_name[0].".";
            }
        }
        $view->with('currentUser', $userData);
    }

}