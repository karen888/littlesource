<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Auth;

use App\Http\Models\MenuModel;

class MenuComposer
{

    protected $menu;

    protected $menuItems;

    /**
     * Create a new Menu composer.
     *
     * @param  MenuModel  $menuItems
     * @return void
     */
    public function __construct(Request $request, MenuModel $menuItems)
    {

        $this->menuItems = $menuItems->all();
        // Dependencies automatically resolved by service container...
        if (Auth::check()) {
            $this->menu = $this->getAuthMenu();
        } else {
            $this->menu = $this->getGuestMenu();
        }
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('main_menu', $this->menu);
    }


    public function getAuthMenu()
    {
        $menuView = view('menu.auth');
        $response = [];

        $menuItems = $this->menuItems;

        foreach ($menuItems as $menuItem) {
            if(!$menuItem->main_menu) { // not parent urls.
                // $parentUrl = trim($menuItem->url, '/');

                $menuItem['submenu'] = $this->getSubMenu($menuItem->id);
                $response[$menuItem->id] = $menuItem;
            }
        }

        $menuView->with('top_menu_list', $response);

        return $menuView;
    }

    public function getGuestMenu()
    {
        return view('menu.guest');
    }

    private function getSubMenu($menuItemId, $sec = true)
    {
        $response['submenu'] = [];

        $menuItems = $this->menuItems; // All Menu Items

        foreach ($menuItems as $menuItem) {
            if ($menuItem->id == $menuItemId) {
                if ($sec) {
                    $menuItem->submenu = $this->getSubMenu($menuItem->id, false);
                }
                $response['submenu'][] = $menuItem;
            }
        }

        return $response;
    }
}