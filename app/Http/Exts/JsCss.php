<?php namespace App\Http\Exts;

use Illuminate\Support\Facades\Facade;
use App\Http\Exts\ExtCtl;


class JsCss {

    use ExtCtl;

    protected $js = Array();
    protected $css = Array();

    public function getData()
    {
        $cache = $this->settings();
        $res['javascripts'] = '';
        $res['csss'] = '';
        foreach ($this->js as $val) {
            $res['javascripts'] .= '<script src="'.$val.$cache.'"></script>';
        }

        foreach ($this->css as $val) {
            $res['csss'] .= '<link href="'.$val.$cache.'" rel="stylesheet">';
        }

        //print_r($this->js);
        //print_r($res);
        return $res;
    }

    private function settings()
    {
        $info = $this->ctl('SettingsCtl')->getByName('jscss');

        // set files from DB list
        foreach ($info->js_files as $val) {
            $this->addJs($val);
        }

        foreach ($info->css_files as $val) {
            $this->addCss($val);
        }

        if ($info->status == 0) {
            $cache = '';//time();
        } else if ($info->status == 1) {
            $cache = $info->cache;
        } else {
            $cache = '';//time();

            $data['status'] = 1;
            $data['cache'] = $cache;
            $this->ctl('SettingsCtl')->saveByName('jscss', $data, $info);
        }

        //$cache = '?v='.$cache;

        return $cache;
    }

    public function addJs($val)
    {
        $val = $this->chVal($val);
        $this->js[] = $val;
    }

    public function addCss($val)
    {
        $val = $this->chVal($val, false);
        $this->css[] = $val;
    }
 
    private function chVal($val, $js = true)
    {
        $val = str_replace('\\', '/', $val);
        $val = trim($val, '/');

        if (strpos($val, '/') === false) {
            if ($js) $val = 'js/'.$val;
            else $val = 'css/'.$val;
        }

        $val = '/'.$val;

        return $val;
    }
}
