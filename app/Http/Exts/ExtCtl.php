<?php namespace App\Http\Exts;

trait ExtCtl {

    protected function setVC($view, $ctl = '')
    {
        $this->setView($view);

        if (!empty($ctl)) {
            if (is_object($ctl)) $res = $ctl->getData($view);
            else $res = $this->ctl($ctl)->getData($view);
            if ($res) return $res;
        }

        return $this->view;
    }

    protected function chValid($validator, $replace = Array())
    {
        $res = Array();

        if ($validator->fails()) {
            $errs = Array();
            $err = $validator->messages()->getMessages();
            foreach ($err as $name => $err) {
                if (isset($replace[$name])) {
                    $name = $replace[$name];
                }

                $data['name'] = $name;
                $data['error'] = '';
                foreach ($err as $str) {
                    $data['error'] .= $str;
                }
                $errs[] = $data;
                $res = $errs;
            }
        }

        return $res;
    }    

    protected function ctl($name, $err = true)
    {
        $name = str_replace('.', '\\', $name);
        $name_sp = '\\App\\Http\\Controllers\\'.$name;

        return \App::make($name_sp);
    }

    protected function ext($name)
    {
        $name = str_replace('.', '\\', $name);
        $name_sp = '\\App\\Http\\Exts\\'.$name;

        return \App::make($name_sp);
    }

    protected function mdl($name, $err = true)
    {
        //$name_arr = explode('.', $name);
        //$name_class = $name_arr[count($name_arr) - 1];

        $name = str_replace('.', '\\', $name);
        $name_sp = '\\App\\Http\\Models\\'.$name;

        //if (!isset($this->mdl)) $this->mdl = new \StdClass();
        //$this->mdl->$name_class = \App::make($name_sp);
        return \App::make($name_sp);
    }

    protected function mdlOrm($name, $err = true)
    {
        $name = str_replace('.', '\\', $name);
        $name_sp = '\\App\\Http\\Models\\'.$name;

        return new $name_sp;
    }

    protected function mdlOrmm($name, $err = true)
    {
        $name = str_replace('.', '\\', $name);
        $name_sp = '\\App\\Http\\Models\\'.$name;

        return $name_sp;
    }

    protected function withViewComp($view_file, $name, $data)
    {
        \View::composer($view_file, function($view) use ($name, $data) {
            $view->with($name, $data);
        });
    }

    protected function getFileErr($code_id)
    {
        $code_list = Array( 
            0 => 'There is no error, the file uploaded with success',
            1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
            2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
            3 => 'The uploaded file was only partially uploaded',
            4 => 'No file was uploaded',
            6 => 'Missing a temporary folder',
            7 => 'Failed to write file to disk.',
            8 => 'A PHP extension stopped the file upload.',
        );

        if (isset($code_list[$code_id])) $res = $code_list[$code_id];
        else $res = 'some error';

        return $res;
    }

    protected function routeClear($name)
    {
        $route = route($name);
        $route = preg_replace('/(\%7B.*\%7D)/Uis', '', $route);
        $route = rtrim($route, '/');

        return $route;
    }
}
