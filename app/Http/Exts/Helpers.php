<?php 


if ( ! function_exists('routeClear'))
{
    function routeClear($name, $parameters = array(), $absolute = true, $route = null)
    {
        $route = app('url')->route($name, $parameters, $absolute, $route);
        $route = preg_replace('/(\%7B.*\%7D)/Uis', '', $route);
        $route = rtrim($route, '/');

        return $route;
    }
}

if ( ! function_exists('userPhoto'))
{
    function userPhoto($user)
    {
        $res = new \StdClass();
        $res->photo_100 = '/img/no_photo_100.jpg';
        $res->photo_64 = '/img/no_photo_64.jpg';

        if (!empty($user->user_photo_100)) {
            $res->photo_100 = $user->user_photo_100;
        }

        if (!empty($user->user_photo_64)) {
            $res->photo_64 = $user->user_photo_64;
        }

        return $res;
    }
}

if ( ! function_exists('userPhotoFull'))
{
    function userPhotoFull($user)
    {
        if (empty($user->user_photo_100)) {
            $user->user_photo_100 = '/img/no_photo_100.jpg';
        }

        if (empty($user->user_photo_64)) {
            $user->user_photo_64 = '/img/no_photo_64.jpg';
        }

        return $user;
    }
}

	/**
	 * Create a new paginator instance.
	 *
	 * @param  mixed  $items
	 * @param  int  $total
	 * @param  int  $perPage
	 * @param  int|null  $currentPage
	 * @param  array  $options (path, query, fragment, pageName)
	 * @return void
	 */
if ( ! function_exists('manPaginator'))
{
    function manPaginator($items, $total, $perPage = 10, array $options = [])
    {
        $currentPage = \Illuminate\Pagination\Paginator::resolveCurrentPage();

        $skip = 0;
        if ($currentPage > 0) $skip = ($currentPage - 1) * $perPage;
        $items = $items->skip($skip)->take($perPage)->get();
        $res = new \Illuminate\Pagination\LengthAwarePaginator($items, $total, $perPage, $currentPage);

        $res->setPath(\Request::url());

        return $res;
    }
}

if ( ! function_exists('manPaginator2'))
{
    function manPaginator2($items, $items_count, $total, $perPage = 10, array $options = [])
    {
        $currentPage = \Illuminate\Pagination\Paginator::resolveCurrentPage();


            //$items_count = $items_count->select('messages.id', \DB::raw('count(*) as count'))->groupBy('messages.id');
            //$total = count($items_count->get());
        $total = 12;

        $skip = 0;
        if ($currentPage > 0) $skip = ($currentPage - 1) * $perPage;
        $items = $items->skip($skip)->take($perPage)->get();
        $res = new \Illuminate\Pagination\LengthAwarePaginator($items, $total, $perPage, $currentPage);

        $res->setPath(\Request::url());

        return $res;
    }
}

if ( ! function_exists('convToTimezone'))
{
    function convToTimezone($date, $format = false)
    {
        if (!\Auth::guest()) $timezone_id = \Auth::user()->timezone_id;
        else $timezone_id = 0;

        $date = \Carbon\Carbon::parse($date);

        if ($timezone_id) {
            $info = \App\Http\Models\TimezonesMdl::find($timezone_id);
            if (!empty($info)) {
                //$date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date, $info->timezone_name);
                $date = $date->timezone($info->timezone_name);
            }
        }

        if ($format) $date = $date->format('j M Y h:i A');

        return $date;
    }
}

if ( ! function_exists('getUri'))
{
    /**
     * Removes the first / in the url
     * @param  [type] $url [description]
     * @return [type]      [description]
     */
    
    function getUri($url)
    {
        $haystack = $url;
        $needle = '/';
        $replace = '';

        $pos = strpos($haystack,$needle);
        if ($pos !== false) {
            $url = substr_replace($haystack,$replace,$pos,strlen($needle));
        }
        return $url;
    }
}

//if ( ! function_exists('countryInfo'))
//{
    //function countryInfo($country_id)
    //{
        //$res = new \StdClass();
        //$res->country_code = '';
        //$res->country_name = '';

        //if ($timezone_id) {
            //$info = \App\Http\Models\CountriesMdl::find($country_id);
            //if (!empty($info)) $res = $info;
        //}

        //return $res;
    //}
//}

//namespace App\Http\Exts;

//class HelpersCustom {

    //public function hz()
    //{
    //}

//}
