<?php

namespace App\Http\Exts;

use Illuminate\Support\MessageBag;

class MessageBagCustom extends MessageBag
{
    /**
     * Overriding the default transform method
     * Made to apply original transform message for strings messages only
     *
     * @param array $messages
     * @param string $format
     * @param string $messageKey
     * @return array
     *
     */
    protected function transform($messages, $format, $messageKey)
    {
        $messages = (array)$messages;

        $replace = [':message', ':key'];

        foreach ($messages as &$message) {
            if (is_string($message))
                $message = str_replace($replace, [$message, $messageKey], $format);
        }

        return $messages;
    }

    /**
     * Overriding the default transform method
     * Made to add ability to pass in arrays of messages, needed for criteria validation
     *
     * @param  string $key
     * @param  mixed $message
     * @return $this
     */
    public function add($key, $message)
    {
        if ($this->isUnique($key, $message)) {
            if (is_array($message)) {
                if (!array_has($this->messages, $key)) {
                    array_set($this->messages, $key, []);
                }
                $messages = array_merge(array_get($this->messages, $key), $message);
                array_set($this->messages, $key, $messages);
            } else {
                $this->messages[$key][] = $message;
            }
        }

        return $this;
    }
}