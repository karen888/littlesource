<?php namespace App\Http\Exts;

class JobDuration {

    protected $durations = Array(
        1 => 'More than 6 months',
        2 => '3 to 6 months',
        3 => '1 to 3 months',
        4 => 'Less than 1 month',
        5 => 'Less than 1 week',
    );

    public function getList()
    {
        return $this->durations;
    }

    public function getByKey($key)
    {
        $res = 'unknown';
        if (isset($this->durations[$key])) $res = $this->durations[$key];

        return $res;
    }

    public function chDuration($key)
    {
        $res = 1;
        if (isset($this->durations[$key])) $res = $key;

        return $res;
    }
}
