<?php namespace App\Http\Exts;

use Illuminate\Validation\Validator as IlluminateValidator;
use App\Http\Exts\MessageBagCustom as MessageBag;

class ValidatorCustom extends IlluminateValidator {

    use ExtCtl;

    public function validateHz()
    {
    }

    public function validateCardId($attribute, $value, $parameters)
    {
        $res = $this->mdl('CardsMdl')->chCardIdByUser(\Auth::user()->id, $value);
        if (empty($res)) return false;
        return true;
    }

    /**
     * Overriden original passes method
     * Uses custom message bag, that can use arrays of messages, needed for criteria validation
     * @return bool
     */
    public function passes()
    {
        $this->messages = new MessageBag;

        foreach ($this->rules as $attribute => $rules) {
            foreach ($rules as $rule) {
                $this->validate($attribute, $rule);
            }
        }

        foreach ($this->after as $after) {
            call_user_func($after);
        }

        return count($this->messages->all()) === 0;
    }


}
