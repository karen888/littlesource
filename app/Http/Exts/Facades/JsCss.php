<?php namespace App\Http\Exts\Facades;

use Illuminate\Support\Facades\Facade;

class JsCss extends Facade {

    protected static function getFacadeAccessor()
    {
        return 'JsCss';
    }
}

