<?php

//TODO: remove this controller after checking if it's safe
//Route::controller('home/messages', 'Messages\MessagesCtl');


Route::group(['prefix' => 'messages', 'middleware' => 'auth', 'namespace' => 'Messages'], function () {
    Route::get('/{direct?}', ['as' => 'messages.index', 'uses' => 'MessagesController@index']);

// TODO: remove extraneous routes
//    Route::get('/inbox', ['as' => 'messages.inbox', 'uses' => 'MessagesController@inbox']);
//    Route::get('/archive', ['as' => 'messages.archive', 'uses' => 'MessagesController@archive']);
//    Route::get('/inbox/{conversation}', ['as' => 'messages.inbox.conversation', 'uses' => 'MessagesController@conversation']);
    Route::get('/conversation/{id}', ['as' => 'messages.conversation', 'uses' => 'MessagesController@conversation']);
});
//References:
// api/v1/messages @ App\Http\Routes\api.php @ App\Http\Controllers\Api\v1\Messages\MessagesController@inbox
