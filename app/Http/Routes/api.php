<?php

/**
 * Routes for the API
 */


Route::group(['prefix' => 'api/v1', 'namespace' => 'Api\v1'], function () {

    /**
     * Common endpoints
     */
    Route::group(['prefix' => 'common', 'namespace' => 'Common'], function () {
        Route::get('/countries', 'CommonController@getCountries');
        Route::get('/states', 'CommonController@getStates');
        Route::get('/timezones', 'CommonController@getTimezones');
        Route::get('/qualifications', 'CommonController@getListQualifications');
        Route::get('/categories', 'CommonController@getListCategories');
        Route::get('/job-duration', 'CommonController@getListJobDurations');
        Route::get('/job-workloads', 'CommonController@getListJobWorkloads');
        Route::get('/feedback-reason', 'CommonController@getFeedbackReason');
        Route::get('/rating-options/{type}', 'CommonController@getRatingOptions');
        Route::get('/criteria/{criteria}', 'CommonController@getCriteria');
        Route::get('/criterias/', 'CommonController@getCriteriasList');
        Route::post('/criteria/{criteria}', 'CommonController@postCriteria');
    });

    /**
     *Contracts
     */
    Route::group(['prefix' => 'contracts', 'middleware' => 'auth', 'namespace' => 'Jobs'], function () {
        Route::post('/feedback', ['as' => 'jobs.contracts.post-feedback', 'uses' => 'JobsController@postFeedback']);
        Route::get('/feedback/{contract}', ['as' => 'jobs.contracts.get-feedback', 'uses' => 'JobsController@getContractFeedback']);

    });

    /**
     * Settings Api
     */
    Route::get('/settings/profile-picture', 'User\SettingsController@getCurrentUserProfilePicture');
    Route::get('/settings/user-info', 'User\SettingsController@getCurrentUserInfo');
    Route::get('/settings/notifications', 'User\SettingsController@getNotificationsSettings');
    Route::get('/settings/security-question', 'User\SettingsController@getUserSecurityQuestion');
    Route::get('/user/progress', 'User\SettingsController@getUserProgress');
    Route::post('/settings/profile-picture', 'User\SettingsController@updateProfilePicture');
    Route::patch('/settings/notifications', 'User\SettingsController@updateNotificationsSettings');
    Route::post('/settings/address', 'User\SettingsController@setCurrentUserAddress');
    Route::patch('/settings/email', 'User\SettingsController@setCurrentUserNewEmail');
    Route::patch('/settings/timezone', 'User\SettingsController@setCurrentUserTimezone');
    Route::patch('/settings/phone', 'User\SettingsController@setCurrentUserPhone');
    Route::patch('/settings/sendSMS', 'User\SettingsController@sendSmsCode');
    Route::patch('/settings/verifySMS', 'User\SettingsController@verifySmsCode');
    Route::post('/settings/basic-details', 'User\SettingsController@saveBasicDetails');
    Route::post('/settings/work-preferences', 'User\SettingsController@saveWorkPreferences');
    Route::post('/settings/availability-and-hourly-rate', 'User\SettingsController@saveAvailability');
    Route::post('/settings/skills-and-qualifications', 'User\SettingsController@saveSkills');
    Route::post('/settings/verification', 'User\SettingsController@saveIdAndWWWC');

    //me
    Route::patch('/settings/wwcc', 'User\SettingsController@setCurrentUserWWCC');
    Route::post('/settings/saveid', 'User\SettingsController@saveVerid');
    Route::put('/settings/save-adress', 'User\SettingsController@saveAdress');
    Route::patch('/settings/save-soclink', 'User\SettingsController@saveSoclink');
    Route::post('/settings/delete-verifed', 'User\SettingsController@deleteVerifed');

    Route::patch('/settings/abn', 'User\SettingsController@setCurrentUserABN');
    Route::patch('/settings/password', 'User\SettingsController@updatePassword');
    Route::patch('/settings/security-question', 'User\SettingsController@updateSecurityQuestion');
    Route::patch('/settings/name', 'User\SettingsController@setCurrentUserName');    
    Route::post('/settings/birthday', 'User\SettingsController@setCurrentUserBirthday');
    Route::post('/settings/send-message', 'User\SettingsController@sendMessage');
    Route::post('/upload/image', 'User\SettingsController@uploadImage');
    Route::post('/upload/video', 'User\SettingsController@uploadVideo');
    Route::post('/remove/video', 'User\SettingsController@removeVideo');
    Route::delete('/settings/reference/{reference}', 'User\SettingsController@deleteReference');

    /**
     * Clients Api
     */
    Route::group(['prefix' => 'clients', 'middleware' => ['auth', 'client']], function () {

        /** Jobs **/
        Route::group(['namespace' => 'Jobs'], function () {
            Route::post('/job', 'JobsController@store');
            Route::get('/get-contract', ['as' => 'api.v1.clients.job.get_contract', 'uses' => 'JobsController@getContract']);
            Route::get('/job/{job}', 'JobsController@show');
            Route::get('/job/{job}/close', ['as' => 'api.v1.clients.job.close', 'uses' => 'JobsController@close']);
            Route::get('/job/{job}/make-public', ['as' => 'api.v1.clients.job.make_public', 'uses' => 'JobsController@makePublic']);
            Route::get('/job/{job}/repost-job',['as' => 'api.v1.clients.job.repost_job', 'uses' => 'JobsController@repostJob']);
            Route::patch('/job/{job}', 'JobsController@update');
            Route::post('/job/invite', 'JobInviteController@store');
            Route::post('/job/invite-all', 'JobInviteController@inviteAll');
        });

        Route::group(['namespace' => 'Billing'], function () {
            Route::post('/card/create', 'CardsController@store');
            Route::post('/card/update/{id}', 'CardsController@update');
        });

        Route::group(['namespace' => 'Offers'], function () {
            Route::post('/offer/create', ['as' => 'api.v1.clients.offer.create', 'uses' => 'OffersController@store']);
        });

    });

    /**
     * Employee Api
     */

    Route::group(['prefix' => 'employee', 'middleware' => ['auth']], function () {
        Route::group(['namespace' => 'User'], function () {
            Route::get('/get-info/{id?}', [
                'as' => 'api.v1.employee.get-info',
                'uses' => 'EmployeeController@getEmployeeInfo'
            ]);
        });

        Route::group(['middleware' => ['auth', 'employee']], function () {
            /** Jobs **/
            Route::group(['namespace' => 'Jobs'], function () {
                Route::put('/job/accept', ['as' => 'api.v1.employee.job.accept', 'uses' => 'JobInviteController@accept']);
                Route::put('/job/decline', ['as' => 'api.v1.employee.job.decline', 'uses' => 'JobInviteController@decline']);
                Route::put('/job/proposal', ['as' => 'api.v1.employee.job.proposal', 'uses' => 'JobProposalController@store']);
                Route::get('/job/find-jobs', ['as' => 'api.v1.employee.job.get_find_jobs', 'uses' => 'JobsController@getFindJobs']);
                Route::post('/job/find-jobs-advanced', ['as' => 'api.v1.employee.job.get_find_jobs', 'uses' => 'JobsController@getAdvancedFindJobs']);
                Route::get('/job/get-post', ['as' => 'api.v1.employee.job.get_post', 'uses' => 'JobsController@getPost']);
                Route::get('/job/find-jobs-category', ['as' => 'api.v1.employee.job.get_find_jobs_category', 'uses' => 'JobsController@getJobsByCategory']);
                Route::get('/job/{job_id}/questions', ['as' => 'api.v1.employee.job.get_job-questions', 'uses' => 'JobsController@getJobQuestions']);
                Route::post('/job/{job_id}/save-job', ['as' => 'api.v1.employee.job.save_job', 'uses' => 'JobsController@saveJob']);
                Route::get('/job/{job}/remove-job', ['as' => 'api.v1.employee.job.remove_job', 'uses' => 'JobsController@removeJob']);
            });

            Route::group(['namespace' => 'User'], function () {
                Route::put('/profile/availability', [
                    'as' => 'api.v1.employee.save.availability',
                    'uses' => 'EmployeeController@saveAvailability'
                ]);
                Route::put('/profile/education', [
                    'as' => 'api.v1.employee.save.education',
                    'uses' => 'EmployeeController@saveEducation'
                ]);
                
                Route::put('/profile/education-verify', [
                    'as' => 'api.v1.employee.save.education.verify',
                    'uses' => 'EmployeeController@saveEducationVerify'
                ]);

                Route::put('/profile/certificates', [
                    'as' => 'api.v1.employee.save.certificates',
                    'uses' => 'EmployeeController@saveCertificates'
                ]);                
                Route::put('/profile/hourlyrate', [
                    'as' => 'api.v1.employee.save.hourlyrate',
                    'uses' => 'EmployeeController@saveHourlyRate'
                ]);
                Route::put('/profile/nametitle', [
                    'as' => 'api.v1.employee.save.nametitle',
                    'uses' => 'EmployeeController@saveNameTitle'
                ]);
                Route::put('/profile/overview', [
                    'as' => 'api.v1.employee.save.overview',
                    'uses' => 'EmployeeController@saveOverview'
                ]);
                Route::put('/profile/qualifications', [
                    'as' => 'api.v1.employee.save.qualifications',
                    'uses' => 'EmployeeController@saveQualifications'
                ]);
                Route::put('/profile/skills', [
                    'as' => 'api.v1.employee.save.skills',
                    'uses' => 'EmployeeController@saveSkills'
                ]);
                Route::put('/profile/experience', [
                    'as' => 'api.v1.employee.save.experience',
                    'uses' => 'EmployeeController@saveExperience'
                ]);
                
            });

            Route::group(['namespace' => 'Offers'], function () {
                Route::put('/offer/accept', ['as' => 'api.v1.employee.offer.accept', 'uses' => 'OffersController@accept']);
                Route::put('/offer/decline', ['as' => 'api.v1.employee.offer.decline', 'uses' => 'OffersController@decline']);
            });
        });
    });

    Route::group(['prefix'=>'client', 'middleware'=>['auth', 'client']], function() {
        Route::group(['prefix'=>'caregivers', 'namespace'=>'User'], function() {
            Route::post('/find', ['as'=>'api.v1.client.caregivers.find', 'uses'=>'EmployeeController@findCaregiversPost']);
        });
        Route::group(['prefix'=>'caregivers', 'namespace'=>'Clients'], function() {
            Route::get('/messaged', ['as'=>'api.v1.client.caregivers.messaged', 'uses'=>'ClientController@getMessaged']);
            Route::get('/hired', ['as'=>'api.v1.client.caregivers.hired', 'uses'=>'ClientController@getHired']);
        });
    });

    Route::group(['prefix'=>'client', 'middleware'=>['auth']], function() {
        Route::group(['prefix'=>'caregivers', 'namespace'=>'Clients'], function() {
            Route::get('/find', ['as'=>'api.v1.client.caregivers.find', 'uses'=>'ClientController@findCaregivers']);
        });
    });

    /**
     * Applicants
     */
    Route::group(['prefix' => 'applicants', 'namespace' => 'Applicants'], function () {

        Route::get('/{job}', 'ApplicantsController@index');
        Route::get('/shortlist/{applicant}', 'ApplicantsController@shortlist');
        Route::get('/hide/{applicant}', 'ApplicantsController@hide');
    });

    /**
     * Pre-launch
     */
    Route::group(['prefix' => 'pre-launch', 'namespace' => 'Prelaunch'], function () {

        Route::post('/sign-up', 'SignupsController@store');
    });

    /**
     * Messages
     */
    Route::group(['prefix' => 'messages', 'namespace' => 'Messages'], function () {

        Route::get('/', 'MessagesController@inbox');
        Route::get('/inbox', 'MessagesController@inbox');
        Route::get('/archive', 'MessagesController@archive');
        Route::get('/conversation/{id}', 'MessagesController@conversation');
        Route::get('/corespondents', 'MessagesController@corespondents');
        Route::post('/conversation/{id?}', 'MessagesController@messagePost');
    });

    /** Favorites Api **/
    Route::group(['prefix' => 'favorites', 'namespace' => 'Favorites', 'middleware' => ['auth']], function () {
        Route::get('/list', 'FavoritesController@getList');
        Route::put('/notes', 'FavoritesController@updateNotes');
        Route::post('/add/{id}', 'FavoritesController@add');
        Route::post('/delete/{id}', 'FavoritesController@delete');
    });
});
