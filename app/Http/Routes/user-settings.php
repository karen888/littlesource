<?php

/*
User Settings
 */

Route::group(['prefix' => 'user/settings', 'middleware' => 'auth', 'namespace' => 'User\Settings'], function () {

    /*
    My Info
     */
    Route::group(['prefix' => 'my-info'], function () {
        Route::get('/', ['as' => 'client.settings.my-info', 'uses' => 'MyInformationController@index']);
    });

    Route::group(['prefix' => 'get-verified', 'middleware' => ['auth', 'employee']], function () {
        Route::get('/', ['as' => 'client.settings.get-verifed', 'uses' => 'MyInformationController@verified']);
        Route::get('mobile-number', ['as' => 'client.settings.mobile-number', 'uses' => 'MyInformationController@mobile']);
        Route::get('qualifications', ['as' => 'client.settings.qualification', 'uses' => 'MyInformationController@qualification']);
        Route::get('wwcc', ['as' => 'client.settings.wwcc', 'uses' => 'MyInformationController@wwcc']);
        Route::get('address', ['as' => 'client.settings.address', 'uses' => 'MyInformationController@address']);
        Route::get('certificates', ['as' => 'client.settings.certificates', 'uses' => 'MyInformationController@certificates']);
        Route::get('social-links', ['as' => 'client.settings.socialLinks', 'uses' => 'MyInformationController@socialLinks']);
        Route::get('abn', ['as' => 'client.settings.abn', 'uses' => 'MyInformationController@abn']);
        Route::get('id', ['as' => 'client.settings.id', 'uses' => 'MyInformationController@id']);
        Route::get('convertImageTo64', ['as' => 'client.settings.convertImage', 'uses' => 'MyInformationController@convertImageTo64']);
    });
 
    /*
    Billing
     */
    Route::get('billing', ['as' => 'client.billing-methods.index', 'uses' => 'BillingController@index']);
    Route::get('billing/add-payment-method', ['as' => 'client.billing-methods.create', 'uses' => 'BillingController@create']);
    Route::get('billing/update-payment-method/{id}', ['as' => 'client.billing-methods.update', 'uses' => 'BillingController@update']);

    /*
    Password and security
     */
    Route::get('password-and-security', ['as' => 'client.settings.password', 'uses' => 'SecurityController@index']);

    /*
    Notifications
     */
    Route::get('notifications', ['as' => 'client.settings.notifications', 'uses' => 'NotificationsController@index']);
});
