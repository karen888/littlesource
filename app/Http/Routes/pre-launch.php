<?php

Route::group(['namespace'=>'Prelaunch', 'middleware'=>'guest'], function() {
    Route::get('/pre-launch', ['as'=>'pre-launch.index', 'uses'=>'PrelaunchController@index']);
    Route::get('/pre-launch/clients', ['as'=>'pre-launch.clients', 'uses'=>'PrelaunchController@clients']);
    Route::get('/pre-launch/caregivers', ['as'=>'pre-launch.caregivers', 'uses'=>'PrelaunchController@caregivers']);
    Route::get('/pre-launch/confirm/{confirmationCode}', ['as'=>'pre-launch.confirm', 'uses'=>'PrelaunchController@confirm']);
    Route::get('/pre-launch/unsubscribe/{email}', ['as'=>'pre-launch.unsubscribe', 'uses'=>'PrelaunchController@unsubscribe']);
    Route::get('/pre-launch/contact', ['as'=>'pre-launch.contact', 'uses'=>'PrelaunchController@contact']);
    Route::post('/pre-launch/contactsend', ['as'=>'pre-launch.contactsend', 'uses'=>'PrelaunchController@contactsend']);
    Route::get('/pre-launch/email-template', ['as'=>'pre-launch.email-template', 'uses'=>'PrelaunchController@emailTemplate']);
    Route::get('/pre-launch/competition-terms-and-conditions', ['as'=>'pre-launch.competition-terms-and-conditions', 'uses'=>'PrelaunchController@competitionTermsAndConditions']);
    Route::get('/pre-launch/faqs', ['as'=>'pre-launch.faqs', 'uses'=>'PrelaunchController@faqs']);
    Route::get('/pre-launch/caregivers-verification', ['as'=>'pre-launch.caregivers-verification', 'uses'=>'PrelaunchController@caregiversVerification']);
});
