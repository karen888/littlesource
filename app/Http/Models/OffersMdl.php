<?php namespace App\Http\Models;

use App\Http\Models\Model;

class OffersMdl extends Model {
    protected $table = 'offers';

    public function chOffer($from_user_id, $to_user_id, $job_id)
    {
        $res = \DB::table($this->table)
            ->where('from_user_id', $from_user_id)
            ->where('to_user_id', $to_user_id)
            ->where('job_id', $job_id)
            ->first();

        return $res;
    }

    public function chOfferByApply($job_apply_id, $from_user_id)
    {
        $res = \DB::table($this->table)
            ->leftJoin('job_applies', $this->table.'.job_apply_id', '=', 'job_applies.id')
            ->leftJoin('jobs', 'job_applies.job_id', '=', 'jobs.id')
            ->select(
                $this->table.'.*', 
                'job_applies.active as job_apply_active',
                'jobs.title'
            )
            ->where('offers.job_apply_id', $job_apply_id)
            ->where('offers.from_user_id', $from_user_id)
            ->first();

        return $res;
    }

    public function chOfferByUser($offer_id, $user_id, $status = null, $not_status = null)
    {
        $res = \DB::table($this->table)
            ->leftJoin('jobs', $this->table.'.job_id', '=', 'jobs.id')
            ->leftJoin('job_applies', $this->table.'.job_apply_id', '=', 'job_applies.id')
            ->select(
                $this->table.'.*', 
                'jobs.title', 'jobs.path',
                'job_applies.job_apply_fixed_price', 'job_applies.job_apply_price'
            )
            ->where('offers.id', $offer_id)
            ->where(function($query) use ($user_id) {
                $query->where('offers.from_user_id', $user_id)
                    ->orWhere('offers.to_user_id', $user_id);
            });
        if ($status !== null) $res = $res->where('offers.status', $status);
        if ($not_status !== null) $res = $res->where('offers.status', '!=', $not_status);

        $res = $res->first();

        return $res;
    }

    public function getAllOfferByUser($user_id)
    {
        $res = \DB::table($this->table)
            ->leftJoin('jobs', $this->table.'.job_id', '=', 'jobs.id')
            ->select(
                $this->table.'.*', 
                'jobs.title', 
                'jobs.path'
            )
            ->where(function($query) {
                $query->where('offers.status', 1)
                    ->orWhere('offers.status', 3);
            })
            ->where(function($query) use ($user_id) {
                $query->where('offers.from_user_id', $user_id)
                    ->orWhere('offers.to_user_id', $user_id);
            });
        $res = $res->get();

        return $res;
    }

    public function getDataForRate($user_id)
    {
        $res = \DB::table($this->table)
            ->leftJoin('feedbacks', $this->table.'.id', '=', 'feedbacks.offer_id')
            ->select(
                $this->table.'.*',
                'feedbacks.feedback_rate', 'feedbacks.to_user_id as feedback_to_user_id', 'feedbacks.from_user_id as feedback_from_user_id'
            )
            ->where('offers.status', 3)
            ->where(function($query) use ($user_id) {
                $query->where('offers.from_user_id', $user_id)
                    ->orWhere('offers.to_user_id', $user_id);
            });
        $res = $res->get();

        return $res;
    }

    public function getOffersFeedbacks($user_id)
    {
        $pr = $this->getPrefix();
        
        $res = \DB::table($this->table)
            ->leftJoin('jobs', $this->table.'.job_id', '=', 'jobs.id')
            //->leftJoin('feedbacks', $this->table.'.id', '=', 'feedbacks.offer_id')
            ->select(
                $this->table.'.*', 
                'jobs.title', 
                'jobs.path'
                //\DB::raw('(SELECT count(`id`) as ids FROM `'.$pr.'feedbacks` WHERE offer_id='.$pr.'offers.id) as count_feedbacks')
                //\DB::raw('(SELECT count(`id`) as ids FROM `'.$pr.'feedbacks` WHERE offer_id='.$pr.'offers.id AND '.$pr.'feedbacks.from_user_id='.$pr.'offers.) as count_feedbacks')
            )
            ->where(function($query) {
                $query->where('offers.status', 1)
                    ->orWhere('offers.status', 3);
            })
            ->where(function($query) use ($user_id) {
                $query->where('offers.from_user_id', $user_id)
                    ->orWhere('offers.to_user_id', $user_id);
            });
        $res = $res->get();

        return $res;
    }

    public function getOffersForContractor($user_id, $active = false)
    {
        $res = \DB::table($this->table)
            ->leftJoin('jobs', $this->table.'.job_id', '=', 'jobs.id')
            ->leftJoin('users', $this->table.'.from_user_id', '=', 'users.id')
            ->select($this->table.'.*', 'jobs.title', 'jobs.path', 'users.name as employer_name')
            ->where('offers.to_user_id', $user_id);
        if ($active) {
            $res = $res->where('offers.status', 1);
        } else {
            $res = $res->where('offers.status', 0);
        }

        $res = $res ->orderBy('offers.created_at', 'desc')
            ->get();

        return $res;
    }

    public function getOffersForContractorTimesheet($user_id, $date_start, $date_end)
    {
        $pr = $this->getPrefix();

        $res = \DB::table($this->table)
            ->leftJoin('jobs', $this->table.'.job_id', '=', 'jobs.id')
            ->leftJoin('job_applies', $this->table.'.job_apply_id', '=', 'job_applies.id')
            ->leftJoin('users', $this->table.'.from_user_id', '=', 'users.id')
            ->select(
                $this->table.'.*', 
                'jobs.title', 'jobs.path', 
                'job_applies.job_apply_fixed_price',
                'users.name as employer_name',
                \DB::raw('(
                    SELECT GROUP_CONCAT(`id`) as ids FROM `'.$pr.'timesheets` 
                        WHERE offer_id='.$pr.'offers.id and timesheet_day_of_week>="'.$date_start.'" and timesheet_day_of_week<="'.$date_end.'"
                    ) as timesheets_ids')
            )
            ->where('job_applies.job_apply_fixed_price', 0)
            ->where('offers.to_user_id', $user_id);

        $res = $res->where('offers.status', 1);

        $res = $res ->orderBy('offers.created_at', 'desc')
            ->get();

        return $res;
    }

    public function getOffersForEmployer($user_id, $active = false, $paginate = false)
    {
        $res = \DB::table($this->table)
            ->leftJoin('jobs', $this->table.'.job_id', '=', 'jobs.id')
            ->leftJoin('users', $this->table.'.to_user_id', '=', 'users.id')
            ->select($this->table.'.*', 'jobs.title', 'jobs.path', 'users.name as contractor_name', 'users.user_path')
            ->where('offers.from_user_id', $user_id);
        if ($active) {
            $res = $res->where('offers.status', 1);
        } else {
            $res = $res->where('offers.status', 0);
        }

        $res = $res ->orderBy('offers.created_at', 'desc');
        if ($paginate) {
            $res = $res->paginate(10);
        } else {
            $res = $res->get();
        }

        return $res;
    }

    public function getOffersFrelancers($user_id)
    {
        $pr = $this->getPrefix();

        $res = \DB::table($this->table)
            ->leftJoin('users', $this->table.'.to_user_id', '=', 'users.id')
            ->select(
                $this->table.'.*', 
                'users.name as contractor_name', 
                'users.user_path',
                'users.user_title',
                'users.user_photo_64',
                \DB::raw('GROUP_CONCAT(`job_id`) as jobs_id')
            )
            ->where('offers.from_user_id', $user_id)
            ->where('offers.status', 1)
            ->groupBy('offers.to_user_id')
            ->paginate(10);

        return $res;
    }

    public function getOfferByIdUser($id, $user_id, $status = 0)
    {
        $res = \DB::table($this->table)
            ->leftJoin('jobs', $this->table.'.job_id', '=', 'jobs.id')
            ->leftJoin('users', $this->table.'.to_user_id', '=', 'users.id')
            ->select($this->table.'.*', 'jobs.title', 'jobs.path', 'users.name as contractor_name', 'users.user_path')
            ->where('offers.from_user_id', $user_id)
            ->where('offers.status', $status)
            ->first();

        return $res;
    }

    public function chCardId($card_id)
    {
        $res = \DB::table($this->table)
            ->where('card_id', $card_id);

        $res = $res->first();

        return $res;
    }
}
