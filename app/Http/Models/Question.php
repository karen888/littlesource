<?php

namespace App\Http\Models;

use App\Http\Models\Model;
use App\Entities\Job\Job;

class Question extends Model
{
    /**
     * Fillable for eloquent model
     * @var array
     */
    protected $fillable = ['body'];

    /**
     * Returns the job associated with this question.
     * @return [type] [description]
     */
    public function job() {
        return $this->belongsToMany(Job::class);
    }
}
