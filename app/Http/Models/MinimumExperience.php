<?php

namespace App\Http\Models;

use App\Http\Models\Model;

class MinimumExperience extends Model
{
    /**
     * Fillable for eloquent model
     * @var array
     */
    protected $fillable = ['value', 'is_active'];


    /**
     * Show only Number of Minimum Experiences on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    /**
     * Returns a customselect object of Minimum Experiences;
     * @return [type] [description]
     */
    public static function customSelectListForActiveMinimumExperiences()
    {
        $numberOfHires = static::active()->get();

        $customSelect = $numberOfHires->map(function ($item, $key) {
            return ['value' => $item->id, 'text' => $item->value];
        });

        return $customSelect;
    }

}
