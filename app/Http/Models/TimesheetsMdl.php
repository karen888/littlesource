<?php namespace App\Http\Models;

use App\Http\Models\Model;

class TimesheetsMdl extends Model {
    protected $table = 'timesheets';

    public function getTimeByIds($ids)
    {
        $res = Array();
        $ids = explode(',', $ids);
        if (!empty($ids)) {
            $res = \DB::table($this->table)
                ->whereIn('id', $ids);

            $res = $res->get();
        }

        return $res;
    }

    public function sumTimeDay($day)
    {
        $res = \DB::table($this->table)
            ->where('timesheet_day_of_week', $day);

        $res = $res->sum('timesheet_hours');

        return $res;
    }
}
