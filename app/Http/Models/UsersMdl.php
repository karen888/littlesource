<?php namespace App\Http\Models;

use App\Http\Models\Model;
use App\Http\Exts\ExtCtl;

class UsersMdl extends Model {
    
    use ExtCtl;

    protected $table = 'users';

    public function search($data)
    {
        $pr = $this->getPrefix();

        $skills = $this->mdl('Skills.SkillsMdl')->searchSkill($data['search'], true);

        $res = \DB::table('users');
        $res = $res->leftJoin('skills_users', 'users.id', '=', 'skills_users.user_id');
        $res = $res->select('users.*', \DB::raw('(SELECT GROUP_CONCAT(`skill_id`) as ids FROM `'.$pr.'skills_users` WHERE user_id='.$pr.'users.id) as skills_ids'));
        
        $res = $res->where('users.user_available', 1);
        if (!empty($data['search'])) {
            $res = $res->where(function($query) use ($data, $skills) {
                if (!empty($skills )) $query = $query->whereIN('skills_users.skill_id', $skills);
                $query = $query->orWhere('users.user_overview', 'LIKE', '%'.$data['search'].'%');
                $query = $query->orWhere('users.user_title', 'LIKE', '%'.$data['search'].'%');
            });
        }

        $res = $res->orderBy('users.created_at', 'desc');
        $res = $res->groupBy('users.id');
        $res = $res->paginate(10);

        return $res;
    }
}
