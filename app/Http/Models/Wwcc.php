<?php

namespace App\Http\Models;

use App\User;
use Carbon\Carbon;

class Wwcc extends Model
{
    protected $fillable = [
        'user_id',
        'state_id',
        'application_card_number',
        'card_name',
        'expiry_date',
        'verified',
    ];

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * state
     */
    public function state(){
        return $this->belongsTo(State::class);
    }

    /**
     * Returns an array of validation rules
     * @return array
     */
    public static function getRules()
    {
        return [
            'user_id' => 'integer|exists:users,id',
            'state_id' => 'required|integer|exists:states,id',
            'application_card_number' => 'required|string|min:3|max:24',
            'card_name' => 'required|string|min:3|max:24',
            'expiry_date' => 'required|date_format:d/m/Y|after:today',
            'verified' => 'integer|in:0,1,2',
        ];
    }

    /**
     * Returns an array of validation error messages
     * @return array
     */
    public static function getMessages()
    {
        return [];
    }

    /**
     * @param $value
     * @return false|string
     */
    public function getExpiryDateAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    /**
     * @param $value
     */
    public function setExpiryDateAttribute($value)
    {
        $this->attributes['expiry_date'] = Carbon::createFromFormat("d/m/Y", $value);
    }

    /**
     * @param $value
     * @return false|string
     */
    public function getUpdatedAtAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
}
