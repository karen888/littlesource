<?php namespace App\Http\Models;

class MenuModel extends Model{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'menu_top';

    public function getTop()
    {
        $res = \DB::table('menu_top')->get();

        return $res;
    }

}

