<?php

namespace App\Http\Models;

use App\Http\Models\Model;

class MarketplaceVisibility extends Model
{
    /**
     * Fillable for eloquent model
     * @var [type]
     */
    protected $fillable = ['value', 'is_active'];

    /**
     * Show only active Marketplace Visibilities on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    /**
     * Returns a customselect object of Active Marketplace Visibilities;
     * @return [type] [description]
     */
    public static function customSelectListForActiveMarketplaceVisibilities()
    {
        $marketplaceVisibilities = static::active()->get();

        $customSelect = $marketplaceVisibilities->map(function ($item, $key) {
            return ['value' => $item->id, 'text' => $item->value];
        });

        return $customSelect;
    }
}
