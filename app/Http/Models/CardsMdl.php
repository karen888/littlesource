<?php namespace App\Http\Models;

use App\Http\Models\Model;

class CardsMdl extends Model {
    protected $table = 'cards';

    public function getList($user_id) 
    {
        $res = \DB::table($this->table)
            ->where('user_id', $user_id)
            ->where('card_status', 1);

        $res = $res->get();

        return $res;
    }

    public function chCardIdByUser($user_id, $card_id)
    {
        $res = \DB::table($this->table)
            ->where('id', $card_id)
            ->where('user_id', $user_id)
            ->where('card_status', 1);

        $res = $res->first();
        return $res;
    }
}
