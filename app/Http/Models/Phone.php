<?php

namespace App\Http\Models;

use App\User;

class Phone extends Model
{
    protected $fillable = [
        'user_id',
        'phone',
        'verified',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
