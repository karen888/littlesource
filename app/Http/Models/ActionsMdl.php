<?php namespace App\Http\Models;

use App\Http\Models\Model;

class ActionsMdl extends Model {


    public function infoJob($job_id_path, $user_id = 0)
    {
        $res = \DB::table('jobs');

        if (is_numeric($job_id_path)) $res = $res->where('id', $job_id_path);
        else $res = $res->where('path', $job_id_path);

        if ($user_id) {
            $res = $res->where('user_id', $user_id);
        }

        $res = $res->first();

        return $res;
    }
}

