<?php

namespace App\Http\Models;

class State extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
    ];
}
