<?php

namespace App\Http\Models;

use App\User;

class GovernmentId extends Model
{
    protected $fillable = [
        'user_id',
        'user_holding_document_photo',
    ];

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns an array of validation rules
     * @return array
     */
    public static function getRules()
    {
        return [
            'user_id' => 'integer|exists:users,id',
            'user_holding_document_photo' => 'string',
        ];
    }

    /**
     * Returns an array of validation error messages
     * @return array
     */
    public static function getMessages()
    {
        return [];
    }
}
