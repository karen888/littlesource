<?php

namespace App\Http\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PassportId extends Model
{
    protected $fillable = [
        'user_id',
        'verified',
        'year_only',
        'id_number',
        'photo',
        'card_name',
        'to_check'
    ];

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns an array of validation rules
     * @return array
     */
    public static function getRules()
    {
        return [
            'user_id' => 'integer|exists:users,id',
            'verified' => 'integer|in:0,-1,1,2',
            'year_only' => 'boolean',
            'id_number' => 'required|string|min:3|max:24',
            'photo' => 'required|string',
            'card_name' => 'string',
            'to_check' => 'boolean',
        ];
    }

    /**
     * Returns an array of validation error messages
     * @return array
     */
    public static function getMessages()
    {
        return [
            'id_number.required' => 'The Travel Document Number field is required.'
        ];
    }

    public static function boot() {
        parent::boot();
        self::updating(function($model) {
            $newAttributes = $model->attributes;
            $oldAttributes = $model->original;

            $diff = array_diff_assoc($newAttributes, $oldAttributes);
            if (!empty($diff) && !array_key_exists('verified', $diff)) {
                $model->verified = 0;
            }
        });
    }
}
