<?php

namespace App\Http\Models;

use App\Http\Models\Model;

class Category extends Model {
    /**
     * Table name of model
     * @var string
     */
    protected $table = 'categories';

    /**
     * Fillable for eloquent model
     * @var [type]
     */
    protected $fillable = ['name', 'main_category'];

     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Show only active categories on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('active', 1);
    }

    /**
     * Returns a customselect object of Active Categories;
     * @return [type] [description]
     */
    public static function customSelectListForActiveCategories()
    {
        $categories = static::active()->get();

        $customSelect = $categories->map(function ($item, $key) {
            return [
                'value' => $item->id,
                'text' => $item->name
            ];
        });

        return $customSelect;
    }

    /**
     * Returns a customselect object of Active Categories;
     * @return [type] [description]
     */
    public static function customListCategories($selected = [])
    {
        $categories = static::active()->get();

        $customSelect = $categories->map(function ($item, $key) use ($selected) {
            return [
                'value' => $item->id,
                'text' => $item->name,
                'is_selected' => in_array($item->id, $selected)
            ];
        });

        return $customSelect;
    }
}
