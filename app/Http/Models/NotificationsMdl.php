<?php namespace App\Http\Models;

use App\Http\Models\Model;

class NotificationsMdl extends Model {
    protected $table = 'notifications';

    public function getList($user_id, $limit = 0, $unreaded = false)
    {
        $res = \DB::table($this->table)
            ->where('user_id', $user_id);
        if ($unreaded) {
            $res = $res->where('notification_readed', 0);
        }
        $res = $res->orderBy('created_at', 'desc');
        if ($limit) {
            $res = $res->limit($limit);
            $res = $res->get();
        } else $res = $res ->paginate(10);

        return $res;
    }
}
