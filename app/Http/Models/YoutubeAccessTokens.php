<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class YoutubeAccessTokens extends Model
{
    //
    protected $table = 'youtube_access_tokens';
}
