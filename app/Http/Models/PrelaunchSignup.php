<?php

namespace App\Http\Models;

use App\Http\Models\Model;

class PrelaunchSignup extends Model
{
    /**
     * Fillable for eloquent model
     * @var [type]
     */
    protected $fillable = ['email', 'type', 'opt_beta', 'opt_competition', 'confirmation_code', 'is_confirmed', 'first_name', 'last_name', 'opt_here_more'];

}
