<?php

namespace App\Http\Models;

use App\User;

class Reference extends Model
{
    protected $fillable = [
        'user_id',
        'path',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
