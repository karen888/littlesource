<?php

namespace App\Http\Models;

class JobsMdl extends Model {
    protected $table = 'skills_jobs';

    public function getListByJob($job_id)
    {
        $res = \DB::table($this->table)
            ->leftJoin('skills', $this->table.'.skill_id', '=', 'skills.id')
            ->where('skills_jobs.job_id', $job_id)
            ->get();

        return $res;
    }
}

