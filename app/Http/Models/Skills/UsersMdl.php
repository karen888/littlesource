<?php

namespace App\Http\Models;

class UsersMdl extends Model {
    protected $table = 'skills_users';

    public function getListByUser($user_id)
    {
        $res = \DB::table($this->table)
            ->leftJoin('skills', $this->table.'.skill_id', '=', 'skills.id')
            ->where('skills_users.user_id', $user_id)
            ->get();

        return $res;
    }
}
