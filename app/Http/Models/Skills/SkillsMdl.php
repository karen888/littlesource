<?php
namespace App\Http\Models;

class SkillsMdl extends Model {
    protected $table = 'skills';

    public function searchSkill($skill, $only_id = false)
    {
        $res = Array();

        if (!empty($skill)) {
            $res = \DB::table($this->table)
                ->where('skill_name', 'LIKE', '%'.$skill.'%')
                ->get();

            if ($only_id) {
                if (!empty($res)) {
                    $res_tmp = Array();
                    foreach ($res as $val) {
                        $res_tmp[] = $val->id;
                    }

                    $res = $res_tmp;
                }
            }
        }

        return $res;
    }

    public function getSkillsByIds($ids)
    {
        $res = Array();

        if (!empty($ids)) {
            if (!is_array($ids)) $ids = explode(',', $ids);

            $res = \DB::table($this->table)
                ->whereIN('id', $ids)
                ->get();
        }

        return $res;
    }

}
