<?php

namespace App\Http\Models;

use App\Http\Models\Model;

class Timezone extends Model
{
    /**
     * Fillable for eloquent model
     * @var [type]
     */
    protected $fillable = ['name', 'offset', 'is_enabled', 'php_timezone'];

     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    public static function getCustomSelectTimezones() {
        $timezones = static::all();

        $customSelect = $timezones->map(function ($item, $key) {
            return ['value' => $item->id, 'text' => $item->name];
        });

        return $customSelect;
    }

}
