<?php

namespace App\Http\Models;

use App\Http\Models\Model;
use App\User;

class VerificationChats extends Model
{

    public $table = 'verification_chats';
    /**
     * Fillable for eloquent model
     * @var [type]
     */
    protected $fillable = ['user_id', 'verification_type', 'verification_id', 'message', 'message_sender'];

     /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public static function getCountUnread()
    {
        return self::where('is_answered', '=', '0')->where('message_sender', '=', 'user')->count();
    }

    public static function getCountUserUnread($chat = null)
    {
        $user = \Auth::user();

        if(!$user) return 0;

        $q =  self::where('is_answer_read', '=', '0')->where('message_sender', '=', 'admin')->where('user_id', '=', $user->id);
        if($chat !== null){
            $q = $q->where('verification_type', '=',$chat);
        }

        return $q->count();
    }
}
