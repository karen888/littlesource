<?php namespace App\Http\Models;

use App\Http\Models\Model;

class PaymentsMdl extends Model {
    protected $table = 'payments';

    public function getByDate($user_id, $from, $to, $billing = true)
    {
        $res = \DB::table($this->table);

        if ($billing) {
            $res = $res->leftJoin('users', $this->table.'.to_user_id', '=', 'users.id');
        } else {
            $res = $res->leftJoin('users', $this->table.'.from_user_id', '=', 'users.id');
        }

        $res = $res->select($this->table.'.*', 'users.name')
            ->where('payments.created_at', '>=', $from)
            ->where('payments.created_at', '<=', $to);
            //->whereBetween('created_at', [$from, $to]);

        if ($billing) {
            $res = $res->where('payments.from_user_id', $user_id);
        } else {
            $res = $res->where('payments.to_user_id', $user_id);
        }

        $res = $res->orderBy('payments.created_at', 'desc');

        $res = $res->get();

        return $res;
    }
}
