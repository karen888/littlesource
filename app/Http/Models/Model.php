<?php namespace App\Http\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as ElModel;

class Model extends  ElModel {


    public function getPrefix()
    {
        $prefix = \Config::get('database.default');
        $prefix = \Config::get('database.connections.'.$prefix.'.prefix');

        return $prefix;
    }

}
