<?php namespace App\Http\Models;

use App\Http\Models\Model;

class SettingsMdl extends Model {
    
    protected $table = 'settings';

    public function getByName($name)
    {
        $res = \DB::table($this->table)
            ->where('setting_name', $name)
            ->first();

        return $res;
    }
}
