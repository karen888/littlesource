<?php

namespace App\Http\Models;

use App\Http\Models\Model;
use App\Entities\Country\Country;
use App\Http\Models\Timezone;
use App\User;

class UserInfo extends Model
{

    protected $fillable = [
        'wwcc_image',
        'wwcc_number',
        'wwcc_date',
        'wwcc_user_img',
        'wwcc_check_agree',
        'document_type_name',
        'document_type_id',
        'document_date',
    ];


    /**
     * Returns the user associated with this information
     * @return [type] [description]
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns the country associated with this information
     * @return [type] [description]
     */
    public function country() {
        return $this->belongsTo(Country::class);
    }

    /**
     * Returns the timezone associated with this information
     * @return [type] [description]
     */
    public function timezone() {
        return $this->belongsTo(Timezone::class);
    }

    /**
     * Gets the email for the user
     * @return [type] [description]
     */
    public function getEmailAttribute() {
        return $this->user->email;
    }

    /**
     * Gets the email for the user
     * @return [type] [description]
     */
    public function getCountryNameAttribute() {
        if( $country = $this->country ) {
            return $country->name;
        }
    }

    /**
     * Gets the timezone name for the user
     * @return [type] [description]
     */
    public function getTimezoneNameAttribute() {
        if( $timezone = $this->timezone ) {
            return $timezone->name;
        }
    }

     /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['email', 'country_name', 'timezone_name'];

}
