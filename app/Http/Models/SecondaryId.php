<?php

namespace App\Http\Models;

use App\User;
use Carbon\Carbon;

class SecondaryId extends Model
{
    protected $fillable = [
        'user_id',
        'type',
        'card_names',
        'id_number',
        'acquisition_date',
        'expiry_date',
        'card_type',
        'individual_ref_number',
        'verified',
    ];

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns an array of validation rules
     * @return array
     */
    public static function getRules()
    {
        return [
            'user_id' => 'integer|exists:users,id',
            'type' => 'integer|in:1,2',
            'id_number' => 'required|string|min:10|max:24',
            'expiry_date' => 'required|date_format:d/m/Y|after:today',
            'card_names' => 'required|string|min:3|max:24',
            'card_type' => 'required',
            'individual_ref_number' => 'required|integer|min:1',
            'verified' => 'integer|in:0,-1,1,2'
        ];
    }

    /**
     * Returns an array of validation error messages
     * @return array
     */
    public static function getMessages()
    {
        return [
            'id_number.required' => 'The card number field is required.'
        ];
    }

    /**
     * @param $value
     * @return false|string
     */
    public function getExpiryDateAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }

    /**
     * @param $value
     */
    public function setExpiryDateAttribute($value)
    {
        $this->attributes['expiry_date'] = Carbon::createFromFormat("d/m/Y", $value);
    }

    public static function boot() {
        parent::boot();
        self::updating(function($model) {
            $newAttributes = $model->attributes;
            $oldAttributes = $model->original;

            if (gettype($newAttributes['expiry_date']) != 'string') {
                $newAttributes['expiry_date'] = $newAttributes['expiry_date']->toDateString();
            }

            $diff = array_diff_assoc($newAttributes, $oldAttributes);
            if (!empty($diff) && !array_key_exists('verified', $diff)) {
                $model->verified = 0;
            }
        });
    }
}
