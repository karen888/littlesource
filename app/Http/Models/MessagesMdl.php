<?php namespace App\Http\Models;

use App\Http\Models\Model;

class MessagesMdl extends Model {
    protected $table = 'messages';

    //public function getListThreads($user_id, $inbox = true)
    //{
        //$pr = $this->getPrefix();
        ////$res = \DB::table($this->table)
            ////->leftJoin('jobs', $this->table.'.job_id', '=', 'jobs.id')
            ////->leftJoin('offers', $this->table.'.offer_id', '=', 'offers.id')
            ////->leftJoin('users', $this->table.'.to_user_id', '=', 'users.id')
            ////->select($this->table.'.*', 'users.name', 'jobs.title');

        ////if ($inbox) {
            ////$res = $res->where('to_user_id', $user_id);
            ////$res = $res->groupBy('from_user_id');
        ////} else {
            ////$res = $res->where('from_user_id', $user_id);
            ////$res = $res->groupBy('to_user_id');
        ////}

        ////$res = $res->get();

        ////$query = '
            ////SELECT 
                ////'.$pr.'messages_threads.*, '.$pr.'jobs.title, 
                ////(SELECT id FROM '.$pr.'messages WHERE user_id='.$user_id.' AND message_thread_id='.$pr.'messages_threads.id ORDER BY created_at DESC LIMIT 1) as message_id
            ////FROM 
                ////`'.$pr.'messages_threads` 
            ////LEFT JOIN '.$pr.'jobs ON '.$pr.'messages_threads.job_id='.$pr.'jobs.id
            ////WHERE 
                ////find_in_set ('.$user_id.', cast('.$pr.'messages_threads.users as char)) > 0
            ////';

        ////$query = '
            ////SELECT 
                ////'.$pr.'messages_threads.*, '.$pr.'jobs.title, 
                ////(SELECT created_at FROM '.$pr.'messages WHERE user_id='.$user_id.' AND message_thread_id='.$pr.'messages_threads.id ORDER BY created_at DESC LIMIT 1) as msg_created_at
            ////FROM 
                ////`'.$pr.'messages_threads` 
            ////LEFT JOIN '.$pr.'jobs ON '.$pr.'messages_threads.job_id='.$pr.'jobs.id
            ////WHERE 
                ////find_in_set ('.$user_id.', cast('.$pr.'messages_threads.users as char)) > 0
            ////ORDER BY msg_created_at DESC 
            ////';

        //$query = '
            //SELECT 
                //'.$pr.'messages_threads.*, '.$pr.'jobs.title, msgs.created_at as msg_created_at, msgs.message
            //FROM 
                //`'.$pr.'messages_threads`
            //LEFT JOIN '.$pr.'messages AS msgs ON msgs.message_thread_id='.$pr.'messages_threads.id
            //LEFT JOIN '.$pr.'jobs ON '.$pr.'messages_threads.job_id='.$pr.'jobs.id
            //WHERE
                //find_in_set ('.$user_id.', cast('.$pr.'messages_threads.users as char)) > 0
            //GROUP BY '.$pr.'messages_threads.id
            //ORDER BY msg_created_at DESC
            //';

        ////$query = '
            ////SELECT 
                ////mt.*, jj.title, mm.*
            ////FROM 
                ////`'.$pr.'messages_threads` mt
            ////LEFT JOIN 
                ////(
                ////SELECT 
                    ////msgs.id as msg_id, 
                    ////msgs.message as msg_message, 
                    ////msgs.created_at as msg_created_at,
                    ////msgs.user_id as msg_user_id,
                    ////msgs.message_thread_id
                ////FROM 
                    ////'.$pr.'messages msgs 
                ////ORDER BY msgs.created_at DESC 
                ////) mm ON (mt.id=mm.message_thread_id AND mm.msg_user_id=1)
            ////LEFT JOIN '.$pr.'jobs jj ON mt.job_id=jj.id
            ////WHERE 
                ////find_in_set ('.$user_id.', cast(mt.users as char)) > 0
            ////';

        //$res = \DB::select(\DB::raw($query));

        //$res = \DB::table('messages_threads')
            //->join('messages', 'messages.message_thread_id', '=', 'messages_threads.id')
            //->leftJoin('jobs', 'jobs.id', '=', 'messages_threads.job_id')
            ////->select('messages_threads.*', 'jobs.title', 'messages.created_at as msg_created_at', 'messages.message',
            ////\DB::raw('(SELECT created_at FROM '.$pr.'messages WHERE user_id='.$user_id.' AND message_thread_id='.$pr.'messages_threads.id ORDER BY created_at DESC LIMIT 1) as ord_created_at')
            ////)
            //->select('messages_threads.id', \DB::raw('count(*) as user_count'))
            //->whereRaw('find_in_set ('.$user_id.', cast(users as char)) > 0');

        //if ($inbox) {
            //$res = $res->where('messages.user_id', '!=', $user_id);
        //} else {
            //$res = $res->where('messages.user_id', $user_id);
        //}

        //$res = $res->groupBy('messages_threads.id');
        //print_r($res->get());
        ////$query = $res->toSql();
        ////print_r($query);

        //$res = $res->orderBy('ord_created_at', 'desc');
        ////$res = $res ->groupBy('messages_threads.id');


        ////$res = $res->get();
        ////$res = $res->get();
        //$res = $res->paginate(2);
        ////$res = $res->paginate(2);

        ////print_r($res->count());


        //return $res;
    //}


    public function getListThreads($user_id, $inbox = true, $count = false)
    {
        $pr = $this->getPrefix();

        $res = \DB::table('messages_threads')
            ->join('messages', 'messages.message_thread_id', '=', 'messages_threads.id')
            ->leftJoin('jobs', 'jobs.id', '=', 'messages_threads.job_id');

        if ($count) {
            $res = $res->select('messages_threads.id', \DB::raw('count(*) as count'));
        } else {
            $res = $res->select('messages_threads.*', 'jobs.title', 'messages.created_at as msg_created_at', 'messages.message',
                \DB::raw('(SELECT created_at FROM '.$pr.'messages WHERE user_id='.$user_id.' AND message_thread_id='.$pr.'messages_threads.id ORDER BY created_at DESC LIMIT 1) as ord_created_at')
            );
        }

        $res = $res->whereRaw('find_in_set ('.$user_id.', cast(users as char)) > 0');

        if ($inbox) {
            $res = $res->where('messages.user_id', '!=', $user_id);
        } else {
            $res = $res->where('messages.user_id', $user_id);
        }

        $res = $res->groupBy('messages_threads.id');
        if ($count) {
            $res = count($res->get());
        } else {
            $res = $res->orderBy('ord_created_at', 'desc');
        }

        if (!$count) {
            $count_val = $this->getListThreads($user_id, $inbox, true);
            $res = manPaginator($res, $count_val, 10);
        }

        return $res;
    }

    public function chThreadByUser($user_id, $thread_id)
    {
        $res = \DB::table('messages_threads')
            ->leftJoin('jobs', 'jobs.id', '=', 'messages_threads.job_id')
            ->select('messages_threads.*', 'jobs.title', 'jobs.path')
            ->whereRaw('find_in_set ('.$user_id.', cast(users as char)) > 0')
            ->where('messages_threads.id', $thread_id)
            ->first();

        if (!empty($res)) {
                $users = explode(',', $res->users);
                $kk = array_search($user_id, $users);
                if ($kk !== false) {
                    unset($users[$kk]);
                }
                $res->users_info = \App\User::find($users);

        }

        return $res;
    }

    public function chThreadByUsersJob($users_arr, $job_id, $limit = true)
    {
        $pr = $this->getPrefix();

        if (!is_array($users_arr)) $users_arr = Array($users_arr);

        $where_sql = '';
        $users_arr_count = count($users_arr);
        foreach ($users_arr as $key => $val) {
            $where_sql .= ' find_in_set ('.$val.', cast(users as char)) > 0';

            if ($users_arr_count != ($key+1)) $where_sql .= ' AND';
        }

        if ($limit) $query_limit = ' LIMIT 1';
        else $query_limit = '';

        $query = '
            SELECT * 
            FROM 
                `'.$pr.'messages_threads` 
            WHERE 
                '.$where_sql.'
                AND
                job_id='.$job_id.'
            '.$query_limit;
        $res = \DB::select(\DB::raw($query));

        if ($limit) {
            if (!empty($res)) $res = $res[0];
        }

        return $res;
    }

    public function createThread($users_arr, $job_id)
    {
        $date = \Carbon\Carbon::now()->toDateTimeString();

        $data['users'] = implode(',', $users_arr);
        $data['job_id'] = $job_id;
        $data['created_at'] = $date;
        $data['updated_at'] = $date;

        $res = \DB::table('messages_threads')
            ->insertGetId($data);

        return $res;
    }

    public function getLastMessage($thread_id)
    {
        $res = \DB::table($this->table)
            ->leftJoin('users', $this->table.'.user_id', '=', 'users.id')
            ->select($this->table.'.*')
            ->where('message_thread_id', $thread_id)
            ->orderBy('created_at', 'desc')
            ->first();

        return $res;
    }

    public function getMessages($thread_id, $job_id, $count = false)
    {
        $pr = $this->getPrefix();

        $res = \DB::table($this->table)
            ->leftJoin('users', $this->table.'.user_id', '=', 'users.id')
            ->leftJoin('offers', $this->table.'.offer_id', '=', 'offers.id')
            ->leftJoin('job_applies', $this->table.'.invite_id', '=', 'job_applies.id')
            ->where('message_thread_id', $thread_id)
            ->groupBy('messages.id');

        if ($count) {
            $res = $res->select(\DB::raw('count(*) as count'));
            $res = count($res->get());
        } else {
            $res = $res->select(
                    $this->table.'.*', 
                    'users.name', 
                    'offers.offer_text', 
                    'job_applies.cover_letter_employer as invite_text',
                    \DB::raw('(SELECT GROUP_CONCAT(`id`) as ids FROM `'.$pr.'files` WHERE message_id='.$pr.'messages.id) as files_ids')
                )
                ->orderBy('created_at', 'desc');
            
                //->paginate(10);
        }


        //$res = manPaginator2($res_query, $res_count, 0, 10);
        //print_r($res);
        if (!$count) {
            $count_val = $this->getMessages($thread_id, $job_id, true);
            $res = manPaginator($res, $count_val, 10);
        }

        return $res;
    }

    public function addMsg($from_user_id, $thread_id, $data)
    {
        $date = \Carbon\Carbon::now()->toDateTimeString();

        $data['user_id'] = $from_user_id;
        $data['message_thread_id'] = $thread_id;
        $data['created_at'] = $date;
        $data['updated_at'] = $date;

        $id = \DB::table($this->table)->insertGetId($data);

        return $id;
    }

    public function chNewMsgThreads($user_id, $message_thread_id)
    {
        $res = \DB::table('messages_threads')
            ->join('messages', 'messages.message_thread_id', '=', 'messages_threads.id')
            ->where('messages.user_id', '!=', $user_id)
            ->where('messages.message_thread_id', '=', $message_thread_id)
            ->where('messages.is_read', 0)
            ->count();


        return $res;
    }

    public function chNewMsg($user_id)
    {
        $res = \DB::table('messages_threads')
            ->join('messages', 'messages.message_thread_id', '=', 'messages_threads.id')
            // ->whereRaw('find_in_set ('.$user_id.', cast(users as char)) > 0')
            ->where('messages.is_read', 0)
            ->where('messages.user_id', '!=', $user_id)
            ->where(function ($query) use ( $user_id ) {
                $query->where('sender_id', '=', $user_id)
                      ->orWhere('receiver_id', '=', $user_id);
            })
            ->distinct('message_thread_id')->count('message_thread_id');


        return $res;
    }
}
