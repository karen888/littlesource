<?php

namespace App\Http\Models;

use App\Http\Models\Model;
use Auth;

class AllowedLocations extends Model
{
    /**
     * Fillable for eloquent model
     * @var [type]
     */
    protected $fillable = ['value', 'is_selected'];

    /**
     * Show only active Allowed Locations on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    /**
     * Returns a customselect object of Active Allowed Locations;
     * @return [type] [description]
     */
    public static function customSelectListForActiveAllowedLocations()
    {
        $text = '';
        if (!empty(Auth::user()->info->zipcode)) {
            $text = ' from ' . Auth::user()->info->zipcode;
        }

        $allowedLocations = static::active()->get();
        $customSelect = $allowedLocations->map(function ($item, $key) use ($text) {
            return ['value' => $item->id, 'text' => $item->value . $text];
        });
        return $customSelect;
    }
}
