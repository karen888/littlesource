<?php

namespace App\Http\Models;

use App\User;

class Location extends Model
{
    protected $fillable = [
        'user_id',
        'country_id',
        'latitude',
        'longitude',
        'street',
        'suburb',
        'state',
        'post_code',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}