<?php

namespace App\Http\Models;

use App\Http\Models\Model;

class MinimumFeedback extends Model
{
    /**
     * Fillable for eloquent model
     * @var array
     */
    protected $fillable = ['value'];

    /**
     * Show only active Minimum Feedbacks on the query
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeActive($query) {
        return $query->where('is_active', 1);
    }

    /**
     * Returns a customselect object of Active MinimumFeedback;
     * @return [type] [description]
     */
    public static function customSelectListForActiveMinimumFeedback()
    {
        $minimumFeedback = static::active()->get();

        $customSelect = $minimumFeedback->map(function ($item, $key) {
            return ['value' => $item->id, 'text' => $item->value];
        });

        return $customSelect;
    }
}
