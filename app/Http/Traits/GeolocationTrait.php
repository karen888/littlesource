<?php
namespace App\Http\Traits;
use Auth;
use DB;
use App\Http\Models\UserInfo;

trait GeolocationTrait {

    private function getCaregiversByRange($range)
    {
        if(request('lat', false) && request('lng', false)) {
            $lat = request('lat');
            $lng = request('lng');
        } else {
            $lat = Auth::user()->info->lat;
            $lng = Auth::user()->info->lng;
        }

        return UserInfo::select('user_id')
            ->addSelect(DB::raw(
                "(3959 * acos(cos(radians({$lat})) * cos(radians(lat)) * cos( radians(lng) - radians({$lng})) + sin(radians({$lat})) * sin(radians(lat)))) AS distance"
            ))->having('distance', '<', ($range / 1.61))->get()->map(function($item) {
                return $item->user_id;
            })->toArray();
    }

    private function getCaregiversRange($users_ids)
    {
        $return = [];
        $lat = Auth::user()->info->lat;
        $lng = Auth::user()->info->lng;

        $distance = UserInfo::select('user_id')
            ->addSelect(DB::raw(
                "(3959 * acos(cos(radians({$lat})) * cos(radians(lat)) * cos( radians(lng) - radians({$lng})) + sin(radians({$lat})) * sin(radians(lat)))) AS distance"
            ))->whereIn('user_id', $users_ids)->get();
        
        foreach ($distance as $itm) {
            $return[$itm->user_id] = (float)$itm->distance;
        }
        
        return $return;
    }
    
    private function getGeoDataByPostcode($postcode) {
        $url = config('google.geocode');
        $url .= 'key=' . config('google.map_key');
        $url .= "&address=postcode+{$postcode}+australia";

        return $this->getGeoContent($url);
    }

    private function getGeoContent($url)
    {
        try {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            $return = json_decode(curl_exec($curl));
            curl_close($curl);
        } catch (\Exception $e) {
            return false;
        }
        return $return;
    }
}
