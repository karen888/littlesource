<?php
namespace App\Http\Traits;

use Mail;

trait MailTraits {
    private function sendMail ($template, $user, $subject, $data = []) {
        Mail::send($template, ['user' => $user, 'data' => $data], function ($m) use ($user, $subject) {
            $m->from('info@littleones.com.au', 'Little Ones');
            $m->to($user->email, $user->name)->subject($subject);
        });
    }
}