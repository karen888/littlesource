<?php
namespace App\Http\Traits;

use App\Http\Requests\ProfileBuilderRequest;

trait ResponsesTrait {
    private function setResponse ($data = "Response Success", $redirect = false, $success = true, $status = 200) {
        return response([
            "data" => $data,
            "status" => $status,
            "redirect" => $redirect,
            "success" => $success
        ], $status);
    }

    private function responseValidation ($data = "Validation Error") {
        return response([
            "data" => $data,
            "status" => 422,
            "success" => false
        ], 422);
    }

    private function responseValidationOld ($content = ["Validation Error"]) {
        return response()->json($content, 422);
    }

    private function setSplitResponse(ProfileBuilderRequest $request){
        return response([
            "data" => $request->getErrors(),
            "success" => is_null($request->getErrors())
        ], is_null($request->getErrors()) ? 200 : 422);
    }
}
