<?php

Route::any('/sys/email/inbound', function() {
    $input = request()->all();

    file_put_contents('inbound.txt', print_r($input, true));

    $to = request('to');
    $text = request('text');
    // Get just first line of E-mail message, because the others lines contain a citation of previous message
    $text = explode("\n", $text);
    $text = $text[0];

    if($to && $text) {
        $to = explode('@', $to)[0]; // Get recipient of reply
        $to = hex2bin($to); // decode hexadecimal to binary

        // Decrypt
        $message = openssl_decrypt($to, 'AES-256-CBC', config('app.key'), 0, substr(config('app.key'), 0, 16));
        if(!$message) {
            die("error");
        }
        // Decode resulting JSON
        $message = json_decode($message, true);

        if(json_last_error() !== JSON_ERROR_NONE) {
            die("error json");
        }

        // Create new message for specified verification type, id and send message
        $msg = new \App\Http\Models\VerificationChats();
        $msg->user_id = $message['user_id'];
        $msg->verification_type = $message['vertype'];
        $msg->verification_id = $message['verid'];
        $msg->message = $text;
        $msg->message_sender = 'user';
        $msg->save();

        // Exit
        echo "SUCCESS";
        exit;
    }
});


/* ================================================== */
// TODO: it's only for testing
if(config('app.debug')) {
    Route::get('/secure/auth', function () {
        $users = \App\User::orderBy('id', 'desc')->get();

        echo '<body bgcolor="#000" color="#fff" alink="#f00" vlink="#f00" link="#f00">';
        echo "<h1 style='text-align:center;color:#0f0'>BLACKHOLE AUTH AREA</h1>";
        echo '<table style="width: 500px; margin: 0 auto; " border="1" cellpadding="10" cellspacing="0">';
        foreach ($users as $user) {
            echo "<tr style='color:#fff;font-family: monospace'><td>$user->id</td> <td >$user->email</td><td>$user->first_name</td><td>" . ($user->is_caregiver ? 'Caregiver' : 'Parent') . "</td><td><a href='/secure/auth/$user->id'>Auth</a></td></tr>";
        }
        echo '</table>';
    });

    Route::get('/secure/auth/{id}', function ($id) {
        $user = Auth::loginUsingId($id);
        echo "<div style=' position: fixed;
          top: 50%;
          left: 50%;
          font-family: Arial, Tahoma;
          margin-top: -50px;
          margin-left: -100px;'>";

        echo "Auth OK! UserID: " . $user->id;
        echo "<br/> Redirecting to homepage...";
        echo "</div>";
        echo '<meta http-equiv="refresh" content="2; url=/">';
    });
}

/* ================================================== */


/** Route Partial Map
=================================================== */
$route_partials = [
    'pre-launch',
    'user-settings',
    'api',
    'messages'
];


/** Route Partial Loadup
=================================================== */
foreach ($route_partials as $partial) {
    $file = app_path().'/Http/Routes/'.$partial.'.php';
    if (file_exists($file)) {
        //here should be require_once, but codeception will err
        require $file;
    }
}

//Route::get('/org', function(){
//   return '<h1 style="text-align:center"><a href="/org">SPEED.SHISHKI.BARYGA.ORG</a></h1><br/>><h2>СОЛИ МИКСЫ СПАЙС ЛЕГАЛЬНО </h2><br/><img style="display: block; width: 500px; margin: 0 auto; " src="http://psyplants.info/wp-content/uploads/2012/09/%D1%88%D0%B8%D1%88%D0%BA%D0%B0-%D0%BC%D0%B0%D1%80%D0%B8%D1%85%D1%83%D0%B0%D0%BD%D0%B0.jpg"/>';
//});
Route::post('ajax/delete-billing-method', 'User\Settings\BillingController@delete');
Route::post('ajax/update-billing-method', 'User\Settings\BillingController@update');

Route::get('/photo_core/user/{type}_{id}', function($type, $id) {
    $user = \App\User::find($id);

    if(!$user) {
        app()->abort(404,'Photo not found');

    } else {
        switch($type) {
            default:
                $image = $user->user_photo_100;
                break;
            case '64':
                $image = $user->user_photo_64;
                break;
        }

        $image = str_replace('data:image/png;base64,', '', $image);

        $image = base64_decode($image);

        if(!$image) {
            app()->abort(403,'Thats not a photo');
        }

        return response()->make($image)->header('Content-Type', 'image/png');
    }
});

Route::any('/photo_core/int/{number}', function($number) {
    $text = $number;
    define("FONT_SIZE", 20);                            // font size in points
    define("FONT_PATH", "fonts/myriad-set-pro_bold.ttf"); // path to a ttf font file
    define("FONT_COLOR", 0x00FFFFFF);                  // 4 byte color
    // alpha  -- 0x00 thru 0x7F; solid thru transparent
    // red    -- 0x00 thru 0xFF
    // green -- 0x00 thru 0xFF
    // blue -- 0x00 thru 0xFF

    $gdimage = imagecreatefrompng("marker_bg.png");
    imagesavealpha($gdimage, true);
    list($x0, $y0, , , $x1, $y1) = imagettfbbox(FONT_SIZE, 0, FONT_PATH, $text);
    $imwide = imagesx($gdimage);
    $imtall = imagesy($gdimage) - 0;                  // adjusted to exclude the "tail" of the marker
    $bbwide = abs($x1 - $x0);
    $bbtall = abs($y1 - $y0);
    $tlx = ($imwide - $bbwide) >> 1; $tlx -= 0;        // top-left x of the box
    $tly = ($imtall - $bbtall) >> 1; $tly -= 1;        // top-left y of the box
    $bbx = $tlx - $x0;                                 // top-left x to bottom left x + adjust base point
    $bby = $tly + $bbtall - $y0;                       // top-left y to bottom left y + adjust base point
    imagettftext($gdimage, FONT_SIZE, 0, $bbx, $bby, FONT_COLOR, FONT_PATH, $text);
    header("Content-Type: image/png");
  //  header("Expires: " . gmdate("D, d M Y H:i:s", time() + 60 * 60 * 24 * 180) . " GMT");
    imagepng($gdimage);
    exit;
});
Route::get('/generate/code/{code}', 'Api\v1\User\SettingsController@speakCode');

Route::get('/', [
    'as' => 'index',
    'uses' => 'IndexController@index',
]);

Route::get('/show-email-activation/{code}', 'IndexController@showEmailActivation');
Route::get('/show-email-reset-password/{code}', 'IndexController@showEmailResetPassword');
Route::get('/parent/dashboard', 'Parent\ParentController@show');

Route::get('home', ['as' => 'index', 'uses' => 'IndexController@index']);

// Route::get('/',  ['as'=>'pre-launch.index', 'uses'=>'Prelaunch\PrelaunchController@index']);
Route::get('/privacypolicy', function(){
    return view('privacy');
})->name('privacy');

Route::get('/faqs', function(){
    return view('faqs');
})->name('faqs');

Route::get('/caregivers-verification', function(){
    return view('pom-parents');
})->name('pom-parents');

Route::get('/c/user/{user}', array(
    'as' => 'user_link',
    'uses' => 'Common\CommonCtl@getUserInfo',
));

Route::get('/contact', function() {
    return view('contact_us');
})->name('aboutus');


Route::post('/contact', function() {

    $data = request()->only(['name', 'email', 'message']);
    $validator = Validator::make($data, [
        'name' => 'required|min:3:max:50',
        'email' => 'required|email',
        'message' => 'required|min:2'
    ]);

    if($validator->fails()) {
        return $validator->errors()->toArray();
    }


    Mail::send('emails.contact', ['data' => $data], function ($m) use ($data) {
        $subj = 'A Contact Us page message from '.$data['name'];
        $m->from($data['email'], $data['name']);
        $m->to('info@littleones.com.au')->subject($subj);
        $m->priority(Swift_Message::PRIORITY_HIGHEST);
    });

    return response()->json(['success' => true]);
});

Route::get('about', array(
    'as' => 'about',
    'uses' => 'Common\AboutController@index',
));

Route::any('/legal/{section?}', function($section = null){

    $replacements = \App\DBConfigAccess::getBulk([], '$');

    if($section !== null) {
        if(false === view()->exists('terms.'.$section)) {
            return response()->make('<h1>404 - Not found</h1>', 404);
        }

        $response = view('terms.'.$section, ['section' => $section])->render();

        return strtr($response, $replacements);
    }

    return redirect()->to('/legal/user-agreement', 303);

});

Route::get('/usermarker/{user_id}', function($user_id) {
    $user = \App\User::findOrFail($user_id);

    $photo = $user->user_photo_100;


    $isBase64 = false;

    if(strpos($photo, 'base64,') !== false) {
        $isBase64 = true;
    }

    $photo = str_replace('data:image/png;base64,', '', $photo);
    $photo = str_replace('data:image/jpeg;base64,', '', $photo);

    $marker_size = 180;


    if($isBase64) {
        $photo = base64_decode($photo);
    } else {
        $photo = file_get_contents(".".$photo);
    }

    $marker = new Imagick('marker.png');

    $mask = new Imagick('mask.png');
    $mask->adaptiveResizeImage($marker_size, $marker_size, false);

    $imagick = new Imagick();


    if(empty($photo)) {
        header("Content-Type: image/png");
        echo $marker->getImageBlob();
        exit;
    }

    $imagick->readImageBlob($photo);


    $imagick->adaptiveResizeImage($marker_size, $marker_size,true);
    $imagick->compositeImage($mask, Imagick::COMPOSITE_COPYOPACITY, -2, 3);
    $marker->compositeImage($imagick, Imagick::COMPOSITE_DEFAULT, 40, 13);
//   $imagick->flattenImages();

    header("Content-Type: image/png");
   echo $marker->getImageBlob();
   exit;

});

Route::get('/pre-register', function(){
    return redirect()->to('/register', 301);
});

Route::get('register/{role?}', array(
    'as' => 'pre-register',
    'uses' => 'Common\RegisterController@index',
));

Route::get('how-it-works', array(
    'as' => 'how-it-works',
    'uses' => 'Common\HowitworksController@index',
));

//End Vishal editing


Route::get('home/file/{one}', array(
    'as' => 'file_link',
    'uses' => 'FilesCtl@getFile',
));

Route::group(['prefix' => 'home/offer/{one?}/{two?}'], function () {
    Route::get('/', array(
        'as' => 'home_offer',
        'uses' => 'Home\HomeController@getOffer',
    ));
    Route::post('/', 'Home\HomeController@postOffer');
});

Route::group(['prefix' => 'home/invite/{one?}/{two?}'], function () {
    Route::get('/', array(
        'as' => 'home_invite',
        'uses' => 'Home\HomeController@getInvite',
    ));
    Route::post('/', 'Home\HomeController@postInvite');
});

Route::group(['prefix' => 'home/reports/{one?}/{two?}/{three?}'], function () {
    Route::get('/', array(
        'as' => 'reports',
        'uses' => 'Reports\ReportsCtl@getRoute',
    ));
    Route::post('/', 'Reports\ReportsCtl@postRoute');
});

// Profile Edit
Route::group(['middleware' => ['auth'], 'namespace' => 'User'], function () {

    Route::get('/profile/build/{stepNum?}', ['as' => 'profile.build', 'uses' => 'ProfileController@edit']);
    Route::get('/profile/edit/{stepNum?}', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::get('/profile/verification', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
});

Route::group(['middleware' => ['auth', 'client'], 'namespace' => 'Jobs'], function () {

    /*
    My Jobs
     */
    Route::get('/my-jobs', ['as' => 'client.jobs.my-jobs', 'uses' => 'JobsController@index']);
    Route::get('/get-contract', ['as' => 'clients.jobs.get_contract', 'uses' => 'JobsController@getContract']);
    Route::get('/my-jobs/{job}/details', ['as' => 'client.jobs.my-jobs.details', 'uses' => 'JobsController@details']);
    Route::get('/my-jobs/{job}/edit', ['as' => 'client.jobs.my-jobs.edit', 'uses' => 'JobsController@edit']);
    Route::get('/my-jobs/{job}/applicants', ['as' => 'client.jobs.my-jobs.view-applicants', 'uses' => 'JobsController@applicants']);
    Route::get('/my-jobs/all-jobs', ['as' => 'client.jobs.my-jobs.all', 'uses' => 'JobsController@allJobs']);
    Route::get('/contracts', ['as' => 'client.jobs.contracts', 'uses' => 'JobsController@contracts']);
    Route::post('/search-contract', ['as' => 'clients.jobs.search_contract', 'uses' => 'JobsController@searchContract']);

    Route::get('/create-job', ['as' => 'client.jobs.create', 'uses' => 'JobsController@create']);
    Route::get('/create-job/{job}/success/{view?}', ['as' => 'client.jobs.create.success', 'uses' => 'JobsController@success']);
});

/*
Contracts
 */
Route::group(['prefix' => 'contracts', 'middleware' => 'auth', 'namespace' => 'Jobs'], function () {
    Route::get('/{contract}', ['as' => 'jobs.contracts.details', 'uses' => 'JobsController@contractDetails']);
    Route::get('/{contract}/end-contract', ['as' => 'jobs.contracts.end-contract', 'uses' => 'JobsController@endContract']);
});

/*
Client Caregivers
 */
Route::group(['prefix' => 'caregivers', 'middleware' => ['auth']], function () {
    Route::group([ 'namespace' => 'Employee'], function () {
        Route::get('/employee-proposals', ['as' => 'caregivers.employee.employee-proposals', 'uses' => 'CaregiversController@employeeProposals']);
        Route::get('/employee-contracts', "CaregiversController@employeeContracts");
        //Route::get('/my-jobs', ['as' => 'html.employee.my-jobs', 'uses' => 'CaregiversController@myJobs']);
        Route::get('/proposals-list', "CaregiversController@proposalsList");
        Route::get('/empty-jobs', ['as' => 'html.employee.empty-jobs', 'uses' => 'CaregiversController@emptyJobs']);
        Route::get('/proposals-cat', "CaregiversController@proposalsCat");
    });
});

Route::group(['prefix' => 'caregiver', 'middleware' => ['auth', 'caregiver']], function () {
    Route::group([ 'namespace' => 'Caregiver'], function () {
        Route::get('/dashboard', ['as' => 'caregiver.dashboard', 'uses' => 'CaregiverController@dashboard']);
        Route::get('/my-profile', ['as' => 'caregiver.my-profile', 'uses' => 'CaregiverController@myProfile']);
    });
});

Route::group(['prefix' => 'employee', 'middleware' => ['auth', 'client'], 'namespace' => 'Employee'], function () {

    /*
    Find Caregivers
     */
    Route::get('/find', ['as' => 'client.employee.find', 'uses' => 'CaregiversController@find']);

    /*
    Hire Caregiver
     */
    Route::get('{employee}/hire/{job?}', ['as' => 'client.employee.hire', 'uses' => 'CaregiversController@hire']);

    Route::get('{caregiver}/invite', ['as' => 'client.employee.invite', 'uses' => 'CaregiversController@invite']);
    /*
    My Caregivers
     */
    Route::group(['prefix' => 'my-caregivers'], function () {
        Route::get('/hired', ['as' => 'client.employee.hired', 'uses' => 'CaregiversController@hired']);
        Route::get('/messaged', ['as' => 'client.employee.messaged', 'uses' => 'CaregiversController@messaged']);
        Route::get('/favorites', ['as' => 'client.employee.favorites', 'uses' => 'CaregiversController@favorites']);
        Route::get('/recently-viewed', ['as' => 'client.employee.recently-viewed', 'uses' => 'CaregiversController@recentlyViewed']);
    });

});

/*
Employee Profile
 */

Route::group(['prefix' => 'employee'], function () {
    Route::group(['middleware' => ['auth', 'employee'], 'namespace' => 'Employee'], function () {
        Route::get('/profile', ['as' => 'employee.profile.edit', 'uses' => 'CaregiversController@profile']);
    });
    Route::group([ 'middleware' => 'auth', 'namespace' => 'Jobs'], function () {
        Route::get('/search-results', "JobsController@searchResults");
      //  Route::get('/find-work', ['as' => 'employee.find-work', 'uses' => 'JobsController@findWork']);
        Route::get('/my-jobs', ['as' => 'employee.my-jobs', 'uses' => 'JobsController@myJobs']);
        Route::get('/contracts', ['as' => 'employee.contracts', 'uses' => 'JobsController@EmpContracts']);
        Route::get('/work-diary', ['as' => 'employee.work-diary', 'uses' => 'JobsController@WorkDiary']);
        Route::get('/saved-jobs', ['as' => 'employee.saved-jobs', 'uses' => 'JobsController@savedJobs']);
    });
    /*
    Public Profile
     */
    Route::get('/{caregiver}', ['as' => 'employee.profile', 'uses' => 'Employee\CaregiversController@show']);
});

/*
 * View Offer
 * */
Route::group(['prefix' => 'employee', 'middleware' => 'auth', 'namespace' => 'Employee'], function () {
        Route::get('/{offer}/offer', ['as' => 'employee.view.offer', 'uses' => 'CaregiversController@offer']);
});
/*
Caregiver Jobs
 */

Route::group(['middleware' => ['auth'], 'namespace' => 'Jobs'], function () {
    Route::get('/jobs/{job}/details', ['as' => 'caregiver.jobs.details', 'uses' => 'JobsController@detailsCaregiver']);
});



Route::controllers([
    '/recruit' => 'Recruit\RecruitCtl',
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/*
Admin Zone
 */

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'  , 'middleware' => 'admin' ], function () {

    Route::any('/', function(){
        return redirect()->to('/admin/etc?rnd='.rand(1000000,9999999));
    });


    Route::get('/etc/config', 'DashboardController@config');
    Route::post('/etc/config', 'DashboardController@configPost');
    Route::post('/etc/config/add', 'DashboardController@configAdd');
    Route::delete('/etc/config/remove', 'DashboardController@configRemove');

    Route::get('/etc/messages/', [
        'as' => 'admin.messages',
        'uses' => 'DashboardController@messages',
    ]);

    Route::post('/etc/messages', [
        'as' => 'admin.messages.send',
        'uses' => 'DashboardController@sendMessage',
    ]);


    Route::get('/etc/', [
        'as' => 'admin.dashboard',
        'uses' => 'DashboardController@index',
    ]);
    Route::get('/etc/caregiverinfo/{id}', [
        'as' => 'admin.caregiverinfo',
        'uses' => 'DashboardController@caregiverinfo',
    ]);
    Route::get('/etc/board/{id}', [
        'as' => 'admin.board',
        'uses' => 'DashboardController@board',
    ]);
    Route::get('/etc/board/{id}/wwcc', [
        'as' => 'admin.board.wwcc',
        'uses' => 'DashboardController@boardWwcc',
    ]);
    Route::get('/etc/board/{id}/wwcc/manual', [
        'as' => 'admin.board.wwcc.manual',
        'uses' => 'DashboardController@boardWwccManual',
    ]);
    Route::get('/etc/board/{id}/id', [
        'as' => 'admin.board.id',
        'uses' => 'DashboardController@boardId',
    ]);
    Route::get('/etc/board/{id}/phone', [
        'as' => 'admin.board.phone',
        'uses' => 'DashboardController@phone',
    ]);
    Route::get('/etc/board/{id}/address', [
        'as' => 'admin.board.address',
        'uses' => 'DashboardController@address',
    ]);
    Route::get('/etc/board/{id}/certificates', [
        'as' => 'admin.board.certificates',
        'uses' => 'DashboardController@certificates',
    ]);
    Route::get('/etc/board/{id}/abn', [
        'as' => 'admin.board.abn',
        'uses' => 'DashboardController@abn',
    ]);
    Route::get('/etc/board/{id}/qualification', [
        'as' => 'admin.board.qualification',
        'uses' => 'DashboardController@qualification',
    ]);
    Route::get('/etc/board/{id}/social-links', [
        'as' => 'admin.board.social-links',
        'uses' => 'DashboardController@socialLinks',
    ]);
    Route::get('/etc/board/{id}/medicare-card', [
        'as' => 'admin.board.medicare-card',
        'uses' => 'DashboardController@medicareCard',
    ]);
    Route::get('/etc/board/{id}/medicare-card-dvs', [
        'as' => 'admin.board.medicare-card-dvs',
        'uses' => 'DashboardController@medicareCardDvsCheck',
    ]);
    Route::get('/etc/board/{id}/passport', [
        'as' => 'admin.board.passport',
        'uses' => 'DashboardController@passport',
    ]);
    Route::get('/etc/board/{id}/passport-dvs', [
        'as' => 'admin.board.passport-dvs',
        'uses' => 'DashboardController@passportDvsCheck',
    ]);
    Route::get('/etc/board/{id}/driver-licence', [
        'as' => 'admin.board.driver-licence',
        'uses' => 'DashboardController@driverLicence',
    ]);
    Route::get('/etc/board/{id}/driver-licence-dvs', [
        'as' => 'admin.board.driver-licence-dvs',
        'uses' => 'DashboardController@driverLicenceDvsCheck',
    ]);
    Route::get('/etc/export-caregivers', [
        'as' => 'admin.export-caregivers',
        'uses' => 'DashboardController@exportCaregivers',
    ]);
});

