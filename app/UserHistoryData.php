<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHistoryData extends Model
{
    //
    protected $table = 'user_history_data';
}
