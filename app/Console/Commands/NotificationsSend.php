<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Entities\Notifications\Notifications;
use App\Entities\Notifications\NotificationsData;

class NotificationsSend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifications:send {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');

        switch ($type) {
            case '30min':
                $aproximately = '30min';
                break;
            case '1h':
                $aproximately = '1h';
                break;
            case '1d':
                $aproximately = '1d';
                break;
        }

        $data = Notifications::where('aproximately', $aproximately)->get();

        foreach ($data as $item) {
            $notify = new NotificationsData;
            $notify->setNotifications($item);
        }
    }
}
