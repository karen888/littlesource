<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Entities\Job\Job;

class CloseJobsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jobs:close';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Closes all jobs with over 30 days scheduled for cron jobs running';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notify = Carbon::now()->subDays(27)->toDateTimeString();
        $cancel = Carbon::now()->subDays(30)->toDateTimeString();

        $notify = Job::whereClosed('!=', true)->where('created_at', '>=', $notify)->whereCreatedAt('<', $notify)->get();
        $cancel = Job::whereClosed('!=', true)->where('created_at', '>=', $cancel)->whereCreatedAt('<', $cancel)->get();
    }
}
