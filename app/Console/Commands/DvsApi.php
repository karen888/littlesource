<?php

namespace App\Console\Commands;

use App\ApiServices\DvsApiService;;
use App\User;
use Illuminate\Console\Command;


class DvsApi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dvs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var DvsApiService
     */
    protected $api;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DvsApiService $api)
    {
        parent::__construct();
        $this->api = $api;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $caregivers = User::query()->where('is_caregiver', 1)->get();
        foreach ($caregivers as $caregiver){
            $progress = $caregiver->getProgress();
            $this->line('');
            $this->info('Verification "' . $caregiver->first_name . ' ' . $caregiver->last_name . '"');

            if($caregiver->isDVSVerified()){
                $this->info('Verified!');
                continue;
            }

            if(count($progress['verification_requirements']) == 0){

                if(!$caregiver->passport_id->verified && $caregiver->passport_id->to_check){
                    $res = $this->api->passportVerification($caregiver);
                    if($res['status'] == 200){
                        $caregiver->passport_id->verified = $res['success'];
                        $this->info('Passport Verification = ' . ($res['success'] ? 'true' : 'false'));
                        $caregiver->passport_id->save();
                    } else{
                        $this->error('Passport Verification Error: ' . $res['message']);
                    }
                }

                if(!$caregiver->driver_licence_id->verified && $caregiver->driver_licence_id->to_check){
                    $res = $this->api->driverLicenceVerification($caregiver);
                    if($res['status'] == 200){
                        $caregiver->driver_licence_id->verified = $res['success'];
                        $this->info('Driver Licence Verification = ' . ($res['success'] ? 'true' : 'false'));
                        $caregiver->driver_licence_id->save();
                    } else{
                        $this->error('Driver Licence Verification Error: ' . $res['message']);
                    }
                }

                if(!$caregiver->secondary_id->verified){
                    $res = $this->api->medicareVerification($caregiver);
                    if($res['status'] == 200){
                        $caregiver->secondary_id->verified = $res['success'];
                        $this->info('Medicare Verification = ' . ($res['success'] ? 'true' : 'false'));
                        $caregiver->secondary_id->save();
                    } else{
                        $this->error('Medicare Verification Error: ' . $res['message']);
                    }
                }

            }else{
                $not_ready = [];
                foreach ($progress['verification_requirements'] as $field){
                    $not_ready[] = $field['name'];
                }
                $this->info('Verification isn`t possible, not ready for verification: '. implode(', ', $not_ready));
            }
        }
    }
}
