<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DropAllTables extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'dat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Drops all the tables from the database.';

    /**
     * Create a new command instance.
     *
     * @return \App\Console\Commands\DropAllTables
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (getenv('APP_ENV') === 'production') {
            exit('This is a production database. We can\'t drop tables on it');
        }

        //Dropping Views
        $views = DB::select("SHOW FULL TABLES IN " . getenv('DB_DATABASE') . " WHERE TABLE_TYPE LIKE 'VIEW'");
        foreach ($views as $view) {
            $view = (array)$view;
            $view_name = $view['Tables_in_' . getenv('DB_DATABASE')];
            DB::statement("DROP VIEW $view_name");
            $message = sprintf("<info>View</info> %s <info>has been dropped.</info>", $view_name);
            $this->getOutput()->writeln($message);
        }

        $tables = [];

        // We allow deleting rows without foreign keys constraint
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //find all tables
        foreach (DB::select('SHOW TABLES') as $v) {
            $tables[] = array_values((array)$v)[0];
        }

        //drop all tables
        foreach ($tables as $table) {
            $table = str_replace('do5_', '', $table);
            Schema::drop($table);
            $message = sprintf("<info>Table</info> %s <info>has been dropped.</info>", $table);
            $this->getOutput()->writeln($message);
        }

        // We put back the configuration for data integrity
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $this->call('migrate:refresh');
        $this->call('db:seed');

        return 0;
    }
}
