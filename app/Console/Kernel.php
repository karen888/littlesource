<?php

namespace App\Console;

use App\Console\Commands\DropAllTables;
use App\Console\Commands\DvsApi;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\CloseJobsCommand;
use App\Console\Commands\NotificationsSend;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CloseJobsCommand::class,
        NotificationsSend::class,
        DropAllTables::class,
        DvsApi::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('job:close')->daily();
        $schedule->command('notifications:send 30min')->everyThirtyMinutes();
        $schedule->command('notifications:send 1h')->hourly();
        $schedule->command('notifications:send 1d')->daily();
    }
}
