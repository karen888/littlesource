<?php

namespace App\ApiServices;


use App\ApiServices\Models\DvsApiAccess;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;

class DvsApiService
{
    /**
     * Required properties for DVS API
     */
    private $user_name, $password, $grant_type, $access_token, $url;

    /**
     * Property for the HTTP client
     */
    private $debag;

    /**
     * Properties for local testing
     */
    private $test_status, $local_test;

    /**
     * DvsApiService constructor.
     */
    public function __construct()
    {
        $this->url = env('DVS_URL');
        $this->user_name = env('DVS_USER_NAME');
        $this->password = env('DVS_PASSWORD');
        $this->grant_type = env('DVS_GRANT_TYPE');

        $this->debag = env('DVS_DEBUG', false);

        $this->test_status = env('DVS_TEST_RESPONSE_STATUS', 200);
        $this->local_test = env('DVS_LOCAL_TEST', false);
    }

    /**
     * Initialization API Token
     */
    private function initToken(){
        if(!$this->access_token){
            $now = Carbon::now();
            $access = DvsApiAccess::query()
                ->where('expires', '>', $now)
                ->where('issued', '<', $now)
                ->first();
            if($access){
                $this->access_token = $access->access_token;
            }else{
                $data = [
                    'username' => $this->user_name,
                    'password' => $this->password,
                    'grant_type' => $this->grant_type
                ];
                $res = self::callApi('POST', 'services/Authorization/token', $data);
                if($res['status'] == 200){
                    DvsApiAccess::create([
                        'access_token' => $res['access_token'],
                        'token_type' => $res['token_type'],
                        'issued' => $res['.issued'],
                        'expires' => $res['.expires']
                    ]);
                    $this->access_token = $res['access_token'];
                }
            }
        }
    }

    /**
     *  Call DVS API
     *
     * @param $method
     * @param $route
     * @param null $data
     * @return mixed
     */
    private function callApi($method, $route, $data = null){
        $url = $this->url .'/'. $route;
        $options = ['http_errors' => $this->debag];
        if($this->access_token){
            $options['headers']['Authorization'] = 'Bearer ' . $this->access_token;
        }
        if(strcasecmp($method, 'post') == 0){
            $options['headers']['Content-Type'] = 'application/x-www-form-urlencoded';
            $options['form_params'] = $data;
        } else if(strcasecmp($method, 'get') == 0){
            $options['query'] = $data;
        }

        $guzzleClient = new Client();
        $response = $guzzleClient->request($method, $url, $options);

        $body = (string)$response->getBody();
        $resData = json_decode($body, true);

        if (gettype($resData) != 'array') {
            $resData = [];
        }

        if($body === 'true' || $body === 'false'){
            $resData['success'] = $body;
        }

        $resData['status'] = $response->getStatusCode();
        $resData['message'] = self::getStatusMessage($resData['status']);

        return $resData;
    }

    /**
     * Passport Verification
     *
     * @param User $user
     * @return mixed
     */
    public function passportVerification(User $user){
        self::initToken();
        $date = getdate(strtotime($user->employee_info->birthday));
        $data = [
            'firstName' => $user->first_name,
            'lastName' => $user->last_name,
            'DOBDay' => (Integer)$date['mday'],
            'DOBMonth' => (Integer)$date['mon'],
            'DOBYear' => (Integer)$date['year'],
            'yearOnly' => (Boolean)$user->driver_licence_id->year_only ? 'true' : 'false',
            'travelDocNumber' => $user->passport_id->id_number,
            'gender' => isset($user->gender[0]) ? self::getGenderLetter($user->gender[0]->name) : 'X'
        ];
        return self::callApi('GET', 'services/DVS/API/Passport', $data);
    }

    /**
     * DriverLicence Verification
     *
     * @param User $user
     * @return mixed
     */
    public function driverLicenceVerification(User $user){
        self::initToken();
        $date = getdate(strtotime($user->employee_info->birthday));
        $data = [
            'lastName' => $user->last_name,
            'firstName' => $user->first_name,
            'DOBDay' => (Integer)$date['mday'],
            'DOBMonth' => (Integer)$date['mon'],
            'DOBYear' => (Integer)$date['year'],
            'yearOnly' => (Boolean)$user->driver_licence_id->year_only ? 'true' : 'false',
            'licenceNumber' => $user->driver_licence_id->id_number,
            'registrationState' => self::getStateName($user->driver_licence_id->state_id)
        ];
        return self::callApi('GET', 'services/DVS/API/DrivingLicence', $data);
    }

    /**
     * Medicare Verification
     *
     * @param User $user
     * @return mixed
     */
    public function medicareVerification(User $user){
        self::initToken();
        $date = getdate(strtotime(str_replace('/', '-', $user->secondary_id->expiry_date)));
        $data = [
            'cardNames' => $user->secondary_id->card_names,
            'cardExpiryDay' => $date['mday'],
            'cardExpiryMonth' => $date['mon'],
            'cardExpiryYear' => $date['year'],
            'cardNumber' => $user->secondary_id->id_number,
            'cardType' =>  self::getCardType((Integer)$user->secondary_id->card_type),
            'individualRefNumber' => $user->secondary_id->individual_ref_number
        ];
        return self::callApi('GET', 'services/DVS/API/Medicare', $data);
    }


    /**
     * Get message by HTTP status code
     *
     * @param $code
     * @return string
     */
    public function getStatusMessage($code){
        switch ($code){
            case 200: return 'Success';
            case 204: return 'Resource not found';
            case 400: return 'Bad Request';
            case 401: return 'Authorization Failure';
            case 500: return 'Internal Server Error';
            case 503: return 'Service Unavailable';
            default:  return '';
        }
    }

    /**
     * Get card type letter
     *
     * @param $code
     * @return string
     */
    public function getCardType($code){
        switch ($code){
            case 0: return 'G';
            case 1: return 'B';
            case 2: return 'Y';
            default:  return '';
        }
    }

    /**
     * Get state name
     *
     * @param $state id
     * @return string
     */
    public function getStateName($stateId){
        switch ($stateId) {
            case 1:
                return 'ACT';
                break;
            case 2:
                return 'NT';
                break;
            case 3:
                return 'SA';
                break;
            case 4:
                return 'WA';
                break;
            case 5:
                return 'NSW';
                break;
            case 6:
                return 'QLD';
                break;
            case 7:
                return 'VIC';
                break;
            case 8:
                return 'TAS';
                break;
            default:
                return '';
                break;
        }
    }

    /**
     * Get gender letter
     *
     * @param $name
     * @return string
     */
    public function getGenderLetter($name){
        switch ($name){
            case 'Male': return 'M';
            case 'Female': return 'F';
            default:  return 'X';
        }
    }

    /**
     * @param $route
     * @param $code
     * @return array|mixed
     */
    public function testResponse($route, $code){
        $now = Carbon::now();
        $response = '';
        if (strcasecmp($route, 'services/Authorization/token') == 0){
            if($code == 200)
                $response = '{"access_token":"ngzpeISijCSVg54YUpXGfhVeAxKacfLJf0HfEDcyEbXpSozp2zMYnWN9uplGJkdT0GGLeSY2eX2X82J5ZN1urXsj14lC-_78ecxCx8ZJ45sUOsCYhlvpRnkm1_FmA9Sr5IaQDiQDDeXteIF6TWWJs9Ft9hfOFtB0QSpBB7_XzcTk5Ruu8juXYnYEf6kK4fR2wbKmMNOfP_UBZgfLK7CnPzBrGmpuxEhja1KTKHF3z2trys6ssHeZfmaQGEgLfgC8ayb7XgM2tkR6nF-3AA81nbeRXQJPtPPJon4NY4kQHVVdIdb9p--V36_YCL_PKv2NKO-lqD7s83UhNebXSJ4g1P-mBtpm6ee48B7YBgPPeLOnuVSmOacqUFMcyK-_Dz9oU3Syo1prEL9Rfz6kjyb6eg5aZ04dJglk8MGZDqPbBZgL8-J5umAD2E-LM8s534IdesZfK89ce-rW7SNYyrhf11KJE-4_EJFav19m3OVpEKg1-qJ3Ynhbh-qqvwXGvyZn","token_type":"bearer","expires_in":1209599,"username":"PRMTest112",".issued":"'.$now->toDateTimeString().'",".expires":"'.$now->addHour(8)->toDateTimeString().'"}';
        } else if (
            strcasecmp($route, 'services/DVS/API/Passport') == 0 ||
            strcasecmp($route, 'services/DVS/API/DrivingLicence') == 0 ||
            strcasecmp($route, 'services/DVS/API/Medicare') == 0
        ){
            if($code == 200) $response = (rand(1, 2)%2) === 0;
        }

        $resData = [];
        if($response === true || $response === false){
            $resData['success'] = $response;
        }else{
            $resData = json_decode($response, true);
        }
        $resData['status'] = $code;
        $resData['message'] = self::getStatusMessage($code);
        return $resData;
    }
}
