<?php

namespace App\ApiServices\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DvsApiAccess extends Model
{
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['access_token', 'token_type', 'issued', 'expires'];

    /**
     * Set value issued date
     *
     * @param $value
     */
    public function setIssuedAttribute($value)
    {
        $this->attributes['issued'] = new Carbon($value);
    }

    /**
     * Set value expires date
     *
     * @param $value
     */
    public function setExpiresAttribute($value)
    {
        $this->attributes['expires'] = new Carbon($value);
    }
}
