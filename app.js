var mysql = require('mysql');
var scribe = require('scribe-js')(),
    console = process.console,
    config = require('./config.js'),
    redis = require('redis'),
    requestify = require('requestify');
var io = require('socket.io').listen(config.serverPort); 
var redisClient = redis.createClient(),
    client = redis.createClient();
    
console.log('Server started on ' + config.domain + ':' + config.serverPort);


redisClient.subscribe('show.winners');
redisClient.subscribe('queue');
redisClient.subscribe('newDeposit');
redisClient.subscribe('depositDecline');

//fastmessages
redisClient.subscribe('newBetDouble');
redisClient.subscribe('newBankDouble');
redisClient.subscribe('newBetClassic');
redisClient.subscribe('newBankClassic');
redisClient.subscribe('newBetElite');
redisClient.subscribe('newBankElite');

//game
redisClient.subscribe('countGameToday');
redisClient.subscribe('maxPriceToday');
redisClient.subscribe('priceAll');
redisClient.setMaxListeners(0);
redisClient.on("message", function (channel, message) {
    if (channel == 'depositDecline' || channel == 'queue') {
        io.sockets.emit(channel, message); 
    }
    else if (channel == 'show.winners') {
        clearInterval(timer);
        timerStatus = false;
        console.log('Force Stop');
        game.status = 3;
        showSliderWinners();
    }
    else if (channel == 'newDeposit') {
        io.sockets.emit(channel, message);

        message = JSON.parse(message);
        client.set("newBankClassic", message.gamePrice);
        io.sockets.emit("newBetClassic", message.bet.price);
        io.sockets.emit("newBankClassic", message.gamePrice);
        if (!timerStatus && message.gameStatus == 1) {
            game.status = 1;
            startTimer(io.sockets);
        }
    }
    else{
        console.log(channel, message);
        io.sockets.emit(channel, message);
    }
});

/* CHAT MESSGAGE */
redisClient.subscribe('timeout.maza');

/* CHAT MESSGAGE END */

//mysql

var pool  = mysql.createPool({
    connectionLimit : 10,
    host     : config.db.host,
    user     : config.db.user,
    password : config.db.password,
    database : config.db.database
});

function query(sql, callback) {
    if (typeof callback === 'undefined') {
        callback = function() {};
    }
    pool.getConnection(function(err, connection) {
        if(err) return callback(err);
        // console.info('Ид соединения с базой данных: '+connection.threadId);
        connection.query(sql, function(err, rows) {
            if(err) return callback(err);
            connection.release();
            return callback(null, rows);
        });
    });
}


/* USERS ONLINE SITE */



userlist = {};

var chatMessage = [];
var maxMessage = 15;

io.sockets.on('connection', function(socket) {

    updateOnline();

    //chat
    socket.on('chatmessage', function (newMessage) {
        try {
            newMessage = JSON.parse(newMessage);
            var result = '';
            if(newMessage.token == 'null'){
                io.sockets.emit('chat', {'userid': newMessage.user, 'error': 'You must login!'});
                return;              
            }
            rtok = base64_decode(newMessage.token);
            var sql    = 'SELECT username, avatar, is_admin, banchat, steamid64 FROM users WHERE tokens = ' + pool.escape(rtok);
            query(sql, function(err, row) {
                if((err) || (!row.length)) return socket.disconnect();
                user = row[0];
                if(user.banchat == 1){
                    io.sockets.emit('chat', {'userid': newMessage.user, 'error': 'You are banned in chat.'});
                    return;
                }
                var regex = new RegExp("(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)");
                if(regex.test(newMessage.messages)){
                    io.sockets.emit('chat', {'userid': newMessage.user, 'error': 'Links banned!'});
                    return;
                }        

				var regex2 = new RegExp(".*<.*");	
				if(regex2.test(newMessage.messages)){
                    io.sockets.emit('chat', {'userid': newMessage.user, 'error': 'fuck off!'});
                    return;
                } 
				var regex3 = new RegExp("#");	
				if(regex3.test(newMessage.messages)){
                    io.sockets.emit('chat', {'userid': newMessage.user, 'error': 'fuck off!'});
                    return;
                } 				
                if(user.is_admin == 1){
                    user.avatar = 'http://csfate.com/admin.png';
                    user.username = "<font color='red'><b>Admin</b></font>";
                    if(newMessage.messages == '/clear'){
                        chatMessage = [];
                        return;
                    }
                }
                result = JSON.stringify({user: user, message: newMessage.messages});
                n = [];
                if(chatMessage.length>maxMessage){
                    chatMessage.splice(0, 1);
                }
                n.push(result);
                chatMessage.push( result );
                io.sockets.emit('chat', n);
            });

        } catch (e) {
        }    
    });
    socket.on('loadmessage', function(data){
        io.sockets.emit('chat', {user:data, message:chatMessage});
    });

});

function pushChat(){
   
}

setInterval(function(){io.sockets.emit('online', Object.keys(io.sockets.adapter.rooms).length);}, 5000);
function updateOnline(){
    io.sockets.emit('online', Object.keys(io.sockets.adapter.rooms).length);
}

/* USERS ONLINE SITE END */


var steamStatus = [],
    game,
    timer,
    ngtimer,
    timerStatus = false,
    timerTime = 90,
    preFinishingTime = 3;
getPriceItems();    
getUpdatePriceItems();
getActualCurs();
getCurrentGame();
checkSteamInventoryStatus();

var preFinish = false;

// function updateAll() {
//     requestify.post('http://' + config.domain + '/elitemazapi/newInfo', {
//         secretKey: config.secretKey
//     })
//         .then(function(response) { 
//             data = response.body;
//             io.sockets.emit('updateAll', data);
//             // console.tag('updateAll').log('Allupd');
//         }, function(response) {
//             console.tag('updateAll').log('Something wrong [updateAll]');
//         });
// }
// setInterval(function(){updateAll();}, 5000);



function startTimer() {
    var time = timerTime;
    timerStatus = true;
    clearInterval(timer);
    console.tag('Game').log('Game start.');
    timer = setInterval(function () {
        console.tag('Game').log('Timer:' + time);
        io.sockets.emit('timer', time--);
        if ((game.status == 1) && (time <= preFinishingTime)) {
            if (!preFinish) {
                preFinish = true;
                setGameStatus(2);
            }
        }
        if (time <= 0) {
            clearInterval(timer);
            timerStatus = false;
            console.tag('Game').log('Game end.');
            io.sockets.emit('timeout.maza', 1);
            
            setTimeout(function(){
                showSliderWinners();
            }, 3000);
            
            
        }
    }, 1000);
}

function startNGTimer(winners) {
    // io.sockets.emit('timeout.maza', 0);
    var time = 20;
    clearInterval(ngtimer);
    io.sockets.emit('sliderer', winners);
    setGameStatus(3);
    ngtimer = setInterval(function () {
        console.tag('Game').log('NewGame Timer:' + time);
        times = time--;
        io.sockets.emit('timenew', times);
        if (time == 7) {
            getPreviousWinner(); 
        }
        if (time <= 0) {
            clearInterval(ngtimer);
            newGame();
        }
    }, 1000); 
}

function getPreviousWinner(){
    requestify.post('http://'+config.domain+'/mazapi/getPreviousWinner', {
        secretKey: config.secretKey 
    }) 
        .then(function(response) {
             //data = JSON.parse(response);
             data = response.body;
            console.tag('getPreviousWinner').log('getPreviousWinner! #');
            io.sockets.emit('getPreviousWinner', data);
        },function(response){
            console.tag('getPreviousWinner').error('Something wrong lastwinner');
            setTimeout(getPreviousWinner, 1000);  
        });
}
function getUpdatePriceItems() {
    requestify.get('http://' + config.domain + '/gamedouble/updatepsrices')
        .then(function (response) {
            console.tag('SteamPrices').log('Prices for items added');
        }, function (response) {
            console.tag('SteamPrices').log('Something wrong [updatepsrices]');
        });  
}
function getPriceItems() {
    requestify.post('http://' + config.domain + '/mazapi/getPriceItems', {
        secretKey: config.secretKey
    })
        .then(function (response) {
            console.tag('SteamPrices').log('Prices for items added');
        }, function (response) {
            console.tag('SteamPrices').log('Something wrong [getPriceItems]');
        });   
}
function getActualCurs() {
    requestify.get('http://' + config.domain + '/ackursus', {
        secretKey: config.secretKey
    })
        .then(function (response) {
            console.tag('ackursus').log('save');
        }, function (response) {
            console.tag('ackursus').log('Something wrong [ackursus]');
        }); 
}


function getCurrentGame() {
    requestify.post('http://' + config.domain + '/mazapi/getCurrentGame', {
        secretKey: config.secretKey
    })
        .then(function (response) {
            game = JSON.parse(response.body);
            console.tag('Game').log('Current Game #' + game.id);
            if (game.status == 1) startTimer();
            if (game.status == 2) startTimer();
            if (game.status == 3) newGame();
        }, function (response) {
            console.tag('Game').log('Something wrong [getCurrentGame]');
            setTimeout(getCurrentGame, 1000);
        });
}

function newGame() {
    requestify.post('http://'+config.domain+'/mazapi/newGamee12322', {
        secretKey: config.secretKey
    })
        .then(function (response) {
            preFinish = false;
            game = JSON.parse(response.body); 
            console.tag('Game').log('New game! #' + game.id);
            setGameStatus(0);
            setGameStatus(0); 
            io.sockets.emit('newGame', game);
            // bot.handleOffers();
        }, function (response) {
            console.tag('Game').error('Something wrong [newGame]');
            setTimeout(newGame, 1000);
        });
}

function showSliderWinners() {
    requestify.post('http://' + config.domain + '/mazapi/getWinners', {
        secretKey: config.secretKey
    })
        .then(function (response) {
            var winners = response.body;
            console.tag('Game').log('Show slider!');
            setGameStatus(3);
            startNGTimer(winners);
        }, function (response) {
            console.tag('Game').error('Something wrong [showSlider]');
            setTimeout(showSliderWinners, 1000);
        });
}

function setGameStatus(status) {
    requestify.post('http://' + config.domain + '/mazapi/setGameStatus', {
        status: status,
        secretKey: config.secretKey
    })
        .then(function (response) {
            game = JSON.parse(response.body);
            console.tag('Game').log('Set game to a prefinishing status. Bets are redirected to a new game.');
        }, function (response) {
            console.tag('Game').error('Something wrong [setGameStatus]');
            setTimeout(setGameStatus, 1000);
        });
}

function checkSteamInventoryStatus() {
    requestify.get('https://api.steampowered.com/ICSGOServers_730/GetGameServersStatus/v1/?key=' + config.apiKey)
        .then(function (response) {
            var answer = JSON.parse(response.body);
            steamStatus = answer.result.services;
            console.tag('SteamStatus').info(steamStatus);
            client.set('steam.community.status', steamStatus.SteamCommunity);
            client.set('steam.inventory.status', steamStatus.IEconItems);
        }, function (response) {
            console.log('Something wrong [5]'); 
        });
}

setInterval(checkSteamInventoryStatus, 900000);
setInterval(getPriceItems, 43200000); 
setInterval(getUpdatePriceItems, 43200000); 
setInterval(getActualCurs, 43200000);   

function updateUSERCOIN() {
    requestify.post('http://' + config.domain + '/mazapi/giveusercoin', {secretKey: config.secretKey})
        .then(function(response) { 
            // console.tag('giveusercoin').log('Allupd');
        }, function(response) {
            console.tag('giveusercoin').log('Something wrong [giveusercoin]');
        });
}
setInterval(function(){updateUSERCOIN();}, 10000);



function base64_decode( data ) { 

    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i=0, enc='';

    do {  // unpack four hexets into three octets using index points in b64
        h1 = b64.indexOf(data.charAt(i++));
        h2 = b64.indexOf(data.charAt(i++));
        h3 = b64.indexOf(data.charAt(i++));
        h4 = b64.indexOf(data.charAt(i++));

        bits = h1<<18 | h2<<12 | h3<<6 | h4;

        o1 = bits>>16 & 0xff;
        o2 = bits>>8 & 0xff;
        o3 = bits & 0xff;

        if (h3 == 64)     enc += String.fromCharCode(o1);
        else if (h4 == 64) enc += String.fromCharCode(o1, o2);
        else               enc += String.fromCharCode(o1, o2, o3);
    } while (i < data.length);

    return enc;
}