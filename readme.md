# Little Ones

Little Ones provides care at a click! We connect parents / guardians with qualified caregivers. Whether you need a quick break just for an hour or longer term care, Little Ones can help you. It's very simple to use, just jump onto Little Ones and you can see which certified caregivers are available in your area, it's just like ordering an Uber! The best part about Little Ones is it's available to you around the clock, not just in business hours, like childcare centres.

The safety and care of your Little Ones is our top priority, we ensure all of our caregivers are qualified, you can read more about our verification process here! You have access to reviews of each caregiver from other parents and can see each caregivers experience in their Little Ones profile. Rest easy knowing that your Little Ones are in good hands!.

# Installation!

Requirements:
  - Vagrant
  - VirtualBox

After install VirtualBox and Vagrant, please follow these steps.

Clone the repository or download it (make sure you have the access to the repository).
```sh
$ git clone git@bitbucket.org:truelifeajf/littleones-new.git
```

Go into the project source and start Vagrant. This will take a while until download the Vagrant box and install its dependencies.
```sh
$ cd littleones-new
$ vagrant up
```

For Mac and Linux users, add the IP to the hosts file.
```sh
# Add this entry in the file /etc/hosts
192.168.10.13 little-ones.test
```

For Windows users, add the IP to the hosts file.
```sh
# Add this entry in the file c:\Windows\System32\Drivers\etc\hosts
192.168.10.13 little-ones.test
```

Ready!

Access the project site: https://little-ones.test

You can access the admin dashboard using email: dev@littleones.com.au, password: 123456 in the login form

Run vagrant halt to turn off the machine once you are done. Run vagrant up before start working.

## vagrant ssh ##
In order to run migrations, seeds, composer etc, you need to ssh the virtual machine

Use the following command in terminal to ssh the virtual machine
```sh
vagrant ssh
```

Go to the directory where the laravel installation is
```sh
cd /vagrant
```

Run the commands you need


## GIT Workflow ##
* Branch master = little-ones.com.au (Only Kostas can write)
* Branch development
* Branch staging = staging.little-ones.com.au (Only Kostas can write)
* Create a branch for every hotfix or feature to work indepent.
* Feature branch: name your feature branch like this feature/my-new-feature. Feature branch must always be a branch of development
* Hotfix branch: name your hotfic branch like this hotfix/my-hotfix. Hotfix branch must always be a branch of master
* Work finished and ready to release according to you? Create a pull request from your branch against the development branch and add Kostas as reviewer. Make sure all conflicts are resolved in the branch by rebasing development branch often. Important! mention in your pull request description if any command needed to run after deployment. i.e. php artisan or anything else.
* For hotfix branch you create a pull request against master and resolve conflicts by rebasing master branch often.
* Reviewer will merge your pull request to development once it is tested and has no bugs.
* Every week or every 2 weeks, the development branch will merged to master branch in order to publish new features that are ready to released till that time.
* Feature branches will often merged to staging branch for reviewing/testing by client
* Remember to often merge development branch into your feature branch to get the latest updates

## Database ##
* Create migrations for any database structure changes
* Use seeds for data changes
* Access vagrant database using 192.168.10.13:3306. database name: little-ones, username: homestead, password: secret

## Coding Standards ##
* Follow Laravel coding standards