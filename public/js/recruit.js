$(document).ready(function(){

    appl_modal = new applModal();
});

function delJobsList(form, msg) {
    form.closest('tr').remove();
}

function closeApplModal(form, msg) {
    appl_modal.closeModal();
}

var applModal = function () {
    var self = this;
    var modal_el = null;
    var par_el = null;

    this.closeModal = function() {
        modal_el.modal('hide');
    };

    var constructor = function() {
        modal_el = $('#appl-send-message');
    };
    constructor();

    $('.appl-modal-message').on('click', function (e) {
         e.preventDefault();

         par_el = $(this).closest('.appl-items-bl');

         var user_id = $(this).data('user-id');
         var name = par_el.find('.appl-items-name').html();
         var photo = par_el.find('.appl-items-photo').html();
         
         modal_el.find('input[name="user_id"]').val(user_id);
         modal_el.find('textarea[name="message"]').val('');
         modal_el.find('.appl-modal-photo').html(photo);
         modal_el.find('.appl-modal-username').html(name);


         modal_el.modal('show');
    });
    
};
