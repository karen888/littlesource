$(document).ready(function(){
    token = $('#_token').val();

    //var ajax_req = new ajaxReq();
    ajaxEvent();

    topSearch();
    skillsSel();
    uploadAddFile();
    //feedbackRate();
    var scroll = null;
    $("body").on('click', '.notes a', function(){
        if(scroll === null){
            scroll = $(window).scrollTop();
            console.log(scroll);
        }else{
            // setTimeout(function(){
                $(window).scrollTop(scroll);
                scroll = null;
                console.log(scroll);
            // }, 2000);
        }
    });
    $("#login-form").on('focus', 'input', function(){
        name = $(this).attr('name');
        $(".field-"+name+"-show").toggleClass('focus');
    });

    $("#login-form").on('blur', 'input', function(){
        name = $(this).attr('name');
        $(".field-"+name+"-show").toggleClass('focus');
    });

    $("#registr-form").on('focus', 'input', function(){
        name = $(this).attr('name');
        $(".field-"+name+"-show").toggleClass('focus');
    });

    $("#registr-form").on('blur', 'input', function(){
        name = $(this).attr('name');
        $(".field-"+name+"-show").toggleClass('focus');
    });

    $(".padd-top-30").on('focus', 'input', function(){
        name = $(this).attr('name');
        $(".field-"+name+"-show").toggleClass('focus');
    });

    $(".padd-top-30").on('blur', 'input', function(){
        name = $(this).attr('name');
        $(".field-"+name+"-show").toggleClass('focus');
    });

    $("#login-form").on('keyup', 'input', function(){
        name = $(this).attr('name');
        val = $(this).val();
        if(val == ''){
            $(".field-"+name+"-show").css('display', 'none');
        }else{
            $(".field-"+name+"-show").css('display', 'block');
        }
    });
    $("#registr-form").on('keyup', 'input', function(){
        name = $(this).attr('name');
        val = $(this).val();
        if(val == ''){
            $(".field-"+name+"-show").css('display', 'none');
        }else{
            $(".field-"+name+"-show").css('display', 'block');
        }
    });
    $("#registr-form").on('click', '#resend', function(){
        var id = $(this).attr("data-id");
        var _token = $('#_token').attr("value");
        $.post( "/auth/resend", { _token: _token , id: id })
          .done(function( data ) {
            // alert( "Data Loaded: " + data );
          });
    });
    
    $(".padd-top-30").on('keyup', 'input', function(){
        name = $(this).attr('name');
        val = $(this).val();
        if(val == ''){
            $(".field-"+name+"-show").css('display', 'none');
        }else{
            $(".field-"+name+"-show").css('display', 'block');
        }
    });


    $(document).bind( "mouseup touchend", function(event){
        console.log($(event.target).closest(".list").length);
      if( $(event.target).closest(".open").length ){
        console.log("list");
      }else{
        $('.customselect .list').css("display", 'none');
      }
      // event.stopPropagation();
    });    
});



//function feedbackRate() {
   //$('#feedback-rate').rating('refresh' ,{
       //showClear: false
   //});
//}

function uploadAddFile() {
    $('.upload-add-file').on('click', function(e) {
        e.preventDefault();
        var form = $(this).closest('form');
        var copy = form.find('.upload-file-block:first').clone();
        copy.find('input').val('');
        copy.find('.upload-file-remove').parent().css('display', 'table-cell');

        copy.insertBefore($(this));
    });

    $(document).on('click', '.upload-file-clear', function(e) {
        e.preventDefault();
        $(this).closest('.upload-file-block').find('input').val('');
    });

    $(document).on('click', '.upload-file-remove', function(e) {
        e.preventDefault();
        $(this).closest('.upload-file-block').remove();
    });

    $(document).on('change', '.btn-file :file', function() {
        var input = $(this);
        var numFiles = input.get(0).files ? input.get(0).files.length : 1;
        var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input = $(this).parents('.input-group').find(':text');
        input.val(label);
    });
}

function topSearch () {
    $('#top-search-sel').on('change', function() {
        var val = $(this).val();
        var form = $(this).closest('form');
        
        form.attr('action', val);
    });
}

function skillsSel() {
    $("#my-skills").select2({
        multiple: true,
        placeholder: "Find a tag",
        ajax: {
            url: "/skills/ajax-list",
            dataType: 'json',
            type: "POST",
            cache: "true",
            data: function (term, page) {
                return {
                    _token: token,
                    skill: term.term
                };
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data, function(obj) {
                        return { id: obj.id, text: obj.skill_name };
                    })                    
                };                
            }
        },
    });
}

function delTd(form, msg) {
    form.closest('tr').remove();
}

var ajaxReq = function () {
    var self = this;
    var send_but = null;
    var form = null;
    var form_act = null;
    var alert_bl = null;
    var data_send = null;
    var error_ajax = null;
    var redir = false;

    var send = function () {
        var form_data =  new FormData(form[0]);
        
        $.ajax({
            type: "POST",
            url: form_act,
            //data: form.serialize(),
            data: form_data,
            processData: false,
            contentType: false,
            dataType: "JSON",
            xhrFields: {
                // add listener to XMLHTTPRequest object directly for progress (jquery doesn't have this yet)
                onprogress: function(progress) {
                }
            },            
            success: function(msg){
                respProc(msg);
            },
            error: function (msg) {
                if (error_ajax) alert('err ajax');
            },
            timeout: function (msg) {
                alert('timeout');
            },
            beforeSend: function() {
                send_but.trigger("start.search");
            },
            complete: function() {
                if (!redir) send_but.trigger("finish.search");
            }            
      });
    };

    var respProc = function (msg) {
        redir = false;
        alert_bl.hide();

        if(parseInt(msg.status)==1) {
            redir = true;
            window.location.href = msg.msg;
        } else if (msg.status == 2) {
            alert_bl.html(msg.msg);
            alert_bl.show();
        } else if (msg.status == 3) {
            var func = msg.msg[0].name;
            window[func](form, msg.msg);
        } else if (msg.status == 4) {
            alert(msg.msg);
        } else if (msg.status == 5) {
            redir = true;
            alert_bl.html(msg.msg.msg);
            alert_bl.show();
            window.location.href = msg.msg.url;
        } else if (msg.status == 6) {
            alert_bl.html(msg.msg);
            alert_bl.show();
        } else if (msg.status == 7) {
            alert_bl.html(msg.msg);
            alert_bl.show();
            form.find('input').not('input[name="_token"]').val('');
        } else {
            if (form.find('.form-group-m').length) {
                var form_cl = 'form-group-m';
            } else {
                var form_cl = 'form-group';
            }

            $.each(msg.msg, function(i, val){
                form.find('.'+form_cl+'[data-name="'+val.name+'"]').addClass('has-error');
                if (val.error != '') {
                    form.find('[name="'+val.name+'"]').popover({content : val.error, placement: 'top', trigger: 'focus'});
                }
                        
            });  
            form.find('*').popover('show');
        }
    };

    var showPopover = function () {
        form.find('*').not($(this)).popover('hide');
        $(this).popover('show');
    };    

    var hidePopover = function () {
        form.find('*').popover('hide');
    };    

    var hideProperErr = function () {
        form.find('.add').val('');
        form.find('*').popover('destroy');
        form.find(".form-group").removeClass('has-error');
        form.find(".form-group-m").removeClass('has-error');
    };

    self.setData = function (el) {
        var act_sub = true;

        var conf_msg = el.data('confirm');
        if (conf_msg) {
            act_sub = confirm(conf_msg);
        }

        if (act_sub) {
            data_send = null;

            send_but = el;
            form = el.closest('form');
            form_act = form.attr('action');
            alert_bl = form.find('.alert');

            hideProperErr();

            var ajsend = el.data('ajsend');
            if (ajsend != 'off') mch_ajsend(el);

            var func_call = el.data('func');
            if (func_call) window[func_call](form);

            var error = el.data('error');
            if (error == 0) error_ajax = 0;
            else error_ajax = 1;

            //form.submit();
            send();
        }

        return false;
    };

    var constructor = function () {
    };
    constructor();
};
function ajaxEvent() {

    $('body').on("click", ".send-req-form", function(e){
        var ajax_req = new ajaxReq();
        return ajax_req.setData($(this)); 
    });
}


function mch_ajsend(el) {
    el.on("start.search", function() {
        el.button('loading');
    });
    el.on("finish.search", function() {
        el.button('reset');
    });
}


function print_r(arr, level) {
    var print_red_text = "";
    if(!level) level = 0;
    var level_padding = "";
    for(var j=0; j<level+1; j++) level_padding += "    ";
    if(typeof(arr) == 'object') {
        for(var item in arr) {
            var value = arr[item];
            if(typeof(value) == 'object') {
                print_red_text += level_padding + "'" + item + "' :\n";
                print_red_text += print_r(value,level+1);
		} 
            else 
                print_red_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
        }
    } 

    else  print_red_text = "===>"+arr+"<===("+typeof(arr)+")";
    alert(print_red_text);
}
