$(document).ready(function(){

    $('#post-job-type-price').on('change', function() {
        typePrice($(this));
    });
    //dispCatSub($('#post-job-category-main option:selected').data('par'), false);
    $('#post-job-category-main').on('change', function() {
        var val = $(this).val();
        dispCatSub(val, true);
    });
});
 
function typePrice(el) {
    var val = el.val();
    if (val == 'fixed') {
        $('.post-job-fixed-price').show();
        $('.post-job-hourly-price').hide();
    } else {
        $('.post-job-fixed-price').hide();
        $('.post-job-hourly-price').show();
    }
}

function dispCatSub(main_id, sel_def) {
    var el = $('#post-job-category-sub');
    el.find('option').hide();
    el.find('option[data-par="'+main_id+'"]').show();
    el.find('option[data-par="0"]').show();

    if (sel_def) {
        el.find('option').removeAttr('selected');
        el.find('option[data-par="0"]').attr('selected', 'selected');
    }
}
