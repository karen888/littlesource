$(document).ready(function(){

    dropdownNotification();
    chNotifications();
});

function chNotifications() {
    var form = $('#ch-notifications');
    var new_notif = $('#dropdown-toggle-notification').find('i');
    setInterval(function() {
        form.find('.send-req-form').trigger('click');
    }, 5000);
}

function chNotificationsRes(form, msg) {
    var par = $('#dropdown-toggle-notification');
    if (msg[1] == 1) {
        par.find('i').addClass('text-danger');
        par.data('submit', 1);
        par.find('.form_submit').show();
        par.find('.notification-items').remove();
    } else par.find('i').removeClass('text-danger');

    if (msg[2] == 1) {
        $('#menu-messages').show();
    } else $('#menu-messages').hide();

    form.find('input[name="_token"]').val(msg[3]);
    $('#dropdown-toggle-notification').find('input[name="_token"]').val(msg[3]);
}

function dropdownNotification() {
    $('#dropdown-toggle-notification').on('show.bs.dropdown', function () {
        var subm = $(this).data('submit');
        if (subm == 1) {
            $(this).data('submit', 0);
            $(this).find('.send-req-form').trigger('click');
        }
    });
}

function dropdownNotificationRes(form, msg) {
    var par = $('#dropdown-toggle-notification');
    var el_tmp = par.find('.form_submit');

    $.each(msg[1], function(i, val){
        var text = val.notification_text;
        if (val.notification_readed == 0) text = '<strong>'+text+'</strong>';

        el_tmp.before('<li class="notification-items"><a href="'+val.notification_url+'"><small class="text-muted">'+text+'</small></a></li><li class="divider notification-items"></li>');
    });

    el_tmp.hide();
    par.find('i').removeClass('text-danger');
}
