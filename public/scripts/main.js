$(document).ready(function(){
    custom_select();
});


function isMobile(){
    var devices = ['Android', 'BlackBerry', 'iPhone', 'iPad', 'iPod', 'Opera Mini', 'IEMobile', 'Silk'];
    var ua = navigator.userAgent || navigator.vendor || window.opera;

    // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
    for (var i = 0; i < devices.length; i++) if (ua.toString().toLowerCase().indexOf(devices[i].toLowerCase()) > 0) return devices[i];
    return false;
}

function custom_select() {
    custom_select_create();

    if(mobile === true) {
        $('.customselect').removeClass('v2').addClass('v1');
    }

    $('.customselect.v1').each(function(){
        var c=$(this),sel=$('select',c),t=$('.text',c);
        sel.change(function(){
            t.html(sel.children('option:selected').text());

            fireCustomSelectChange();
            $('.artist_products_block .list li[data-value="'+sel.val()+'"]').click();
        });
    });

    $('.customselect.v2').each(function(){
        var c=$(this),sel=$('select',c),t=$('.text',c),tr=$('.trigger',c),ex=$('.list',c);
        tr.click(function(){
            if(c.hasClass('open'))
            {
                c.removeClass('open');
            }
            else
            {
                c.addClass('open');
            }
        });
        $(document).click(function(e){
            var th=$(e.target);
            if(!th.closest('.customselect').length)
            {
                c.removeClass('open');
            }
            else if(!th.closest('.customselect').is(c))
            {
                c.removeClass('open');
            }
        });
        $('.option',ex).click(function(){
            c.removeClass('open');
            sel.val($(this).data('value'));
            t.html($(this).html());

            var data=$(this).data('value');
            $('.artist_products_block .list li[data-value="'+data+'"]').click();

            fireCustomSelectChange();
        });

        // Custom select "selected" option (usage - data-selected=1/0)
        $.each($("select option"), function (index, option){
            if($(option).data('selected') == 1){
                $(option).prop('selected', 1);
                $.each($(".option", ex), function(index, opt) {
                    if($(opt).data('selected')==1)
                    {
                        t.html($(opt).html());
                    }
                });
                fireCustomSelectChange();
            };
        });

        if( isMobile() ){
            c.addClass('mobile');

            sel.on("change", function(){
                $('.option[data-value="'+$(this).val()+'"]',ex).click();
            });
        }
    });
}


function custom_select_create()
{
    $('.customselect .create').each(function(){
        var th = $(this);
        var parent = th.parent();
        var selected = th.children('option:selected');

        parent.append('<div class="trigger"><div class="text_outer"><div class="text">'+selected.text()+'</div></div></div>');
        parent.append('<div class="list"></div>');
        th.children('option').each(function(){
            parent.children('.list').append('<div class="option" data-value="'+$(this).val()+'">'+$(this).text()+'</div>');
        });
    });
}
//# sourceMappingURL=main.js.map
