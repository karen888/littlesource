
//
// https://forms.pfes.nt.gov.au/safent/CheckValidity.aspx?IsValidityCheck=true
//
var Horseman = require('node-horseman');
var prompt = require('prompt');
var fs = require('fs');
var download = require('download-file')
var DeathByCaptcha = require("deathbycaptcha");
var dbc = new DeathByCaptcha('littleones', 'temp50temp');

var scrapConf = {
	'timeout': 90000,
	'loadImages': false,
	'ignoreSSLErrors': true,
	'webSecurity': false
};

//
// captcha text
//
var captcha_text = '';
var schema = {
	properties: {
		given_name: {
			description: 'Enter your Given Name',
			pattern: /^[0-9a-zA-Z]+$/,
			message: 'Given Name must be valid',
			required: true
		},
		surname: {
			description: 'Enter your Surname',
			pattern: /^[0-9a-zA-Z]+$/,
			message: 'Surname must be valid',
			required: true
		},
		clearance: {
			description: 'Enter Clearance Number',
			pattern: /^[0-9]+$/,
			meszsage: 'Clearance must be only numbers'
		},
		date_of_birth: {
			description: 'Enter your Date of Birth like this: 01/31/2017',
			message: 'Date of Birth must be valid'
		}
	}
};


//
// create horseman with custom settings
//
var horseman = new Horseman(scrapConf);
//
console.log('Please input Given Name, Surname, and Clearance or Date of Birth');

//
// start prompt to get user input of card number , and surname
//

prompt.start();

//
// Get two properties from the user: card number, and surname
//

prompt.get(schema, (err, result) => {
	//
	// Log the results
	//

	console.log('Received Input Data');
	console.log('Given Name: ' + result.given_name);
	console.log('Surname: ' + result.surname);
	console.log('Clearance: ' + result.clearance);
	console.log('Date of Birth: ' + result.date_of_birth);
	console.log('\x1b[36m%s\x1b[0m', 'Attempting to check....');
	horseman
		.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0')
		.viewport(1280, 1024)
		.open('https://forms.pfes.nt.gov.au/safent/CheckValidity.aspx?IsValidityCheck=true')
		.waitForNextPage()
		.type('#MainContent_pnlCheckValidity_txtGivenName_I', result.given_name)
		.type('#MainContent_pnlCheckValidity_txtSurname_I', result.surname)
		.type('#MainContent_pnlCheckValidity_txtClearanceNumber_I', result.clearance)
		.type('#MainContent_pnlCheckValidity_dteDateOfBirth_I', result.date_of_birth)
	    .evaluate(function () {
	    	return 'https://forms.pfes.nt.gov.au' + $('#MainContent_pnlCheckValidity_Captcha_IMG').attr('src');
	    })
	    .then(function(url) {
	    	var options = {
	    		directory: './',
	    		filename: 'captcha.png'
	    	};
	    	download(url, options, function(err) {
	    		if (err) throw err
	    		console.log(url);
	    		dbc.solve(fs.readFileSync('./captcha.png'), function(err, id, text) {
	    			if (err) throw err
	    				console.log('Captcha text is ', text);
	    		})
	    	})
	    })
		.close();
});
