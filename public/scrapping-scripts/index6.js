
//
// https://wwccheck.ccyp.nsw.gov.au/Employers/Login?ReturnUrl=%2fEmployers%2fSearch
//
const Horseman = require('node-horseman');
const { validate, isValidDate } = require('./helpers/validation');
const scrapConf = {
    'timeout': 90000,
    'loadImages': false,
    'ignoreSSLErrors': true,
    'webSecurity': false
};

const rules = {
    cardNumber: /^[a-zA-Z0-9]+$/,
    issueNumber: /^[a-zA-Z0-9]+$/,
    surname: /^[a-zA-Z]+$/,
    day: /^[0-9]+$/,
    month: /^[0-9]+$/,
    year: /^[0-9]+$/
};
const arguments = process.argv.slice(2); // removing first two arguments: node & filename



const cardNumber = arguments[0].split('/')[0];
const issueNumber = arguments[0].split('/')[1];
const surname = arguments[1];
const day = arguments[2];
const month = arguments[3];
const year = arguments[4];
const fullDate = `${month}/${day}/${year}`;

if(!validate(rules, [cardNumber, issueNumber, surname, day, month, year]) || !isValidDate(fullDate)) {
    console.log('invalid');
    process.exit();
}
const horseman = new Horseman(scrapConf);

try {
    horseman
        .userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0')
        .viewport(1280, 1024)
        .open('https://wwccheck.ccyp.nsw.gov.au/Employers/Login?ReturnUrl=%2fEmployers%2fSearch')
        .waitForNextPage()
        .type('#Username', 'littleones')
        .type('#Password', 'little123!')
        .click('#Login')
        .waitForNextPage()
        .type('#Criteria_0__FamilyName', surname)
        .type('#Criteria_0__BirthDate', fullDate)
        .type('#Criteria_0__AuthorisationNumber', cardNumber)
        .click('#Verify')
        .evaluate(function () {
            if ($('#tblVerificationResults').length !== 0) {
                return $('#tblVerificationResults tbody').find('td:eq(2)').html() === "NOT FOUND";
            }
        })
        .then(function(result) {
            console.log(result ? 'invalid' : 'valid');
        })
        .close();
} catch(e) {
    console.log('invalid');
}

