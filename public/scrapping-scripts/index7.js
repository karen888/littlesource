// 
// https://www.dcsiscreening.sa.gov.au/
// https://www.dcsiscreening.sa.gov.au/SCRegisteredPersonSearch
//

const puppeteer = require('puppeteer');


const { validate, isValidDate } = require('./helpers/validation');
const scrapConf = {
    'timeout': 90000,
    'loadImages': false,
    'ignoreSSLErrors': true,
    'webSecurity': false
};

const rules = {
    referenceNumber: /^[a-zA-Z0-9]+$/,
    firstName: /^[a-zA-Z\s]*$/,
    middleName: /^[a-zA-Z\s]*$/,
    lastName: /^[a-zA-Z\s]+$/,
    day: /^[0-9]+$/,
    month: /^[0-9]+$/,
    year: /^[0-9]+$/
};

const arguments = process.argv.slice(2); // removing first two arguments: node & filename

const referenceNumber = arguments[0];
const fullName = arguments[1].split('_');
var firstName = '';
var middleName = '';
var lastName = '';

if (fullName.length == 1) {
    lastName = fullName[0];
} else if(fullName.length == 2) {
    firstName = fullName[0];
    lastName = fullName[1];
} else if(fullName.length == 3) {
    firstName = fullName[0];
    middleName = fullName[1];
    lastName = fullName[2];
}

const day = arguments[2];
const month = arguments[3].replace(/^0+/, '');
const year = arguments[4];
const screeningtype = '';

if(!validate(rules, [referenceNumber, firstName, middleName, lastName, day, month, year]) || !isValidDate(`${month}/${day}/${year}`)) {
    console.log('___validate(rules, [referenceNumber, firstName, middleName, lastName, day, month, year])', validate(rules, [referenceNumber, firstName, middleName, lastName, day, month, year]));
    console.log('___validation date:', isValidDate(`${month}/${day}/${year}`));
    console.log('invalid, not passed validation');
    console.log('invalid');
    process.exit();
}

//
// username and password
//
const username = 'info@rosscorp.com.au';
const password = 'little123#';

(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    page.on('console', msg => console.log('PAGE LOG:', msg.text()));
    await page.setViewport({width: 1280, height: 1600});
    await page.goto('https://www.dcsiscreening.sa.gov.au/');
    await page.evaluate(() => console.log(`url is ${location.href}`));
    await page.click('input[title="Username"]');
    await page.keyboard.type(username);
    await page.click('input[title="Password"]');
    await page.keyboard.type(password);
    await page.click('input[value="Login"]');
    await page.waitForNavigation();
    await page.waitForSelector('a[href="/SCRegisteredPersonSearch"]');
    await page.evaluate(() => console.log(`url is ${location.href}`));
    await page.goto('https://www.dcsiscreening.sa.gov.au/SCRegisteredPersonSearch');
    await page.waitForSelector('#searchBtn');
    await page.evaluate(() => console.log(`url is ${location.href}`));
    await page.type('#firstName', firstName, {delay: 50});
    await page.type('#middleName', middleName, {delay: 50});
    await page.type('#lastName', lastName, {delay: 50});
    await page.type('#inputDOBDay', day, {delay: 50});
    await page.select('#selDOBMonth', month);
    await page.type('#inputDOBDay', day, {delay: 50});
    await page.type('#inputDOBYear', year, {delay: 50});
    await page.evaluate(() => console.log(`url is ${location.href}`));
    await page.evaluate(() => console.log(`divNoResults class ${document.querySelector('#divNoResults').getAttribute('class')}`));
    if ((referenceNumber !== '') && (screeningtype !== '')) {
        console.log('You must enter only one , either referencenumber or screeningtype');
        await browser.close();
    }

    if (referenceNumber !== '') {
        await page.type('#referenceNumber', referenceNumber, {delay: 50});
        await page.click('#searchBtn');
        await page.waitForSelector('#divSectionResults:not(.hidden)');
        await page.evaluate(() => console.log(`url is ${location.href}`));
        await page.evaluate(() => console.log(`divNoResults class ${document.querySelector('#divNoResults').getAttribute('class')}`));
        await page.evaluate(() => console.log(`divSectionResults class ${document.querySelector('#divSectionResults:not(.hidden)').getAttribute('class')}`));
        await page.evaluate(() => console.log(`divResults class ${document.querySelector('#divResults').getAttribute('class')}`));
        await page.screenshot({path: '../upload/user/divSectionResults.png'});
        const isVisible = await page.evaluate(() => {
            const e = document.querySelector('#divNoResults');
            if (!e)
                return false;
            const style = window.getComputedStyle(e);
            return style && style.display !== 'none' && style.visibility !== 'hidden' && style.opacity !== '0';
        });
    
        console.log(isVisible ? 'invalid' : 'valid');
    }
    await browser.close();
})();