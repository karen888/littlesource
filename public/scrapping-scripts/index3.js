
//
// https://wwcforms.justice.tas.gov.au/Registration/
//
const Horseman = require('node-horseman');
const { validate, isValidDate } = require('./helpers/validation');
const scrapConf = {
	'timeout': 90000,
	'loadImages': false,
	'ignoreSSLErrors': true,
	'webSecurity': false
};

const rules = {
    cardNumber: /^[0-9A-Za-z]+$/,
    surname: /^[a-zA-Z]+$/,
    day: /^[0-9]+$/,
    month: /^[0-9]+$/,
    year: /^[0-9]+$/
};

const arguments = process.argv.slice(2); // removing first two arguments: node & filename



const cardNumber = arguments[0];
const surname = arguments[1];
const day = arguments[2];
const month = arguments[3];
const year = arguments[4];

if(!validate(rules, arguments) || !isValidDate(`${month}/${day}/${year}`)) {
    console.log('invalid');
    process.exit();
}

const horseman = new Horseman(scrapConf);
try {
    horseman
        .userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0')
        .viewport(1280, 1024)
        .open('https://wwcforms.justice.tas.gov.au/Registration/')
        .waitForNextPage()
        .type('input[name="ctl00$ctl00$ctlMainContent$ctlMainContent$ctlAmendmentSearch$txtCardNumber"]', cardNumber)
        .type('input[name="ctl00$ctl00$ctlMainContent$ctlMainContent$ctlAmendmentSearch$txtSurname"]', surname)
        .type('input[name="ctl00$ctl00$ctlMainContent$ctlMainContent$ctlAmendmentSearch$ctlDOB$txtD"]', day)
        .type('input[name="ctl00$ctl00$ctlMainContent$ctlMainContent$ctlAmendmentSearch$ctlDOB$txtM"]', month)
        .type('input[name="ctl00$ctl00$ctlMainContent$ctlMainContent$ctlAmendmentSearch$ctlDOB$txtY"]', year)
        .click('#ctl00_ctl00_ctlMainContent_ctlMainContent_ctlAmendmentSearch_btnSearch')
        .waitForNextPage()
        .evaluate(function () {
            return !!$('.MessageError').length;
        })
        .then(function(result) {
            console.log(result ? 'invalid' : 'valid');
        })
        .close();
} catch (e) {
    console.log('invalid');
}
