
//
// https://www.bluecard.qld.gov.au/onlinevalidation/validation.aspx
//
const Horseman = require('node-horseman');
const { validate, isValidDate } = require('./helpers/validation');
const scrapConf = {
    'timeout': 90000,
	'loadImages': false,
	'ignoreSSLErrors': true,
	'webSecurity': false
};

const rules = {
    cardNumber: /^[a-zA-Z0-9]+$/,
    issueNumber: /^[a-zA-Z0-9]+$/,
    fullName: /^[a-zA-Z\s]+$/,
    day: /^[0-9]+$/,
    month: /^[0-9]+$/,
    year: /^[0-9]+$/
};

const arguments = process.argv.slice(2); // removing first two arguments: node & filename


const cardNumber = arguments[0].split('/')[0];
const issueNumber = arguments[0].split('/')[1];
const fullName = arguments[1].replace(/_/g, ' ');
const day = arguments[2].replace(/^0+/, '');
const month = arguments[3].replace(/^0+/, '');
const year = arguments[4];

if(!validate(rules, [cardNumber, issueNumber, fullName, day, month, year]) || !isValidDate(`${month}/${day}/${year}`)) {
    console.log('invalid');
    process.exit();
}

const horseman = new Horseman(scrapConf);
horseman
	.userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0')
	.viewport(1280, 1024)
	.open('https://www.bluecard.qld.gov.au/onlinevalidation/validation.aspx')
	.waitForNextPage()
	.type('input[name="FullName"]', fullName)
	.type('input[name="CardNumber"]', cardNumber)
	.type('input[name="IssueNumber"]', issueNumber)
	.select('select[name="ExpiryDate$selDay"]', day)
	.select('select[name="ExpiryDate$selMonth"]', month)
	.select('select[name="ExpiryDate$selYear"]', year)
    .click('input[name="ValidateCardBtn"]')
    .waitForNextPage()
    .evaluate(function () {
        return !!($('#ResultMessages').find('p').text().search('is a valid card') == -1 || $('#ValidationSummaryCtrl').find('ul').find('li').length > 0);
    })
    .then(function(result) {
        console.log(result ? 'invalid' : 'valid');
    })
    .close();

