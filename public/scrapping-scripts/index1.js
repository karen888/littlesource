//
// https://online.justice.vic.gov.au/wwccu/checkstatus.doj
//
const Horseman = require('node-horseman');
const { validate } = require('./helpers/validation');
const scrapConf = {
	'timeout': 90000,
	'loadImages': false,
	'ignoreSSLErrors': true,
	'webSecurity': false
};
const rules = {
    cardNumber: /^([0-9]{7})([a-zA-Z0-9])+$/,
    surname: /^[a-zA-Z]+$/
};

const arguments = process.argv.slice(2);


if (!validate(rules, arguments)) {
    console.log('invalid');
    process.exit();
}

const cardNumber = arguments[0];
const surname = arguments[1];


//
// create horseman with custom settings
//
//

const horseman = new Horseman(scrapConf);
try {
    horseman
        .userAgent('Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0')
        .viewport(1280, 1024)
        .open('https://online.justice.vic.gov.au/wwccu/checkstatus.doj')
        .waitForNextPage()
        .type('input[name="cardnumber"]', cardNumber)
        .type('input[name="lastname"]', surname)
        .click('#pageAction_submit')
        .waitForNextPage()
        .evaluate(function () {
            return !!($('#message .error').find('p').length);
        })
        .then(function(result) {
            console.log(result ? 'invalid' : 'valid');
        })
        .close();
} catch (e) {
    console.log('invalid');
}

